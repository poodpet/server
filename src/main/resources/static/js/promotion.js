const instance = axios.create({
  // baseURL: 'https://poodone.com',
  baseURL: 'https://test.pood.pet',
  withCredentials: false,
  headers: {
    // 'Cache-Control': 'no-cache',
    'Content-Type': 'application/json',
    Accept: '*/*'
  }
})



var app = new Vue({
  el: '#app',
  data: {
    type: '',
    name: '',
    subName: '',
    startPeriod: '',
    endPeriod: '',
    promotionImage: '',
    promotionGoods: [],
    promotionGoodsGroup: [],
    activeTab: 0,
    pageInit: false,
    slickOption: {
      slidesToShow: 1,
      infinite: true,
      arrows: false,
      dots: true,
      dotsClass: 'custom-dots'
    },
    idx: null
  },
  methods: {
    tabClickEvent (index) {
      this.activeTab = index
    },
    convertProductImage (url) {
      return {'background-image': 'url(' + url + ')'}
    },
    convertThumbnail (url) {
      return {'background-image': 'url(' + url + ')', 'height': window.innerHeight + 'px', 'width': window.innerWidth + 'px'}
    },
    screenWidth () {
      return { 'width': window.innerWidth + 'px' }
    },
    backButtonEvent () {
      window.flutter_inappwebview.callHandler('closeWebView')
    },
    productClick (idx) {
      window.flutter_inappwebview.callHandler('getDealItem', idx)
    },
    setCommonData (data) {
      this.type = data.type.toLocaleLowerCase()
      this.name = data.name
      this.subName = data.subName
      this.startPeriod = data.startPeriod.slice(0, 10)
      this.endPeriod = data.endPeriod.slice(0, 10)
    },
    setTypeE (data) {
      if (this.type === 'e') {
        this.promotionImage = data.promotionImageList.map(item => item.url)
        this.slickOption.dotsClass += ' length-' + this.promotionImage.length
        this.$nextTick(() => {
          $(".slider").slick(this.slickOption)
        })
      }
    },
    setTypeB (data) {
      if (this.type === 'b') {
        this.promotionGoodsGroup = data.promotionGroupGoods.map(item => {
          return { name: item.name }
        })
        this.promotionGoods = data.promotionGroupGoods.map(item => {
          return item.promotionGoods.map(promotion => {
            return promotion
          })
        })
      }
    },
    setTypeNotB (data) {
      if (this.type !== 'b') {
        this.promotionGoods = data.promotionGroupGoods[0].promotionGoods
      }
    },
    setTypeNotE (data) {
      if (this.type !== 'e') {
        this.promotionImage = data.promotionImageList[0].url
      }
    },
    async init () {
      var search = location.search
      var params = new URLSearchParams(search);
      this.idx = new URLSearchParams(location.search).get('idx');
      const response = await instance.get(`/api/pood/promotion/${this.idx}`)
      this.setCommonData(response.data)
      this.setTypeNotB(response.data)
      this.setTypeNotE(response.data)
      this.setTypeB(response.data)
      this.setTypeE(response.data)
      this.$nextTick(() => {
        this.pageInit = true
      })
    }
  },
  async mounted () {
    this.init();
  }
})
