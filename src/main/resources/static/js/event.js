const instance = axios.create({
  baseURL: 'https://poodone.com',
  withCredentials: false,
  headers: {
    // 'Cache-Control': 'no-cache',
    'Content-Type': 'application/json',
    Accept: '*/*'
  }
})

var app = new Vue({
  el: '#app',
  data: {
    title: '',
    type: null,
    startPeriod: '',
    endPeriod: '',
    showCommentRegister: false,
    buttonBottomMode: false,
    buttonWording: '',
    activeTab: 0,
    comment: '',
    commentList: [],
    imageList: [],
    idx: null,
    uuid: null,
    token: null,
    headImage: '',
    pageInit: false,
    showPhotoDetail: false,
    schemeUri: '',
    slickOption: {
      slidesToShow: 1,
      infinite: true,
      arrows: false,
      dots: true,
      dotsClass: 'custom-dots'
    },
    photoDetail: {
      userProfile: '',
      userName: '',
      photoDetails: [],
      petList: []
    },
    petSlick: {
      infinite: true,
      arrows: false,
      dots: false,
      variableWidth: true
    }
  },
  computed: {
    showEvent () {
      return this.activeTab === 0
    },
    showJoin () {
      return this.activeTab === 1
    },
    showButton () {
      return this.type === 'b' || this.type === 'c' || this.type === 'd' || this.type === 'e'
    },
    buttonDisabled () {
      return this.buttonBottomMode && this.comment === ''
    }
  },
  methods: {
    async showImageDetail (userIdx) {
      this.showPhotoDetail = true
      const response = await instance.get(`/api/pood/event/photo/${this.idx}/${userIdx}`)
      console.log(response.data)
      this.photoDetail = {
        userProfile: response.data.userInfoSimpleData.userBaseImage.url,
        userName: response.data.userInfoSimpleData.nickName,
        photoDetails: response.data.photoSimpleDataList,
        petList: response.data.userPetSimpleDataList
        // age: 0
        // idx: 565
        // pcKind: "치와와"
        // pcSize: "소형견"
        // petName: "10101"
        // userPetImage: null
      }
      // this.slickOption.dotsClass += ' length-' + this.photoDetail.photoDetails.length
      // this.slickOption.dotsClass += ' length-2'
      this.$nextTick(() => {
        $(".slider").slick(this.slickOption)
        $(".pet-list").slick(this.petSlick)
      })
    },
    screenWidth () {
      return { 'width': window.innerWidth + 'px' }
    },
    closePhotoDetail () {
      console.log('click')
      document.getElementsByClassName('photo-detail')[0].classList.add('close')
      setTimeout(() => {
        console.log($('.slider'))
        $(".slider").unslick()
        this.showPhotoDetail = false
      }, 600)
    },
    setCommonData (data) {
      console.log(data)
      this.type = data.typeName.toLocaleLowerCase()
      this.title = data.title
      this.startPeriod = data.startDate.slice(0, 10).replaceAll('-', '.')
      this.endPeriod = data.endDate.slice(0, 10).replaceAll('-', '.')
      this.headImage = data.eventImageList[0].url
    },
    async tabClickEvent (index) {
      this.activeTab = index
      if (index === 1) {
        if (this.type === 'c') {
          const response = await instance.get(`/api/pood/event/comment/${this.idx}`)
          this.commentList = response.data.content
        } else if (this.type === 'd') {
          await this.getImageList()
        }
      }
    },
    async getImageList () {
      const response = await instance.get(`/api/pood/event/photo/${this.idx}`)
      for (let i = 0; i<25; i++) {
        this.imageList = this.imageList.concat(response.data.content)
      }
    },
    loadMore () {
      console.log('hi')
      // for (let i = 0; i<25; i++) {
      //   this.imageList = this.imageList.concat([this.imageList[0], this.imageList[1]])
      // }
    },
    closeCommentRegister () {
      this.showCommentRegister = false
      this.buttonBottomMode = false
      this.buttonWording = '참여하기'
      this.comment = ''
      document.body.style.overflowY = ''
    },
    convertProductImage (url) {
      return {'background-image': 'url(' + url + ')'}
    },
    convertEventThumbnail (url) {
      return {'background-image': 'url(' + url + ')', 'height': window.innerHeight + 'px'}
    },
    convertPhotoDetailSlick (url) {
      return {height: window.innerWidth + 'px', 'background-image': 'url(' + url + ')'}
    },
    async buttonClick () {
      if (this.type === 'b') {
        // app handler api call - 앱내 다른 페이지로 이동
        window.flutter_inappwebview.callHandler('showModal', this.schemeUri)
      } else if (!this.uuid && !this.token) {
        // 로그인 안했을 때
        window.flutter_inappwebview.callHandler('showModal', 3)
      } else if (this.type === 'd' && this.uuid && this.token) {
        // 이벤트 타입 D
        // app handler api call - 포토 이벤트 참여
      } else if (this.type === 'e' && this.uuid && this.token) {
        // 이벤트 타입 E
        try {
          await instance.post(`/api/pood/event/participation`, {idx: this.idx}, {
            headers: {
              'uuid': this.uuid,
              'token': this.token
            }
          })
          window.flutter_inappwebview.callHandler('showModal', 0)
        } catch (error) {
          window.flutter_inappwebview.callHandler('showModal', 1)
        }
      } else if (this.type === 'c' && this.comment === '') {
        // 이벤트 타입 C 댓글 입력창 보이게
        this.buttonBottomMode = true
        this.showCommentRegister = true
        this.buttonWording = '등록하기'
        this.comment = ''
        document.body.style.overflowY = 'hidden'
      } else if (this.type === 'c' && this.comment !== '' && this.uuid && this.token) {
        // 이벤트 타입 C 댓글 등록
        try {
          await instance.post('/api/pood/event/comment', { comment: this.comment, eventInfoidx: this.idx }, {
            headers: {
              'uuid': this.uuid,
              'token': this.token
            }
          })
          this.closeCommentRegister()
          window.flutter_inappwebview.callHandler('showModal', 0)
        } catch (error) {
          this.closeCommentRegister()
          window.flutter_inappwebview.callHandler('showModal', 1)
        }
      }
    },
    backButtonEvent () {
      window.flutter_inappwebview.callHandler('closeWebView')
    },
    async setTypeD () {
      if (this.type === 'd') {
        this.buttonWording = '참여하기'
      }
    },
    setTypeB (data) {
      if (this.type === 'b') {
        this.buttonWording = '확인하기'
        this.schemeUri = data.schemeUri
      }
    },
    setTypeE () {
      if (this.type === 'e') {
        this.buttonWording = '참여하기'
      }
    },
    async init (result) {
      this.idx = result.idx
      this.token = result.token
      this.uuid = result.uuid
      const response = await instance.get(`/api/pood/event/${this.idx}`)
      this.setCommonData(response.data)
      this.setTypeB(response.data)
      await this.setTypeD()
      this.setTypeE()
      this.pageInit = true
    }
  },
  async mounted () {
    window.addEventListener("flutterInAppWebViewPlatformReady", (event) => {
      window.flutter_inappwebview.callHandler('setWebView', '*** Message from server js *** ')
          .then(async (result) => {
            this.init(result)
          })
    });
    // 54 - type D event
    // 66 - type C event
    // this.init({ idx: 26, token: '8155c2c5-5c96-492c-bb70-137a340c80ea', uuid: '545eec37-ada8-439f-8446-d6d28742e04c' })
  }
})
