package com.pood.server;

import com.pood.server.config.AES256;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) throws Exception {
        AES256.keySpecGenerator();
        SpringApplication.run(Application.class, args);
    }
}