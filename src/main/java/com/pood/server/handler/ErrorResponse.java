package com.pood.server.handler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import java.time.LocalDateTime;
import lombok.Value;

@Value(staticConstructor = "of")
class ErrorResponse {

    LocalDateTime timestamp;
    int status;
    String error;
    String message;
    String path;

    static ErrorResponse badRequest(final String message, final String path) {
        return ErrorResponse.of(LocalDateTime.now(), BAD_REQUEST.value(),
            BAD_REQUEST.getReasonPhrase(), message, path);
    }

    static ErrorResponse unAuthorized(final String message, final String path) {
        return ErrorResponse.of(LocalDateTime.now(), UNAUTHORIZED.value(),
            UNAUTHORIZED.getReasonPhrase(), message, path);
    }

    static ErrorResponse forbidden(final String message, final String path) {
        return ErrorResponse.of(LocalDateTime.now(), FORBIDDEN.value(),
            FORBIDDEN.getReasonPhrase(), message, path);
    }

}
