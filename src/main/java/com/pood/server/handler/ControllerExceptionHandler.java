package com.pood.server.handler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import com.pood.server.exception.AlreadyExistException;
import com.pood.server.exception.CustomStatusException;
import com.pood.server.exception.ErrorMessage;
import com.pood.server.exception.IndexNotMatchException;
import com.pood.server.exception.InvalidTokenException;
import com.pood.server.exception.LimitQuantityException;
import com.pood.server.exception.NotEmailTypeException;
import com.pood.server.exception.NotFoundException;
import com.pood.server.exception.PasswordNotMatchException;
import com.pood.server.exception.RefundException;
import com.pood.server.exception.SmsSendException;
import com.pood.server.exception.UsageTimeExpiredException;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> errorResponse(final ConstraintViolationException e,
        HttpServletRequest request) {
        final Optional<ConstraintViolation<?>> validated = e.getConstraintViolations().stream()
            .findFirst();

        if (validated.isEmpty()) {
            throw CustomStatusException.serverError(ErrorMessage.SERVER);
        }

        final String errorMessage = validated.get().getMessage();
        return ResponseEntity.badRequest()
            .body(ErrorResponse.badRequest(errorMessage,
                request.getRequestURI()));
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> errorResponse(final MethodArgumentNotValidException e,
        final HttpServletRequest request) {
        return ResponseEntity.badRequest()
            .body(ErrorResponse.badRequest(
                e.getBindingResult().getAllErrors().get(0).getDefaultMessage(),
                request.getRequestURI()));
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> errorResponse(final InvalidTokenException e,
        final HttpServletRequest request) {
        return ResponseEntity.status(UNAUTHORIZED)
            .body(ErrorResponse.unAuthorized(
                e.getMessage(), request.getRequestURI()));
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> errorResponse(final UsageTimeExpiredException exception,
        final HttpServletRequest request) {
        return ResponseEntity.status(FORBIDDEN)
            .body(ErrorResponse.forbidden(exception.getMessage(), request.getRequestURI()));
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> errorResponse(final NotFoundException exception,
        final HttpServletRequest request) {
        return ResponseEntity.badRequest()
            .body(ErrorResponse.badRequest(exception.getMessage(), request.getRequestURI()));
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> errorResponse(final AlreadyExistException exception,
        final HttpServletRequest request) {
        return ResponseEntity.badRequest()
            .body(ErrorResponse.badRequest(exception.getMessage(), request.getRequestURI()));
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> errorResponse(final PasswordNotMatchException exception,
        final HttpServletRequest request) {
        return ResponseEntity.status(BAD_REQUEST)
            .body(ErrorResponse.unAuthorized(exception.getMessage(), request.getRequestURI()));
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> errorResponse(final SmsSendException exception, final HttpServletRequest request) {
        return ResponseEntity.status(BAD_REQUEST)
            .body(ErrorResponse.badRequest(exception.getMessage(), request.getRequestURI()));
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> errorResponse(final NotEmailTypeException exception, final HttpServletRequest request) {
        return ResponseEntity.status(BAD_REQUEST)
            .body(ErrorResponse.badRequest(exception.getMessage(), request.getRequestURI()));
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> errorResponse(final LimitQuantityException exception, final HttpServletRequest request) {
        return ResponseEntity.status(BAD_REQUEST)
            .body(ErrorResponse.badRequest(exception.getMessage(), request.getRequestURI()));
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> errorResponse(final IndexNotMatchException exception, final HttpServletRequest request) {
        return ResponseEntity.status(UNAUTHORIZED)
            .body(ErrorResponse.unAuthorized(exception.getMessage(), request.getRequestURI()));
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> errorResponse(final RefundException exception, final HttpServletRequest request) {
        return ResponseEntity.status(BAD_REQUEST)
            .body(ErrorResponse.badRequest(exception.getMessage(), request.getRequestURI()));
    }
}
