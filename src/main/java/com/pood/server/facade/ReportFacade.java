package com.pood.server.facade;

import com.pood.server.controller.request.report.UserReportSaveRequest;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.service.UserReportSeparate;
import com.pood.server.service.UserSeparate;
import lombok.RequiredArgsConstructor;

@Facade
@RequiredArgsConstructor
public class ReportFacade {

    private final UserReportSeparate userReportSeparate;
    private final UserSeparate userSeparate;

    public void reportUser(final UserReportSaveRequest request, final UserInfo myInfo) {
        final UserInfo reportUser = userSeparate.findByIdx(request.getUserInfoIdx());
        userReportSeparate.reportUser(request, reportUser, myInfo.getIdx());
    }

}
