package com.pood.server.facade.home;

import com.pood.server.controller.response.home.rank.PoodRankCategoryDto;
import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.dto.meta.goods.PromotionDto.PromotionGoodsProductInfo;
import com.pood.server.dto.order.OrderRankGroup;
import com.pood.server.entity.meta.GoodsSubCt;
import com.pood.server.entity.meta.group.CustomRankInfoGroup;
import com.pood.server.facade.Facade;
import com.pood.server.entity.meta.dto.home.OrderRankDto;
import com.pood.server.service.GoodsSeparate;
import com.pood.server.service.GoodsSubCtSeparate;
import com.pood.server.service.OrderSeparate;
import com.pood.server.service.PoodRankCategorySeparate;
import com.pood.server.service.PromotionSeparate;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Facade
@RequiredArgsConstructor
public class PoodHomeRankFacade {

    private static final String DAY = "DAY";
    private static final String WEEK = "WEEK";
    private static final String MONTH = "MONTH";
    private static final int MAX_RANK_SIZE = 100;

    private final PoodRankCategorySeparate poodRankCategorySeparate;
    private final GoodsSubCtSeparate goodsSubCtSeparate;
    private final GoodsSeparate goodsSeparate;
    private final PromotionSeparate promotionSeparate;
    private final OrderSeparate orderSeparate;

    public List<PoodRankCategoryDto> getPoodHomeRankCategory(final int pcIdx) {
        return poodRankCategorySeparate.getCategoryList(pcIdx);
    }

    public Page<SortedGoodsList> getPoodHomeRankGoodsList(final Long subCtIdx, final int pcIdx,
        final String searchType, final Pageable pageable) {

        final List<Integer> goodsIdxSearchList = getGoodsIdxListByGoodSubType(subCtIdx, pcIdx);

        final OrderRankGroup purchaseRankGroup = orderSeparate.getRankGoodsGroup(
            goodsIdxSearchList, searchType, pageable);

        final OrderRankGroup orderRankGroup = addCustomRankContents(purchaseRankGroup, pcIdx,
            getCustomRankSize(searchType), goodsIdxSearchList);

        List<Integer> goodsIdxList =  orderRankGroup.getGoodsIdsList();

        List<SortedGoodsList> goodsInfoList = goodsSeparate.getSortedGoodsInIdxList(goodsIdxList);

        Page<SortedGoodsList> sortedGoodsListPage =  orderRankGroup.convertSortedGoods(goodsInfoList);

        for (PromotionGoodsProductInfo promotionGoodsProductInfo : promotionSeparate.findByGoodsIdxAndNowActiveList(
            goodsIdxList)) {
            for (SortedGoodsList sortedGoodsWishList : sortedGoodsListPage) {
                if (sortedGoodsWishList.getIdx().equals(promotionGoodsProductInfo.getGoodsIdx())) {
                    sortedGoodsWishList.promotionIfExistUpdatePrice(promotionGoodsProductInfo);
                }
            }
        }

        return sortedGoodsListPage;
    }

    private List<Integer> getGoodsIdxListByGoodSubType(final Long subCtIdx, final int pcIdx) {
        final GoodsSubCt goodsSubCt = goodsSubCtSeparate.getGoodsSubCtInfoByIdxOrNull(subCtIdx,
            pcIdx);
        return goodsSeparate.getRankGoodsIdxList(goodsSubCt, pcIdx);
    }

    private OrderRankGroup addCustomRankContents(final OrderRankGroup originRankGroup,
        final int pcIdx, final int addSize, final List<Integer> goodsIdxSearchList) {

        if(originRankGroup.getSize() >= MAX_RANK_SIZE){
            return originRankGroup;
        }

        final CustomRankInfoGroup customRankAllList = goodsSeparate
            .getCustomRankListNotIn(originRankGroup.getGoodsIdsList(), pcIdx)
            .sortedScoreAndSize(addSize);

        final List<OrderRankDto> customRankList = customRankAllList
            .findContainIdxList(goodsIdxSearchList)
            .stream().map(OrderRankDto::of)
            .collect(Collectors.toList());

        return originRankGroup.addCustomRankContents(
            customRankList.stream()
                .limit(MAX_RANK_SIZE - originRankGroup.getSize())
                .collect(Collectors.toList()));
    }

    private int getCustomRankSize(final String searchType) {
        if (DAY.equals(searchType)) {
            return 15;
        }
        if (WEEK.equals(searchType)) {
            return 30;
        }
        if (MONTH.equals(searchType)) {
            return 100;
        }
        return 0;
    }

}
