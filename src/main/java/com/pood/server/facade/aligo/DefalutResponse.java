package com.pood.server.facade.aligo;

import lombok.Getter;

@Getter
public class DefalutResponse implements AligoDefalutResponse {

    private int code;
    private String message;

    @Override
    public boolean isFail() {
        return code != 0;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
