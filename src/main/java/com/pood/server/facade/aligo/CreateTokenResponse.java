package com.pood.server.facade.aligo;

import lombok.Getter;

@Getter
public class CreateTokenResponse implements AligoDefalutResponse {

    private int code;
    private String message;
    private String token;
    private String urlencode;

    @Override
    public boolean isFail() {
        return code != 0;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
