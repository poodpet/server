package com.pood.server.facade.aligo;

public interface AligoDefalutResponse {

    boolean isFail();

    String getMessage();

}
