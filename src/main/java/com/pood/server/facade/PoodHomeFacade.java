package com.pood.server.facade;

import com.pood.server.dto.meta.marketing.MarketingDataSet;
import com.pood.server.service.MarketingSortSeparate;
import java.util.List;
import lombok.RequiredArgsConstructor;

@Facade
@RequiredArgsConstructor
public class PoodHomeFacade {

    private final MarketingSortSeparate marketingSortSeparate;

    public List<MarketingDataSet> findAllMarketingData(final int petCategoryIdx) {
        return marketingSortSeparate.findAllMarketingData(petCategoryIdx)
            .sortedList();
    }

}
