package com.pood.server.facade;

import com.pood.server.api.req.header.pay.hPay_cancel_1_2;
import com.pood.server.dto.meta.order.dto_order;
import com.pood.server.dto.order.OrderBasketQuantity;
import com.pood.server.dto.order.OrderBasketUserGroup;
import com.pood.server.dto.user.basket.BasketQuantity;
import com.pood.server.dto.user.basket.BuyLimit;
import com.pood.server.entity.meta.Goods;
import com.pood.server.entity.meta.mapper.order.RefundDeliveryMapper;
import com.pood.server.entity.order.Order;
import com.pood.server.entity.order.OrderBasket;
import com.pood.server.entity.order.OrderCancel;
import com.pood.server.entity.order.OrderDelivery;
import com.pood.server.entity.order.dto.OrderBaseDataDto;
import com.pood.server.entity.order.group.NaverProductGroup;
import com.pood.server.entity.order.group.OrderCancelPriceInfoGroup;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.exception.NotFoundException;
import com.pood.server.facade.order.OrderCancelInfoRequest;
import com.pood.server.facade.order.OrderCancelInfoResponse;
import com.pood.server.facade.order.RefundAddressResponse;
import com.pood.server.service.DeliveryCourierSeparate;
import com.pood.server.service.GoodsSeparate;
import com.pood.server.service.OrderBasketSeparate;
import com.pood.server.service.OrderCancelSeparate;
import com.pood.server.service.OrderDeliverySeparate;
import com.pood.server.service.OrderSeparate;
import com.pood.server.service.UserBasketSeparate;
import com.pood.server.service.factory.OrderCancelFactory;
import com.pood.server.service.factory.OrderCancelPriceInfo;
import com.pood.server.web.mapper.payment.pay.NaverProduct;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;

@Facade
@RequiredArgsConstructor
public class OrderFacade {

    private final static int ISISLAND_REMOTE_TYPE = 2;

    private final OrderBasketSeparate orderBasketSeparate;
    private final GoodsSeparate goodsSeparate;
    private final UserBasketSeparate userBasketSeparate;
    private final OrderSeparate orderSeparate;
    private final OrderCancelSeparate orderCancelSeparate;
    private final OrderDeliverySeparate orderDeliverySeparate;
    private final DeliveryCourierSeparate deliveryCourierSeparate;

    public String getGoodsCancelMsg(final dto_order order,
        final List<hPay_cancel_1_2> canceList) {
        StringBuilder sendCancelMsg = new StringBuilder("[");

        List<Integer> goodsIdxList = canceList.stream().map(hPay_cancel_1_2::getGoods_idx)
            .collect(Collectors.toList());
        List<String> goodsNameList = goodsSeparate.getGoodsNameList(goodsIdxList);
        sendCancelMsg.append(goodsNameList.get(0));
        appendSendCancelMsg(canceList, sendCancelMsg, goodsNameList);

        sendCancelMsg.append("]이 정상적으로 취소되었습니다.");
        return sendCancelMsg.toString();
    }

    private void appendSendCancelMsg(List<hPay_cancel_1_2> canceList, StringBuilder sendCancelMsg,
        List<String> goodsNameList) {
        if (goodsNameList.size() > 1) {
            sendCancelMsg.append(" 외 ");
            sendCancelMsg.append(canceList.size() - 1);
            sendCancelMsg.append("건");
        }
    }

    public List<NaverProduct> getIamportNaverProducts(final String orderNumber) {

        List<OrderBasket> orderBasketList = orderBasketSeparate.getOrderBasketByOrderNumber(
            orderNumber);
        NaverProductGroup naverProductGroup = new NaverProductGroup();
        List<Goods> goodsListByIdxIn = goodsSeparate.getGoodsListByIdxIn(
            orderBasketList.stream().map(OrderBasket::getGoodsIdx).collect(
                Collectors.toList()));
        for (OrderBasket orderBasket : orderBasketList) {
            addNaverProductGroup(naverProductGroup, goodsListByIdxIn, orderBasket);
        }

        return naverProductGroup.getNaverProducts();
    }


    private void addNaverProductGroup(final NaverProductGroup naverProductGroup,
        List<Goods> goodsListByIdxIn,
        OrderBasket orderBasket) {
        for (Goods goods : goodsListByIdxIn) {
            if (orderBasket.getGoodsIdx().equals(goods.getIdx())) {
                naverProductGroup.add(orderBasket, goods);
                break;
            }
        }
    }

    public int availableQuantity(final int goodsIdx, final UserInfo userInfo) {
        final Goods goods = goodsSeparate.findById(goodsIdx);
        final int totalQuantity = userBasketSeparate.totalQuantity(goods.getIdx(),
            userInfo.getUserUuid());

        final OrderBasketUserGroup userOrderGroup = orderBasketSeparate.getGoodsQuantity(
            userInfo.getIdx(), goods);
        final OrderBasketQuantity orderBasketQuantity = userOrderGroup.filterGoodsIdx(
            goods.getIdx());

        BasketQuantity basketQuantity = BasketQuantity.of(0);

        if (Objects.nonNull(orderBasketQuantity)) {
            basketQuantity = basketQuantity.update(orderBasketQuantity.getOrderQuantity());
        }

        return BuyLimit.of(goods.getLimitQuantity(), totalQuantity, goods.getQuantity(),
            basketQuantity);
    }

    public boolean buyAvailable(final int goodsIdx, final UserInfo userInfo) {
        final Goods goods = goodsSeparate.findById(goodsIdx);
        if (goods.isOnlyOneBoughtChance()) {
            return orderBasketSeparate.isBuyAvailable(userInfo, goods);
        }

        return true;
    }

    public RefundAddressResponse getRefundAddress(final int refundIdx,
        final UserInfo userInfo) {
        RefundDeliveryMapper refundInfo = orderSeparate.getRefundInfo(refundIdx, userInfo.getIdx());
        if (Objects.isNull(refundInfo)) {
            throw new NotFoundException("반품 정보를 찾을 수 없습니다.");
        }
        return new RefundAddressResponse(refundInfo);
    }

    public Order getOrderInfo(final String orderNumber) {
        return orderSeparate.getOrderInfo(orderNumber);
    }

    public OrderBaseDataDto getOrderInfoByOrderNumberAndUserUUID(final String orderNumber,
        final String userUUID) {
        final Order orderInfo = orderSeparate.findByOrderNumberAndUserUuid(orderNumber, userUUID);
        final List<OrderCancel> orderCancelList = orderCancelSeparate.getOrderCancelListByOrderNumber(
            orderNumber);
        final List<OrderBasket> orderBasket = orderBasketSeparate.getOrderBasketByOrderNumber(
            orderNumber);
        final OrderDelivery orderDelivery = orderDeliverySeparate.getOrderDeliveryByOrderNumberAndDefault(
            orderNumber);

        int orgDeliveryFee = deliveryCourierSeparate.getDeliveryFee(orderDelivery.getZipcode(),
            orderDelivery.getRemoteType());
        return new OrderBaseDataDto(orderInfo, orderDelivery, orderBasket, orderCancelList,
            orgDeliveryFee);
    }

    public OrderCancelInfoResponse getOrderCancelInfo(final UserInfo userInfo,
        final OrderCancelInfoRequest orderCancelInfoRequest) {

        final OrderBaseDataDto orderBaseData = this.getOrderInfoByOrderNumberAndUserUUID(
            orderCancelInfoRequest.getOrderNumber(), userInfo.getUserUuid());

        final OrderCancelPriceInfoGroup orderCancelPriceInfoGroup = new OrderCancelPriceInfoGroup();

        for (final Integer goodsIdx : orderCancelInfoRequest.getGoodsIdxList()) {
            final OrderCancelPriceInfo info = OrderCancelFactory.create(orderBaseData,
                orderCancelInfoRequest.getGoodsIdxList(), goodsIdx);
            orderCancelPriceInfoGroup.add(info);
        }
        return orderBaseData.getOrderCancelInfoResponse(orderCancelInfoRequest.getGoodsIdxList(),
            orderCancelPriceInfoGroup, orderBaseData);

    }

    public void updateDeliveryFee(final String merchantUid, final int deliveryFee) {
        orderSeparate.updateDeliveryFee(merchantUid, deliveryFee);
    }


}
