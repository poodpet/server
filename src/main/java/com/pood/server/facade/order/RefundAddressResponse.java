package com.pood.server.facade.order;

import com.pood.server.entity.meta.mapper.order.RefundDeliveryMapper;
import lombok.Value;

@Value
public class RefundAddressResponse {

    Integer idx;
    String uuid;
    String orderNumber;
    Integer goodsIdx;
    Integer qty;
    String nickname;
    String zipcode;
    String receiver;
    String address;
    String addressDetail;
    String phoneNumber;

    public RefundAddressResponse(final RefundDeliveryMapper refundInfo) {
        idx = refundInfo.getIdx();
        uuid = refundInfo.getUuid();
        orderNumber = refundInfo.getOrderNumber();
        goodsIdx = refundInfo.getGoodsIdx();
        qty = refundInfo.getQty();
        nickname = refundInfo.getNickname();
        zipcode = refundInfo.getZipcode();
        receiver = refundInfo.getReceiver();
        address = refundInfo.getAddress();
        addressDetail = refundInfo.getAddressDetail();
        phoneNumber = refundInfo.getPhoneNumber();
    }
}
