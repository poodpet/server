package com.pood.server.facade.order;

import lombok.Value;

@Value
public class OrderCancelInfoResponse {

    int orgGoodsPrice;

    int deliveryFee;

    int goodsPrice;

    long point;

}
