package com.pood.server.facade.order;

import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Value;

@Value

public class OrderCancelInfoRequest {

    @NotNull(message = "주문 번호는 필수 값입니다.")
    String orderNumber;

    @NotNull(message = "goodsIdxList는 필수 값 입니다.")
    @Size(min = 1, message = "취소 상품은 한가지 이상 이여야 합니다.")
    List<Integer> goodsIdxList;

}
