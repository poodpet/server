package com.pood.server.facade;

import com.pood.server.dto.user.point.ExpiredPointGroup;
import com.pood.server.dto.user.point.PointInfoResponse;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.service.LogUserSavePointSeparate;
import com.pood.server.service.UserPointSeparate;
import com.pood.server.util.PointCalculate;
import lombok.RequiredArgsConstructor;

@Facade
@RequiredArgsConstructor
public class UserPointFacade {

    private final UserPointSeparate userPointSeparate;
    private final LogUserSavePointSeparate logUserSavePointSeparate;

    public PointInfoResponse userPointInfoAll(final UserInfo userInfo) {
        final int presentPoint = userInfo.getUserPoint();

        final int receivedBenefitPoint = PointCalculate.totalSavePoint(
            logUserSavePointSeparate.findReceivedBenefitAll(
                userInfo.getUserUuid()));

        final int willBeRewardPoint = PointCalculate.totalSavePoint(
            logUserSavePointSeparate.findToBeRewardPoints(
                userInfo.getUserUuid()));

        final ExpiredPointGroup expiredPointGroup = new ExpiredPointGroup(
            userPointSeparate.willBeExpiredPoint(
                userInfo.getUserUuid()));

        final int removedPoint = expiredPointGroup.removedPoint();

        return PointInfoResponse.of(
            presentPoint,
            receivedBenefitPoint,
            willBeRewardPoint,
            removedPoint
        );
    }

    public int userExpiredPointInfoBy30Day(final UserInfo userInfo) {
        return userPointSeparate.userExpiredPointInfoBy30Day(userInfo.getUserUuid());
    }
}
