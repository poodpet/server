package com.pood.server.facade;

import com.pood.server.controller.response.home.icon.ShortCutIconResponse;
import com.pood.server.service.ShortCutSeparate;
import java.util.List;
import lombok.RequiredArgsConstructor;

@Facade
@RequiredArgsConstructor
public class ShortCutFacade {

    private final ShortCutSeparate shortCutSeparate;

    public List<ShortCutIconResponse> findIconList(final int pcIdx) {
        return shortCutSeparate.findAll(pcIdx);
    }
}
