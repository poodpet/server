package com.pood.server.facade;

import com.pood.server.api.file.uploader.StorageService;
import com.pood.server.controller.request.event.EventBlockRequest;
import com.pood.server.controller.request.user.UserInfoUpdateRequest;
import com.pood.server.controller.request.user.UserSignUpRequest;
import com.pood.server.controller.request.userinfo.UserLoginRequest;
import com.pood.server.controller.request.userinfo.UserLoginResponseDto;
import com.pood.server.controller.request.userwish.UserWishCreateDto;
import com.pood.server.dto.meta.goods.GoodsDto;
import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.dto.meta.goods.PromotionDto;
import com.pood.server.dto.meta.goods.PromotionDto.PromotionGoodsProductInfo;
import com.pood.server.dto.user.point.FriendInvitedGroup;
import com.pood.server.dto.user.point.MyReceivedBenefitResponse;
import com.pood.server.entity.meta.Coupon;
import com.pood.server.entity.meta.Goods;
import com.pood.server.entity.meta.PoodImage;
import com.pood.server.entity.user.UserBaseImage;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserReferralCode;
import com.pood.server.entity.user.UserToken;
import com.pood.server.entity.user.UserWish;
import com.pood.server.exception.AlreadyExistException;
import com.pood.server.facade.user.image.request.UserBaseImageUpdateRequest;
import com.pood.server.facade.user.image.response.UserBaseImagesResponse;
import com.pood.server.facade.user.info.FindUserEmailRequest;
import com.pood.server.facade.user.info.FindUserEmailResponse;
import com.pood.server.facade.user.info.UserInfoSignUpResponse;
import com.pood.server.facade.user.info.UserPasswordChangeRequest;
import com.pood.server.facade.user.info.request.DormantCancelRequest;
import com.pood.server.facade.user.info.request.RequstGoodsRequest;
import com.pood.server.service.CouponSeparate;
import com.pood.server.service.GoodsSeparate;
import com.pood.server.service.LogUserJoinSeparate;
import com.pood.server.service.LogUserMarketingPolicySeparate;
import com.pood.server.service.LogUserTokenSeparate;
import com.pood.server.service.LoginLogSeparate;
import com.pood.server.service.OrderSeparate;
import com.pood.server.service.PoodImageSeparate;
import com.pood.server.service.PromotionSeparate;
import com.pood.server.service.UserBaseImageSeparate;
import com.pood.server.service.UserCouponSeparate;
import com.pood.server.service.UserGoodsRequstSeparate;
import com.pood.server.service.UserPointSeparate;
import com.pood.server.service.UserPostingBlockSeparate;
import com.pood.server.service.UserRecommendCodeSeparate;
import com.pood.server.service.UserRequestRestockSeparate;
import com.pood.server.service.UserSeparate;
import com.pood.server.service.UserSmsAuthSeparate;
import com.pood.server.service.UserTokenSeparate;
import com.pood.server.service.UserWishSeparate;
import com.pood.server.util.SmsGenerator;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Facade
@RequiredArgsConstructor
public class UserFacade {

    private static final int FRIEND_INVITATION = 31;
    private static final int SNS = 3;


    @Value("${legacy.server-url}")
    private String serverUrl;

    private final UserSeparate userSeparate;
    private final UserWishSeparate userWishSeparate;
    private final UserGoodsRequstSeparate userGoodsRequstSeparate;
    private final GoodsSeparate goodsSeparate;
    private final PromotionSeparate promotionSeparate;
    private final UserPointSeparate userPointSeparate;
    private final UserRecommendCodeSeparate userRecommendCodeSeparate;
    private final OrderSeparate orderSeparate;
    private final PoodImageSeparate poodImageSeparate;

    private final LoginLogSeparate loginLogSeparate;
    private final UserTokenSeparate userTokenSeparate;
    private final LogUserTokenSeparate logUserTokenSeparate;

    private final UserPostingBlockSeparate userPostingBlockSeparate;

    private final UserSmsAuthSeparate userSmsAuthSeparate;

    private final LogUserJoinSeparate logUserJoinSeparate;
    private final LogUserMarketingPolicySeparate logUserMarketingPolicySeparate;
    private final UserCouponSeparate userCouponSeparate;
    private final CouponSeparate couponSeparate;

    private final UserBaseImageSeparate userBaseImageSeparate;

    @Qualifier("storageService")
    private final StorageService storageService;

    private final UserRequestRestockSeparate userRequestRestockSeparate;

    public void saveUserWish(final String token, final UserWishCreateDto userWishCreateDto) {
        final UserInfo userInfo = userSeparate.getOptionalUserByToken(token);

        userWishSeparate.saveUserWish(UserWish.builder()
            .userIdx(userInfo.getIdx())
            .goodsIdx(userWishCreateDto.getGoodsIdx())
            .build());
    }

    public boolean userWishValidation(final UserInfo userInfo, final int goodsIdx) {
        return userWishSeparate.wishValidation(userInfo.getIdx(), goodsIdx);
    }

    public void deleteUserWish(final String token, final int goodsIdx) {
        final UserInfo userInfo = userSeparate.getOptionalUserByToken(token);
        userWishSeparate.deleteUserWish(userInfo.getIdx(), goodsIdx);
    }

    public List<SortedGoodsList> getMyGoodsWishList(final String token) {
        final UserInfo userInfo = userSeparate.getOptionalUserByToken(token);
        final List<UserWish> userWishList = userSeparate.getUserWishList(userInfo.getUserUuid());

        final List<Integer> goodsIdxList = userWishList.stream()
            .map(UserWish::getGoodsIdx)
            .collect(Collectors.toList());

        final List<GoodsDto.SortedGoodsList> myGoodsWishList = goodsSeparate.getSortedGoodsInIdxList(
            goodsIdxList);

        final List<PromotionDto.PromotionGoodsProductInfo> promotionList = promotionSeparate.findByGoodsIdxAndNowActiveList(
            goodsIdxList);

        for (PromotionGoodsProductInfo promotionGoodsProductInfo : promotionList) {
            for (SortedGoodsList sortedGoodsWishList : myGoodsWishList) {
                if (sortedGoodsWishList.getIdx().equals(promotionGoodsProductInfo.getGoodsIdx())) {
                    sortedGoodsWishList.promotionIfExistUpdatePrice(promotionGoodsProductInfo);
                }
            }
        }

        return myGoodsWishList;
    }

    public MyReceivedBenefitResponse myReceivedBenefit(final UserInfo userInfo,
        final int imageIdx) {
        final List<UserReferralCode> writeByMyRecommendCodeList = userRecommendCodeSeparate.findAllJoinUserInfoId(
            userInfo.getIdx());

        int firstBought = 0;
        for (UserReferralCode userRecommendCode : writeByMyRecommendCodeList) {
            if (orderSeparate.isExistOrderHistory(userRecommendCode.getUserIdx())) {
                firstBought++;
            }
        }

        final FriendInvitedGroup friendInvitedGroup = new FriendInvitedGroup(
            userPointSeparate.findAllByUserUuidAndPointIdx(
                userInfo.getUserUuid(), FRIEND_INVITATION));

        final int totalPoint = friendInvitedGroup.totalFriendInvitePoint();

        final PoodImage friendInviteImage = poodImageSeparate.findByIdx(imageIdx);

        return MyReceivedBenefitResponse.of(writeByMyRecommendCodeList.size(), totalPoint,
            firstBought, friendInviteImage.getUrl());
    }

    public void logout(final UserInfo userInfo) {
        userSeparate.logout(userInfo);
        loginLogSeparate.saveLogOut(userInfo);
    }

    public UserLoginResponseDto login(final UserLoginRequest userLoginRequest) {
        final UserInfo userInfo = userSeparate.loginValidation(userLoginRequest.getEmail(),
            userLoginRequest.getPassword());
        loginLogSeparate.saveLog(userInfo, userLoginRequest.getOsType().getSendType());

        final UserToken userToken = userTokenSeparate.saveToken(userInfo.getUserUuid());
        logUserTokenSeparate.saveUserTokenLog(userToken);

        return UserLoginResponseDto.toResponse(userInfo, userToken,
            userLoginRequest.getLoginType());
    }

    public void findPassword(final String userEmail, final String phoneNumber,
        final String decodedUserName) {
        final UserInfo user = userSeparate.findByExactlyUser(userEmail, phoneNumber,
            decodedUserName);
        SmsGenerator.findPasswordSmsSend(serverUrl, user.getUserPhone());
    }

    public void userPostingBlock(final EventBlockRequest eventBlockRequest,
        final UserInfo userInfo) {
        final UserInfo blockUser = userSeparate.findByIdx(eventBlockRequest.getUserInfoIdx());
        userPostingBlockSeparate.save(eventBlockRequest.toEntity(blockUser, userInfo));
    }

    public void checkAuthentication(final String phoneNumber, final String authNumber) {
        userSmsAuthSeparate.findAuthenticationRecord(phoneNumber, authNumber);
    }

    public UserInfo getUserByUUID(final String userUuid) {
        return userSeparate.getUserByUUID(userUuid);
    }

    public void changePassword(final UserPasswordChangeRequest request, final UserInfo userInfo) {
        userSeparate.changePassword(request, userInfo);
    }

    public void updateUserInfo(final UserInfoUpdateRequest request) {
        userSeparate.updateUserInfo(request);
    }

    public UserInfoSignUpResponse emailSignUp(final UserSignUpRequest request) throws Exception {

        if (userSeparate.existByEmail(request.getEmail())) {
            throw new AlreadyExistException("이미 가입된 이메일입니다.");
        }

        if (userSeparate.existsByUserPhone(request.getPhoneNumber())) {
            throw new AlreadyExistException("이미 가입된 휴대폰 번호입니다.");
        }

        request.insertPassword();

        final UserBaseImage defaultImage = userBaseImageSeparate.findDefaultRandomImage();

        final UserInfo saveUser = userSeparate.save(request, defaultImage);

        if (!StringUtils.isEmpty(request.getRecommendCode())) {
            final Integer receivedUserIdx = userSeparate.findByRecommendUser(
                    request.getRecommendCode())
                .getIdx();

            userRecommendCodeSeparate.save(receivedUserIdx, saveUser.getReferralCode(),
                saveUser.getIdx());
        }

        if (Objects.nonNull(request.getSnsKey())) {
            logUserJoinSeparate.save(request, saveUser);
        }

        logUserMarketingPolicySeparate.savePolicyAll(request, saveUser);

        final List<Coupon> welcomeCouponList = couponSeparate.findWelcomeCouponList();

        userCouponSeparate.publishWelcomeCoupon(welcomeCouponList, saveUser);
        return new UserInfoSignUpResponse(saveUser.getUserEmail(), request.getLoginType(),
            saveUser.getUserUuid());
    }

    public void userWithdrawal(final UserInfo userInfo) {
        userSeparate.withdrawalUser(userInfo);
    }

    public List<UserBaseImagesResponse> getUserBaseImgList() {
        return userBaseImageSeparate.findDefaultImageList().stream()
            .map(baseImage -> new UserBaseImagesResponse(baseImage.getIdx(), baseImage.getUrl()))
            .collect(Collectors.toList());
    }

    public void saveUserBaseImg(final UserBaseImageUpdateRequest request, final UserInfo userInfo) {
        UserBaseImage userBaseImageOrg = userInfo.getUserBaseImage();
        userInfo.setUserBaseImage(userBaseImageSeparate.findByIdAndBaseIsTrue(request.getIdx()));
        userSeparate.save(userInfo);
        if (!userBaseImageOrg.isBasic()) {
            basicImageDelete(userBaseImageOrg);
        }
    }

    public UserBaseImagesResponse modifyUserImages(final MultipartFile file,
        final UserInfo userInfo)
        throws IOException {
        UserBaseImage userBaseImageOrg = userInfo.getUserBaseImage();
        userInfo.setUserBaseImage(
            userBaseImageSeparate.create(storageService.multiPartFileStore(file, "pood_image"))
        );
        userSeparate.save(userInfo);
        if (!userBaseImageOrg.isBasic()) {
            basicImageDelete(userBaseImageOrg);
        }
        return new UserBaseImagesResponse(userInfo.getUserBaseImageIdx(),
            userInfo.getUserBaseImageUrl());
    }

    private void basicImageDelete(final UserBaseImage userBaseImageOrg) {
        userBaseImageSeparate.delete(userBaseImageOrg);
        storageService.deleteObject(userBaseImageOrg.getUrl());
    }

    public FindUserEmailResponse findUserEmail(final FindUserEmailRequest request) {
        final UserInfo findUser = userSeparate.findByUserNameAndPhoneNumber(
            request.getPhoneNumber(),
            request.getUserName());
        return FindUserEmailResponse.of(
            findUser.getUserEmail(), findUser.getUserUuid()
        );
    }

    public void addRequstGoods(final RequstGoodsRequest request, final UserInfo userInfo) {
         final Goods goodsInfo = goodsSeparate.findById(request.getGoodsIdx());
        userGoodsRequstSeparate.save(userInfo, goodsInfo.getIdx());
    }

    public String requestReStock(final Integer goodsIdx, final UserInfo userInfo) {
        final Goods goods = goodsSeparate.findById(goodsIdx);
        return userRequestRestockSeparate.insertRequestRestock(goods.getIdx(), userInfo.getUserUuid());
    }

    public void cancelDormantAccount(final DormantCancelRequest request) {

        final String userUuid = logUserJoinSeparate.findBySnsKey(request.getSnsKey());
        final UserInfo userInfo = userSeparate.changeActive(request.getEmail(), userUuid);

        loginLogSeparate.insert(userInfo.getIdx(), userInfo.getUserUuid(), userInfo.getDeviceType());

    }

    public void doDormant(final UserInfo userInfo) {

        userInfo.doDormant();
        userSeparate.save(userInfo);
        loginLogSeparate.doDormant(userInfo.getUserUuid());
    }
}
