package com.pood.server.facade.user.pet.response;

import java.util.List;
import lombok.Value;

@Value
public class WorryManagerResponse {

    String dailyLife;
    String diet;
    List<String> solution;
    
}
