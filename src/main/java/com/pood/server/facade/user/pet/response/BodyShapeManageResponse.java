package com.pood.server.facade.user.pet.response;

import com.pood.server.entity.user.BodyShape;
import com.pood.server.util.date.DateUtil;
import java.time.LocalDate;
import java.util.Objects;
import lombok.Value;

@Value
public class BodyShapeManageResponse {

    float weight;
    String bodyType;
    int weightRecordAgoWeek;
    int bodyShapeRecordAgoWeek;

    private BodyShapeManageResponse(final float weight, final String bodyType,
        final int weightRecordAgoWeek, final int bodyShapeRecordAgoWeek) {
        this.weight = weight;
        this.bodyType = bodyType;
        this.weightRecordAgoWeek = weightRecordAgoWeek;
        this.bodyShapeRecordAgoWeek = bodyShapeRecordAgoWeek;
    }

    public static BodyShapeManageResponse emptyInfo() {
        return new BodyShapeManageResponse(0, "", 0, 0);
    }

    public static BodyShapeManageResponse of(final Float weight, final LocalDate weightBaseDate,
        final BodyShape bodyShape, final LocalDate bodyShapeBaseDate) {

        return new BodyShapeManageResponse(
            nullSafeWeight(weight),
            nullSafeBody(bodyShape),
            DateUtil.of(weightBaseDate).checkWeekAgoFromToday(),
            DateUtil.of(bodyShapeBaseDate).checkWeekAgoFromToday()
        );
    }

    private static String nullSafeBody(final BodyShape bodyShape) {
        if (Objects.nonNull(bodyShape)) {
            return bodyShape.getType();
        }
        return "";
    }

    private static float nullSafeWeight(final Float weight) {
        if (Objects.nonNull(weight)) {
            return weight;
        }
        return 0f;
    }
}
