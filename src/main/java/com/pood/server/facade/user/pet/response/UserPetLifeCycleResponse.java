package com.pood.server.facade.user.pet.response;

import java.util.List;
import lombok.Value;

@Value
public class UserPetLifeCycleResponse {

    String sleep;
    String teeth;
    String feces;
    String diet;
    String bath;
    String hairCareAndBath;
    String weightAndRegularCheck;
    String vaccine;
    String neutering;
    List<String> solution;


}
