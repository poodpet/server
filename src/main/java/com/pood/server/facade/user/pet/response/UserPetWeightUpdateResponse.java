package com.pood.server.facade.user.pet.response;

import lombok.Value;

@Value
public class UserPetWeightUpdateResponse {

    Long idx;

    Float weight;

}
