package com.pood.server.facade.user.pet.request;

import com.pood.server.entity.user.BodyShape;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.Value;

@Value
public class BodyShapeModifyRequest {

    @ApiModelProperty(value = "유저 펫 idx")
    @NotNull(message = "펫 idx 가 null 일 수 없습니다.")
    Integer userPetIdx;

    @ApiModelProperty(value = "펫 체형")
    @NotNull(message = "펫의 체형을 입력해주세요")
    BodyShape bodyShape;

    public BodyShapeModifyRequest(final Integer userPetIdx, final String type) {

        this.userPetIdx = userPetIdx;
        this.bodyShape = BodyShape.findType(type);
    }
}
