package com.pood.server.facade.user.info;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class UserPasswordChangeRequest {

    @NotNull(message = "현재 비밀번호는 null 일 수 없습니다.")
    private String currentPassword;

    @NotNull(message = "바꿀 비밀번호는 null 일 수 없습니다.")
    private String newPassword;

}
