package com.pood.server.facade.user.pet.request;

import com.pood.server.entity.user.BodyShape;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.Value;

@Value
public class BodyShapeManageCreateRequest {

    @ApiModelProperty(value = "유저 펫 idx")
    @NotNull(message = "펫 idx 가 null 일 수 없습니다.")
    Integer userPetIdx;

    @ApiModelProperty(value = "펫 체중")
    @Positive(message = "무게는 0보다 커야 합니다.")
    @NotNull(message = "펫의 체중을 입력해주세요")
    Float weight;

    @ApiModelProperty(value = "펫 체형")
    @NotNull(message = "펫의 체형을 입력해주세요")
    BodyShape bodyShape;

    public BodyShapeManageCreateRequest(final Integer userPetIdx, final Float weight,
        final String type) {

        this.userPetIdx = userPetIdx;
        this.weight = weight;
        this.bodyShape = BodyShape.findType(type);
    }
}
