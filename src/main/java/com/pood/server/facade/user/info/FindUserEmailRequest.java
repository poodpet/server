package com.pood.server.facade.user.info;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class FindUserEmailRequest {

    @NotNull(message = "이름이 null일 수 없습니다.")
    private String userName;

    @NotNull(message = "전화번호가 null일 수 없습니다.")
    private String phoneNumber;
}
