package com.pood.server.facade.user.pet.request;

import java.time.LocalDate;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.Value;

@Value
public class WeightUpdateRequest {

    Long idx;

    @NotNull(message = "유자의 펫이 null 일 수 없습니다.")
    Integer userPetIdx;

    @NotNull(message = "기준일이 null 일 수 없습니다.")
    LocalDate date;

    int week;

    @NotNull(message = "무게가 null 일 수 없습니다.")
    @Positive(message = "무게는 0보다 커야 합니다.")
    Float weight;

}
