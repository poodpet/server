package com.pood.server.facade.user.pet.request;

import javax.validation.constraints.NotNull;
import lombok.Value;

@Value
public class WeightDeleteRequest {

    @NotNull(message = "삭제할 몸무게의 id 값이 null 일 수 없습니다.")
    Long idx;

    @NotNull(message = "유자의 펫이 null 일 수 없습니다.")
    Integer userPetIdx;

}
