package com.pood.server.facade.user.info;

import lombok.Value;

@Value
public class UserInfoSignUpResponse {

    String userEmail;
    Integer userLoginType;
    String userUuid;

}
