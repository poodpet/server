package com.pood.server.facade.user.info.request;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class RequstGoodsRequest {

    @NotNull(message = "상품 idx는 필수 값 입니다.")
    Integer goodsIdx;

}
