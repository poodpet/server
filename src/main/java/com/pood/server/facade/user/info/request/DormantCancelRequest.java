package com.pood.server.facade.user.info.request;

import javax.validation.constraints.NotNull;
import lombok.Value;

@Value
public class DormantCancelRequest {

    @NotNull String email;
    String snsKey;
}
