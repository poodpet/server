package com.pood.server.facade.user.solution.response;

import com.pood.server.entity.meta.dto.ProductGoodsBaseDataDto;
import lombok.Value;

@Value
public class BodyShapeRecommendGoodsResponse {

    Integer idx;
    Integer pcIdx;
    String displayType;
    String goodsName;
    Integer goodsOriginPrice;
    Integer goodsPrice;
    Integer discountRate;
    Integer discountPrice;
    Integer saleStatus;
    Integer mainProduct;
    Double averageRating;
    String mainImage;
    Boolean isRecommend;
    String brandName;
    boolean isNewest;
    boolean isLike;


    public static BodyShapeRecommendGoodsResponse toResponse(final ProductGoodsBaseDataDto goodsDto,
        final boolean isLike) {

        return new BodyShapeRecommendGoodsResponse(
            goodsDto.getGoodsIdx(),
            goodsDto.getPcIdx(),
            goodsDto.getDisplayType(),
            goodsDto.getGoodsName(),
            goodsDto.getGoodsOriginPrice(),
            goodsDto.getGoodsPrice(),
            goodsDto.getDiscountRate(),
            goodsDto.getDiscountPrice(),
            goodsDto.getSaleStatus(),
            goodsDto.getMainProduct(),
            goodsDto.getAverageRating(),
            goodsDto.getMainImage(),
            goodsDto.getIsRecommend(),
            goodsDto.getBrandName(),
            goodsDto.isNewest(),
            isLike
        );
    }
}
