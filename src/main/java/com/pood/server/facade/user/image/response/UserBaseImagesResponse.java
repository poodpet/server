package com.pood.server.facade.user.image.response;

import lombok.Value;

@Value
public class UserBaseImagesResponse {
    Long idx;
    String url;
}
