package com.pood.server.facade.user.image.request;

import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class UserBaseImageUpdateRequest {

    @NotNull(message = "사용자의 기본 id 값이 null 일 수 없습니다.")
    private Integer idx;

}
