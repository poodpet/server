package com.pood.server.facade.user.pet.response;

import lombok.Value;

@Value
public class BodyShapeManageCreateResponse {

    boolean conflictPetWorry;
    boolean selectHealthConcerns;
}
