package com.pood.server.facade.user.info;

import lombok.Value;

@Value(staticConstructor = "of")
public class FindUserEmailResponse {

    String email;
    String userUuid;
}