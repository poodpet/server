package com.pood.server.facade.user.pet.response;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserPetWeightResponse {

    private Long idx;

    private LocalDate date;

    private int week;

    private Float weight;

}
