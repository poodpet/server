package com.pood.server.facade.event;

import static com.pood.server.exception.ErrorMessage.EVENT_HAVE_STARTED;
import static com.pood.server.exception.ErrorMessage.EVENT_PAUSE;

import com.pood.server.controller.response.event.DonationEventListResponse;
import com.pood.server.controller.response.event.photoaward.AwardParticipateResponse;
import com.pood.server.controller.response.event.photoaward.EndPhotoAwardDetailResponse;
import com.pood.server.controller.response.event.photoaward.EndPhotoAwardResponse;
import com.pood.server.controller.response.event.photoaward.EndPhotoAwardWinner;
import com.pood.server.dto.meta.event.EndEvent;
import com.pood.server.dto.meta.event.EndPhotoAward;
import com.pood.server.dto.meta.event.EndPhotoImgUrl;
import com.pood.server.dto.meta.event.EventDto;
import com.pood.server.dto.meta.event.EventDto.EventDetail;
import com.pood.server.dto.meta.event.RunningPhotoAward;
import com.pood.server.entity.meta.EventImage;
import com.pood.server.entity.meta.EventInfo;
import com.pood.server.entity.meta.mapper.event.EventDonationMapper;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.exception.ClosedEventException;
import com.pood.server.exception.PauseEventException;
import com.pood.server.facade.Facade;
import com.pood.server.facade.event.response.EventDonationResponse;
import com.pood.server.service.EventDonationSeparate;
import com.pood.server.service.EventImageSeparate;
import com.pood.server.service.EventSeparate;
import com.pood.server.service.EventTypeJoinPossible;
import com.pood.server.service.UserSeparate;
import com.pood.server.service.factory.EventTypeFactory;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.util.Strings;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Facade
@RequiredArgsConstructor
public class EventFacade {

    private final EventSeparate eventSeparate;
    private final EventImageSeparate eventImageSeparate;
    private final UserSeparate userSeparate;
    private final EventTypeFactory eventTypeFactory;
    private final EventDonationSeparate eventDonationSeparate;

    public EventDetail getEventDetail(final int idx, final String token) {
        EventDetail eventDetail = eventSeparate.getEventDetail(idx);
        eventValidCheck(eventDetail);
        eventDetail.setEventImageList(eventImageSeparate.getEventImagesByEventIdx(idx));
        eventDetail.setParticipationPossible(isParticipationPossible(token, eventDetail));

        return eventDetail;
    }


    private void eventValidCheck(EventDetail eventInfo) {
        if (eventInfo.isWaiting()) {
            throw new ClosedEventException(EVENT_HAVE_STARTED.getMessage());
        }

        if (eventInfo.isPause()) {
            throw new PauseEventException(EVENT_PAUSE.getMessage());
        }
    }

    private boolean isParticipationPossible(final String token,
        final EventDto.EventDetail eventDetail) {
        if (Strings.isBlank(token)) {
            return true;
        }
        UserInfo userInfo = userSeparate.getUserByToken(token);
        if (Objects.isNull(userInfo)) {
            return true;
        }

        EventTypeJoinPossible eventTypeJoinPossibleMap = eventTypeFactory.getEventTypeJoinPossibleMap(
            eventDetail.getTypeName());

        return eventTypeJoinPossibleMap.isParticipationPossible(
            eventDetail, userInfo.getIdx());

    }

    public Page<EndEvent> findEndEvent(final Pageable pageable, final int pcIdx) {
        return eventSeparate.findEndEvent(pageable, pcIdx);
    }

    public List<EndPhotoAwardWinner> findEndPhotoAwardWinners(final int year) {
        final List<EndPhotoAward> endPhotoAwardWinners = eventSeparate.findEndPhotoAwardWinners(
            year, userSeparate.getUserIdxListByResignUser());
        return endPhotoAwardWinners.stream()
            .map(endPhotoAward -> new EndPhotoAwardWinner(endPhotoAward.getIdx(),
                endPhotoAward.getImgUrl()))
            .collect(Collectors.toList());
    }

    public List<EndPhotoAwardResponse> findEndPhotoAwards(final int year) {
        final List<EndPhotoAward> endPhotoAwards = eventSeparate.findEndPhotoAwards(year);

        return endPhotoAwards.stream()
            .map(endPhotoAward -> new EndPhotoAwardResponse(endPhotoAward.getIdx(),
                endPhotoAward.getImgUrl()))
            .collect(Collectors.toList());
    }

    public EndPhotoAwardDetailResponse endPhotoAwardsDetail(final int eventIdx) {
        int count = eventSeparate.countAwards(eventIdx);

        final EventInfo endEvent = eventSeparate.findEndPhotoAward(eventIdx);
        final EventImage mainImage = eventImageSeparate.findMainImage(endEvent.getIdx());

        final String intro = endEvent.getIntro();
        final String startDate = endEvent.convertStartDate();
        final String endDate = endEvent.convertEndDate();

        final EndPhotoImgUrl winnerImage = eventSeparate.findPhotoAwardWinner(eventIdx,
            userSeparate.getUserIdxListByResignUser());

        return new EndPhotoAwardDetailResponse(count, intro, startDate, endDate, mainImage.getUrl(),
            winnerImage);
    }

    public AwardParticipateResponse awardParticipateList(final int eventIdx,
        final Pageable pageable) {

        return new AwardParticipateResponse(
            eventSeparate.findPhotoAwardAllImage(eventIdx,
                userSeparate.getUserIdxListByResignUser(), pageable));
    }

    public RunningPhotoAward runningPhotoAward() {
        return eventSeparate.runningPhotoAward();
    }

    public EventDonationResponse getDonationEventDetail(final Integer idx) {

        final EventDonationMapper eventDonationMapper = eventSeparate.findDonationDetail(idx);
        final List<EventImage> eventImages = eventImageSeparate.getEventImagesByEventIdx(
            eventDonationMapper.getIdx());
        final long donationParticipation = eventDonationSeparate.getTotalParticipation(
            eventDonationMapper.getIdx()
        );

        return EventDonationResponse.of(eventDonationMapper, eventImages, donationParticipation);
    }

    public void participateDonationEvent(final Integer eventIdx, final UserInfo userInfo) {

        EventInfo eventInfo = eventSeparate.findByIdx(eventIdx);

        eventDonationSeparate.participateDonation(userInfo.getUserUuid(), eventInfo.getIdx(),
            eventInfo.getPcId());

    }

    public List<DonationEventListResponse> getDonationList() {

        return eventDonationSeparate.getThumbnailAll()
            .stream()
            .map(DonationEventListResponse::of)
            .collect(Collectors.toList());
    }
}
