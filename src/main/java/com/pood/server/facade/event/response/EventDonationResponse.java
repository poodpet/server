package com.pood.server.facade.event.response;

import com.pood.server.entity.meta.EventImage;
import com.pood.server.entity.meta.mapper.event.EventDonationMapper;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.Value;

@Value
@RequiredArgsConstructor
public class EventDonationResponse {

    private static final int donationDefaultGram = 10;

    @ApiModelProperty(value = "이벤트 레코드 번호")
    long idx;

    @ApiModelProperty(value = "타이틀")
    String title;

    @ApiModelProperty(value = "서브 타이틀")
    String intro;

    @ApiModelProperty(value = "이벤트 시작 일자")
    String startDate;

    @ApiModelProperty(value = "이벤트 종료 일자")
    String endDate;

    @ApiModelProperty(value = "이벤트 썸네일 (공유이미지)")
    String headImage;

    @ApiModelProperty(value = "이벤트 상세 사진")
    List<EventDetailImageResponse> detailImage;

    @ApiModelProperty(value = "이벤트 상태 : 1(정상), 3(일시정지)")
    int status ;

    @ApiModelProperty(value = "펫 카테고리 0 : 공통, 1: 강아지, 2:고양이")
    int pcIdx ;

    @ApiModelProperty(value = "기부결과입니다 - g 단위")
    long donationResult ;

    public static EventDonationResponse of(final EventDonationMapper mapper,
        final List<EventImage> eventImages, final long donationResult) {

        return new EventDonationResponse(
            mapper.getIdx(),
            mapper.getTitle(),
            mapper.getIntro(),
            mapper.getStartDate(),
            mapper.getEndDate(),
            getMainImg(eventImages),
            getDetailImg(eventImages),
            mapper.getStatus(),
            mapper.getPcIdx(),
            donationResult * donationDefaultGram
        );
    }

    private static String getMainImg(final List<EventImage> eventImages) {

        return eventImages.stream().filter(EventImage::isMainImg).findFirst()
            .map(EventImage::getUrl).orElse("");
    }

    private static List<EventDetailImageResponse> getDetailImg(final List<EventImage> eventImages) {

        return eventImages.stream().filter(EventImage::isDetailImg)
            .map(EventDetailImageResponse::of)
            .collect(Collectors.toList());
    }
}
