package com.pood.server.facade.event.response;

import com.pood.server.entity.meta.EventImage;
import io.swagger.annotations.ApiModelProperty;
import lombok.Value;

@Value
public class EventDetailImageResponse {

    @ApiModelProperty(value = "이미지 url")
    String url;

    @ApiModelProperty(value = "사진 우선순위")
    Integer priority;

    public static EventDetailImageResponse of(final EventImage eventImage) {
        return new EventDetailImageResponse(
            eventImage.getUrl(),
            eventImage.getPriority()
        );
    }
}
