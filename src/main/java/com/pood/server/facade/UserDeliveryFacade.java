package com.pood.server.facade;

import com.pood.server.dto.user.delivery.UserDeliveryAddressResponse;
import com.pood.server.dto.user.delivery.UserDeliveryCreateRequest;
import com.pood.server.dto.user.delivery.UserDeliveryUpdateRequest;
import com.pood.server.entity.user.UserDeliveryAddress;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.service.DeliveryCourierSeparate;
import com.pood.server.service.UserDeliverySeparate;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;

@Facade
@RequiredArgsConstructor
public class UserDeliveryFacade {

    private final UserDeliverySeparate userDeliverySeparate;
    private final DeliveryCourierSeparate deliveryCourierSeparate;

    public void saveAddress(final UserDeliveryCreateRequest userDeliveryCreateRequest,
        final UserInfo userInfo) {
        final int fitRemoteType = deliveryCourierSeparate.findFitRemoteType(userDeliveryCreateRequest.getZipcode());
        userDeliverySeparate.save(userDeliveryCreateRequest, userInfo, fitRemoteType);
    }

    public List<UserDeliveryAddressResponse> findAllAddressOfUser(final UserInfo userInfo) {
        final List<UserDeliveryAddress> userDeliveryAddressList = userDeliverySeparate.findAllAddressOfUser(
            userInfo);
        return userDeliveryAddressList.stream()
            .map(UserDeliveryAddressResponse::toResponse)
            .collect(Collectors.toList());
    }

    public void updateAddress(final UserDeliveryUpdateRequest userDeliveryUpdateRequest,
        final UserInfo userInfo) {

        final int fitRemoteType = deliveryCourierSeparate.findFitRemoteType(userDeliveryUpdateRequest.getZipcode());
        userDeliverySeparate.updateDeliveryAddress(userDeliveryUpdateRequest, userInfo, fitRemoteType);
    }

    public void deleteAddress(final int deliveryIdx,
        final UserInfo userInfo) {
        userDeliverySeparate.deleteByIdx(deliveryIdx);
        userDeliverySeparate.onlyOneAddressThenChangeDefault(userInfo);
    }
}
