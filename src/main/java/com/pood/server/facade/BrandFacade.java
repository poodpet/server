package com.pood.server.facade;

import com.pood.server.controller.response.brand.BrandInfoResponse;
import com.pood.server.entity.meta.Brand;
import com.pood.server.entity.meta.BrandImage;
import com.pood.server.service.BrandImagesSeparate;
import com.pood.server.service.BrandSeparate;
import java.util.List;
import lombok.RequiredArgsConstructor;

@Facade
@RequiredArgsConstructor
public class BrandFacade {

    private final BrandSeparate brandSeparate;
    private final BrandImagesSeparate brandImagesSeparate;

    public BrandInfoResponse getProductBrandInfo(final int brandIdx) {

        Brand brand = brandSeparate.getBrandInfo(brandIdx);
        List<BrandImage> brandImages = brandImagesSeparate.getBrandImages(brandIdx);
        return new BrandInfoResponse(brand, brandImages);

    }

}
