package com.pood.server.facade.solution.response;

import com.pood.server.service.pet_solution.group.UserPetGrainSizeAiSolutionGroup.GrainSizeStr;
import java.util.List;
import java.util.Objects;
import lombok.Value;

@Value
public class FeedPreferenceResponse {

    Integer goodsIdx;
    String feedFormulation;
    Boolean isFit;
    List<GrainSizeStr> grainSizeStrList;

    public static FeedPreferenceResponse of(final Integer goodsIdx, final String feedFormulation,
        final List<GrainSizeStr> grainSizeStrList) {

        return new FeedPreferenceResponse(goodsIdx, feedFormulation, isFit(grainSizeStrList), grainSizeStrList);
    }

    private static boolean isFit(final List<GrainSizeStr> grainSizeStrList) {
        return grainSizeStrList.stream().map(GrainSizeStr::getFitString).anyMatch(Objects::nonNull);
    }
}
