package com.pood.server.facade.solution.response;

import com.pood.server.service.pet_solution.group.UserPetAllergyAiSolutionGroup.AllergyStr;
import java.util.List;
import lombok.Value;

@Value
public class FeedAllergyResponse {

    Integer goodsIdx;
    List<AllergyStr> allergyStrList;

    public static FeedAllergyResponse of(final Integer goodsIdx,
        final  List<AllergyStr> allergyStrList) {

        return new FeedAllergyResponse(goodsIdx, allergyStrList);
    }
}
