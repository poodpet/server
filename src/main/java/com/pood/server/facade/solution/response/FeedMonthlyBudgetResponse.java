package com.pood.server.facade.solution.response;

import lombok.Value;

@Value
public class FeedMonthlyBudgetResponse {

    private static final Integer NOT_TREATED = 4;

    Integer goodsIdx;
    int monthlyBudget;
    boolean isPoodGoods;

    public static FeedMonthlyBudgetResponse of(final Integer goodsIdx, final int monthlyBudget,
        final Integer saleStatus) {

        return new FeedMonthlyBudgetResponse(goodsIdx, monthlyBudget, isPoodGoods(saleStatus));
    }

    private static boolean isPoodGoods(final Integer status) {
        return !NOT_TREATED.equals(status);
    }
}
