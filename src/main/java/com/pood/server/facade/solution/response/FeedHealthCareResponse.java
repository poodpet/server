package com.pood.server.facade.solution.response;

import com.pood.server.service.pet_solution.group.UserPetAiDiagnosisAiSolutionGroup.UserPetAiDiagnosisStr;
import java.util.List;
import lombok.Value;

@Value
public class FeedHealthCareResponse {

    Integer goodsIdx;
    int score;
    List<UserPetAiDiagnosisStr> userPetAiDiagnosisStrList;

    public static FeedHealthCareResponse of(final Integer goodsIdx, final int score,
        final List<UserPetAiDiagnosisStr> userPetAiDiagnosisStrList) {

        return new FeedHealthCareResponse(goodsIdx, score, userPetAiDiagnosisStrList);
    }
}
