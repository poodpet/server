package com.pood.server.facade.solution.response;

import com.pood.server.dto.meta.pet.PetProductWord;
import lombok.Value;

@Value
public class FeedBasicInformationResponse {

    Integer goodsIdx;
    int score;
    PetProductWord petProductWord;

    public static FeedBasicInformationResponse of(final Integer goodsIdx, final int score,
        final PetProductWord petProductWord) {

        return new FeedBasicInformationResponse(goodsIdx, score, petProductWord);
    }
}
