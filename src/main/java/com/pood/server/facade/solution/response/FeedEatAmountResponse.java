package com.pood.server.facade.solution.response;

import lombok.Value;

@Value
public class FeedEatAmountResponse {

    Integer goodsIdx;
    double dailyAmount;
    String cupType;
    String cupSubType;
    int gram;
    int calorie;

    public static FeedEatAmountResponse of(final Integer goodsIdx, final double dailyAmount,
        final String cupType, final String cupSubType, final int gram, final int calorie) {

        return new FeedEatAmountResponse(goodsIdx, dailyAmount, cupType, cupSubType, gram, calorie);
    }

}
