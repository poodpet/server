package com.pood.server.facade.solution;

import com.pood.server.controller.response.solution.SolutionResponse;
import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.dto.meta.goods.PromotionDto.PromotionGoodsProductInfo;
import com.pood.server.dto.meta.goods.SortedGoodsGroup;
import com.pood.server.dto.meta.product.ProductBaseDataGroup;
import com.pood.server.dto.order.OrderRankGroup;
import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.entity.meta.AllergyData;
import com.pood.server.entity.meta.Goods;
import com.pood.server.entity.meta.GrainSize;
import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.meta.PetWorry;
import com.pood.server.entity.meta.dto.ProductAndWorryFilterDto;
import com.pood.server.entity.meta.dto.user_pet_life_cycle.UserPetLifeCycle;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.UserPetWorryManager;
import com.pood.server.entity.meta.group.ProductGoodsBaseDataDtoGroup;
import com.pood.server.entity.meta.mapper.AiRecommendDiagnosisMapper;
import com.pood.server.entity.meta.mapper.solution.ProductGoodsBaseDataMapper;
import com.pood.server.entity.user.BodyShape;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserPet;
import com.pood.server.entity.user.UserPetAiDiagnosis;
import com.pood.server.entity.user.UserPetAllergy;
import com.pood.server.entity.user.UserPetBodyShapeDiary;
import com.pood.server.entity.user.UserPetGrainSize;
import com.pood.server.exception.NotFoundException;
import com.pood.server.facade.Facade;
import com.pood.server.facade.solution.request.FeedMonthlyBudgetRequest;
import com.pood.server.facade.solution.response.FeedAllergyResponse;
import com.pood.server.facade.solution.response.FeedBasicInformationResponse;
import com.pood.server.facade.solution.response.FeedEatAmountResponse;
import com.pood.server.facade.solution.response.FeedHealthCareResponse;
import com.pood.server.facade.solution.response.FeedMonthlyBudgetResponse;
import com.pood.server.facade.solution.response.FeedNutrientResponse;
import com.pood.server.facade.solution.response.FeedPreferenceResponse;
import com.pood.server.facade.user.pet.response.UserPetLifeCycleResponse;
import com.pood.server.facade.user.pet.response.WorryManagerResponse;
import com.pood.server.facade.user.solution.response.BodyShapeRecommendGoodsResponse;
import com.pood.server.service.AafcoNrcSeparate;
import com.pood.server.service.AllergyDataSeparate;
import com.pood.server.service.GoodsSeparate;
import com.pood.server.service.GrainSizeSeparate;
import com.pood.server.service.OrderSeparate;
import com.pood.server.service.PetSeparate;
import com.pood.server.service.PetWorrySeparate;
import com.pood.server.service.ProductSeparate;
import com.pood.server.service.PromotionSeparate;
import com.pood.server.service.UserPetAiDiagnosisSeparate;
import com.pood.server.service.UserPetAllergySeparate;
import com.pood.server.service.UserPetBodyShapeSeparate;
import com.pood.server.service.UserPetGrainSizeSeparate;
import com.pood.server.service.UserPetSeparate;
import com.pood.server.service.UserWishSeparate;
import com.pood.server.service.factory.UserPetBodyWorryFilterFactory;
import com.pood.server.service.factory.UserPetLifeCycleFactory;
import com.pood.server.service.factory.UserPetWorryManagerFactory;
import com.pood.server.service.goods.GoodsProductSeparate;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.ProductType;
import com.pood.server.service.pet_solution.SolutionCalculateResponseGroup;
import com.pood.server.service.pet_solution.SolutionScore;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.service.pet_solution.UserPetCalorieCalc;
import com.pood.server.service.pet_solution.calc.AllergyExtract;
import com.pood.server.service.pet_solution.calc.DoctorPersonal;
import com.pood.server.service.pet_solution.calc.FeedCompareProduct;
import com.pood.server.service.pet_solution.calc.Nutrition;
import com.pood.server.service.pet_solution.calc.PetProduct;
import com.pood.server.service.pet_solution.calc.Suitable;
import com.pood.server.service.pet_solution.calc.Worry;
import com.pood.server.service.pet_solution.group.SuitableGroup;
import com.pood.server.service.pet_solution.group.UserPetAiDiagnosisAiSolutionGroup;
import com.pood.server.service.pet_solution.group.UserPetAllergyAiSolutionGroup;
import com.pood.server.service.pet_solution.group.UserPetGrainSizeAiSolutionGroup;
import com.pood.server.service.pet_solution.intake.ProductStore;
import com.pood.server.service.pet_solution.intake.ProductStoreFactory;
import com.pood.server.service.pet_solution.intake.UserPetIntake;
import com.pood.server.service.pet_solution.intake.UserPetIntakeFactory;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModel;
import com.pood.server.service.pet_solution.nutrition.NutritionFactory;
import com.pood.server.util.SaleStatus;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;

@Facade
@RequiredArgsConstructor
public class SolutionFacade {

    private static final int DEFAULT_SIZE = 8;
    private static final int FEED_SNACK_DEFAULT_SIZE = 12;
    private static final int BODY_MANAGEMENT_RECOMMEND_GOODS_SIZE = 4;
    private final UserPetSeparate userPetSeparate;
    private final ProductSeparate productSeparate;
    private final AafcoNrcSeparate aafcoNrcSeparate;
    private final UserPetGrainSizeSeparate userPetGrainSeparate;
    private final UserPetAllergySeparate userPetAllergySeparate;
    private final UserPetAiDiagnosisSeparate userPetAiDiagnosisSeparate;
    private final GrainSizeSeparate grainSizeSeparate;
    private final AllergyDataSeparate allergyDataSeparate;
    private final PetSeparate petSeparate;
    private final List<UserPetCalorieCalc> userPetCalcs;
    private final GoodsSeparate goodsSeparate;
    private final PromotionSeparate promotionSeparate;
    private final PetWorrySeparate petWorrySeparate;
    private final OrderSeparate orderSeparate;
    private final UserPetBodyShapeSeparate userPetBodyShapeSeparate;
    private final UserWishSeparate userWishSeparate;
    private final GoodsProductSeparate goodsProductSeparate;

    public SolutionResponse getGoodsSolution(final UserInfo userInfo, final int userPetIdx,
        final int goodsIdx, final int productIdx) {
        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());

        final UserPetBaseData userPetBaseData = getPetSolutionBaseData(userPetIdx);

        final AafcoNrc aafcoNrc = aafcoNrcSeparate.getAafcoNrc(userPetBaseData.getPcIdx(),
            userPetBaseData.getPetGrowthStateType());

        final ProductBaseData productBaseData = productSeparate.getSolutionProductDetail(
            goodsIdx, productIdx);

        final SolutionCalculateResponseGroup group = new SolutionCalculateResponseGroup(
            userPetBaseData, productBaseData, aafcoNrc);

        final UserPetIntakeFactory userPetIntakeFactory = new UserPetIntakeFactory(userPetBaseData,
            productBaseData);

        final ProductStoreFactory productStoreFactory = new ProductStoreFactory(productBaseData,
            userPetIntakeFactory.make());

        final List<Integer> allergyIdxList = userPetBaseData.getAllergyIdxList();
        final List<Integer> grainSizeIdxList = userPetBaseData.getGrainSizeIdxList();

        final List<AllergyData> allergyDataList = allergyDataSeparate.findAllByIdxIn(
            allergyIdxList);
        final List<GrainSize> grainSizeList = grainSizeSeparate.findAllIdxIn(grainSizeIdxList);

        final ProductStore productStore = productStoreFactory.make();
        return group.toResponse(productStore, allergyDataList, grainSizeList);
    }

    public List<SortedGoodsList> getBestGoodsList(final UserInfo userInfo, final int userPetIdx) {

        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());
        UserPetBaseData userPetData = getPetSolutionBaseData(userPetIdx);
        AafcoNrc aafcoNrc = aafcoNrcSeparate.getAafcoNrc(userPetData.getPcIdx(),
            userPetData.getPetGrowthStateType());
        List<ProductBaseData> productInfoList = new ProductBaseDataGroup(
            productSeparate.getAllProductsOnStockAndPoodMarketNotIn(userPetData.getPcIdx()))
            .getAllergyFilter(userPetData.getAllergyNameList());
        List<SortedGoodsList> resultList = getPackageRecommendGoodsList(userPetData, aafcoNrc,
            productInfoList);

        setPromotionData(resultList);
        return resultList;
    }

    private List<SortedGoodsList> getPackageRecommendGoodsList(final UserPetBaseData userPetData,
        final AafcoNrc aafcoNrc, final List<ProductBaseData> productInfoList) {
        List<SortedGoodsList> resultList = new ArrayList<>();
        SolutionScore solutionScore = getSolutionScore(userPetData, aafcoNrc, productInfoList);
        for (Pair<Integer, Double> productScore : solutionScore.getTopPackageProduct()) {
            SortedGoodsList recommendGoodsList = goodsSeparate.getRecommendGoodsListByIsSale(
                productScore.getFirst(), userPetData.getPcIdx());
            recommendGoodsList.setScore(productScore.getSecond().intValue());
            resultList.add(recommendGoodsList);
        }
        if (resultList.size() <= 1) {
            return new ArrayList<>();
        }
        return resultList;
    }

    private SolutionScore getSolutionScore(final UserPetBaseData userPetData,
        final AafcoNrc aafcoNrc, final List<ProductBaseData> productInfoList) {
        List<Map<ProductType, Pair<Integer, Double>>> list = new ArrayList<>();

        for (ProductBaseData productData : productInfoList) {
            SuitableGroup aiSolutionPoint = getAiSolutionPointGroup(
                userPetData, aafcoNrc, productData);
            list.add(Map.of(productData.getProductType(),
                Pair.of(productData.getIdx(), aiSolutionPoint.getTotalScore())));
        }

        return new SolutionScore(list);
    }

    private SuitableGroup getAiSolutionPointGroup(final UserPetBaseData userPetBaseData,
        final AafcoNrc aafcoNrc, final ProductBaseData productBaseData) {
        final Suitable petInfoCompatibility = new PetProduct(userPetBaseData,
            productBaseData);
        final Suitable worryCompatibility = new Worry(userPetBaseData, productBaseData);
        final Suitable doctorPersonalSuitable = new DoctorPersonal(productBaseData);
        NutritionFactory nutritionFactory = new NutritionFactory(userPetBaseData, productBaseData,
            aafcoNrc);

        if (productBaseData.eqProductType(ProductType.FEED)) {
            final List<NutritionDetailModel> nutritionModel = nutritionFactory.createNutritionModel();
            final Suitable nutritionSuitable = new Nutrition(productBaseData,
                nutritionModel);
            return new SuitableGroup(
                Arrays.asList(petInfoCompatibility, worryCompatibility, doctorPersonalSuitable,
                    nutritionSuitable));
        }
        return new SuitableGroup(
            Arrays.asList(petInfoCompatibility, worryCompatibility, doctorPersonalSuitable));
    }

    public List<SortedGoodsList> getWorryGoodsList(final UserInfo userInfo, final int worryIdx,
        final int userPetIdx) {

        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());
        UserPetBaseData userPetData = getPetSolutionBaseData(userPetIdx);
        AafcoNrc aafcoNrc = aafcoNrcSeparate.getAafcoNrc(userPetData.getPcIdx(),
            userPetData.getPetGrowthStateType());

        List<ProductBaseData> productInfoList = new ProductBaseDataGroup(
            productSeparate.getProductInfoListByWorryIdx(
                userPetData.getPcIdx(), worryIdx))
            .getAllergyFilter(userPetData.getAllergyNameList());

        List<SortedGoodsList> resultList = getPackageRecommendGoodsList(userPetData, aafcoNrc,
            productInfoList);

        setPromotionData(resultList);
        return resultList;
    }

    public Page<SortedGoodsList> getUserFrequentlyBuyGoodsList(final int buyCount,
        final int userPetIdx, final UserInfo userInfo, final Pageable pageable) {

        //valied check
        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());
        //user Pet Info
        UserPetBaseData userPetBaseData = getPetSolutionBaseData(userPetIdx);

        OrderRankGroup orderRankGroup = orderSeparate.getUserBuyGoodsListByBuyCount(buyCount,
            userInfo.getIdx(), goodsSeparate.getGoodsIdxByPcIdx(userPetBaseData.getPcIdx()),
            pageable);

        List<Integer> goodsIdxList = orderRankGroup.getGoodsIdsList();
        Page<SortedGoodsList> sortedGoodsListPage = orderRankGroup.convertFrequentlyGoods(
            goodsSeparate.getSortedGoodsInIdxList(goodsIdxList));

        final List<PromotionGoodsProductInfo> promotionGoodsInfo = promotionSeparate.findByGoodsIdxAndNowActiveList(
            goodsIdxList);

        for (PromotionGoodsProductInfo promotionGoods : promotionGoodsInfo) {
            for (SortedGoodsList goodsInfo : sortedGoodsListPage) {
                if (goodsInfo.getIdx().equals(promotionGoods.getGoodsIdx())) {
                    goodsInfo.promotionIfExistUpdatePrice(promotionGoods);
                    goodsInfo.setScore((int)
                        getMainProductScore(userPetBaseData, goodsInfo.getMainProduct(),
                            goodsInfo.getIdx()));
                }
            }
        }

        return sortedGoodsListPage;
    }

    public double getMainProductScore(final UserPetBaseData userPetBaseData,
        final int productIdx, final int goodsIdx) {

        AafcoNrc aafcoNrc = aafcoNrcSeparate.getAafcoNrc(userPetBaseData.getPcIdx(),
            userPetBaseData.getPetGrowthStateType());

        //Goods Product Info list
        ProductBaseData productBaseData = productSeparate.getSolutionProductDetail(goodsIdx,
            productIdx);

        final Suitable petInfoCompatibility = new PetProduct(userPetBaseData,
            productBaseData);
        final Suitable worryCompatibility = new Worry(userPetBaseData, productBaseData);
        final Suitable doctorPersonalSuitable = new DoctorPersonal(productBaseData);

        if (productBaseData.eqProductType(ProductType.FEED)) {
            NutritionFactory nutritionFactory = new NutritionFactory(userPetBaseData,
                productBaseData, aafcoNrc);

            final List<NutritionDetailModel> nutritionModel = nutritionFactory.createNutritionModel();

            final Suitable nutritionSuitable = new Nutrition(productBaseData,
                nutritionModel);

            final SuitableGroup aiSolutionPointGroup = new SuitableGroup(
                Arrays.asList(petInfoCompatibility, worryCompatibility, doctorPersonalSuitable,
                    nutritionSuitable));
            return aiSolutionPointGroup.getTotalScore();
        }

        return new SuitableGroup(
            Arrays.asList(petInfoCompatibility, worryCompatibility,
                doctorPersonalSuitable)).getTotalScore();
    }

    private void setPromotionData(final List<SortedGoodsList> resultList) {
        List<PromotionGoodsProductInfo> promotionList = promotionSeparate.findByGoodsIdxAndNowActiveList(
            getSortedGoodsIdxList(resultList));
        promotionList.forEach(promotionInfo -> resultList.forEach(goodsInfo -> {
            if (goodsInfo.getIdx().equals(promotionInfo.getGoodsIdx())) {
                goodsInfo.setPromotionInfo(promotionInfo);
                goodsInfo.setDiscountPrice(promotionInfo.getPrDiscountPrice());
                goodsInfo.setDiscountRate(promotionInfo.getPrDiscountRate());
                goodsInfo.setGoodsPrice(promotionInfo.getPrPrice());
            }
        }));
    }

    private UserPetBaseData getPetSolutionBaseData(final Integer userPetIdx) {
        final UserPet userPet = userPetSeparate.findByIdx(userPetIdx);

        final List<UserPetGrainSize> userPetGrainSizeList = userPetGrainSeparate.getUserPetGrainSizeList(
            userPetIdx);

        final List<UserPetAllergy> userPetAllergyList = userPetAllergySeparate.getUserPetAllergyList(
            userPetIdx);

        final List<UserPetAiDiagnosis> userPetAiDiagnosisList = userPetAiDiagnosisSeparate.getUserPetAiDiagnosisList(
            userPetIdx);

        UserPetAiDiagnosisAiSolutionGroup userPetAiDiagnosisAiSolutionGroup = new UserPetAiDiagnosisAiSolutionGroup(
            userPetAiDiagnosisList);

        for (Long worryIdx : userPetAiDiagnosisAiSolutionGroup.getWorryIdxOrderByPriority()) {
            userPetAiDiagnosisAiSolutionGroup = userPetAiDiagnosisAiSolutionGroup.setAiRecommendDiagnosis(
                worryIdx, petWorrySeparate.findDiagnosisByWorryIdx(worryIdx));
        }

        final UserPetAllergyAiSolutionGroup userPetAllergyAiSolutionGroup = new UserPetAllergyAiSolutionGroup(
            userPetAllergyList);

        final UserPetGrainSizeAiSolutionGroup userPetGrainSizeAiSolutionGroup = new UserPetGrainSizeAiSolutionGroup(
            userPetGrainSizeList);

        final Pet petInfo = petSeparate.getPetInfo(userPet.getPscId());

        final List<PetWorry> worryList = petWorrySeparate.findWorryByPetCategoryIdx(
            userPet.getPcId());

        UserPetCalorieCalc userPetCalc = userPetCalcs.stream()
            .filter(x -> x.support(userPet.getPcId()))
            .findFirst().orElseThrow(() -> new NotFoundException("해당 펫 타입을 찾을수 없습니다."));

        return new UserPetBaseData(userPet, userPetGrainSizeAiSolutionGroup,
            userPetAllergyAiSolutionGroup, userPetAiDiagnosisAiSolutionGroup, petInfo, userPetCalc,
            worryList);
    }


    public List<SortedGoodsList> getBestGoodsListBySnack(final UserInfo userInfo,
        final int userPetIdx) {

        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());
        UserPetBaseData userPetData = getPetSolutionBaseData(userPetIdx);
        AafcoNrc aafcoNrc = aafcoNrcSeparate.getAafcoNrc(userPetData.getPcIdx(),
            userPetData.getPetGrowthStateType());

        List<ProductBaseData> allProductInfoList = new ProductBaseDataGroup(
            productSeparate.getProductInfoListByPcIdxAndProductType(userPetData.getPcIdx(),
                ProductType.SNACK))
            .getAllergyFilter(userPetData.getAllergyNameList());

        List<SortedGoodsList> resultList = getRecommendGoodsList(userPetData, aafcoNrc,
            allProductInfoList, FEED_SNACK_DEFAULT_SIZE);

        setPromotionData(resultList);
        return resultList;
    }

    public List<SortedGoodsList> getBestGoodsListByBestWorryScoreFilter(final UserInfo userInfo,
        final int userPetIdx, final long worryIdx, final ProductType productType) {

        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());
        UserPetBaseData userPetData = getPetSolutionBaseData(userPetIdx);
        AafcoNrc aafcoNrc = aafcoNrcSeparate.getAafcoNrc(userPetData.getPcIdx(),
            userPetData.getPetGrowthStateType());
        final Integer ardCode = petWorrySeparate.findDiagnosisByWorryIdx(
            worryIdx).getArdGroupCode();

        final List<ProductBaseData> allProductInfoList = new ProductBaseDataGroup(
            productSeparate.getProductInfoListByBestWorryScoreFilter(
                userPetData.getPcIdx(), worryIdx, userPetData.getPetFeedType()))
            .getBestScoreByArdCode(ardCode)
            .getProductTypeAndAllergyFilter(productType, userPetData.getAllergyNameList());

        final List<SortedGoodsList> resultList = getRecommendGoodsList(userPetData, aafcoNrc,
            allProductInfoList, DEFAULT_SIZE);
        setPromotionData(resultList);
        return resultList;
    }

    public List<SortedGoodsList> getBestGoodsListByProductTypeAndWorry(final UserInfo userInfo,
        final int userPetIdx, final int productType, final long worryIdx) {

        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());
        UserPetBaseData userPetData = getPetSolutionBaseData(userPetIdx);
        AafcoNrc aafcoNrc = aafcoNrcSeparate.getAafcoNrc(userPetData.getPcIdx(),
            userPetData.getPetGrowthStateType());

        List<ProductBaseData> allProductInfoList = new ProductBaseDataGroup(
            productSeparate.getProductInfoListByWorryIdx(userPetData.getPcIdx(), worryIdx))
            .getProductTypeAndAllergyFilter(ProductType.getProductTypeByIdx(productType),
                userPetData.getAllergyNameList());

        List<SortedGoodsList> resultList = getRecommendGoodsList(userPetData, aafcoNrc,
            allProductInfoList, DEFAULT_SIZE);
        setPromotionData(resultList);
        return resultList;
    }


    public List<SortedGoodsList> getBestSuppliesGoodsListByPhysical(final UserInfo userInfo,
        final int userPetIdx) {

        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());
        UserPetBaseData userPetData = getPetSolutionBaseData(userPetIdx);
        AafcoNrc aafcoNrc = aafcoNrcSeparate.getAafcoNrc(userPetData.getPcIdx(),
            userPetData.getPetGrowthStateType());

        List<ProductBaseData> allProductInfoList = new ProductBaseDataGroup(
            productSeparate.getProductInfoListByFeedTarget(userPetData.getPcIdx(),
                userPetData.getUserPetFeedTargetNum()))
            .getProductTypeFilter(ProductType.SUPPLIES).getProductBaseDataList();

        List<SortedGoodsList> resultList = getRecommendGoodsList(userPetData, aafcoNrc,
            allProductInfoList, DEFAULT_SIZE);
        setPromotionData(resultList);
        return resultList;
    }

    public List<SortedGoodsList> getBestSuppliesGoodsListByAge(final UserInfo userInfo,
        final int userPetIdx) {
        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());
        UserPetBaseData userPetData = getPetSolutionBaseData(userPetIdx);
        AafcoNrc aafcoNrc = aafcoNrcSeparate.getAafcoNrc(userPetData.getPcIdx(),
            userPetData.getPetGrowthStateType());

        List<ProductBaseData> allProductInfoList = new ProductBaseDataGroup(
            productSeparate.getProductInfoListByFeedType(userPetData.getPcIdx(),
                userPetData.getPetFeedType()))
            .getProductTypeFilter(ProductType.SUPPLIES).getProductBaseDataList();

        List<SortedGoodsList> resultList = getRecommendGoodsList(userPetData, aafcoNrc,
            allProductInfoList, DEFAULT_SIZE);
        setPromotionData(resultList);
        return resultList;
    }

    public List<SortedGoodsList> getBestFeedGoodsListByLifeCycle(final int userPetIdx,
        final UserInfo userInfo, final int size) {

        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());
        UserPetBaseData userPetData = getPetSolutionBaseData(userPetIdx);
        AafcoNrc aafcoNrc = aafcoNrcSeparate.getAafcoNrc(userPetData.getPcIdx(),
            userPetData.getPetGrowthStateType());

        List<ProductBaseData> productInfoList = new ProductBaseDataGroup(
            productSeparate.getProductInfoListByFeedTypeAndFoodMartNotIn(
                userPetData.getPcIdx(), userPetData.getPetFeedType()
            )
        ).getProductTypeAndAllergyFilter(ProductType.FEED, userPetData.getAllergyNameList());

        Collections.shuffle(productInfoList);
        ProductBaseDataGroup productBaseDataGroup = new ProductBaseDataGroup(productInfoList)
            .getLimitBySize(size);

        SortedGoodsGroup sortedGoodsList = new SortedGoodsGroup(
            getCreateSortedGoodsGroup(productBaseDataGroup.getProductIdxList(),
                userPetData.getPcIdx()));
        List<PromotionGoodsProductInfo> promotionList = promotionSeparate.findByGoodsIdxAndNowActiveList(
            sortedGoodsList.getIdxList());

        return sortedGoodsList.setPromotionData(promotionList).getSortedGoodsList();
    }

    private List<SortedGoodsList> getRecommendGoodsList(final UserPetBaseData userPetData,
        final AafcoNrc aafcoNrc, final List<ProductBaseData> productInfoList, final int size) {

        List<SortedGoodsList> resultList = new ArrayList<>();
        for (Pair<Integer, Double> productScore : getSolutionScore(userPetData, aafcoNrc,
            productInfoList).getListOrderByScoreDesc()) {
            SortedGoodsList recommendGoodsList = goodsSeparate.getRcommendGoodsListByProductIdxAndGoodsIdxListNotIn(
                userPetData.getPcIdx(),
                productScore.getFirst(), getSortedGoodsIdxList(resultList));
            if (Objects.isNull(recommendGoodsList)) {
                continue;
            }
            recommendGoodsList.setScore(productScore.getSecond().intValue());
            resultList.add(recommendGoodsList);
            if (resultList.size() >= size) {
                break;
            }
        }
        return resultList;
    }


    public UserPetLifeCycleResponse getUserPetLifeCycleInfo(final UserInfo userInfo,
        final int userPetIdx) {

        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());
        UserPetBaseData userPetData = getPetSolutionBaseData(userPetIdx);

        UserPetLifeCycle userPetLifeCycle = new UserPetLifeCycleFactory(userPetData).create();

        return new UserPetLifeCycleResponse(userPetLifeCycle.getSleep(),
            userPetLifeCycle.getTeeth(),
            userPetLifeCycle.getFeces(),
            userPetLifeCycle.getDiet(),
            userPetLifeCycle.getBath(),
            userPetLifeCycle.getHairCareAndBath(),
            userPetLifeCycle.getWeightAndRegularCheck(),
            userPetLifeCycle.getVaccine(),
            userPetLifeCycle.getNeutering(),
            userPetLifeCycle.getSolution()
        );
    }


    public WorryManagerResponse getUserPetWorryManagerInfo(final UserInfo userInfo,
        final long worryIdx, final int userPetIdx) {

        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());
        UserPetBaseData userPetData = getPetSolutionBaseData(userPetIdx);

        AiRecommendDiagnosisMapper diagnosisInfoByWorry = petWorrySeparate.findDiagnosisByWorryIdx(
            worryIdx);

        UserPetWorryManager userPetWorryManager = new UserPetWorryManagerFactory(userPetData,
            diagnosisInfoByWorry.getArdGroupCode()).create();

        return new WorryManagerResponse(userPetWorryManager.getDailyLife(),
            userPetWorryManager.getDiet(userPetData.getPetName()),
            userPetWorryManager.getSolution());
    }

    private List<Integer> getSortedGoodsIdxList(final List<SortedGoodsList> resultList) {
        return resultList.stream().map(SortedGoodsList::getIdx).collect(Collectors.toList());
    }

    private List<SortedGoodsList> getCreateSortedGoodsGroup(
        final List<Integer> productBaseDataGroupIdxList, final int pcIdx) {
        List<SortedGoodsList> sortedGoodsList = new ArrayList<>();
        for (Integer idx : productBaseDataGroupIdxList) {
            sortedGoodsList.add(goodsSeparate.getRecommendGoodsList(idx, pcIdx));
        }
        return sortedGoodsList;
    }

    public List<SortedGoodsList> getTodaySnackGoodsList(final int userPetIdx,
        final UserInfo userInfo, final int size) {

        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());
        UserPetBaseData userPetData = getPetSolutionBaseData(userPetIdx);
        AafcoNrc aafcoNrc = aafcoNrcSeparate.getAafcoNrc(userPetData.getPcIdx(),
            userPetData.getPetGrowthStateType());

        ProductBaseDataGroup productBaseDataGroup = new ProductBaseDataGroup(
            productSeparate.getProductInfoListByPcIdxAndProductTypeAndFoodMartNotIn(
                userPetData.getPcIdx(), ProductType.SNACK
            )
        ).getAllergyFilterAndShuffle(userPetData.getAllergyNameList(), size);

        SortedGoodsGroup sortedGoodsList = new SortedGoodsGroup(
            getCreateSortedGoodsGroup(productBaseDataGroup.getProductIdxList(),
                userPetData.getPcIdx()));
        List<PromotionGoodsProductInfo> promotionList = promotionSeparate.findByGoodsIdxAndNowActiveList(
            sortedGoodsList.getIdxList());

        return sortedGoodsList.setPromotionData(promotionList).getSortedGoodsList();

    }

    public List<SortedGoodsList> getBestNutrientsGoodsList(final int userPetIdx,
        final UserInfo userInfo, final int size) {
        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());
        UserPetBaseData userPetData = getPetSolutionBaseData(userPetIdx);
        AafcoNrc aafcoNrc = aafcoNrcSeparate.getAafcoNrc(userPetData.getPcIdx(),
            userPetData.getPetGrowthStateType());

        ProductBaseDataGroup productBaseDataGroup = new ProductBaseDataGroup(
            productSeparate.getProductInfoListByWorryIdxInAndFoodMartNotIn(
                userPetData.getPcIdx(), userPetData.getPetWorryListIdxOrderByPriority()
            )
        ).getProductTypeFilter(ProductType.NUTRIENTS)
            .getAllergyFilterAndShuffle(userPetData.getAllergyNameList(), size);

        SortedGoodsGroup sortedGoodsList = new SortedGoodsGroup(
            getCreateSortedGoodsGroup(productBaseDataGroup.getProductIdxList(),
                userPetData.getPcIdx()));
        List<PromotionGoodsProductInfo> promotionList = promotionSeparate.findByGoodsIdxAndNowActiveList(
            sortedGoodsList.getIdxList());

        return sortedGoodsList.setPromotionData(promotionList).getSortedGoodsList();

    }

    public List<SortedGoodsList> getBestSuppliesGoodsList(final int userPetIdx,
        final UserInfo userInfo, final int size) {
        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());
        UserPetBaseData userPetData = getPetSolutionBaseData(userPetIdx);
        AafcoNrc aafcoNrc = aafcoNrcSeparate.getAafcoNrc(userPetData.getPcIdx(),
            userPetData.getPetGrowthStateType());

        ProductBaseDataGroup productBaseDataGroup = getTodayGoodsListBySupplies(userPetData, size);

        SortedGoodsGroup sortedGoodsList = new SortedGoodsGroup(
            getCreateSortedGoodsGroup(productBaseDataGroup.getProductIdxList(),
                userPetData.getPcIdx()));
        List<PromotionGoodsProductInfo> promotionList = promotionSeparate.findByGoodsIdxAndNowActiveList(
            sortedGoodsList.getIdxList());

        return sortedGoodsList.setPromotionData(promotionList).getSortedGoodsList();
    }

    private ProductBaseDataGroup getTodayGoodsListBySupplies(final UserPetBaseData userPetData,
        final int size) {
        ProductBaseDataGroup productBaseDataGroup = null;
        if (userPetData.isPetTypeDog()) {
            productBaseDataGroup = new ProductBaseDataGroup(
                productSeparate.getProductInfoListByFeedTargetAndProductTypeAndPoodMarketNotIn(
                    userPetData.getPcIdx(), userPetData.getUserPetFeedTargetNum(),
                    ProductType.SUPPLIES)
            );
        }
        productBaseDataGroup = new ProductBaseDataGroup(
            productSeparate.getProductInfoListByFeedTypeAndProductType(
                userPetData.getPcIdx(), userPetData.getPetFeedType(), ProductType.SUPPLIES
            )
        );

        return productBaseDataGroup.getAllergyFilterAndShuffle(
            userPetData.getAllergyNameList(), size);
    }


    public List<SortedGoodsList> getBestGoodsListByFeed(final UserInfo userInfo,
        final int userPetIdx) {

        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());
        UserPetBaseData userPetData = getPetSolutionBaseData(userPetIdx);
        AafcoNrc aafcoNrc = aafcoNrcSeparate.getAafcoNrc(userPetData.getPcIdx(),
            userPetData.getPetGrowthStateType());

        List<ProductBaseData> allProductInfoList = new ProductBaseDataGroup(
            productSeparate.getProductInfoListByFeedTypeAndProductType(userPetData.getPcIdx(),
                userPetData.getPetFeedType(), ProductType.FEED))
            .getAllergyFilter(userPetData.getAllergyNameList());

        List<SortedGoodsList> resultList = getRecommendGoodsList(userPetData, aafcoNrc,
            allProductInfoList, FEED_SNACK_DEFAULT_SIZE);

        setPromotionData(resultList);
        return resultList;
    }

    public List<SortedGoodsList> getNonLoginBestGoodsList(final int pcIdx, final long worryIdx) {
        final List<ProductBaseData> productInfoListByWorry = productSeparate.getProductInfoListByWorryIdxInAndFoodMartNotIn(
            pcIdx, List.of(worryIdx));

        List<SortedGoodsList> resultList = getGoodsListByWorryIdxAndProductType(
            productInfoListByWorry, 4,
            ProductType.FEED);
        resultList.addAll(
            getGoodsListByWorryIdxAndProductType(productInfoListByWorry, 2, ProductType.SNACK));
        resultList.addAll(
            getGoodsListByWorryIdxAndProductType(productInfoListByWorry, 2, ProductType.NUTRIENTS));
        resultList.addAll(
            getGoodsListByWorryIdxAndProductType(productInfoListByWorry, 2, ProductType.SUPPLIES));

        setPromotionData(resultList);
        return resultList;
    }

    private ArrayList<SortedGoodsList> getGoodsListByWorryIdxAndProductType(
        final List<ProductBaseData> productInfoListByWorry,
        final int size, final ProductType feed) {
        return new ArrayList<>(
            new SortedGoodsGroup(goodsSeparate.getSortedGoodsByProductIdxsOrderReviewCount(
                new ProductBaseDataGroup(productInfoListByWorry)
                    .getProductTypeFilter(feed)
                    .getProductIdxList()))
                .limitSize(size)
                .getSortedGoodsList());
    }

    public List<BodyShapeRecommendGoodsResponse> findSolutionGoodsByBodyShape(
        final UserInfo userInfo, final int userPetIdx) {

        final UserPetBaseData userPetData = getPetSolutionBaseData(userPetIdx);
        userPetData.petOwnerIsCorrectValid(userInfo.getIdx());

        final UserPetBodyShapeDiary bodyShapeDiary = userPetBodyShapeSeparate.findRecentInfo(
            userPetData.getUserPetIdx());

        final ProductAndWorryFilterDto productFilterDto = new UserPetBodyWorryFilterFactory(
            bodyShapeDiary.getBodyShape()).create();

        final Integer ardCode = petWorrySeparate.findDiagnosisByWorryIdx(
            productFilterDto.getWorryIdx()).getArdGroupCode();

        final ProductGoodsBaseDataDtoGroup sortedGoodsListByWorryScoreGroup = ProductGoodsBaseDataDtoGroup.ofMapper(
                getProductInfoListByBestWorryScoreAndProductTypeFilter(
                    userPetData, productFilterDto, ardCode, List.of()))
            .sortedByBodyShape(bodyShapeDiary.getBodyShape())
            .filterAllergyAndSize(userPetData.getAllergyNameList(),
                BODY_MANAGEMENT_RECOMMEND_GOODS_SIZE);

        final ProductGoodsBaseDataDtoGroup fillTheGoodsListByWorryScoreGroup = fillBodyShapeRecommendGoods(
            sortedGoodsListByWorryScoreGroup, ardCode, userPetData, bodyShapeDiary.getBodyShape(),
            sortedGoodsListByWorryScoreGroup.getGoodsIdxList());

        final List<Integer> userWishGoodsIdxList = userWishSeparate.findUserWishList(
            userInfo.getIdx(), fillTheGoodsListByWorryScoreGroup.getGoodsIdxList());

        final List<PromotionGoodsProductInfo> promotionInfoList = promotionSeparate.findByGoodsIdxAndNowActiveList(
            fillTheGoodsListByWorryScoreGroup.getGoodsIdxList());

        return fillTheGoodsListByWorryScoreGroup
            .updatePromotionPrice(promotionInfoList)
            .toResponseForBodyManagement(userWishGoodsIdxList);
    }

    private List<ProductGoodsBaseDataMapper> getProductInfoListByBestWorryScoreAndProductTypeFilter(
        final UserPetBaseData userPetData, final ProductAndWorryFilterDto productFilterDto,
        final Integer ardCode, final List<Integer> alreadyIncludeGoodsIdxList) {

        return productSeparate.getProductInfoListByBestWorryScoreAndProductTypeFilter(
            userPetData.getPcIdx(), ardCode, userPetData.getPetFeedType(),
            productFilterDto.getGoodsCtIdx(), productFilterDto.getGoodsSubCtIdxList(),
            alreadyIncludeGoodsIdxList);
    }

    private ProductGoodsBaseDataDtoGroup fillBodyShapeRecommendGoods(
        final ProductGoodsBaseDataDtoGroup dtoGroup, final Integer ardCode,
        final UserPetBaseData userPetData, final BodyShape petBody,
        final List<Integer> existGoodsIdxList) {

        if (BodyShape.HIGHLY_OBESE.equals(petBody) && dtoGroup.isLessThan(
            BODY_MANAGEMENT_RECOMMEND_GOODS_SIZE)) {

            final ProductAndWorryFilterDto productFilterDto = new UserPetBodyWorryFilterFactory(
                BodyShape.OBESITY).create();

            final ProductGoodsBaseDataDtoGroup plusGoodsGroup = ProductGoodsBaseDataDtoGroup.ofMapper(
                    getProductInfoListByBestWorryScoreAndProductTypeFilter(userPetData,
                        productFilterDto, ardCode, existGoodsIdxList))
                .filterAllergyAndSize(userPetData.getAllergyNameList(),
                    BODY_MANAGEMENT_RECOMMEND_GOODS_SIZE - dtoGroup.getSize()
                );

            return dtoGroup.add(plusGoodsGroup.getDtoList());
        }

        return dtoGroup;
    }

    public List<FeedEatAmountResponse> getAmountOfEat(final UserInfo userInfo,
        final List<Integer> goodsIdxList, final Integer userPetIdx) {

        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());

        final UserPetBaseData userPetBaseData = getPetSolutionBaseData(userPetIdx);

        final List<Goods> goodsList = getGoodsList(goodsIdxList);

        final List<FeedEatAmountResponse> result = new LinkedList<>();

        for (final Goods goods : goodsList) {

            final ProductBaseData productBaseData = productSeparate.getSolutionProductDetail(
                goods.getIdx(), goods.getMainProduct());

            final UserPetIntakeFactory userPetIntakeFactory = new UserPetIntakeFactory(
                userPetBaseData,
                productBaseData);

            final UserPetIntake petIntake = userPetIntakeFactory.make();

            final ProductStoreFactory productStoreFactory = new ProductStoreFactory(productBaseData,
                petIntake);

            final ProductStore productStore = productStoreFactory.make();

            result.add(FeedEatAmountResponse.of(
                    goods.getIdx(),
                    productStore.dailyTakesToEat(),
                    productStore.cupType(),
                    productStore.cupSubType(),
                    Math.round(petIntake.calcEatFoodGrams()),
                    Math.round(petIntake.calcEatFoodGrams() * productBaseData.getCalorie() / 1000f)
                )
            );
        }

        return result;
    }


    public List<FeedMonthlyBudgetResponse> getMonthlyBudget(final UserInfo userInfo,
        final FeedMonthlyBudgetRequest request) {

        userPetSeparate.petOwnerValidation(request.getPetIdx(), userInfo.getIdx());

        final UserPetBaseData userPetBaseData = getPetSolutionBaseData(request.getPetIdx());

        final List<Goods> goodsList = getGoodsList(
            new ArrayList<>(request.getGoodsIdxPrice().keySet()));

        final List<FeedMonthlyBudgetResponse> result = new LinkedList<>();

        for (final Goods goods : goodsList) {
            result.add(makeMonthlyBudget(goods, userPetBaseData,
                request.getGoodsIdxPrice().get(goods.getIdx())));
        }

        return result;
    }

    private List<Goods> getGoodsList(final List<Integer> goodsIdxList) {
        final List<Goods> goodsList = goodsSeparate.getGoodsListByIdxIn(goodsIdxList);

        if (goodsList.size() != goodsIdxList.size()) {
            throw new NotFoundException(
                goodsIdxList.size() + " 개의 딜 정보를 조회했지만 " + goodsList.size() + "개만 조회되었습니다.");
        }
        return goodsList;
    }

    private FeedMonthlyBudgetResponse makeMonthlyBudget(final Goods goods,
        final UserPetBaseData userPetBaseData, final Integer price) {

        if (SaleStatus.NOT_TREATED.getStatus() == goods.getSaleStatus()) {
            return FeedMonthlyBudgetResponse.of(goods.getIdx(), 0, goods.getSaleStatus());
        }

        final ProductBaseData productBaseData = productSeparate.getSolutionProductDetail(
            goods.getIdx(), goods.getMainProduct());

        final int productQuantity = goodsProductSeparate.getProductQuantity(goods.getIdx(),
            goods.getMainProduct());

        final UserPetIntakeFactory userPetIntakeFactory = new UserPetIntakeFactory(userPetBaseData,
            productBaseData);

        final UserPetIntake petIntake = userPetIntakeFactory.make();

        return FeedMonthlyBudgetResponse.of(goods.getIdx(),
            calculateBudget(price, productQuantity, productBaseData.getWeight(),
                (int) petIntake.calcEatFoodGrams()),
            goods.getSaleStatus()
        );
    }

    private int calculateBudget(final int price, final int productQuantity,
        final int weight, final int dailyIntake) {

        return (price * dailyIntake * 30) / (weight * productQuantity);
    }

    public List<FeedBasicInformationResponse> getBasicInformation(final UserInfo userInfo,
        final List<Integer> goodsIdxList, final Integer userPetIdx) {

        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());

        final UserPetBaseData userPetBaseData = getPetSolutionBaseData(userPetIdx);

        final List<Goods> goodsList = getGoodsList(goodsIdxList);

        final List<FeedBasicInformationResponse> result = new LinkedList<>();

        for (final Goods goods : goodsList) {

            final ProductBaseData productBaseData = productSeparate.getSolutionProductDetail(
                goods.getIdx(), goods.getMainProduct());

            final FeedCompareProduct feedCompareProduct = new FeedCompareProduct(userPetBaseData,
                productBaseData);

            result.add(FeedBasicInformationResponse.of(goods.getIdx(),
                (int) feedCompareProduct.getScore(), feedCompareProduct.fitWord())
            );

        }

        return result;
    }

    public List<FeedAllergyResponse> getAllergyInfo(final UserInfo userInfo,
        final List<Integer> goodsIdxList, final Integer userPetIdx) {

        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());

        final UserPetBaseData userPetBaseData = getPetSolutionBaseData(userPetIdx);

        final List<Integer> allergyIdxList = userPetBaseData.getAllergyIdxList();

        final List<AllergyData> allergyDataList = allergyDataSeparate.findAllByIdxIn(
            allergyIdxList);

        final List<Goods> goodsList = getGoodsList(goodsIdxList);

        final List<FeedAllergyResponse> result = new LinkedList<>();

        for (final Goods goods : goodsList) {

            final ProductBaseData productBaseData = productSeparate.getSolutionProductDetail(
                goods.getIdx(), goods.getMainProduct());

            result.add(FeedAllergyResponse.of(goods.getIdx(),
                new AllergyExtract(userPetBaseData, productBaseData, allergyDataList).toWordList())
            );
        }

        return result;
    }

    public List<FeedNutrientResponse> getNutrientsInfo(final UserInfo userInfo,
        final List<Integer> goodsIdxList, final Integer userPetIdx) {

        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());

        final UserPetBaseData userPetBaseData = getPetSolutionBaseData(userPetIdx);

        final AafcoNrc aafcoNrc = aafcoNrcSeparate.getAafcoNrc(userPetBaseData.getPcIdx(),
            userPetBaseData.getPetGrowthStateType());

        final List<Goods> goodsList = getGoodsList(goodsIdxList);

        final List<FeedNutrientResponse> result = new LinkedList<>();

        for (final Goods goods : goodsList) {

            final ProductBaseData productBaseData = productSeparate.getSolutionProductDetail(
                goods.getIdx(), goods.getMainProduct());

            result.add(FeedNutrientResponse.of(goods.getIdx(),
                new Nutrition(productBaseData,
                    new NutritionFactory(userPetBaseData, productBaseData,
                        aafcoNrc).createNutritionModel()).toWordList()
                )
            );
        }

        return result;
    }

    public List<FeedPreferenceResponse> getFeedGrainSizeInfo(final UserInfo userInfo,
        final List<Integer> goodsIdxList, final Integer userPetIdx) {

        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());

        final UserPetBaseData userPetBaseData = getPetSolutionBaseData(userPetIdx);

        final List<Integer> grainSizeIdxList = userPetBaseData.getGrainSizeIdxList();

        final List<GrainSize> grainSizeList = grainSizeSeparate.findAllIdxIn(grainSizeIdxList);

        final List<Goods> goodsList = getGoodsList(goodsIdxList);

        final List<FeedPreferenceResponse> result = new LinkedList<>();

        for (final Goods goods : goodsList) {

            final ProductBaseData productBaseData = productSeparate.getSolutionProductDetail(
                goods.getIdx(), goods.getMainProduct());

            result.add(FeedPreferenceResponse.of(goods.getIdx(),
                productBaseData.getFeedFormulation(),
                userPetBaseData.fitEachALLUnitSize(productBaseData.getUnitSize(), grainSizeList)
                )
            );
        }

        return result;
    }

    public List<FeedHealthCareResponse> getHealthCareInfo(final UserInfo userInfo, final List<Integer> goodsIdxList,
        final Integer userPetIdx) {

        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());

        final UserPetBaseData userPetBaseData = getPetSolutionBaseData(userPetIdx);

        final List<Goods> goodsList = getGoodsList(goodsIdxList);

        final List<FeedHealthCareResponse> result = new LinkedList<>();

        for (final Goods goods : goodsList) {

            final ProductBaseData productBaseData = productSeparate.getSolutionProductDetail(
                goods.getIdx(), goods.getMainProduct());

            final Worry worry = new Worry(userPetBaseData, productBaseData);

            result.add(FeedHealthCareResponse.of(goods.getIdx(), (int) worry.getScore(),
                worry.toWordList()));
        }

        return result;
    }
}
