package com.pood.server.facade.solution.response;

import com.pood.server.dto.meta.pet.NutritionWord;
import java.util.List;
import lombok.Value;

@Value
public class FeedNutrientResponse {

    Integer goodsIdx;
    List<NutritionWord> nutritionWords;

    public static FeedNutrientResponse of(final Integer goodsIdx,
        final  List<NutritionWord> nutritionWords) {

        return new FeedNutrientResponse(goodsIdx, nutritionWords);
    }
}
