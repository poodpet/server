package com.pood.server.facade.solution.request;

import java.util.Map;
import javax.validation.constraints.NotNull;
import lombok.Value;

@Value
public class FeedMonthlyBudgetRequest {

    @NotNull
    Integer petIdx;

    @NotNull
    Map<Integer, Integer> goodsIdxPrice;
}
