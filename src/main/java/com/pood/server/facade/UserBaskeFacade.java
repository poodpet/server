package com.pood.server.facade;

import com.pood.server.entity.user.UserInfo;
import com.pood.server.service.GoodsSeparate;
import com.pood.server.service.UserBaskeSeparate;
import lombok.RequiredArgsConstructor;

@Facade
@RequiredArgsConstructor
public class UserBaskeFacade {

    private final UserBaskeSeparate userBaskeSeparate;
    private final GoodsSeparate goodsSeparate;

    public Long getBasketGoodsCount(final UserInfo userInfo) {

        Long basketGoodsCount = userBaskeSeparate.getBasketGoodsCount(userInfo.getUserUuid(),
            goodsSeparate.getStopWaitGoodsIdxList());
        return basketGoodsCount;
    }
}
