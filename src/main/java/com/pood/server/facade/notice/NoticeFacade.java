package com.pood.server.facade.notice;

import com.pood.server.entity.meta.mapper.notice.NoticeDetailMapper;
import com.pood.server.exception.NotFoundException;
import com.pood.server.facade.Facade;
import com.pood.server.facade.notice.response.NoticeDetailResponse;
import com.pood.server.facade.notice.response.NoticePageResponse;
import com.pood.server.service.notice.NoticeSeparate;
import com.pood.server.service.notice.NoticeTypeSeparate;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Facade
@RequiredArgsConstructor
public class NoticeFacade {

    private final NoticeSeparate noticeSeparate;
    private final NoticeTypeSeparate noticeTypeSeparate;

    public Page<NoticePageResponse> getNoticePage(final Pageable pageable) {
        return noticeSeparate.getNoticePage(pageable)
            .map(notice -> new NoticePageResponse(notice.getIdx(),
                notice.getTypeName(), notice.getTitle(), notice.getDate()));
    }

    public NoticeDetailResponse getNoticeDetail(final int idx) {
        final NoticeDetailMapper noticeDetail = noticeSeparate.getNoticeDetail(idx)
            .orElseThrow(() -> new NotFoundException("해당 하는 공지 사항이 존재하지 않습니다."));

        return new NoticeDetailResponse(noticeDetail.getIdx(), noticeDetail.getTitle(),
            noticeDetail.getText(), noticeDetail.getDate());
    }

    public NoticeDetailResponse getNoticePopupInfo() {
        return noticeSeparate.getNoticePopupInfoByTypeOrNull(noticeTypeSeparate.getSystemType())
            .map(noticeDetail -> new NoticeDetailResponse(noticeDetail.getIdx(),
                noticeDetail.getTitle(), noticeDetail.getText(),
                noticeDetail.getRecordbirth().toLocalDate()))
            .orElse(null);
    }
}
