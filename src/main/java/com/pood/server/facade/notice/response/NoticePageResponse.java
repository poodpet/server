package com.pood.server.facade.notice.response;

import java.time.LocalDate;
import lombok.Value;

@Value
public class NoticePageResponse {

    Integer idx;

    String typeName;

    String title;

    LocalDate date;

}
