package com.pood.server.facade.notice.response;

import java.time.LocalDate;
import lombok.Value;

@Value
public class NoticeDetailResponse {

    Integer idx;

    String title;

    String text;

    LocalDate date;

}
