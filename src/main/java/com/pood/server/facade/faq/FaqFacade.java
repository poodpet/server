package com.pood.server.facade.faq;

import com.pood.server.controller.response.faq.FaqCategoryResponse;
import com.pood.server.controller.response.faq.FaqResponse;
import com.pood.server.dto.meta.faq.FaqCategoryResponseGroup;
import com.pood.server.dto.meta.faq.FaqResponseGroup;
import com.pood.server.facade.Facade;
import com.pood.server.service.faq.FaqCategorySeparate;
import com.pood.server.service.faq.FaqSeparate;
import java.util.List;
import lombok.RequiredArgsConstructor;

@Facade
@RequiredArgsConstructor
public class FaqFacade {

    private final FaqCategorySeparate faqCategorySeparate;
    private final FaqSeparate faqSeparate;

    public List<FaqCategoryResponse> getFeqCatagoryList() {

        FaqCategoryResponseGroup faqCategoryResponseGroup = new FaqCategoryResponseGroup(
            faqCategorySeparate.getFaqCategoryList());
        return faqCategoryResponseGroup.getFaqCategoryResponseList();
    }

    public List<FaqResponse> getFaqListByCategoryIdx(final long catagoryIdx) {

        FaqResponseGroup faqResponseGroup = new FaqResponseGroup(
            faqSeparate.getFaqListByCategoryIdx(catagoryIdx));
        return faqResponseGroup.getFaqResponseList();
    }

}
