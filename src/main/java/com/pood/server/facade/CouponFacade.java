package com.pood.server.facade;

import com.pood.server.controller.request.coupon.CouponCreateDto;
import com.pood.server.dto.user.coupon.CouponBrandGroup;
import com.pood.server.dto.user.coupon.CouponGoodsGroup;
import com.pood.server.dto.user.coupon.CouponGroup;
import com.pood.server.dto.user.coupon.CouponResponseDto;
import com.pood.server.dto.user.coupon.CouponResponseDtoGroup;
import com.pood.server.entity.group.FirstRegisterPetCouponGroup;
import com.pood.server.entity.log.LogUserCouponCode;
import com.pood.server.entity.meta.Coupon;
import com.pood.server.entity.meta.CouponCode;
import com.pood.server.entity.user.UserCoupon;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.coupon.response.FirstRegisterPetCouponResponse;
import com.pood.server.service.CouponBrandSeparate;
import com.pood.server.service.CouponCodeSeparate;
import com.pood.server.service.CouponGoodsSeparate;
import com.pood.server.service.CouponSeparate;
import com.pood.server.service.LogUserCouponCodeSeparate;
import com.pood.server.service.UserCouponSeparate;
import com.pood.server.service.UserSeparate;
import com.pood.server.util.UserCouponStatus;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;


@Facade
@RequiredArgsConstructor
public class CouponFacade {

    private final CouponCodeSeparate couponCodeSeparate;
    private final CouponSeparate couponSeparate;
    private final LogUserCouponCodeSeparate logUserCouponCodeSeparate;
    private final UserCouponSeparate userCouponSeparate;
    private final UserSeparate userSeparate;
    private final CouponBrandSeparate couponBrandSeparate;
    private final CouponGoodsSeparate couponGoodsSeparate;

    public void createCoupon(final CouponCreateDto couponCreateDto, final String token) {
        final UserInfo userInfo = userSeparate.getOptionalUserByToken(token);

        final CouponCode couponCode = couponCodeSeparate.findByCode(couponCreateDto.getCode());

        final Coupon coupon = couponSeparate.findByIdx(couponCode.getCouponIdx())
            .isBeforeCurrentTime();

        logUserCouponCodeSeparate.isExistCouponCode(userInfo.getUserUuid(),
            couponCreateDto.getCode());

        userCouponSeparate.save(UserCoupon.builder()
            .couponIdx(coupon.getIdx())
            .userUuid(userInfo.getUserUuid())
            .userIdx(userInfo.getIdx())
            .applyCartIdx(null)
            .availableTime(
                LocalDateTime.now().plusDays(coupon.getAvailableDay()).with(LocalTime.MAX)
                    .minusSeconds(1))
            .status(UserCouponStatus.AVAILABLE)
            .overType(coupon.getOverType())
            .publishTime(LocalDateTime.now())
            .build());

        logUserCouponCodeSeparate.save(LogUserCouponCode.builder()
            .code(couponCreateDto.getCode())
            .userUuid(userInfo.getUserUuid())
            .build());
    }

    public CouponResponseDtoGroup findCoupon(final String token) {
        final UserInfo userInfo = userSeparate.getOptionalUserByToken(token);
        final List<CouponResponseDto> list = new ArrayList<>();
        final List<UserCoupon> userCouponList = userCouponSeparate.findAllByAvailableCouponIdx(
            userInfo.getUserUuid());

        final CouponGroup couponGroup = new CouponGroup(couponSeparate.findAll());
        final CouponGoodsGroup couponGoodsGroup = new CouponGoodsGroup(
            couponGoodsSeparate.findAll());
        final CouponBrandGroup couponBrandGroup = new CouponBrandGroup(
            couponBrandSeparate.findAll());

        for (UserCoupon userCoupon : userCouponList) {
            int couponIdx = userCoupon.getCouponIdx();
            list.add(CouponResponseDto.of(
                userCoupon,
                couponGroup.getByCouponIdx(couponIdx),
                couponBrandGroup.couponBrandIdxList(couponIdx),
                couponGoodsGroup.getCouponGoodsIdxList(couponIdx),
                LocalDate.now().until(userCoupon.getAvailableTime().toLocalDate(), ChronoUnit.DAYS)
            ));
        }

        return new CouponResponseDtoGroup(list);
    }

    public List<FirstRegisterPetCouponResponse> firstRegisterPetCouponList() {
        return new FirstRegisterPetCouponGroup(
            couponSeparate.findFirstRegisteredPetCouponList()).toResponse();
    }
}
