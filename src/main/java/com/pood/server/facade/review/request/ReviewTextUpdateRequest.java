package com.pood.server.facade.review.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Value;

@Value
public class ReviewTextUpdateRequest {

    @NotNull
    Integer idx;

    @NotNull
    @Size(min = 10, message = "리뷰의 글자수는 최소 10글자 이상 적어주셔야 합니다.")
    String text;

}
