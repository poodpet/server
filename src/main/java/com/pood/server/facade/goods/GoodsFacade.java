package com.pood.server.facade.goods;

import com.pood.server.controller.request.goods.GoodsFilterRequest;
import com.pood.server.controller.request.search.LatestGoodsSearchRequest;
import com.pood.server.controller.response.goods.SuggestRandomGoodsResponse;
import com.pood.server.dto.meta.goods.GoodsDto;
import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.dto.meta.goods.PromotionDto;
import com.pood.server.dto.meta.goods.PromotionDto.PromotionGoodsProductInfo;
import com.pood.server.dto.meta.goods.SortedGoodsGroup;
import com.pood.server.dto.meta.goods.SuggestRandomGoodsGroup;
import com.pood.server.dto.meta.product.ProductBaseDataGroup;
import com.pood.server.entity.meta.dto.goods.GoodsProductGroupByGoodsDto;
import com.pood.server.entity.meta.mapper.SuggestRandomGoodsMapper;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserWish;
import com.pood.server.exception.NotFoundException;
import com.pood.server.facade.Facade;
import com.pood.server.facade.goods.response.GoodsIngredientsResponse;
import com.pood.server.service.GoodsSeparate;
import com.pood.server.service.ProductSeparate;
import com.pood.server.service.PromotionSeparate;
import com.pood.server.service.UserSeparate;
import com.pood.server.service.goods.GoodsProductSeparate;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.ProductType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Facade
@RequiredArgsConstructor
public class GoodsFacade {

    private final GoodsSeparate goodsSeparate;
    private final UserSeparate userSeparate;
    private final PromotionSeparate promotionSeparate;
    private final ProductSeparate productSeparate;
    private final GoodsProductSeparate goodsProductSeparate;


    public Page<GoodsDto.SortedGoodsList> getSortedGoodsList(final Pageable pageable,
        final GoodsFilterRequest request) {
        Page<GoodsDto.SortedGoodsList> sortedGoodsList = goodsSeparate.goodsFilteredList(request,
            pageable);
        List<Integer> goodsIdxList = sortedGoodsList.stream().map(GoodsDto.SortedGoodsList::getIdx)
            .collect(Collectors.toList());
        List<PromotionDto.PromotionGoodsProductInfo> promotionList = promotionSeparate.findByGoodsIdxAndNowActiveList(
            goodsIdxList);

        ifDuringPromotionThenApply(promotionList, sortedGoodsList);

        return sortedGoodsList;
    }

    public Page<SortedGoodsList> goodsSearchList(final Pageable pageable, final String searchText,
        final Integer pcIdx, final Long goodsCtIdx) {

        final Page<SortedGoodsList> sortedGoodsList = goodsSeparate.goodsSearchList(
            pageable, searchText, pcIdx, goodsCtIdx);
        return setPromotionPriceForGoodsList(sortedGoodsList);
    }

    public Page<SortedGoodsList> goodsSearchListSoringProductCt(final Pageable pageable,
        final String searchText, final Integer pcIdx) {

        final Page<SortedGoodsList> sortedGoodsList = goodsSeparate.goodsSearchListFilterByProductCt(
            pageable, searchText, pcIdx);
        return setPromotionPriceForGoodsList(sortedGoodsList);
    }

    private Page<SortedGoodsList> setPromotionPriceForGoodsList(Page<SortedGoodsList> sortedGoodsList) {
        List<Integer> goodsIdxList = sortedGoodsList.stream().map(SortedGoodsList::getIdx)
            .collect(Collectors.toList());
        List<PromotionGoodsProductInfo> promotionList = promotionSeparate.findByGoodsIdxAndNowActiveList(
            goodsIdxList);

        ifDuringPromotionThenApply(promotionList, sortedGoodsList);

        return sortedGoodsList;
    }

    private void ifDuringPromotionThenApply(
        final List<PromotionDto.PromotionGoodsProductInfo> promotionList,
        final Page<SortedGoodsList> sortedGoodsList) {
        promotionList.forEach(promotionInfo -> sortedGoodsList.forEach(goods -> {
            if (goods.getIdx().equals(promotionInfo.getGoodsIdx())) {
                goods.setPromotionInfo(promotionInfo);
                goods.setDiscountPrice(promotionInfo.getPrDiscountPrice());
                goods.setDiscountRate(promotionInfo.getPrDiscountRate());
                goods.setGoodsPrice(promotionInfo.getPrPrice());
            }
        }));
    }

    public List<SortedGoodsList> poodHomeNewGoodsList(final int petCategoryIdx) {
        return goodsSeparate.poodHomeNewGoodsList(petCategoryIdx);
    }

    public List<SortedGoodsList> poodHomeSetGoodsList(final int petCategoryIdx) {
        final List<SortedGoodsList> setGoodsList = goodsSeparate.poodHomeSetGoodsList(
            petCategoryIdx);

        final List<Integer> goodsIdxList = setGoodsList.stream().map(SortedGoodsList::getIdx)
            .collect(Collectors.toList());
        final List<PromotionGoodsProductInfo> activeList = promotionSeparate.findByGoodsIdxAndNowActiveList(
            goodsIdxList);

        for (PromotionGoodsProductInfo promotionGoods : activeList) {
            for (SortedGoodsList setGoods : setGoodsList) {
                setGoods.promotionIfExistUpdatePrice(promotionGoods);
            }
        }

        return setGoodsList;
    }

    public List<SortedGoodsList> poodMarketGoodsList(final int petCategoryIdx) {
        return goodsSeparate.poodMarketGoodsList(petCategoryIdx);
    }

    public List<SortedGoodsList> poodMatchGoodsList(final int petCategoryIdx,
        final List<String> searchList) {
        return goodsSeparate.poodMatchGoodsList(petCategoryIdx, searchList);
    }

    public List<SortedGoodsList> getSortedGoodsInIdxList(
        final LatestGoodsSearchRequest latestGoodsSearch) {
        SortedGoodsGroup sortedGoodsGroup = new SortedGoodsGroup(
            goodsSeparate.getSortedGoodsInIdxList(latestGoodsSearch.getGoodsIdxList()));
        List<SortedGoodsList> sortedGoodsList = sortedGoodsGroup.getSortedListByLatestGoods(
            latestGoodsSearch);
        List<PromotionGoodsProductInfo> activePromotionList = promotionSeparate.findByGoodsIdxAndNowActiveList(
            sortedGoodsList.stream()
                .map(SortedGoodsList::getIdx)
                .collect(Collectors.toList()));

        for (PromotionGoodsProductInfo promotionGoods : activePromotionList) {
            for (SortedGoodsList setGoods : sortedGoodsList) {
                if (setGoods.getIdx().equals(promotionGoods.getGoodsIdx())) {
                    setGoods.promotionIfExistUpdatePrice(promotionGoods);
                }
            }
        }

        return sortedGoodsList;
    }

    public List<SuggestRandomGoodsResponse> suggest10Goods() {
        final List<SuggestRandomGoodsMapper> suggestRandomGoodsList = goodsSeparate.suggest10Goods();

        final SuggestRandomGoodsGroup group = new SuggestRandomGoodsGroup(suggestRandomGoodsList);

        final List<PromotionGoodsProductInfo> activePromotionList = promotionSeparate.findByGoodsIdxAndNowActiveList(
            group.goodsIdxList());

        group.isPromotionThenUpdatePrice(activePromotionList);

        return group.toResponse();
    }

    public List<SortedGoodsList> getRandomFeedGoodsList(final int pcIdx,
        final ProductType productType, final int size) {
        if (Objects.isNull(productType)) {
            throw new NotFoundException("상품 타입을 찾을수 없습니다.");
        }
        List<ProductBaseData> allProductInfoList = productSeparate.getAllProductsOnStockAndPoodMarketNotIn(pcIdx);
        Collections.shuffle(allProductInfoList);
        ProductBaseDataGroup productBaseDataGroup = new ProductBaseDataGroup(allProductInfoList);
        SortedGoodsGroup sortedGoodsList = new SortedGoodsGroup(
            getCreateSortedGoodsGroup(
                productBaseDataGroup.getProductTypeFilter(productType)
                    .getLimitBySize(size)
                    .getProductIdxList(),
                pcIdx));
        List<PromotionGoodsProductInfo> promotionList = promotionSeparate.findByGoodsIdxAndNowActiveList(
            sortedGoodsList.getIdxList());

        return sortedGoodsList.setPromotionData(promotionList).getSortedGoodsList();
    }

    private List<SortedGoodsList> getCreateSortedGoodsGroup(
        final List<Integer> productBaseDataGroupIdxList, final int pcIdx) {
        List<SortedGoodsList> sortedGoodsList = new ArrayList<>();
        for (Integer idx : productBaseDataGroupIdxList) {
            sortedGoodsList.add(goodsSeparate.getRecommendGoodsList(idx, pcIdx));
        }
        return sortedGoodsList;
    }

    public List<SortedGoodsList> getMyGoodsWishList(final UserInfo userInfo) {
        final List<UserWish> userWishList = userSeparate.getUserWishList(userInfo.getUserUuid());
        final List<Integer> goodsIdxList = userWishList.stream().map(UserWish::getGoodsIdx)
            .collect(Collectors.toList());

        final List<PromotionDto.PromotionGoodsProductInfo> promotionList = promotionSeparate.findByGoodsIdxAndNowActiveList(
            goodsIdxList);

        SortedGoodsGroup myGoodsWishList = new SortedGoodsGroup(
            goodsSeparate.getSortedGoodsInIdxList(
                goodsIdxList));

        return myGoodsWishList.setPromotionData(promotionList).getSortedGoodsList();
    }

    public Page<SortedGoodsList> findNewGoodsList(final int pcIdx,
        final Pageable pageable) {
        Page<SortedGoodsList> newstGoodsList = goodsSeparate.getNewstGoodsList(pcIdx, pageable);
        final SortedGoodsGroup sortedGoodsList = new SortedGoodsGroup(
            newstGoodsList.toList());
        sortedGoodsList.setPromotionData(promotionSeparate.findByGoodsIdxAndNowActiveList(
            sortedGoodsList.getIdxList()));
        return newstGoodsList;
    }

    public List<GoodsIngredientsResponse> getGoodsIngredientsList(
        final List<Integer> goodsIdxList) {

        final List<GoodsIngredientsResponse> result = new LinkedList<>();

        for (final Integer goodsIdx : goodsIdxList) {
            final GoodsProductGroupByGoodsDto goodsProduct = goodsProductSeparate.getGoodsProductIdxDto(
                goodsIdx);

            result.add(new GoodsIngredientsResponse(goodsProduct.getGoodsIdx(),
                productSeparate.findAllIngredientInIdxList(goodsProduct.getProductIdxList())));

        }
        return result;
    }

}