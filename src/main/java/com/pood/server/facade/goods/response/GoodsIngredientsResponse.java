package com.pood.server.facade.goods.response;

import com.pood.server.entity.meta.mapper.ProductIngredientMapper;
import java.util.List;
import lombok.Value;

@Value
public class GoodsIngredientsResponse {

    Integer goodsIdx;
    List<ProductIngredientMapper> productIngredientList;
}
