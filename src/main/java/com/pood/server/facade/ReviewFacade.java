package com.pood.server.facade;

import com.pood.server.api.file.uploader.StorageService;
import com.pood.server.api.mapper.user.review.ReviewRatingAndCountDto;
import com.pood.server.controller.request.review.ReviewBlockAndSueDto;
import com.pood.server.controller.response.review.GoodsDetailReviewResponse;
import com.pood.server.controller.response.review.GoodsImagesReviewResponse;
import com.pood.server.dto.meta.goods.UserReviewImageDto;
import com.pood.server.dto.meta.user.review.UserReviewGroup;
import com.pood.server.dto.user.review.GoodsDetailReviewDto;
import com.pood.server.dto.user.review.GoodsDetailReviewDtoGroup;
import com.pood.server.dto.user.review.GoodsImagesReviewDto;
import com.pood.server.dto.user.review.GoodsImagesReviewDtoGroup;
import com.pood.server.dto.user.review.UserReviewSaveDto;
import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.order.group.OrderBasketGroup;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserPetImage;
import com.pood.server.entity.user.UserReview;
import com.pood.server.entity.user.UserReviewBlock;
import com.pood.server.entity.user.UserReviewImage;
import com.pood.server.entity.user.UserReviewSue;
import com.pood.server.exception.AlreadyExistException;
import com.pood.server.exception.NotFoundException;
import com.pood.server.facade.review.request.ReviewTextUpdateRequest;
import com.pood.server.service.GoodsSeparate;
import com.pood.server.service.OrderBasketSeparate;
import com.pood.server.service.PetSeparate;
import com.pood.server.service.UserPetImageSeparate;
import com.pood.server.service.UserReviewBlockSeparate;
import com.pood.server.service.UserReviewImageSeparate;
import com.pood.server.service.UserReviewSeparate;
import com.pood.server.service.UserReviewSueSeparate;
import com.pood.server.service.UserSeparate;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.util.StringUtils;

@Facade
@RequiredArgsConstructor
public class ReviewFacade {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReviewFacade.class);

    private final UserReviewSeparate userReviewSeparate;
    private final UserSeparate userSeparate;
    private final PetSeparate petSeparate;
    private final GoodsSeparate goodsSeparate;
    private final UserReviewImageSeparate userReviewImageSeparate;
    private final UserPetImageSeparate userPetImageSeparate;
    private final StorageService storageService;
    private final UserReviewBlockSeparate userReviewBlockSeparate;
    private final UserReviewSueSeparate userReviewSueSeparate;
    private final OrderBasketSeparate orderBasketSeparate;

    public Page<GoodsDetailReviewDto> getGoodsReviewList(final Pageable pageable,
        final int goodsIdx, final String token) {

        Integer userIdx = null;
        UserInfo userInfo = null;
        if (!StringUtils.isEmpty(token)) {
            userInfo = userSeparate.getUserByToken(token);
            userIdx = userInfo.getIdx();
        }

        GoodsDetailReviewDtoGroup goodsDetailReviewDtoGroup = new GoodsDetailReviewDtoGroup(
            userReviewSeparate.getGoodsReviewList(pageable, goodsIdx, userIdx,
                getNotInReviewIdx(userInfo)));

        return new GoodsDetailReviewResponse(goodsDetailReviewDtoGroup.getPage(),
            getUserPetListByGoodsDetailReviewDto(goodsDetailReviewDtoGroup),
            getUserPetImageListByGoodsDetailReviewDto(goodsDetailReviewDtoGroup),
            getUserREviewImageList(goodsDetailReviewDtoGroup.getPage())).getJsonData();
    }

    private List<Integer> getNotInReviewIdx(final UserInfo userInfo) {
        List<Integer> notInReviewIdx = new ArrayList<>();
        if (Objects.nonNull(userInfo)) {
            notInReviewIdx.addAll(userReviewBlockSeparate.getReviewIdxList(userInfo));
        }

        notInReviewIdx.addAll(userReviewSueSeparate.getCheckingReviewIdx());
        return notInReviewIdx.isEmpty() ? null : notInReviewIdx;
    }

    private List<Pet> getUserPetListByGoodsDetailReviewDto(
        final GoodsDetailReviewDtoGroup goodsReviewList) {
        return petSeparate.getPetAllByIdxIn(goodsReviewList.getUserPscId());
    }

    private List<UserPetImage> getUserPetImageListByGoodsDetailReviewDto(
        final GoodsDetailReviewDtoGroup goodsReviewList) {
        return userPetImageSeparate.userPetImageList(goodsReviewList.getUserPetIdx());
    }

    private List<UserReviewImage> getUserREviewImageList(
        final Page<GoodsDetailReviewDto> goodsReviewList) {
        List<Integer> reviewIdxList = goodsReviewList
            .stream()
            .map(GoodsDetailReviewDto::getIdx)
            .collect(Collectors.toList());

        return userReviewImageSeparate.getReviewAllByIdxIn(
            reviewIdxList);
    }

    public ReviewRatingAndCountDto getReviewRating(final int goodsIdx) {
        if (!goodsSeparate.existsByIdx(goodsIdx)) {
            throw new NotFoundException("상품 정보를 찾을 수 없습니다.");
        }
        return userReviewSeparate.getReviewRating(goodsIdx);
    }

    public void saveUserReview(final UserReviewSaveDto userReviewSaveDto, final String token,
        final List<MultipartFile> fileList) {
        final UserInfo userInfo = userSeparate.getOptionalUserByToken(token);
        final UserReview userReview = userReviewSaveDto.toEntity(userInfo);

        List<UserReviewImage> saveReviewImageList = new ArrayList<>();

        for (MultipartFile file : fileList) {
            try {
                String imgUrl = storageService.multiPartFileStore(file, "user_review");
                saveReviewImageList.add(UserReviewImage.builder()
                    .userReview(userReview)
                    .url(imgUrl)
                    .seq(0)
                    .build());

            } catch (IOException e) {
                LOGGER.error("s3에 이미지 저장을 실패했습니다.");
            }
        }

        userReviewImageSeparate.saveAll(saveReviewImageList);

    }

    public void insertSue(final ReviewBlockAndSueDto reviewBlockAndSueDto, final String token) {
        final UserInfo userInfo = userSeparate.getOptionalUserByToken(token);
        UserReview userReview = userReviewSeparate.findByIdx(reviewBlockAndSueDto.getReviewIdx());

        if (userReviewSueSeparate.existsByUserReviewAndUserInfo(userReview, userInfo)) {
            throw new AlreadyExistException("이미 신고된 리뷰 입니다.");
        }

        UserReviewSue userReviewSue = UserReviewSue.create(userInfo, userReview,
            reviewBlockAndSueDto.getText());

        userReviewSueSeparate.save(userReviewSue);

    }

    public void insertBlock(final ReviewBlockAndSueDto reviewBlockAndSueDto, final String token) {
        final UserInfo userInfo = userSeparate.getOptionalUserByToken(token);
        UserReview userReview = userReviewSeparate.findByIdx(reviewBlockAndSueDto.getReviewIdx());

        if (userReviewBlockSeparate.existsByUserReviewAndUserInfo(userReview, userInfo)) {
            throw new AlreadyExistException("이미 차단된 리뷰 입니다.");
        }

        UserReviewBlock userReviewBlock = UserReviewBlock.create(userInfo, userReview,
            reviewBlockAndSueDto.getText());

        userReviewBlockSeparate.save(userReviewBlock);
    }

    public Page<UserReviewImageDto.ReviewPhotoList> getReviewPhotoImgList(final Integer idx,
        final String token,
        final Pageable pageable) {
        List<Integer> notInReviewIdx = new ArrayList<>();
        if (!StringUtils.isEmpty(token)) {
            UserInfo userInfo = userSeparate.getUserByToken(token);
            notInReviewIdx.addAll(userReviewBlockSeparate.getReviewIdxList(userInfo));
        }
        notInReviewIdx.addAll(userReviewSueSeparate.getCheckingReviewIdx());
        return userSeparate.getReviewPhotoImgList(idx, notInReviewIdx, pageable);
    }

    public Page<GoodsImagesReviewDto> getReviewPhotoImgListV2(final Integer goodsIdx,
        final String token, final Pageable pageable) {
        Integer userIdx = null;
        UserInfo userInfo = null;
        if (!StringUtils.isEmpty(token)) {
            userInfo = userSeparate.getUserByToken(token);
            userIdx = userInfo.getIdx();
        }

        GoodsImagesReviewDtoGroup goodsImagesReviewDtoGroup = new GoodsImagesReviewDtoGroup(
            userReviewSeparate.getReviewPhotoImgListV2(pageable, goodsIdx,
                getNotInReviewIdx(userInfo)));

        return new GoodsImagesReviewResponse(
            goodsImagesReviewDtoGroup.getPage(),
            getUserPetListByGoodsImagesReviewDto(goodsImagesReviewDtoGroup),
            getUserPetImageListByGoodsImagesReviewDto(goodsImagesReviewDtoGroup)).getList();
    }

    private List<Pet> getUserPetListByGoodsImagesReviewDto(
        final GoodsImagesReviewDtoGroup goodsReviewList) {
        return petSeparate.getPetAllByIdxIn(goodsReviewList.getUserPscId());
    }

    private List<UserPetImage> getUserPetImageListByGoodsImagesReviewDto(
        final GoodsImagesReviewDtoGroup goodsReviewList) {
        return userPetImageSeparate.userPetImageList(goodsReviewList.getUserPetIdx());
    }

    public Boolean isReviewPossible(final UserInfo userInfo) {
        List<UserReview> userReviewList = userReviewSeparate.getUserReviewList(userInfo.getIdx());
        return new UserReviewGroup(userReviewList).isReviewPossible(
            new OrderBasketGroup(orderBasketSeparate.getUserOrderBasket(userInfo.getIdx())));
    }

    public void modifyReviewText(final UserInfo userInfo,
        final ReviewTextUpdateRequest reviewTextUpdateRequest) {
        userReviewSeparate.modifyReviewText(userInfo.getIdx(), reviewTextUpdateRequest.getIdx(),
            reviewTextUpdateRequest.getText());
    }
}
