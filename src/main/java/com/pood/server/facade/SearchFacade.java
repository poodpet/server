package com.pood.server.facade;

import com.pood.server.service.GoodsSeparate;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional("metaTransactionManager")
@RequiredArgsConstructor
public class SearchFacade {

    private final GoodsSeparate goodsSeparate;

    public List<String> searchCategoryAndKeyword(final int petCategoryIndex, final String keyWord,
        final Long goodsCt) {

        return goodsSeparate.searchCategoryAndKeyword(petCategoryIndex, keyWord, goodsCt);
    }

    public List<String> searchCategoryAndKeywordForFeedCompare(final int petCategoryIndex,
        final String keyWord) {

        return goodsSeparate.searchCategoryAndKeywordForFeedCompare(petCategoryIndex, keyWord);
    }
}
