package com.pood.server.facade;

import com.pood.server.dto.meta.goods.GoodsDto.GoodsCtDto;
import com.pood.server.entity.meta.GoodsCt;
import com.pood.server.service.GoodsCtService;
import com.pood.server.service.GoodsFilterCtService;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;

@Facade
@RequiredArgsConstructor
public class GoodsCtFacade {

    private final GoodsCtService goodsCtService;
    private final GoodsFilterCtService goodsFilterCtService;

    public List<GoodsCtDto> getGoodsFilterCtList(final int pcIdx) {
        List<GoodsCt> goodsCtList = goodsCtService.getGoodsCtList();

        List<GoodsCtDto> resultList = new ArrayList<>();
        for (GoodsCt goodsCt : goodsCtList) {
            resultList.add(GoodsCtDto.create(goodsCt, goodsFilterCtService.getGoodsCtList(pcIdx)));
        }

        return resultList;
    }
}
