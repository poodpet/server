package com.pood.server.facade;

import com.pood.server.controller.request.reviewclap.ReviewClapCreateDto;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.exception.CustomStatusException;
import com.pood.server.service.UserReviewClapSeparate;
import com.pood.server.service.UserReviewSeparate;
import com.pood.server.service.UserSeparate;
import lombok.RequiredArgsConstructor;

@Facade
@RequiredArgsConstructor
public class ReviewClapFacade {

    private final UserReviewClapSeparate userReviewClapSeparate;
    private final UserReviewSeparate userReviewSeparate;
    private final UserSeparate userSeparate;

    public void insertReviewLike(final ReviewClapCreateDto reviewClapCreateDto,
        final String token) {
        final UserInfo userInfo = tokenValidate(token);

        conflictValidation(reviewClapCreateDto, userInfo);
        userReviewClapSeparate.saveReviewLike(userInfo.getIdx(),
            userReviewSeparate.findByIdx(
                reviewClapCreateDto.getReviewIdx()));
    }

    private void conflictValidation(final ReviewClapCreateDto reviewClapCreateDto,
        final UserInfo userInfo) {
        if (userReviewClapSeparate.isExistReviewLike(reviewClapCreateDto, userInfo)) {
            throw CustomStatusException.conflict("이미 추천하기를 눌렀습니다.");
        }
    }

    public void deleteReviewLike(final int reviewIdx, final String token) {
        final UserInfo userInfo = tokenValidate(token);
        userReviewClapSeparate.deleteReviewLike(userInfo.getIdx(), reviewIdx);
    }

    private UserInfo tokenValidate(final String token) {
        return userSeparate.getUserByToken(token);
    }

}
