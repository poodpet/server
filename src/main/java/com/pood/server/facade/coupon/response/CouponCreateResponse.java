package com.pood.server.facade.coupon.response;

import lombok.Value;

@Value(staticConstructor = "of")
public class CouponCreateResponse {

    int discountPrice;
    int limitPrice;
    int availableTime;
    int overType;

}
