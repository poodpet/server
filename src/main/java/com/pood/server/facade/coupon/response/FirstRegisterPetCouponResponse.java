package com.pood.server.facade.coupon.response;

import lombok.Value;

@Value
public class FirstRegisterPetCouponResponse {

    Integer idx;

    String name;

    Integer type;

    Integer discountRate;

    Integer maxPrice;

    Integer limitPrice;

    Integer availableDay;

}
