package com.pood.server.facade;

import com.pood.server.api.service.sns.snsService;
import com.pood.server.config.meta.user.USER_NOTI;
import com.pood.server.dto.meta.order.dto_order;
import com.pood.server.entity.DeviceType;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.service.UserSeparate;
import com.pood.server.service.factory.SnsFactory;
import java.util.Objects;
import lombok.RequiredArgsConstructor;

@Facade
@RequiredArgsConstructor
public class SnsFacade {

    private final SnsFactory snsFactory;
    private final UserSeparate userSeparate;

    public void sendOrderCancel(final UserInfo user, final dto_order order,
        final String goodsCancelMsg) throws Exception {

        if (Objects.nonNull(user.getDeviceType())) {
            snsService snsService = snsFactory.getSnsFactoryMap(
                DeviceType.getType(user.getDeviceType()));
            // 반품이 아닌 경우에만 주문 취소일 경우 알림 보냄
            snsService.send(
                goodsCancelMsg,
                order.getUser_uuid(),
                order.getUser_idx(),
                USER_NOTI.CANCEL_FINISHED,
                "주문취소 알림",
                null);
        }
    }
}
