package com.pood.server.facade;

import com.pood.server.dto.meta.goods.PromotionDto;
import com.pood.server.dto.meta.goods.PromotionDto.PromotionGoodsListInfo;
import com.pood.server.dto.meta.promotion.PromotionGroupDtoGroup;
import com.pood.server.dto.meta.promotion.RunningPromotion;
import com.pood.server.entity.meta.PromotionGroup;
import com.pood.server.service.MarketingSortSeparate;
import com.pood.server.service.PromotionGroupSeparate;
import com.pood.server.service.PromotionImageSeparate;
import com.pood.server.service.PromotionSeparate;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;

@Facade
@RequiredArgsConstructor
public class PromotionFacade {

    private final PromotionSeparate promotionSeparate;
    private final PromotionGroupSeparate promotionGroupSeparate;
    private final PromotionImageSeparate promotionImageSeparate;

    private final MarketingSortSeparate marketingSortSeparate;

    public PromotionGoodsListInfo getPromotionDetail(final int idx) {

        PromotionDto.PromotionGoodsListInfo promotionGoodsListInfo = promotionSeparate.getPromotionGooodsInfo(
            idx);

        promotionGoodsListInfo.setPromotionImageList(
            promotionImageSeparate.getPromoitionImagesByPromotionIdx(idx));

        List<PromotionGroup> promotionGroupList = promotionGroupSeparate.promotionGroupListByPromotionIdx(
            idx);

        List<PromotionDto.PromotionGoods> promotionGroupGoodsList = promotionSeparate.getPromotionGoodsDetailInGroupIdx(
            promotionGroupList.stream()
                .map(PromotionGroup::getIdx)
                .collect(Collectors.toList())
        );

        PromotionGroupDtoGroup promotionGroupDtoGroup = new PromotionGroupDtoGroup();
        for (PromotionGroup promotionGroup : promotionGroupList) {
            promotionGroupDtoGroup.add(promotionGroup, promotionGroupGoodsList);
        }

        promotionGoodsListInfo.setPromotionGroupGoods(promotionGroupDtoGroup.getData());

        return promotionGoodsListInfo;
    }

    public List<RunningPromotion> proceedingPromotion(final int pcIdx) {
        return marketingSortSeparate.runningPromotions(pcIdx)
            .sortedList();
    }
}
