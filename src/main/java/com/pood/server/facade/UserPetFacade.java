package com.pood.server.facade;

import com.pood.server.controller.request.userpet.UserPetCreateDto;
import com.pood.server.controller.request.userpet.UserPetUpdateRequest;
import com.pood.server.controller.response.pet.AllergyDataResponse;
import com.pood.server.controller.response.pet.GrainSizeResponse;
import com.pood.server.controller.response.pet.UserPetCreateResponse;
import com.pood.server.controller.response.pet.UserPetInfoResponse;
import com.pood.server.dto.meta.coupon.PetRegisterCoupons;
import com.pood.server.dto.meta.pet.AllergyGroup;
import com.pood.server.dto.meta.pet.GrainSizeGroup;
import com.pood.server.dto.meta.pet.PetWorryGroup;
import com.pood.server.dto.meta.user.pet.UserPetAiDiagnosisGroup;
import com.pood.server.dto.meta.user.pet.UserPetAllergyMap;
import com.pood.server.dto.meta.user.pet.UserPetGrainSizeMap;
import com.pood.server.dto.meta.user.pet.UserPetGroup;
import com.pood.server.dto.meta.user.pet.UserPetResponseCreateGroup;
import com.pood.server.dto.meta.user.pet.UserPetWorryMap;
import com.pood.server.dto.user.pet.PetWorryCreateGroup;
import com.pood.server.dto.user.pet.UserPetAiDiagnosisDto;
import com.pood.server.dto.user.pet.UserPetAiDiagnosisDtoGroup;
import com.pood.server.dto.user.pet.UserPetAllergyDto;
import com.pood.server.dto.user.pet.UserPetAllergyDtoGroup;
import com.pood.server.dto.user.pet.UserPetGrainSizeDto;
import com.pood.server.dto.user.pet.UserPetGrainSizeDtoGroup;
import com.pood.server.entity.group.UserPetWeightDiaryGroup;
import com.pood.server.entity.group.UserPetWeightGroup;
import com.pood.server.entity.meta.AllergyData;
import com.pood.server.entity.meta.Coupon;
import com.pood.server.entity.meta.GrainSize;
import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.meta.PetWorry;
import com.pood.server.entity.user.BodyShape;
import com.pood.server.entity.user.UserCoupon;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserPet;
import com.pood.server.entity.user.UserPetBodyShapeDiary;
import com.pood.server.entity.user.UserPetFeed;
import com.pood.server.entity.user.UserPetImage;
import com.pood.server.entity.user.UserPetWeightDiary;
import com.pood.server.entity.user.group.AiRecommendDiagnosisArdCodeGroup;
import com.pood.server.facade.coupon.response.CouponCreateResponse;
import com.pood.server.facade.user.pet.request.BodyShapeManageCreateRequest;
import com.pood.server.facade.user.pet.request.BodyShapeModifyRequest;
import com.pood.server.facade.user.pet.request.WeightDeleteRequest;
import com.pood.server.facade.user.pet.request.WeightUpdateRequest;
import com.pood.server.facade.user.pet.response.BodyShapeManageCreateResponse;
import com.pood.server.facade.user.pet.response.BodyShapeManageResponse;
import com.pood.server.facade.user.pet.response.UserPetWeightResponse;
import com.pood.server.facade.user.pet.response.UserPetWeightUpdateResponse;
import com.pood.server.service.AiRecommendDiagnosisSeparate;
import com.pood.server.service.AllergyDataSeparate;
import com.pood.server.service.CouponSeparate;
import com.pood.server.service.GrainSizeSeparate;
import com.pood.server.service.PetAllergyJoinViewSeparate;
import com.pood.server.service.PetDiagnosisJoinViewSeparate;
import com.pood.server.service.PetGrainSizeJoinViewSeparate;
import com.pood.server.service.PetSeparate;
import com.pood.server.service.PetWorrySeparate;
import com.pood.server.service.UserAiRecommendDiagnosisSeparate;
import com.pood.server.service.UserCouponSeparate;
import com.pood.server.service.UserPetAllergySeparate;
import com.pood.server.service.UserPetBodyShapeSeparate;
import com.pood.server.service.UserPetFeedSeparate;
import com.pood.server.service.UserPetGrainSizeSeparate;
import com.pood.server.service.UserPetImageSeparate;
import com.pood.server.service.UserPetSeparate;
import com.pood.server.service.UserPetWeightDiarySeparate;
import com.pood.server.util.date.PetWeightDateUtil;
import com.pood.server.util.strategy.PercentNumberStrategy;
import com.pood.server.util.strategy.SerialNumberStrategy;
import com.pood.server.util.strategy.YearSliceStrategy;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@Facade
@RequiredArgsConstructor
public class UserPetFacade {

    private final UserPetSeparate userPetSeparate;
    private final UserPetImageSeparate userPetImageSeparate;

    private final UserPetAllergySeparate userPetAllergySeparate;
    private final AllergyDataSeparate allergyDataSeparate;

    private final UserPetGrainSizeSeparate userPetGrainSizeSeparate;
    private final GrainSizeSeparate grainSizeSeparate;

    private final UserPetFeedSeparate userPetFeedSeparate;

    private final PetDiagnosisJoinViewSeparate petDiagnosisJoinViewSeparate;
    private final PetAllergyJoinViewSeparate petAllergyJoinViewSeparate;
    private final PetGrainSizeJoinViewSeparate petGrainSizeJoinViewSeparate;

    private final UserAiRecommendDiagnosisSeparate userAiRecommendDiagnosisSeparate;
    private final PetWorrySeparate petWorrySeparate;
    private final UserPetWeightDiarySeparate userPetWeightDiarySeparate;
    private final UserPetBodyShapeSeparate userPetBodyShapeSeparate;

    private final PetSeparate petSeparate;

    private final UserCouponSeparate userCouponSeparate;
    private final CouponSeparate couponSeparate;
    private final AiRecommendDiagnosisSeparate aiRecommendDiagnosisSeparate;

    public UserPetCreateResponse createUserPet(final UserPetCreateDto userPetCreateDto,
        final UserInfo userInfo,
        final List<MultipartFile> multipartFile) {
        final UserPet userPetEntity = UserPet.toEntity(userInfo, userPetCreateDto);

        while (userPetSeparate.existsSerialNumber(userPetEntity.getSerialNumber())) {
            userPetEntity.serialNumber(new YearSliceStrategy(), new SerialNumberStrategy());
        }

        final UserPet savedUserPet = userPetSeparate.save(userPetEntity);

        userPetWeightDiarySeparate.save(savedUserPet, userPetCreateDto.getPetWeight());

        userPetImageSeparate.saveUserPetImage(savedUserPet.getIdx(), multipartFile);
        final PetWorryCreateGroup group = new PetWorryCreateGroup(
            userPetCreateDto.getPetWorryList());
        final List<PetWorry> petWorryList = petWorrySeparate.findAllByIdxIn(
            group.petWorryIdxList());
        userAiRecommendDiagnosisSeparate.insertPetWorry(petWorryList, group, savedUserPet.getIdx());
        final List<AllergyData> insertAllergyDataList = allergyDataSeparate.findAllByIdxIn(
            userPetCreateDto.getAllergyList());

        userPetAllergySeparate.insertAllAllergy(insertAllergyDataList, savedUserPet.getIdx());

        final List<GrainSize> insertGrainSizeList = grainSizeSeparate.findAllIdxIn(
            userPetCreateDto.getGrainSizeList());

        userPetGrainSizeSeparate.insertGrainSizeAll(insertGrainSizeList, savedUserPet.getIdx());

        userPetFeedSeparate.save(
            UserPetFeed.toEntity(savedUserPet.getIdx(), userPetCreateDto.getOtherFeedIdx(), null));

        final PetRegisterCoupons coupons = new PetRegisterCoupons(
            couponSeparate.findFirstRegisteredPetCouponList(),
            new PercentNumberStrategy());

        if (userCouponSeparate.isNotExistHistory(userInfo.getIdx(), coupons.findIdxList())) {
            final Coupon coupon = coupons.findPercent();
            userCouponSeparate.save(UserCoupon.init(coupon, userInfo));
            return UserPetCreateResponse.of(savedUserPet.getPetName(),
                savedUserPet.getSerialNumber(),
                CouponCreateResponse.of(coupon.getMaxPrice(), coupon.getLimitPrice(),
                    coupon.getAvailableDay(),
                    coupon.getOverType()));
        }

        return UserPetCreateResponse.isNotFirst(savedUserPet.getPetName(),
            savedUserPet.getSerialNumber());
    }

    public List<UserPetInfoResponse> userPetInfoList(final UserInfo userInfo) {
        UserPetGroup userPetGroup = new UserPetGroup(userPetSeparate.findAllByUserIdx(
            userInfo.getIdx()));
        final List<Pet> petInfoList = petSeparate.getPetAllByIdxIn(userPetGroup.petInfoIdxList());
        final List<Integer> userPetIdxList = userPetGroup.userPetIdxList();
        final Map<Integer, List<UserPetImage>> userPetImageMap = makeUserPetImageMap(
            userPetIdxList);

        final UserPetResponseCreateGroup group = UserPetResponseCreateGroup.builder()
            .userPetList(userPetGroup.getUserPetList())
            .petInfoList(petInfoList)
            .userPetWorry(new UserPetWorryMap(makeUserPetAiDiagnosisDtoMap(userPetIdxList)))
            .userPetAllergy(new UserPetAllergyMap(makeUserPetAllergyDtoMap(userPetIdxList)))
            .userPetGrainSize(new UserPetGrainSizeMap(makeUserPetGrainSizeDtoMap(userPetIdxList)))
            .userPetImageMap(userPetImageMap)
            .build();

        return group.toResponseList();
    }

    private Map<Integer, List<UserPetImage>> makeUserPetImageMap(
        final List<Integer> userPetIdxList) {
        return userPetImageSeparate.findAllByIdxInRecordBirthDesc(userPetIdxList)
            .stream().collect(Collectors.groupingBy(UserPetImage::getUserPetIdx));
    }

    private Map<Integer, List<UserPetAiDiagnosisDto>> makeUserPetAiDiagnosisDtoMap(
        final List<Integer> userPetIdxList) {
        final UserPetAiDiagnosisDtoGroup userPetAiDiagnosisDtoGroup = new UserPetAiDiagnosisDtoGroup(
            petDiagnosisJoinViewSeparate.findAllByIdxIn(
                userPetIdxList));

        return userPetAiDiagnosisDtoGroup.createAndGroupByUserPetIdxMap();
    }

    private Map<Integer, List<UserPetAllergyDto>> makeUserPetAllergyDtoMap(
        final List<Integer> userPetIdxList) {
        final UserPetAllergyDtoGroup userPetAllergyDtoGroup = new UserPetAllergyDtoGroup(
            petAllergyJoinViewSeparate.findAllByUserPetIdx(
                userPetIdxList));
        return userPetAllergyDtoGroup.createAndGroupByUserPetIdxMap();
    }

    private Map<Integer, List<UserPetGrainSizeDto>> makeUserPetGrainSizeDtoMap(
        final List<Integer> userPetIdxList) {
        final UserPetGrainSizeDtoGroup userPetGrainSizeDtoGroup = new UserPetGrainSizeDtoGroup(
            petGrainSizeJoinViewSeparate.findAllByUserPetIdx(
                userPetIdxList));
        return userPetGrainSizeDtoGroup.createAndGroupByUserPetIdxMap();
    }

    public void deleteUserPet(final UserInfo userInfo, final int userPetIdx) {
        userPetSeparate.petOwnerValidation(userPetIdx, userInfo.getIdx());
        userPetSeparate.deleteByUserPetIdx(userPetIdx);
        userPetImageSeparate.deleteByUserPetIdx(userPetIdx);
        userAiRecommendDiagnosisSeparate.deleteAllByUserPetIdx(userPetIdx);
        userPetAllergySeparate.deleteAllByUserPetIdx(userPetIdx);
        userPetGrainSizeSeparate.deleteAllByUserPetIdx(userPetIdx);
        userPetFeedSeparate.deleteByUserPetIdx(userPetIdx);
        userPetWeightDiarySeparate.deleteByUserPetIdx(userPetIdx);
    }

    public void updateUserPet(final UserInfo userInfo, final UserPetUpdateRequest userPetUpdateRequest,
        final
        List<MultipartFile> multipartFile) {
        userPetSeparate.petOwnerValidation(userPetUpdateRequest.getUserPetIdx(), userInfo.getIdx());
        final UserPet userPet = userPetSeparate.updateUserPet(userPetUpdateRequest);

        userPetImageSeparate.findByIdxAndThenUpdateImage(userPet.getIdx(), multipartFile);

        final PetWorryCreateGroup group = new PetWorryCreateGroup(
            userPetUpdateRequest.getPetWorryList());
        final List<PetWorry> petWorryList = petWorrySeparate.findAllByIdxIn(
            group.petWorryIdxList());
        userAiRecommendDiagnosisSeparate.deleteAllByUserPetIdx(userPet.getIdx());

        userAiRecommendDiagnosisSeparate.insertPetWorry(petWorryList, group, userPet.getIdx());

        final List<AllergyData> allergyDataList = allergyDataSeparate.findAllByIdxIn(
            userPetUpdateRequest.getAllergyList());
        userPetAllergySeparate.deleteAllByUserPetIdx(userPet.getIdx());
        userPetAllergySeparate.insertAllAllergy(allergyDataList, userPet.getIdx());

        final List<GrainSize> grainSizeList = grainSizeSeparate.findAllIdxIn(
            userPetUpdateRequest.getGrainSizeList());
        userPetGrainSizeSeparate.deleteAllByUserPetIdx(userPet.getIdx());
        userPetGrainSizeSeparate.insertGrainSizeAll(grainSizeList, userPet.getIdx());
    }

    public List<AllergyDataResponse> findAllergyDataList() {
        final AllergyGroup allergyGroup = new AllergyGroup(
            allergyDataSeparate.findAllOrderByPriority());
        return allergyGroup.toResponse();
    }

    public List<GrainSizeResponse> findGrainSizeDataList() {
        final GrainSizeGroup grainSizeGroup = new GrainSizeGroup(grainSizeSeparate.findAll());
        return grainSizeGroup.toResponse();
    }

    public PetWorryGroup findWorry(final int petCategoryIdx) {
        return petWorrySeparate.getFitSolutionWorryList(petCategoryIdx);
    }

    public BodyShapeManageResponse getBodyManagementInfo(final UserInfo userInfo,
        final int userPetIdx) {

        final UserPet userPet = userPetSeparate.findByIdx(userPetIdx);
        userPet.petOwnerIsCorrectValid(userInfo.getIdx());

        final UserPetWeightDiary recentWeightInfo = userPetWeightDiarySeparate.findRecentInfo(
            userPet.getIdx());

        final UserPetBodyShapeDiary recentBodyShapeInfo = userPetBodyShapeSeparate.findRecentInfoOrElseNull(
            userPet.getIdx());

        return BodyShapeManageResponse.of(recentWeightInfo.getWeight(),
            recentWeightInfo.getBaseDate(), recentBodyShapeInfo.getBodyShape(),
            recentBodyShapeInfo.getBaseDate());
    }

    public BodyShapeManageCreateResponse recordBodyShapeManagement(
        final BodyShapeManageCreateRequest request, final UserInfo userInfo) {

        final UserPet userPet = userPetSeparate.findByIdx(request.getUserPetIdx());
        userPet.petOwnerIsCorrectValid(userInfo.getIdx());

        userPetWeightDiarySeparate.recordBodyManagement(userPet, request.getWeight());
        userPetBodyShapeSeparate.recordBodyManagement(userPet, request.getBodyShape());

        return getBodyShapeManageCreateResponse(request.getBodyShape(), userPet.getIdx());
    }

    private BodyShapeManageCreateResponse getBodyShapeManageCreateResponse(
        final BodyShape requestBodyShape, final Integer userPetIdx) {

        final List<Long> userPetWorryIdx = userAiRecommendDiagnosisSeparate.getUserPetWorryIdxList(
            userPetIdx);

        final AiRecommendDiagnosisArdCodeGroup diagnosisIdxGroup = new AiRecommendDiagnosisArdCodeGroup(
            aiRecommendDiagnosisSeparate.findArdCodeAllByIdxIn(
                petWorrySeparate.getAiRecommendDiagnosisIdxList(userPetWorryIdx)
            )
        );

        final boolean conflictPetWorry = diagnosisIdxGroup.isConflictDiagnosisByBodyShape(
            requestBodyShape);

        final boolean selectHealthConcerns = diagnosisIdxGroup.isVisibleSelectPetWorry(
            requestBodyShape);

        return new BodyShapeManageCreateResponse(conflictPetWorry, selectHealthConcerns);
    }

    public BodyShapeManageCreateResponse modifyBodyShape(final UserInfo userInfo,
        final BodyShapeModifyRequest request) {

        final UserPet userPet = userPetSeparate.findByIdx(request.getUserPetIdx());
        userPet.petOwnerIsCorrectValid(userInfo.getIdx());

        userPetBodyShapeSeparate.recordBodyManagement(userPet, request.getBodyShape());

        return getBodyShapeManageCreateResponse(request.getBodyShape(), userPet.getIdx());
    }

    public UserPetWeightUpdateResponse weightChange(final WeightUpdateRequest request,
        final UserInfo userInfo) {
        userPetSeparate.petOwnerValidation(request.getUserPetIdx(), userInfo.getIdx());

        UserPet userPet = userPetSeparate.findByIdx(request.getUserPetIdx());
        if (isRequestIdxIsNull(request)) {
            return createUserPetWeightDiary(request, userPet);
        }
        return updateUserPetWeightDiary(request, userPet);
    }

    private UserPetWeightUpdateResponse updateUserPetWeightDiary(final WeightUpdateRequest request,
        final UserPet userPet) {
        UserPetWeightDiary weightRecord = userPetWeightDiarySeparate.updateWeightRecord(
            request.getIdx(), request.getWeight());
        return new UserPetWeightUpdateResponse(weightRecord.getIdx(), weightRecord.getWeight());
    }

    private UserPetWeightUpdateResponse createUserPetWeightDiary(final WeightUpdateRequest request,
        final UserPet userPet) {
        UserPetWeightDiary weightRecord = userPetWeightDiarySeparate.createWeightRecord(userPet,
            request.getDate(), request.getWeight());
        return new UserPetWeightUpdateResponse(weightRecord.getIdx(), weightRecord.getWeight());
    }

    private boolean isRequestIdxIsNull(final WeightUpdateRequest request) {
        return Objects.isNull(request.getIdx());
    }

    public UserPetWeightDiaryGroup findWeightDiary(final int userPetIdx, final UserInfo userInfo) {
        final UserPet userPet = userPetSeparate.findByIdx(userPetIdx);
        userPet.petOwnerIsCorrectValid(userInfo.getIdx());
        return new UserPetWeightDiaryGroup(
            userPetWeightDiarySeparate.findAllPetWeightRecord(userPet));
    }

    public List<UserPetWeightResponse> findWeightDiaryByYear(final int userPetIdx,
        final UserInfo userInfo, final int year) {
        UserPet userPet = userPetSeparate.findByIdx(userPetIdx);
        userPet.petOwnerIsCorrectValid(userInfo.getIdx());

        LocalDate nowDate = getYearEndDate(year);
        LocalDate startDate = LocalDate.of(nowDate.getYear(), 1, 1);

        UserPetWeightGroup userPetWeightGroup = new UserPetWeightGroup(
            new UserPetWeightDiaryGroup(userPet.getUserPetWeightDiaryList()),
            PetWeightDateUtil.of(startDate, nowDate).getWeekOfMonth());
        return userPetWeightGroup.toResponse();

    }

    public void deleteUserPetWeight(final UserInfo userInfo, final WeightDeleteRequest request) {
        UserPet userPet = userPetSeparate.findByIdx(request.getUserPetIdx());
        userPet.petOwnerIsCorrectValid(userInfo.getIdx());
        userPet.deleteWeightByIdx(request.getIdx());
        userPetSeparate.save(userPet);
    }

    private LocalDate getYearEndDate(final int year) {
        if (year == LocalDate.now().getYear()) {
            return LocalDate.now();
        }
        return LocalDate.of(year, 12, 31);
    }

    public void updatePetWorryConflictByBodyShape(final int userPetIdx, final int userIdx) {

        final UserPet userPet = userPetSeparate.findByIdx(userPetIdx);
        userPet.petOwnerIsCorrectValid(userIdx);

        final BodyShape recentBodyShape = userPetBodyShapeSeparate.findRecentInfo(
            userPet.getIdx()).getBodyShape();

        final List<Long> replaceWorryIdxListByBodyShape = petWorrySeparate.findAllIdxInDiagnosisIdx(
            aiRecommendDiagnosisSeparate.findAllByArdGroupCode(
                recentBodyShape.getArdGroupCode()));

        final List<Long> deleteWorryIdxListByDiagnosis = petWorrySeparate
            .findAllIdxInDiagnosisIdx(aiRecommendDiagnosisSeparate.findAllByArdGroupCode(
                recentBodyShape.getReverseArdGroupCode()));

        final UserPetAiDiagnosisGroup userPetWorryGroup = userAiRecommendDiagnosisSeparate
            .findUserPetWorryList(userPet.getIdx());

        final PetWorryCreateGroup group = new PetWorryCreateGroup(
            userPetWorryGroup.toRequestList());

        final PetWorryCreateGroup replaceGroup = group.replaceWorry(replaceWorryIdxListByBodyShape,
            deleteWorryIdxListByDiagnosis);

        final List<PetWorry> petWorryList = petWorrySeparate.findAllByIdxIn(
            replaceGroup.petWorryIdxList());

        userAiRecommendDiagnosisSeparate.updateUserPetWorry(petWorryList, replaceGroup,
            userPet.getIdx());
    }
}
