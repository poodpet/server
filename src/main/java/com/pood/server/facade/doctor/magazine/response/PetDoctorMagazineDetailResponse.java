package com.pood.server.facade.doctor.magazine.response;

import lombok.Value;

@Value
public class PetDoctorMagazineDetailResponse {

    long idx;
    String ctName;
    String title;
    String contents;

}
