package com.pood.server.facade.doctor.magazine.response;

import com.pood.server.entity.meta.PetDoctorMagazineCategory;
import lombok.Value;

@Value
public class PetDoctorMagazineCtResponse {

    Long idx;
    String name;
    String imgUrl;

    public PetDoctorMagazineCtResponse(final PetDoctorMagazineCategory petDoctorMagazineCategory) {
        this.idx = petDoctorMagazineCategory.getIdx();
        this.name = petDoctorMagazineCategory.getName();
        this.imgUrl = petDoctorMagazineCategory.getImgUrl();
    }
}
