package com.pood.server.facade.doctor.magazine;

import com.pood.server.entity.meta.mapper.doctor.PetDoctorMagazineDetailMapper;
import com.pood.server.facade.Facade;
import com.pood.server.facade.doctor.magazine.response.PetDoctorMagazineCtResponse;
import com.pood.server.facade.doctor.magazine.response.PetDoctorMagazineDetailResponse;
import com.pood.server.facade.doctor.magazine.response.PetDoctorMagazineResponse;
import com.pood.server.service.doctor.magazine.PetDoctorMagazineCategorySeparate;
import com.pood.server.service.doctor.magazine.PetDoctorMagazineSeparate;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Facade
@RequiredArgsConstructor
public class PetDoctorMagazineFacade {

    private final PetDoctorMagazineCategorySeparate petDoctorMagazineCategorySeparate;
    private final PetDoctorMagazineSeparate petDoctorMagazineSeparate;

    public List<PetDoctorMagazineCtResponse> getCategory() {
        return petDoctorMagazineCategorySeparate.getAllList().stream()
            .map(PetDoctorMagazineCtResponse::new)
            .collect(Collectors.toList());
    }

    public Page<PetDoctorMagazineResponse> getMagazineList(final Long ctIdx, final int pcIdx,
        final Pageable pageable) {
        return petDoctorMagazineSeparate.getMagazineListByCtIdx(ctIdx, pcIdx, pageable)
            .map(x -> new PetDoctorMagazineResponse(x.getIdx(), x.getCtImgUrl(), x.getCtName(),
                x.getTitle(), x.getRecordBirth()));
    }

    public PetDoctorMagazineDetailResponse getPetDoctorMagazineDetail(final long idx) {
        final PetDoctorMagazineDetailMapper magazineDetail = petDoctorMagazineSeparate.getMagazineDetail(
            idx);
        return new PetDoctorMagazineDetailResponse(magazineDetail.getIdx(),
            magazineDetail.getCtName(),
            magazineDetail.getTitle(),
            magazineDetail.getContents());
    }
}
