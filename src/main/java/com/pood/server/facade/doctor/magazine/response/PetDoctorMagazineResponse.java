package com.pood.server.facade.doctor.magazine.response;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import lombok.Value;

@Value
public class PetDoctorMagazineResponse {

    long idx;
    String ctImgUrl;
    String ctName;
    String title;
    String recordBirth;

    public PetDoctorMagazineResponse(final long idx, final String ctImgUrl, final String ctName,
        final String title,
        final LocalDateTime recordBirth) {
        this.idx = idx;
        this.ctImgUrl = ctImgUrl;
        this.ctName = ctName;
        this.title = title;
        this.recordBirth = recordBirth.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }
}
