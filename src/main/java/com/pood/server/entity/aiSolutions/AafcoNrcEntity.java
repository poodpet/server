package com.pood.server.entity.aiSolutions;

import com.pood.server.entity.calcUserPetInfo.CalcUserPetInfo;
import com.pood.server.entity.calcUserPetInfo.CalcUserPetInfoCat;
import com.pood.server.entity.calcUserPetInfo.CalcUserPetInfoDog;
import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.entity.meta.Feed;
import com.pood.server.entity.meta.Product;
import com.pood.server.entity.user.UserPet;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModelLegacy;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Deprecated
public abstract class AafcoNrcEntity {

    //    UserPet petType = null;
    Product product = null;
    AafcoNrc afcMin = null;
    Feed feed = null;
    UserPet pet = null;
    CalcUserPetInfo calcUserPetInfo;

    public AafcoNrcEntity(Product product, AafcoNrc afcMin, Feed feed, UserPet pet) {
        this.product = product;
        this.afcMin = afcMin;
        this.feed = feed;
        this.pet = pet;
        calcUserPetInfo = pet.getPcId() ==1 ?new CalcUserPetInfoDog() : new CalcUserPetInfoCat();
        calcUserPetInfo.setFeedNutrition(pet.getPcId(), product, product.getCupWeight(), pet);

    }

    //    Product product;


    static final int UNIT_ZERO = 0;
    static final int UNIT_G = 10;
    static final int UNIT_MG_2 = 20;
    static final int UNIT_IU_2 = 30;
    static final int UNIT_IU_4 = 40;
    static final int UNIT_MG_KG = 50;
    static final int UNIT_IU_KG = 60;

    static int type_one = 1;
    static int type_ten = 10;


    StringValues sv = new StringValues();
    List<NutritionDetailModelLegacy> nutritionDetailModelLegacyList = new ArrayList<>();

//    petCalorie = weight * (pet_act.pet_weight.pow(0.75f))

    /**
     * 최소필요 구하기
     * 영양소 = 아프코, 사용자에게 보여줄 데이터는 Min값으로
     * 왼쪽
     */
    private float needMinimum(float nutrition, float petCalorie) {
        return nutrition * (petCalorie / 1000f);
    }

    private Double needMinimum(Double nutrition, float petCalorie) {
        return Double.valueOf(nutrition.floatValue() * (petCalorie / 1000f));
    }

    /**
     * 제품이 함유하고 있는 양
     * type = 영양소별로 계산 방법이 다르다. 10 곱하는 경우와 1을 곱하는 경우가 있음
     * 오른쪽
     */
    private float productContainAmount(int type, float nutrition, float productCalorie, float petCalorie) {
        return (type * nutrition) * (1 / productCalorie) * petCalorie;
    }

    private float productContainAmount(int type, Double nutrition, Double productCalorie, float petCalorie) {
        return (type * nutrition.floatValue()) * (1 / productCalorie.floatValue()) * petCalorie;
    }


    /**
     * 제품의 특정 성분이 아프코 기준을 충족시켰는지 여부 체크
     */
    private boolean checkEnough(float min, float includeValue) {
        float minValue = Float.parseFloat(String.format(sv.percent_n, min));
        float containValue = Float.parseFloat(String.format(sv.percent_n, includeValue));
        return containValue >= minValue;
    }

    /**
     * 제품의 특정 성분이 아프코 기준을 충족시켰는지 여부 체크
     * max 값 존재
     */
    private boolean checkEnoughMinMax(Double min, float max, Double includeValue) {
        Double minValue = Double.parseDouble(String.format(sv.percent_n, min));
        float maxValue = Float.parseFloat(String.format(sv.percent_n, max));
        Double containValue = Double.parseDouble(String.format(sv.percent_n, includeValue));
        return containValue >= minValue && containValue < maxValue;
    }

    public float calculateScore(List<NutritionDetailModelLegacy> nutritionDetailList) {
        float score = 0f;
        for (int i = 0; i < nutritionDetailList.size(); i++) {
            if (nutritionDetailList.get(i) == null) {
                continue;
            }
            if (nutritionDetailList.get(i).isEnough()) {
                score += nutritionDetailList.get(i).getScore();
            }
        }

        return score;
    }

    public NutritionDetailModelLegacy makeNutritionDataMinMax(
        Double afcoMin, float afcoMax, Double productData, String title, int unit, float feed_calorie, float score, boolean round,
        int type) {
        if (productData <= 0) {
            return null;
        }

        float petCalorie = calcUserPetInfo.getPetCalorie();
        Double limitMin = 0d;
        float limitMax = 0f;
        Double contain = 0d;
        String minimumStr = "";
        String maximumStr = "";
        String containStr = "";

        if (title.equals("칼슘:인")) {
            limitMin = afcoMin;
            limitMax = afcoMax;
            contain = productData;
        } else {
            limitMin = needMinimum(afcoMin, petCalorie);
            limitMax = needMinimum(afcoMax, petCalorie);
            contain = Double.valueOf(productContainAmount(type, productData.floatValue(), feed_calorie, petCalorie));
        }

        switch (unit) {
            case UNIT_G:
                minimumStr = String.format(sv.need_g, limitMin);
                maximumStr = String.format(sv.need_g_max, limitMax);
                containStr = String.format(sv.include_g, contain);
                break;
            case UNIT_MG_2:
                minimumStr = String.format(sv.need_mg, limitMin);
                maximumStr = String.format(sv.need_mg_max, limitMax);
                containStr = String.format(sv.include_mg, contain);
                break;
            case UNIT_IU_2:
                minimumStr = String.format(sv.need_IU, limitMin);
                maximumStr = String.format(sv.need_IU_max, limitMax);
                containStr = String.format(sv.include_IU, contain);
                break;
            case UNIT_IU_4:
                minimumStr = String.format(sv.need_IU_4, limitMin);
                maximumStr = String.format(sv.need_IU_4_max, limitMax);
                containStr = String.format(sv.include_IU_4, contain);
                break;
            case UNIT_MG_KG:
                minimumStr = String.format(sv.need_mg_kg, limitMin);
                maximumStr = String.format(sv.need_mg_kg_max, limitMax);
                containStr = String.format(sv.include_mg_kg, contain);
                break;
            case UNIT_IU_KG:
                minimumStr = String.format(sv.need_iu_kg, limitMin);
                maximumStr = String.format(sv.need_iu_kg_max, limitMax);
                containStr = String.format(sv.include_iu_kg, contain);
                break;
            case UNIT_ZERO:
            default:
                minimumStr = String.format(sv.percent_n, limitMin);
                maximumStr = String.format(sv.percent_n, limitMax);
                containStr = String.format(sv.percent_n, contain);
                break;
        }

        if (round) {
            if (title.equals("비타민A") || title.equals("비타민D") || title.equals("비타민E")) {
                minimumStr = String.format(sv.need_IU_round, limitMin.intValue());
                maximumStr = String.format(sv.need_IU_round_max, (int) limitMax);
                containStr = String.format(sv.include_round, contain.intValue());
            }
        }

        if (title.equals("칼슘:인")) {
            minimumStr = "최소필요 " + limitMin;
            maximumStr = "최대허용 " + afcoMax;
            containStr =  contain.toString();
        }

        return new NutritionDetailModelLegacy(
            title,
            checkEnoughMinMax(limitMin, limitMax, contain),
            minimumStr,
            maximumStr,
            containStr,
            score, false
        );
//        }
    }

    public NutritionDetailModelLegacy makeNutritionData(Double afcoMin, Double productData, String title, int unit, float feed_calorie,
        float score, boolean round, int type) {

        if (productData <= 0) {
            return null;
        }

        float petCalorie = calcUserPetInfo.getPetCalorie();
        float limitMin = needMinimum(afcoMin, petCalorie).floatValue();
        float contain = productContainAmount(type, productData.floatValue(), feed_calorie, petCalorie);

        String minimumStr = "";
        String containStr = "";

        switch (unit) {
            case UNIT_G:
                minimumStr = String.format(sv.need_g, limitMin);
                containStr = String.format(sv.include_g, contain);
                break;
            case UNIT_MG_2:
                minimumStr = String.format(sv.need_mg, limitMin);
                containStr = String.format(sv.include_mg, contain);
                break;
            case UNIT_IU_2:
                minimumStr = String.format(sv.need_IU, limitMin);
                containStr = String.format(sv.include_IU, contain);
                break;
            case UNIT_IU_4:
                minimumStr = String.format(sv.need_IU_4, limitMin);
                containStr = String.format(sv.include_IU_4, contain);
                break;
            case UNIT_MG_KG:
                minimumStr = String.format(sv.need_mg_kg, limitMin);
                containStr = String.format(sv.include_mg_kg, contain);
                break;
            case UNIT_IU_KG:
                minimumStr = String.format(sv.need_iu_kg, limitMin);
                containStr = String.format(sv.include_iu_kg, contain);
                break;
            case UNIT_ZERO:
            default:
                minimumStr = String.format(sv.percent_n, limitMin);
                containStr = String.format(sv.percent_n, contain);
                break;
        }
        if (round) {
            if (title.equals("비타민A") || title.equals("비타민D") || title.equals("비타민E")) {
                minimumStr = String.format(sv.need_IU_round, (int) limitMin);
                containStr = String.format(sv.include_round, (int) contain);
            }
        }

        if (title.equals("칼슘:인")) {
            minimumStr = "최소필요 " + limitMin;
            containStr = String.valueOf(contain);
        }

        return new NutritionDetailModelLegacy(
            title,
            checkEnough(limitMin, contain),
            minimumStr,
            "",
            containStr,
            score, true
        );
    }

    public NutritionDetailModelLegacy getMiIron() {
        return makeNutritionData(
            afcMin.getMiIron(), feed.getMiIron(),
            "철", UNIT_MG_2, product.getCalorie(), 1f, false, type_one);
    }

    public NutritionDetailModelLegacy getMiZinc() {
        return makeNutritionData(
            afcMin.getMiZinc(), feed.getMiZinc(),
            "아연", UNIT_MG_2, product.getCalorie(), 1f, false, type_one);
    }

    public NutritionDetailModelLegacy getFaLinoleicacid() {
        return makeNutritionData(
            afcMin.getFaLinoleicacid(), feed.getFaLinoleicacid(),
            "리놀레산", UNIT_MG_2, product.getCalorie(), 0.1f, false, type_ten);
    }

    public NutritionDetailModelLegacy getMiSodium() {
        return makeNutritionData(
            afcMin.getMiSodium(), feed.getMiSodium(),
            "나트륨", UNIT_G, product.getCalorie(), 1f, false, type_ten);
    }

    public NutritionDetailModelLegacy getMiChloride() {
        return makeNutritionData(
            afcMin.getMiChloride(), feed.getMiChloride(),
            "염화이온", UNIT_G, product.getCalorie(), 0.5f, false, type_ten);
    }

    public NutritionDetailModelLegacy getViVitaminB1() {
        return makeNutritionData(
            afcMin.getViVitaminB1(), feed.getViVitaminB1(),
            "비타민B1", UNIT_IU_2, product.getCalorie(), 0.5f, false, type_one);
    }

    public NutritionDetailModelLegacy getViVitaminB2() {
        return makeNutritionData(
            afcMin.getViVitaminB2(), feed.getViVitaminB2(),
            "비타민B2", UNIT_IU_2, product.getCalorie(), 0.5f, false, type_one);
    }

    public NutritionDetailModelLegacy getViVitaminB3() {
        return makeNutritionData(
            afcMin.getViVitaminB3(), feed.getViVitaminB3(),
            "비타민B3", UNIT_IU_2, product.getCalorie(), 0.5f, false, type_one);
    }

    public NutritionDetailModelLegacy getViVitaminB5() {
        return makeNutritionData(
            afcMin.getViVitaminB5(), feed.getViVitaminB5(),
            "비타민B5", UNIT_IU_2, product.getCalorie(), 0.5f, false, type_one);
    }

    public NutritionDetailModelLegacy getViVitaminB6() {
        return makeNutritionData(
            afcMin.getViVitaminB6(), feed.getViVitaminB6(),
            "비타민B6", UNIT_IU_2, product.getCalorie(), 0.5f, false, type_one);
    }

    public NutritionDetailModelLegacy getViVitaminB7() {
        return makeNutritionData(
            afcMin.getViVitaminB7(), feed.getViVitaminB7(),
            "비타민B7", UNIT_IU_2, product.getCalorie(), 0f, false, type_one);
    }

    public NutritionDetailModelLegacy getViVitaminB9() {
        return makeNutritionData(
            afcMin.getViVitaminB9(), feed.getViVitaminB9(),
            "비타민B9", UNIT_IU_2, product.getCalorie(), 0.5f, false, type_one);
    }

    public NutritionDetailModelLegacy getViVitaminB12() {
        return makeNutritionData(
            afcMin.getViVitaminB12(), feed.getViVitaminB12(),
            "비타민B12", UNIT_IU_4, product.getCalorie(), 0.5f, false, type_one);
    }

    public NutritionDetailModelLegacy getAmLeucine() {
        return makeNutritionData(
            afcMin.getAmLeucine(), feed.getAmLeucine(),
            "류신", UNIT_MG_2, product.getCalorie(), 0.1f, false, type_ten);
    }

    public NutritionDetailModelLegacy getAmLysine() {
        return makeNutritionData(
            afcMin.getAmLysine(), feed.getAmLysine(),
            "라이신", UNIT_MG_2, product.getCalorie(), 0.1f, false, type_ten);
    }

    public NutritionDetailModelLegacy getAmPhenylanlanine() {

        return makeNutritionData(
//                getAm_phenylanlanine
            afcMin.getAmPhenylanlanine(), feed.getAmPhenylanlanine(),
            "페닐알라닌", UNIT_MG_2, product.getCalorie(), 0.1f, false, type_ten);
    }

    public NutritionDetailModelLegacy getAmThreonine() {
        return makeNutritionData(
            afcMin.getAmThreonine(), feed.getAmThreonine(),
            "트레오닌", UNIT_MG_2, product.getCalorie(), 0.1f, false, type_ten);

    }

    public NutritionDetailModelLegacy getAmValine() {
        return makeNutritionData(
            afcMin.getAmValine(), feed.getAmValine(),
            "발린", UNIT_MG_2, product.getCalorie(), 0.1f, false, type_ten);
    }

    public NutritionDetailModelLegacy getViVitaminK3() {
        return makeNutritionData(
            afcMin.getViVitaminK3(), feed.getViVitaminK3(),
            "비타민K", UNIT_IU_2, product.getCalorie(), 0f, false, type_one);
    }

    public NutritionDetailModelLegacy getAmHistidine() {
        return makeNutritionData(
            afcMin.getAmHistidine(), feed.getAmHistidine(),
            "히스티딘", UNIT_MG_2, product.getCalorie(), 0.1f, false, type_ten);
    }

    public NutritionDetailModelLegacy getAmIsoleucine() {
        return makeNutritionData(
            afcMin.getAmIsoleucine(), feed.getAmIsoleucine(),
            "아이소류신", UNIT_MG_2, product.getCalorie(), 0.1f, false, type_ten);
    }


    abstract NutritionDetailModelLegacy getMiCaPh();  // 개일 경우만 있음

    abstract NutritionDetailModelLegacy getOtTaurine();  // 고양이일 경우만 있음


    abstract NutritionDetailModelLegacy getCrudeProtein();

    abstract NutritionDetailModelLegacy getCrudeFat();

    abstract NutritionDetailModelLegacy getCalcium();

    abstract NutritionDetailModelLegacy getMiPhosphours();

    abstract NutritionDetailModelLegacy getMiMagnessium();

    abstract NutritionDetailModelLegacy getMiPotassium();

    abstract NutritionDetailModelLegacy getMiCopper();

    abstract NutritionDetailModelLegacy getMiManganese();

    abstract NutritionDetailModelLegacy getMiIodine();

    abstract NutritionDetailModelLegacy getMiSelenium();

    abstract NutritionDetailModelLegacy getViVitaminA();

    abstract NutritionDetailModelLegacy getViVitaminD();

    abstract NutritionDetailModelLegacy getViVitaminE();

    abstract NutritionDetailModelLegacy getAmMetCys();

    abstract NutritionDetailModelLegacy getAmPheTyr();

    abstract NutritionDetailModelLegacy getAmMethionine();

    abstract NutritionDetailModelLegacy getAmTryptophan();

    abstract NutritionDetailModelLegacy getFaALinoleicacid();

    abstract NutritionDetailModelLegacy getFaArachidonicAcid();

    public List<NutritionDetailModelLegacy> getAppData() {

        nutritionDetailModelLegacyList.add(getAmPhenylanlanine());
        nutritionDetailModelLegacyList.add(getAmValine());
        nutritionDetailModelLegacyList.add(getAmThreonine());
        nutritionDetailModelLegacyList.add(getMiIron());
        nutritionDetailModelLegacyList.add(getMiZinc());
        nutritionDetailModelLegacyList.add(getFaLinoleicacid());
        nutritionDetailModelLegacyList.add(getMiSodium());
        nutritionDetailModelLegacyList.add(getMiChloride());
        nutritionDetailModelLegacyList.add(getViVitaminB1());
        nutritionDetailModelLegacyList.add(getViVitaminB2());
        nutritionDetailModelLegacyList.add(getViVitaminB3());
        nutritionDetailModelLegacyList.add(getViVitaminB5());
        nutritionDetailModelLegacyList.add(getViVitaminB6());
        nutritionDetailModelLegacyList.add(getViVitaminB7());
        nutritionDetailModelLegacyList.add(getViVitaminB9());
        nutritionDetailModelLegacyList.add(getViVitaminB12());
        nutritionDetailModelLegacyList.add(getAmLeucine());
        nutritionDetailModelLegacyList.add(getAmLysine());
        nutritionDetailModelLegacyList.add(getViVitaminK3());
        nutritionDetailModelLegacyList.add(getAmHistidine());
        nutritionDetailModelLegacyList.add(getAmIsoleucine());

        nutritionDetailModelLegacyList.add(getCrudeProtein());
        nutritionDetailModelLegacyList.add(getCrudeFat());
        nutritionDetailModelLegacyList.add(getCalcium());
        nutritionDetailModelLegacyList.add(getMiPhosphours());
        nutritionDetailModelLegacyList.add(getMiMagnessium());
        nutritionDetailModelLegacyList.add(getMiPotassium());
        nutritionDetailModelLegacyList.add(getMiCopper());
        nutritionDetailModelLegacyList.add(getMiManganese());
        nutritionDetailModelLegacyList.add(getMiIodine());
        nutritionDetailModelLegacyList.add(getMiSelenium());
        nutritionDetailModelLegacyList.add(getViVitaminA());
        nutritionDetailModelLegacyList.add(getViVitaminD());
        nutritionDetailModelLegacyList.add(getViVitaminE());
        nutritionDetailModelLegacyList.add(getAmMetCys());
        nutritionDetailModelLegacyList.add(getAmPheTyr());
        nutritionDetailModelLegacyList.add(getAmMethionine());
        nutritionDetailModelLegacyList.add(getAmTryptophan());
        nutritionDetailModelLegacyList.add(getFaALinoleicacid());
        nutritionDetailModelLegacyList.add(getFaArachidonicAcid());

        if (pet.getPcId().equals(1)) {
            nutritionDetailModelLegacyList.add(getMiCaPh());
        } else {
            nutritionDetailModelLegacyList.add(getOtTaurine());
        }

        return nutritionDetailModelLegacyList;

    }

    public Float getNutritionDetailModelListScore() {
        Float result = 0f;

        nutritionDetailModelLegacyList = nutritionDetailModelLegacyList.stream().filter(Objects::nonNull).collect(Collectors.toList());
        for (NutritionDetailModelLegacy nutritionDetailModelLegacy : nutritionDetailModelLegacyList) {
            if (nutritionDetailModelLegacy.isEnough()) {
                result += nutritionDetailModelLegacy.getScore();
            }
        }
        return result;
    }
}
