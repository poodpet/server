package com.pood.server.entity.aiSolutions;


import com.pood.server.entity.Triple;
import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.user.UserPet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

@Deprecated
public class PetLifeCycleSolution {


    static final int SMALL_AND_PUPPY = 10;
    static final int SMALL_AND_ADULT = 20;
    static final int SMALL_AND_SENIOR = 30;
    static final int MIDDLE_AND_PUPPY = 40;
    static final int MIDDLE_AND_ADULT = 50;
    static final int MIDDLE_AND_SENIOR = 60;
    static final int BIG_AND_PUPPY = 70;
    static final int BIG_AND_ADULT = 80;
    static final int BIG_AND_SENIOR = 90;

    static final int DOG_MIX_SMALL = 100;//10kg 이하
    static final int DOG_MIX_MIDDLE = 101;//23kg 이하
    static final int DOG_MIX_BIG = 102;// 24kg 이상

    static final int CAT_KITTEN = 110;
    static final int CAT_ADULT = 120;
    static final int CAT_SENIOR = 130;
    static final int CAT_MIX_KITTEN = 140;
    static final int CAT_MIX_ADULT = 141;
    static final int CAT_MIX_SENIOR = 142;

    List<String> imageResource = new ArrayList<>(
            Arrays.asList("balance", "cal", "mineral", "nut")
    );

    public Triple create(UserPet userPet, Pet pscInfo  // Pet class
    ) {
        int type = calPetSizeAndAgeType(pscInfo, userPet);
        return new Triple(lifeCycleComment(type), iconImagePetSizeAge(type), imageCommentText(type));
    }

    ///라이프사이클 코멘트
    List<String> lifeCycleComment(int petSizeAndAgeType) {
        switch (petSizeAndAgeType) {
            case SMALL_AND_PUPPY:
                return Arrays.asList(petSizeAndAgeCommentText.get(0));
            case SMALL_AND_ADULT:
                return Arrays.asList(petSizeAndAgeCommentText.get(1));
            case SMALL_AND_SENIOR:
                return Arrays.asList(petSizeAndAgeCommentText.get(2));

            case MIDDLE_AND_PUPPY:
                return Arrays.asList(petSizeAndAgeCommentText.get(3));
            case MIDDLE_AND_ADULT:
                return Arrays.asList(petSizeAndAgeCommentText.get(4));
            case MIDDLE_AND_SENIOR:
                return Arrays.asList(petSizeAndAgeCommentText.get(5));

            case BIG_AND_PUPPY:
                return Arrays.asList(petSizeAndAgeCommentText.get(6));
            case BIG_AND_ADULT:
                return Arrays.asList(petSizeAndAgeCommentText.get(7));
            case BIG_AND_SENIOR:
                return Arrays.asList(petSizeAndAgeCommentText.get(8));

            case DOG_MIX_SMALL:
            case DOG_MIX_MIDDLE:
            case DOG_MIX_BIG:
                return Arrays.asList(petSizeAndAgeCommentText.get(9));

            case CAT_KITTEN:
                return Arrays.asList(petSizeAndAgeCommentText.get(10));
            case CAT_ADULT:
                return Arrays.asList(petSizeAndAgeCommentText.get(11));
            case CAT_SENIOR:
                return Arrays.asList(petSizeAndAgeCommentText.get(12));
            case CAT_MIX_KITTEN:
            case CAT_MIX_ADULT:
            case CAT_MIX_SENIOR:
                return Arrays.asList(petSizeAndAgeCommentText.get(13));
            default:
                return Arrays.asList(petSizeAndAgeCommentText.get(0));

        }

    }

    ///펫 사이즈와 나이에 따른 아이콘 리스트
    List<String> iconImagePetSizeAge(int petSizeAndAgeType) {
        switch (petSizeAndAgeType) {
            case SMALL_AND_PUPPY:
            case SMALL_AND_ADULT:
                return Arrays.asList(imageResource.get(1));
            case SMALL_AND_SENIOR:
                return Arrays.asList(imageResource.get(0));
            case MIDDLE_AND_PUPPY:
                return Arrays.asList(imageResource.get(1));
            case MIDDLE_AND_ADULT:
                return Arrays.asList(imageResource.get(1));
            case MIDDLE_AND_SENIOR:
                return Arrays.asList(imageResource.get(0), imageResource.get(2));
            case BIG_AND_PUPPY:
                return Arrays.asList(imageResource.get(2));
            case BIG_AND_ADULT:
                return Arrays.asList(imageResource.get(3));
            case BIG_AND_SENIOR:
                return Arrays.asList(imageResource.get(0));

            case DOG_MIX_SMALL:
                return Arrays.asList(imageResource.get(3), imageResource.get(0), imageResource.get(3));


            case DOG_MIX_MIDDLE:
                return Arrays.asList(imageResource.get(3), imageResource.get(2), imageResource.get(0));
            case DOG_MIX_BIG:
                return Arrays.asList(imageResource.get(3), imageResource.get(3), imageResource.get(2));

            case CAT_KITTEN:
                return Arrays.asList(imageResource.get(1));
            case CAT_ADULT:
                return Arrays.asList(imageResource.get(3));
            case CAT_SENIOR:
                return Arrays.asList(imageResource.get(0));

            case CAT_MIX_KITTEN:
                return Arrays.asList(imageResource.get(3), imageResource.get(0), imageResource.get(3));
            case CAT_MIX_ADULT:
                return Arrays.asList(imageResource.get(0), imageResource.get(2), imageResource.get(3));
            case CAT_MIX_SENIOR:
                return Arrays.asList(imageResource.get(2), imageResource.get(3), imageResource.get(3));

            default:
                return Arrays.asList(imageResource.get(1));
        }
    }

    ///아이콘 이미지 옆에 붙은 텍스트
    List<String> imageCommentText(int petSizeAndAgeType) {
        switch (petSizeAndAgeType) {
            case SMALL_AND_PUPPY:
                return Arrays.asList("첫 발정이 오기 전에 중성화 수술 추천", "목욕은 2주에 한 번 이하", "치아 건강에는 칫솔질이 최고", "칼/고칼로리 식단");
            case SMALL_AND_ADULT:
                return Arrays.asList("치아 건강에는 칫솔질이 최고", "적어도 3년에 한 번은 예방접종", "목욕은 2주에 한 번 이하", "칼/고칼로리 식단");
            case SMALL_AND_SENIOR:
            case MIDDLE_AND_SENIOR:
                return Arrays.asList("치아 건강에는 칫솔질이 최고", "목욕은 2주에 한 번 이하", "동물병원에서 1년에 1번 이상 건강검진", "밸/초고단백, 초고지방 식단 배제", "미/인이 높은 식단 배제");
            case MIDDLE_AND_PUPPY:
                return Arrays.asList("첫 발정이 오기 전에 중성화 수술 추천", " 목욕은 2주에 한 번 이하", "치아 건강에는 칫솔질이 최고", "칼/고칼로리 식단");
            case MIDDLE_AND_ADULT:
                return Arrays.asList("치아 건강에는 칫솔질이 최고", "적어도 3년에 한 번은 예방접종", "목욕은 2주에 한 번 이하", "칼/고칼로리 식단");
            case BIG_AND_PUPPY:
                return Arrays.asList("첫 발정이 오기 전에 중성화 수술 추천", "목욕은 2주에 한 번 이하", "치아 건강에는 칫솔질이 최고", "미/칼슘, 인이 높은 식단 배제");
            case BIG_AND_ADULT:
                return Arrays.asList("치아 건강에는 칫솔질이 최고", "적어도 3년에 한 번은 예방접종", "동물병원에서 1년에 1번 이상 건강검진", "목욕은 2주에 한 번 이하", "영/유산균이 풍부한 식단");
            case BIG_AND_SENIOR:
                return Arrays.asList("치아 건강에는 칫솔질이 최고", "목욕은 2주에 한 번 이하", "동물병원에서 1년에 1번 이상 건강검진", "밸/초고단백, 초고지방 식단 배제", "미/인이 높은 식단 배제");
            case DOG_MIX_SMALL:
                return Arrays.asList("글루코사민, 콘드로이틴, MSM 함유", "단일 단백질 식단 적용", "D-만노스 함유");

            case DOG_MIX_MIDDLE:
                return Arrays.asList("글루코사민, 콘드로이틴, MSM 함유",
                        "철, 아연, 구리가 풍부한 식단",
                        "고단백 식단 배제", "단일 단백질 식단 적용", "D-만노스 함유");

            case DOG_MIX_BIG:
                return Arrays.asList("오메가3가 풍부한 식단", "오메가3가 풍부한 식단", "칼슘, 인이 높은 식단 배제");
            case CAT_KITTEN:
                return Arrays.asList("첫 발정이 오기 전에 중성화 수술 추천", "치아 건강에는 칫솔질이 최고", "칼/고칼로리 식단");
            case CAT_ADULT:
                return Arrays.asList("치아 건강에는 칫솔질이 최고", "적어도 3년에 한 번은 예방접종", "영/타우린이 풍부한 식단");
            case CAT_MIX_SENIOR:
                return Arrays.asList("치아 건강에는 칫솔질이 최고", "동물병원에서 1년에 1번 이상 건강검진", "밸 / 고단백 식단", "미/인이 높은 식단 배제");
            default:
                return Arrays.asList("고칼로리 식단");

        }
    }

    // 0 : 퍼피, 1 : 어덜트, 2 : 시니어
    private int calPetSizeAndAgeType(Pet pscInfo, UserPet userPet) {
        int result = 0;

        int ageType = calcPetBirthType(userPet.getPetBirth().toString());

        //강아지
        if (pscInfo.getPcId() == 1) {
            String petSize = pscInfo.getPcSize();

            //믹스 먼저 필터링
            if (pscInfo.getPcTag().contains("믹스")) {

                if (ageType == 0) {
                    if (petSize.contains("소형")) {
                        result = DOG_MIX_SMALL;
                    } else if (petSize.contains("중형")) {
                        result = DOG_MIX_MIDDLE;
                    } else {
                        result = DOG_MIX_BIG;
                    }
//                    result = DOG_MIX_SMALL;
                } else if (ageType == 1) {  // 8 살 아래
                    result = DOG_MIX_MIDDLE;
                } else {   //  8 < 나이  >30
                    result = DOG_MIX_BIG;
                }

            } else if (petSize.contains("소형")) {
                if (ageType == 0) result = SMALL_AND_PUPPY;
                else if (ageType == 1) result = SMALL_AND_ADULT;
                else result = SMALL_AND_SENIOR;
            } else if (petSize.contains("중형")) {
                if (ageType == 0) result = MIDDLE_AND_PUPPY;
                else if (ageType == 1) result = MIDDLE_AND_ADULT;
                else result = MIDDLE_AND_SENIOR;
            } else if (petSize.contains("대형")) {
                if (ageType == 0) result = BIG_AND_PUPPY;
                else if (ageType == 1) result = BIG_AND_ADULT;
                else result = BIG_AND_SENIOR;
            }

        } else {

            //고양이
            if (pscInfo.getPcTag().contains("믹스")) {
                if (ageType == 0) result = CAT_MIX_KITTEN;
                else if (ageType == 1) result = CAT_MIX_ADULT;
                else result = CAT_MIX_SENIOR;
            } else {
                if (ageType == 0) result = CAT_KITTEN;
                else if (ageType == 1) result = CAT_ADULT;
                else result = CAT_SENIOR;
            }
        }

        return result;
    }

    //펫 나이에 대한 타입 세팅하기
    // 0 : 퍼피, 1 : 어덜트, 2 : 시니어
    int calcPetBirthType(String petBirth) {
        int type = 0;
        int age = getPetAge(petBirth.replaceAll("-", ""));
        if(age == 0){
            type = 0;
        }else if (age < 8) {
            type = 1;
        } else {
            type = 2;
        }
        return type;

    }

    //몇살인지 계산
    int getPetAge(String petBirth) {
        if (petBirth.equals("") || petBirth.equals("-")) return 0;
        Calendar cl = Calendar.getInstance();
        cl.set(Calendar.YEAR, Integer.parseInt(petBirth.substring(0, 4)));
        cl.set(Calendar.MONTH, Integer.parseInt(petBirth.substring(4, 6)) - 1);
        cl.set(Calendar.DAY_OF_MONTH, Integer.parseInt(petBirth.substring(6, 8)));

        long MINUTE_MS = 60 * 1000;
        long HOUR_MS = MINUTE_MS * 60;
        long DAY_MS = HOUR_MS * 24;
        long MONTH_MS = HOUR_MS * 24L * 30L;

        Calendar today = Calendar.getInstance();
        long month = (today.getTimeInMillis() - cl.getTimeInMillis()) / MONTH_MS;
        return (int) month / 12;

    }


    //생애주기
    //펫사이즈 및 나이
    //0 :소형견 퍼피
    //1 :소형견 어덜트
    //2 :소형견 시니어
    //3 :중형견 퍼피
    //4 :중형견 어덜트
    //5 :중형견 시니어
    //6 :대형견 퍼피
    //7 :대형견 어덜트
    //8 :대형견 시니어
    //9 :믹스견
    //10 :고양이 키튼
    //11 :고양이 어덜트
    //12 :고양이 시니어
    //13 :믹스묘
    List<String> petSizeAndAgeCommentText = new ArrayList<>(
            Arrays.asList(
                    "퍼피 시기에는 하루 중 18시간 이상을 자는 데 시간을 보냅니다. 너무 많이 자더라도 성장에 필요한" +
                            "수면이니 걱정하지 않으셔도 됩니다. " +
                            "동물병원에서  5번에 나누어 종합백신, 코로나, 켄넬코프, 신종플루, 광견병 예방백신을 합니다. 또한" +
                            "중성화 수술을 고려할 시기입니다. 첫 발정이 오기 전에 수술을 하는 것이 건강에 좋습니다. 암컷의 경우에는" +
                            "발정이 오면 생식기에 피가 약간 맺혀 있을 수 있어, 정상적인 것이니 놀라지 않으셔도 됩니다. " +
                            "배냇털이 빠지기 시작하는 시기입니다. 털을 모두 밀면 자극이 될 수 있으니 가위미용 정도만 추천합니다." +
                            "목욕 주기는 2주에 한 번 이하가 적당합니다. 너무 잦은 목욕은 피부에 자극이 될 수 있습니다." +
                            "치아 건강에는 칫솔질이 가장 도움이 됩니다. 칫솔질은 잇몸과 치아 사이의 연결 부분에 집중하여 닦아주는" +
                            "것이 좋습니다. 특히 어릴 때 손가락을 입 주위에 갖다 대는 것부터 천천히 연습하는 것을 추천하며, 치킨 맛" +
                            "치약을 사용하는 것도 좋은 방법입니다." +
                            "소변은 투명한 노란색이 정상이며, 변은 약간 묻어나오는 정도가 좋습니다. " +
                            "소형견 퍼피는 짧은 기간 빠르게 성장합니다. 뼈를 튼튼하게 하기 위해 칼슘, 인 등 많은 미네랄을 섭취해야" +
                            "합니다. 근육과 올바른 성장을 위해 많은 단백질과 필수지방산이 필요합니다. 성견에 비하여 위가 작아서" +
                            "고칼로리 식단을 먹어야 합니다.",
                    "성견은 하루 중 12시간 이상 자는 데 시간을 보냅니다. 사람보다는 더 많은 수면 시간이 필요합니다. " +
                            "동물병원에서 1년에 1번의 정기검진으로 혈액검사와 소변검사를 추천하며, 빈혈, 각종 감염병, 당뇨, 신장" +
                            "기능과 간기능을 확인할 수 있습니다. 적어도 3년에 한 번은 예방접종 부스팅이 필요합니다. 한 달에 한 번" +
                            "이상 몸무게를 측정하는 것이 좋으며, 1주일에 2%가 넘는 변동이 없는 것이 좋습니다." +
                            "치아 건강에는 칫솔질이 가장 도움이 됩니다. 칫솔질은 잇몸과 치아 사이의 연결 부분에 집중하여 닦아주는" +
                            "것이 좋습니다. 익숙하지 않더라도 손가락을 입 주위에 갖다 대는 것부터 천천히 연습하는 것을 추천하며," +
                            "치킨 맛 치약을 사용하는 것도 좋은 방법입니다." +
                            "소변은 투명한 노란색이 정상이며, 변은 약간 묻어나오는 정도가 좋습니다." +
                            "목욕 주기는 2주에 한 번 이하가 적당합니다. 너무 잦은 목욕은 피부에 자극이 될 수 있습니다." +
                            "소형견 어덜트는 긴 기간 주식으로 섭취해도 영양이 부족하거나 과도하지 않도록 균형 잡힌 식단이" +
                            "필요합니다.  대형견보다 위가 작기 때문에 적은 양을 먹어도 많은 칼로리를 줄 수 있는 고칼로리 식단을" +
                            "먹어야 합니다.",
                    "시니어 시기에 접어들면, 활동성이 떨어지고 자는 시간이 길어집니다. 질병이 있으면 자는 시간이 더 길어질" +
                            "수 있습니다." +
                            "10살 이상의 반려동물 절반이 종양이 있을 정도로 흔합니다. 동물병원에서 1년에 2번의 정기검진으로" +
                            "혈액검사와 소변검사를 추천하며, 빈혈, 각종 감염병, 당뇨, 신장 기능과 간기능을 확인할 수 있습니다. 한" +
                            "달에 한 번 이상 몸무게를 측정하는 것이 좋으며, 1주일에 2%가 넘는 변동이 없는 것이 좋습니다." +
                            "치아 건강에는 칫솔질이 가장 도움이 됩니다. 칫솔질은 잇몸과 치아 사이의 연결 부분에 집중하여 닦아주는" +
                            "것이 좋습니다. 익숙하지 않더라도 손가락을 입 주위에 갖다 대는 것부터 천천히 연습하는 것을 추천하며," +
                            "치킨 맛 치약을 사용하는 것도 좋은 방법입니다." +
                            "소변은 투명한 노란색이 정상이며, 변은 약간 묻어나오는 정도가 좋습니다." +
                            "목욕 주기는 이주에 한 번 이하가 적당합니다. 너무 잦은 목욕은 피부에 자극이 될 수 있습니다." +
                            "소형견 시니어는 소화능력과 노폐물 배설 능력이 떨어지기 때문에 지나친 고단백 고지방 사료는 추천하지" +
                            "않습니다. 신장건강을 위해 인을 미리 제한하는 것이 좋습니다. 많은 항산화물질을 넣는 경우가 많습니다." +
                            "또한 관절염이 있는 경우가 많아서 특정 영양소(글루코사민, 콘드로이틴, MSM, 오메가3, 녹색입홍합 등)" +
                            "이 필요합니다.",
                    "퍼피 시기에는 하루 중 18시간 이상을 자는 데 시간을 보냅니다. 너무 많이 자더라도 성장에 필요한" +
                            "수면이니 걱정하지 않으셔도 됩니다. " +
                            "동물병원에서  5번에 나누어 종합백신, 코로나, 켄넬코프, 신종플루, 광견병 예방백신을 합니다. 또한" +
                            "중성화 수술을 고려할 시기입니다. 첫 발정이 오기 전에 수술을 하는 것이 건강에 좋습니다. 암컷의 경우에는" +
                            "발정이 오면 생식기에 피가 약간 맺혀 있을 수 있어, 정상적인 것이니 놀라지 않으셔도 됩니다. " +
                            "배냇털이 빠지기 시작하는 시기입니다. 털을 모두 밀면 자극이 될 수 있으니 가위미용 정도만 추천합니다." +
                            "목욕 주기는 2주에 한 번 이하가 적당합니다. 너무 잦은 목욕은 피부에 자극이 될 수 있습니다." +
                            "치아 건강에는 칫솔질이 가장 도움이 됩니다. 칫솔질은 잇몸과 치아 사이의 연결 부분에 집중하여 닦아주는" +
                            "것이 좋습니다. 특히 어릴 때 손가락을 입 주위에 갖다 대는 것부터 천천히 연습하는 것을 추천하며, 치킨 맛" +
                            "치약을 사용하는 것도 좋은 방법입니다." +
                            "소변은 투명한 노란색이 정상이며, 변은 약간 묻어나오는 정도가 좋습니다. " +
                            "중형견 퍼피는 짧은 기간 빠르게 성장합니다. 뼈를 튼튼하게 하기 위해 칼슘, 인 등 많은 미네랄을 섭취해야" +
                            "합니다. 근육과 올바른 성장을 위해 많은 단백질과 필수지방산이 필요합니다. 성견에 비하여 위가 작아서" +
                            "적은 양으로도 많은 칼로리를 줄 수 있는 고칼로리 식단을 먹어야 합니다.",
                    "성견은 하루 중 12시간 이상 자는 데 시간을 보냅니다. 사람보다는 더 많은 수면 시간이 필요합니다. " +
                            "동물병원에서 1년에 1번의 정기검진으로 혈액검사와 소변검사를 추천하며, 빈혈, 각종 감염병, 당뇨, 신장" +
                            "기능과 간기능을 확인할 수 있습니다. 적어도 3년에 한 번은 예방접종 부스팅이 필요합니다. 한 달에 한 번" +
                            "이상 몸무게를 측정하는 것이 좋으며, 1주일에 2%가 넘는 변동이 없는 것이 좋습니다." +
                            "치아 건강에는 칫솔질이 가장 도움이 됩니다. 칫솔질은 잇몸과 치아 사이의 연결 부분에 집중하여 닦아주는" +
                            "것이 좋습니다. 익숙하지 않더라도 손가락을 입 주위에 갖다 대는 것부터 천천히 연습하는 것을 추천하며," +
                            "치킨 맛 치약을 사용하는 것도 좋은 방법입니다." +
                            "소변은 투명한 노란색이 정상이며, 변은 약간 묻어나오는 정도가 좋습니다." +
                            "목욕 주기는 2주에 한 번 이하가 적당합니다. 너무 잦은 목욕은 피부에 자극이 될 수 있습니다." +
                            "중형견 어덜트는 긴 기간 주식으로 섭취해도 영양이 부족하거나 과도하지 않도록 균형 잡힌 식단이" +
                            "필요합니다.  대형견보다 위가 작기 때문에 적은 양을 먹어도 많은 칼로리를 줄 수 있는 고칼로리 식단을" +
                            "먹어야 합니다.",
                    "시니어 시기에 접어들면, 활동성이 떨어지고 자는 시간이 길어집니다. 질병이 있으면 자는 시간이 더 길어질" +
                            "수 있습니다." +
                            "10살 이상의 반려동물 절반이 종양이 있을 정도로 흔합니다. 동물병원에서 1년에 2번의 정기검진으로" +
                            "혈액검사와 소변검사를 추천하며, 빈혈, 각종 감염병, 당뇨, 신장 기능과 간기능을 확인할 수 있습니다. 한" +
                            "달에 한 번 이상 몸무게를 측정하는 것이 좋으며, 1주일에 2%가 넘는 변동이 없는 것이 좋습니다." +
                            "치아 건강에는 칫솔질이 가장 도움이 됩니다. 칫솔질은 잇몸과 치아 사이의 연결 부분에 집중하여 닦아주는" +
                            "것이 좋습니다. 익숙하지 않더라도 손가락을 입 주위에 갖다 대는 것부터 천천히 연습하는 것을 추천하며," +
                            "치킨 맛 치약을 사용하는 것도 좋은 방법입니다." +
                            "소변은 투명한 노란색이 정상이며, 변은 약간 묻어나오는 정도가 좋습니다." +
                            "목욕 주기는 이주에 한 번 이하가 적당합니다. 너무 잦은 목욕은 피부에 자극이 될 수 있습니다." +
                            "중형견 시니어는 소화능력과 노폐물 배설 능력이 떨어지기 때문에 지나친 초고단백, 초고지방 식단은" +
                            "피해야합니다. 신장건강을 위해 인을 미리 제한하는 것이 좋습니다. 많은 항산화물질을 넣는 경우가" +
                            "많습니다. 또한 관절염이 있는 경우가 많아서 특정 영양소(글루코사민, 콘드로이틴, MSM, 오메가3," +
                            "녹색입홍합 등)이 필요합니다.",
                    "퍼피 시기에는 하루 중 18시간 이상을 자는 데 시간을 보냅니다. 너무 많이 자더라도 성장에 필요한" +
                            "수면이니 걱정하지 않으셔도 됩니다. " +
                            "동물병원에서  5번에 나누어 종합백신, 코로나, 켄넬코프, 신종플루, 광견병 예방백신을 합니다. 또한" +
                            "중성화 수술을 고려할 시기입니다. 첫 발정이 오기 전에 수술을 하는 것이 건강에 좋습니다. 암컷의 경우에는" +
                            "발정이 오면 생식기에 피가 약간 맺혀 있을 수 있어, 정상적인 것이니 놀라지 않으셔도 됩니다. " +
                            "배냇털이 빠지기 시작하는 시기입니다. 털을 모두 밀면 자극이 될 수 있으니 가위미용 정도만 추천합니다." +
                            "목욕 주기는 2주에 한 번 이하가 적당합니다. 너무 잦은 목욕은 피부에 자극이 될 수 있습니다." +
                            "치아 건강에는 칫솔질이 가장 도움이 됩니다. 칫솔질은 잇몸과 치아 사이의 연결 부분에 집중하여 닦아주는" +
                            "것이 좋습니다. 특히 어릴 때 손가락을 입 주위에 갖다 대는 것부터 천천히 연습하는 것을 추천하며, 치킨 맛" +
                            "치약을 사용하는 것도 좋은 방법입니다." +
                            "소변은 투명한 노란색이 정상이며, 변은 약간 묻어나오는 정도가 좋습니다. " +
                            "대형견 퍼피는 미네랄 중 칼슘과 인이 적당해야 합니다. 많은 칼슘과 인의 섭취는 오히려 체내에서 뼈를" +
                            "녹여내는 등 병을 유발하기 때문에 주의가 필요합니다. 근육과 올바른 성장을 위해 많은 단백질과" +
                            "필수지방산이 필요합니다. 성견에 비하여 위가 작아서 적은 양으로도 많은 칼로리를 줄 수 있는 고칼로리" +
                            "식단을 먹어야 합니다. 또한 대형견 퍼피는 적정 몸무게에 도달할 때까지 1년 이상 퍼피사료를 먹이는 것을" +
                            "추천합니다." +
                            "식이섬유 중 셀룰로오스와 같은 불용성 식이섬유 비중이 높은 것이 좋습니다. " +
                            "또한 소화가 잘되는 좋은 단백질 원료와 유산균 함량이 높은 사료가 도움이 됩니다.",
                    "성견은 하루 중 12시간 이상 자는 데 시간을 보냅니다. 사람보다는 더 많은 수면 시간이 필요합니다. " +
                            "동물병원에서 1년에 1번의 정기검진으로 혈액검사와 소변검사를 추천하며, 빈혈, 각종 감염병, 당뇨, 신장" +
                            "기능과 간기능을 확인할 수 있습니다. 적어도 3년에 한 번은 예방접종 부스팅이 필요합니다. 한 달에 한 번" +
                            "이상 몸무게를 측정하는 것이 좋으며, 1주일에 2%가 넘는 변동이 없는 것이 좋습니다." +
                            "치아 건강에는 칫솔질이 가장 도움이 됩니다. 칫솔질은 잇몸과 치아 사이의 연결 부분에 집중하여 닦아주는" +
                            "것이 좋습니다. 익숙하지 않더라도 손가락을 입 주위에 갖다 대는 것부터 천천히 연습하는 것을 추천하며," +
                            "치킨 맛 치약을 사용하는 것도 좋은 방법입니다." +
                            "소변은 투명한 노란색이 정상이며, 변은 약간 묻어나오는 정도가 좋습니다." +
                            "목욕 주기는 2주에 한 번 이하가 적당합니다. 너무 잦은 목욕은 피부에 자극이 될 수 있습니다." +
                            "대형견 어덜트는 소형견에 비하여 변이 무른 편입니다. 식이섬유 중 셀룰로오스와 같은 불용성 식이섬유" +
                            "비중이 높은 것이 좋습니다. 또한 소화가 잘되는 좋은 단백질 원료와 유산균 함량이 높은 사료가 도움이" +
                            "됩니다.",
                    "시니어 시기에 접어들면, 활동성이 떨어지고 자는 시간이 길어집니다. 질병이 있으면 자는 시간이 더 길어질" +
                            "수 있습니다." +
                            "10살 이상의 반려동물 절반이 종양이 있을 정도로 흔합니다. 동물병원에서 1년에 2번의 정기검진으로" +
                            "혈액검사와 소변검사를 추천하며, 빈혈, 각종 감염병, 당뇨, 신장 기능과 간기능을 확인할 수 있습니다. 한" +
                            "달에 한 번 이상 몸무게를 측정하는 것이 좋으며, 1주일에 2%가 넘는 변동이 없는 것이 좋습니다." +
                            "치아 건강에는 칫솔질이 가장 도움이 됩니다. 칫솔질은 잇몸과 치아 사이의 연결 부분에 집중하여 닦아주는" +
                            "것이 좋습니다. 익숙하지 않더라도 손가락을 입 주위에 갖다 대는 것부터 천천히 연습하는 것을 추천하며," +
                            "치킨 맛 치약을 사용하는 것도 좋은 방법입니다." +
                            "소변은 투명한 노란색이 정상이며, 변은 약간 묻어나오는 정도가 좋습니다." +
                            "목욕 주기는 이주에 한 번 이하가 적당합니다. 너무 잦은 목욕은 피부에 자극이 될 수 있습니다." +
                            "대형견 시니어는 소화능력과 노폐물 배설 능력이 떨어지기 때문에 지나친 초고단백, 초고지방 식단은" +
                            "피해야합니다. 신장건강을 위해 인을 미리 제한하는 것이 좋습니다. 많은 항산화물질을 넣는 경우가" +
                            "많습니다. 또한 관절염이 있는 경우가 많아서 특정 영양소(글루코사민, 콘드로이틴, MSM, 오메가3," +
                            "녹색입홍합 등)이 필요합니다.",
                    "믹스견은 서로 다른 부모로부터 다양한 유전자가 섞인 덕분에 품종견보다 건강하다고 알려져 있습니다. \n\n " +
                            "하지만 최근 미국 연구에 따르면 불독과 같이 코가 많이 눌린 종을 제외하면 품종견과 믹스견의 질병 발생 차이가 크게 없습니다. " +
                            "그러므로 믹스견이니 건강할 것이라 생각하기보다는 나이가 들며 생길 수 있는 질병들(관절염, 신장질환 등)에 관심을 갖고 주기적인 건강검진으로 예방하는 것이 좋습니다.",
                    "키튼 시기에는 하루 중 20시간 이상을 자는 데 시간을 보냅니다. 너무 많이 자더라도 성장에 필요한" +
                            "수면이니 걱정하지 않으셔도 됩니다." +
                            "동물병원에서 3차에 나누어 종합백신을 합니다. 또한 중성화 수술을 고려할 시기입니다. 첫 발정이 오기" +
                            "전에 수술을 하는 것이 건강에 도움이 됩니다. 고양이는 무혈 생리를 하므로, 겉으로 티가 나지 않습니다." +
                            "대신 특유의 소리를 통해 발정이 온 것을 짐작할 수 있습니다. " +
                            "치아 건강에는 칫솔질이 가장 도움이 됩니다. 칫솔질은 잇몸과 치아 사이의 연결 부분에 집중하여 닦아주는" +
                            "것이 좋습니다. 특히 어릴 때 손가락을 입 주위에 갖다 대는 것부터 천천히 연습하는 것을 추천하며, 치킨 맛" +
                            "치약을 사용하는 것도 좋은 방법입니다." +
                            "키튼은 짧은 기간 빠르게 성장합니다. 뼈를 튼튼하게 하기 위해 칼슘, 인 등 많은 미네랄을 섭취해야 합니다." +
                            "근육과 올바른 성장을 위해 많은 단백질과 필수지방산이 필요합니다. 성묘에 비하여 위가 작아서 적은" +
                            "양으로도 많은 칼로리를 줄 수 있는 고칼로리 식단을 먹어야 합니다.",
                    "성묘는 하루 중 16시간 이상 자는 데 시간을 보냅니다. 사람의 두 배 많은 수면 시간이 필요합니다. 깨어" +
                            "있는 시간 동안은 하루 20분 이상 술래잡기, 낚시놀이를 하며 놀아주는 것이 좋습니다." +
                            "동물병원에서 1년에 1번의 정기검진으로 혈액검사와 소변검사를 추천하며, 빈혈, 각종 감염병, 당뇨, 신장" +
                            "기능과 간기능을 확인할 수 있습니다. 적어도 3년에 한 번은 예방접종 부스팅이 필요합니다. 한 달에 한 번" +
                            "이상 몸무게를 측정하는 것이 좋으며, 1주일에 2%가 넘는 변동이 없는 것이 좋습니다." +
                            "치아 건강에는 칫솔질이 가장 도움이 됩니다. 칫솔질은 잇몸과 치아 사이의 연결 부분에 집중하여 닦아주는" +
                            "것이 좋습니다. 익숙하지 않더라도 손가락을 입 주위에 갖다 대는 것부터 천천히 연습하는 것을 추천하며," +
                            "치킨 맛 치약을 사용하는 것도 좋은 방법입니다." +
                            "화장실 청소를 하며, 덩어리의 크기와 개수를 주기적으로 확인하면 건강관리에 도움이 됩니다. 덩어리" +
                            "개수는 일정한 것이 좋으며 적어져도 많아져도 건강의 이상 신호일 수 있습니다." +
                            "긴 기간 주식으로 섭취해도 영양이 부족하거나 과도하지 않도록 균형 잡힌 식단이 필요합니다. 특히" +
                            "고양이는 강아지와 달리 철저한 육식 동물이므로 타우린, 단백질 및 필수지방산 요구량이 다릅니다." +
                            "강아지사료를 고양이에게 장기간 급여 시 심장질환의 위험이 있습니다.",
                    "시니어 시기에 접어들면, 활동성이 떨어지고 자는 시간이 길어집니다. 질병이 있으면 자는 시간이 더 길어질" +
                            "수 있습니다. " +
                            "10살 이상의 반려동물 절반이 종양이 있을 정도로 흔합니다. 동물병원에서 1년에 2번의 정기검진으로" +
                            "혈액검사와 소변검사를 추천하며, 빈혈, 각종 감염병, 당뇨, 신장 기능과 간기능을 확인할 수 있습니다. 한" +
                            "달에 한 번 이상 몸무게를 측정하는 것이 좋으며, 1주일에 2%가 넘는 변동이 없는 것이 좋습니다." +
                            "치아 건강에는 칫솔질이 가장 도움이 됩니다. 칫솔질은 잇몸과 치아 사이의 연결 부분에 집중하여 닦아주는" +
                            "것이 좋습니다. 익숙하지 않더라도 손가락을 입 주위에 갖다 대는 것부터 천천히 연습하는 것을 추천하며," +
                            "치킨 맛 치약을 사용하는 것도 좋은 방법입니다." +
                            "화장실 청소를 하며, 덩어리의 크기와 개수를 주기적으로 확인하면 건강관리에 도움이 됩니다. 덩어리" +
                            "개수는 일정한 것이 좋으며 적어져도 많아져도 건강의 이상 신호일 수 있습니다." +
                            "고양이 시니어는 근육량이 떨어지고 체중이 줄어들 가능성이 높기 때문에 높은 단백 식단이 필요합니다." +
                            "신장건강을 위해 인을 미리 제한하는 것이 좋습니다. 많은 항산화물질을 넣는 경우가 많습니다. 또한" +
                            "관절염이 있는 경우가 많아서 특정 영양소(글루코사민, 콘드로이틴, MSM, 오메가3, 녹색입홍합 등)이" +
                            "필요합니다.",
                    "믹스묘는 서로 다른 부모로부터 다양한 유전자가 섞인 덕분에 품종묘보다 건강하다고 알려져 있습니다. " +
                            "하지만 최근 미국 연구에 따르면 페르시안과 같이 코가 많이 눌린 종을 제외하면 품종묘과 믹스묘의 질병 발생 차이가 크게 없습니다. " +
                            "그러므로 믹스묘라 건강할 것이라 생각하기보다는 나이가 들며 생길 수 있는 질병들(관절염, 신장질환 등)에 관심을 갖고 주기적인 건강검진으로 예방하는 것이 좋습니다"
            )
    );
}
