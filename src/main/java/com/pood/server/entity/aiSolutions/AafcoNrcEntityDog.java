package com.pood.server.entity.aiSolutions;

import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.entity.meta.Feed;
import com.pood.server.entity.meta.Product;
import com.pood.server.entity.user.UserPet;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModelLegacy;

@Deprecated
public class AafcoNrcEntityDog extends AafcoNrcEntity {

    static int type_one = 1;
    static int type_ten = 10;

    static Double afco_mi_ca_ph_min_dog = 1d;
    static float afco_mi_ca_ph_max_dog = 2f;

    static Double afco_cystine_plus_methionine_min_dog = 1.63d;
    static Double afco_phenylanlanine_plus_tyrosine_min_dog = 1.85d;

    public AafcoNrcEntityDog(Product product, AafcoNrc afcMin, Feed feed, UserPet pet) {
        super(product, afcMin, feed, pet);
    }

    float productCalorie = product.getCalorie();

    static final float variable = 1000f;

    @Override
    public NutritionDetailModelLegacy getCrudeProtein() {
        return makeNutritionData(
            afcMin.getPrProtein(), feed.getPrProtein(),
            "조단백", UNIT_G, productCalorie, 16f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getCrudeFat() {
        return makeNutritionData(
            afcMin.getPrFat(), feed.getPrFat(),
            "조지방", UNIT_G, productCalorie, 16f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getCalcium() {
        return makeNutritionDataMinMax(
            afcMin.getMiCalcium(), 6.25f, feed.getMiCalcium(),
            "칼슘", UNIT_G, productCalorie, 16f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getMiPhosphours() {
        return makeNutritionDataMinMax(
            afcMin.getMiPhosphours(), 4f, feed.getMiPhosphours(),
            "인", UNIT_G, productCalorie, 16f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getMiMagnessium() {
        return makeNutritionData(
            afcMin.getMiMagnessium(), feed.getMiMagnessium(),
            "마그네슘", UNIT_G, productCalorie, 1f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getMiPotassium() {
        return makeNutritionData(
            afcMin.getMiPotassium(), feed.getMiPotassium(),
            "칼륨", UNIT_G, productCalorie, 1f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getMiCopper() {
        return makeNutritionData(
            afcMin.getMiCopper(), feed.getMiCopper(),
            "구리", UNIT_MG_2, productCalorie, 0.5f, false, type_one);
    }

    @Override
    public NutritionDetailModelLegacy getMiManganese() {
        return makeNutritionData(
            afcMin.getMiManganese(), feed.getMiManganese(),
            "망간", UNIT_MG_2, productCalorie, 1f, false, type_one);
    }


    @Override
    public NutritionDetailModelLegacy getMiIodine() {
        return makeNutritionDataMinMax(
            afcMin.getMiIodine(), 2.75f, feed.getMiIodine(), "요오드",
            UNIT_MG_2, productCalorie, 1f, false, type_one);
    }

    @Override
    public NutritionDetailModelLegacy getMiSelenium() {
        return makeNutritionDataMinMax(
            afcMin.getMiSelenium(), 0.5f, feed.getMiSelenium(), "셀레늄",
            UNIT_MG_2, productCalorie, 1f, false, type_one);
    }

    @Override
    public NutritionDetailModelLegacy getViVitaminA() {
        return makeNutritionDataMinMax(
            afcMin.getViVitaminA(), 62500f, feed.getViVitaminA(),
            "비타민A", UNIT_IU_2, productCalorie, 1f, true, type_one);
    }

    @Override
    public NutritionDetailModelLegacy getViVitaminD() {
        return makeNutritionDataMinMax(
            afcMin.getViVitaminD(), 750f, feed.getViVitaminD(),
            "비타민D", UNIT_IU_2, productCalorie, 1f, true, type_one
        );
    }

    @Override
    public NutritionDetailModelLegacy getViVitaminE() {
        return makeNutritionData(
            afcMin.getViVitaminE(), feed.getViVitaminE(), "비타민E", UNIT_IU_2,
            productCalorie, 1f, true, type_one);
    }


    @Override
    public NutritionDetailModelLegacy getAmMetCys() {
        return makeNutritionData(
            afco_cystine_plus_methionine_min_dog, feed.getAmMetCys(),
            "메티오닌+시스틴", UNIT_MG_2, productCalorie, 0.8f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getAmPheTyr() {
        return makeNutritionData(
            afco_phenylanlanine_plus_tyrosine_min_dog, feed.getAmPheTyr(),
            "페닐알라닌+티로신", UNIT_MG_2, productCalorie, 0.1f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getAmMethionine() {
        return makeNutritionData(
            afcMin.getAmMethionine(), feed.getAmMethionine(),
            "메티오닌", UNIT_MG_2, productCalorie, 0.8f, false, type_ten);
    }


    @Override
    public NutritionDetailModelLegacy getAmTryptophan() {
        return makeNutritionData(
            afcMin.getAmTryptophan(), feed.getAmTryptophan(),
            "트립토판", UNIT_MG_2, productCalorie, 0.1f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getFaALinoleicacid() {
        return makeNutritionData(
            afcMin.getFaALinoleicacid(), feed.getFaALinoleicacid(), "알파-리놀렌산",
            UNIT_MG_2, productCalorie, 0f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getFaArachidonicAcid() {
        return makeNutritionData(
            afcMin.getFaArachidonicAcid(), feed.getFaArachidonicAcid(), "아라키돈산",
            UNIT_MG_2, productCalorie, 1f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getMiCaPh() {
        return makeNutritionDataMinMax(
            afco_mi_ca_ph_min_dog, afco_mi_ca_ph_max_dog, feed.getMiCaPh(), "칼슘:인",
            UNIT_ZERO, productCalorie, 16f, false, type_one);
    }

    @Override
    public NutritionDetailModelLegacy getOtTaurine() {
        return null;
    }


}
