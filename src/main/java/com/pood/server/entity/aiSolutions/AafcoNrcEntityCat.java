package com.pood.server.entity.aiSolutions;

import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.entity.meta.Feed;
import com.pood.server.entity.meta.Product;
import com.pood.server.entity.user.UserPet;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModelLegacy;

@Deprecated
public class AafcoNrcEntityCat extends AafcoNrcEntity {

    static int type_one = 1;
    static int type_ten = 10;

    static Double afco_cystine_plus_methionine_min_cat = 1d;
    static Double afco_phenylanlanine_plus_tyrosine_min_cat = 3.83d;

    float productCalorie = product.getCalorie();

    public AafcoNrcEntityCat(Product product, AafcoNrc afcMin, Feed feed, UserPet pet) {
        super(product, afcMin, feed, pet);
    }

    static final float variable = 1000f;

    @Override
    public NutritionDetailModelLegacy getCrudeProtein() {

        return makeNutritionData(
            afcMin.getPrProtein(), feed.getPrProtein(),
            "조단백", UNIT_G, productCalorie, 20f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getCrudeFat() {
        return makeNutritionData(
            afcMin.getPrFat(), feed.getPrFat(),
            "조지방", UNIT_G, productCalorie, 20f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getCalcium() {

        return makeNutritionData(
            afcMin.getMiCalcium(), 6.25d,
            "칼슘", UNIT_G, productCalorie, 20f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getMiPhosphours() {

        return makeNutritionData(
            afcMin.getMiPhosphours(), 4d,
            "인", UNIT_G, productCalorie, 20f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getMiMagnessium() {
        return makeNutritionData(
            afcMin.getMiMagnessium(), feed.getMiMagnessium(),
            "마그네슘", UNIT_G, productCalorie, 0.5f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getMiPotassium() {
        return makeNutritionData(
            afcMin.getMiPotassium(), feed.getMiPotassium(),
            "칼륨", UNIT_G, productCalorie, 0.5f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getMiCopper() {
        return makeNutritionData(
            afcMin.getMiCopper(), feed.getMiCopper(),
            "구리", UNIT_MG_2, productCalorie, 0.5f, false, type_one);
    }

    @Override
    public NutritionDetailModelLegacy getMiManganese() {
        return makeNutritionData(
            afcMin.getMiManganese(), feed.getMiManganese(),
            "망간", UNIT_MG_2, productCalorie, 0.5f, false, type_one);
    }

    @Override
    public NutritionDetailModelLegacy getMiIodine() {
        if (afcMin.getMiIodine() > 0 && feed.getMiIodine() > 0) {
            return null;
        }
        return makeNutritionDataMinMax(
            afcMin.getMiIodine(), 2.75f, feed.getMiIodine(), "요오드",
            UNIT_MG_2, productCalorie, 0.5f, false, type_one);
    }

    @Override
    public NutritionDetailModelLegacy getMiSelenium() {
        return makeNutritionData(
            afcMin.getMiSelenium(), feed.getMiSelenium(), "셀레늄", UNIT_MG_2,
            productCalorie, 0.5f, false, type_one);
    }

    @Override
    public NutritionDetailModelLegacy getViVitaminA() {
        if (afcMin.getViVitaminA() > 0 && feed.getViVitaminA() > 0) {
            return null;
        }
        return makeNutritionDataMinMax(
            afcMin.getViVitaminA(), 83325f, feed.getViVitaminA(),
            "비타민A", UNIT_IU_2, productCalorie, 1f, true, type_one);
    }

    @Override
    public NutritionDetailModelLegacy getViVitaminD() {
        if (afcMin.getViVitaminD() > 0 && feed.getViVitaminD() > 0) {
            return null;
        }
        return makeNutritionDataMinMax(
            afcMin.getViVitaminD(), 7520f, feed.getViVitaminD(),
            "비타민D", UNIT_IU_2, productCalorie, 1f, true, type_one
        );
    }

    @Override
    public NutritionDetailModelLegacy getViVitaminE() {
        return makeNutritionData(
            afcMin.getViVitaminE(), feed.getViVitaminE(), "비타민E", UNIT_IU_2,
            productCalorie, 0.8f, true, type_one);
    }

    @Override
    public NutritionDetailModelLegacy getAmMetCys() {
        return makeNutritionData(
            afco_cystine_plus_methionine_min_cat, feed.getAmMetCys(),
            "메티오닌+시스틴", UNIT_MG_2, productCalorie, 0.4f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getAmPheTyr() {
        return makeNutritionData(
            afco_phenylanlanine_plus_tyrosine_min_cat, feed.getAmPheTyr(),
            "페닐알라닌+티로신", UNIT_MG_2, productCalorie, 0.1f, false, type_ten
        );
    }

    @Override
    public NutritionDetailModelLegacy getAmMethionine() {
        if (afcMin.getAmMethionine() > 0 && feed.getAmMethionine() > 0) {
            return null;
        }
        return makeNutritionDataMinMax(
            afcMin.getAmMethionine(), 3.75f, feed.getAmMethionine(),
            "메티오닌", UNIT_MG_2, productCalorie, 0.4f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getAmTryptophan() {
        if (afcMin.getAmTryptophan() > 0 && feed.getAmTryptophan() > 0) {
            return null;
        }
        return makeNutritionDataMinMax(
            afcMin.getAmTryptophan(), 4.25f, feed.getAmTryptophan(),
            "트립토판", UNIT_MG_2, productCalorie, 0.1f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getFaALinoleicacid() {
        return makeNutritionData(
            afcMin.getFaALinoleicacid(), feed.getFaALinoleicacid(),
            "알파-리놀렌산", UNIT_MG_2, productCalorie, 1f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getFaArachidonicAcid() {
        return makeNutritionData(
            afcMin.getFaArachidonicAcid(), feed.getFaArachidonicAcid(), "아라키돈산",
            UNIT_MG_2, productCalorie, 0f, false, type_ten);
    }

    @Override
    public NutritionDetailModelLegacy getMiCaPh() {
        return null;
    }

    @Override
    public NutritionDetailModelLegacy getOtTaurine() {
        return makeNutritionData(
            afcMin.getOtTaurine(), feed.getOtTaurine(),
            "타우린", UNIT_G, productCalorie, 2f, false, type_ten);
    }


}
