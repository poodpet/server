package com.pood.server.entity.aiSolutions;


import com.pood.server.entity.TripleAndType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Deprecated
public abstract class PetDiseaseSolution {

    static final int ALLERGY = 501;
    static final int UROLITHIASIS_1 = 122;
    static final int UROLITHIASIS_2 = 123;
    static final int UROLITHIASIS_3 = 124;
    static final int BONE = 301;
    static final int SKIN = 421;
    static final int VITALITY = 961;
    static final int EYE = 201;
    static final int TEETH = 241;
    static final int DIET = 941;
    //질병그룹에서 활력과 눈은 제외할 것
    List<String> diseaseCommentText = new ArrayList<>();
    List<String> imageResource = Arrays.asList("balance", "cal", "mineral", "nut");

    //유전질병
    public TripleAndType create(
            String petName, int diseaseGroup
    ) {


        return new TripleAndType(diseaseComment(petName, diseaseGroup),
                iconImage(diseaseGroup),
                iconImageComment(diseaseGroup),
                converType(diseaseGroup));

    }

    String converType(int diseaseGroup) {
        String result = "";
        switch (diseaseGroup) {
            case ALLERGY:
                result = "Allergy";  //알러지
                break;
            case UROLITHIASIS_1:
                result = "Urolithiasis_1"; //배변/배뇨
                break;
            case UROLITHIASIS_2:
                result = "Urolithiasis_2"; //배변/배뇨
                break;
            case UROLITHIASIS_3:
                result = "Urolithiasis_3"; //배변/배뇨
                break;
            case BONE:
                result = "Bone"; //뼈
                break;
            case SKIN:
                result = "Skin"; // 피부
                break;
            case EYE:
                result = "Eye"; //눈
                break;
            case TEETH:
                result = "Teeth"; //치아
                break;
            case DIET:
                result = "diet"; //다이어트
                break;
            case VITALITY:
                result = "vitality"; //활력
                break;
        }

        return result;
    }

    ///코멘트 세팅    //알러지 ,배변, 뼈, 피부, 눈, 치아, 활력, 다이어트
    List<String> diseaseComment(String petName, int diseaseGroup) {
        List<String> resultList = new ArrayList<>();
        String comment = "";
        switch (diseaseGroup) {
            case ALLERGY:
                comment = getCommentText(0);
                break;
            case UROLITHIASIS_1:
            case UROLITHIASIS_2:
            case UROLITHIASIS_3:
                comment = getCommentText(1);
                break;
            case BONE:
                comment = getCommentText(2);
                break;
            case SKIN:
                comment = getCommentText(3);
                break;
            case EYE:
                comment = getCommentText(4);
                break;
            case TEETH:
                comment = getCommentText(5);
                break;
            case VITALITY:
                comment = getCommentText(6);
                break;
            case DIET:
                comment = getCommentText(7);
                break;

        }

        if (!comment.isEmpty()) {
            resultList.add(String.format(comment, petName));
        }

        return resultList;
    }

    //아이콘 이미지 설정
    List<String> iconImage(int diseaseGroup) {

        List<String> iconImageList = new ArrayList<>();
        switch (diseaseGroup) {
            case ALLERGY:
            case UROLITHIASIS_1:
            case UROLITHIASIS_2:
            case UROLITHIASIS_3:
            case EYE:
            case TEETH:
            case DIET:
            case BONE:
                iconImageList = Arrays.asList(imageResource.get(3), imageResource.get(3));
                break;
            case SKIN:
                iconImageList = Arrays.asList(imageResource.get(0), imageResource.get(3), imageResource.get(2));
                break;
            default:
                iconImageList = Arrays.asList(imageResource.get(0), imageResource.get(2), imageResource.get(3));
                break;
        }
        return iconImageList;
    }


    abstract List<String> iconImageComment(int diseaseGroup);

    private String getCommentText(int diseaseNumber) {
        return diseaseCommentText.get(diseaseNumber);
    }

    protected void setDiseaseCommentText(List<String> diseaseCommentText) {
        this.diseaseCommentText = diseaseCommentText;
    }
}
