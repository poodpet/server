package com.pood.server.entity.aiSolutions;

@Deprecated
public class StringValues {

    public String need_g = "최소필요 %.2fg";
    public String need_g_max = "최대허용 %.2fg";
    public String include_g = "%.2fg";
    public String need_mg = "최소필요 %.2fmg";
    public String need_mg_max = "최대허용 %.2fmg";
    public String include_mg = "%.2fmg";
    public String need_IU = "최소필요 %.3fIU";
    public String need_IU_max = "최대허용 %.3fIU";
    public String include_IU = "%.3fIU";
    public String need_IU_4 = "최소필요 %.4fIU";
    public String need_IU_4_max = "최대허용 %.4fIU";
    public String include_IU_4 = "%.4fIU";
    public String need_mg_kg = "최소필요 %.1fmg/kg";
    public String need_mg_kg_max = "최대허용 %.1fmg/kg";
    public String include_mg_kg = "%.1fmg/kg";
    public String need_iu_kg = "최소필요 %.1fIU/kg";
    public String need_iu_kg_max = "최소필요 %.1fIU/kg";
    public String include_iu_kg = "%.1fIU/kg";
    public String percent_n = "%.2f";
    public String percent = "%.2f%%";
    public String percent_string = "%s%";
    public String need_IU_round = "최소필요 %dIU";
    public String need_IU_round_max ="최대허용 %dIU";
    public String include_round = "%dIU";
}
