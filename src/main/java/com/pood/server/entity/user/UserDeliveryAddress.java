package com.pood.server.entity.user;

import com.pood.server.dto.user.delivery.UserDeliveryUpdateRequest;
import com.pood.server.entity.BaseIdxTimeEntity;
import com.pood.server.util.DeliveryRemoteType;
import com.pood.server.util.converter.DeliveryRemoteConverter;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user_delivery_address")
@EntityListeners(AuditingEntityListener.class)
public class UserDeliveryAddress extends BaseIdxTimeEntity {

    @ApiModelProperty(value = "배송지 주소")
    @Column(name = "address")
    private String address;

    @ApiModelProperty(value = "0: 기본 배송지 아님, 1: 기본 배송지")
    @Column(name = "default_type")
    private boolean defaultType;

    @ApiModelProperty(value = "배송지 상세 주소")
    @Column(name = "detail_address")
    private String detailAddress;

    @ApiModelProperty(value = "받는 사람")
    @Column(name = "name")
    private String name;

    @ApiModelProperty(value = "배송지 별칭(집, 회사) 등..")
    @Column(name = "nickname")
    private String nickname;

    @ApiModelProperty(value = "우편 번호")
    @Column(name = "zipcode")
    private String zipcode;

    @ApiModelProperty(value = "0: 검색 입력, 1: 직접 입력")
    @Column(name = "input_type")
    private boolean inputType;

    @ApiModelProperty(value = "0: 도서 산간 아님, 1: 제주도기본, 2: 제주도산간, 3:기타섬지역, 4: 산간")
    @Column(name = "remote_type")
    @Convert(converter = DeliveryRemoteConverter.class)
    private DeliveryRemoteType remoteType;

    @ApiModelProperty(value = "받는자 연락처")
    @Column(name = "phone_number")
    private String phoneNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_idx")
    private UserInfo userInfo;

    @Builder
    private UserDeliveryAddress(final String address,
        final boolean defaultType,
        final String detailAddress, final String name, final String nickname, final String zipcode,
        final boolean inputType,
        final DeliveryRemoteType remoteType, final String phoneNumber, final UserInfo userInfo) {
        this.address = address;
        this.defaultType = defaultType;
        this.detailAddress = detailAddress;
        this.name = name;
        this.nickname = nickname;
        this.zipcode = zipcode;
        this.inputType = inputType;
        this.remoteType = remoteType;
        this.phoneNumber = phoneNumber;
        this.userInfo = userInfo;
    }

    public void updateNotDefaultType() {
        this.defaultType = false;
    }

    public void updateDefaultType() {
        this.defaultType = true;
    }

    public void updateAll(final UserDeliveryUpdateRequest userDeliveryUpdateRequest,
        final UserInfo userInfo, final int remoteType) {
        this.address = userDeliveryUpdateRequest.getAddress();
        this.defaultType = userDeliveryUpdateRequest.getDefaultType() == 1;
        this.detailAddress = userDeliveryUpdateRequest.getDetailAddress();
        this.name = userDeliveryUpdateRequest.getName();
        this.nickname = userDeliveryUpdateRequest.getNickname();
        this.zipcode = userDeliveryUpdateRequest.getZipcode();
        this.remoteType = DeliveryRemoteType.ofLegacyType(remoteType);
        this.phoneNumber = userDeliveryUpdateRequest.getPhoneNumber();
        this.userInfo = userInfo;
    }
}
