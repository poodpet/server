package com.pood.server.entity.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user_noti")
public class UserNoti {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_info.idx")
    @Column(name = "user_idx")
    private Integer userIdx;

    @Column(name = "user_uuid")
    private String userUuid;

    @ApiModelProperty(value = "0:안 읽음, 1:읽음(기본 0으로 앱에서 읽었을 때 읽음 요청)")
    @Column(name = "noti_read")
    private Integer notiRead;

    @ApiModelProperty(value = "내용 e.g) 결제가 완료되었습니다")
    @Column(name = "noti_text")
    private String notiText;

    @ApiModelProperty(value = "제목")
    @Column(name = "noti_title")
    private String notiTitle;

    @ApiModelProperty(value = "스키마")
    @Column(name = "noti_scheme")
    private String notiScheme;

    @ApiModelProperty(value = "이미지 주소")
    @Column(name = "noti_url")
    private String notiUrl;

    @ApiModelProperty(value = "0:공지, 1:광고, 2:주문, 3:리뷰, 4:정기배송, 5:기타")
    @Column(name = "noti_status")
    private Integer notiStatus;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
