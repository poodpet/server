package com.pood.server.entity.user;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user_pet_image")
@EntityListeners(AuditingEntityListener.class)
public class UserPetImage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_pet.idx")
    @Column(name = "user_pet_idx")
    private Integer userPetIdx;

    @ApiModelProperty(value = "회원 동물 이미지 URL")
    @Column(name = "url")
    private String url;

    @ApiModelProperty(value = "0:기본, 1:등록자만, 2:안보임, 3:정책위반")
    @Column(name = "visible")
    private Integer visible;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    @LastModifiedDate
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth", updatable = false)
    @CreatedDate
    private LocalDateTime recordbirth;

    @Builder
    public UserPetImage(final Integer userPetIdx, final String url,
        final int visible) {
        this.userPetIdx = userPetIdx;
        this.url = url;
        this.visible = visible;
    }

    public void updateImageUrl(final String url) {
        this.url = url;
    }

}
