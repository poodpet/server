package com.pood.server.entity.user;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user_review")
@EntityListeners(AuditingEntityListener.class)
public class UserReview {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_info.idx")
    @Column(name = "user_idx")
    private Integer userIdx;

    @ApiModelProperty(value = "반려동물 항목 번호 : meta_db.pet.idx")
    @Column(name = "pet_idx")
    private Integer petIdx;

    @ApiModelProperty(value = "회원 항목 번호 : order_db.order.idx")
    @Column(name = "order_idx")
    private Integer orderIdx;

    @ApiModelProperty(value = "굿즈 항목 번호 : order_db.order.order_number")
    @Column(name = "order_number")
    private String orderNumber;

    @ApiModelProperty(value = "굿즈 항목 번호 : order_db.goods.idx")
    @Column(name = "goods_idx")
    private Integer goodsIdx;

    @ApiModelProperty(value = "굿즈 항목 이름 : order_db.goods.goods_name")
    @Column(name = "goods_name")
    private String goodsName;

    @ApiModelProperty(value = "상품 항목 이름 : meta_db.product.idx")
    @Column(name = "product_idx")
    private Integer productIdx;

    @ApiModelProperty(value = "5점 만점")
    @Column(name = "rating")
    private Integer rating;

    @ApiModelProperty(value = "리뷰 텍스트")
    @Column(name = "review_text")
    private String reviewText;

    @ApiModelProperty(value = "수정할 때마다 카운트 차감, 0이 되면 수정 불가. 삭제만 가능")
    @Column(name = "update_count")
    private Integer updateCount;

    @ApiModelProperty(value = "0:기본, 1:삭제됨(목록에 안보임)")
    @Column(name = "isDelete")
    private Integer isDelete;

    @ApiModelProperty(value = "0:기본, 1:등록자만 보임, 2:안보임, 3:정책위반")
    @Column(name = "isVisible")
    private Integer isVisible;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    @LastModifiedDate
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    @CreatedDate
    private LocalDateTime recordbirth;

    @Builder
    public UserReview(final Integer userIdx, final Integer petIdx,
        final String orderNumber, final Integer goodsIdx,
        final Integer rating, final String reviewText) {
        this.userIdx = userIdx;
        this.petIdx = petIdx;
        this.orderNumber = orderNumber;
        this.goodsIdx = goodsIdx;
        this.rating = rating;
        this.reviewText = reviewText;
    }

    public boolean eqOrderNumber(final String orderNumber) {
        return this.orderNumber.equals(orderNumber);
    }

    public boolean eqGoodsIdx(final int goodsIdx) {
        return this.goodsIdx.equals(goodsIdx);
    }

    public void modifyReviewText(final String reviewText){
        this.reviewText = reviewText;
    }
}
