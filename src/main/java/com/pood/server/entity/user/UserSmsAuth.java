package com.pood.server.entity.user;

import com.pood.server.exception.AlreadyChangedException;
import com.pood.server.exception.NumberNotMatchException;
import com.pood.server.exception.UsageTimeExpiredException;
import io.swagger.annotations.ApiModelProperty;
import java.time.Duration;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user_sms_auth")
public class UserSmsAuth {

    private static final int LIMIT_SECONDS = 180;
    private static final int SUCCESS_STATUS = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "핸드폰 번호")
    @Column(name = "phone_number")
    private String phoneNumber;

    @ApiModelProperty(value = "인증 번호")
    @Column(name = "sms_auth")
    private String smsAuth;

    @ApiModelProperty(value = "0:발행, 1:인증 성공")
    @Column(name = "sms_status")
    private Integer smsStatus;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    public void isNotExpired() {
        final Duration diff = Duration.between(recordbirth, LocalDateTime.now());
        if (diff.getSeconds() > LIMIT_SECONDS) {
            throw new UsageTimeExpiredException("인증 시간이 초과되었습니다.");
        }
    }

    public void updateAuthenticationStatus() {
        this.smsStatus = SUCCESS_STATUS;
    }

    public void authenticationCheck(final String authNumber) {
        sameNumberCheck(authNumber);
        alreadySuccessStatus();
    }

    private void sameNumberCheck(final String authNumber) {
        if (!smsAuth.equals(authNumber)) {
            throw new NumberNotMatchException();
        }
    }

    private void alreadySuccessStatus() {
        if (smsStatus == SUCCESS_STATUS) {
            throw new AlreadyChangedException();
        }
    }

}
