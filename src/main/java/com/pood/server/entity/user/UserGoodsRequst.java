package com.pood.server.entity.user;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user_goods_request")
public class UserGoodsRequst {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Long idx;

    @ManyToOne
    @JoinColumn(name = "user_info_idx", insertable = true, updatable = false)
    private UserInfo userInfo;

    @ApiModelProperty(value = "meta_db.goods.idx")
    @Column(name = "goods_idx")
    private Integer goodsIdx;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @CreatedDate
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    public UserGoodsRequst(final Long idx, final UserInfo userInfo, final Integer goodsIdx) {
        this.idx = idx;
        this.userInfo = userInfo;
        this.goodsIdx = goodsIdx;
    }

    public static UserGoodsRequst create(final UserInfo userInfo, final Integer goodsIdx) {
        return new UserGoodsRequst(null, userInfo, goodsIdx);
    }

}
