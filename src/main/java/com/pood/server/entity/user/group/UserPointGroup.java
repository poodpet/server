package com.pood.server.entity.user.group;

import com.pood.server.entity.user.UserPoint;
import java.util.List;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;

@Value
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class UserPointGroup {

    List<UserPoint> userPoints;

    public static UserPointGroup of(final List<UserPoint> userPoints) {
        return new UserPointGroup(userPoints);
    }

    public UserPoint getUserPoint(final String pointUuid) {
        return this.userPoints.stream()
            .filter(userPoint -> userPoint.eqPointUuid(pointUuid))
            .findFirst()
            .orElseThrow(() -> new IllegalArgumentException("올바르지 않은 포인트 번호입니다."));
    }

}
