package com.pood.server.entity.user;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user_review_image")
@EntityListeners(AuditingEntityListener.class)
public class UserReviewImage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "order_idx")
    private Integer orderIdx;

    @Column(name = "order_number")
    private String orderNumber;

    @Column(name = "url")
    private String url;

    @Column(name = "visible")
    private Integer visible;

    @Column(name = "seq")
    private Integer seq;

    @Column(name = "updatetime")
    @LastModifiedDate
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    @CreatedDate
    private LocalDateTime recordbirth;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "review_idx")
    private UserReview userReview;

    @Builder
    public UserReviewImage(final String url, final Integer seq, final UserReview userReview) {
        this.url = url;
        this.seq = seq;
        this.userReview = userReview;
    }


}
