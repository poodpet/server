package com.pood.server.entity.user;

import com.pood.server.controller.request.userpet.UserPetCreateDto;
import com.pood.server.controller.request.userpet.UserPetUpdateRequest;
import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.exception.IndexNotMatchException;
import com.pood.server.exception.NotFoundException;
import com.pood.server.exception.NumberCreationException;
import com.pood.server.service.pet_solution.UserPetCalorieCalc;
import com.pood.server.util.PetCategoryName;
import com.pood.server.util.strategy.NumberStrategy;
import com.pood.server.util.strategy.SerialNumberStrategy;
import com.pood.server.util.strategy.SliceStrategy;
import com.pood.server.util.strategy.YearSliceStrategy;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user_pet")
@EntityListeners(AuditingEntityListener.class)
public class UserPet {

    private static final int LIMIT_YEAR_LENGTH = 2;
    private static final int MIN_NUMBER = 100_000;
    private static final int NUMBER_LIMIT = 999_999;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_info.idx")
    @Column(name = "user_idx")
    private Integer userIdx;

    @ApiModelProperty(value = "사용 안함")
    @Column(name = "allergy")
    private String allergy;

    @ApiModelProperty(value = "사용 안함 곡물 사이즈 테이블이 따로 존재(user_pet_grain_size.idx)")
    @Column(name = "grain_size")
    private Integer grainSize;

    @ApiModelProperty(value = "반려동물 활동량")
    @Column(name = "pet_activity")
    private Integer petActivity;

    @ApiModelProperty(value = "반려동물 항목 번호 : meta_db.pet_category.idx")
    @Column(name = "pc_id")
    private Integer pcId;

    @ApiModelProperty(value = "meta_db.pet.idx")
    @Column(name = "psc_id")
    private Integer pscId;

    @ApiModelProperty(value = "반려동물 이름")
    @Column(name = "pet_name")
    private String petName;

    @ApiModelProperty(value = "반려동물 생일")
    @Column(name = "pet_birth")
    private LocalDate petBirth;

    @ApiModelProperty(value = "반려동물 성별 (중성화 수컷 3, 암컷 4)")
    @Column(name = "pet_gender")
    private Integer petGender;

    @ApiModelProperty(value = "반려동물 무게")
    @Column(name = "pet_weight")
    private Double petWeight;

    @ApiModelProperty(value = "0:기본, 1:등록자만, 2:안보임, 3:죽음")
    @Column(name = "pet_status")
    private Integer petStatus;

    @ApiModelProperty(value = "펫의 고유 번호")
    @Column(name = "serial_number")
    private String serialNumber;

    @ApiModelProperty(value = "user_db.user_pet_weight_diary join")
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "userPet", orphanRemoval = true)
    private List<UserPetWeightDiary> userPetWeightDiaryList = new ArrayList<>();

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    @LastModifiedDate
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    @CreatedDate
    private LocalDateTime recordbirth;

    @Transient
    AafcoNrc aafcoNrc;

    @Transient
    ArrayList<UserPetGrainSize> unitSizeList;

    @Transient
    ArrayList<UserPetAllergy> allergyData;

    @Builder
    public UserPet(final Integer userIdx, final String allergy, final Integer grainSize,
        final Integer petActivity,
        final Integer pcId, final Integer pscId, final String petName, final LocalDate petBirth,
        final Integer petGender,
        final Integer petStatus) {
        this.userIdx = userIdx;
        this.allergy = allergy;
        this.grainSize = grainSize;
        this.petActivity = petActivity;
        this.pcId = pcId;
        this.pscId = pscId;
        this.petName = petName;
        this.petBirth = petBirth;
        this.petGender = petGender;
        this.petWeight = null;
        this.petStatus = petStatus;
        this.serialNumber = serialNumber(new YearSliceStrategy(), new SerialNumberStrategy());
    }

    public void setUnitSizeList(ArrayList<UserPetGrainSize> unitSizeList) {
        this.unitSizeList = unitSizeList;
    }

    public void setAllergyData(ArrayList<UserPetAllergy> allergyData) {
        this.allergyData = allergyData;
    }

    public void setPetWeightDiaryLists(final List<UserPetWeightDiary> userPetWeightDiaryList) {
        this.userPetWeightDiaryList = userPetWeightDiaryList;
    }

    public void setAafcoNrc(AafcoNrc aafcoNrc) {
        this.aafcoNrc = aafcoNrc;
    }

    public ArrayList<UserPetAllergy> getAllergyList() {
        return allergyData;
    }

    public ArrayList<UserPetGrainSize> getGrainSizeList() {
        return unitSizeList;
    }

    public static UserPet toEntity(final UserInfo userInfo,
        final UserPetCreateDto userPetCreateDto) {
        return UserPet.builder()
            .userIdx(userInfo.getIdx())
            .allergy(null)
            .grainSize(null)
            .pcId(userPetCreateDto.getPcId())
            .pscId(userPetCreateDto.getPscId())
            .petActivity(userPetCreateDto.getPetActivity())
            .petBirth(userPetCreateDto.getPetBirth())
            .petGender(userPetCreateDto.getPetGender())
            .petStatus(userPetCreateDto.getPetStatus())
            .petName(userPetCreateDto.getPetName())
            .build();
    }

    public void updatePetNameByDto(final UserPetUpdateRequest userPetUpdateRequest) {
        this.petName = userPetUpdateRequest.getPetName();
        this.petGender = userPetUpdateRequest.getPetGender();
    }

    public String serialNumber(final SliceStrategy sliceStrategy,
        final NumberStrategy numberStrategy) {
        final String year = sliceStrategy.sliceNumber();
        final int randomNumber = numberStrategy.makeNumber();
        if (isTwoDigits(year)) {
            throw new NumberCreationException("년도는 2자리여야만 합니다.");
        }

        if (isSixDigits(randomNumber)) {
            throw new NumberCreationException("랜덤숫자는 6자리만 가능합니다.");
        }

        return year + "-" + randomNumber;
    }

    private boolean isTwoDigits(final String year) {
        return year.length() != LIMIT_YEAR_LENGTH;
    }

    private boolean isSixDigits(final int randomNumber) {
        return randomNumber > NUMBER_LIMIT || randomNumber < MIN_NUMBER;
    }

    public void petOwnerIsCorrectValid(final int userIdx) {
        if (this.userIdx != userIdx) {
            throw new IndexNotMatchException("해당하는 유저의 펫이 아닙니다.");
        }
    }

    public long getAgeMonth() {
        return ChronoUnit.MONTHS.between(this.petBirth, LocalDate.now());
    }

    public float getPetCalorie(final UserPetCalorieCalc userPetCalc) {
        return userPetCalc.getPetCalorie(getAgeMonth(), getNowPetWeight(),
            userPetCalc.getPetWeight(this));
    }


    public boolean isCat() {
        return pcId.equals(2);
    }

    public boolean isDog() {
        return pcId.equals(1);
    }

    public boolean isNeutering() {
        return petGender.equals(3) || petGender.equals(4);
    }

    public PetCategoryName getPetGrowthState() {
        return PetCategoryName.petGrowthState(getAgeMonth(), isDog());
    }

    public boolean isPetTypeCatAndIsNeutering() {
        return isCat() && isNeutering();
    }

    public boolean isPetTypeCatAndIsNotNeutering() {
        return isCat() && !isNeutering();
    }

    public boolean isPetTypeDogAndIsNotNeutering() {
        return isDog() && !isNeutering();
    }

    public boolean isPetTypeDogAndIsNeutering() {
        return isDog() && isNeutering();
    }

    public float dogBasicWeight() {
        final long month = getAgeMonth();
        if (month <= 4L) {
            return 210f;
        }

        if (month <= 7L) {
            return 170f;
        }

        if (month <= 9L) {
            return 140f;
        }

        if (month <= 11L) {
            return 122.5f;
        }

        if (month < 96L) {
            //1: 남아, 2: 여아, 3 : 남아 중성화, 4: 여아 중성화
            if (!isNeutering()) {
                return 112f;
            }
        }

        return 98f;
    }

    public float catBasicWeight() {
        long month = getAgeMonth();
        if (month <= 1) {
            return 240f;
        }

        if (month == 2) {
            return 210f;
        }

        if (month == 3) {
            return 200f;
        }

        if (month == 4) {
            return 175f;
        }

        if (month == 5) {
            return 145f;
        }

        if (month == 6) {
            return 135f;
        }

        if (month == 7) {
            return 120f;
        }

        if (month == 8) {
            return 110f;
        }

        if (month == 9) {
            return 100f;
        }

        if (month == 10) {
            return 95f;
        }

        if (month == 11) {
            return 90f;
        }

        if (month == 12) {
            return 85f;
        }

        //1: 남아, 2: 여아, 3 : 남아 중성화, 4: 여아 중성화
        // 13개월 지나고
        if (!isNeutering()) {
            return 77.6f;
        }

        return 62f;
    }

    public void deleteWeightByIdx(final long idx) {
        UserPetWeightDiary userPetWeightDiary = userPetWeightDiaryList.stream()
            .filter(diary -> diary.eqIdx(idx)).findFirst()
            .orElseThrow(() -> new NotFoundException("해당하는 몸무게가 존재하지 않습니다."));
        userPetWeightDiaryList.remove(userPetWeightDiary);
    }

    public float getNowPetWeight() {
        return userPetWeightDiaryList.stream()
            .sorted(Comparator.comparing(UserPetWeightDiary::getBaseDate).reversed())
            .map(UserPetWeightDiary::getWeight)
            .findAny()
            .orElseThrow(() -> new NotFoundException("몸무게 정보를 찾을 수 없습니다."));
    }

}
