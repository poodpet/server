package com.pood.server.entity.user;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "admin_user")
public class AdminUser {

    public static AdminUser EMPTY = new AdminUser(null,null,null);

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "uuid")
    private String uuid;

    @Column(name = "name")
    private String name;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    @Column(name = "nick_name")
    private String nickName;

    @Column(name = "user_grade")
    private String userGrade;

    @Column(name = "password")
    private String password;

    @Column(name = "auth")
    private Integer auth;

    @Column(name = "type")
    private Integer type;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;


    public AdminUser(String email, String password, String name) {
        this.email = email;
        this.password = password;
        this.name = name;
    }

    public AdminUser(Integer idx, String email, String password, String name) {
        this.idx = idx;
        this.email = email;
        this.password = password;
        this.name = name;
    }

    public Boolean isEmpty(){
        return this.getEmail() == null;
    }


}
