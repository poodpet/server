package com.pood.server.entity.user;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "user_event_custom_block")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EntityListeners(AuditingEntityListener.class)
public class UserEventCustomBlock {

    @Id
    @Column(name = "idx", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "event_info_idx")
    private Integer eventInfoIdx;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_info_idx")
    private UserInfo userInfo;

    @Column(name = "block_message", nullable = false, length = 50)
    private String blockMessage;

    //내 계정의 idx
    @Column(name = "my_info_idx")
    private int myInfoIdx;

    @Column(name = "record_birth")
    @CreatedDate
    private LocalDateTime recordBirth;

    @Builder(access = AccessLevel.PACKAGE)
    private UserEventCustomBlock(final Integer eventInfoIdx, final UserInfo userInfo, final String blockMessage, final int myInfoIdx) {
        this.eventInfoIdx = eventInfoIdx;
        this.userInfo = userInfo;
        this.blockMessage = blockMessage;
        this.myInfoIdx = myInfoIdx;
    }

    public static UserEventCustomBlock of(final Integer eventInfoIdx, final UserInfo userInfo, final String blockMessage,
        final int myInfoIdx) {
        return UserEventCustomBlock.builder()
            .eventInfoIdx(eventInfoIdx)
            .userInfo(userInfo)
            .blockMessage(blockMessage)
            .myInfoIdx(myInfoIdx)
            .build();
    }

}