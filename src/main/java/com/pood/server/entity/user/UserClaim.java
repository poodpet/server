package com.pood.server.entity.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user_claim")
public class UserClaim {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_info.user_idx")
    @Column(name = "user_idx")
    private Integer userIdx;

    @Column(name = "user_uuid")
    private String userUuid;

    @ApiModelProperty(value = "0:영양학 상담, 1:상품 문의, 2:배송문의 3:푸드 칭찬하기 4:시스템 개선")
    @Column(name = "claim_type")
    private Integer claimType;

    @ApiModelProperty(value = "문의내용")
    @Column(name = "claim_text")
    private String claimText;

    @ApiModelProperty(value = "0: 일반, 1:비밀글(주로 상품딜 문의에서 사용)")
    @Column(name = "visible")
    private Integer visible;

    @ApiModelProperty(value = "0: 답변 안됨, 1:답변 됨")
    @Column(name = "isAnswer")
    private Integer isAnswer;

    @ApiModelProperty(value = "문의 제목")
    @Column(name = "claim_title")
    private String claimTitle;

    @ApiModelProperty(value = "문의 답변")
    @Column(name = "claim_answer")
    private String claimAnswer;

    @ApiModelProperty(value = "문의 굿즈 이름")
    @Column(name = "claim_goods_name")
    private String claimGoodsName;

    @ApiModelProperty(value = "문의 굿즈 항목 번호")
    @Column(name = "claim_goods_idx")
    private Integer claimGoodsIdx;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
