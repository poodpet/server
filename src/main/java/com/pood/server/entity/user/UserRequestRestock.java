package com.pood.server.entity.user;

import com.pood.server.util.RestockState;
import com.pood.server.util.converter.RestockStateConverter;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@ToString
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Table(name = "user_request_restock")
public class UserRequestRestock {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    private Long idx;

    @Column(name = "goods_idx")
    @ApiModelProperty(value = "meta_db.idx")
    private Integer goodsIdx;

    @Column(name = "user_uuid")
    @ApiModelProperty(value = "user_db.user_uuid")
    private String userUuid;

    @Convert(converter = RestockStateConverter.class)
    @ApiModelProperty(value = "재입고 요청 상태 (WAIT, DONE)")
    private RestockState state;

    @CreatedDate
    @Column(name = "record_birth")
    private LocalDateTime recordBirth;

    @LastModifiedDate
    @Column(name = "update_time")
    private LocalDateTime updateTime;

    @Builder
    private UserRequestRestock(final Long idx, final Integer goodsIdx, final String userUuid,
        final RestockState state, final LocalDateTime recordBirth, final LocalDateTime updateTime) {
        this.idx = idx;
        this.goodsIdx = goodsIdx;
        this.userUuid = userUuid;
        this.state = state;
        this.recordBirth = recordBirth;
        this.updateTime = updateTime;
    }

    public static UserRequestRestock create(final int goodsIdx, final String userUuid) {
        return UserRequestRestock.builder()
            .goodsIdx(goodsIdx)
            .userUuid(userUuid)
            .state(RestockState.WAIT)
            .build();
    }
}
