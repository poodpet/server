package com.pood.server.entity.user;

import com.pood.server.util.converter.BodyShapeConverter;
import com.pood.server.util.date.DateUtil;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@Builder
@Table(name = "user_pet_body_shape_diary")
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor
@AllArgsConstructor
public class UserPetBodyShapeDiary {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Long idx;

    @ManyToOne
    @JoinColumn(name = "user_pet_idx")
    private UserPet userPet;

    @Column(name = "body_shape")
    @Convert(converter = BodyShapeConverter.class)
    private BodyShape bodyShape;

    @Column(name = "base_date")
    private LocalDate baseDate;

    @Column(name = "update_time")
    @LastModifiedDate
    private LocalDateTime updateTime;

    @Column(name = "record_birth")
    @CreatedDate
    private LocalDateTime recordBirth;

    public static UserPetBodyShapeDiary of(final UserPet userPet, final BodyShape bodyShape) {
        return UserPetBodyShapeDiary.builder()
            .userPet(userPet)
            .bodyShape(bodyShape)
            .baseDate(
                LocalDate.now().plusDays(
                    DateUtil.of(LocalDate.now()).getCompareDayOfWeekNumber(DayOfWeek.FRIDAY)))
            .build();
    }

    public static UserPetBodyShapeDiary empty() {
        return UserPetBodyShapeDiary.builder().build();
    }

    public void updateBodyShape(final BodyShape bodyShape) {
        this.bodyShape = bodyShape;
    }
}
