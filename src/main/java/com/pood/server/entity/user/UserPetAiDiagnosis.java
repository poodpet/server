package com.pood.server.entity.user;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user_pet_ai_diagnosis")
public class UserPetAiDiagnosis {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_pet.idx")
    @Column(name = "user_pet_idx")
    private Integer userPetIdx;

    @ApiModelProperty(value = "사용 x → 알러지 항목 번호 : meta_db.ai_recommend_diagnosis.idx")
    @Column(name = "ai_diagnosis_idx")
    private Integer aiDiagnosisIdx;

    @Column(name = "ard_name")
    private String ardName;

    @Column(name = "ard_group")
    private String ardGroup;

    @Column(name = "ard_group_code")
    private Integer ardGroupCode;

    @Column(name = "pet_worry_idx")
    @ApiModelProperty(value = "meta_db.pet_worry의 idx")
    private Long petWorryIdx;

    @Column(name = "priority")
    private Integer priority;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private String recordbirth;

    @Builder
    public UserPetAiDiagnosis(final Integer idx, final Integer userPetIdx,
        final Integer aiDiagnosisIdx,
        final String ardName, final String ardGroup, final Integer ardGroupCode,
        final Long petWorryIdx, final Integer priority) {
        this.idx = idx;
        this.userPetIdx = userPetIdx;
        this.aiDiagnosisIdx = aiDiagnosisIdx;
        this.ardName = ardName;
        this.ardGroup = ardGroup;
        this.ardGroupCode = ardGroupCode;
        this.petWorryIdx = petWorryIdx;
        this.priority = priority;
    }

    @PrePersist
    void onPrePersist() {
        this.recordbirth = LocalDateTime.now()
            .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    public void setArdGroupCode(Integer ardGroupCode) {
        this.ardGroupCode = ardGroupCode;
    }

    public boolean eqWorryIdx(final Long petWorryIdx){
        return this.petWorryIdx.equals(petWorryIdx);
    }
}
