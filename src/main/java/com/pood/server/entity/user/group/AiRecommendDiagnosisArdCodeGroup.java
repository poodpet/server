package com.pood.server.entity.user.group;

import com.pood.server.entity.user.BodyShape;
import java.util.List;
import lombok.Value;

@Value
public class AiRecommendDiagnosisArdCodeGroup {

    List<Integer> ardGroupCodeList;

    public boolean isConflictDiagnosisByBodyShape(final BodyShape bodyShape) {
        if (BodyShape.NORMAL.equals(bodyShape)) {
            return false;
        }

        return ardGroupCodeList.contains(bodyShape.getReverseArdGroupCode());
    }

    public boolean isVisibleSelectPetWorry(final BodyShape bodyShape) {
        if (BodyShape.NORMAL.equals(bodyShape)) {
            return isExistWeightWorry();
        }

        return isNotExistWeightWorry();
    }

    private boolean isExistWeightWorry() {
        return ardGroupCodeList.stream()
            .anyMatch(ardCode -> ardCode.equals(BodyShape.SKINNY.getArdGroupCode()) ||
                ardCode.equals(BodyShape.OBESITY.getArdGroupCode()) ||
                ardCode.equals(BodyShape.HIGHLY_OBESE.getArdGroupCode())
            );
    }

    private boolean isNotExistWeightWorry() {
        return ardGroupCodeList.stream()
            .noneMatch(ardCode -> ardCode.equals(BodyShape.SKINNY.getArdGroupCode()) ||
                ardCode.equals(BodyShape.OBESITY.getArdGroupCode()) ||
                ardCode.equals(BodyShape.HIGHLY_OBESE.getArdGroupCode())
            );
    }

}
