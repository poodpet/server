package com.pood.server.entity.user;

import com.pood.server.controller.request.user.UserInfoUpdateRequest;
import com.pood.server.exception.PasswordNotMatchException;
import com.pood.server.util.UserStatus;
import com.pood.server.util.converter.UserStatusConverter;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EntityListeners(AuditingEntityListener.class)
@Table(name = "user_info")
public class UserInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "uuid")
    @Column(name = "user_uuid")
    private String userUuid;

    @ApiModelProperty(value = "회원 닉네임")
    @Column(name = "user_nickname")
    private String userNickname;

    @ApiModelProperty(value = "회원 이메일")
    @Column(name = "user_email")
    private String userEmail;

    @ApiModelProperty(value = "회원 비밀번호")
    @Column(name = "user_password")
    private String userPassword;

    @ApiModelProperty(value = "가입시 동의하지 않으면 가입 안됨")
    @Column(name = "user_service_agree")
    private Integer userServiceAgree;

    @ApiModelProperty(value = "0:정상, 2:정책위반로그인불가, 3:휴면계정, 5:탈퇴예정, 6:탈퇴")
    @Column(name = "user_status")
    @Convert(converter = UserStatusConverter.class)
    private UserStatus userStatus;

    @ApiModelProperty(value = "회원 이름")
    @Column(name = "user_name")
    private String userName;

    @Column(name = "user_point")
    private Integer userPoint;

    @ApiModelProperty(value = "회원 전화번호")
    @Column(name = "user_phone")
    private String userPhone;

    @ApiModelProperty(value = "회원 추천인 코드")
    @Column(name = "referral_code")
    private String referralCode;

    @ApiModelProperty(value = "주문 알림 수신 동의 0:미동의 1:동의")
    @Column(name = "order_push")
    private Boolean orderPush;

    @ApiModelProperty(value = "주문 알림 수신 동의 날짜/시각")
    @Column(name = "order_push_cg_time")
    private LocalDateTime orderPushCgTime;

    @ApiModelProperty(value = "꿀혜택 알림 수신 동의 0:미동의 1:동의")
    @Column(name = "pood_push")
    private Boolean poodPush;

    @ApiModelProperty(value = "꿀혜택 알림 수신 동의 날짜/시각")
    @Column(name = "pood_push_cg_time")
    private LocalDateTime poodPushCgTime;

    @ApiModelProperty(value = "고객 센터 알림 수신 동의 0:미동의 1:동의")
    @Column(name = "service_push")
    private Boolean servicePush;

    @ApiModelProperty(value = "고객 센터 알림 수신 동의 날짜/시각")
    @Column(name = "service_push_cg_time")
    private LocalDateTime servicePushCgTime;

    @ApiModelProperty(value = "디바이스 키")
    @Column(name = "device_key")
    private String deviceKey;

    @ApiModelProperty(value = "A:ANDROID, I:IOS")
    @Column(name = "device_type")
    private Integer deviceType;

    @Column(name = "token_arn")
    private String tokenArn;

    @ApiModelProperty(value = "멤버쉽 이름")
    @Column(name = "ml_name")
    private String mlName;

    @ApiModelProperty(value = "멤버쉽 적립포인트 : 추가 적립율(1%, 2%, 3%)")
    @Column(name = "ml_rate")
    private Double mlRate;

    @ApiModelProperty(value = "멤버쉽 적립 금액")
    @Column(name = "ml_price")
    private Integer mlPrice;

    @ApiModelProperty(value = "멤버쉽 적용되는 개월 수")
    @Column(name = "ml_month")
    private Integer mlMonth;

    @ApiModelProperty(value = "유저의 이용현황을 변경시 날짜 등록")
    @Column(name = "status_update_date")
    private LocalDateTime statusUpdateDate;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @LastModifiedDate
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @CreatedDate
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_base_image_idx", insertable = true, updatable = true)
    private UserBaseImage userBaseImage;

    @Column(name = "dog_pood_push")
    private boolean dogPoodPush = true;

    @Column(name = "dog_pood_push_cg_time")
    private LocalDateTime dogPoodPushCgTime;

    @Column(name = "cat_pood_push")
    private boolean catPoodPush = true;

    @Column(name = "cat_pood_push_cg_time")
    private LocalDateTime catPoodPushCgTime;

    @Column(name = "ad_agreement")
    private boolean adAgreement = false;

    @Column(name = "ad_agreement_cg_time")
    private LocalDateTime adAgreementCgTime;

    @Builder
    public UserInfo(final Integer idx, final String userUuid, final String userNickname,
        final String userEmail,
        final String userPassword, final Integer userServiceAgree, final UserStatus userStatus,
        final String userName,
        final Integer userPoint, final String userPhone, final String referralCode,
        final Boolean orderPush,
        final LocalDateTime orderPushCgTime, final Boolean poodPush,
        final LocalDateTime poodPushCgTime,
        final Boolean servicePush, final LocalDateTime servicePushCgTime, final String deviceKey,
        final Integer deviceType, final String tokenArn, final String mlName, final Double mlRate,
        final Integer mlPrice,
        final Integer mlMonth, final LocalDateTime statusUpdateDate, final LocalDateTime updatetime,
        final LocalDateTime recordbirth, final UserBaseImage userBaseImage,
        final Boolean dogPoodPush,
        final LocalDateTime dogPoodPushCgTime, final Boolean catPoodPush,
        final LocalDateTime catPoodPushCgTime,
        final Boolean adAgreement, final LocalDateTime adAgreementCgTime) {
        this.idx = idx;
        this.userUuid = userUuid;
        this.userNickname = userNickname;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
        this.userServiceAgree = userServiceAgree;
        this.userStatus = userStatus;
        this.userName = userName;
        this.userPoint = userPoint;
        this.userPhone = userPhone;
        this.referralCode = referralCode;
        this.orderPush = orderPush;
        this.orderPushCgTime = orderPushCgTime;
        this.poodPush = poodPush;
        this.poodPushCgTime = poodPushCgTime;
        this.servicePush = servicePush;
        this.servicePushCgTime = servicePushCgTime;
        this.deviceKey = deviceKey;
        this.deviceType = deviceType;
        this.tokenArn = tokenArn;
        this.mlName = mlName;
        this.mlRate = mlRate;
        this.mlPrice = mlPrice;
        this.mlMonth = mlMonth;
        this.statusUpdateDate = statusUpdateDate;
        this.updatetime = updatetime;
        this.recordbirth = recordbirth;
        this.userBaseImage = userBaseImage;
        this.dogPoodPush = Objects.nonNull(dogPoodPush) && dogPoodPush;
        this.dogPoodPushCgTime = dogPoodPushCgTime;
        this.catPoodPush = Objects.nonNull(catPoodPush) && catPoodPush;
        this.catPoodPushCgTime = catPoodPushCgTime;
        this.adAgreement = Objects.nonNull(adAgreement) && adAgreement;
        this.adAgreementCgTime = adAgreementCgTime;
    }

    public void setUserBaseImage(final UserBaseImage userBaseImage) {
        this.userBaseImage = userBaseImage;
    }

    public void softDelete() {
        this.deviceKey = null;
        this.tokenArn = null;
    }

    public UserInfo isMatchedPassword(final String inputPassword) {
        if (!this.userPassword.equals(inputPassword)) {
            throw new PasswordNotMatchException();
        }
        return this;
    }

    public void changePassword(final String password) {
        this.userPassword = password;
    }

    public void updateInfo(final UserInfoUpdateRequest request) {
        final String userNickName = request.getUserNickName();
        final String userPhone = request.getUserPhone();

        if (Objects.nonNull(userNickName)) {
            this.userNickname = userNickName;
        }

        if (Objects.nonNull(userPhone)) {
            this.userPhone = userPhone;
        }
    }

    public void toResignUser() {
        this.userStatus = UserStatus.TO_RESIGN;
        this.statusUpdateDate = LocalDateTime.now();
    }

    public Long getUserBaseImageIdx() {
        return this.getUserBaseImage().getIdx();
    }

    public String getUserBaseImageUrl() {
        return this.getUserBaseImage().getUrl();
    }

    public boolean currentPasswordValidate(final String requestPassword) {
        if (!userPassword.equals(requestPassword)) {
            throw new PasswordNotMatchException("현재 비밀번호가 서로 일치하지 않습니다.");
        }

        return true;
    }

    public void addUserPoint(final int retrievePoint) {
        userPoint += retrievePoint;
    }

    public void returnActive() {
        userStatus = UserStatus.ACTIVE;
    }

    public void doDormant() {
        userStatus = UserStatus.DORMANT_ACCOUNT;
    }
}
