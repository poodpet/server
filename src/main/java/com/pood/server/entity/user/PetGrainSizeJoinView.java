package com.pood.server.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Immutable;

@Getter
@Entity
@Table(name = "pet_grain_size_join_view")
@Immutable
@NoArgsConstructor
public class PetGrainSizeJoinView {

    @Id
    private Integer idx;

    @Column(name = "user_pet_idx")
    private int userPetIdx;

    @Column(name = "grain_size_idx")
    private int grainSizeIdx;

    private String name;

    @Column(name = "size_min")
    private double sizeMin;

    @Column(name = "size_max")
    private double sizeMax;

    @Column(name = "recordbirth")
    private String recordBirth;
}
