package com.pood.server.entity.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user_fcm_token")
public class UserFcmToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "user_idx")
    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_info.idx")
    private Integer userIdx;

    @ApiModelProperty(value = "디바이스 키")
    @Column(name = "device_key")
    private String deviceKey;

    @ApiModelProperty(value = "A:ANDROID, I:IOS")
    @Column(name = "device_type")
    private Integer deviceType;

    @Column(name = "token_arn")
    private String tokenArn;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
