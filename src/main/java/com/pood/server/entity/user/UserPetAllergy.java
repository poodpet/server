package com.pood.server.entity.user;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user_pet_allergy")
@EntityListeners(AuditingEntityListener.class)
public class UserPetAllergy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_pet.idx")
    @Column(name = "user_pet_idx")
    private Integer userPetIdx;

    @ApiModelProperty(value = "알러지 항목 번호 : meta_db.allergy_data.idx")
    @Column(name = "allergy_idx")
    private Integer allergyIdx;

    @Column(name = "name")
    private String name;


    @Column(name = "type")
    private Integer type;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    @CreatedDate
    private LocalDateTime recordbirth;

    @Builder(access = AccessLevel.PRIVATE)
    private UserPetAllergy(final int userPetIdx, final int allergyIdx, final String name,
        final int type) {
        this.userPetIdx = userPetIdx;
        this.allergyIdx = allergyIdx;
        this.name = name;
        this.type = type;
    }

    public static UserPetAllergy toEntity(final int userPetIdx, final int allergyIdx,
        final String name,
        final int type) {
        return UserPetAllergy.builder()
            .userPetIdx(userPetIdx)
            .allergyIdx(allergyIdx)
            .name(name)
            .type(type)
            .build();
    }

}
