package com.pood.server.entity.user;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EntityListeners(AuditingEntityListener.class)
@Table(name = "user_review_clap")
public class UserReviewClap {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_info.idx")
    @Column(name = "user_idx")
    private Integer userIdx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_review.idx")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "review_idx")
    private UserReview userReview;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    @LastModifiedDate
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    @CreatedDate
    private LocalDateTime recordbirth;

    @Builder(access = AccessLevel.PRIVATE)
    private UserReviewClap(final int userIdx, final UserReview userReview) {
        this.userIdx = userIdx;
        this.userReview = userReview;
    }

    public static UserReviewClap toEntity(final int userIdx, final UserReview userReview) {
        return UserReviewClap.builder()
            .userIdx(userIdx)
            .userReview(userReview)
            .build();
    }
}
