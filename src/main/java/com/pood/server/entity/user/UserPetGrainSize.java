package com.pood.server.entity.user;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user_pet_grain_size")
public class UserPetGrainSize {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_pet.idx")
    @Column(name = "user_pet_idx")
    private Integer userPetIdx;

    @ApiModelProperty(value = "곡물 항목 번호 : meta_db.grain_size.idx")
    @Column(name = "grain_size_idx")
    private Integer grainSizeIdx;

    @ApiModelProperty(value = "이름 : small, medium, large")
    @Column(name = "name")
    private String name;

    @ApiModelProperty(value = "크기_최소")
    @Column(name = "size_min")
    private Double sizeMin;

    @ApiModelProperty(value = "크기_최대")
    @Column(name = "size_max")
    private Double sizeMax;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private String recordbirth;

    @Builder(access = AccessLevel.PRIVATE)
    private UserPetGrainSize(final Integer userPetIdx, final Integer grainSizeIdx,
        final String name,
        final Double sizeMin, final Double sizeMax) {
        this.userPetIdx = userPetIdx;
        this.grainSizeIdx = grainSizeIdx;
        this.name = name;
        this.sizeMin = sizeMin;
        this.sizeMax = sizeMax;
    }

    public static UserPetGrainSize toEntity(final int userPetIdx, final int grainSizeIdx,
        final String name,
        final double sizeMin, final double sizeMax) {
        return UserPetGrainSize.builder()
            .userPetIdx(userPetIdx)
            .grainSizeIdx(grainSizeIdx)
            .name(name)
            .sizeMin(sizeMin)
            .sizeMax(sizeMax)
            .build();
    }

    @PrePersist
    void onPrePersist() {
        this.recordbirth = LocalDateTime.now()
            .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    public boolean isWetType(){
        return grainSizeIdx.equals(3);
    }

    public boolean isNotWetType(){
        return !grainSizeIdx.equals(3);
    }
}
