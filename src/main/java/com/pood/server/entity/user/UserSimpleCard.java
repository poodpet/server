package com.pood.server.entity.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user_simple_card")
public class UserSimpleCard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_info.idx")
    @Column(name = "user_idx")
    private Integer userIdx;

    @ApiModelProperty(value = "사용 안함")
    @Column(name = "billing_key")
    private String billingKey;

    @ApiModelProperty(value = "카드 이름")
    @Column(name = "card_name")
    private String cardName;

    @ApiModelProperty(value = "카드 번호")
    @Column(name = "card_number")
    private String cardNumber;

    @ApiModelProperty(value = "카드 사용자 이름")
    @Column(name = "card_user")
    private String cardUser;

    @ApiModelProperty(value = "카드 배경 이미지")
    @Column(name = "card_bg")
    private String cardBg;

    @ApiModelProperty(value = "0: 대표 카드아님, 1: 대표 카드")
    @Column(name = "main_card")
    private Integer mainCard;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
