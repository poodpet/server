package com.pood.server.entity.user;

import com.pood.server.exception.RefundException;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user_point")
public class UserPoint {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "항목 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_uuid")
    @Column(name = "user_uuid")
    private String userUuid;

    @ApiModelProperty(value = "포인트 발급 고유 번호")
    @Column(name = "point_uuid")
    private String pointUuid;

    @ApiModelProperty(value = "구매 적립금인 경우 주문 번호")
    @Column(name = "order_number")
    private String orderNumber;

    @ApiModelProperty(value = "리뷰 적립금인 경우 리뷰 항목 번호 : user_review.idx")
    @Column(name = "review_idx")
    private Integer reviewIdx;

    @ApiModelProperty(value = "meta_db.point.idx")
    @Column(name = "point_idx")
    private Integer pointIdx;

    @ApiModelProperty(value = "포인트 이름")
    @Column(name = "point_name")
    private String pointName;

    @ApiModelProperty(value = "0: 적용 금액, 1 : 적용 퍼센트")
    @Column(name = "point_type")
    private Integer pointType;

    @ApiModelProperty(value = "point_type이 0인 경우 금액 수치")
    @Column(name = "point_price")
    private Integer pointPrice;

    @ApiModelProperty(value = "point_type이 1인 경우 구매 금액의 % 수치")
    @Column(name = "point_rate")
    private Integer pointRate;

    @ApiModelProperty(value = "포인트 만료 기간")
    @Column(name = "expired_date")
    private LocalDateTime expiredDate;

    @ApiModelProperty(value = "0 : 포인트 적립 예정, 1 : 포인트 적립 완료, 2 : 포인트 적립 취소, 3 : 포인트 사용됨,  5 : 포인트 만료, 6 : 포인트 회수")
    @Column(name = "status")
    private Integer status;

    @ApiModelProperty(value = "적립된 포인트")
    @Column(name = "saved_point")
    private Integer savedPoint;

    @ApiModelProperty(value = "사용되었으면 사용된 포인트")
    @Column(name = "used_point")
    private Integer usedPoint;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    public boolean isNotRewardPoint() {
        final int notRewardPoint = 0;
        return !status.equals(notRewardPoint);
    }

    public int getAvailablePoint() {
        return savedPoint - usedPoint;
    }

    public boolean eqPointUuid(final String pointUuid) {
        return this.pointUuid.equals(pointUuid);
    }

    public void refundPoint(final Integer refundablePoint) {
        validateRefundPoint(refundablePoint);

        usedPoint -= refundablePoint;
        updateStatusAfterRefund();
    }

    public void validateRefundPoint(final Integer refundablePoint) {
        if (usedPoint < refundablePoint) {
            throw new RefundException("사용한 포인트금액보다 환급할 포인트 금액큽니다.");
        }
    }

    private void updateStatusAfterRefund() {
        if (savedPoint > usedPoint) {
            status = 1;
        }
    }
}
