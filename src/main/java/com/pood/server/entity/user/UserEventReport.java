package com.pood.server.entity.user;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Getter
@Entity
@Table(name = "user_event_report")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EntityListeners(AuditingEntityListener.class)
public class UserEventReport {

    @Id
    @Column(name = "idx", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "event_info_idx")
    private Integer eventInfoIdx;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_info_idx")
    private UserInfo userInfo;

    @Column(name = "report_message", nullable = false, length = 50)
    private String reportMessage;

    @Column(name = "my_info_idx")
    private int myInfoIdx;

    @Column(name = "record_birth")
    @CreatedDate
    private LocalDateTime recordBirth;

    @Builder(access = AccessLevel.PRIVATE)
    private UserEventReport(final Integer eventInfoIdx, final UserInfo userInfo, final String reportMessage, final int myInfoIdx) {
        this.eventInfoIdx = eventInfoIdx;
        this.userInfo = userInfo;
        this.reportMessage = reportMessage;
        this.myInfoIdx = myInfoIdx;
    }

    public static UserEventReport of(final Integer eventInfoIdx, final UserInfo userInfo, final String reportMessage, final int myInfoIdx) {
        return UserEventReport.builder()
            .eventInfoIdx(eventInfoIdx)
            .userInfo(userInfo)
            .reportMessage(reportMessage)
            .myInfoIdx(myInfoIdx)
            .build();
    }
}