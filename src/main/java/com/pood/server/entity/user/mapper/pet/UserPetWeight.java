package com.pood.server.entity.user.mapper.pet;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserPetWeight {

    private Long idx;

    private LocalDate date;

    private int week;

    private Float weight;

}
