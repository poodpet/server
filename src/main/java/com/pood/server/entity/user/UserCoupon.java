package com.pood.server.entity.user;

import com.pood.server.entity.meta.Coupon;
import com.pood.server.util.UserCouponStatus;
import com.pood.server.util.converter.UserCouponStatusConverter;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import java.time.LocalTime;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EntityListeners(AuditingEntityListener.class)
@Table(name = "user_coupon")
public class UserCoupon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "쿠폰 항목 번호 : meta_db.coupon.idx")
    @Column(name = "coupon_idx")
    private Integer couponIdx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_info.idx")
    @Column(name = "user_idx")
    private Integer userIdx;

    @Column(name = "user_uuid")
    private String userUuid;

    @ApiModelProperty(value = "장바구니 항목 번호 : user_db.user_basket.idx")
    @Column(name = "apply_cart_idx")
    private Integer applyCartIdx;

    @ApiModelProperty(value = "(사용 했을 경우) 사용한 날짜/시각")
    @Column(name = "used_time")
    private LocalDateTime usedTime;

    @ApiModelProperty(value = "0: 사용 불가, 1: 사용 가능, 2: 사용함")
    @Column(name = "status")
    @Convert(converter = UserCouponStatusConverter.class)
    private UserCouponStatus status;

    @ApiModelProperty(value = "0: 중복 쿠폰 아님, 1: 중복 쿠폰")
    @Column(name = "over_type")
    private Integer overType;

    @ApiModelProperty(value = "사용 가능한 날짜/시각")
    @Column(name = "available_time")
    private LocalDateTime availableTime;

    @ApiModelProperty(value = "쿠폰 발행(발급 받은) 날짜/시각")
    @Column(name = "publish_time")
    private LocalDateTime publishTime;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    @LastModifiedDate
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    @CreatedDate
    private LocalDateTime recordbirth;

    @Builder
    private UserCoupon(final Integer couponIdx, final Integer userIdx, final String userUuid,
        final Integer applyCartIdx, final UserCouponStatus status,
        final Integer overType,
        final LocalDateTime availableTime, final LocalDateTime publishTime) {
        this.couponIdx = couponIdx;
        this.userIdx = userIdx;
        this.userUuid = userUuid;
        this.applyCartIdx = applyCartIdx;
        this.usedTime = null;
        this.status = status;
        this.overType = overType;
        this.availableTime = availableTime;
        this.publishTime = publishTime;
    }

    public static UserCoupon init(final Coupon coupon, final UserInfo userInfo) {
        final LocalDateTime now = LocalDateTime.now();
        return UserCoupon.builder()
            .couponIdx(coupon.getIdx())
            .userIdx(userInfo.getIdx())
            .userUuid(userInfo.getUserUuid())
            .applyCartIdx(null)
            .availableTime(now.plusDays(coupon.getAvailableDay()).with(LocalTime.MAX)
                .minusSeconds(1))
            .status(UserCouponStatus.AVAILABLE)
            .overType(coupon.getOverType())
            .publishTime(now)
            .build();
    }
}
