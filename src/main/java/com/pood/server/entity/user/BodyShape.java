package com.pood.server.entity.user;

import com.pood.server.exception.NotFoundException;
import java.util.Arrays;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BodyShape {

    SKINNY("A", 942),
    NORMAL("B", null),
    OBESITY("C", 941),
    HIGHLY_OBESE("D", 941);

    private final String type;
    private final Integer ardGroupCode;

    public static BodyShape findType(final String request) {
        if (Objects.isNull(request)) {
            return null;
        }

        return Arrays.stream(BodyShape.values())
            .filter(bodyShape -> bodyShape.getType().equals(request))
            .findFirst()
            .orElseThrow(() -> new NotFoundException(
                String.format("해당하는 체형 타입 [%s]가 존재하지 않습니다.", request)
            ));
    }

    public Integer getReverseArdGroupCode() {
        if (this.type.equals(SKINNY.type)) {
            return OBESITY.ardGroupCode;
        }

        if (this.type.equals(OBESITY.type) || this.type.equals(HIGHLY_OBESE.type)) {
            return SKINNY.ardGroupCode;
        }

        return NORMAL.ardGroupCode;
    }
}
