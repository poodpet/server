package com.pood.server.entity.user;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EntityListeners(AuditingEntityListener.class)
@Table(name = "user_review_sue")
public class UserReviewSue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "user_db.user_info.idx")
    @ManyToOne
    @JoinColumn(name = "user_info_idx", insertable = true, updatable = false)
    private UserInfo userInfo;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_review.idx")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "review_idx")
    private UserReview userReview;

    @ApiModelProperty(value = "사유")
    @Column(name = "text")
    private String text;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    @CreatedDate
    private LocalDateTime recordbirth;

    @Builder(access = AccessLevel.PRIVATE)
    private UserReviewSue(final Integer idx, final UserInfo userInfo, final UserReview userReview,
        final String text) {
        this.idx = idx;
        this.userInfo = userInfo;
        this.userReview = userReview;
        this.text = text;
    }

    public static UserReviewSue create(final UserInfo userInfo, final UserReview userReview,
        final String text){
        return  UserReviewSue.builder()
            .userInfo(userInfo)
            .userReview(userReview)
            .text(text)
            .build();
    }
}
