package com.pood.server.entity.user;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user_base_image")
public class UserBaseImage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "idx")
    @Column(name = "idx")
    private Long idx;

    @ApiModelProperty(value = "url")
    @Column(name = "url")
    private String url;

    @ApiModelProperty(value = "기본 이미지 설정")
    @Column(name = "basic", columnDefinition = "tinyint(1) default 0")
    private boolean basic;

    @LastModifiedDate
    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @CreatedDate
    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    public boolean isBasic() {
        return basic;
    }

    public static UserBaseImage create(final String url){
        return UserBaseImage.builder().url(url).basic(false).build();
    }

    @Builder(access = AccessLevel.PRIVATE)
    public UserBaseImage(Long idx, String url, boolean basic) {
        this.idx = idx;
        this.url = url;
        this.basic = basic;
    }

}
