package com.pood.server.entity.user;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Immutable;

@Getter
@Entity
@Table(name = "pet_diagnosis_join_view")
@Immutable
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PetDiagnosisJoinView implements Serializable {

    @Id
    private Integer idx;

    @Column(name = "user_pet_idx")
    private Integer userPetIdx;

    @Column(name = "pet_worry_idx")
    private Long petWorryIdx;

    @Column(name = "name")
    private String name;

    @Column(name = "dog_url")
    private String dogUrl;

    @Column(name = "cat_url")
    private String catUrl;
}
