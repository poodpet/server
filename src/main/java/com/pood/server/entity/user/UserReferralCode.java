package com.pood.server.entity.user;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user_referral_code")
@EntityListeners(AuditingEntityListener.class)
public class UserReferralCode {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "추천된 친구(혜택을 받을 사람) 회원 항목 번호 : user_db.user_info.idx")
    @Column(name = "received_user_idx")
    private Integer receivedUserIdx;

    @ApiModelProperty(value = "추천인 코드")
    @Column(name = "referral_code")
    private String referralCode;

    @ApiModelProperty(value = "추천한 친구 (회원 가입을 하는사람) 회원 항목 번호 : user_db.user_info.idx")
    @Column(name = "user_idx")
    private Integer userIdx;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    @CreatedDate
    private LocalDateTime recordbirth;

    public UserReferralCode(final Integer receivedUserIdx, final String referralCode, final Integer userIdx) {
        this.receivedUserIdx = receivedUserIdx;
        this.referralCode = referralCode;
        this.userIdx = userIdx;
    }

}
