package com.pood.server.entity.user.mapper.pet;

import com.querydsl.core.annotations.QueryProjection;
import lombok.Value;

@Value
public class UserPetTotalCountMapper {

    Integer pcIdx;

    Long count;

    @QueryProjection
    public UserPetTotalCountMapper(Integer pcIdx, Long count) {
        this.pcIdx = pcIdx;
        this.count = count;
    }

}
