package com.pood.server.entity.user;

import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@Table(name = "user_pet_weight_diary")
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor
@AllArgsConstructor
public class UserPetWeightDiary {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Long idx;

    @ManyToOne
    @JoinColumn(name = "user_pet_idx")
    private UserPet userPet;

    private Float weight;

    @Column(name = "base_date")
    private LocalDate baseDate;

    @Column(name = "update_time")
    @LastModifiedDate
    private LocalDateTime updateTime;

    @Column(name = "record_birth")
    @CreatedDate
    private LocalDateTime recordBirth;

    @Builder
    private UserPetWeightDiary(final UserPet userPet, final Float weight,
        final LocalDate baseDate) {

        validPositiveWeight(weight);

        this.userPet = userPet;
        this.weight = weight;
        this.baseDate = baseDate;
    }

    public static UserPetWeightDiary empty() {
        return new UserPetWeightDiary();
    }

    public void updateWeight(final float weight) {
        validPositiveWeight(weight);
        this.weight = weight;
    }

    private void validPositiveWeight(final float weight) {
        if (weight <= 0f) {
            throw new IllegalArgumentException("펫의 체중이 0보다 커야합니다.");
        }
    }

    public boolean eqIdx(final long idx){
        return this.idx.equals(idx);
    }
}
