package com.pood.server.entity.user;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user_pet_feed")
@EntityListeners(AuditingEntityListener.class)
public class UserPetFeed {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_pet.idx")
    @Column(name = "user_pet_idx")
    private Integer userPetIdx;

    @ApiModelProperty(value = "다른 사료 항목 번호 : meta_db.feed_other_product.idx")
    @Column(name = "of_idx")
    private Integer ofIdx;

    @ApiModelProperty(value = "상품 항목 번호 : meta_db.product.idx")
    @Column(name = "product_idx")
    private Integer productIdx;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    @CreatedDate
    private LocalDateTime recordbirth;

    @Builder(access = AccessLevel.PRIVATE)
    UserPetFeed(final Integer userPetIdx, final Integer ofIdx, final Integer productIdx) {
        this.userPetIdx = userPetIdx;
        this.ofIdx = ofIdx;
        this.productIdx = productIdx;
    }

    public static UserPetFeed toEntity(final Integer userPetIdx, final Integer ofIdx,
        final Integer productIdx) {
        return UserPetFeed.builder()
            .userPetIdx(userPetIdx)
            .ofIdx(ofIdx)
            .productIdx(productIdx)
            .build();
    }
}
