package com.pood.server.entity.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user_basket")
public class UserBasket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_info.user_idx")
    @Column(name = "user_idx")
    private Integer userIdx;

    @Column(name = "user_uuid")
    private String userUuid;

    @ApiModelProperty(value = "굿즈 항목 번호 : order_db.goods.idx")
    @Column(name = "goods_idx")
    private Integer goodsIdx;

    @ApiModelProperty(value = "meta_db.promotion.idx")
    @Column(name = "pr_code_idx")
    private Integer prCodeIdx;

    @ApiModelProperty(value = "장바구니 수량")
    @Column(name = "qty")
    private Integer qty;

    @ApiModelProperty(value = "굿즈 금액")
    @Column(name = "goods_price")
    private Integer goodsPrice;

    @ApiModelProperty(value = "정기배송지정날짜")
    @Column(name = "regular_date")
    private String regularDate;

    @ApiModelProperty(value = "주문 번호 : order_db.order.order_number")
    @Column(name = "order_number")
    private String orderNumber;

    @ApiModelProperty(value = "1: 일반, 2: 바로구매(안보임), 3: 임시저장(안보임)")
    @Column(name = "status")
    private Integer status;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
