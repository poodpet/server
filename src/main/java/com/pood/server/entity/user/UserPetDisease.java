package com.pood.server.entity.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user_pet_disease")
public class UserPetDisease {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_pet.idx")
    @Column(name = "user_pet_idx")
    private Integer userPetIdx;

    @ApiModelProperty(value = "질병 항목 번호 : meta_db.sick_info")
    @Column(name = "sick_idx")
    private Integer sickIdx;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
