package com.pood.server.entity.user;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Immutable;

@Getter
@Entity
@Table(name = "pet_allergy_join_view")
@Immutable
@NoArgsConstructor
public class PetAllergyJoinView {

    @Id
    private Integer idx;

    @Column(name = "user_pet_idx")
    private int userPetIdx;

    @Column(name = "allergy_idx")
    private int allergyIdx;

    private String name;

    private int type;

    @Column(name = "recordbirth")
    private LocalDateTime recordBirth;

}
