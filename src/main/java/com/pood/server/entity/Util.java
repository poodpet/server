package com.pood.server.entity;

import com.pood.server.entity.user.UserPetAllergy;
import java.util.Calendar;
import java.util.List;

@Deprecated
public class Util {

    //중성화 체크
    //pet_gender = 1 남아 중성화 안함, 2 여아 중성화 안함, 3 남아 중성화 함, 4 여아 중성화 함
    static boolean checkSurgery(int petGender) {
        return petGender == 3 || petGender == 4;
    }

    //유저펫의 allergy와 product의 main_propety(주요단백질)체크
    //true면 알러지 문제 없음 -> 통과
    //false면 펫에게 알러지 유발하는 성분 포함하는 경우, -> 불통과
    static boolean checkAllergy(List<UserPetAllergy> allergyDataList, String main_property) {
        for (UserPetAllergy data : allergyDataList) {
            if (data.getName().contains(main_property)) {
                return false;
            }
        }
        return true;
    }

    //펫 나이를 month로 변환
    public static int getNowDateMonth(String date) {
        if (date.equals("-") || date.isEmpty()) return 0;
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Integer.parseInt(date.substring(0, 4)));
        calendar.set(Calendar.MONTH, Integer.parseInt(date.substring(4, 6)) - 1);
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date.substring(6, 8)));

        long minute_ms = 60 * 1000;
        long hour_ms = minute_ms * 60;
        long month_ms = hour_ms * 24 * 30;
        Calendar today = Calendar.getInstance();
        long month = (today.getTimeInMillis() - calendar.getTimeInMillis()) / month_ms;

        return (int) month;
    }

    static double changeScore(int pc_id, int pet_gender, double dogSurgeryTrue, double dogSurgeryFalse, double catSurgeryTrue, double catSurgeryFalse) {
        if (pc_id == 1) {
            return checkSurgery(pet_gender) ? dogSurgeryTrue : dogSurgeryFalse;
        } else {
            return checkSurgery(pet_gender) ? catSurgeryTrue : catSurgeryFalse;
        }
    }
}
