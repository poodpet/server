package com.pood.server.entity;

@Deprecated
public class UserPetCupInfo {

    public String cupType = "";
    public String cupSubType = "";

    public UserPetCupInfo(String cupType, String cupSubType) {
        this.cupType = cupType;
        this.cupSubType = cupSubType;
    }

    public UserPetCupInfo() {
    }
}
