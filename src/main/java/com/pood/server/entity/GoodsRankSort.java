package com.pood.server.entity;

import java.util.Comparator;
import org.springframework.data.util.Pair;

@Deprecated
public class GoodsRankSort implements Comparator<Pair<Integer, Long>> {

    @Override
    public int compare(Pair<Integer, Long> integerDoublePair, Pair<Integer, Long> t1) {
        return integerDoublePair.getSecond().compareTo(t1.getSecond());
    }
}
