package com.pood.server.entity;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Deprecated
public class TripleAndType {

    List<String> first ;
    List<String> second ;
    List<String> third;
    String type;
    public TripleAndType(List<String> first, List<String> second, List<String> third, String type) {
        this.first = first;
        this.second= second;
        this.third = third;
        this.type = type;
    }

}
