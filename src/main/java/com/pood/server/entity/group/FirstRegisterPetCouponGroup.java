package com.pood.server.entity.group;

import com.pood.server.entity.meta.Coupon;
import com.pood.server.facade.coupon.response.FirstRegisterPetCouponResponse;
import java.util.List;
import java.util.stream.Collectors;

public class FirstRegisterPetCouponGroup {

    private final List<Coupon> couponList;

    public FirstRegisterPetCouponGroup(final List<Coupon> couponList) {
        this.couponList = couponList;
    }

    public List<FirstRegisterPetCouponResponse> toResponse() {
        return couponList.stream()
            .map(coupon -> new FirstRegisterPetCouponResponse(coupon.getIdx(), coupon.getName(),
                coupon.getType(), coupon.getDiscountRate(), coupon.getMaxPrice(),
                coupon.getLimitPrice(), coupon.getAvailableDay()))
            .collect(Collectors.toList());
    }

}
