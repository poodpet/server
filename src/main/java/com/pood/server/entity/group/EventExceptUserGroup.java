package com.pood.server.entity.group;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class EventExceptUserGroup {

    private final Set<Integer> userIdxList;

    public EventExceptUserGroup(final List<Integer>... idxArray) {
        userIdxList =  Arrays.stream(idxArray)
            .filter(Objects::nonNull)
            .flatMap(Collection::stream)
            .collect(Collectors.toSet());
    }

    public Set<Integer> getList() {
        if (userIdxList.isEmpty()) {
            return null;
        }
        return userIdxList;
    }

}
