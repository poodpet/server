package com.pood.server.entity.group;

import com.pood.server.controller.response.pet.UserPetTotalCountResponse;
import com.pood.server.entity.user.mapper.pet.UserPetTotalCountMapper;
import java.util.List;
import java.util.function.Predicate;

public class UserPetTotalCountGroup {

    private static final int DOG_CATAGORY = 1;
    private static final int CAT_CATAGORY = 2;

    private final List<UserPetTotalCountMapper> userPetTotalCountMapper;

    public UserPetTotalCountGroup(final List<UserPetTotalCountMapper> userPetTotalCountMapper) {
        this.userPetTotalCountMapper = userPetTotalCountMapper;
    }

    public UserPetTotalCountResponse getResponse() {
        long dogCount = getPetCountById(x -> x.getPcIdx().equals(DOG_CATAGORY));
        long catCount = getPetCountById(x -> x.getPcIdx().equals(CAT_CATAGORY));
        return new UserPetTotalCountResponse(dogCount, catCount);
    }

    private long getPetCountById(final Predicate<UserPetTotalCountMapper> predicate) {
        return userPetTotalCountMapper.stream().filter(predicate)
            .mapToLong(UserPetTotalCountMapper::getCount).findAny().orElse(0);
    }

}
