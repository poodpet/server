package com.pood.server.entity.group;

import com.pood.server.entity.user.mapper.pet.UserPetWeight;
import com.pood.server.entity.user.UserPetWeightDiary;
import com.pood.server.facade.user.pet.response.UserPetWeightResponse;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.data.util.Pair;

public class UserPetWeightGroup {

    private final List<UserPetWeight> userPetWeightList;

    public UserPetWeightGroup(final UserPetWeightDiaryGroup userPetWeightDiaryList,
        final List<Pair<LocalDate, Integer>> weekOfMonth) {
        List<UserPetWeight> userPetWeightList = new ArrayList<>();
        for (Pair<LocalDate, Integer> dateAndWeed : weekOfMonth) {
            UserPetWeightDiary userPetWeightDiary = userPetWeightDiaryList.
                getUserPetWeightDiaryByBaseDate(dateAndWeed.getFirst());

            if (Objects.isNull(userPetWeightDiary)) {
                userPetWeightList.add(setEmptyUserPetWeightDiary(dateAndWeed));
            } else {
                userPetWeightList.add(setUserPetWeightDiary(dateAndWeed, userPetWeightDiary));
            }
        }
        this.userPetWeightList = userPetWeightList;
    }

    public List<UserPetWeightResponse> toResponse() {
        return userPetWeightList.stream()
            .map(x-> new UserPetWeightResponse(x.getIdx(),x.getDate(),x.getWeek(),x.getWeight()))
            .collect(Collectors.toList());
    }

    private UserPetWeight setUserPetWeightDiary(final Pair<LocalDate, Integer> dateAndWeed,
        final UserPetWeightDiary userPetWeightDiary) {
        return new UserPetWeight(userPetWeightDiary.getIdx(), dateAndWeed.getFirst(),
            dateAndWeed.getSecond(), userPetWeightDiary.getWeight());
    }

    private UserPetWeight setEmptyUserPetWeightDiary(final Pair<LocalDate, Integer> dateAndWeed) {
        return new UserPetWeight(null, dateAndWeed.getFirst(), dateAndWeed.getSecond(), null);
    }

}
