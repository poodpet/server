package com.pood.server.entity.group;

import com.pood.server.controller.response.pet.UserPetWeightDiaryResponse;
import com.pood.server.entity.user.UserPetWeightDiary;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class UserPetWeightDiaryGroup {

    private final List<UserPetWeightDiary> diaryList;

    public UserPetWeightDiaryGroup(final List<UserPetWeightDiary> diaryList) {
        this.diaryList = Collections.unmodifiableList(diaryList);
    }

    public List<UserPetWeightDiaryResponse> toResponse() {
        return diaryList.stream()
            .map(UserPetWeightDiaryResponse::of)
            .collect(Collectors.toList());
    }

    public UserPetWeightDiary getUserPetWeightDiaryByBaseDate(final LocalDate baseDate) {
        return diaryList.stream()
            .filter(userPetWeightDiary -> userPetWeightDiary.getBaseDate().equals(baseDate))
            .findAny()
            .orElse(null);
    }
}
