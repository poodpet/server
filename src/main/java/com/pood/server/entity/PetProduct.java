package com.pood.server.entity;

import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.meta.Product;
import com.pood.server.entity.user.UserPet;
import com.pood.server.entity.user.UserPetAllergy;
import com.pood.server.entity.user.UserPetGrainSize;
import java.util.HashMap;
import java.util.List;

@Deprecated
public class PetProduct {

    public double calc(UserPet userPet, Product product, List<UserPetAllergy> allergyDataList, Pet pet,
        List<UserPetGrainSize> userPetGrainSizes) {
        double totalPoint = 0.0;

        int month = Util.getNowDateMonth(userPet.getPetBirth().toString().replace("-", ""));
        totalPoint += calcFeedTarget(userPet, product, pet);  //getPsc_info
        totalPoint += calcPetAge(userPet, product, pet, month); //getPsc_info
        totalPoint += calcFeedUnitSize(userPet, product, userPetGrainSizes);
        totalPoint += calcAllergy(userPet, product, allergyDataList);
        totalPoint += calcSurgery(userPet, product);
        totalPoint += calcMoisture(userPet, product);
        if (totalPoint > 100) {
            totalPoint = 100;
        }
        return totalPoint;
    }

    public double lifeRecommendDealCalc(UserPet userPet, Product product, List<UserPetAllergy> allergyDataList, Pet pet,
        List<UserPetGrainSize> userPetGrainSizes) {
        double totalPoint = 0.0;

        int month = Util.getNowDateMonth(userPet.getPetBirth().toString().replace("-", ""));
        totalPoint += calcFeedTarget(userPet, product, pet);  //getPsc_info
        totalPoint += calcPetAge(userPet, product, pet, month); //getPsc_info
        totalPoint += calcAllergy(userPet, product, allergyDataList);
        if (totalPoint > 100) {
            totalPoint = 100;
        }
        return totalPoint;
    }

    public double allergyRecommendDealCalc(UserPet userPet, Product product, List<UserPetAllergy> allergyDataList) {
        double totalPoint = 0.0;

        totalPoint += calcAllergy(userPet, product, allergyDataList);
        if (totalPoint > 100) {
            totalPoint = 100;
        }
        return totalPoint;
    }


    public HashMap<String, Boolean> productAiSolution(UserPet userPet, Product product, List<UserPetAllergy> allergyDataList, Pet pet,
        List<UserPetGrainSize> userPetGrainSizes) {
        HashMap<String, Boolean> result = new HashMap<>();

        String strPetState = "";
        int month = Util.getNowDateMonth(userPet.getPetBirth().toString().replace("-", ""));
        if (month > 12) {
            if (userPet.getPcId() == 1) {
                strPetState = "어린 강아지";
            } else {
                strPetState = "성묘";
            }
        } else if (month <= 12 && month > 84) {
            if (userPet.getPcId() == 1) {
                strPetState = "성견";
            } else {
                strPetState = "성묘";
            }
        } else if (userPet.getPcId() == 1) {
            strPetState = "노령견";
        } else {
            strPetState = "노령묘";
        }

        if (pet.getPcId().equals(1)) {
            result.put(pet.getPcSize(), calcFeedTarget(userPet, product, pet) > 0 ? Boolean.TRUE : Boolean.FALSE);
        }
        result.put(strPetState, calcPetAge(userPet, product, pet, month) > 0 ? Boolean.TRUE : Boolean.FALSE);
        result.put("알갱이 크기", calcFeedUnitSize(userPet, product, userPetGrainSizes) > 0 ? Boolean.TRUE : Boolean.FALSE);
        result.put("알러지 프리", calcAllergy(userPet, product, allergyDataList) > 0 ? Boolean.TRUE : Boolean.FALSE);
        result.put("중성화", calcSurgery(userPet, product) > 0 ? Boolean.TRUE : Boolean.FALSE);
        if (product.getCtSubIdx() == 1) {
            result.put("수분공급", calcMoisture(userPet, product) > 0 ? Boolean.TRUE : Boolean.FALSE);
        }
        return result;
    }

    /**
     * feedTarget/펫 크기 유형 체크 펫 크기는 분류는 강아지만 있다
     *
     * @param userPet
     * @return
     */
    private double calcFeedTarget(UserPet userPet, Product product, Pet pet) {

        //강아지가 아닐경우 0점
        if (userPet.getPcId() != 1) {
            return 0.0;
        }

        double point = 0.0;
        //feed_target== 0은 모두에게 잘 맞음
        if (product.getFeedTarget() == 0) {
            point += Util.checkSurgery(userPet.getPetGender()) ? AnalysisPointsType.FEED_TARGET_SURGERY_TRUE
                : AnalysisPointsType.FEED_TARGET_SURGERY_FALSE;
        } else {

            int user_pet_feed_tartget_num = 0;

            //소형
            if (pet.getPcSize().contains("소형")) {
                user_pet_feed_tartget_num = 1;
                //중형
            } else if (pet.getPcSize().contains("중형")) {
                user_pet_feed_tartget_num = 2;
                //대형, 그외
            } else {
                user_pet_feed_tartget_num = 3;
            }

            ///제품의 FeedTarget과 유저 펫의 feedTarget이 일치한다면
            if (product.getFeedTarget() == user_pet_feed_tartget_num) {
                point += Util.checkSurgery(userPet.getPetGender()) ? AnalysisPointsType.FEED_TARGET_SURGERY_TRUE
                    : AnalysisPointsType.FEED_TARGET_SURGERY_FALSE;
            }
        }
        return point;
    }


    /**
     * 펫 나이 체크
     *
     * @param userPet
     * @param product
     * @return
     */
    private double calcPetAge(UserPet userPet, Product product, Pet pet, int month) {

        boolean isFlag = true;
        //퍼피(P)/어덜트(A)/시니어(S)/PA/PAL/AS/전체(M)
        switch (product.getFeedType()) {
            case "P":
                if (month > 12) {
                    isFlag = false;
                }
                break;
            case "AS":
                if (month < 12) {
                    isFlag = false;
                }
                break;
            case "A":
                if (month < 12 || month > 84) {
                    isFlag = false;
                }
                break;
            case "S":
                if (month < 84) {
                    isFlag = false;
                }
                break;
            case "PA":
                if (month > 84) {
                    isFlag = false;
                }
                break;
            case "PAL":
                if (month > 84 && pet.getPcSize().contains("대형")) {
                    isFlag = false;
                }
                break;
        }

        if (isFlag) {

            //강아지와 고양이의 배점이 다름
            return Util.changeScore(userPet.getPcId(), userPet.getPetGender(), AnalysisPointsType.PET_AGE_DOG_SURGERY_TRUE,
                AnalysisPointsType.PET_AGE_DOG_SURGERY_FALSE, AnalysisPointsType.PET_AGE_CAT_SURGERY_TRUE,
                AnalysisPointsType.PET_AGE_CAT_SURGERY_FALSE);
        } else {
            return 0.0;
        }

    }


    /**
     * 선호하는 알갱이 크기 분석 습식인 경우 분석 X
     *
     * @param userPet
     * @param product
     * @return
     */
    private double calcFeedUnitSize(UserPet userPet, Product product, List<UserPetGrainSize> userPetGrainSizes) {
        //펫 등록할 때 선택한 선호하는 알갱이 정보
        //선호하는 알갱이는 소중대 3개가 있으며, 유저가 1개를 선택할 수도 있고, 3개를 선택할 수도 있다.
        //선택한 알갱이 크기 중에 한개라도 사료와 적합하면 배점 부가
        if (userPetGrainSizes != null && userPetGrainSizes.size() > 0) {
            for (UserPetGrainSize feedSize : userPetGrainSizes) {
                if (feedSize.getGrainSizeIdx() == 3 && product.getCtSubIdx() == 1) { //펫이 선호하는 알갱이 중에 습식이 있고, 제품의 타입이 습식인 경우
                    return Util.changeScore(userPet.getPcId(), userPet.getPetGender(),
                        AnalysisPointsType.PET_UNIT_SIZE_WEB_DOG_SURGERY_TRUE,
                        AnalysisPointsType.PET_UNIT_SIZE_WEB_DOG_SURGERY_FALSE, AnalysisPointsType.PET_UNIT_SIZE_WEB_CAT_SURGERY_TRUE,
                        AnalysisPointsType.PET_UNIT_SIZE_WEB_CAT_SURGERY_FALSE);
                } else {
                    if (product.getUnitSize() >= feedSize.getSizeMin() &&
                        product.getUnitSize() <= feedSize.getSizeMax()) {
                        return Util.changeScore(userPet.getPcId(), userPet.getPetGender(),
                            AnalysisPointsType.PET_UNIT_SIZE_DOG_SURGERY_TRUE,
                            AnalysisPointsType.PET_UNIT_SIZE_DOG_SURGERY_FALSE, AnalysisPointsType.PET_UNIT_SIZE_CAT_SURGERY_TRUE,
                            AnalysisPointsType.PET_UNIT_SIZE_CAT_SURGERY_FALSE);
                    }
                }
            }
        }
        return 0.0;
    }


    /**
     * 펫이 가지고 있는 알러지에 도움이 되는 제품인지 분석
     *
     * @param userPet
     * @param product
     * @return
     */
    private double calcAllergy(UserPet userPet, Product product, List<UserPetAllergy> allergyDataList) {

        //펫이 가지고 있는 알러지가 없다면 무조건 점수 부여
        if (allergyDataList != null && allergyDataList.size() != 0) {

            //알러지 있는 제품인지 체크
            for (UserPetAllergy allergyType : allergyDataList) {
                if (allergyType.getName().contains(product.getAnimalProtein()) ||
                    allergyType.getName().contains(product.getMainProperty())) {
                    return 0.0;
                }
            }
        }

        return Util.changeScore(userPet.getPcId(), userPet.getPetGender(), AnalysisPointsType.PET_ALLERGY_DOG_SURGERY_TRUE,
            AnalysisPointsType.PET_ALLERGY_DOG_SURGERY_FALSE, AnalysisPointsType.PET_ALLERGY_CAT_SURGERY_TRUE,
            AnalysisPointsType.PET_ALLERGY_CAT_SURGERY_FALSE);
    }

    /**
     * 중성화 수술 여부 상품의 칼로리가 4000이 넘거나 중성화 수술을 안했을 경우 점수 계산 X
     *
     * @param userPet
     * @param product
     * @return
     */
    private double calcSurgery(UserPet userPet, Product product) {
        //상품의 칼로리가 4000이 넘거나 중성화 수술을 안했을 경우 점수 계산 X
        if (product.getCalorie() >= 4000 || !Util.checkSurgery(userPet.getPetGender())) {
            return 0;
        }

        if (userPet.getPcId() == 1) {
            return AnalysisPointsType.PET_SURGERY_DOG;
        } else {
            return AnalysisPointsType.PET_SURGERY_CAT;
        }


    }

    /**
     * 수분공급 분석, 습식(ct_sub_idx == 1) 인 경우에만 점수 추가
     *
     * @param userPet
     * @param product
     * @return
     */
    private double calcMoisture(UserPet userPet, Product product) {
        if (product.getCtSubIdx() != 1) {
            return 0;
        } else {
            if (userPet.getPcId() == 1) {
                if (Util.checkSurgery(userPet.getPetGender())) {
                    return AnalysisPointsType.PET_MOISTURE_DOG_SURGERY_TRUE;
                } else {
                    return AnalysisPointsType.PET_MOISTURE_DOG_SURGERY_FALSE;
                }
            } else {
                if (Util.checkSurgery(userPet.getPetGender())) {
                    return AnalysisPointsType.PET_MOISTURE_CAT_SURGERY_TRUE;
                } else {
                    return AnalysisPointsType.PET_MOISTURE_CAT_SURGERY_FALSE;
                }
            }
        }
    }


}
