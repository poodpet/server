package com.pood.server.entity;

import java.util.Comparator;
import org.springframework.data.util.Pair;

public class ScoreSort implements Comparator<Pair<Integer, Double>> {

    @Override
    public int compare(Pair<Integer, Double> integerDoublePair, Pair<Integer, Double> t1) {
        return integerDoublePair.getSecond().compareTo(t1.getSecond());
    }
}
