package com.pood.server.entity;

import com.pood.server.entity.meta.Product;
import com.pood.server.entity.user.UserPet;
import com.pood.server.entity.user.UserPetAllergy;
import java.util.List;

@Deprecated
public class BaseCompatibility {

    public boolean calc(UserPet userPet, Product product, List<UserPetAllergy>  allergyDataList) {

        return Util.checkAllergy(allergyDataList, product.getMainProperty()); //유저 펫의 알러지 반응 체크
    }


}
