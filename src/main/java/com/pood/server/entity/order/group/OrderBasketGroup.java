package com.pood.server.entity.order.group;

import com.pood.server.entity.order.mapper.OrderBasketMapper;
import com.pood.server.entity.user.UserReview;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class OrderBasketGroup {

    private final List<OrderBasketMapper> list;

    public OrderBasketGroup(final List<OrderBasketMapper> orderBasketMappers) {
        if (Objects.isNull(orderBasketMappers)) {
            list = new ArrayList<>();
        } else {
            this.list = orderBasketMappers;
        }
    }


    public boolean isEmpty() {
        return list.isEmpty();
    }

    public boolean isReviewPossible(final UserReview review) {
        return list.stream().noneMatch(x -> x.getOrderNumber().equals(review.getOrderNumber())
            && x.getGoodsIdx().equals(review.getGoodsIdx()));
    }

    public int getSize(){
        return list.size();
    }
}
