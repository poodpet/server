package com.pood.server.entity.order;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "seller")
public class Seller{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "셀러 이름")
    @Column(name = "seller_name")
    private String sellerName;

    @ApiModelProperty(value = "셀러 소개말")
    @Column(name = "seller_intro")
    private String sellerIntro;

    @ApiModelProperty(value = "배송비")
    @Column(name = "shipping_fee")
    private Integer shippingFee;

    @ApiModelProperty(value = "무료배송 최소 조건")
    @Column(name = "free_shipping_limit")
    private Integer freeShippingLimit;

    @ApiModelProperty(value = "배송지가 제주도 기본에 해당될 때 부가되는 추가 배송비")
    @Column(name = "jeju_remote")
    private Integer jejuRemote;

    @ApiModelProperty(value = "배송지가 제주도 외지에 해당될 때 부가되는 추가 배송비")
    @Column(name = "jeju_remote_etc")
    private Integer jejuRemoteEtc;

    @ApiModelProperty(value = "배송지가 기타 섬지역에 해당될 때 부가되는 추가 배송비")
    @Column(name = "island_remote")
    private Integer islandRemote;

    @ApiModelProperty(value = "기타 섬지역에 해당될 때 부가되는 추가 배송비")
    @Column(name = "etc_remote")
    private Integer etcRemote;

    @ApiModelProperty(value = "교환 설명")
    @Column(name = "exchange_desc")
    private String exchangeDesc;

    @ApiModelProperty(value = "교환배송비")
    @Column(name = "exchange_fee")
    private Integer exchangeFee;

    @ApiModelProperty(value = "환불 기간")
    @Column(name = "refund_day")
    private Integer refundDay;

    @ApiModelProperty(value = "환불 설명")
    @Column(name = "refund_desc")
    private String refundDesc;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
