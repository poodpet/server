package com.pood.server.entity.order.mapper;

import com.querydsl.core.annotations.QueryProjection;
import lombok.Value;

@Value
public class OrderPointMapper {

    Integer idx;
    String orderNumber;
    String pointUuid;
    Integer usedPoint;
    Integer type;

    @QueryProjection
    public OrderPointMapper(final Integer idx, final String orderNumber, final String pointUuid,
        final Integer usedPoint, final Integer type) {

        this.idx = idx;
        this.orderNumber = orderNumber;
        this.pointUuid = pointUuid;
        this.usedPoint = usedPoint;
        this.type = type;
    }
}
