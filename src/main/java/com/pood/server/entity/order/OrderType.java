package com.pood.server.entity.order;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "order_type")
public class OrderType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "주문 상태 값 ( e.g 0,1,2,3,4, )")
    @Column(name = "value")
    private Integer value;

    @ApiModelProperty(value = "주문 상태 텍스트 ( e.g 0:주문완료대기, 1:결제완료, 2:주문취소신청 ...)")
    @Column(name = "text")
    private String text;

    @ApiModelProperty(value = "항목 수정/날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입/날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
