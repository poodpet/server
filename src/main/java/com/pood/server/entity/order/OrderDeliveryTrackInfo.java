package com.pood.server.entity.order;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "order_delivery_track_info")
public class OrderDeliveryTrackInfo{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "주문 항목 번호 : order_db.order.idx")
    @Column(name = "order_number")
    private String orderNumber;

    @ApiModelProperty(value = "택배사 이름")
    @Column(name = "courier")
    private String courier;

    @ApiModelProperty(value = "0:일반택배, 1:방문수령, 2:퀵 배송")
    @Column(name = "type")
    private Integer type;

    @ApiModelProperty(value = "택배사 운송장 번호")
    @Column(name = "tracking_id")
    private String trackingId;

    @ApiModelProperty(value = "발송일자")
    @Column(name = "startdate")
    private LocalDateTime startdate;

    @ApiModelProperty(value = "0:대표 운송장 번호 아님, 1 : 대표 운송장 번호")
    @Column(name = "main_address")
    private Integer mainAddress;

    @ApiModelProperty(value = "0:배송 중, 1:배송 완료, 2: 배송 시작")
    @Column(name = "send")
    private Integer send;

    @ApiModelProperty(value = "[배송정보구분]10:일반 배송지 택배정보 31:교환신청-고객이 이미 보냈을 경우 운송장 번호 41:환불신청-고객이 이미 보냈을 경우 운송장 번호")
    @Column(name = "send_type")
    private Integer sendType;

    @ApiModelProperty(value = "주문 로그 항목 번호 : log_db.log_user_order.idx")
    @Column(name = "history_idx")
    private Integer historyIdx;

    @ApiModelProperty(value = "굿즈 항목 번호 : meta_db.goods.idx")
    @Column(name = "goods_idx")
    private Integer goodsIdx;

    @ApiModelProperty(value = "수량(분석 필요)")
    @Column(name = "qty")
    private Integer qty;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
