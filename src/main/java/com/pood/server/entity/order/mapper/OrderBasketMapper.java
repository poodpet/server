package com.pood.server.entity.order.mapper;

import com.querydsl.core.annotations.QueryProjection;
import lombok.Value;

@Value
public class OrderBasketMapper {

    Integer idx;

    Integer goodsIdx;

    String orderNumber;

    Integer status;

    @QueryProjection
    public OrderBasketMapper(Integer idx, Integer goodsIdx, String orderNumber,
        Integer status) {
        this.idx = idx;
        this.goodsIdx = goodsIdx;
        this.orderNumber = orderNumber;
        this.status = status;
    }

}
