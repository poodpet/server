package com.pood.server.entity.order.group;

import com.google.gson.Gson;
import com.pood.server.entity.meta.Goods;
import com.pood.server.entity.order.OrderBasket;
import com.pood.server.web.mapper.payment.pay.NaverProduct;
import java.util.ArrayList;
import java.util.List;

public class NaverProductGroup {

    private static final String CATEGORY_TYPE = "PRODUCT";
    private static final String CATEGORY_ID = "GENERAL";
    private final List<NaverProduct> naverProducts;

    public NaverProductGroup() {
        naverProducts = new ArrayList<>();
    }

    public void add(final OrderBasket orderBasket, final Goods goods) {
        naverProducts.add(new NaverProduct(CATEGORY_TYPE, CATEGORY_ID, orderBasket.getGoodsIdx(),
            goods.getGoodsName(), orderBasket.getQuantity()));
    }

    public String getJsonData() {
        Gson gson = new Gson();
        return gson.toJson(naverProducts);
    }

    public List<NaverProduct> getNaverProducts(){
        return naverProducts;
    }
}
