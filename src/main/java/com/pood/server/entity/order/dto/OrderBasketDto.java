package com.pood.server.entity.order.dto;

import com.pood.server.entity.order.OrderBasket;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class OrderBasketDto {

    private Integer idx;

    private Integer basketIdx;

    private Integer orderIdx;

    private String orderNumber;

    private Integer couponIdx;

    private Integer prCodeIdx;

    private Integer goodsIdx;

    private Integer sellerIdx;

    private Integer quantity;

    private Integer goodsPrice;

    private boolean isDeliveryFeeCalculation;

    public static OrderBasketDto create(final OrderBasket orderBasket) {
        return null;
    }

    public void setDeliveryFeeCalculation(final boolean deliveryFeeCalculation) {
        isDeliveryFeeCalculation = deliveryFeeCalculation;
    }
}

