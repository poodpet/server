package com.pood.server.entity.order;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;


@Entity
@Getter
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "order_basket")
public class OrderBasket{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "장바구니 항목 번호 : user_db.user_basket.idx")
    @Column(name = "basket_idx")
    private Integer basketIdx;

    @ApiModelProperty(value = "주문 항목 번호 : order_db.order.idx")
    @Column(name = "order_idx")
    private Integer orderIdx;

    @ApiModelProperty(value = "쿠폰 항목 번호 : meta_db.coupon.idx")
    @Column(name = "order_number")
    private String orderNumber;

    @ApiModelProperty(value = "쿠폰 항목 번호 : meta_db.coupon.idx")
    @Column(name = "coupon_idx")
    private Integer couponIdx;

    @ApiModelProperty(value = "프로모션 항목 번호 : meta_db.pr_code.idx")
    @Column(name = "pr_code_idx")
    private Integer prCodeIdx;

    @ApiModelProperty(value = "굿즈 항목 번호 : order_db.goods.idx")
    @Column(name = "goods_idx")
    private Integer goodsIdx;

    @ApiModelProperty(value = "셀러 항목 번호 : order_db.seller.idx")
    @Column(name = "seller_idx")
    private Integer sellerIdx;

    @Column(name = "discount")
    private Integer discount;

    @ApiModelProperty(value = "구매 시 장바구니 수량")
    @Column(name = "quantity")
    private Integer quantity;

    @ApiModelProperty(value = "1:일반, 2:바로구매(안보임), 3:임시저장(안보임)")
    @Column(name = "status")
    private Integer status;

    @Column(name = "goods_price")
    private Integer goodsPrice;

    @CreatedDate
    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private String recordbirth;

    public OrderBasket(Integer basketIdx, Integer orderIdx, String orderNumber, Integer couponIdx, Integer prCodeIdx, Integer goodsIdx, Integer sellerIdx, Integer discount, Integer quantity, Integer status, Integer goodsPrice) {
        this.basketIdx = basketIdx;
        this.orderIdx = orderIdx;
        this.orderNumber = orderNumber;
        this.couponIdx = couponIdx;
        this.prCodeIdx = prCodeIdx;
        this.goodsIdx = goodsIdx;
        this.sellerIdx = sellerIdx;
        this.discount = discount;
        this.quantity = quantity;
        this.status = status;
        this.goodsPrice = goodsPrice;
    }
}
