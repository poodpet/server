package com.pood.server.entity.order;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "order_point")
public class OrderPoint{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "항목 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "주문 항목 번호 : order_db.order_number")
    @Column(name = "order_number")
    private String orderNumber;

    @ApiModelProperty(value = "회원 포인트 항목 번호 : user_db.point_uuid")
    @Column(name = "point_uuid")
    private String pointUuid;

    @ApiModelProperty(value = "주문시 사용된 포인트")
    @Column(name = "used_point")
    private Integer usedPoint;

    @ApiModelProperty(value = "0: 사용, 1: 반환")
    @Column(name = "type")
    private Integer type;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    public void updateTypeToPointReturn() {
        type = 1;
    }

    public void minusUsedPoint(final int returnPoint) {
        this.usedPoint -= returnPoint;
    }
}
