package com.pood.server.entity.order;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "order_delivery_image")
public class OrderDeliveryImage{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "주문 항목 번호 : order_db.order.idx")
    @Column(name = "order_idx")
    private Integer orderIdx;

    @ApiModelProperty(value = "이미지 주소 URL")
    @Column(name = "url")
    private String url;

    @ApiModelProperty(value = "1:기본, 2:등록자만, 3:안보임, 4:정책 위반")
    @Column(name = "visible")
    private Integer visible;

    @ApiModelProperty(value = "0:발송전 패키지된 박스내부 이미지, 1:운송장 보이는 박스 이미지")
    @Column(name = "type")
    private Integer type;

    @ApiModelProperty(value = "이미지 우선순위")
    @Column(name = "priority")
    private Integer priority;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
