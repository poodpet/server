package com.pood.server.entity.order.group;

import com.pood.server.service.factory.OrderCancelPriceInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;
import lombok.Value;

@Value
public class OrderCancelPriceInfoGroup {

    List<OrderCancelPriceInfo> orderCancelPriceInfoList;

    public OrderCancelPriceInfoGroup() {
        this.orderCancelPriceInfoList = new ArrayList<>();
    }

    public void add(final OrderCancelPriceInfo orderCancelPriceInfo) {
        orderCancelPriceInfoList.add(orderCancelPriceInfo);
    }

    public int getOrgGoodsPrice() {
        return getSum(OrderCancelPriceInfo::getOrgGoodsPrice);
    }

    public int getRetreievePrice() {
        final ToIntFunction<OrderCancelPriceInfo> getRetreievePrice = x -> x.getRetreievePrice()
            .getIntValue();
        return getSum(getRetreievePrice);
    }


    public int getDeliveryFee() {
        return getSum(OrderCancelPriceInfo::getDeliveryFee);
    }

    public long getPoint() {
        return getSum(OrderCancelPriceInfo::getRetreievePoint);
    }

    public int getGoodsPrice() {
        final ToIntFunction<OrderCancelPriceInfo> function = x -> x.getGoodsPrice()
            .getIntValue();
        return getSum(function);
    }

    private int getSum(final ToIntFunction<OrderCancelPriceInfo> toIntfunction) {
        return orderCancelPriceInfoList.stream()
            .mapToInt(toIntfunction)
            .sum();
    }

    private long getSum(final ToLongFunction<OrderCancelPriceInfo> toLongfunction) {
        return orderCancelPriceInfoList.stream()
            .peek(x-> System.out.println(x.getRetreievePrice()))
            .mapToLong(toLongfunction)
            .sum();
    }

}
