package com.pood.server.entity.order.group;

import com.pood.server.entity.order.mapper.OrderPointMapper;
import com.pood.server.exception.RefundException;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Value;

@Value
public class OrderPointMapperGroup {

    List<OrderPointMapper> mappers;

    public static OrderPointMapperGroup of(final List<OrderPointMapper> pointMappers) {
        return new OrderPointMapperGroup(pointMappers);
    }

    public List<String> getPointUuidList() {
        return mappers.stream().map(OrderPointMapper::getPointUuid)
            .collect(Collectors.toList());
    }

    public OrderPointMapper getIndex(final int index) {
        if (mappers.size() <= index) {
            throw new RefundException("반환하려는 포인트가 주문 포인트보다 많습니다.");
        }

        return mappers.get(index);
    }

}
