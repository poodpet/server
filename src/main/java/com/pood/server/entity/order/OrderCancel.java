package com.pood.server.entity.order;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "order_cancel")
public class OrderCancel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "항목 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "취소 신청시 고유번호(요청시 발급됨)")
    @Column(name = "uuid")
    private String uuid;

    @ApiModelProperty(value = "order_db.order.order_number")
    @Column(name = "order_number")
    private String orderNumber;

    @ApiModelProperty(value = "meta_db.goods.idx")
    @Column(name = "goods_idx")
    private Integer goodsIdx;

    @ApiModelProperty(value = "상품 취소 수량")
    @Column(name = "qty")
    private Integer qty;

    @ApiModelProperty(value = "0:접수, 1:완료")
    @Column(name = "type")
    private Integer type;

    @ApiModelProperty(value = "사유")
    @Column(name = "text")
    private String text;

    @ApiModelProperty(value = "부과 배송비")
    @Column(name = "refund_delivery_fee")
    private Integer refundDeliveryFee;

    @ApiModelProperty(value = "반환 포인트")
    @Column(name = "refund_point")
    private Integer refundPoint;

    @ApiModelProperty(value = "반환 주문 가격")
    @Column(name = "refund_order_price")
    private Integer refundOrderPrice;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
