package com.pood.server.entity.order;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "order_refund")
public class OrderRefund{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "항목 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "반품 신청시 고유번호(요청시 발급됨)")
    @Column(name = "uuid")
    private String uuid;

    @ApiModelProperty(value = "order_db.order.order_number")
    @Column(name = "order_number")
    private String orderNumber;

    @ApiModelProperty(value = "meta_db.goods.idx")
    @Column(name = "goods_idx")
    private Integer goodsIdx;

    @ApiModelProperty(value = "상품 반품 수량")
    @Column(name = "qty")
    private Integer qty;

    @ApiModelProperty(value = "0: 이미 보냈어요, 1: 상품 회수해 주세요")
    @Column(name = "send_type")
    private Integer sendType;

    @ApiModelProperty(value = "0:요청, 1:승인, 2:완료,  3:거부, 4:철회, 5:접수, 7:회수접수, 9:완료 10: 철회 승인")
    @Column(name = "type")
    private Integer type;

    @ApiModelProperty(value = "0:구매자 귀책, 1:판매자 귀책")
    @Column(name = "attr")
    private Integer attr;

    @ApiModelProperty(value = "사유")
    @Column(name = "text")
    private String text;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
