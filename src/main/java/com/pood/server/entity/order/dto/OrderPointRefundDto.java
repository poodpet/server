package com.pood.server.entity.order.dto;

import com.pood.server.entity.order.mapper.OrderPointMapper;
import lombok.Value;

@Value
public class OrderPointRefundDto {
    Integer idx;
    String orderNumber;
    String pointUuid;
    Integer usedPoint;
    Integer refundPoint;

    public static OrderPointRefundDto of(final OrderPointMapper mapper, final Integer refundPoint) {
        return new OrderPointRefundDto(
            mapper.getIdx(),
            mapper.getOrderNumber(),
            mapper.getPointUuid(),
            mapper.getUsedPoint(),
            refundPoint
        );
    }

    public boolean isAllRefund() {
        if (usedPoint < refundPoint) {
            throw new IllegalArgumentException("반환하고자 하는 포인트가 주문에 사용한 포인트보다 많습니다.");
        }

        return usedPoint.equals(refundPoint);
    }
}
