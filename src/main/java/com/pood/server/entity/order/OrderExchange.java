package com.pood.server.entity.order;

import com.pood.server.dto.log.user_order.OrderDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "order_exchange")
public class OrderExchange{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "항목 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "교환 신청시 고유번호(요청시 발급됨)")
    @Column(name = "uuid")
    private String uuid;

    @ApiModelProperty(value = "order_db.order.order_number")
    @Column(name = "order_number")
    private String orderNumber;

    @ApiModelProperty(value = "meta_db.goods.idx")
    @Column(name = "goods_idx")
    private Integer goodsIdx;

    @ApiModelProperty(value = "상품 교환 수량")
    @Column(name = "qty")
    private Integer qty;

    @ApiModelProperty(value = "0: 이미 보냈어요, 1: 상품 회수해 주세요")
    @Column(name = "send_type")
    private Integer sendType;

    @ApiModelProperty(value = "0:요청, 1:승인, 2:완료, 3:거부, 4:배송준비, 5:배송시작, 6:배송완료, 7:철회, 8:접수, 9:배송지연, 10:배송불가, 11:회수접수, 12:출고대기, 13:출고거부, 14:완료 15: 철회 승인")
    @Column(name = "type")
    private Integer type;

    @ApiModelProperty(value = "0:구매자 귀책, 1:판매자 귀책")
    @Column(name = "attr")
    private Integer attr;

    @ApiModelProperty(value = "사유")
    @Column(name = "text")
    private String text;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    @Transient
    private OrderDto.Delivery delivery;

    public void setDelivery(OrderDto.Delivery delivery) {
        this.delivery = delivery;
    }
}
