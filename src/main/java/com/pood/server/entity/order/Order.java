package com.pood.server.entity.order;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "`order`")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "user_idx")
    private Integer userIdx;

    @ApiModelProperty(value = "회원 UUID")
    @Column(name = "user_uuid")
    private String userUuid;

    @ApiModelProperty(value = "쿠폰 항목 번호 : meta_db.coupon.idx")
    @Column(name = "over_coupon_idx")
    private Integer overCouponIdx;

    @ApiModelProperty(value = "주문 이름")
    @Column(name = "order_name")
    private String orderName;

    @ApiModelProperty(value = "주문 번호")
    @Column(name = "order_number")
    private String orderNumber;

    @ApiModelProperty(value = "주문 디바이스 정보")
    @Column(name = "order_device")
    private String orderDevice;

    @Column(name = "total_discount_price")
    private Integer totalDiscountPrice;

    @Column(name = "total_price")
    private Integer totalPrice;

    @ApiModelProperty(value = "주문 가격")
    @Column(name = "order_price")
    private Integer orderPrice;

    @ApiModelProperty(value = "주문 금액에서 취소 가격을 뺀 가격 : 예를 들어서 사용자가 5천원 취소하고 최초 주문금액이 만 2천원이면 7천원(서버에서 보는 용도)")
    @Column(name = "order_price_2")
    private Integer orderPrice2;

    @ApiModelProperty(value = "주문 상태 : order_type_db.order_type.idx")
    @Column(name = "order_status")
    private Integer orderStatus;

    @ApiModelProperty(value = "결제 수단 : meta_db.payment_type idx")
    @Column(name = "order_type")
    private Integer orderType;

    @ApiModelProperty(value = "포인트 사용 금액")
    @Column(name = "used_point")
    private Integer usedPoint;

    @ApiModelProperty(value = "포인트 적립 금액")
    @Column(name = "saved_point")
    private Integer savedPoint;

    @ApiModelProperty(value = "배송비")
    @Column(name = "delivery_fee")
    private Integer deliveryFee;

    @ApiModelProperty(value = "배송사 추적 정보")
    @Column(name = "tracking_id")
    private String trackingId;

    @ApiModelProperty(value = "0:일반택배, 1:방문수령, 2:퀵배송")
    @Column(name = "delivery_type")
    private Integer deliveryType;

    @ApiModelProperty(value = "쿠폰 할인 금액")
    @Column(name = "discount_coupon_price")
    private Integer discountCouponPrice;

    @ApiModelProperty(value = "무료결제여부 - 0:일반결제, 1:할인에 따른 무료결제")
    @Column(name = "free_purchase")
    private Integer freePurchase;

    @ApiModelProperty(value = "주문 배송지 수정 여부 - 0:수정안함, 1:수정함")
    @Column(name = "address_update")
    private Integer addressUpdate;

    @ApiModelProperty(value = "삭제 - 0:기본, 1:삭제(안보임)")
    @Column(name = "is_delete")
    private Integer isDelete;

    @ApiModelProperty(value = "주문시 메모")
    @Column(name = "memo")
    private String memo;

    @Column(name = "release_date")
    private LocalDateTime releaseDate;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    public void addDeliveryFee(final int deliveryFee) {
        this.deliveryFee += deliveryFee;
    }

}
