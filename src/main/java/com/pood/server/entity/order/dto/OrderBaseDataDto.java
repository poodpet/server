package com.pood.server.entity.order.dto;

import com.amazonaws.services.kms.model.AlreadyExistsException;
import com.amazonaws.services.kms.model.NotFoundException;
import com.pood.server.api.req.header.pay.hPay_cancel_1_2;
import com.pood.server.config.meta.META_DELIVERY_FEE;
import com.pood.server.entity.order.Order;
import com.pood.server.entity.order.OrderBasket;
import com.pood.server.entity.order.OrderCancel;
import com.pood.server.entity.order.OrderDelivery;
import com.pood.server.entity.order.group.OrderCancelPriceInfoGroup;
import com.pood.server.facade.order.OrderCancelInfoResponse;
import com.pood.server.web.mapper.payment.Money;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;

//@Value
@Getter
@AllArgsConstructor
public class OrderBaseDataDto {

    private Order orderInfo;

    private List<OrderBasket> orderBasketList;

    private List<OrderCancel> orderCancelList;

    private OrderDelivery orderDelivery;

    private Integer totalUsedPoint;

    private Money totalPrice;

    private Money orderPrice;

    private int orgDeliveryFee;

    private int reOrgDeliveryFee;

    public OrderBaseDataDto(final Order orderInfo,
        final OrderDelivery orderDelivery,
        final List<OrderBasket> orderBasketList,
        final List<OrderCancel> orderCancelList,
        final int orgDeliveryFee) {
        this.orderInfo = orderInfo;
        this.orderDelivery = orderDelivery;
        this.orderBasketList = orderBasketList;
        this.orderCancelList = orderCancelList;
        totalPrice = new Money(orderInfo.getTotalPrice());
        totalUsedPoint = orderInfo.getUsedPoint();
        orderPrice = new Money(orderInfo.getOrderPrice());
        this.orgDeliveryFee = orgDeliveryFee;
        this.reOrgDeliveryFee = META_DELIVERY_FEE.DEFUALT_DELIVERY_FEE;

    }

    public int orderBasketSize() {
        return orderBasketList.size();
    }

    public int orderCancelSize() {
        return orderCancelList.size();
    }

    public boolean isAllCancel(final List<Integer> goodsIdxList) {
        isAlreadyCanceledCheck(goodsIdxList);
        isDataInBasketCheck(goodsIdxList);

        final long count = orderCancelList.stream()
            .filter(x -> !goodsIdxList.contains(x.getGoodsIdx()))
            .count();

        return orderBasketSize() == count + goodsIdxList.size();
    }

    public int getDeliveryFee() {
        return orderInfo.getDeliveryFee() +
            orderCancelList.stream().mapToInt(OrderCancel::getRefundDeliveryFee).sum();
    }

    public boolean isDeliveryFee() {
        return orgDeliveryFee == getDeliveryFee();
    }

    public boolean isDeliveryFeeFree() {
        return getDeliveryFee() <= orgDeliveryFee;
    }

    public Money getRetreievePrice(final int totalCancelPrice) {
        final Double retreieveRatio = getRetreieveRatio(totalCancelPrice);
        return getRetreievePrice(retreieveRatio);
    }

    public Double getRetreieveRatio(int totalCancelPrice) {
        return totalCancelPrice / totalPrice.getDoubleValue();
    }

    public long getRetreievePoint(final int totalCancelPrice) {
        final Double retreieveRatio = getRetreieveRatio(totalCancelPrice);
        return getRetreievePoint(retreieveRatio);
    }

    public boolean isTotalMoneyGreaterThanDeliveryFee(final List<Integer> goodsIdxList) {
        final int totlaMoney = this.orderBasketList.stream()
            .filter(x -> goodsIdxList.contains(x.getGoodsIdx()))
            .mapToInt(this::getGoodsPrice)
            .sum();
        return totlaMoney > META_DELIVERY_FEE.DEFUALT_DELIVERY_FEE;
    }

    public boolean isGoodsPriceGreaterThanDeliveryFee(final List<Integer> goodsIdxList) {
        final Integer goodsIdx = this.orderBasketList.stream()
            .filter(x -> goodsIdxList.contains(x.getGoodsIdx()))
            .filter(y -> orgDeliveryFee <= getGoodsPrice(y))
            .map(OrderBasket::getGoodsIdx)
            .findFirst()
            .orElse(null);

        return Objects.nonNull(goodsIdx);
    }

    public boolean isGoodsPriceGreaterThanDeliveryFee(final List<Integer> goodsIdxList,
        final Integer goodsIdx) {
        final Integer deliveryFeeGoodsIdx = this.orderBasketList.stream()
            .filter(x -> goodsIdxList.contains(x.getGoodsIdx()))
            .filter(y -> orgDeliveryFee <= getGoodsPrice(y))
            .map(OrderBasket::getGoodsIdx)
            .findFirst()
            .orElseThrow(() -> new NotFoundException("배달비를 부과할 상품을 찾을수 없습니다."));
        return deliveryFeeGoodsIdx.equals(goodsIdx);
    }


    public int getGoodsPriceByGoodsIdx(final Integer goodsIdx) {
        return orderBasketList.stream()
            .filter(x -> x.getGoodsIdx().equals(goodsIdx))
            .mapToInt(y -> y.getGoodsPrice() * y.getQuantity())
            .findFirst()
            .orElseThrow(() -> new NullPointerException("해당 상품이 주문에 포함되어 있지 않습니다."));
    }

    public int getGoodsPrice(final OrderBasket goodsBasketInfo) {
        return getRetreievePrice(goodsBasketInfo.getGoodsPrice()
            * goodsBasketInfo.getQuantity()).getIntValue();
    }

    private Money getRetreievePrice(final Double retreieveRatio) {
        return orderPrice.minus(new Money(orderInfo.getDeliveryFee())).ratio(retreieveRatio);
    }

    private long getRetreievePoint(final Double retreieveRatio) {
        return Math.round(retreieveRatio * totalUsedPoint);
    }

    private void isDataInBasketCheck(final List<Integer> goodsIdxList) {
        final boolean inBasketCheck = orderBasketList.stream()
            .filter(x -> goodsIdxList.contains(x.getGoodsIdx()))
            .findAny()
            .isEmpty();

        if (inBasketCheck) {
            throw new NullPointerException("해당 상품이 주문에 포함되어 있지 않습니다.");
        }
    }

    private void isAlreadyCanceledCheck(final List<Integer> goodsIdxList) {
        final boolean canceledGoodsCheck = orderCancelList.stream()
            .filter(x -> goodsIdxList.contains(x.getGoodsIdx()))
            .findFirst()
            .isEmpty();

        if (!canceledGoodsCheck) {
            throw new AlreadyExistsException("해당 상품은 이미 취소되어 있습니다.");
        }
    }

    public int getDeliveryFee(final int devisionDeliveryFee, final int goodsPrice) {
        final Money retreievePrice = getRetreievePrice(goodsPrice);
        if (devisionDeliveryFee < retreievePrice.getIntValue()) {
            reOrgDeliveryFee -= devisionDeliveryFee;
            return devisionDeliveryFee;
        }
        reOrgDeliveryFee -= retreievePrice.getIntValue();
        return retreievePrice.getIntValue();
    }

    public boolean isOrderPriceThenDefualtPrice(final List<Integer> goodsIdxList) {
        final Money cancelMoney = new Money(this.orderBasketList.stream()
            .filter(x -> goodsIdxList.contains(x.getGoodsIdx()))
            .mapToInt(this::getGoodsPrice)
            .sum());

        final Money totalMoney = new Money(this.orderBasketList.stream()
            .mapToInt(this::getGoodsPrice)
            .sum());

        int intValue = totalMoney.minus(cancelMoney).getIntValue();
        return META_DELIVERY_FEE.DEFUALT_PRICE <= intValue;
    }

    public OrderCancelInfoResponse getOrderCancelInfoResponse(final List<Integer> goodsIdxList,
        final OrderCancelPriceInfoGroup orderCancelPriceInfoGroup,
        final OrderBaseDataDto orderBaseDataDto) {
        final boolean isAllCancel = isAllCancel(goodsIdxList);

        if (isAllCancel) {
            return new OrderCancelInfoResponse(
                orderBaseDataDto.getOrderPrice().getIntValue(),
                orderCancelPriceInfoGroup.getDeliveryFee(),
                orderBaseDataDto.getOrderPrice().getIntValue(),
                orderBaseDataDto.getTotalUsedPoint());
        }
        return new OrderCancelInfoResponse(
            orderCancelPriceInfoGroup.getOrgGoodsPrice(),
            orderCancelPriceInfoGroup.getDeliveryFee(),
            orderCancelPriceInfoGroup.getRetreievePrice(),
            orderCancelPriceInfoGroup.getPoint());
    }

    public long refundCompletePoint() {
        final List<Integer> cancelGoodsIdxList = getCancelGoodsIdxList();
        return orderBasketList.stream()
            .filter(x -> cancelGoodsIdxList.contains(x.getGoodsIdx()))
            .map(x -> getGoodsPriceByGoodsIdx(x.getGoodsIdx()))
            .mapToLong(x -> (int) getRetreievePoint(x))
            .sum();
    }

    private List<Integer> getCancelGoodsIdxList() {
        return orderCancelList.stream()
            .map(OrderCancel::getGoodsIdx).collect(Collectors.toList());
    }

    public List<Integer> getNonCancelledGoodsIdxList() {
        return orderBasketList.stream()
            .map(OrderBasket::getGoodsIdx)
            .filter(x -> !getCancelGoodsIdxList().contains(x))
            .collect(Collectors.toList());
    }

    public List<hPay_cancel_1_2> getPayCancelInfoList(){
        return this.orderBasketList.stream()
            .filter(x -> !getCancelGoodsIdxList().contains(x.getGoodsIdx()))
            .map(y -> new hPay_cancel_1_2(y.getGoodsIdx(), y.getQuantity(), null,null))
            .collect(Collectors.toList());
    }
}
