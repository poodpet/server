package com.pood.server.entity.order;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "order_delivery")
public class OrderDelivery{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "주문 항목 번호 : order_db.order.idx")
    @Column(name = "order_idx")
    private Integer orderIdx;

    @Column(name = "order_number")
    private String orderNumber;

    @Column(name = "exchange_number")
    private String exchangeNumber;

    @Column(name = "refund_number")
    private String refundNumber;

    @ApiModelProperty(value = "배송지 별칭(집, 회사 등 )")
    @Column(name = "nickname")
    private String nickname;

    @ApiModelProperty(value = "우편 번호")
    @Column(name = "zipcode")
    private String zipcode;

    @ApiModelProperty(value = "받는자 분 성함")
    @Column(name = "receiver")
    private String receiver;

    @ApiModelProperty(value = "배송지 주소")
    @Column(name = "address")
    private String address;

    @ApiModelProperty(value = "배송지 상세 주소")
    @Column(name = "address_detail")
    private String addressDetail;

    @ApiModelProperty(value = "택배사 이름")
    @Column(name = "courier")
    private String courier;

    @ApiModelProperty(value = "배송 시작일자")
    @Column(name = "startdate")
    private LocalDateTime startdate;

    @ApiModelProperty(value = "[배송정보구분] 10:배송지택배정보, 30:교환회수택배정보, 31:교환배송택배정보, 40:환불회수택배정보, 41:환불배송택배정보")
    @Column(name = "delivery_type")
    private Integer deliveryType;

    @ApiModelProperty(value = "받는자분 핸드폰 주소")
    @Column(name = "phone_number")
    private String phoneNumber;

    @ApiModelProperty(value = "입력 타입 - 0:검색입력, 1:직접입력")
    @Column(name = "input_type")
    private Integer inputType;

    @ApiModelProperty(value = "기본배송지 - 0:기본배송지 아님, 1:기본 배송지 ")
    @Column(name = "default_address")
    private Integer defaultAddress;

    @ApiModelProperty(value = "도서산간타입 - 0:도서산간아님, 1:제주도, 2:기타 섬지역")
    @Column(name = "remote_type")
    private Integer remoteType;

    @ApiModelProperty(value = "정기 배송 지정 날짜")
    @Column(name = "regular_date")
    private LocalDateTime regularDate;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
