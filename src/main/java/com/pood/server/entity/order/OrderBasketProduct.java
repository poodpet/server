package com.pood.server.entity.order;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EntityListeners(AuditingEntityListener.class)
@Table(name = "order_basket_product")
public class OrderBasketProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Long idx;

    @Column(name = "order_basket_idx")
    private Integer orderBasketIdx;

    @Column(name = "product_idx")
    private Integer productIdx;

    @Column(name = "product_qty")
    private Integer productQty;

    @Column(name = "product_price")
    private Integer productPrice;


    @CreatedDate
    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    public OrderBasketProduct(Integer orderBasketIdx, Integer productIdx, Integer productQty, Integer productPrice) {
        this.orderBasketIdx = orderBasketIdx;
        this.productIdx = productIdx;
        this.productQty = productQty;
        this.productPrice = productPrice;
    }
}
