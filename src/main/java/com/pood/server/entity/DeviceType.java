package com.pood.server.entity;

public enum DeviceType {
    AOS(1), IOS(2);

    private int sendType;

    DeviceType(int sendType) {
        this.sendType = sendType;
    }

    public static DeviceType getType(int type) {
        if (type == 1) {
            return DeviceType.AOS;
        }
        return DeviceType.IOS;
    }

    public int getSendType() {
        return sendType;
    }
}
