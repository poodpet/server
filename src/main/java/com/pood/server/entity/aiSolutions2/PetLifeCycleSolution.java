package com.pood.server.entity.aiSolutions2;


import com.pood.server.entity.Triple;
import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.user.UserPet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;


@Deprecated
public class PetLifeCycleSolution {


    static final int SmallAndPuppy = 10;
    static final int SmallAndAdult = 20;
    static final int SmallAndSenior = 30;
    static final int MiddleAndPuppy = 40;
    static final int MiddleAndAdult = 50;
    static final int MiddleAndSenior = 60;
    static final int BigAndPuppy = 70;
    static final int BigAndAdult = 80;
    static final int BigAndSenior = 90;
    static final int DogMixSmall = 100;//10kg 이하
    static final int DogMixMiddle = 101;//23kg 이하
    static final int DogMixBig = 102;// 24kg 이상

    static final int CatKitten = 110;
    static final int CatAdult = 120;
    static final int CatSenior = 130;
    static final int CatMIXKitten = 140;
    static final int CatMIXAdult = 141;
    static final int CatMIXSenior = 142;

    ArrayList<String> imageResource = new ArrayList<>(
        Arrays.asList("balance", "cal", "mineral", "nut")
    );

    public Triple create( UserPet userPet, Pet pscInfo  // Pet class
    ) {
        int type = calPetSizeAndAgeType(pscInfo, userPet);
        return new Triple(lifeCycleComment(type), iconImagePetSizeAge(type), imageCommentText(type));
    }

    ///라이프사이클 코멘트
    ArrayList<String> lifeCycleComment(int petSizeAndAgeType) {
        switch (petSizeAndAgeType) {
            case SmallAndPuppy:
                return new ArrayList<String>(Arrays.asList(petSizeAndAgeCommentText.get(0)));
            case SmallAndAdult:
                return new ArrayList<String>(Arrays.asList(petSizeAndAgeCommentText.get(1)));
            case SmallAndSenior:
                return new ArrayList<String>(Arrays.asList(petSizeAndAgeCommentText.get(2)));

            case MiddleAndPuppy:
                return new ArrayList<String>(Arrays.asList(petSizeAndAgeCommentText.get(3)));
            case MiddleAndAdult:
                return new ArrayList<String>(Arrays.asList(petSizeAndAgeCommentText.get(4)));
            case MiddleAndSenior:
                return new ArrayList<String>(Arrays.asList(petSizeAndAgeCommentText.get(5)));

            case BigAndPuppy:
                return new ArrayList<String>(Arrays.asList(petSizeAndAgeCommentText.get(6)));
            case BigAndAdult:
                return new ArrayList<String>(Arrays.asList(petSizeAndAgeCommentText.get(7)));
            case BigAndSenior:
                return new ArrayList<String>(Arrays.asList(petSizeAndAgeCommentText.get(8)));

            case DogMixSmall:
            case DogMixMiddle:
            case DogMixBig:
                return new ArrayList<String>(Arrays.asList(petSizeAndAgeCommentText.get(9)));

            case CatKitten:
                return new ArrayList<String>(Arrays.asList(petSizeAndAgeCommentText.get(10)));
            case CatAdult:
                return new ArrayList<String>(Arrays.asList(petSizeAndAgeCommentText.get(11)));
            case CatSenior:
                return new ArrayList<String>(Arrays.asList(petSizeAndAgeCommentText.get(12)));
            case CatMIXKitten:
            case CatMIXAdult:
            case CatMIXSenior:
                return new ArrayList<String>(Arrays.asList(petSizeAndAgeCommentText.get(13)));
            default:
                return new ArrayList<String>(Arrays.asList(petSizeAndAgeCommentText.get(0)));

        }

    }

    ///펫 사이즈와 나이에 따른 아이콘 리스트
    ArrayList<String> iconImagePetSizeAge(int petSizeAndAgeType) {
        switch (petSizeAndAgeType) {
            case SmallAndPuppy:
            case SmallAndAdult:
                return new ArrayList<String>(Arrays.asList(imageResource.get(1)));
            case SmallAndSenior:
                return new ArrayList<String>(Arrays.asList(imageResource.get(0)));
            case MiddleAndPuppy:
                return new ArrayList<String>(Arrays.asList(imageResource.get(1)));
            case MiddleAndAdult:
                return new ArrayList<String>(Arrays.asList(imageResource.get(1)));
            case MiddleAndSenior:
                return new ArrayList<String>(Arrays.asList(imageResource.get(0), imageResource.get(2)));
            case BigAndPuppy:
                return new ArrayList<String>(Arrays.asList(imageResource.get(2)));
            case BigAndAdult:
                return new ArrayList<String>(Arrays.asList(imageResource.get(3)));
            case BigAndSenior:
                return new ArrayList<String>(Arrays.asList(imageResource.get(0)));

            case DogMixSmall:
                return new ArrayList<String>(Arrays.asList(imageResource.get(3), imageResource.get(0), imageResource.get(3)));

            case DogMixMiddle:
                return new ArrayList<String>(Arrays.asList(imageResource.get(3), imageResource.get(2), imageResource.get(0)));
            case DogMixBig:
                return new ArrayList<String>(Arrays.asList(imageResource.get(3), imageResource.get(3), imageResource.get(2)));

            case CatKitten:
                return new ArrayList<String>(Arrays.asList(imageResource.get(1)));
            case CatAdult:
                return new ArrayList<String>(Arrays.asList(imageResource.get(3)));
            case CatSenior:
                return new ArrayList<String>(Arrays.asList(imageResource.get(0)));

            case CatMIXKitten:
                return new ArrayList<String>(Arrays.asList(imageResource.get(3), imageResource.get(0), imageResource.get(3)));
            case CatMIXAdult:
                return new ArrayList<String>(Arrays.asList(imageResource.get(0), imageResource.get(2), imageResource.get(3)));
            case CatMIXSenior:
                return new ArrayList<String>(Arrays.asList(imageResource.get(2), imageResource.get(3), imageResource.get(3)));

            default:
                return new ArrayList<String>(Arrays.asList(imageResource.get(1)));
        }
    }

    ///아이콘 이미지 옆에 붙은 텍스트
    ArrayList<String> imageCommentText(int petSizeAndAgeType) {
        switch (petSizeAndAgeType) {
            case SmallAndPuppy:
            case SmallAndAdult:
                return new ArrayList<String>(Arrays.asList("고칼로리 식단"));
            case SmallAndSenior:
                return new ArrayList<String>(Arrays.asList("초고단백, 초고지방 식단 배제"));

            case MiddleAndPuppy:
            case MiddleAndAdult:
                return new ArrayList<String>(Arrays.asList("고칼로리 식단"));
            case MiddleAndSenior:
                return new ArrayList<String>(Arrays.asList("초고단백, 초고지방 식단 배제", "인이 높은 식단 배제"));

            case BigAndPuppy:
                return new ArrayList<String>(Arrays.asList("칼슘, 인이 높은 식단 배제"));
            case BigAndAdult:
                return new ArrayList<String>(Arrays.asList("유산균이 풍부한 식단"));
            case BigAndSenior:
                return new ArrayList<String>(Arrays.asList("초고단백, 초고지방 식단 배제"));
            case DogMixSmall:
                return new ArrayList<String>(Arrays.asList("글루코사민, 콘드로이틴, MSM 함유", "단일 단백질 식단 적용", "D-만노스 함유"));

            case DogMixMiddle:
                return new ArrayList<String>(Arrays.asList("글루코사민, 콘드로이틴, MSM 함유",
                    "철, 아연, 구리가 풍부한 식단",
                    "고단백 식단 배제", "단일 단백질 식단 적용", "D-만노스 함유"));

            case DogMixBig:
                return new ArrayList<String>(Arrays.asList("오메가3가 풍부한 식단", "오메가3가 풍부한 식단", "칼슘, 인이 높은 식단 배제"));
            default:
                return new ArrayList<String>(Arrays.asList("고칼로리 식단"));

        }
    }

    // 0 : 퍼피, 1 : 어덜트, 2 : 시니어
    int calPetSizeAndAgeType(Pet pscInfo, UserPet userPet) {
        int result = 0;

        int ageType = calcPetBirthType(   userPet.getPetBirth().toString());

        //강아지
        if (pscInfo.getPcId() == 1) {
            String petSize = pscInfo.getPcSize();

            //믹스 먼저 필터링
            if (pscInfo.getPcTag().contains("믹스")) {

                if (ageType == 0) {
                    if (petSize.contains("소형")) {
                        result = DogMixSmall;
                    } else if (petSize.contains("중형")) {
                        result = DogMixMiddle;
                    } else {
                        result = DogMixBig;
                    }
                    result = DogMixSmall;
                } else if (ageType == 1) {
                    result = DogMixMiddle;
                } else {
                    result = DogMixBig;
                }

            } else if (petSize.contains("소형")) {
                if (ageType == 0) result = SmallAndPuppy;
                else if (ageType == 1) result = SmallAndAdult;
                else result = SmallAndSenior;
            } else if (petSize.contains("중형")) {
                if (ageType == 0) result = MiddleAndPuppy;
                else if (ageType == 1) result = MiddleAndAdult;
                else result = MiddleAndSenior;
            } else if (petSize.contains("대형")) {
                if (ageType == 0) result = BigAndPuppy;
                else if (ageType == 1) result = BigAndAdult;
                else result = BigAndSenior;
            }

        } else {

            //고양이
            if (pscInfo.getPcTag().contains("믹스")) {
                if (ageType == 0) result = CatMIXKitten;
                else if (ageType == 1) result = CatMIXAdult;
                else result = CatMIXSenior;
            } else {
                if (ageType == 0) result = CatKitten;
                else if (ageType == 1) result = CatAdult;
                else result = CatSenior;
            }
        }

        return result;
    }

    //펫 나이에 대한 타입 세팅하기
    // 0 : 퍼피, 1 : 어덜트, 2 : 시니어
    int calcPetBirthType(String petBirth) {
        int type = 0;
        int age = getPetAge(petBirth.replaceAll("-", ""));
        if (age < 8) {
            type = 1;
        } else if (age < 30) {
            type = 2;

        } else {
            type = 0;
        }
        return type;

    }

    //몇살인지 계산
    int getPetAge(String petBirth) {
        if (petBirth.equals("") || petBirth.equals("-")) return 0;
        Calendar cl = Calendar.getInstance();
        cl.set(Calendar.YEAR, Integer.parseInt(petBirth.substring(0, 4)));
        cl.set(Calendar.MONTH, Integer.parseInt(petBirth.substring(4, 6)) - 1);
        cl.set(Calendar.DAY_OF_MONTH, Integer.parseInt(petBirth.substring(6, 8)));

        long MINUTE_MS = 60 * 1000;
        long HOUR_MS = MINUTE_MS * 60;
        long DAY_MS = HOUR_MS * 24;
        long MONTH_MS = HOUR_MS * 24L * 30L;

        Calendar today = Calendar.getInstance();
        long month = (today.getTimeInMillis() - cl.getTimeInMillis()) / MONTH_MS;
        return (int) month / 12;

    }


    //생애주기
    //펫사이즈 및 나이
    //0 :소형견 퍼피
    //1 :소형견 어덜트
    //2 :소형견 시니어
    //3 :중형견 퍼피
    //4 :중형견 어덜트
    //5 :중형견 시니어
    //6 :대형견 퍼피
    //7 :대형견 어덜트
    //8 :대형견 시니어
    //9 :믹스견
    //10 :고양이 키튼
    //11 :고양이 어덜트
    //12 :고양이 시니어
    //13 :믹스묘
    ArrayList<String> petSizeAndAgeCommentText = new ArrayList<>(
        Arrays.asList(
            "소형견 퍼피는 짧은 기간 빠르게 성장합니다. 뼈를 튼튼하게 하기 위해 칼슘, 인 등 많은 미네랄을 섭취해야합니다. " +
                "근육과 올바른 성장을 위해 많은 단백질과 필수지방산이 필요합니다. 성견에 비하여 위가 작아서 고칼로리 식단을 먹어야 합니다.",
            "소형견 어덜트는 긴 기간 주식으로 섭취해도 영양이 부족하거나 과도하지 않도록 균형잡힌 식단이 필요합니다. " +
                "대형견보다 위가 작기 때문에 적은 양을 먹어도 많은 칼로리를 줄 수 있는 고칼로리 식단을 먹어야 합니다.",
            "소형견 시니어는 소화능력과 노폐물 배설 능력이 떨어지기 때문에 지나친 고단백 고지방 사료는 추천하지 않습니다. " +
                "많은 항산화물질을 넣는 경우가 많습니다. " +
                "또한 관절염이 있는 경우가 많아서 특정 영양소(글루코사민, 콘드로이틴, MSM, 오메가3, 녹색입홍합 등)이 필요합니다.",
            "중형견 퍼피는 짧은 기간 빠르게 성장합니다. 뼈를 튼튼하게 하기 위해 칼슘, 인 등 많은 미네랄을 섭취해야합니다. " +
                "근육과 올바른 성장을 위해 많은 단백질과 필수지방산이 필요합니다. " +
                "성견에 비하여 위가 작아서 적은 양으로도 많은 칼로리를 줄 수 있는 고칼로리 식단을 먹어야 합니다.",
            "중형견 어덜트는 긴 기간 주식으로 섭취해도 영양이 부족하거나 과도하지 않도록 균형잡힌 식단이 필요합니다. " +
                "대형견보다 위가 작기 때문에 적은 양을 먹어도 많은 칼로리를 줄 수 있는 고칼로리 식단을 먹어야 합니다.",
            "중형견 시니어는 소화능력과 노폐물 배설 능력이 떨어지기 때문에 지나친 초고단백, 초고지방 식단은 피해야합니다. " +
                "신장건강을 위해 인을 미리 제한하는 것이 좋습니다. 많은 항산화물질을 넣는 경우가 많습니다. " +
                "또한 관절염이 있는 경우가 많아서 특정 영양소(글루코사민, 콘드로이틴, MSM, 오메가3, 녹색입홍합 등)이 필요합니다.",
            "대형견 퍼피는 미네랄 중 칼슘과 인이 적당해야합니다. 많은 칼슘과 인의 섭취는 오히려 체내에서 뼈를 녹여내는 등 병을 유발하기 때문에 주의가 필요합니다. " +
                "근육과 올바른 성장을 위해 많은 단백질과 필수지방산이 필요합니다. " +
                "성견에 비하여 위가 작아서 적은 양으로도 많은 칼로리를 줄 수 있는 고칼로리 식단을 먹어야 합니다. " +
                "또한 대형견 퍼피는 적정 몸무게에 도달할 때까지 1년 이상 퍼피사료를 먹이는 것을 추천합니다.",
            "대형견 어덜트는 소형견에 비하여 변이 무른 편입니다. " +
                "식이섬유 중 셀룰로오스와 같은 불용성 식이섬유 비중이 높은 것이 좋습니다. " +
                "또한 소화가 잘되는 좋은 단백질 원료와 유산균 함량이 높은 사료가 도움이 됩니다.",
            "대형견 시니어는 소화능력과 노폐물 배설 능력이 떨어지기 때문에 지나친 초고단백, 초고지방 식단은 피해야합니다. " +
                "많은 항산화물질을 넣는 경우가 많습니다. " +
                "또한 관절염이 있는 경우가 많아서 특정 영양소(글루코사민, 콘드로이틴, MSM, 오메가3, 녹색입홍합 등)이 필요합니다.",
            "믹스견은 서로 다른 부모로부터 다양한 유전자가 섞인 덕분에 품종견보다 건강하다고 알려져 있습니다. " +
                "하지만 최근 미국 연구에 따르면 불독과 같이 코가 많이 눌린 종을 제외하면 품종견과 믹스견의 질병 발생 차이가 크게 없습니다. " +
                "그러므로 믹스견이니 건강할 것이라 생각하기보다는 나이가 들며 생길 수 있는 질병들(관절염, 신장질환 등)에 관심을 갖고 주기적인 건강검진으로 예방하는 것이 좋습니다.",
            "키튼은 짧은 기간 빠르게 성장합니다. 뼈를 튼튼하게 하기 위해 칼슘, 인 등 많은 미네랄을 섭취해야합니다. " +
                "근육과 올바른 성장을 위해 많은 단백질과 필수지방산이 필요합니다. " +
                "성묘에 비하여 위가 작아서 적은 양으로도 많은 칼로리를 줄 수 있는 고칼로리 식단을 먹어야합니다.",
            "긴 기간 주식으로 섭취해도 영양이 부족하거나 과도하지 않도록 균형잡힌 식단이 필요합니다. " +
                "특히 고양이는 강아지와 달리 철저한 육식동물이므로 타우린, 단백질 및 필수지방산 요구량이 다릅니다. " +
                "강아지사료를 고양이에게 장기간 급여 시 심장질환의 위험이 있습니다.",
            "고양이 시니어는 근육량이 떨어지고 체중이 줄어들 가능성이 높기 때문에 높은 단백 식단이 필요합니다. " +
                "많은 항산화물질을 넣는 경우가 많습니다. " +
                "또한 관절염이 있는 경우가 많아서 특정 영양소(글루코사민, 콘드로이틴, MSM, 오메가3, 녹색입홍합 등)이 필요합니다",
            "믹스묘는 서로 다른 부모로부터 다양한 유전자가 섞인 덕분에 품종묘보다 건강하다고 알려져 있습니다. " +
                "하지만 최근 미국 연구에 따르면 페르시안과 같이 코가 많이 눌린 종을 제외하면 품종묘과 믹스묘의 질병 발생 차이가 크게 없습니다. " +
                "그러므로 믹스묘라 건강할 것이라 생각하기보다는 나이가 들며 생길 수 있는 질병들(관절염, 신장질환 등)에 관심을 갖고 주기적인 건강검진으로 예방하는 것이 좋습니다"
        )
    );
}
