package com.pood.server.entity.aiSolutions2;


import com.pood.server.entity.UserPetCupInfo;
import com.pood.server.entity.calcUserPetInfo.CalcUserPetInfo;
import com.pood.server.entity.calcUserPetInfo.CalcUserPetInfoCat;
import com.pood.server.entity.calcUserPetInfo.CalcUserPetInfoDog;
import com.pood.server.entity.meta.Goods;
import com.pood.server.entity.meta.GoodsProduct;
import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.meta.Product;
import com.pood.server.entity.meta.Snack;
import com.pood.server.entity.user.UserPet;
import com.pood.server.entity.user.UserPetAllergy;
import com.pood.server.entity.user.UserPetGrainSize;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;


@Deprecated
public class ProductUserPetAnalysis {


    final static int SNACK_TYPE_PER = 0; //개
    final static int SNACK_TYPE_GRAM = 1;//그람
    final static int SNACK_TYPE_ML = 2;//ml
    final static int SNACK_TYPE_NUM = 3; //회

    //type 0 :개, 1 : g, 2 :ml, 3 :회
    final static int FEEDING_DETAIL_TYPE_NUM = 0;
    final static int FEEDING_DETAIL_TYPE_GRAM = 1;
    final static int FEEDING_DETAIL_TYPE_ML = 2;
    final static int FEEDING_DETAIL_TYPE_COUNT = 3;


    final static String PACKAGE_TYPE_CAN = "C";
    final static String PACKAGE_TYPE_CUP = "U";
    final static String PACKAGE_TYPE_NORMAL = "N";
    final static String PACKAGE_TYPE_POUCH = "P";

    ArrayList<String> dogSizeList = new ArrayList(Arrays.asList("소형견", "중형견", "대형견"));
//    Goods

    Product product;  //신규 세팅
    GoodsProduct goodsProduct;  //신규 세팅
    Pet pet;  //신규 세팅

    Goods poodGoods;
    UserPet userPetInfo;
    Snack feedingDetail;
    String petName = "";

    String cupType = "";
    String cupSubType = "";

    float dailyCupAnalysisResult = 0f; // 하루 사료 섭취량
    int allDayAnalysisResult = 0;// 먹는데 걸리는 기간
    double dailySnackAnalysisResult = 0f;//하루 간식 섭취량

    public float getDailyCupAnalysisResult() {
        return dailyCupAnalysisResult;
    }

    public int getAllDayAnalysisResult() {
        return allDayAnalysisResult;
    }

    String petSizeAnalysisResultText = "";
    String ageAnalysisResultText = "";
    String petNameOfAge = ""; // 어린 강아지, 성견, 성묘 등
    String grainSizeAnalysisResultText = "";
    String allergyAnalysisText = "";

    String feedText = "";
    String feedTextEndPoint = "을 나누어 급여해주세요";
    String cupDayAnalysisText = "";
    String nutriTitleText = "";
    int checkCount = 0;  // 0,1 : 잘 맞지 않아요, 2 : 적당해요, 3,4 : 잘 맞아요
    String nutriSubTitleText = "";

    float cupSize = 0f;

    float petCalorie = 0f;
    float feedGram = 0f;


    public ProductUserPetAnalysis(Goods poodGoods, UserPet userPetInfo, Snack feedingDetail, Product product,  GoodsProduct goodsProduct, Pet pet) {
        this.poodGoods = poodGoods;
        this.userPetInfo = userPetInfo;
        this.feedingDetail = feedingDetail;

        this.product = product;
        this.goodsProduct = goodsProduct;
        this.pet = pet;
        CalcUserPetInfo calcUserPetInfo  = pet.getPcId() ==1 ?new CalcUserPetInfoDog() : new CalcUserPetInfoCat();
        calcUserPetInfo.setFeedNutrition(pet.getPcId(),product, product.getCupWeight(), userPetInfo);
        this.petCalorie = calcUserPetInfo.getPetCalorie();
        this.feedGram = calcUserPetInfo.getFeedGram();

        cupSize = calculateCupSize();
        petName = userPetInfo.getPetName();

        //펫사이즈 적합도 분석
//        analysisPetSizeMatch(product.getFeedTarget());
//
//        //나이적합분석
//        analysisPetAgeMatch();
//
//        //유저 펫 알러지 분석
//        analysisAllergy();
//
//        //알갱이 크기 분석
//        analysisUnitSizeMatch();

        //섭취 수량 관련 세팅
        setIntakeAmountText();

        //적합도 타이틀 텍스트 세팅
        nutriTitleText = petName + "에게 " + analysisLastMatch();

    }

    //적합도 타이틀 텍스트 세팅
    String analysisLastMatch(){
        switch (checkCount){
            case 0:
            case 1:
                return "잘 맞지 않아요";
            case 2:
                return "적당해요";
            default:
                return "잘 맞아요";

        }
    }


    /**
     * 컵 사이즈 계산
     * product에 입력된 cupWeigth가 없다면 100으로 셋팅
     *
     * @return
     */
    float calculateCupSize() {
        if (product.getCupWeight() > 0) {
            return (float) product.getCupWeight();
        } else {
            return 100f;
        }
    }

    /**
     * 하루 사료 섭취량 구하기
     *
     * @return
     */
    float calculateFeedIntakeAmountPerDay() {
        return calculateIntakeRage((float) Math.round(this.feedGram  * 10) / 10);
//    return 0;
    }

    /**
     * 하루 간식 섭취량 구하기
     *
     * @return
     */
    float calculateSnackIntakeAmountPerDay() {
        return calculateIntakeRage((float) (this.petCalorie * 0.1) / feedingDetail.getPerCal());
//        return 0;
    }

    /**
     * 섭취량 범위 체크
     * 소수점 첫째자리만 사용하기 위해서 VALUE 에 10을 곱해주고 그 뒤의 소수점은 모두 버린다.
     *
     * @param value
     * @return
     */
    float calculateIntakeRage(double value) {
        float rValue = (float) (Math.floor(value * 10) / 10f);
        if (rValue >= 0f && rValue <= 0.7f) return 0.5f;
        else if (rValue >= 0.8f && rValue <= 1.2f) return 1f;
        else if (rValue >= 1.3f && rValue <= 1.7f) return 1.5f;
        else if (rValue >= 1.8f && rValue <= 2.2f) return 2f;
        else if (rValue >= 2.3f && rValue <= 2.7f) return 2.5f;
        else if (rValue >= 2.8f && rValue <= 3.2f) return 3f;
        else if (rValue >= 3.3f && rValue <= 3.7f) return 3.5f;
        else if (rValue >= 3.8f && rValue <= 4.2f) return 4f;
        else if (rValue >= 4.3f && rValue <= 4.7f) return 4.5f;
        else if (rValue >= 4.8f && rValue <= 5.2f) return 5f;
        else if (rValue >= 5.3f && rValue <= 5.7f) return 5.5f;
        else if (rValue >= 5.8f && rValue <= 6.2f) return 6f;
        else if (rValue >= 6.3f && rValue <= 6.7f) return 6.5f;
        else if (rValue >= 6.8f && rValue <= 7.2f) return 7f;
        else if (rValue >= 7.3f && rValue <= 7.7f) return 7.5f;
        else if (rValue >= 7.8f && rValue <= 8.2f) return 8f;
        else if (rValue >= 8.3f && rValue <= 8.7f) return 8.5f;
        else if (rValue >= 8.8f && rValue <= 9.2f) return 9f;
        else if (rValue >= 9.3f && rValue <= 9.7f) return 9.5f;
        else if (rValue >= 9.8f && rValue <= 10.2f) return 10f;
        else if (rValue >= 10.3f && rValue <= 10.7f) return 10.5f;
        else if (rValue >= 10.8f && rValue <= 11.2f) return 11f;
        else if (rValue >= 11.3f && rValue <= 11.7f) return 11.5f;
        else if (rValue >= 11.8f && rValue <= 12.2f) return 12f;
        else return 12f;
    }

    /**
     * 펫 사이즈 적합도 분석
     * feedTarget = 0 : 모두, 1 : 소, 2 : 중, 3 : 대
     *
     * @param feedTarget
     * @return
     */
    void analysisPetSizeMatch(int feedTarget) {
        boolean value = true;
        if (feedTarget == 0) {
            value = true;
        } else {
            String userPetSize = pet.getPcSize();

            int targetNum = 0;
            for (int i = 0; i < dogSizeList.size(); i++) {
                if (userPetSize.equals(dogSizeList.get(i))) {
                    targetNum = i + 1;
                    break;
                }
            }
            value = targetNum == feedTarget;
        }

        String result = "";
        if (value) {
            checkCount++;
            result = OK;
        } else {
            result = NO;
        }
        petSizeAnalysisResultText = pet.getPcSize()+ "에 " + result;
    }


    /**
     * 나이 적합 분석
     * @return
     */
    void analysisPetAgeMatch() {

        int petMonth = getNowDateMonth( userPetInfo.getPetBirth().toString());

        //펫 나이에 따른 명칭 세팅
        petNameOfAge = calculatePetNameAccordingToAge(petMonth);

        //펫 나이에 따른 적합도 분석
        boolean value = analysisPetAgeMatch(petMonth, product.getFeedType());
        String result = "";
        if (value) {
            checkCount++;
            result = OK;
        } else {
            result = NO;
        }
        ageAnalysisResultText = petNameOfAge + "에 " + result;


    }

    final static String OK = "잘 맞아요";
    final static String NO = "맞지 맞아요";


    /**
     * 펫 개월수 구하기
     *
     * @param date
     * @return
     */
    private int getNowDateMonth(String date) {
        if (date.equals("") || date.equals("-")) return 0;

        Calendar value = Calendar.getInstance();
        List<String> dateList = Arrays.asList(userPetInfo.getPetBirth().toString().split("-"));
        value.set(Calendar.YEAR, Integer.parseInt(dateList.get(0)));
        value.set(Calendar.MONTH, Integer.parseInt(dateList.get(1)));
        value.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateList.get(2)));

        long MINUTE_MS = 60 * 1000;
        long HOUR_MS = 60 * MINUTE_MS;
        long MONTH_MS = HOUR_MS * 24L * 30L;
        Calendar today = Calendar.getInstance();
        long month = (today.getTimeInMillis() - value.getTimeInMillis()) / MONTH_MS;
        return (int) month;

    }


    /**
     * 펫 나이에 따라서 명칭 세팅
     *
     * @param petMonth
     * @return
     */
    private String calculatePetNameAccordingToAge(int petMonth) {
        if (petMonth >= 0 && petMonth < 12) {
            switch (userPetInfo.getPcId()) {
                case 1:
                default:
                    return "어린 강아지";
                case 2:
                    return "어린 고양이";
            }
        } else if (petMonth >= 12 && petMonth < 83) {
            switch (userPetInfo.getPcId()) {
                case 1:
                default:
                    return "성견";
                case 2:
                    return "성묘";
            }
        } else {
            switch (userPetInfo.getPcId()) {
                case 1:
                default:
                    return "노령견";
                case 2:
                    return "노령묘";
            }
        }
    }


    //펫 타입과 나이에 따른 적합도 분석
    private boolean analysisPetAgeMatch(int petMonth, String feedType) {
        boolean result = true;
        switch (feedType) {
            case "P":
                if (petMonth > 12) result = false;
                break;
            case "A":
                if (petMonth <= 12 || petMonth > 84) result = false;
                break;
            case "S":
                if (petMonth < 84) result = false;
                break;
            case "PA":
                if (petMonth > 84) result = false;
                break;
            case "PAL":
                if (petMonth > 84 && pet.getPcSize().contains("대형")) result = false;
                break;
            case "AS":
                if (petMonth < 12) result = false;
                break;
        }
        return result;
    }


    /**
     * 유저 펫 알러지 적합도 분석
     *
     * @return
     */
    private void analysisAllergy() {
        String animalProtein = product.getAnimalProtein();
        ArrayList<UserPetAllergy> allergyData = userPetInfo.getAllergyList();

        boolean result = true;
        if (allergyData == null) {
            result = true;
        } else {
            int count = 0;
            for (int i = 0; i < allergyData.size(); i++) {
                if (allergyData.get(i).getName().contains(animalProtein)) {
                    count++;
                }
            }
            result = count == 0;
        }

        if (result) {
            checkCount++;
            allergyAnalysisText = "알러지 가능성이 없어요";
        } else {
            allergyAnalysisText = "알러지 가능성이 있어요";
        }
    }


    /**
     * 펫이 선호하는 알갱이 크기인지 아닌지 분석
     *
     * @return
     */
    private void analysisUnitSizeMatch() {
        float unitSize = product.getUnitSize().floatValue();
        ArrayList<UserPetGrainSize> unitSizeList = userPetInfo.getGrainSizeList();
//userPetInfo.getGrainSize()
        int count = 0;
        if (unitSizeList != null) {
            for (int i = 0; i < unitSizeList.size(); i++) {
                if (unitSize >= unitSizeList.get(i).getSizeMin() && unitSize <= unitSizeList.get(i).getSizeMax()) {
                    count++;
                }
            }
        }
        if (count > 0) {
            checkCount++;
            grainSizeAnalysisResultText = "선호하는 알갱이 크기에요";
        } else {
            grainSizeAnalysisResultText = "선호하지 않는 알갱이 크기에요";
        }
    }


    //섭취 수량 관련 세팅
    // 사료(0), 샘플스토어(14), 간식(1), 영양제(2) 만 분석
    void setIntakeAmountText() {
        switch (product.getCtIdx()) {
            case 0:
            case 14:

                //하루 사료 섭취량
                dailyCupAnalysisResult = calculateFeedIntakeAmountPerDay();

                //섭취수량 텍스트 세팅
                feedText = setFeedText();

                //섭취 기간 분석
                allDayAnalysisResult = analysisIntakePeriod(dailyCupAnalysisResult, null);

                nutriSubTitleText = "입력하신 정보와 제품의 정보를 비교해서 " + petName + "에게 이 제품이 잘 맞는지 분석했어요";

                break;

            case 1:
            case 2:
                //min max 빈값일 경우 = -1
                if (feedingDetail != null) {
                    double maxValue = feedingDetail.getMax();
                    double minValue = feedingDetail.getMin();

                    // gram으로 나오는 경우 ex) (캣만두 펫칼로리 *0.1 ) /per_grams
                    if (feedingDetail.getType() == SNACK_TYPE_GRAM) {
                        dailySnackAnalysisResult = Math.round(calculateIntakeRage((petCalorie * 0.1) / feedingDetail.getPerCal()));

                        // max 있을 경우 하루간식섭취량구하기() 계산해서 max 넘으면 max값으로 세팅
                    } else if (maxValue > 0) {

                        if (calculateSnackIntakeAmountPerDay() > maxValue) {
                            dailySnackAnalysisResult = calculateIntakeRage(maxValue);
                        } else {

                            dailySnackAnalysisResult = calculateIntakeRage(calculateSnackIntakeAmountPerDay());
                            if(dailySnackAnalysisResult >= 12f){
                                dailySnackAnalysisResult = maxValue;
                            }
                        }

                        // max 없을 경우(-1) -> (펫칼로리 *0.1) / per_cal
                    } else if (maxValue == -1) {
                        dailySnackAnalysisResult = calculateSnackIntakeAmountPerDay();

                        // min max 같을 경우 min 값으로 세팅
                    } else if (maxValue == minValue) {
                        dailySnackAnalysisResult = calculateIntakeRage(minValue);

                    } else if (calculateSnackIntakeAmountPerDay() < feedingDetail.getMin()) {
                        dailySnackAnalysisResult = calculateIntakeRage(feedingDetail.getMax());

                    } else {
                        dailySnackAnalysisResult = calculateSnackIntakeAmountPerDay();
                    }


                }

                // dailySnackAnalysisResult 값이 0보다 크면서 min보다 작을 경우 min값으로 사용
                if (dailySnackAnalysisResult <= feedingDetail.getMin() &&
                    feedingDetail.getMin() > 0) {
                    dailySnackAnalysisResult = feedingDetail.getMin();
                }

                //dailySnackAnalysisResult 값이 max보다 크면서 max 값이 0보다 클 경우 max값으로 사용
                if (dailySnackAnalysisResult >= feedingDetail.getMax() && feedingDetail.getMax() > 0) {
                    dailySnackAnalysisResult = feedingDetail.getMax();
                }
                setSnackText(feedingDetail.getType(), dailySnackAnalysisResult);

                //하루 사료 섭취량
//                dailyCupAnalysisResult = calculateFeedIntakeAmountPerDay();

                allDayAnalysisResult = analysisIntakePeriod(dailySnackAnalysisResult, feedingDetail);
                break;
        }

        if (allDayAnalysisResult <= 1) {
            cupDayAnalysisText = petName + "의 하루 섭취량으로 분석한 결과 해당 상품을 다 먹으려면 하루 정도 걸려요";
        } else {
            cupDayAnalysisText = petName + "의 하루 섭취량으로 분석한 결과 해당 상품을 다 먹으려면 " + allDayAnalysisResult + "일 정도 걸려요";
        }

    }


    public UserPetCupInfo getUserPetCupInfo(){
        return new UserPetCupInfo(this.cupType,this.cupSubType);
    }

    /**
     * 사료섭취수량 텍스트 세팅
     *
     * @return
     */
    public String setFeedText() {
//        switch (product.getPackageType()) {
//            case PACKAGE_TYPE_NORMAL:
//            case PACKAGE_TYPE_CUP:
//            default:
//                this.cupType = "종이컵";
//                this.cupSubType = "컵";
//                break;
//            case PACKAGE_TYPE_CAN:
//                this.cupType = "캔";
//                this.cupSubType = "캔";
//                break;
//            case PACKAGE_TYPE_POUCH:
//                this.cupType = "파우치";
//                this.cupSubType = "팩";
//                break;
//        }

        String feedText = "";
        if (dailyCupAnalysisResult <= 0.5) {
            if (product.getPackageType().equals(PACKAGE_TYPE_NORMAL) ||
                product.getPackageType().equals(PACKAGE_TYPE_CUP)
            ) {
                feedText = "하루에 " + cupType + " 반" + this.cupSubType + feedTextEndPoint;
            } else {
                feedText = "하루에 반" + this.cupSubType + feedTextEndPoint;
            }
        } else {

            if (dailyCupAnalysisResult % 1.0 == 0.0) {

                if (product.getPackageType().equals(PACKAGE_TYPE_NORMAL) ||
                    product.getPackageType().equals(PACKAGE_TYPE_CUP)
                ) {
                    feedText = "하루에 " + cupType + " " + (int) Math.floor(dailyCupAnalysisResult) + this.cupSubType + feedTextEndPoint;
                } else {
                    feedText = "하루에 " + (int) Math.floor(dailyCupAnalysisResult) + this.cupSubType + feedTextEndPoint;
                }

            } else {

                if (product.getPackageType().equals(PACKAGE_TYPE_NORMAL) ||
                    product.getPackageType().equals(PACKAGE_TYPE_CUP)
                ) {
                    feedText = "하루에 " + cupType + " " + (int) Math.floor(dailyCupAnalysisResult) + this.cupSubType + " 반" + feedTextEndPoint;
                } else {
                    feedText = "하루에 " + (int) Math.floor(dailyCupAnalysisResult) + this.cupSubType + " 반" + feedTextEndPoint;
                }
            }
        }
        return feedText;
    }


    /**
     * 간식 텍스트 세팅 포장 방식에 따라 텍스트가 달라짐
     *
     * @param type
     * @param dailySnackAnalysisResult
     */
    void setSnackText(int type, double dailySnackAnalysisResult) {

//        cupType = "";
//        cupSubType = "";

        switch (type) {
            case SNACK_TYPE_PER:
            default:
                cupType = "개";
                cupSubType = "를";
                break;
            case SNACK_TYPE_GRAM:
                cupType = "g";
                cupSubType = "을";
                break;
            case SNACK_TYPE_ML:
                cupType = "ml";
                cupSubType = "를";
                break;
            case SNACK_TYPE_NUM:
                cupType = "회";
                cupSubType = "를";
                break;
        }

        if (dailySnackAnalysisResult <= 0.5) {
            feedText = "하루에 최대 반" + cupType + cupSubType + feedTextEndPoint;
        } else {
            if (dailySnackAnalysisResult % 1.0 == 0.0) {
                feedText = "하루에 최대 " + dailySnackAnalysisResult + cupType + cupSubType + feedTextEndPoint;
            } else {
                feedText = "하루에 최대 " + dailySnackAnalysisResult + cupSubType + "반" + feedTextEndPoint;
            }
        }
    }


    /**
     * 섭취 기간 분석
     *
     * @param dailyEat
     * @param feedingDetail
     * @return
     */
    int analysisIntakePeriod(double dailyEat, Snack feedingDetail) {
        double period = 0f;
        float feedWeight = 0f;
        if (feedingDetail == null) {
            feedWeight = goodsProduct.getProductQty() * product.getWeight();
            period = (feedWeight / cupSize) / dailyEat;

        } else {
            //사료무게
            if (product.getWeight() == 0) {
                feedWeight = (float) (goodsProduct.getProductQty() * feedingDetail.getPerGrams());
            } else {
                feedWeight = goodsProduct.getProductQty() * product.getWeight();
            }

            if (feedingDetail.getType() == FEEDING_DETAIL_TYPE_NUM) {
                period = (float) (feedWeight / (feedingDetail.getPerGrams() * dailyEat));
            } else if (feedingDetail.getType() == FEEDING_DETAIL_TYPE_GRAM) {
                period = feedWeight / dailyEat;
            }
        }

        return (int) period;
    }

}
