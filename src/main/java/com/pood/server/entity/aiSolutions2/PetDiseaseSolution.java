package com.pood.server.entity.aiSolutions2;


import com.pood.server.entity.TripleAndType;
import java.util.ArrayList;


@Deprecated
public class PetDiseaseSolution {

    //유전질병
    public TripleAndType create(
        String petName, int diseaseGroup
    ) {
        PetDiseaseType petDiseaseType = getPetDiseaseType(diseaseGroup);
        return new TripleAndType(diseaseComment(petName, petDiseaseType),
            null,
            petDiseaseType.getReferList(),
            petDiseaseType.getName());
    }

    String converType(int diseaseGroup) {
        return getPetDiseaseType(diseaseGroup).getName();
    }

    ///코멘트 세팅
    ArrayList<String> diseaseComment(String petName, PetDiseaseType petDiseaseType) {
        ArrayList<String> resultList = new ArrayList<>();
        String comment = petDiseaseType.getComment();
        if (!comment.isEmpty()) {
            resultList.add(String.format(comment, petName));
        }
        return resultList;
    }

    private PetDiseaseType getPetDiseaseType(int diseaseGroup) {
        return PetDiseaseType.stream()
            .filter(petDiseaseType -> petDiseaseType.getCode() == diseaseGroup).findFirst()
            .orElse(PetDiseaseType.ETC);
    }


}
