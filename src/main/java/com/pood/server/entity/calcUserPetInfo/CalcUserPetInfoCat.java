package com.pood.server.entity.calcUserPetInfo;

import com.pood.server.entity.meta.Product;
import com.pood.server.entity.user.UserPet;
import java.util.Arrays;

@Deprecated
public class CalcUserPetInfoCat implements CalcUserPetInfo{


    static float cupWidget = 100f;

    float petCalorie = 0f; // 반려동물의 하루치 칼로리
    int cupCalorie = 0; // 한컵 칼로리
    float feedGram = 0f; //  펫칼로리를 컵칼로리로 나눈 수량
    float foodGram = 0f; // 제품에 대한 동물이 먹어야 하는 그램
    float kg1 = 1000f;

    public void setFeedNutrition(int pcId, Product product, float cupSize, UserPet userPetInfo) {
        float weight = 0f;
        int month = getAgeMonth(userPetInfo.getPetBirth().toString().replace("-", ""));

        if (Arrays.asList(0,1).contains(month)) {
            weight = 240f;
        } else if (month == 2) {
            weight = 210f;
        }else if (month == 3) {
            weight = 200f;
        }else if (month == 4) {
            weight = 175f;
        }else if (month == 5) {
            weight = 145f;
        }else if (month == 6) {
            weight = 135f;
        }else if (month == 7) {
            weight = 120f;
        }else if (month == 8) {
            weight = 110f;
        }else if (month == 9) {
            weight = 100f;
        }else if (month == 10) {
            weight = 95f;
        }else if (month == 11) {
            weight = 90f;
        }else if (month == 12) {
            weight = 85f;
        }else  {
            //1: 남아, 2: 여아, 3 : 남아 중성화, 4: 여아 중성화
            if(Arrays.asList(1,2).contains(userPetInfo.getPetGender())){
                weight = 77.6f;
            }else{
                weight = 62f;
            }
        }
        if(month >= 13)
            petCalorie = (float) (weight * (Math.pow(userPetInfo.getPetWeight(), 0.711f)));
        else
            petCalorie = (float) (weight * (userPetInfo.getPetWeight()));

        cupCalorie = (int) ((product.getCalorie() / kg1) * cupSize);
        feedGram = petCalorie / cupCalorie;
        foodGram = petCalorie / (product.getCalorie() / kg1);

    }

    public float getPetCalorie(){
        return this.petCalorie;
    }

    public float getFeedGram() {
        return feedGram;
    }

}
