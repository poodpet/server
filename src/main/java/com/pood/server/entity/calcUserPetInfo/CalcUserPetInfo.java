package com.pood.server.entity.calcUserPetInfo;

import com.pood.server.entity.meta.Product;
import com.pood.server.entity.user.UserPet;
import java.util.Calendar;

@Deprecated
public interface CalcUserPetInfo {

    public void setFeedNutrition(int pcId, Product product, float cupSize, UserPet petInfo);

    //반려동물 개월수 계산
    default int getAgeMonth(String petBirth) {
        Calendar newDate = Calendar.getInstance();
        newDate.set(Calendar.YEAR, Integer.parseInt(petBirth.substring(0, 4)));
        newDate.set(Calendar.MONTH, Integer.parseInt(petBirth.substring(4, 6)) - 1);
        newDate.set(Calendar.DAY_OF_MONTH, Integer.parseInt(petBirth.substring(6, 8)));

        long MINUTE_MS = 60 * 1000;
        long HOUR_MS = MINUTE_MS * 60;
        long DAY_MS = HOUR_MS * 24;
        long MONTH_MS = HOUR_MS * 24L * 30L;

        Calendar today = Calendar.getInstance();

        return (int) ((today.getTimeInMillis() - newDate.getTimeInMillis()) / MONTH_MS);

    }

    public float getPetCalorie();

    public float getFeedGram();

}
