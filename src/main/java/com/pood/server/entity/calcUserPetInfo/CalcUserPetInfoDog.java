package com.pood.server.entity.calcUserPetInfo;

import com.pood.server.entity.meta.Product;
import com.pood.server.entity.user.UserPet;
import java.util.Arrays;

@Deprecated
public class CalcUserPetInfoDog implements CalcUserPetInfo{


    static float cupWidget = 100f;

    float petCalorie = 0f; // 반려동물의 하루치 칼로리
    int cupCalorie = 0; // 한컵 칼로리
    float feedGram = 0f; //  펫칼로리를 컵칼로리로 나눈 수량
    float foodGram = 0f; // 제품에 대한 동물이 먹어야 하는 그램
    float kg1 = 1000f;

    public void setFeedNutrition(int pcId, Product product, float cupSize, UserPet userPetInfo) {
        float weight = 0f;
        int month = getAgeMonth(userPetInfo.getPetBirth().toString().replace("-", ""));

        if (Arrays.asList(0,1,2,3,4).contains(month)) {
            weight = 210f;
        } else if (Arrays.asList(5,6,7).contains(month)) {
            weight = 170f;
        }else if (Arrays.asList(8,9).contains(month)) {
            weight = 140f;
        }else if (Arrays.asList(10,11).contains(month)) {
            weight = 122.5f;
        }else if(month >= 12 && month < 96){
            //1: 남아, 2: 여아, 3 : 남아 중성화, 4: 여아 중성화
            if(Arrays.asList(1,2).contains(userPetInfo.getPetGender())){
                weight = 112;
            }else{
                weight = 98f;
            }
        }else{
            weight = 98f;
        }

        petCalorie = (float) (weight * (Math.pow(userPetInfo.getPetWeight(), 0.75f)));
        cupCalorie = (int) ((product.getCalorie() / kg1) * cupSize);
        feedGram = petCalorie / cupCalorie;
        foodGram = petCalorie / (product.getCalorie() / kg1);

    }

    public float getPetCalorie(){
        return this.petCalorie;
    }

    public float getFeedGram() {
        return feedGram;
    }
}
