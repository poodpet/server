package com.pood.server.entity;

/**
 * 적합성, 펫 타입, 중성화 수술 여부에 따른 배점이 달라진다.
 */
@Deprecated
public class AnalysisPointsType {

    static double WORRY_DIAGNOSIS_1 = 100;                   //펫의 고민이 한개인 경우 배점
    static double WORRY_DIAGNOSIS_2 = 50;                    //펫의 고민이 두개인 경우 배점
    static double WORRY_DIAGNOSIS_3_1 = 50;                  //펫의 고민이 세개인 경우 첫번째 고민의 배점
    static double WORRY_DIAGNOSIS_3_2 = 30;                  //펫의 고민이 세개인 경우 두번째 고민의 배점
    static double WORRY_DIAGNOSIS_3_3 = 20;                  //펫의 고민이 세개인 경우 세번째 고민의 배점

    //FeedTarget/펫크기 유형 적합성 배점
    static double FEED_TARGET_SURGERY_TRUE = 20;             //feed_target 통과하고 중성화 수술 한 경우
    static double FEED_TARGET_SURGERY_FALSE = 25;            //feed_target 통과하고 중성화 수술 안 한 경우

    //펫 나이 적합성 배점
    static double PET_AGE_DOG_SURGERY_TRUE = 20;             //펫 나이 체크 통과하고, 강아지이며, 중성화 한 경우
    static double PET_AGE_DOG_SURGERY_FALSE = 25;            //펫 나이 체크 통과하고, 강아지이며, 중성화 안 한 경우
    static double PET_AGE_CAT_SURGERY_TRUE = 25;             //펫 나이 체크 통과하고, 고양이이며, 중성화 한 경우
    static double PET_AGE_CAT_SURGERY_FALSE = 34;            //펫 나이 체크 통과하고, 고양이이며, 중성화 안 한 경우

    //알갱이 크기 배점
    static double PET_UNIT_SIZE_DOG_SURGERY_TRUE = 20;       //강아지가 선호하는 알갱이 크기, 중성화 한 경우
    static double PET_UNIT_SIZE_DOG_SURGERY_FALSE = 25;      //강아지가 선호하는 알갱이 크기, 중성화 안 한 경우
    static double PET_UNIT_SIZE_CAT_SURGERY_TRUE = 25;       //고양이가 선호하는 알갱이 크기, 중성화 한 경우
    static double PET_UNIT_SIZE_CAT_SURGERY_FALSE = 33;      //고양이가 선호하는 알갱이 크기, 중성화 안 한 경우
    static double PET_UNIT_SIZE_WEB_DOG_SURGERY_TRUE = 18;   //강아지, 습식, 중성화 한 경우
    static double PET_UNIT_SIZE_WEB_DOG_SURGERY_FALSE = 18;  //강아지, 습식, 중성화 안 한 경우
    static double PET_UNIT_SIZE_WEB_CAT_SURGERY_TRUE = 22;   //고양이, 습식, 중성화 한 경우
    static double PET_UNIT_SIZE_WEB_CAT_SURGERY_FALSE = 27;  //고양이, 습식, 중성화 안 한 경우

    static double PET_ALLERGY_DOG_SURGERY_TRUE = 20;         //알러지에 도움이 되는 제품이며, 강아지이고, 중성화 수술 한 경우
    static double PET_ALLERGY_DOG_SURGERY_FALSE = 25;        //알러지에 도움이 되는 제품이며, 강아지이고, 중성화 수술 안 한 경우
    static double PET_ALLERGY_CAT_SURGERY_TRUE = 25;         //알러지에 도움이 되는 제품이며, 고양이이고, 중성화 수술 한 경우
    static double PET_ALLERGY_CAT_SURGERY_FALSE = 33;        //알러지에 도움이 되는 제품이며, 고양이이고, 중성화 수술 안 한 경우

    static double PET_SURGERY_DOG = 20;                      //중성화수술을 한 강아지인 경우
    static double PET_SURGERY_CAT = 25;                      //중성화수술을 한 고양이인 경우

    static double PET_MOISTURE_DOG_SURGERY_TRUE = 20;        //수분공급분석, 강아지이며 중성화 수술을 한 경우
    static double PET_MOISTURE_DOG_SURGERY_FALSE = 25;       //수분공급분석, 강아지이며 중성화 수술을 안 한 경우
    static double PET_MOISTURE_CAT_SURGERY_TRUE = 25;        //수분공급분석, 고양이이며 중성화 수술을 한 경우
    static double PET_MOISTURE_CAT_SURGERY_FALSE = 33;       //수분공급분석, 강아지이며 중성화 수술을 안 한 경우
}
