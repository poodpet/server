package com.pood.server.entity;

import com.pood.server.entity.meta.PetDoctorFeedDescription;
import com.pood.server.entity.meta.Product;
import com.pood.server.entity.user.UserPet;
import com.pood.server.entity.user.UserPetAiDiagnosis;
import java.util.List;

@Deprecated
public class WorryCompatibility {

    public double calc(final UserPet userPet, final Product product,
        final List<UserPetAiDiagnosis> userPetAiDiagnosisList) {
        double point = 0.0;

        //userPet.getAi_diagnosis()).size() 값이 0이면 펫 등록할 때 선택한 고민이 없음
        //고민이 없으므로 point 0점
        //유저가 펫을 등록할 때 1~3개의 고민을 등록할 수 있다.
        //고민 개수에 따라서 배점이 달라진다.
        //고민이 1개인 경우 1개의 적합성이 통과하면 100점
        //고민이 2개인 경우 각각 50점
        //고민이 3개인 경우 첫번째 고민이 통과하면 50점, 두번째 30, 세번째 20점
        if (userPetAiDiagnosisList.size() > 0) {
            switch (userPetAiDiagnosisList.size()) {
                case 1:
                case 2:
                    for (UserPetAiDiagnosis index : userPetAiDiagnosisList) {
                        String stringPosition = changePositionString(index.getAiDiagnosisIdx());
                        boolean temp = checkWorry(product, stringPosition,
                            product.getPetDoctorFeedDescription());
                        if (temp) {
                            if (userPetAiDiagnosisList.size() == 1) {
                                point += AnalysisPointsType.WORRY_DIAGNOSIS_1;
                            } else {
                                point += AnalysisPointsType.WORRY_DIAGNOSIS_2;
                            }
                        }
                    }
                    break;
                case 3:
                    for (int i = 0; i < userPetAiDiagnosisList.size(); i++) {
                        String stringPosition = changePositionString(
                            userPetAiDiagnosisList.get(i).getAiDiagnosisIdx());
                        boolean temp = checkWorry(product, stringPosition,
                            product.getPetDoctorFeedDescription());
                        switch (i) {
                            case 0:
                                if (temp) {
                                    point += AnalysisPointsType.WORRY_DIAGNOSIS_3_1;
                                }
                                break;
                            case 1:
                                if (temp) {
                                    point += AnalysisPointsType.WORRY_DIAGNOSIS_3_2;
                                }
                                break;
                            case 2:
                                if (temp) {
                                    point += AnalysisPointsType.WORRY_DIAGNOSIS_3_3;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        return point;
    }


    //서버에서 내려주는 고민 타입(int)를 string으로 변환
    //api에서 내려받는 펫 고민 정보가 String으로 내려와서 String값으로 변환하여 비교 분석중
    public String changePositionString(final int ai_diagnosis_index) {
        switch (ai_diagnosis_index) {
            case 0:
                return "알러지/아토피";
            case 1:
                return "눈";
            case 2:
                return "뼈/관절";
            case 3:
                return "활력";
            case 4:
                return "치아";
            case 5:
                return "배변/배뇨";
            case 6:
                return "다이어트";
            case 7:
            default:
                return "피부/피모";
        }
    }

    //해당 제품이 유저펫의 고민적합성에 해당되는건지 확인
    public boolean checkWorry(final Product product, final String stringPosition,
        final PetDoctorFeedDescription petDoctorFeedDescription) {
        return petDoctorFeedDescription.getPosition1().contains(stringPosition) ||
            petDoctorFeedDescription.getPosition2().contains(stringPosition) ||
            petDoctorFeedDescription.getPosition3().contains(stringPosition);
    }


}
