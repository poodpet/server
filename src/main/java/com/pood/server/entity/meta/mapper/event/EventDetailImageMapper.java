package com.pood.server.entity.meta.mapper.event;

import com.querydsl.core.annotations.QueryProjection;
import lombok.Value;

@Value
public class EventDetailImageMapper {

    String url;
    Integer type;
    Integer priority;

    @QueryProjection
    public EventDetailImageMapper(final String url, final Integer type, final Integer priority) {
        this.url = url;
        this.type = type;
        this.priority = priority;
    }
}
