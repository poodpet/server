package com.pood.server.entity.meta;

import com.pood.server.util.MarketingType;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "marketing_sort")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class MarketingSort {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx", nullable = false)
    private Long id;

    @Column(name = "pet_category_idx")
    private Integer petCategoryIdx;

    @Column(name = "marketing_info_idx")
    private Integer marketingInfoIdx; // 이벤트나 프로모션의 ID

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "visible")
    private Boolean visible;

    @Column(name = "type", length = 10)
    @Enumerated(value = EnumType.STRING)
    private MarketingType type;

    @Column(name = "update_time")
    private LocalDateTime updateTime;

    @Column(name = "record_birth")
    private LocalDateTime recordBirth;

}