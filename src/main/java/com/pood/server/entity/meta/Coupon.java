package com.pood.server.entity.meta;

import com.pood.server.exception.UsageTimeExpiredException;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "coupon")
public class Coupon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(name = "쿠폰 설명")
    @Column(name = "description")
    private String description;

    @Column(name = "name")
    @ApiModelProperty(name = "쿠폰 이름")
    private String name;

    @Column(name = "type")
    @ApiModelProperty(name = "쿠폰 타입: 0(정액), 1(정률), 2(배송비)")
    private Integer type;

    @Column(name = "pc_id")
    @ApiModelProperty(name = "쿠폰 유형이 펫 타입 쿠폰일 경우만 사용")
    private Integer pcId;

    @Column(name = "goods_type")
    private Integer goodsType;

    @Column(name = "apply_coupon_type")
    @ApiModelProperty(name = "0: 딜, 1: 딜 타입")
    private Integer applyCouponType;

    @ApiModelProperty(name = "사용 X, 아래의 idx를 사용")
    @Column(name = "publish_type")
    private Integer publishType;

    @Column(name = "publish_type_idx")
    @ApiModelProperty(name = "meta_db.coupon_publish_type idx")
    private Integer publishTypeIdx;

    @Column(name = "over_type")
    @ApiModelProperty(name = "0: 상품쿠폰, 1: 전체쿠폰")
    private Integer overType;

    @Column(name = "discount_rate")
    @ApiModelProperty(name = "정률일때 할인율")
    private Integer discountRate;

    @Column(name = "max_price")
    @ApiModelProperty(name = "할인 금액")
    private Integer maxPrice;

    @Column(name = "limit_price")
    @ApiModelProperty(name = "사용하기 위한 최소 구매 금액")
    private Integer limitPrice;

    @Column(name = "payment_type")
    @ApiModelProperty(name = "결제 타입, meta_db.payment_type_idx, 사용 X")
    private Integer paymentType;

    @Column(name = "tag")
    @ApiModelProperty(name = "특정 태그를 통해 사용가능한 쿠폰이 있는지 확인")
    private String tag;

    @Column(name = "issued_count")
    @ApiModelProperty(name = "0: 제한 없음, 2: 수량만큼 제한")
    private Integer issuedCount;

    @Column(name = "issued_type")
    @ApiModelProperty(name = "0: 중복 발행 X, 1: 중복 발급가능")
    private Integer issuedType;

    @Column(name = "available_day")
    @ApiModelProperty(name = "사용 가능 시간")
    private Integer availableDay;

    @Column(name = "available_time")
    private LocalDateTime availableTime;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    public Coupon isBeforeCurrentTime() {
        if (availableTime.isBefore(LocalDateTime.now())) {
            throw new UsageTimeExpiredException("사용기간이 지난 쿠폰입니다.");
        }

        return this;
    }

}
