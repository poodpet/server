package com.pood.server.entity.meta.mapper;

import com.querydsl.core.annotations.QueryProjection;
import lombok.Value;

@Value
public class AiRecommendDiagnosisMapper {

    Integer idx;
    String ardName;
    String ardGroup;
    Integer ardGroupCode;
    String updatetime;
    String recordbirth;

    @QueryProjection
    public AiRecommendDiagnosisMapper(final Integer idx, final String ardName,
        final String ardGroup, final Integer ardGroupCode, final String updatetime,
        final String recordbirth) {
        this.idx = idx;
        this.ardName = ardName;
        this.ardGroup = ardGroup;
        this.ardGroupCode = ardGroupCode;
        this.updatetime = updatetime;
        this.recordbirth = recordbirth;
    }

    public static AiRecommendDiagnosisMapper ofEmpty() {
        return new AiRecommendDiagnosisMapper(null, null, null, null, null, null);
    }
}
