package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "maintab_goods")
public class MaintabGoods  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "maintab_idx")
    private Integer maintabIdx;

    @Column(name = "goods_idx")
    private Integer goodsIdx;

    @Column(name = "goods_name")
    private String goodsName;

    @Column(name = "goods_price")
    private Integer goodsPrice;

    @Column(name = "discount_rate")
    private Integer discountRate;

    @Column(name = "image")
    private String image;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "display_type")
    private String displayType;

    @Column(name = "visible")
    private Integer visible;

    @Column(name = "average_rating")
    private Double averageRating;

    @Column(name = "review_cnt")
    private Integer reviewCnt;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
