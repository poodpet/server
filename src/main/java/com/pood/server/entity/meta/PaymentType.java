package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "payment_type")
public class PaymentType  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "visible")
    private Integer visible;

    @Column(name = "pg")
    private String pg;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "type")
    private Integer type;

    @Column(name = "type_name")
    private String typeName;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
