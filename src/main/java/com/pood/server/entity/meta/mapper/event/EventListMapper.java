package com.pood.server.entity.meta.mapper.event;

import com.pood.server.util.MarketingType;
import com.querydsl.core.annotations.QueryProjection;
import lombok.Value;

@Value
public class EventListMapper {

    Integer idx;
    String title;
    String intro;
    String startDate;
    String endDate;
    String imgUrl;
    MarketingType type;
    String typeName;
    Integer pcIdx;

    @QueryProjection
    public EventListMapper(final Integer idx, final String title, final String intro,
        final String startDate, final String endDate, final String imgUrl, final MarketingType type,
        final String typeName, final Integer pcIdx) {

        this.idx = idx;
        this.title = title;
        this.intro = intro;
        this.startDate = startDate;
        this.endDate = endDate;
        this.imgUrl = imgUrl;
        this.type = type;
        this.typeName = typeName;
        this.pcIdx = pcIdx;
    }
}
