package com.pood.server.entity.meta.dto.user_pet_life_cycle;

import java.util.List;

public class SmallAdultDog implements UserPetLifeCycle {

    private static final String SLEEP = "사람보다 훨씬 더 많은 수면이 필요하니 하루 중 12시간 이상 자는 데 시간을 보낼 거예요. 그러니 너무 많이 잔다고 해서 걱정하지 마세요.";
    private static final String TEETH = "치아 건강에는 칫솔질만한 것도 없어요. 칫솔질은 잇몸과 치아 사이의 연결 부분에 집중하여 닦아주는 것이 좋아요. 칫솔질에 거부감을 느낀다면 치킨 맛 치약을 사용하는 것도 좋은 방법이에요.";
    private static final String FECES = "소변은 투명한 노란색이 정상이며, 변은 약간 묻어나오는 정도가 좋아요.";
    private static final String DIET = "긴 기간 주식으로 섭취해도 영양이 부족하거나 과도하지 않도록 균형 잡힌 식단이 필요해요. 대형견보다 위가 작기 때문에 적은 양을 먹어도 많은 칼로리를 줄 수 있는 고칼로리 식단을 먹어야 합니다.";
    private static final String BATH = "너무 잦은 목욕은 피부에 자극이 될 수 있으니 2주에 한 번 이하가 적당해요.";
    private static final String VACCINE = "1년에 1번의 정기검진으로 혈액검사와 소변검사를 추천하며, 빈혈, 각종 감염병, 당뇨, 신장 기능과 간기능을 확인할 수 있어요. 적어도 3년에 한 번은 종합백신 예방접종이 필요해요. 한 달에 한 번 이상 몸무게를 측정하는 것이 좋으며, 1주일에 2%가 넘는 변동이 없는 것이 건강한 체중을 유지하기에 적합해요.";
     private static final List<String> SOLUTION = List.of("적어도 3년에 한 번 예방접종 실시",
        "목욕은 2주에 한 번 이하",
        "고칼로리 식단 챙기기");

    @Override
    public String getSleep() {
        return SLEEP;
    }

    @Override
    public String getTeeth() {
        return TEETH;
    }

    @Override
    public String getFeces() {
        return FECES;
    }

    @Override
    public String getDiet() {
        return DIET;
    }

    @Override
    public String getBath() {
        return BATH;
    }

    @Override
    public String getHairCareAndBath() {
        return null;
    }

    @Override
    public String getWeightAndRegularCheck() {
        return null;
    }

    @Override
    public String getVaccine() {
        return VACCINE;
    }

    @Override
    public String getNeutering() {
        return null;
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }
}
