package com.pood.server.entity.meta;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "brand")
public class Brand {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "brand_name")
    private String brandName;

    @Column(name = "brand_name_en")
    private String brandNameEn;

    @Column(name = "brand_origin")
    private String brandOrigin;

    @Column(name = "brand_description")
    private String brandDescription;

    @Column(name = "brand_intro")
    private String brandIntro;

    @Column(name = "brand_logo")
    private String brandLogo;

    @Column(name = "brand_tag")
    private String brandTag;

    @Column(name = "brand_menufac")
    private String brandMenufac;

    @Column(name = "brand_seller")
    private String brandSeller;

    @Column(name = "brand_phone")
    private String brandPhone;

    @Column(name = "brand_visible")
    private Integer brandVisible;

    @Column(name = "pet_idx")
    private Integer petIdx;

    @Column(name = "pood_grade")
    private Integer poodGrade;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;


}
