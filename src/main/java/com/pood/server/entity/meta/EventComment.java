package com.pood.server.entity.meta;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EntityListeners(AuditingEntityListener.class)
@Table(name = "event_comment")
public class EventComment  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Long idx;

    @ManyToOne
    @JoinColumn(name = "event_info_idx", insertable = true, updatable = false)
    private EventInfo eventInfo;

    @Column(name = "user_info_idx")
    private Integer userInfoIdx;

    @Column(name = "comment")
    private String comment;

    @CreatedDate
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    @Column(name = "ranking")
    private String ranking;

    public EventComment(EventInfo eventInfo, Integer userInfoIdx, String comment) {
        this.eventInfo = eventInfo;
        this.userInfoIdx = userInfoIdx;
        this.comment = comment;
    }

    public boolean isPrize() {
        return "O".equals(ranking);
    }
}
