package com.pood.server.entity.meta;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "banner")
public class Banner  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "pc_id")
    private Integer pcId;

    @Column(name = "page_landing_idx")
    private Integer pageLandingIdx;

    @Column(name = "importance")
    private Integer importance;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "type")
    private Integer type;

    @ApiModelProperty(value = "배너 시작 기간")
    @Column(name = "start_period")
    private LocalDateTime startPeriod;

    @ApiModelProperty(value = "배너 종료 기간")
    @Column(name = "end_period")
    private LocalDateTime endPeriod;

    @ApiModelProperty(value = "상태값 = 0 : 진행, 1 : 일시정지")
    @Column(name = "status", columnDefinition="tinyint(1) default 0")
    private boolean status;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    @Column(name = "title")
    private String title;

    @Transient
    BannerImage bannerImage;

    public void setBannerImage(BannerImage bannerImage) {
        this.bannerImage = bannerImage;
    }
}
