package com.pood.server.entity.meta.dto.user_pet_life_cycle;

import java.util.List;

public class SmallSeniorDog implements UserPetLifeCycle {

    private static final String SLEEP = "시니어 시기에 접어들면, 활동성이 떨어지고 자는 시간이 길어져요. 질병이 있으면 자는 시간이 더 길어질 수 있습니다.";
    private static final String TEETH = "치아 건강에는 칫솔질만한 것도 없어요. 칫솔질은 잇몸과 치아 사이의 연결 부분에 집중하여 닦아주는 것이 좋아요. 칫솔질에 거부감을 느낀다면 치킨 맛 치약을 사용하는 것도 좋은 방법이에요.";
    private static final String FECES = "소변은 투명한 노란색이 정상이며, 변은 약간 묻어나오는 정도가 좋아요.";
    private static final String DIET =
        "소화능력과 노폐물 배설 능력이 떨어지기 때문에 지나친 고단백, 고지방 사료는 피하는 것이 좋아요. 신장건강을 위해 인을 미리 제한하는 것이 좋아요. 보통 시니어 전용 식단에는 노화 방지를 위해 항산화물질을 많이 넣어요. 또한 관절염이 있는 경우가 많아서 특정 영양소(글루코사민, 콘드로이틴, MSM, 오메가3, 녹색입홍합 등) 섭취가 필요해요.";
    private static final String BATH = "너무 잦은 목욕은 피부에 자극이 될 수 있으니 2주에 한 번 이하가 적당해요.";
    private static final String VACCINE = "슬프게도 10살 이상의 반려동물 절반이 종양이 있을 정도로 흔한 질병이에요. 따라서 동물병원에서 1년에 2번의 정기검진으로 혈액검사와 소변검사를 추천해요. 빈혈, 각종 감염병, 당뇨, 신장 기능과 간기능을 확인할 수 있어요. 한 달에 한 번 이상 몸무게를 측정하는 것이 좋으며, 1주일에 2%가 넘는 변동이 없는 것이 건강한 체중을 유지하기에 적합해요.";
    private static final List<String> SOLUTION = List.of("1년에 1번 이상 건강검진 실시",
        "초고단백, 초고지방 식단 피하기",
        "인이 높은 식단 피하기");

    @Override
    public String getSleep() {
        return SLEEP;
    }

    @Override
    public String getTeeth() {
        return TEETH;
    }

    @Override
    public String getFeces() {
        return FECES;
    }

    @Override
    public String getDiet() {
        return DIET;
    }

    @Override
    public String getBath() {
        return BATH;
    }

    @Override
    public String getHairCareAndBath() {
        return null;
    }

    @Override
    public String getWeightAndRegularCheck() {
        return null;
    }

    @Override
    public String getVaccine() {
        return VACCINE;
    }

    @Override
    public String getNeutering() {
        return null;
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }
}
