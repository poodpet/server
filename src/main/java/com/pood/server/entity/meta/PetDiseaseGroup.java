package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "pet_disease_group")
public class PetDiseaseGroup  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "pet_idx")
    private Integer petIdx;

    @Column(name = "dgn_name")
    private String dgnName;

    @Column(name = "dgn_group")
    private String dgnGroup;

    @Column(name = "dgn_group_code")
    private Integer dgnGroupCode;

    @Column(name = "dgn_nutrition")
    private String dgnNutrition;

    @Column(name = "dgn_nutrition_core")
    private String dgnNutritionCore;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
