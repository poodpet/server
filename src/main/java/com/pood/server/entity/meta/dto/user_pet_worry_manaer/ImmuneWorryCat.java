package com.pood.server.entity.meta.dto.user_pet_worry_manaer;

import java.util.List;

public class ImmuneWorryCat implements UserPetWorryManager {

    private static final String DAILY_LIFE = "집에서만 시간을 보내는 고양이를 위해 깨어 있는 시간 동안은 하루 20분 이상 술래잡기, 낚시놀이를 하며 놀아주는 것이 스트레스 해소에 도움이 될 거예요.";
    private static final String DIET_1 = "면역력이 약한 ";
    private static final String DIET_2 = "에게는 고단백 사료를 추천해요. 충분한 단백질 섭취를 통하여 근육을 튼튼하게 해줄 거예요. 또한 항산화제가 풍부한 식단이 도움이 돼요.";
    private static final List<String> SOLUTION = List.of("고단백 식단 챙기기",
        "항산화제가 풍부한 식단 챙기기",
        "아연이 풍부한 식단 챙기기");

    @Override
    public String getDailyLife() {
        return DAILY_LIFE;
    }

    @Override
    public String getDiet(final String petName) {
        return new StringBuilder(DIET_1).append(petName).append(DIET_2).toString();
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }

}
