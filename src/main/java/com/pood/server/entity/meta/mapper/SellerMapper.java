package com.pood.server.entity.meta.mapper;

import com.querydsl.core.annotations.QueryProjection;
import lombok.Value;

@Value
public class SellerMapper {

    Long idx;
    String name;

    @QueryProjection
    public SellerMapper(final Long idx, final String name) {
        this.idx = idx;
        this.name = name;
    }

}
