package com.pood.server.entity.meta;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "notice_image")
public class NoticeImage  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "항목 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "meta_db.notice.idx")
    @Column(name = "notice_idx")
    private Integer noticeIdx;

    @ApiModelProperty(value = "이미지 주소")
    @Column(name = "url")
    private String url;

    @ApiModelProperty(value = "0:안 보임, 1:보임")
    @Column(name = "visible")
    private Integer visible;

    @Column(name = "type")
    private Integer type;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
