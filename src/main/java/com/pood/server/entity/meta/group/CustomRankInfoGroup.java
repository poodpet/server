package com.pood.server.entity.meta.group;

import com.pood.server.entity.meta.dto.home.CustomRankInfoDto;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Value;

@Value
public class CustomRankInfoGroup {

    List<CustomRankInfoDto> customRankInfoDtoList;

    public CustomRankInfoGroup sortedScoreAndSize(final int size) {
        return new CustomRankInfoGroup(
            this.customRankInfoDtoList.stream()
                .sorted(Comparator.comparing(CustomRankInfoDto::getScore).reversed())
                .limit(size)
                .collect(Collectors.toList())
        );
    }

    public List<CustomRankInfoDto> findContainIdxList(final List<Integer> idxList) {

        return this.customRankInfoDtoList.stream()
            .filter(dto -> idxList.contains(dto.getGoodsIdx()))
            .collect(Collectors.toList());
    }
}
