package com.pood.server.entity.meta;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "shortcut_icon")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class ShortcutIcon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Long id;

    @Column(name = "image_url", columnDefinition = "TEXT", nullable = false)
    private String imageUrl;

    @Column(name = "icon_name", nullable = false, length = 10)
    private String iconName;

    @Column(name = "redirect_url", length = 30)
    private String redirectUrl;

    @Column(name = "priority", nullable = false)
    private int priority;

    @Column(name = "record_birth", updatable = false)
    private LocalDateTime recordBirth;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pet_category_idx")
    private PetCategory petCategory;

}