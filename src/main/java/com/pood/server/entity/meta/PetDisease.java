package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "pet_disease")
public class PetDisease  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "pet_idx")
    private Integer petIdx;

    @Column(name = "pc_ds_name")
    private String pcDsName;

    @Column(name = "pc_ds_group")
    private String pcDsGroup;

    @Column(name = "pc_ds_group_code")
    private Integer pcDsGroupCode;

    @Column(name = "pc_ds_desc")
    private String pcDsDesc;

    @Column(name = "pc_ds_hospital")
    private Integer pcDsHospital;

    @Column(name = "pc_ds_priority")
    private Integer pcDsPriority;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
