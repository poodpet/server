package com.pood.server.entity.meta;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "notice")
public class Notice  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "항목 번호")
    @Column(name = "idx")
    private Integer idx;

    @ManyToOne
    @JoinColumn(name = "type")
    private NoticeType noticeType;

    @ApiModelProperty(value = "관리자 UUID")
    @Column(name = "admin_uuid")
    private String adminUuid;

    @ApiModelProperty(value = "공지 제목")
    @Column(name = "title")
    private String title;

    @ApiModelProperty(value = "공지 내용")
    @Column(name = "text")
    private String text;

    @ApiModelProperty(value = "0:안 보임, 1: 보임")
    @Column(name = "isVisible")
    private Boolean isVisible;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    @ApiModelProperty(value = "삭제 여부")
    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @Transient
    List<NoticeImage> noticeImages;
    public void setNoticeImageList(List<NoticeImage> noticeImages) {
        this.noticeImages = noticeImages;
    }

}
