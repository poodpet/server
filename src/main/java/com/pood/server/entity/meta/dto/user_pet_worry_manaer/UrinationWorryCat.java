package com.pood.server.entity.meta.dto.user_pet_worry_manaer;

import java.util.List;

public class UrinationWorryCat implements UserPetWorryManager {

    private static final String DAILY_LIFE = "고양이의 스트레스 해소를 위해 두부 모래보다는 벤토나이트 모래와 넓은 화장실을 추천해요. 다묘 가정의 경우, 고양이 수만큼은 화장실이 있어야 합니다. 화장실 청소를 하며, 덩어리의 크기와 개수를 주기적으로 확인하면 건강관리에 도움이 될 거예요. 덩어리 개수는 하루 기준 소변 덩어리 2-3개 정도가 정상이에요. 아이가 오줌을 화장실이 아닌 곳에서 누거나 발을 화장실에 대지 않는 등의 이상 행동을 보인다면 동물병원에서 감염은 없는 지, 결석은 없는 지 원인을 파악하는 것이 중요해요.";
    private static final String DIET_1 = "배뇨기 질병에 취약한 ";
    private static final String DIET_2 = "에게는 크랜베리추출물의 D-만노스라는 성분이 중요해요. 이 성분은 방광벽에 세균이 붙지 않도록 하여 결석이 생기는 것을 막아줘요. 너무 높지 않은 수준의 단백질, 칼슘, 인의 함량의 식단으로 바꿔보세요.";
    private static final List<String> SOLUTION = List.of("고단백 식단 배제하기",
        "칼슘, 인이 높은 식단 배제하기",
        "D-만노스(크랜베리 추출물) 함유된 식단 챙기기",
        "식이섬유가 풍부한 식단 챙기기",
        "유산균 함량이 높은 영양제 급여하기");


    @Override
    public String getDailyLife() {
        return DAILY_LIFE;
    }

    @Override
    public String getDiet(final String petName) {
        return new StringBuilder(DIET_1).append(petName).append(DIET_2).toString();
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }

}
