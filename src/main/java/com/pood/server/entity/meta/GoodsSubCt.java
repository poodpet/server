package com.pood.server.entity.meta;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "goods_sub_ct")
public class GoodsSubCt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Long idx;

    @ManyToOne(fetch = FetchType.EAGER)
    @ApiModelProperty(value = "상품 카테고리")
    @JoinColumn(name = "goods_ct_idx")
    private GoodsCt goodsCt;

    @ManyToOne(fetch = FetchType.EAGER)
    @ApiModelProperty(value = "펫 타입 정보")
    @JoinColumn(name = "pet_ct_idx")
    private PetCategory petCategory;

    @Column(name = "name")
    private String name;

    @Column(name = "url")
    private String url;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    public boolean eqNameAll(){
       return name.equals("전체");
    }

    public Integer getPcIdx(){
        return petCategory.getIdx();
    }

}
