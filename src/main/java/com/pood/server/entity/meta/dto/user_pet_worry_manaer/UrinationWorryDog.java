package com.pood.server.entity.meta.dto.user_pet_worry_manaer;

import java.util.List;

public class UrinationWorryDog implements UserPetWorryManager {

    private static final String DAILY_LIFE = "소변은 투명한 노란색이 정상이며, 변은 약간 묻어 나오는 정도가 좋아요. 하루 소변, 대변을 2번 정도 누는 것이 정상입니다. 오줌을 자주 찔끔찔끔 여러 곳에 본다면 동물병원에 방문하여 감염은 없는 지, 결석은 없는 지 원인을 파악해주세요.";
    private static final String DIET_1 = "배뇨기 질병에 취약한 ";
    private static final String DIET_2 = "에게는 크랜베리추출물의 D-만노스라는 성분이 중요해요. 이 성분은 방광벽에 세균이 붙지 않도록 하여 결석이 생기는 것을 막아줘요. 너무 높지 않은 수준의 단백질, 칼슘, 인의 함량의 식단으로 바꿔보세요.";
    private static final List<String> SOLUTION = List.of("고단백 식단 배제하기",
        "칼슘, 인이 높은 식단 배제하기",
        "D-만노스(크랜베리 추출물) 함유된 식단 챙기기",
        "식이섬유가 풍부한 식단 챙기기",
        "유산균 함량이 높은 영양제 급여하기");


    @Override
    public String getDailyLife() {
        return DAILY_LIFE;
    }

    @Override
    public String getDiet(final String petName) {
        return new StringBuilder(DIET_1).append(petName).append(DIET_2).toString();
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }

}
