package com.pood.server.entity.meta;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "promotion")
public class Promotion  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "항목 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "0:전체, 1:강아지, 2:고양이")
    @Column(name = "pc_id")
    private Integer pcId;

    @ApiModelProperty(value = "프로모션 이름")
    @Column(name = "name")
    private String name;

    @ApiModelProperty(value = "프로모션 하위 이름")
    @Column(name = "sub_name")
    private String subName;

    @ApiModelProperty(value = "0 :대기 , 1 : 진행중 , 2: 일시중지 , 3: 종료")
    @Column(name = "status")
    private Integer status;

    @Column(name = "type")
    private String type;

    @ApiModelProperty(value = "우선순위")
    @Column(name = "priority")
    private Integer priority;

    @ApiModelProperty(value = "프로모션 시작 기간")
    @Column(name = "start_period")
    private LocalDateTime startPeriod;

    @ApiModelProperty(value = "프로모션 관리자 전용 타이틀")
    @Column(name = "inner_title")
    private String innerTitle;

    @ApiModelProperty(value = "프로모션 상세 정보")
    @Column(name = "details", columnDefinition = "TEXT")
    private String details;

    @ApiModelProperty(value = "프로모션 종료 기간")
    @Column(name = "end_period")
    private LocalDateTime endPeriod;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
