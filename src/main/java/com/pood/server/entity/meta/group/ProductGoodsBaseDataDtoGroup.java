package com.pood.server.entity.meta.group;

import com.pood.server.dto.meta.goods.PromotionDto.PromotionGoodsProductInfo;
import com.pood.server.entity.meta.dto.ProductGoodsBaseDataDto;
import com.pood.server.entity.meta.mapper.solution.ProductGoodsBaseDataMapper;
import com.pood.server.entity.user.BodyShape;
import com.pood.server.facade.user.solution.response.BodyShapeRecommendGoodsResponse;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.Value;
import org.springframework.util.ObjectUtils;

@Value
public class ProductGoodsBaseDataDtoGroup {

    List<ProductGoodsBaseDataDto> dtoList;

    public static ProductGoodsBaseDataDtoGroup ofMapper(final List<ProductGoodsBaseDataMapper> mapperList) {
        return new ProductGoodsBaseDataDtoGroup(
            mapperList.stream()
                .map(ProductGoodsBaseDataMapper::toDataDto)
                .collect(Collectors.toList()));
    }

    public ProductGoodsBaseDataDtoGroup filterAllergyAndSize(final List<String> allergyNameList,
        final int size) {

        return new ProductGoodsBaseDataDtoGroup(dtoList.stream()
            .filter(dto -> dto.isSupplies() || !dto.isIncludeAllergy(allergyNameList))
            .limit(size)
            .collect(Collectors.toList()));
    }

    public ProductGoodsBaseDataDtoGroup sortedByBodyShape(final BodyShape bodyShape) {

        if (BodyShape.SKINNY.equals(bodyShape)) {

            return new ProductGoodsBaseDataDtoGroup(
                dtoList.stream()
                    .sorted(Comparator.comparing(ProductGoodsBaseDataDto::getCalorie).reversed())
                    .collect(Collectors.toList()));
        }
        return this;
    }

    public List<Integer> getGoodsIdxList() {
        return dtoList.stream().map(ProductGoodsBaseDataDto::getGoodsIdx)
            .collect(Collectors.toList());
    }

    public ProductGoodsBaseDataDtoGroup updatePromotionPrice(
        final List<PromotionGoodsProductInfo> promotionInfoList) {

        return new ProductGoodsBaseDataDtoGroup(
            dtoList.stream().map(mapper -> {
                    Optional<PromotionGoodsProductInfo> promotionGoods = getPromotionGoodsInfo(
                        promotionInfoList, mapper.getGoodsIdx());

                    if (promotionGoods.isPresent()) {
                        return mapper.toUpdatePromotionPrice(
                            promotionGoods.get().getPrPrice(),
                            promotionGoods.get().getPrDiscountPrice(),
                            promotionGoods.get().getPrDiscountRate()
                        );
                    }
                    return mapper;
                }
            ).collect(Collectors.toList())
        );
    }

    private Optional<PromotionGoodsProductInfo> getPromotionGoodsInfo(
        final List<PromotionGoodsProductInfo> promotionInfoList,
        final Integer goodsIdx) {

        return promotionInfoList.stream().filter(
                promotionGoodsProductInfo -> promotionGoodsProductInfo.getGoodsIdx().equals(goodsIdx))
            .findFirst();
    }

    public List<BodyShapeRecommendGoodsResponse> toResponseForBodyManagement(
        final List<Integer> wishGoodsList) {

        return this.dtoList.stream()
            .map(dataDto -> BodyShapeRecommendGoodsResponse.toResponse(dataDto,
                isWishGoods(wishGoodsList, dataDto.getGoodsIdx())))
            .collect(Collectors.toList());
    }

    private boolean isWishGoods(final List<Integer> wishList, final Integer goodsIdx) {
        return wishList.contains(goodsIdx);
    }

    public boolean isLessThan(final int size) {
        return size > getSize();
    }

    public int getSize() {
        if (ObjectUtils.isEmpty(dtoList)) {
            return 0;
        }
        return dtoList.size();
    }

    public ProductGoodsBaseDataDtoGroup add(final List<ProductGoodsBaseDataDto> addGoodsMapper) {
        return new ProductGoodsBaseDataDtoGroup(
            Stream.concat(dtoList.stream(), addGoodsMapper.stream())
                .collect(Collectors.toList())
        );
    }
}
