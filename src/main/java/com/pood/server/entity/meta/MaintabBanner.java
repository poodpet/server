package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "maintab_banner")
public class MaintabBanner  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "maintab_idx")
    private Integer maintabIdx;

    @Column(name = "banner_type")
    private Integer bannerType;

    @Column(name = "page_landing_idx")
    private Integer pageLandingIdx;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "title")
    private String title;

    @Column(name = "url")
    private String url;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
