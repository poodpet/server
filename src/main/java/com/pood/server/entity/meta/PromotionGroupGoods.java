package com.pood.server.entity.meta;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "promotion_group_goods")
public class PromotionGroupGoods  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "항목 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "프로모션 항목 번호 : meta_db.promotion.idx")
    @Column(name = "pr_idx")
    private Integer prIdx;

    @ApiModelProperty(value = "프로모션 그룹 항목 번호 : meta_db.promotion_group.idx")
    @Column(name = "pr_group_idx")
    private Integer prGroupIdx;

    @ApiModelProperty(value = "굿즈 항목 번호 : meta_db.goods.idx")
    @Column(name = "goods_idx")
    private Integer goodsIdx;

    @ApiModelProperty(value = "굿즈 항목 이름 : meta_db.goods.goods_name")
    @Column(name = "goods_name")
    private String goodsName;

    @ApiModelProperty(value = "굿즈 가격 이름 : meta_db.goods.goods_price")
    @Column(name = "goods_price")
    private Integer goodsPrice;

    @ApiModelProperty(value = "굿즈 할인율 : meta_db.goods.discount_rate")
    @Column(name = "discount_rate")
    private Integer discountRate;

    @Column(name = "display_type")
    private String displayType;

    @Column(name = "image")
    private String image;

    @ApiModelProperty(value = "우선순위")
    @Column(name = "priority")
    private Integer priority;

    @ApiModelProperty(value = "0: 안보임, 1:보임")
    @Column(name = "visible")
    private Integer visible;

    @Column(name = "average_rating")
    private Double averageRating;


    @Column(name = "review_cnt")
    private Integer reviewCnt;

    @ApiModelProperty(value = "할인 적용 가격")
    @Column(name = "pr_price")
    private Integer prPrice;

    @ApiModelProperty(value = "할인가")
    @Column(name = "pr_discount_price")
    private Integer prDiscountPrice;

    @ApiModelProperty(value = "할인 적용 %")
    @Column(name = "pr_discount_rate")
    private Integer prDiscountRate;


    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
