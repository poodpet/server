package com.pood.server.entity.meta;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@NoArgsConstructor
@Table(name = "event_donation")
@EntityListeners(AuditingEntityListener.class)
public class EventDonation {

    @ApiModelProperty(value = "레코드 번호")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long idx;

    @ApiModelProperty(value = "meta_db.event_info.idx")
    @Column(name = "event_info_idx")
    private Integer eventInfoIdx;

    @ApiModelProperty(value = "user_db.user_info.user_uuid")
    @Column(name = "user_uuid")
    private String userUuid;

    @ApiModelProperty(value = "펫 카테고리")
    @Column(name = "pc_idx")
    private Integer pcIdx;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "record_birth")
    @CreatedDate()
    private LocalDateTime recordBirth;

    @Builder(access = AccessLevel.PRIVATE)
    public EventDonation(Integer eventInfoIdx, String userUuid, Integer pcIdx) {
        this.eventInfoIdx = eventInfoIdx;
        this.userUuid = userUuid;
        this.pcIdx = pcIdx;
    }

    public static EventDonation of(final int eventIdx, final String userUuid, final int pcIdx) {
        return EventDonation.builder()
            .eventInfoIdx(eventIdx)
            .userUuid(userUuid)
            .pcIdx(pcIdx)
            .build();
    }
}
