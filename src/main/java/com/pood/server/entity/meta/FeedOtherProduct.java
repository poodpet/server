package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "feed_other_product")
public class FeedOtherProduct  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "feed_name")
    private String feedName;

    @Column(name = "brand")
    private String brand;

    @Column(name = "category")
    private Integer category;

    @Column(name = "main_ingredient")
    private String mainIngredient;

    @Column(name = "animal_ingredient")
    private String animalIngredient;

    @Column(name = "vegetable_ingredient")
    private String vegetableIngredient;

    @Column(name = "gluten_free")
    private Integer glutenFree;

    @Column(name = "gmo_free")
    private Integer gmoFree;

    @Column(name = "price")
    private Integer price;

    @Column(name = "grade")
    private Integer grade;

    @Column(name = "additives")
    private Integer additives;

    @Column(name = "manufacturer")
    private String manufacturer;

    @Column(name = "country")
    private String country;

    @Column(name = "target")
    private String target;

    @Column(name = "targetHint")
    private String targetHint;

    @Column(name = "unitSize")
    private String unitSize;

    @Column(name = "calorie")
    private Integer calorie;

    @Column(name = "calorie_calc")
    private Double calorieCalc;

    @Column(name = "protein")
    private Double protein;

    @Column(name = "fat")
    private Double fat;

    @Column(name = "ash")
    private Double ash;

    @Column(name = "fiber")
    private Double fiber;

    @Column(name = "moisture")
    private Double moisture;

    @Column(name = "carbohydrate")
    private Double carbohydrate;

    @Column(name = "calcium")
    private Double calcium;

    @Column(name = "phosphorus")
    private Double phosphorus;

    @Column(name = "omega3")
    private Double omega3;

    @Column(name = "omega6")
    private Double omega6;

    @Column(name = "ingredients")
    private String ingredients;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
