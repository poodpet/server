package com.pood.server.entity.meta.mapper.event;

import com.querydsl.core.annotations.QueryProjection;
import lombok.Value;

@Value
public class EventDonationThumbnailMapper {

    Integer idx;
    String imgUrl;

    @QueryProjection
    public EventDonationThumbnailMapper(final Integer idx, final String imgUrl) {
        this.idx = idx;
        this.imgUrl = imgUrl;
    }
}
