package com.pood.server.entity.meta.dto.user_pet_worry_manaer;

import java.util.List;

public class DiarrheaWorryCat implements UserPetWorryManager {

    private static final String DAILY_LIFE = "대변은 1일 기준 1-2회 정도가 정상이에요. 고양이가 다른 증상 없이 묽은 변을 한 번 정도 본 것은 괜찮아요. 액체처럼 묽은 변을 이틀 이상 지속할 경우 병원에 가는 것이 좋아요. 고양이는 장염, 식이알러지 뿐만 아니라, 스트레스를 받았을 때에도 설사를 할 수 있어요. 또한 식단을 갑작스럽게 바꾸었을 때에도 설사를 할 수 있답니다.";
    private static final String DIET_1 = "장이 약한 ";
    private static final String DIET_2 = "에게는 식이섬유(차전자피, 이눌린)가 풍부한 식단이 좋아요. 장의 유산균을 늘려주고, 변의 모양을 올바르게 잡아줄 거예요. 유산균 함량이 높은 사료와 영양제도 변의 모양을 잡는데 도움을 줘요.";
    private static final List<String> SOLUTION = List.of("충분한 수분 공급",
        "갑작스러운 식단 변경 금지",
        "식이섬유가 풍부한 식단 챙기기",
        "유산균이 함유된 식단 챙기기",
        "식이 알러지 가능성 챙기기");

    @Override
    public String getDailyLife() {
        return DAILY_LIFE;
    }

    @Override
    public String getDiet(final String petName) {
        return new StringBuilder().append(DIET_1).append(petName).append(DIET_2).toString();
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }

}
