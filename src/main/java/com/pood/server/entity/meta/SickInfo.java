package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "sick_info")
public class SickInfo  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "sick_name")
    private String sickName;

    @Column(name = "sick_index")
    private Integer sickIndex;

    @Column(name = "sick_group_code")
    private Integer sickGroupCode;

    @Column(name = "pc_id")
    private Integer pcId;

    @Column(name = "nutrition_info")
    private String nutritionInfo;

    @Column(name = "protein_range")
    private String proteinRange;

    @Column(name = "fat_range")
    private String fatRange;

    @Column(name = "carbo_range")
    private String carboRange;

    @Column(name = "fiber_range")
    private String fiberRange;

    @Column(name = "description")
    private String description;

    @Column(name = "updatetime")
    private String updatetime;

    @Column(name = "recordbirth")
    private String recordbirth;

}
