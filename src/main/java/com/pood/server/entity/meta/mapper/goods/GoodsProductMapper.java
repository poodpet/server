package com.pood.server.entity.meta.mapper.goods;

import com.querydsl.core.annotations.QueryProjection;
import lombok.Value;

@Value
public class GoodsProductMapper {

    Integer goodsIdx;
    Integer productIdx;

    public static GoodsProductMapper of(final Integer goodsIdx, final Integer productIdx) {
        return new GoodsProductMapper(goodsIdx, productIdx);
    }

    @QueryProjection
    public GoodsProductMapper(final Integer goodsIdx, final Integer productIdx) {
        this.goodsIdx = goodsIdx;
        this.productIdx = productIdx;
    }
}
