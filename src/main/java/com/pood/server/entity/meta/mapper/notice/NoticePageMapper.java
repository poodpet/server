package com.pood.server.entity.meta.mapper.notice;

import com.querydsl.core.annotations.QueryProjection;
import java.time.LocalDate;
import java.time.LocalDateTime;
import lombok.Value;

@Value
public class NoticePageMapper {

    Integer idx;

    String typeName;

    String title;

    LocalDate date;

    @QueryProjection
    public NoticePageMapper(Integer idx, String typeName, String title, LocalDateTime date) {
        this.idx = idx;
        this.typeName = typeName;
        this.title = title;
        this.date = date.toLocalDate();
    }

}
