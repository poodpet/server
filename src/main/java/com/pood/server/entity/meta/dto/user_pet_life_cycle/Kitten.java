package com.pood.server.entity.meta.dto.user_pet_life_cycle;

import java.util.List;

public class Kitten implements UserPetLifeCycle {

    private static final String SLEEP = "하루 중 20시간 이상 자는 데 시간을 보낼 거예요. 그러니 너무 많이 잔다고 해서 걱정하지 마세요. 성장에 필요한 수면이랍니다.";
    private static final String TEETH = "치아 건강에는 칫솔질만한 것도 없어요. 칫솔질은 잇몸과 치아 사이의 연결 부분에 집중하여 닦아주는 것이 좋아요. 특히 어릴 때 손가락을 입 주위에 갖다 대는 것부터 천천히 연습해보세요. 칫솔질에 거부감을 느낀다면 치킨 맛 치약을 사용하는 것도 좋은 방법이에요.";
    private static final String FECES = "아기 고양이는 배변훈련을 따로 하지 않더라도, 본능적으로 모래를 사용할 수 있답니다. 화장실 사용이 익숙하지 않다면, 식사 후 30분 정도 후에 모래 위에 올려주면 도움이 될 거예요. 화장실을 잘 사용하지 않는다면, 모래의 문제일 수 있으니 교체를 해보세요. 고양이들은 대체로 두부모래보다는 입자가 작은 벤토나이트 모래를 선호해요.";
    private static final String DIET = "키튼은 짧은 기간 빠르게 성장합니다. 뼈를 튼튼하게 하기 위해 칼슘, 인 등 많은 미네랄을 섭취해야 해요. 근육과 올바른 성장을 위해 많은 단백질과 필수지방산이 필요하답니다. 성묘에 비하여 위가 작아서 적은 양으로도 많은 칼로리를 줄 수 있는 고칼로리 식단을 급여해주세요.";
    private static final String BATH = "침에 세정작용을 하는 물질이 포함되어 있어 하루에도 여러 번 스스로 깨끗이 하는 그루밍 활동을 해요. 그렇기 때문에 따로 목욕을 시키지 않아도 괜찮아요. 털이 엉키고 많이 지저분해졌을 때에는 목욕을 할 수 있지만 대다수의 고양이가 물을 싫어해서 취향을 존중해줘야 한답니다.";
    private static final String VACCINE = "3번에 나누어 종합백신 접종이 필요해요.";
    private static final String NEUTERING = "중성화 수술을 고려할 시기에요. 첫 발정이 오기 전에 수술을 하는 것이 건강에 도움되며 무혈 생리를 하므로, 겉으로 티가 나지 않아요. 대신 특유의 소리를 통해 발정이 온 것을 짐작할 수 있으니 귀를 기울여 주세요.";
    private static final List<String> SOLUTION = List.of("첫 발정이 오기 전 중성화 수술 해주기",
        "고칼로리 식단 챙기기");

    @Override
    public String getSleep() {
        return SLEEP;
    }

    @Override
    public String getTeeth() {
        return TEETH;
    }

    @Override
    public String getFeces() {
        return FECES;
    }

    @Override
    public String getDiet() {
        return DIET;
    }

    @Override
    public String getBath() {
        return BATH;
    }

    @Override
    public String getHairCareAndBath() {
        return null;
    }

    @Override
    public String getWeightAndRegularCheck() {
        return null;
    }

    @Override
    public String getVaccine() {
        return VACCINE;
    }

    @Override
    public String getNeutering() {
        return NEUTERING;
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }
}
