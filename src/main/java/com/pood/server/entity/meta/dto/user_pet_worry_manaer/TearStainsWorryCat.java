package com.pood.server.entity.meta.dto.user_pet_worry_manaer;

import java.util.List;

public class TearStainsWorryCat implements UserPetWorryManager {

    private static final String DAILY_LIFE = "눈물 자국은 눈썹이 선천적으로 눈을 찌르면서 생기는 문제일 수 있어요. 지속적으로 나오는 눈물을 제 때 닦아주지 않으면, 특히 흰 털을 가진 강아지에게 붉은 눈물 자국이 남게 된답니다. 자극이 덜한 미용티슈나 솜을 이용하여 자주 닦아주세요. 최근에 사료를 교체한 후에 눈물 자국이 유독 심해졌다면 음식 알러지가 있을 가능성이 높기 때문에 단백질을 확인해주세요.";
    private static final String DIET_1 = "알러지가 있는";
    private static final String DIET_2 = "에게는 어떠한 단백질을 주고 있는지 꼼꼼히 살피는 것이 무엇보다 중요해요. 간식도 알러지가 있는 동물성 단백질은 제한이 필요하답니다. 사료는 먹어본 적 없는 단백질이 도움이 되며, 한가지 단백질로 구성된 단일 단백질 식단으로 바꿔 보세요.";
    private static final String DIET_3 = "또한 피부형성을 도와주는 오메가3가 풍부하고 철, 아연, 구리 등 필수 미네랄과 비타민이 풍부한 식단이 도움이 될 거예요.";

    private static final List<String> SOLUTION = List.of("알러지원이 없는 간식으로 대체하기",
        "먹어본 적이 없는 단백질 식단으로 바꾸기",
        "단일 단백질 식단으로 바꾸기",
        "철, 아연, 구리가 풍부한 식단 챙기기",
        "오메가3가 풍부한 식단 챙기기");


    @Override
    public String getDailyLife() {
        return DAILY_LIFE;
    }

    @Override
    public String getDiet(final String petName) {
        return new StringBuilder(DIET_1).append(petName).append(DIET_2).append("\n").append(DIET_3).toString();
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }

}
