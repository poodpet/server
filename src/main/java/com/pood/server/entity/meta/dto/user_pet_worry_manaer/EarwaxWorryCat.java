package com.pood.server.entity.meta.dto.user_pet_worry_manaer;

import java.util.List;

public class EarwaxWorryCat implements UserPetWorryManager {

    private static final String DAILY_LIFE = "최근에 사료를 교체한 후에 귀지가 유독 많아졌다면 음식 알러지가 있을 가능성이 높아요. 귀청소시 귀에서 냄새가 심하게 나거나 어두운 색의 귀지가 많이 보인다면 바로 닦아주세요. 고양이는 주기를 신경 쓰기 보다는 귀지의 양을 주기적으로 체크해주세요. 너무 자주 청소해주는 것이 더 자극이 될 수 있으며, 귀세정제를 묻힌 솜으로 겉만 가볍게 닦아주세요.";
    private static final String DIET_1 = "알러지가 있는";
    private static final String DIET_2 = "에게는 어떠한 단백질을 주고 있는지 꼼꼼히 살피는 것이 중요해요. 간식도 알러지가 있는 동물성 단백질은 제한이 필요하답니다. 사료는 먹어본 적 없는 단백질이 도움이 될 거예요.";
    private static final String DIET_3 = "는 피부가 약하기 때문에 피부 형성을 도와주는 오메가3가 풍부하고 철, 아연, 구리 등 필수 미네랄과 비타민이 풍부한 식단이 도움이 될 거예요.";
    private static final List<String> SOLUTION = List.of("단일 단백질 식단으로 바꾸기",
        "알러지원이 없는 간식으로 대체하기",
        "먹어본 적이 없는 단백질 식단으로 바꾸기",
        "철, 아연, 구리가 풍부한 식단 챙기기",
        "오메가3가 풍부한 식단 챙기기");

    @Override
    public String getDailyLife() {
        return DAILY_LIFE;
    }

    @Override
    public String getDiet(final String petName) {
        return new StringBuilder(DIET_1).append(petName).append(DIET_2).append("\n").append(DIET_3)
            .toString();
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }

}
