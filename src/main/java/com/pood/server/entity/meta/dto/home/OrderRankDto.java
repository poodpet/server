package com.pood.server.entity.meta.dto.home;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class OrderRankDto {

    private Integer goodsIdx;
    private Long rank;

    public static OrderRankDto of(final CustomRankInfoDto customRankInfoDto) {
        return new OrderRankDto(customRankInfoDto.getGoodsIdx(), 0L);
    }

}
