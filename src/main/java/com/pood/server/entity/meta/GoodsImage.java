package com.pood.server.entity.meta;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "goods_image")
public class GoodsImage  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "굿즈 항목 번호 : order_db.goods.idx")
    @Column(name = "goods_idx")
    private Integer goodsIdx;

    @ApiModelProperty(value = "이미지 URL")
    @Column(name = "url")
    private String url;

    @ApiModelProperty(value = "0:기본, 1:등록자만, 2:안보임, 3:정책위반")
    @Column(name = "visible")
    private Integer visible;

    @ApiModelProperty(value = "0:딜메인, 1:상세상단, 2:상세하단")
    @Column(name = "type")
    private Integer type;

    @ApiModelProperty(value = "우선 순위")
    @Column(name = "priority")
    private Integer priority;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
