package com.pood.server.entity.meta.mapper.home;

import com.querydsl.core.annotations.QueryProjection;
import lombok.Value;

@Value
public class CustomRankInfoMapper {

    Integer goodsIdx;
    Long reviewCnt;
    Long wishCnt;
    Long totalPurchaseUserCnt;
    Long rePurchaseUserCnt;

    @QueryProjection
    public CustomRankInfoMapper(final Integer goodsIdx, final Long reviewCnt, final Long wishCnt,
        final Long totalPurchaseUserCnt, final Long rePurchaseUserCnt) {

        this.goodsIdx = goodsIdx;
        this.reviewCnt = reviewCnt;
        this.wishCnt = wishCnt;
        this.totalPurchaseUserCnt = totalPurchaseUserCnt;
        this.rePurchaseUserCnt = rePurchaseUserCnt;
    }
}
