package com.pood.server.entity.meta.dto.home;

import com.pood.server.entity.meta.mapper.home.CustomRankInfoMapper;
import java.util.Objects;
import lombok.Value;

@Value
public class CustomRankInfoDto {

    int goodsIdx;
    long reviewCnt;
    long wishCnt;
    long totalPurchaseUserCnt;
    long rePurchaseUserCnt;
    double score;

    public static CustomRankInfoDto of(final CustomRankInfoMapper mapper) {

        final long reviewCnt = convertOrElseNullZero(mapper.getReviewCnt());
        final long wishCnt = convertOrElseNullZero(mapper.getWishCnt());
        final long totalPurchaseUserCnt = convertOrElseNullZero(mapper.getTotalPurchaseUserCnt());
        final long rePurchaseUserCnt = convertOrElseNullZero(mapper.getRePurchaseUserCnt());

        return new CustomRankInfoDto(
            mapper.getGoodsIdx(),
            reviewCnt,
            wishCnt,
            totalPurchaseUserCnt,
            rePurchaseUserCnt,
            getCustomRankScore(reviewCnt, wishCnt, totalPurchaseUserCnt, rePurchaseUserCnt)
        );
    }

    private static long convertOrElseNullZero(final Long number) {
        return Objects.isNull(number) ? 0L : number;
    }

    private static double getCustomRankScore(final Long reviewCnt, final Long wishCnt,
        final Long totalPurchaseUserCnt, final Long rePurchaseUserCnt) {

        final double repurchaseRate = totalPurchaseUserCnt > 0 ?
            rePurchaseUserCnt / (double) totalPurchaseUserCnt : 0;

        return (reviewCnt + wishCnt) * (1f + repurchaseRate);
    }
}
