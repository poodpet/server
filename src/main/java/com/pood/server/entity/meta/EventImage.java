package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "event_image")
public class EventImage  {

    private static final int MAIN_IMAGE = 0;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "event_idx")
    private Integer eventIdx;

    @Column(name = "url")
    private String url;
//0 : 메인  1 : 상세
    @Column(name = "type")
    private Integer type;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "height")
    private Integer height;

    @Column(name = "width")
    private Integer width;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    public boolean isMainImg() {
        return this.type.equals(MAIN_IMAGE);
    }

    public boolean isDetailImg() {
        return !this.type.equals(MAIN_IMAGE);
    }
}
