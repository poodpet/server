package com.pood.server.entity.meta;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name = "custom_rank_info_view")
@Getter
public class CustomRankInfo {

    @Id
    @Column(name = "goods_idx", nullable = false)
    private Integer goodsIdx;

    @Column(name = "review_cnt")
    private Long reviewCnt;

    @Column(name = "wish_cnt")
    private Long wishCnt;

    @Column(name = "total_purchase_user_cnt")
    private Long totalPurchaseUserCnt;

    @Column(name = "re_purchase_user_cnt")
    private Long rePurchaseUserCnt;

}
