package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "aafco_nrc")
public class AafcoNrc  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "pet_type")
    private Integer petType;

    @Column(name = "pet_idx")
    private Integer petIdx;

    @Column(name = "mrs")
    private String mrs;

    @Column(name = "pr_protein")
    private Double prProtein;

    @Column(name = "am_arginine")
    private Double amArginine;

    @Column(name = "am_histidine")
    private Double amHistidine;

    @Column(name = "am_isoleucine")
    private Double amIsoleucine;

    @Column(name = "am_methionine")
    private Double amMethionine;

    @Column(name = "am_cystine")
    private Double amCystine;

    @Column(name = "am_leucine")
    private Double amLeucine;

    @Column(name = "am_lysine")
    private Double amLysine;

    @Column(name = "am_phenylanlanine")
    private Double amPhenylanlanine;

    @Column(name = "am_tyrosine")
    private Double amTyrosine;

    @Column(name = "am_threonine")
    private Double amThreonine;

    @Column(name = "am_tryptophan")
    private Double amTryptophan;

    @Column(name = "am_valine")
    private Double amValine;

    @Column(name = "am_glutamic_acid")
    private Double amGlutamicAcid;

    @Column(name = "ot_taurine")
    private Double otTaurine;

    @Column(name = "pr_fat")
    private Double prFat;

    @Column(name = "fa_linoleicacid")
    private Double faLinoleicacid;

    @Column(name = "fa_a_linoleicacid")
    private Double faALinoleicacid;

    @Column(name = "fa_arachidonic_acid")
    private Double faArachidonicAcid;

    @Column(name = "fa_epa_dha")
    private Double faEpaDha;

    @Column(name = "mi_calcium")
    private Double miCalcium;

    @Column(name = "mi_phosphours")
    private Double miPhosphours;

    @Column(name = "mi_magnessium")
    private Double miMagnessium;

    @Column(name = "mi_sodium")
    private Double miSodium;

    @Column(name = "mi_potassium")
    private Double miPotassium;

    @Column(name = "mi_chloride")
    private Double miChloride;

    @Column(name = "mi_iron")
    private Double miIron;

    @Column(name = "mi_copper")
    private Double miCopper;

    @Column(name = "mi_zinc")
    private Double miZinc;

    @Column(name = "mi_manganese")
    private Double miManganese;

    @Column(name = "mi_selenium")
    private Double miSelenium;

    @Column(name = "mi_iodine")
    private Double miIodine;

    @Column(name = "pr_moisture")
    private Double prMoisture;

    @Column(name = "vi_vitamin_a")
    private Double viVitaminA;

    @Column(name = "vi_vitamin_d")
    private Double viVitaminD;

    @Column(name = "vi_vitamin_e")
    private Double viVitaminE;

    @Column(name = "vi_vitamin_k3")
    private Double viVitaminK3;

    @Column(name = "vi_vitamin_b1")
    private Double viVitaminB1;

    @Column(name = "vi_vitamin_b2")
    private Double viVitaminB2;

    @Column(name = "vi_vitamin_b3")
    private Double viVitaminB3;

    @Column(name = "vi_vitamin_b5")
    private Double viVitaminB5;

    @Column(name = "vi_vitamin_b6")
    private Double viVitaminB6;

    @Column(name = "vi_vitamin_b7")
    private Double viVitaminB7;

    @Column(name = "vi_vitamin_b9")
    private Double viVitaminB9;

    @Column(name = "vi_vitamin_b12")
    private Double viVitaminB12;

    @Column(name = "vi_Choline")
    private Double viCholine;

    @Column(name = "am_met_cys")
    private Double amMetCys;

    @Column(name = "am_phe_tyr")
    private Double amPheTyr;

    @Column(name = "mi_ca_ph")
    private Double miCaPh;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    @Builder
    public AafcoNrc(Integer idx, Integer petType, Integer petIdx, String mrs,
        Double prProtein, Double amArginine, Double amHistidine, Double amIsoleucine,
        Double amMethionine, Double amCystine, Double amLeucine, Double amLysine,
        Double amPhenylanlanine, Double amTyrosine, Double amThreonine, Double amTryptophan,
        Double amValine, Double amGlutamicAcid, Double otTaurine, Double prFat,
        Double faLinoleicacid, Double faALinoleicacid, Double faArachidonicAcid,
        Double faEpaDha, Double miCalcium, Double miPhosphours, Double miMagnessium,
        Double miSodium, Double miPotassium, Double miChloride, Double miIron,
        Double miCopper, Double miZinc, Double miManganese, Double miSelenium,
        Double miIodine, Double prMoisture, Double viVitaminA, Double viVitaminD,
        Double viVitaminE, Double viVitaminK3, Double viVitaminB1, Double viVitaminB2,
        Double viVitaminB3, Double viVitaminB5, Double viVitaminB6, Double viVitaminB7,
        Double viVitaminB9, Double viVitaminB12, Double viCholine, Double amMetCys,
        Double amPheTyr, Double miCaPh) {
        this.idx = idx;
        this.petType = petType;
        this.petIdx = petIdx;
        this.mrs = mrs;
        this.prProtein = prProtein;
        this.amArginine = amArginine;
        this.amHistidine = amHistidine;
        this.amIsoleucine = amIsoleucine;
        this.amMethionine = amMethionine;
        this.amCystine = amCystine;
        this.amLeucine = amLeucine;
        this.amLysine = amLysine;
        this.amPhenylanlanine = amPhenylanlanine;
        this.amTyrosine = amTyrosine;
        this.amThreonine = amThreonine;
        this.amTryptophan = amTryptophan;
        this.amValine = amValine;
        this.amGlutamicAcid = amGlutamicAcid;
        this.otTaurine = otTaurine;
        this.prFat = prFat;
        this.faLinoleicacid = faLinoleicacid;
        this.faALinoleicacid = faALinoleicacid;
        this.faArachidonicAcid = faArachidonicAcid;
        this.faEpaDha = faEpaDha;
        this.miCalcium = miCalcium;
        this.miPhosphours = miPhosphours;
        this.miMagnessium = miMagnessium;
        this.miSodium = miSodium;
        this.miPotassium = miPotassium;
        this.miChloride = miChloride;
        this.miIron = miIron;
        this.miCopper = miCopper;
        this.miZinc = miZinc;
        this.miManganese = miManganese;
        this.miSelenium = miSelenium;
        this.miIodine = miIodine;
        this.prMoisture = prMoisture;
        this.viVitaminA = viVitaminA;
        this.viVitaminD = viVitaminD;
        this.viVitaminE = viVitaminE;
        this.viVitaminK3 = viVitaminK3;
        this.viVitaminB1 = viVitaminB1;
        this.viVitaminB2 = viVitaminB2;
        this.viVitaminB3 = viVitaminB3;
        this.viVitaminB5 = viVitaminB5;
        this.viVitaminB6 = viVitaminB6;
        this.viVitaminB7 = viVitaminB7;
        this.viVitaminB9 = viVitaminB9;
        this.viVitaminB12 = viVitaminB12;
        this.viCholine = viCholine;
        this.amMetCys = amMetCys;
        this.amPheTyr = amPheTyr;
        this.miCaPh = miCaPh;
    }
}
