package com.pood.server.entity.meta.dto.user_pet_worry_manaer;

import java.util.List;

public class ImmuneWorryDog implements UserPetWorryManager {

    private static final String DAILY_LIFE = "스트레스를 줄여주는 것이 중요해요. 밥을 주더라도 노즈워크나 푸드퍼즐을 이용하여 즐겁게 밥을 먹을 수 있도록 해주세요.";
    private static final String DIET_1 = "면역력이 약한 ";
    private static final String DIET_2 = "에게는 고단백 사료를 추천해요. 충분한 단백질 섭취를 통하여 근육을 튼튼하게 해줄 거예요. 또한 항산화제가 풍부한 식단이 도움이 돼요.";
    private static final List<String> SOLUTION = List.of("고단백 식단 챙기기",
        "항산화제가 풍부한 식단 챙기기",
        "아연이 풍부한 식단 챙기기");

    @Override
    public String getDailyLife() {
        return DAILY_LIFE;
    }

    @Override
    public String getDiet(final String petName) {
        return new StringBuilder(DIET_1).append(petName).append(DIET_2).toString();
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }

}
