package com.pood.server.entity.meta;

import java.time.LocalDateTime;
import java.util.Arrays;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "snack")
public class Snack {

    //type 0 :개, 1 : g, 2 :ml, 3 :회
    private static final int FEEDING_DETAIL_TYPE_NUM = 0;
    private static final int FEEDING_DETAIL_TYPE_GRAM = 1;
    private static final int FEEDING_DETAIL_TYPE_ML = 2;
    private static final int FEEDING_DETAIL_TYPE_COUNT = 3;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "product_idx")
    private Integer productIdx;

    @Column(name = "type")
    private Integer type;

    @Column(name = "per_cal")
    private Double perCal;

    @Column(name = "per_grams")
    private Double perGrams;

    @Column(name = "min")
    private Double min;

    @Column(name = "max")
    private Double max;

    @Column(name = "description")
    private String description;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;


    @Builder
    public Snack(Integer idx, Integer productIdx, Integer type, Double perCal,
        Double perGrams, Double min, Double max, String description) {
        this.idx = idx;
        this.productIdx = productIdx;
        this.type = type;
        this.perCal = perCal;
        this.perGrams = perGrams;
        this.min = min;
        this.max = max;
        this.description = description;
    }

    public boolean isNumOrCountType() {
        return type.equals(FEEDING_DETAIL_TYPE_NUM) || type.equals(FEEDING_DETAIL_TYPE_COUNT);
    }

    public boolean isGramType() {
        return type == FEEDING_DETAIL_TYPE_GRAM;
    }

    public boolean isPetOrNum() {
        return type.equals(SnackType.PER.value) || type.equals(SnackType.NUM.value);
    }

    public boolean isMLType() {
        return type == FEEDING_DETAIL_TYPE_ML;
    }

    @Getter
    @RequiredArgsConstructor
    public enum SnackType {
        PER(0, "개", "를"), //개
        GRAM(1, "g", "을"),//그람
        ML(2, "ml", "를"),//ml
        NUM(3, "회", "를"); //회

        private final Integer value;
        private final String type;
        private final String subType;

        public static SnackType messageType(final Integer type) {
            return Arrays.stream(values())
                .filter(snackType -> snackType.value.equals(type))
                .findFirst()
                .orElse(PER);
        }

    }
}
