package com.pood.server.entity.meta.dto.user_pet_worry_manaer;

import java.util.List;

public class ConstipationWorryDog implements UserPetWorryManager {

    private static final String DAILY_LIFE = "하루 소변, 대변을 2번 정도 누는 것이 정상이에요. 일명 토끼똥이라 하여 모양이 둥글고 끊어져 있는 형태의 변을 누면 변비일 수 있어요. 아침 저녁으로 산책을 해주어, 운동을 해주면 장운동을 촉진하여 도움이 될 거예요.";
    private static final String DIET_1 = "변비가 있는 ";
    private static final String DIET_2 = "에게는 식이섬유(차전자피, 이눌린)가 풍부한 식단이 좋아요. 장의 유산균을 늘려주고, 변의 모양을 올바르게 잡아줄 거예요. 유산균 함량이 높은 사료와 영양제도 변의 모양을 잡는데 도움이 돼요.";
    private static final List<String> SOLUTION = List.of("충분한 수분 공급",
        "갑작스러운 식단 변경 금지",
        "식이섬유가 풍부한 식단 챙기기",
        "유산균이 함유된 식단 챙기기");

    @Override
    public String getDailyLife() {
        return DAILY_LIFE;
    }

    @Override
    public String getDiet(final String petName) {
        return new StringBuilder(DIET_1).append(petName).append(DIET_2).toString();
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }

}
