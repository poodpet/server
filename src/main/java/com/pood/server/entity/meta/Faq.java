package com.pood.server.entity.meta;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "faq")
public class Faq {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Long idx;

    @ManyToOne(fetch = FetchType.LAZY)
    @ApiModelProperty(value = "faq 카테고리 : meta_db.faq_category_idx")
    @JoinColumn(name = "faq_category_idx")
    private FaqCategory faqCategory;

    @Column(name = "title")
    private String title;
    
    @Column(name = "contents")
    private String contents;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "update_time")
    private LocalDateTime updateTime;

    @Column(name = "record_birth")
    private LocalDateTime recordBirth;

}
