package com.pood.server.entity.meta;

import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FeedUnitSize {

    SMALL(0, 0.9, "작은 알갱이"),
    MEDIUM(1, 1.5, "중간 알갱이"),
    LARGE(1.6, 5, "큰 알갱이");

    private final double minSize;
    private final double maxSize;
    private final String name;

    public static String getUnitSizeName(final Double unitSize) {
        if (Objects.isNull(unitSize)) {
            throw new IllegalArgumentException("사료 알갱이 사이즈가 없는 제품입니다.");
        }

        if (isLarge(unitSize)) {
            return LARGE.getName();
        }

        if (isMedium(unitSize)) {
            return MEDIUM.getName();
        }

        return SMALL.getName();
    }

    private static boolean isLarge(final double size) {
        return MEDIUM.getMaxSize() < size;
    }

    private static boolean isMedium(final double size) {
        return SMALL.getMaxSize() < size && LARGE.getMinSize() > size;
    }

}
