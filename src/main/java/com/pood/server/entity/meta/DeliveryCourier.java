package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "delivery_courier")
public class DeliveryCourier  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "courier_code")
    private String courierCode;

    @Column(name = "start_zipcode")
    private String startZipcode;

    @Column(name = "end_zipcode")
    private String endZipcode;

    @Column(name = "area_type")
    private Integer areaType;

    @Column(name = "area")
    private String area;

    @Column(name = "delivery_fee")
    private Integer deliveryFee;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
