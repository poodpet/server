package com.pood.server.entity.meta.group;

import com.pood.server.entity.meta.dto.goods.GoodsProductGroupByGoodsDto;
import com.pood.server.entity.meta.mapper.goods.GoodsProductMapper;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Value;

@Value
public class GoodsProductMapperGroup {

    List<GoodsProductMapper> mappers;

    public static GoodsProductMapperGroup of(final List<GoodsProductMapper> mappers) {
        return new GoodsProductMapperGroup(mappers);
    }

    public GoodsProductGroupByGoodsDto toGoodsProductGroupByDto() {
        return new GoodsProductGroupByGoodsDto(mappers.get(0).getGoodsIdx(),
            mappers.stream().map(GoodsProductMapper::getProductIdx).collect(
                Collectors.toList())
        );
    }
}
