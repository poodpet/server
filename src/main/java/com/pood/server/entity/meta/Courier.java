package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "courier")
public class Courier  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "isDefault")
    private Integer isDefault;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
