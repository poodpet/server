package com.pood.server.entity.meta.dto.user_pet_life_cycle;

import java.util.List;

public class AdultCat implements UserPetLifeCycle {

    private static final String SLEEP = "16시간 이상 자는 데 시간을 보낸다고 놀라지 말아요. 사람의 두 배 많은 수면 시간이 필요하답니다. 깨어 있는 시간 동안은 하루 20분 이상 술래잡기, 낚시놀이를 하며 놀아주는 것이 좋아요.";
    private static final String TEETH = "치아 건강에는 칫솔질만한 것도 없어요. 칫솔질은 잇몸과 치아 사이의 연결 부분에 집중하여 닦아주는 것이 좋아요. 칫솔질에 거부감을 느낀다면 치킨 맛 치약을 사용하는 것도 좋은 방법이에요.";
    private static final String FECES = "화장실 청소를 하며, 덩어리의 크기와 개수를 주기적으로 확인해보세요. 덩어리 개수는 일정한 것이 좋으며 적어져도 많아져도 건강의 이상 신호일 수 있거든요.";
    private static final String DIET =
        "긴 기간 주식으로 섭취해도 영양이 부족하거나 과도하지 않도록 균형 잡힌 식단이 필요한건 당연한 얘기겠죠? 타우린, 단백질 및 필수지방산이 반드시 필요해요. 특히나 강아지 사료를 우리 {꼬냥이}에게 장기간 급여하게된다면 심장질환의 위험이 있을 수 있어요.";
    private static final List<String> SOLUTION = List.of("적어도 3년에 한 번 예방접종 실시", "타우린이 풍부한 식단 챙기기");
    private static final String VACCINE = "1년에 1번의 정기검진으로 혈액검사와 소변검사를 추천하며, 빈혈, 각종 감염병, 당뇨, 신장 기능과 간기능을 확인할 수 있어요. 적어도 3년에 한 번은 종합백신 예방접종이 필요해요. 한 달에 한 번 이상 몸무게를 측정하는 것이 좋으며, 1주일에 2%가 넘는 변동이 없는 것이 건강한 체중을 유지하기에 적합해요.";
    private static final String BATCH = "너무 잦은 목욕은 피부에 자극이 될 수 있으니 2주에 한 번 이하가 적당해요.";

    @Override
    public String getSleep() {
        return SLEEP;
    }

    @Override
    public String getTeeth() {
        return TEETH;
    }

    @Override
    public String getFeces() {
        return FECES;
    }

    @Override
    public String getDiet() {
        return DIET;
    }

    @Override
    public String getBath() {
        return BATCH;
    }

    @Override
    public String getHairCareAndBath() {
        return null;
    }

    @Override
    public String getWeightAndRegularCheck() {
        return null;
    }

    @Override
    public String getVaccine() {
        return VACCINE;
    }

    @Override
    public String getNeutering() {
        return null;
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }
}
