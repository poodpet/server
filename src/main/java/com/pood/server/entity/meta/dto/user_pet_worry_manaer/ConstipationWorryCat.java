package com.pood.server.entity.meta.dto.user_pet_worry_manaer;

import java.util.List;

public class ConstipationWorryCat implements UserPetWorryManager {

    private static final String DAILY_LIFE = "대변은 하루 1-2회 정도가 정상이에요. 화장실에서 힘만 주고 있는 경우에 대변이 아닌 소변을 못 누는 것일 수 있어요. 이 경우 하부요로계질환, 결석도 의심해볼 수 있습니다. 또한 변 상태를 보았을 때 장모종의 경우 헤어볼이 지나치게 많지는 않은 지 체크해보고, 헤어볼이 너무 많다면 빗질을 좀 더 신경을 써주세요. 헤어볼은 고양이 장내에서 뭉친 것으로 소화가 잘 되지 않기 때문에, 변비를 유발하고 심하면 장을 틀어막을 수도 있어요.";
    private static final String DIET_1 = "변비가 있는 ";
    private static final String DIET_2 = "에게는 식이섬유(차전자피, 이눌린)가 풍부한 식단이 좋아요. 장의 유산균을 늘려주고, 변의 모양을 올바르게 잡아줄 거예요. 유산균 함량이 높은 사료와 영양제도 변의 모양을 잡는데 도움이 돼요.";
    private static final List<String> SOLUTION = List.of("충분한 수분 공급",
        "갑작스러운 식단 변경 금지",
        "식이섬유가 풍부한 식단 챙기기",
        "유산균이 함유된 식단 챙기기");

    @Override
    public String getDailyLife() {
        return DAILY_LIFE;
    }

    @Override
    public String getDiet(final String petName) {
        return new StringBuilder(DIET_1).append(petName).append(DIET_2).toString();
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }

}
