package com.pood.server.entity.meta;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "goods_sub_ct_group")
public class GoodsSubCtGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Long idx;


    @ManyToOne(fetch = FetchType.EAGER)
    @ApiModelProperty(value = "브랜드 항목 번호 : meta_db.goods.idx")
    @JoinColumn(name = "goods_idx")
    private Goods goods;


    @ManyToOne(fetch = FetchType.EAGER)
    @ApiModelProperty(value = "브랜드 항목 번호 : meta_db.goods_sub_ct.idx")
    @JoinColumn(name = "goods_sub_ct_idx")
    private GoodsSubCt goodsSubCt;

}
