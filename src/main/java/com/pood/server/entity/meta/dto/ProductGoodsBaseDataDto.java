package com.pood.server.entity.meta.dto;

import com.pood.server.service.pet_solution.ProductType;
import java.util.List;
import lombok.Value;

@Value
public class ProductGoodsBaseDataDto {

    Integer goodsIdx;
    Integer pcIdx;
    String displayType;
    String goodsName;
    Integer goodsOriginPrice;
    Integer goodsPrice;
    Integer discountRate;
    Integer discountPrice;
    Integer saleStatus;
    Integer mainProduct;
    Double averageRating;
    String mainImage;
    Boolean isRecommend;
    String brandName;
    boolean isNewest;
    Long ctIdx;
    Long ctSubIdx;
    String mainProperty;
    String animalProtein;
    Integer calorie;


    public boolean isSupplies() {
        return ProductType.SUPPLIES.getIdx() == ctIdx;
    }

    public boolean isIncludeAllergy(final List<String> allergyNameList) {
        return allergyNameList.stream()
            .anyMatch(allergyName -> allergyName.contains(mainProperty)
                || allergyName.contains(
                animalProtein));
    }

    public ProductGoodsBaseDataDto toUpdatePromotionPrice(final Integer promotionGoodsPrice,
        final Integer promotionDiscountPrice, final Integer promotionDiscountRate) {
        return new ProductGoodsBaseDataDto(
            goodsIdx, pcIdx, displayType, goodsName, goodsOriginPrice, promotionGoodsPrice,
            promotionDiscountRate, promotionDiscountPrice, saleStatus, mainProduct, averageRating,
            mainImage, isRecommend, brandName, isNewest, ctIdx, ctSubIdx, mainProperty,
            animalProtein, calorie);
    }
}
