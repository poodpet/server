package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "nuti")
public class Nuti  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "ni_name")
    private String niName;

    @Column(name = "ni_name_en")
    private String niNameEn;

    @Column(name = "ni_tag")
    private String niTag;

    @Column(name = "ni_group")
    private String niGroup;

    @Column(name = "ni_grade")
    private Integer niGrade;

    @Column(name = "ni_ds_dog")
    private String niDsDog;

    @Column(name = "ni_ds_group_dog")
    private String niDsGroupDog;

    @Column(name = "ni_ds_group_dog_code")
    private String niDsGroupDogCode;

    @Column(name = "ni_ds_cat")
    private String niDsCat;

    @Column(name = "ni_ds_group_cat")
    private String niDsGroupCat;

    @Column(name = "ni_ds_group_cat_code")
    private String niDsGroupCatCode;

    @Column(name = "ni_desc")
    private String niDesc;

    @Column(name = "ni_effect")
    private String niEffect;

    @Column(name = "ni_cautions")
    private String niCautions;

    @Column(name = "ni_nt_group")
    private String niNtGroup;

    @Column(name = "ni_priority")
    private Integer niPriority;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
