package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "event_goods_list")
public class EventGoodsList  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "event_idx")
    private Integer eventIdx;

    @Column(name = "goods_idx")
    private Integer goodsIdx;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "goods_type")
    private Integer goodsType;

    @Column(name = "sub_title")
    private String subTitle;

    @Column(name = "event_image_idx")
    private Integer eventImageIdx;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
