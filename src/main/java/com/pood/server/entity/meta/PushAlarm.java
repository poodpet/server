package com.pood.server.entity.meta;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Getter
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "push_alarm")
public class PushAlarm {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Long idx;

    @ApiModelProperty(value = "전송 타입(0: text, 1 : images)")
    @Column(name = "send_type")
    private Integer sendType;

    @ManyToOne(fetch = FetchType.EAGER)
    @ApiModelProperty(value = "펫 타입 정보")
    @JoinColumn(name = "pc_idx")
    private PetCategory petCategory;

    @ApiModelProperty(value = "OS 타입(0: global, 1: 1:android, 2: ios)")
    @Column(name = "os_type")
    private Integer osType;

    @ApiModelProperty(value = "타입(0: promotion, 1 : event)")
    @Column(name = "type")
    private Integer type;

    @ApiModelProperty(value = "어드민용 제목")
    @Column(name = "inner_title")
    private String innerTitle;

    @ApiModelProperty(value = "광고 명")
    @Column(name = "title")
    private String title;

    @ApiModelProperty(value = "내용")
    @Column(name = "contents")
    private String contents;

    @ApiModelProperty(value = "페이지 연결 idx")
    @Column(name = "page_landing_idx")
    private Integer pageLandingIdx;

    @ApiModelProperty(value = "푸시를 통한 이동 카운트")
    @Column(name = "view_count")
    private Integer viewCount;

    @ApiModelProperty(value = "전송 여부")
    @Column(name = "is_sent")
    private Boolean isSent;

    @ApiModelProperty(value = "취소 여부")
    @Column(name = "is_cancel")
    private Boolean isCancel;

    @ApiModelProperty(value = "전송 예약 시각")
    @Column(name = "schedule_send_time")
    private LocalDateTime scheduleSendTime;

    @ApiModelProperty(value = "실제 전 시각")
    @Column(name = "real_send_time")
    private LocalDateTime realSendTime;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @LastModifiedDate
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @CreatedDate
    @Column(name = "recordbirth" , updatable = false)
    private LocalDateTime recordbirth;

    public PushAlarm(PetCategory petCategory, Integer type, String title, String contents, Integer pageLandingIdx, Boolean isSent, Boolean isCancel, LocalDateTime scheduleSendTime, LocalDateTime realSendTime,String innerTitle, Integer sendType, Integer osType) {
        this.petCategory = petCategory;
        this.type = type;
        this.title = title;
        this.contents = contents;
        this.pageLandingIdx = pageLandingIdx;
        this.isSent = isSent;
        this.isCancel = isCancel;
        this.scheduleSendTime = scheduleSendTime;
        this.realSendTime = realSendTime;
        this.innerTitle = innerTitle;
        this.sendType = sendType;
        this.osType = osType;
    }

    public void modify(PetCategory petCategory, Integer type, String title, String contents, Integer pageLandingIdx, LocalDateTime scheduleSendTime,String innerTitle,Integer sendType, Integer osType) {
        this.petCategory = petCategory;
        this.type = type;
        this.title = title;
        this.contents = contents;
        this.pageLandingIdx = pageLandingIdx;
        this.scheduleSendTime = scheduleSendTime;
        this.innerTitle = innerTitle;
        this.sendType = sendType;
        this.osType = osType;

    }

    public void setCancle() {
        this.isCancel = Boolean.TRUE;
    }

    public void pluseViewCount(){
        this.viewCount = this.viewCount + 1;
    }
}
