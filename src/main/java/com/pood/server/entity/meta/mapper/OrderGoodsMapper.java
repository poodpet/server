package com.pood.server.entity.meta.mapper;

import com.querydsl.core.annotations.QueryProjection;
import lombok.Value;

@Value
public class OrderGoodsMapper {

    Integer idx;
    Integer pcIdx;
    Integer goodsTypeIdx;
    String displayType;
    String goodsName;
    Integer goodsOriginPrice;
    Integer goodsPrice;
    Integer discountRate;
    Integer discountPrice;
    Integer saleStatus;
    Integer visible;
    Integer mainProduct;
    Double averageRating;
    Long reviewCnt;
    String mainImage;
    Boolean isRecommend;
    SellerMapper seller;

    @QueryProjection
    public OrderGoodsMapper(final Integer idx, final Integer pcIdx, final Integer goodsTypeIdx,
        final String displayType, final String goodsName, final Integer goodsOriginPrice,
        final Integer goodsPrice, final Integer discountRate, final Integer discountPrice,
        final Integer saleStatus, final Integer visible, final Integer mainProduct,
        final Double averageRating, final Long reviewCnt, final String mainImage,
        final Boolean isRecommend, final SellerMapper seller) {
        this.idx = idx;
        this.pcIdx = pcIdx;
        this.goodsTypeIdx = goodsTypeIdx;
        this.displayType = displayType;
        this.goodsName = goodsName;
        this.goodsOriginPrice = goodsOriginPrice;
        this.goodsPrice = goodsPrice;
        this.discountRate = discountRate;
        this.discountPrice = discountPrice;
        this.saleStatus = saleStatus;
        this.visible = visible;
        this.mainProduct = mainProduct;
        this.averageRating = averageRating;
        this.reviewCnt = reviewCnt;
        this.mainImage = mainImage;
        this.isRecommend = isRecommend;
        this.seller = seller;
    }
}
