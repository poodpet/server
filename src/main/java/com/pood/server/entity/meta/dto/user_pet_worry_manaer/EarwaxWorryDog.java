package com.pood.server.entity.meta.dto.user_pet_worry_manaer;

import java.util.List;

public class EarwaxWorryDog implements UserPetWorryManager {

    private static final String DAILY_LIFE = "최근에 사료를 교체한 후에 귀지가 유독 많아졌다면 음식 알러지가 있을 가능성이 높아요. 귀청소시 귀에서 냄새가 심하게 나거나 어두운 색의 귀지가 많이 보인다면 바로 닦아주세요. 목욕하고 난 뒤, 2주에 한 번 정도 청소해주는 것이 좋아요. 귀가 접힌 품종은 각별히 신경을 써주어야 한답니다. 귀 세정제를 귓구멍에 2-3방울 떨어뜨리고, 귀를 마사지 한 후에 솜으로 겉만 가볍게 닦아주시기만 하세요.";
    private static final String DIET_1 = "알러지가 있는 ";
    private static final String DIET_2 = "에게는 어떠한 단백질을 주고 있는지 꼼꼼히 살피는 것이 중요해요. 간식도 동물성단백질은 제한이 필요하며, 야채와 과일을 주는 것이 좋아요. 사료는 먹어본 적 없는 단백질이 도움이 되어, 곤충단백 사료를 급여해보세요.";
    private static final String DIET_3 = "는 피부가 약하기 때문에 피부 형성을 도와주는 오메가3가 풍부하고 철, 아연, 구리 등 필수 미네랄과 비타민이 풍부한 식단이 도움이 될 거예요.";
    private static final List<String> SOLUTION = List.of("단일 단백질(곤충) 식단으로 바꾸기",
        "알러지원이 없는 간식으로 대체하기",
        "먹어본 적이 없는 단백질 식단으로 바꾸기",
        "철, 아연, 구리가 풍부한 식단 챙기기",
        "오메가3가 풍부한 식단 챙기기");



    @Override
    public String getDailyLife() {
        return DAILY_LIFE;
    }

    @Override
    public String getDiet(final String petName) {
        return new StringBuilder(DIET_1).append(petName).append(DIET_2).append("\n").append(petName)
            .append(DIET_3).toString();
    }
    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }

}
