package com.pood.server.entity.meta;

import com.pood.server.util.converter.PackageTypeConverter;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "상품 UUID")
    @Column(name = "uuid")
    private String uuid;

    @ManyToOne(fetch = FetchType.LAZY)
    @ApiModelProperty(value = "브랜드 항목 번호 : meta_db.brand.idx")
    @JoinColumn(name = "brand_idx")
    private Brand brand;


    @ManyToOne(fetch = FetchType.EAGER)
    @ApiModelProperty(value = "브랜드 항목 번호 : meta_db.seller.idx")
    @JoinColumn(name = "seller_idx")
    private Seller seller;


    @ApiModelProperty(value = "상품 이름")
    @Column(name = "product_name")
    private String productName;

    @ApiModelProperty(value = "상품 코드")
    @Column(name = "product_code")
    private String productCode;

    @Column(name = "barcode")
    private String barcode;

    @ApiModelProperty(value = "반려동물 항목 번호 : meta_db.pet.idx")
    @Column(name = "pc_idx")
    private Integer pcIdx;

    @ApiModelProperty(value = "상품 카테고리 항목 번호 : meta_db.product_ct.idx")
    @Column(name = "ct_idx")
    private Integer ctIdx;

    @ApiModelProperty(value = "상품 하위 카테고리 항목 번호 : meta_db.product_sub_ct.idx")
    @Column(name = "ct_sub_idx")
    private Integer ctSubIdx;

    @ApiModelProperty(value = "상품 하위 카테고리 이름")
    @Column(name = "ct_sub_name")
    private String ctSubName;

    @Column(name = "show_index")
    private Integer showIndex;

    @Column(name = "tag")
    private String tag;

    @Column(name = "all_nutrients")
    private Integer allNutrients;

    @Column(name = "main_property")
    @ApiModelProperty(value = "주 단백질")
    private String mainProperty;

    @Column(name = "tasty")
    private String tasty;

    @Column(name = "product_video")
    private String productVideo;

    @Column(name = "calorie")
    private Integer calorie;

    @Column(name = "feed_target")
    private Integer feedTarget;

    @Column(name = "feed_type")
    private String feedType;

    @Column(name = "gluten_free")
    private Integer glutenFree;

    @Column(name = "animal_protein")
    private String animalProtein;

    @Column(name = "vegetable_protein")
    private String vegetableProtein;

    @Column(name = "aafco")
    private String aafco;

    @Column(name = "single_protein")
    private Integer singleProtein;

    @Column(name = "ingredients")
    private String ingredients;

    @Column(name = "unit_size")
    private Double unitSize;

    @Column(name = "ingredients_search")
    private String ingredientsSearch;

    @Column(name = "notice_desc")
    private String noticeDesc;

    @Column(name = "notice_title")
    private String noticeTitle;

    @Column(name = "package_type")
    @Convert(converter = PackageTypeConverter.class)
    private PackageType packageType;

    @Column(name = "cup_weight")
    private Integer cupWeight;

    @Column(name = "is_recommend")
    private Integer isRecommend;

    @Column(name = "grain_free")
    private Integer grainFree;

    @Column(name = "ava_quantity")
    private Integer avaQuantity;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "weight")
    private Integer weight;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    @ApiModelProperty(value = "수의사 승인여부")
    @Column(columnDefinition = "tinyint(1) default 0")
    private boolean approve;

    @Builder
    private Product(Integer idx, String uuid, Brand brand, Seller seller, String productName,
        String productCode, String barcode, Integer pcIdx, Integer ctIdx, Integer ctSubIdx,
        String ctSubName, Integer showIndex, String tag, Integer allNutrients,
        String mainProperty, String tasty, String productVideo, Integer calorie,
        Integer feedTarget, String feedType, Integer glutenFree, String animalProtein,
        String vegetableProtein, String aafco, Integer singleProtein, String ingredients,
        Double unitSize, String ingredientsSearch, String noticeDesc, String noticeTitle,
        PackageType packageType, Integer cupWeight, Integer isRecommend, Integer grainFree,
        Integer avaQuantity, Integer quantity, Integer weight, boolean approve,
        List<ProductImage> productImageList, Feed feed,
        PetDoctorFeedDescription petDoctorFeedDescription) {
        this.idx = idx;
        this.uuid = uuid;
        this.brand = brand;
        this.seller = seller;
        this.productName = productName;
        this.productCode = productCode;
        this.barcode = barcode;
        this.pcIdx = pcIdx;
        this.ctIdx = ctIdx;
        this.ctSubIdx = ctSubIdx;
        this.ctSubName = ctSubName;
        this.showIndex = showIndex;
        this.tag = tag;
        this.allNutrients = allNutrients;
        this.mainProperty = mainProperty;
        this.tasty = tasty;
        this.productVideo = productVideo;
        this.calorie = calorie;
        this.feedTarget = feedTarget;
        this.feedType = feedType;
        this.glutenFree = glutenFree;
        this.animalProtein = animalProtein;
        this.vegetableProtein = vegetableProtein;
        this.aafco = aafco;
        this.singleProtein = singleProtein;
        this.ingredients = ingredients;
        this.unitSize = unitSize;
        this.ingredientsSearch = ingredientsSearch;
        this.noticeDesc = noticeDesc;
        this.noticeTitle = noticeTitle;
        this.packageType = packageType;
        this.cupWeight = cupWeight;
        this.isRecommend = isRecommend;
        this.grainFree = grainFree;
        this.avaQuantity = avaQuantity;
        this.quantity = quantity;
        this.weight = weight;
        this.approve = approve;
        this.productImageList = productImageList;
        this.feed = feed;
        this.petDoctorFeedDescription = petDoctorFeedDescription;
    }

    @Transient
    List<ProductImage> productImageList;

    @Transient
    Feed feed;

    @Transient
    PetDoctorFeedDescription petDoctorFeedDescription;

    @Getter
    @RequiredArgsConstructor
    public enum PackageType {
        CAN("C", "캔", "캔", "캔"),
        POUCH("P", "파우치", "팩", "파우치"),
        NORMAL("N", "종이컵", "컵", "일반사료 비닐팩"),
        CUP("U", "종이컵", "컵", "컵"),
        VARIETY("B", "종이컵", "컵", "버라이어티 팩"),
        ZIPPER_BAG("Z", "종이컵", "컵", "지퍼백"),
        ECO("G", "종이컵", "컵", "친환경 포장지"),
        ETC("E", "종이컵", "컵", "기타");

        private final String type;
        private final String cupType;
        private final String cupSubType;
        private final String unit;

        public static PackageType ofLegacyType(final String type) {
            return Arrays.stream(values())
                .filter(packageType -> packageType.type.equals(type))
                .findFirst()
                .orElse(null);
        }

    }
}
