package com.pood.server.entity.meta.dto;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class OrderInfoDto {

    String userName;
    String phoneNumber;
    String date;
    String orderNumber;
    String orderName;
    String price;

}
