package com.pood.server.entity.meta.dto.user_pet_worry_manaer;

import java.util.List;

public interface UserPetWorryManager {

    String getDailyLife();

    String getDiet(final String petName);

    List<String> getSolution();


}
