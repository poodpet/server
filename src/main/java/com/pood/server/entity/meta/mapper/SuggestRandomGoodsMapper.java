package com.pood.server.entity.meta.mapper;

import com.querydsl.core.annotations.QueryProjection;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class SuggestRandomGoodsMapper {

    private final int idx;
    private final String goodsName;
    private final int goodsOriginPrice;
    private final Long reviewCnt;
    private final double averageRating;
    private final String mainImage;
    private int goodsPrice;
    private int discountRate;
    private int discountPrice;

    private int saleStatus;
    private String brandName;
    private boolean newest;

    @QueryProjection
    public SuggestRandomGoodsMapper(final int idx, final String goodsName,
        final int goodsOriginPrice, final int goodsPrice,
        final int discountRate, final int discountPrice,
        final Long reviewCnt, final double averageRating, final String mainImage,
        final int saleStatus, final String brandName, final boolean newest) {
        this.idx = idx;
        this.goodsName = goodsName;
        this.goodsOriginPrice = goodsOriginPrice;
        this.goodsPrice = goodsPrice;
        this.discountRate = discountRate;
        this.discountPrice = discountPrice;
        this.reviewCnt = reviewCnt;
        this.averageRating = averageRating;
        this.mainImage = mainImage;
        this.saleStatus = saleStatus;
        this.brandName = brandName;
        this.newest = newest;
    }

    public boolean eqIdx(final int idx) {
        return this.idx == idx;
    }

    public void promotionIfExistUpdatePrice(final int prPrice, final int prDiscountPrice,
        final int prDiscountRate) {
        this.goodsPrice = prPrice;
        this.discountPrice = prDiscountPrice;
        this.discountRate = prDiscountRate;
    }
}

