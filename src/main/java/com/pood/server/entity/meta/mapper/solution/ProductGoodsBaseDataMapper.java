package com.pood.server.entity.meta.mapper.solution;

import com.pood.server.entity.meta.dto.ProductGoodsBaseDataDto;
import com.querydsl.core.annotations.QueryProjection;
import lombok.Value;

@Value
public class ProductGoodsBaseDataMapper {

    Integer goodsIdx;
    Integer pcIdx;
    String displayType;
    String goodsName;
    Integer goodsOriginPrice;
    Integer goodsPrice;
    Integer discountRate;
    Integer discountPrice;
    Integer saleStatus;
    Integer mainProduct;
    Double averageRating;
    String mainImage;
    Boolean isRecommend;
    String brandName;
    boolean isNewest;
    Long ctIdx;
    Long ctSubIdx;
    String mainProperty;
    String tasty;
    Integer calorie;
    Integer feedTarget;
    String feedType;
    Integer glutenFree;
    String animalProtein;
    String vegetableProtein;
    String aafco;
    Integer singleProtein;
    Double unitSize;
    String ingredientsSearch;
    Integer cupWeight;
    Integer grainFree;

    @QueryProjection
    public ProductGoodsBaseDataMapper(final Integer goodsIdx, final Integer pcIdx,
        final String displayType, final String goodsName, final Integer goodsOriginPrice,
        final Integer goodsPrice, final Integer discountRate, final Integer discountPrice,
        final Integer saleStatus, final Integer mainProduct,
        final Double averageRating, final String mainImage, final Boolean isRecommend,
        final String brandName, final boolean isNewest, final Long ctIdx,
        final Long ctSubIdx, final String mainProperty, final String tasty,
        final Integer calorie, final Integer feedTarget, final String feedType,
        final Integer glutenFree, final String animalProtein, final String vegetableProtein,
        final String aafco, final Integer singleProtein, final Double unitSize,
        final String ingredientsSearch, final Integer cupWeight, final Integer grainFree) {

        this.goodsIdx = goodsIdx;
        this.pcIdx = pcIdx;
        this.displayType = displayType;
        this.goodsName = goodsName;
        this.goodsOriginPrice = goodsOriginPrice;
        this.goodsPrice = goodsPrice;
        this.discountRate = discountRate;
        this.discountPrice = discountPrice;
        this.saleStatus = saleStatus;
        this.mainProduct = mainProduct;
        this.averageRating = averageRating;
        this.mainImage = mainImage;
        this.isRecommend = isRecommend;
        this.brandName = brandName;
        this.isNewest = isNewest;
        this.ctIdx = ctIdx;
        this.ctSubIdx = ctSubIdx;
        this.mainProperty = mainProperty;
        this.tasty = tasty;
        this.calorie = calorie;
        this.feedTarget = feedTarget;
        this.feedType = feedType;
        this.glutenFree = glutenFree;
        this.animalProtein = animalProtein;
        this.vegetableProtein = vegetableProtein;
        this.aafco = aafco;
        this.singleProtein = singleProtein;
        this.unitSize = unitSize;
        this.ingredientsSearch = ingredientsSearch;
        this.cupWeight = cupWeight;
        this.grainFree = grainFree;
    }

    public ProductGoodsBaseDataDto toDataDto() {
        return new ProductGoodsBaseDataDto(
            goodsIdx, pcIdx, displayType, goodsName, goodsOriginPrice, goodsPrice, discountRate,
            discountPrice, saleStatus, mainProduct, averageRating, mainImage, isRecommend,
            brandName, isNewest, ctIdx, ctSubIdx, mainProperty, animalProtein,calorie);
    }
}
