package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "feed_dm")
public class FeedDm  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "product_idx")
    private Integer productIdx;

    @Column(name = "feed_idx")
    private Integer feedIdx;

    @Column(name = "feed_name")
    private String feedName;

    @Column(name = "pr_protein")
    private Double prProtein;

    @Column(name = "pr_fat")
    private Double prFat;

    @Column(name = "pr_ash")
    private Double prAsh;

    @Column(name = "pr_fiber")
    private Double prFiber;

    @Column(name = "pr_carbo")
    private Double prCarbo;

    @Column(name = "am_arginine")
    private Double amArginine;

    @Column(name = "am_histidine")
    private Double amHistidine;

    @Column(name = "am_isoleucine")
    private Double amIsoleucine;

    @Column(name = "am_leucine")
    private Double amLeucine;

    @Column(name = "am_lysine")
    private Double amLysine;

    @Column(name = "am_met_cys")
    private Double amMetCys;

    @Column(name = "am_methionine")
    private Double amMethionine;

    @Column(name = "am_phe_tyr")
    private Double amPheTyr;

    @Column(name = "am_phenylanlanine")
    private Double amPhenylanlanine;

    @Column(name = "am_threonine")
    private Double amThreonine;

    @Column(name = "am_tryptophan")
    private Double amTryptophan;

    @Column(name = "am_valine")
    private Double amValine;

    @Column(name = "am_cystine")
    private Double amCystine;

    @Column(name = "am_tyrosine")
    private Double amTyrosine;

    @Column(name = "am_l_carnitine")
    private Double amLCarnitine;

    @Column(name = "am_glutamic_acid")
    private Double amGlutamicAcid;

    @Column(name = "mi_calcium")
    private Double miCalcium;

    @Column(name = "mi_phosphours")
    private Double miPhosphours;

    @Column(name = "mi_potassium")
    private Double miPotassium;

    @Column(name = "mi_sodium")
    private Double miSodium;

    @Column(name = "mi_chloride")
    private Double miChloride;

    @Column(name = "mi_magnessium")
    private Double miMagnessium;

    @Column(name = "mi_iron")
    private Double miIron;

    @Column(name = "mi_copper")
    private Double miCopper;

    @Column(name = "mi_manganese")
    private Double miManganese;

    @Column(name = "mi_zinc")
    private Double miZinc;

    @Column(name = "mi_iodine")
    private Double miIodine;

    @Column(name = "mi_selenium")
    private Double miSelenium;

    @Column(name = "vi_vitamin_a")
    private Double viVitaminA;

    @Column(name = "vi_vitamin_d")
    private Double viVitaminD;

    @Column(name = "vi_vitamin_e")
    private Double viVitaminE;

    @Column(name = "vi_vitamin_b1")
    private Double viVitaminB1;

    @Column(name = "vi_vitamin_b2")
    private Double viVitaminB2;

    @Column(name = "vi_vitamin_b3")
    private Double viVitaminB3;

    @Column(name = "vi_vitamin_b5")
    private Double viVitaminB5;

    @Column(name = "vi_vitamin_b6")
    private Double viVitaminB6;

    @Column(name = "vi_vitamin_b7")
    private Double viVitaminB7;

    @Column(name = "vi_vitamin_b9")
    private Double viVitaminB9;

    @Column(name = "vi_vitamin_b12")
    private Double viVitaminB12;

    @Column(name = "vi_Choline")
    private Double viCholine;

    @Column(name = "vi_vitamin_c")
    private Double viVitaminC;

    @Column(name = "vi_vitamin_k3")
    private Double viVitaminK3;

    @Column(name = "fa_epa_dha")
    private Double faEpaDha;

    @Column(name = "fa_omega3")
    private Double faOmega3;

    @Column(name = "fa_omega6")
    private Double faOmega6;

    @Column(name = "fa_linoleicacid")
    private Double faLinoleicacid;

    @Column(name = "fa_arachidonic_acid")
    private Double faArachidonicAcid;

    @Column(name = "fa_a_linoleicacid")
    private Double faALinoleicacid;

    @Column(name = "ot_taurine")
    private Double otTaurine;

    @Column(name = "ot_glucosamine")
    private Double otGlucosamine;

    @Column(name = "ot_chondroitin")
    private Double otChondroitin;

    @Column(name = "ot_msm")
    private Double otMsm;

    @Column(name = "ot_probiotics")
    private String otProbiotics;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
