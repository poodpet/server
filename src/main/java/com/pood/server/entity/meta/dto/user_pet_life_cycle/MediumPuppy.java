package com.pood.server.entity.meta.dto.user_pet_life_cycle;

import java.util.List;

public class MediumPuppy implements UserPetLifeCycle {

    private static final String SLEEP = "사람보다 훨씬 더 많은 수면이 필요하니 하루 중 18시간 이상 자는 데 시간을 보낼 거예요. 그러니 너무 많이 잔다고 해서 걱정하지 마세요.";
    private static final String TEETH = "치아 건강에는 칫솔질만한 것도 없어요. 칫솔질은 잇몸과 치아 사이의 연결 부분에 집중하여 닦아주는 것이 좋아요. 특히 어릴 때 손가락을 입 주위에 갖다 대는 것부터 천천히 연습해보세요. 칫솔질에 거부감을 느낀다면 치킨 맛 치약을 사용하는 것도 좋은 방법이에요.";
    private static final String FECES = "소변은 투명한 노란색이 정상이며, 변은 약간 묻어나오는 정도가 좋아요.";
    private static final String DIET = "짧은 기간 빠르게 성장해요. 이 시기에 뼈를 튼튼하게 하기 위해서는 칼슘, 인 등 많은 미네랄을 섭취해야 하는 것이 중요하죠. 근육과 올바른 성장을 위해 많은 단백질과 필수지방산이 필요해요. 성견에 비하여 위가 작아서 고칼로리 식단이 필요합니다.";
    private static final String HAIR_CARE_AND_BATH = "배냇털이 빠지기 시작하는 시기에요. 털을 모두 밀면 자극이 될 수 있으니 가위미용 정도 살짝 정리해주시는 것을 추천해요. 너무 잦은 목욕은 피부에 자극이 될 수 있으니 2주에 한 번 이하가 적당해요.";
    private static final String VACCINE = "6번에 나누어 종합백신, 코로나, 켄넬코프, 신종플루, 광견병 예방백신을 맞아야해요. 5번의 종합백신과 1번의 광견병 주사는 필수이며, 코로나 장염과 켄넬 코프, 신종인플루엔자는 권고사항입니다.";
    private static final String NEUTERING = "첫 발정이 오기 전에 수술을 하는 것이 건강에 좋아요. 암컷의 경우에는 발정이 오면 생식기에 피가 약간 맺혀 있을 수 있어요. 정상적인 것이니 놀라지 마세요.";
    private static final List<String> SOLUTION = List.of("첫 발정이 오기 전 중성화 수술 해주기",
        "목욕은 2주에 한 번 이하",
        "고칼로리 식단 챙기기");

    @Override
    public String getSleep() {
        return SLEEP;
    }

    @Override
    public String getTeeth() {
        return TEETH;
    }

    @Override
    public String getFeces() {
        return FECES;
    }

    @Override
    public String getDiet() {
        return DIET;
    }

    @Override
    public String getBath() {
        return null;
    }

    @Override
    public String getHairCareAndBath() {
        return HAIR_CARE_AND_BATH;
    }

    @Override
    public String getWeightAndRegularCheck() {
        return null;
    }

    @Override
    public String getVaccine() {
        return VACCINE;
    }

    @Override
    public String getNeutering() {
        return NEUTERING;
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }
}
