package com.pood.server.entity.meta;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "goods_coupon")
public class GoodsCoupon  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "굿즈 항목 번호 : order_db.goods.idx")
    @Column(name = "goods_idx")
    private Integer goodsIdx;

    @ApiModelProperty(value = "쿠폰 항목 번호 : meta_db.coupon.idx")
    @Column(name = "coupon_idx")
    private Integer couponIdx;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
