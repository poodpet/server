package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "maintab_brand")
public class MaintabBrand  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "maintab_idx")
    private Integer maintabIdx;

    @Column(name = "brand_idx")
    private Integer brandIdx;

    @Column(name = "image")
    private String image;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "visible")
    private Integer visible;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
