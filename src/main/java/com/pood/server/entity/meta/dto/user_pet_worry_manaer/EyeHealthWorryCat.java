package com.pood.server.entity.meta.dto.user_pet_worry_manaer;

import java.util.List;

public class EyeHealthWorryCat implements UserPetWorryManager {

    private static final String DAILY_LIFE = "눈물 자국은 음식 알러지의 가능성이 높아, 알러지가 있는 단백질원을 피하는 식단 관리가 필요해요. 하루 한 번 이상 눈 주변을 자주 닦아주는 것이 좋아요. 눈물이 굳어 있는 상태에서 닦으면 피부에 상처가 날 수 있으니, 물을 묻힌 부드러운 휴지로 눈꼽을 녹여서 깨끗이 닦아주세요. 눈이 잘 보이지 않는 것 같다면 집 가구의 날카로운 모서리를 스펀지로 감싸주세요. ";
    private static final String DIET_1 = "눈이 약한 ";
    private static final String DIET_2 = "에게는 오메가3함량이 높고, 눈건강에 도움이 되는 기능성 영양소인 루테인, 제아잔틴이 좋아요. 눈의 황반을 구성하는 루테인과 제아잔틴은 백내장 발생 위험을 낮추어 줄 거예요. 눈을 산화적 스트레스로부터 보호해주는 항산화 물질이 풍부한 식단이 도움이 됩니다.";
    private static final List<String> SOLUTION = List.of("눈물자국은 식단 관리, 자주 닦아주기",
        "베타카로틴, 루테인, 제아잔틴 함유된 식단 챙기기",
        "오메가3가 풍부한 식단 챙기기",
        "항산화제가 풍부한 식단 챙기기");

    @Override
    public String getDailyLife() {
        return DAILY_LIFE;
    }

    @Override
    public String getDiet(final String petName) {
        return new StringBuilder(DIET_1).append(petName).append(DIET_2).toString();
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }

}
