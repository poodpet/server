package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "maintab_promotion")
public class MaintabPromotion  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "maintab_idx")
    private Integer maintabIdx;

    @Column(name = "pr_idx")
    private Integer prIdx;

    @Column(name = "page_landing_idx")
    private Integer pageLandingIdx;

    @Column(name = "display_type")
    private String displayType;

    @Column(name = "visible")
    private Integer visible;

    @Column(name = "name")
    private String name;

    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "url")
    private String url;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
