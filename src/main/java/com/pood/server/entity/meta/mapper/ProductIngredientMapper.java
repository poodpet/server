package com.pood.server.entity.meta.mapper;

import com.querydsl.core.annotations.QueryProjection;
import lombok.Value;

@Value
public class ProductIngredientMapper {

    Integer idx;
    String ingredient;

    @QueryProjection
    public ProductIngredientMapper(final Integer idx, final String ingredient) {

        this.idx = idx;
        this.ingredient = ingredient;
    }
}
