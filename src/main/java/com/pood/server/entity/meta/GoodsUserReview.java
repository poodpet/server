package com.pood.server.entity.meta;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name = "goods_join_user_review")
@Getter
public class GoodsUserReview {

    @Id
    @Column(name = "idx", nullable = false)
    private Integer idx;

    @Column(name = "pc_idx")
    private Integer pcIdx;

    @Column(name = "seller_idx")
    private Integer sellerIdx;

    @Column(name = "goods_type_idx")
    private Integer goodsTypeIdx;

    @Column(name = "coupon_apply")
    private Integer couponApply;

    @Column(name = "goods_name")
    private String goodsName;

    @Column(name = "goods_descv")
    private String goodsDescv;

    @Column(name = "goods_origin_price")
    private Integer goodsOriginPrice;

    @Column(name = "goods_price")
    private Integer goodsPrice;

    @Column(name = "discount_rate")
    private Integer discountRate;

    @Column(name = "discount_price")
    private Integer discountPrice;

    @Column(name = "display_type", length = 2)
    private String displayType;

    @Column(name = "refund_type")
    private Integer refundType;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "purchase_type")
    private Integer purchaseType;

    @Column(name = "sale_status")
    private Integer saleStatus;

    @Column(name = "startdate")
    private LocalDateTime startdate;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    @Column(name = "visible")
    private Integer visible;

    @Column(name = "isPromotion")
    private Integer isPromotion;

    @Column(name = "pr_price")
    private Integer prPrice;

    @Column(name = "pr_discount_price")
    private Integer prDiscountPrice;

    @Column(name = "pr_discount_rate")
    private Integer prDiscountRate;

    @Column(name = "limit_quantity")
    private Integer limitQuantity;

    @Column(name = "main_product")
    private Integer mainProduct;

    @Column(name = "average_rating")
    private Double averageRating;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    @Column(name = "is_recommend")
    private Boolean isRecommend;

    @Column(name = "review_cnt", nullable = false)
    private Long reviewCnt;

}