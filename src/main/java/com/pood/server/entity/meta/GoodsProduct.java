package com.pood.server.entity.meta;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "goods_product")
public class GoodsProduct  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "굿즈 항목 번호 : order_db.goods.idx")
    @Column(name = "goods_idx")
    private Integer goodsIdx;

    @ApiModelProperty(value = "상품 항목 번호 : meta_db.product.idx")
    @Column(name = "product_idx")
    private Integer productIdx;

    @ApiModelProperty(value = "구성품 상품 개수")
    @Column(name = "product_qty")
    private Integer productQty;

    @Column(name = "product_price")
    private Integer productPrice;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
