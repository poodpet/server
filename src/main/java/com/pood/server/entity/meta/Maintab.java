package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "maintab")
public class Maintab  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "ct_idx")
    private Integer ctIdx;

    @Column(name = "ct_name")
    private String ctName;

    @Column(name = "pc_id")
    private Integer pcId;

    @Column(name = "type")
    private Integer type;

    @Column(name = "type_name")
    private String typeName;

    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    @Column(name = "status")
    private Integer status;

    @Column(name = "title")
    private String title;

    @Column(name = "title_desc")
    private String titleDesc;

    @Column(name = "display_type")
    private String displayType;

    @Column(name = "master_slave")
    private Integer masterSlave;

    @Column(name = "number_tag")
    private Integer numberTag;

    @Column(name = "visible")
    private Integer visible;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
