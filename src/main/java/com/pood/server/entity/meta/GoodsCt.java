package com.pood.server.entity.meta;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "goods_ct")
public class GoodsCt  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Long idx;

    @Column(name = "name")
    private String name;

    @Column(name = "url")
    private String url;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    public boolean isIdxEq(long idx){
        return this.idx.equals(idx);
    }

}
