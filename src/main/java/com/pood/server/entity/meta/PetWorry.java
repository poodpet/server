package com.pood.server.entity.meta;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "pet_worry")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PetWorry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "pc_idx")
    private PetCategory petCategory;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ai_recommend_diagnosis_idx")
    private AiRecommendDiagnosis aiRecommendDiagnosis;

    @Column(name = "name", length = 500)
    private String name;

    @Column(name = "dog_url", length = 500)
    private String dogUrl;

    @Column(name = "cat_url", length = 500)
    private String catUrl;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "record_birth")
    private LocalDateTime recordBirth;

}