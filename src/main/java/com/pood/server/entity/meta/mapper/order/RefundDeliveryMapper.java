package com.pood.server.entity.meta.mapper.order;

import com.querydsl.core.annotations.QueryProjection;
import lombok.Value;

@Value
public class RefundDeliveryMapper {

    Integer idx;
    String uuid;
    String orderNumber;
    Integer goodsIdx;
    Integer qty;
    String nickname;
    String zipcode;
    String receiver;
    String address;
    String addressDetail;
    String phoneNumber;

    @QueryProjection
    public RefundDeliveryMapper(final Integer idx, final String uuid, final String orderNumber,
        final Integer goodsIdx, final Integer qty, final String nickname, final String zipcode,
        final String receiver, final String address, final String addressDetail, final String phoneNumber) {
        this.idx = idx;
        this.uuid = uuid;
        this.orderNumber = orderNumber;
        this.goodsIdx = goodsIdx;
        this.qty = qty;
        this.nickname = nickname;
        this.zipcode = zipcode;
        this.receiver = receiver;
        this.address = address;
        this.addressDetail = addressDetail;
        this.phoneNumber = phoneNumber;
    }
}
