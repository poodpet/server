package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "feed")
public class Feed  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "product_idx")
    private Integer productIdx;

    @Column(name = "feed_name")
    private String feedName;

    @Column(name = "pr_protein")
    private Double prProtein;

    @Column(name = "pr_fat")
    private Double prFat;

    @Column(name = "pr_ash")
    private Double prAsh;

    @Column(name = "pr_fiber")
    private Double prFiber;

    @Column(name = "pr_moisture")
    private Double prMoisture;

    @Column(name = "pr_carbo")
    private Double prCarbo;

    @Column(name = "am_arginine")
    private Double amArginine;

    @Column(name = "am_histidine")
    private Double amHistidine;

    @Column(name = "am_isoleucine")
    private Double amIsoleucine;

    @Column(name = "am_leucine")
    private Double amLeucine;

    @Column(name = "am_lysine")
    private Double amLysine;

    @Column(name = "am_met_cys")
    private Double amMetCys;

    @Column(name = "am_methionine")
    private Double amMethionine;

    @Column(name = "am_phe_tyr")
    private Double amPheTyr;

    @Column(name = "am_phenylanlanine")
    private Double amPhenylanlanine;

    @Column(name = "am_threonine")
    private Double amThreonine;

    @Column(name = "am_tryptophan")
    private Double amTryptophan;

    @Column(name = "am_valine")
    private Double amValine;

    @Column(name = "am_cystine")
    private Double amCystine;

    @Column(name = "am_tyrosine")
    private Double amTyrosine;

    @Column(name = "am_l_carnitine")
    private Double amLCarnitine;

    @Column(name = "am_glutamic_acid")
    private Double amGlutamicAcid;

    @Column(name = "mi_calcium")
    private Double miCalcium;

    @Column(name = "mi_phosphours")
    private Double miPhosphours;

    @Column(name = "mi_potassium")
    private Double miPotassium;

    @Column(name = "mi_sodium")
    private Double miSodium;

    @Column(name = "mi_chloride")
    private Double miChloride;

    @Column(name = "mi_magnessium")
    private Double miMagnessium;

    @Column(name = "mi_iron")
    private Double miIron;

    @Column(name = "mi_copper")
    private Double miCopper;

    @Column(name = "mi_manganese")
    private Double miManganese;

    @Column(name = "mi_zinc")
    private Double miZinc;

    @Column(name = "mi_iodine")
    private Double miIodine;

    @Column(name = "mi_selenium")
    private Double miSelenium;

    @Column(name = "mi_ca_ph")
    private Double miCaPh;

    @Column(name = "vi_vitamin_a")
    private Double viVitaminA;

    @Column(name = "vi_vitamin_d")
    private Double viVitaminD;

    @Column(name = "vi_vitamin_e")
    private Double viVitaminE;

    @Column(name = "vi_vitamin_b1")
    private Double viVitaminB1;

    @Column(name = "vi_vitamin_b2")
    private Double viVitaminB2;

    @Column(name = "vi_vitamin_b3")
    private Double viVitaminB3;

    @Column(name = "vi_vitamin_b5")
    private Double viVitaminB5;

    @Column(name = "vi_vitamin_b6")
    private Double viVitaminB6;

    @Column(name = "vi_vitamin_b7")
    private Double viVitaminB7;

    @Column(name = "vi_vitamin_b9")
    private Double viVitaminB9;

    @Column(name = "vi_vitamin_b12")
    private Double viVitaminB12;

    @Column(name = "vi_Choline")
    private Double viCholine;

    @Column(name = "vi_vitamin_c")
    private Double viVitaminC;

    @Column(name = "vi_vitamin_k3")
    private Double viVitaminK3;

    @Column(name = "fa_omega3")
    private Double faOmega3;

    @Column(name = "fa_epa")
    private Double faEpa;

    @Column(name = "fa_dha")
    private Double faDha;

    @Column(name = "fa_epa_dha")
    private Double faEpaDha;

    @Column(name = "fa_omega6")
    private Double faOmega6;

    @Column(name = "fa_linoleicacid")
    private Double faLinoleicacid;

    @Column(name = "fa_arachidonic_acid")
    private Double faArachidonicAcid;

    @Column(name = "fa_a_linoleicacid")
    private Double faALinoleicacid;

    @Column(name = "fa_omega6_3")
    private Double faOmega63;

    @Column(name = "ot_taurine")
    private Double otTaurine;

    @Column(name = "ot_glucosamine")
    private Double otGlucosamine;

    @Column(name = "ot_chondroitin")
    private Double otChondroitin;

    @Column(name = "ot_msm")
    private Double otMsm;

    @Column(name = "ot_probiotics")
    private String otProbiotics;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    @Builder
    public Feed(Integer idx, Integer productIdx, String feedName, Double prProtein,
        Double prFat, Double prAsh, Double prFiber, Double prMoisture, Double prCarbo,
        Double amArginine, Double amHistidine, Double amIsoleucine, Double amLeucine,
        Double amLysine, Double amMetCys, Double amMethionine, Double amPheTyr,
        Double amPhenylanlanine, Double amThreonine, Double amTryptophan, Double amValine,
        Double amCystine, Double amTyrosine, Double amLCarnitine, Double amGlutamicAcid,
        Double miCalcium, Double miPhosphours, Double miPotassium, Double miSodium,
        Double miChloride, Double miMagnessium, Double miIron, Double miCopper,
        Double miManganese, Double miZinc, Double miIodine, Double miSelenium, Double miCaPh,
        Double viVitaminA, Double viVitaminD, Double viVitaminE, Double viVitaminB1,
        Double viVitaminB2, Double viVitaminB3, Double viVitaminB5, Double viVitaminB6,
        Double viVitaminB7, Double viVitaminB9, Double viVitaminB12, Double viCholine,
        Double viVitaminC, Double viVitaminK3, Double faOmega3, Double faEpa, Double faDha,
        Double faEpaDha, Double faOmega6, Double faLinoleicacid, Double faArachidonicAcid,
        Double faALinoleicacid, Double faOmega63, Double otTaurine, Double otGlucosamine,
        Double otChondroitin, Double otMsm, String otProbiotics) {
        this.idx = idx;
        this.productIdx = productIdx;
        this.feedName = feedName;
        this.prProtein = prProtein;
        this.prFat = prFat;
        this.prAsh = prAsh;
        this.prFiber = prFiber;
        this.prMoisture = prMoisture;
        this.prCarbo = prCarbo;
        this.amArginine = amArginine;
        this.amHistidine = amHistidine;
        this.amIsoleucine = amIsoleucine;
        this.amLeucine = amLeucine;
        this.amLysine = amLysine;
        this.amMetCys = amMetCys;
        this.amMethionine = amMethionine;
        this.amPheTyr = amPheTyr;
        this.amPhenylanlanine = amPhenylanlanine;
        this.amThreonine = amThreonine;
        this.amTryptophan = amTryptophan;
        this.amValine = amValine;
        this.amCystine = amCystine;
        this.amTyrosine = amTyrosine;
        this.amLCarnitine = amLCarnitine;
        this.amGlutamicAcid = amGlutamicAcid;
        this.miCalcium = miCalcium;
        this.miPhosphours = miPhosphours;
        this.miPotassium = miPotassium;
        this.miSodium = miSodium;
        this.miChloride = miChloride;
        this.miMagnessium = miMagnessium;
        this.miIron = miIron;
        this.miCopper = miCopper;
        this.miManganese = miManganese;
        this.miZinc = miZinc;
        this.miIodine = miIodine;
        this.miSelenium = miSelenium;
        this.miCaPh = miCaPh;
        this.viVitaminA = viVitaminA;
        this.viVitaminD = viVitaminD;
        this.viVitaminE = viVitaminE;
        this.viVitaminB1 = viVitaminB1;
        this.viVitaminB2 = viVitaminB2;
        this.viVitaminB3 = viVitaminB3;
        this.viVitaminB5 = viVitaminB5;
        this.viVitaminB6 = viVitaminB6;
        this.viVitaminB7 = viVitaminB7;
        this.viVitaminB9 = viVitaminB9;
        this.viVitaminB12 = viVitaminB12;
        this.viCholine = viCholine;
        this.viVitaminC = viVitaminC;
        this.viVitaminK3 = viVitaminK3;
        this.faOmega3 = faOmega3;
        this.faEpa = faEpa;
        this.faDha = faDha;
        this.faEpaDha = faEpaDha;
        this.faOmega6 = faOmega6;
        this.faLinoleicacid = faLinoleicacid;
        this.faArachidonicAcid = faArachidonicAcid;
        this.faALinoleicacid = faALinoleicacid;
        this.faOmega63 = faOmega63;
        this.otTaurine = otTaurine;
        this.otGlucosamine = otGlucosamine;
        this.otChondroitin = otChondroitin;
        this.otMsm = otMsm;
        this.otProbiotics = otProbiotics;
    }
}
