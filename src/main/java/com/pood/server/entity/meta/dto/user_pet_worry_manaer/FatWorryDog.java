package com.pood.server.entity.meta.dto.user_pet_worry_manaer;

import java.util.List;

public class FatWorryDog implements UserPetWorryManager {

    private static final String DAILY_LIFE = "비만은 관절염, 췌장염, 2형당뇨, 종양, 호흡기 질환을 유발하여 수명이 2년 가량 짧아질 수 있어요. 특히 중성화 수술을 한 이후에는 호르몬 변화로 살이 찌기 쉬운 상태가 되기 때문에, 먹는 열량을 조절해주어야 하며, 먹는 사료도 칼로리가 낮고 포만감이 있는 사료로 바꾸어 주는 것이 좋습니다.";
    private static final String DIET_1 = "다이어트가 필요한 " ;
    private static final String DIET_2 = "에게는 지방이 낮고 열량이 낮은 사료가 좋아요. 칼로리는 낮지만 필수영양소는 모두 함유한 비타민과 미네랄이 풍부한 식단이 필요해요. 단백과 식이섬유, 수분의 함량이 높으면 같은 양을 먹더라도 포만감을 느끼게 해줄 거예요.  L-카르니틴은 지방이 쌓이지 않고 바로 에너지로 쓰게 하여 다이어트에 도움이 돼요. 간식도 제한하는 것이 필요하답니다. 강아지 껌의 경우에 평균적으로 30kcal 이상으로 열량이 높아 반 개로 잘라 주거나, 하루 하나만 주세요. 야채와 같이 칼로리가 낮고 배를 쉽게 부르게 해주는 간식을 추천해요." ;
    private static final List<String> SOLUTION = List.of("지방이 낮고, 열량이 낮은 식단 챙기기",
        "고단백 식단 챙기기",
        "식이섬유가 풍부한 식단 챙기기",
        "습식도 좋은 선택",
        "L-카르니틴 함유된 식단 챙기기",
        "껌은 하루에 하나만, 되도록 야채 간식으로");

    @Override
    public String getDailyLife() {
        return DAILY_LIFE;
    }

    @Override
    public String getDiet(final String petName) {
        return new StringBuilder(DIET_1).append(petName).append(DIET_2).toString();
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }

}
