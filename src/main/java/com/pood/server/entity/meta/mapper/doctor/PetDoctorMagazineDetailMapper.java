package com.pood.server.entity.meta.mapper.doctor;

import com.querydsl.core.annotations.QueryProjection;
import lombok.Value;

@Value
public class PetDoctorMagazineDetailMapper {
    long idx;
    String ctName;
    String title;
    String contents;

    @QueryProjection
    public PetDoctorMagazineDetailMapper(long idx, String contents, String ctName, String title) {
        this.idx = idx;
        this.contents = contents;
        this.ctName = ctName;
        this.title = title;
    }
}
