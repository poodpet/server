package com.pood.server.entity.meta.mapper.event;

import com.querydsl.core.annotations.QueryProjection;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import lombok.Value;

@Value
public class EventDonationMapper {

    private static final int END = 2;

    Integer idx;
    String title;
    String intro;
    String startDate;
    String endDate;
    Integer status;
    Integer pcIdx;

    @QueryProjection
    public EventDonationMapper(final Integer idx, final String title, final String intro,
        final String startDate, final String endDate, final Integer status, final Integer pcIdx) {

        this.idx = idx;
        this.title = title;
        this.intro = intro;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = convertStatus(endDate, status);
        this.pcIdx = pcIdx;
    }

    private Integer convertStatus(final String endDate, final Integer status) {
        return LocalDateTime.now()
            .isBefore(LocalDateTime.parse(endDate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))) ? status : END;
    }
}