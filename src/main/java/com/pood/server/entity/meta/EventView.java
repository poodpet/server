package com.pood.server.entity.meta;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "event_view")
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class EventView {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "pet_category_idx", nullable = false)
    private PetCategory petCategory;

    private int priority;

    @ManyToOne
    @JoinColumn(name = "event_info_idx", nullable = false)
    private EventInfo eventInfo;

    @Column(name = "recordbirth")
    private LocalDateTime recordBirth;
}
