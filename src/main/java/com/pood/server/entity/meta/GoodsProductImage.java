package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "goods_product_image")
public class GoodsProductImage  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "goods_idx")
    private Integer goodsIdx;

    @Column(name = "goods_image_idx")
    private Integer goodsImageIdx;

    @Column(name = "product_image_idx")
    private Integer productImageIdx;

    @Column(name = "image_type")
    private Integer imageType;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "type")
    private Integer type;

    @Column(name = "url")
    private String url;

    @Column(name = "image_priority")
    private Integer imagePriority;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
