package com.pood.server.entity.meta.dto.goods;

import java.util.List;
import lombok.Value;

@Value
public class GoodsProductGroupByGoodsDto {

    Integer goodsIdx;
    List<Integer> productIdxList;
}
