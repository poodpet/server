package com.pood.server.entity.meta.dto.user_pet_worry_manaer;

import java.util.List;

public class ThrowUpWorryDog implements UserPetWorryManager {

    private static final String DAILY_LIFE = "하루에 여러 번 구토를 반복한다면 동물병원에 방문하여 검사를 받아보는게 좋아요. 하루 한 번 까지는 좀 더 지켜보시되 이틀이상 반복적인 구토가 지속된다면 바로 동물병원에 방문해주세요. 구토를 하고 두 시간 정도는 아무것도 먹이지 않는 것이 좋고, 물>습식 사료>건식 사료 순서로 급여해주세요.";
    private static final String DIET = "사료가 그대로 토로 나온 경우, 급히 먹어서 그럴 수 있어요. 특히 건사료는 위에서 부피가 부풀기 때문에 천천히 소량으로 나누어 주는 것이 좋아요. 노란 색 또는 투명한 색의 토를 한 경우 위액이 역류한 경우에요. 스케쥴에 민감한 일부 강아지는 먹던 시간에 식사를 하지 못한 경우 위액은 나오지만 소화할 먹거리가 없어 구토로 이어진답니다.";
    private static final List<String> SOLUTION = List.of("건사료보다는 습식사료 챙기기",
        "소량으로 나누어 급여하기");


    @Override
    public String getDailyLife() {
        return DAILY_LIFE;
    }

    @Override
    public String getDiet(final String petName) {
        return DIET;
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }

}
