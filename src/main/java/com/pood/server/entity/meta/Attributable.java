package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "attributable")
public class Attributable  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "type1")
    private Integer type1;

    @Column(name = "type2")
    private Integer type2;

    @Column(name = "text")
    private String text;

    @Column(name = "code")
    private Integer code;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
