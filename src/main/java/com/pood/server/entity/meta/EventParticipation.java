package com.pood.server.entity.meta;

import com.pood.server.entity.user.UserInfo;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;


@Entity
@Getter
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "event_participation")
public class EventParticipation  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Long idx;

    @ManyToOne
    @JoinColumn(name = "event_info_idx", insertable = true, updatable = false)
    private EventInfo eventInfo;

    @Column(name = "user_info_idx")
    private Integer userInfoIdx;

    @Column(name = "ranking")
    private String ranking;


    @CreatedDate
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    public EventParticipation(EventInfo eventInfo, Integer userInfoIdx) {
        this.eventInfo = eventInfo;
        this.userInfoIdx = userInfoIdx;
    }
}
