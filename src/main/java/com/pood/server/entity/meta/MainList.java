package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "main_list")
public class MainList  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "type_idx")
    private Integer typeIdx;

    @Column(name = "title")
    private String title;

    @Column(name = "startdate")
    private LocalDateTime startdate;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    @Column(name = "status")
    private Integer status;

    @Column(name = "type_priority")
    private Integer typePriority;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "item_cnt")
    private Integer itemCnt;

    @Column(name = "pc_id")
    private Integer pcId;

    @Column(name = "scheme_uri")
    private String schemeUri;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
