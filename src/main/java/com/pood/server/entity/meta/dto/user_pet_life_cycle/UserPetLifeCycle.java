package com.pood.server.entity.meta.dto.user_pet_life_cycle;

import java.util.List;

public interface UserPetLifeCycle {


    String getSleep();

    String getTeeth();

    String getFeces();

    String getDiet();

    String getBath();

    String getHairCareAndBath();

    String getWeightAndRegularCheck();

    String getVaccine();

    String getNeutering();

    List<String> getSolution();
}
