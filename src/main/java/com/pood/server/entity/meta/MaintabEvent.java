package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "maintab_event")
public class MaintabEvent  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "maintab_idx")
    private Integer maintabIdx;

    @Column(name = "event_idx")
    private Integer eventIdx;

    @Column(name = "page_landing_idx")
    private Integer pageLandingIdx;

    @Column(name = "display_type")
    private String displayType;

    @Column(name = "title")
    private String title;

    @Column(name = "image")
    private String image;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "visible")
    private Integer visible;

    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
