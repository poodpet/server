package com.pood.server.entity.meta;

import com.pood.server.service.pet_solution.ArdCodeMakerAnnotation;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "pet_doctor_feed_description")
public class PetDoctorFeedDescription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "pet_doctor_idx")
    private Integer petDoctorIdx;

    @Column(name = "ct_idx")
    private Integer ctIdx;

    @Column(name = "ct_sub_idx")
    private Integer ctSubIdx;

    @Column(name = "product_idx")
    private Integer productIdx;

    @Column(name = "pdfd_title")
    private String pdfdTitle;

    @Column(name = "pdfd_desc")
    private String pdfdDesc;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "pc_id")
    private Integer pcId;

    @Column(name = "base_score")
    private Integer baseScore;

    @Column(name = "position_1")
    private String position1;

    @Column(name = "position_2")
    private String position2;

    @Column(name = "position_3")
    private String position3;

    @ArdCodeMakerAnnotation
    @Column(name = "ard_group_122")
    private Integer ardGroup122;

    @ArdCodeMakerAnnotation
    @Column(name = "ard_group_421")
    private Integer ardGroup421;

    @ArdCodeMakerAnnotation
    @Column(name = "ard_group_501")
    private Integer ardGroup501;

    @ArdCodeMakerAnnotation
    @Column(name = "ard_group_201")
    private Integer ardGroup201;

    @ArdCodeMakerAnnotation
    @Column(name = "ard_group_301")
    private Integer ardGroup301;

    @ArdCodeMakerAnnotation
    @Column(name = "ard_group_961")
    private Integer ardGroup961;

    @ArdCodeMakerAnnotation
    @Column(name = "ard_group_241")
    private Integer ardGroup241;

    @ArdCodeMakerAnnotation
    @Column(name = "ard_group_121")
    private Integer ardGroup121;

    @ArdCodeMakerAnnotation
    @Column(name = "ard_group_941")
    private Integer ardGroup941;

    @ArdCodeMakerAnnotation
    @Column(name = "ard_group_125")
    private Integer ardGroup125;

    @ArdCodeMakerAnnotation
    @Column(name = "ard_group_942")
    private Integer ardGroup942;

    @ArdCodeMakerAnnotation
    @Column(name = "ard_group_126")
    private Integer ardGroup126;

    @ArdCodeMakerAnnotation
    @Column(name = "ard_group_601")
    private Integer ardGroup601;

    @ArdCodeMakerAnnotation
    @Column(name = "ard_group_422")
    private Integer ardGroup422;

    @ArdCodeMakerAnnotation
    @Column(name = "ard_group_423")
    private Integer ardGroup423;

    @ArdCodeMakerAnnotation
    @Column(name = "ard_group_502")
    private Integer ardGroup502;

    @ArdCodeMakerAnnotation
    @Column(name = "ard_group_145")
    private Integer ardGroup145;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    @Builder
    public PetDoctorFeedDescription(Integer idx, Integer petDoctorIdx, Integer ctIdx,
        Integer ctSubIdx, Integer productIdx, String pdfdTitle, String pdfdDesc,
        String productName, Integer pcId, Integer baseScore, String position1,
        String position2, String position3, Integer ardGroup122, Integer ardGroup421,
        Integer ardGroup501, Integer ardGroup201, Integer ardGroup301, Integer ardGroup961,
        Integer ardGroup241, Integer ardGroup121, Integer ardGroup941, Integer ardGroup125,
        Integer ardGroup942, Integer ardGroup126, Integer ardGroup601, Integer ardGroup422,
        Integer ardGroup423, Integer ardGroup502, Integer ardGroup145) {
        this.idx = idx;
        this.petDoctorIdx = petDoctorIdx;
        this.ctIdx = ctIdx;
        this.ctSubIdx = ctSubIdx;
        this.productIdx = productIdx;
        this.pdfdTitle = pdfdTitle;
        this.pdfdDesc = pdfdDesc;
        this.productName = productName;
        this.pcId = pcId;
        this.baseScore = baseScore;
        this.position1 = position1;
        this.position2 = position2;
        this.position3 = position3;
        this.ardGroup122 = ardGroup122;
        this.ardGroup421 = ardGroup421;
        this.ardGroup501 = ardGroup501;
        this.ardGroup201 = ardGroup201;
        this.ardGroup301 = ardGroup301;
        this.ardGroup961 = ardGroup961;
        this.ardGroup241 = ardGroup241;
        this.ardGroup121 = ardGroup121;
        this.ardGroup941 = ardGroup941;
        this.ardGroup125 = ardGroup125;
        this.ardGroup942 = ardGroup942;
        this.ardGroup126 = ardGroup126;
        this.ardGroup601 = ardGroup601;
        this.ardGroup422 = ardGroup422;
        this.ardGroup423 = ardGroup423;
        this.ardGroup502 = ardGroup502;
        this.ardGroup145 = ardGroup145;
    }


}
