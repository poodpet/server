package com.pood.server.entity.meta.dto.user_pet_worry_manaer;

import java.util.List;

public class EyeHealthWorryDog implements UserPetWorryManager {

    private static final String DAILY_LIFE = "눈물 자국은 음식 알러지의 가능성이 높아, 알러지가 있는 단백질원을 피하는 식단 관리가 필요해요. 하루 한 번 이상 물을 묻힌 부드러운 휴지로 눈꼽을 녹여 자주 닦아주는 것이 좋아요. 눈이 잘 보이지 않는 것 같다면 집 가구의 날카로운 모서리를 스펀지로 감싸주세요. 눈이 나이가 들며 탁해졌다면 백내장 혹은 핵경화증을 의심해볼 수 있어.요 핸드폰 후레쉬로 비추어 보았을 때, 안에 ‘Y’자 형태의 선이 보인다면 백내장을 의심해 볼 수 있어요. 백내장은 약, 수술 등의 병원의 처치가 필요한 질환이고, 핵경화증은 질병이라기보다는 노령성 변화에 가까우므로, 크게 걱정하지 않으셔도 돼요. 자세한 검진은 병원을 방문해주세요.";
    private static final String DIET_1 = "눈이 약한 ";
    private static final String DIET_2 = "에게는 오메가3함량이 높고, 눈건강에 도움이 되는 기능성 영양소인 베타카로틴, 루테인, 제아잔틴이 좋아요. 눈의 황반을 구성하는 루테인과 제아잔틴은 백내장 발생위험을 낮추어 줄 거예요. 눈을 산화적 스트레스로부터 보호해주는 항산화 물질이 풍부한 식단이 도움을 줄 거예요.";
    private static final List<String> SOLUTION = List.of("눈물자국은 식단 관리, 자주 닦아주기",
        "베타카로틴, 루테인, 제아잔틴 함유된 식단 챙기기",
        "오메가3가 풍부한 식단 챙기기",
        "항산화제가 풍부한 식단 챙기기");

    @Override
    public String getDailyLife() {
        return DAILY_LIFE;
    }

    @Override
    public String getDiet(final String petName) {
        return new StringBuilder(DIET_1).append(petName).append(DIET_2).toString();
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }

}
