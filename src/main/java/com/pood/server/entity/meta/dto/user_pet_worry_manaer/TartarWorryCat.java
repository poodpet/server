package com.pood.server.entity.meta.dto.user_pet_worry_manaer;

import java.util.List;

public class TartarWorryCat implements UserPetWorryManager {

    private static final String DAILY_LIFE = "많은 치석은 염증으로 심장염, 신장질환을 유발할 수 있어요. 또한 심한 염증으로 농이 차오르면 눈주변이 부풀어 오르고, 다량의 눈꼽과 같은 형태로 보일 수 있답니다. 치아 건강에는 칫솔질이 가장 도움이 되며, 잇몸과 치아 사이의 연결 부분에 집중하여 닦아주는 것이 좋아요. 매일 한 번씩을 목표로 하는 것이 좋으며, 적어도 일주일 3번 이상은 칫솔질을 해야 해요. 익숙하지 않더라도 손가락을 입 주위에 갖다 대는 것부터 천천히 연습하는 것을 추천하며, 치킨 맛 치약을 사용하는 것도 좋은 방법이에요.";
    private static final String DIET_1 = "칫솔질이 어렵다면, ";
    private static final String DIET_2 = "에게는 건사료를 추천해요. 특히 알갱이가 큰 건사료가 물리적으로 치아를 긁어주는 효과를 주어, 치석이 쌓이는 것을 막아줄 거예요. 치아 건강에 도움이 되는 폴리페놀인 녹차잎추출물이 좋으며, 치석이 쌓이는 것을 막아주는 아연과 폴리인산염, 피로인산염이 함유된 식단이 도움이 돼요.";
    private static final List<String> SOLUTION = List.of("일주일 3번 이상 칫솔질하기",
        "알갱이가 큰 건사료로 바꿔보기",
        "녹차잎추출물 함유된 식단 챙기기",
        "아연이 풍부한 식단 챙기기");


    @Override
    public String getDailyLife() {
        return DAILY_LIFE;
    }

    @Override
    public String getDiet(final String petName) {
        return new StringBuilder(DIET_1).append(petName).append(DIET_2).toString();
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }

}
