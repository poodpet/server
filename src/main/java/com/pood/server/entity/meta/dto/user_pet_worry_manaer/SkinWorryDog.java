package com.pood.server.entity.meta.dto.user_pet_worry_manaer;

import java.util.List;

public class SkinWorryDog implements UserPetWorryManager {

    private static final String DAILY_LIFE = "잦은 목욕은 피부에 더 자극을 주고 건조하게 만들 수 있어요. 2주일에 한 번 이하의 목욕을 추천해요. 피부 알러지는 동물병원에서 정밀한 검진을 받아보아야 그 원인을 찾을 수 있어요. 보습제는 세라마이드 성분이 함유된 제품이 도움이 될 거예요.";
    private static final String DIET_1 = "피부/피모가 약한 ";
    private static final String DIET_2 = "에게는 고단백 식단을 급여해보세요. 오메가3함량이 높고 오메가6와 오메가3 필수 지방산의 비율이 적당한 식단이 피부를 튼튼하게 만들어줄 거예요. 또한 필수 아미노산을 고루 갖추고, 피부층 형성에 도움이 되는 철, 아연, 구리 등 필수 미네랄과 비타민이 풍부한 식단을 챙겨주세요.";
    private static final List<String> SOLUTION = List.of("세라마이드 성분이 함유된 보습제 추천",
        "고단백 식단 챙기기",
        "오메가3 & 6 비율이 1:2~5 식단 선택하기",
        "오메가3가 풍부한 식단 챙기기",
        "철, 아연, 구리가 풍부한 식단 챙기기");


    @Override
    public String getDailyLife() {
        return DAILY_LIFE;
    }

    @Override
    public String getDiet(final String petName) {
        return new StringBuilder(DIET_1).append(petName).append(DIET_2).toString();
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }

}
