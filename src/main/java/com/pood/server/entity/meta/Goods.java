package com.pood.server.entity.meta;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "goods")
public class Goods  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "펫 카테고리 항목 번호 : meta_db.pet_category.idx")
    @Column(name = "pc_idx")
    private Integer pcIdx;

    @ApiModelProperty(value = "셀러 항목 번호 : order_db.seller.idx")
    @Column(name = "seller_idx")
    private Integer sellerIdx;

    @ApiModelProperty(value = "굿즈 타입 항목 번호 : meta_db.goods_type.idx")
    @Column(name = "goods_type_idx")
    private Integer goodsTypeIdx;

    @ApiModelProperty(value = "쿠폰 적용가능여부 - 0:적용가능, 1:적용불가(중복쿠폰, 바로구매만 가능) 주문하다 취소시 장바구니에 넣지 않음")
    @Column(name = "coupon_apply")
    private Integer couponApply;

    @ApiModelProperty(value = "굿즈 이름")
    @Column(name = "goods_name")
    private String goodsName;

    @ApiModelProperty(value = "굿즈 설명")
    @Column(name = "goods_descv")
    private String goodsDescv;

    @ApiModelProperty(value = "굿즈 원래 가격")
    @Column(name = "goods_origin_price")
    private Integer goodsOriginPrice;

    @ApiModelProperty(value = "굿즈 가격")
    @Column(name = "goods_price")
    private Integer goodsPrice;

    @ApiModelProperty(value = "할인율")
    @Column(name = "discount_rate")
    private Integer discountRate;

    @ApiModelProperty(value = "할인 금액")
    @Column(name = "discount_price")
    private Integer discountPrice;

    @ApiModelProperty(value = "표출 타입")
    @Column(name = "display_type")
    private String displayType;

    @ApiModelProperty(value = "0:환불가능상품, 1:환불불가")
    @Column(name = "refund_type")
    private Integer refundType;

    @ApiModelProperty(value = "limit_quantity가 2일 때 구매 제한 수량")
    @Column(name = "quantity")
    private Integer quantity;

    @ApiModelProperty(value = "0:일반, 1:한번만 구매")
    @Column(name = "purchase_type")
    private Integer purchaseType;

    @ApiModelProperty(value = "0:판매대기, 1:판매중, 2:판매중지(노출안됨), 3:판매일시중지(리스트나 상세화면에서 확인 가능), 4:미취급 상품")
    @Column(name = "sale_status")
    private Integer saleStatus;

    @ApiModelProperty(value = "특정 프로모션의 경우 해당되는 판매 시작 시간")
    @Column(name = "startdate")
    private LocalDateTime startdate;

    @ApiModelProperty(value = "특정 프로모션의 경우 해당되는 판매 종료 시간")
    @Column(name = "end_date")
    private LocalDateTime endDate;

    @ApiModelProperty(value = "0:기본, 1:특별상품딜(목록에 노출되지 않음)")
    @Column(name = "visible")
    private Integer visible;

    @ApiModelProperty(value = "0:프로모션 진행 안함, 1:프로모션 진행함")
    @Column(name = "isPromotion")
    private Integer isPromotion;

    @ApiModelProperty(value = "프로모션 굿즈 가격")
    @Column(name = "pr_price")
    private Integer prPrice;

    @ApiModelProperty(value = "프로모션 할인 금액")
    @Column(name = "pr_discount_price")
    private Integer prDiscountPrice;

    @ApiModelProperty(value = "프로모션 할인율")
    @Column(name = "pr_discount_rate")
    private Integer prDiscountRate;

    @ApiModelProperty(value = "0:제한 없음, 1:1개, 2: ~이상 구매 제한")
    @Column(name = "limit_quantity")
    private Integer limitQuantity;

    @ApiModelProperty(value = "대표 상품 항목 번호 : meta_db.product.idx")
    @Column(name = "main_product")
    private Integer mainProduct;

    @Column(name = "average_rating")
    private Double averageRating;

    @Column(name = "review_cnt")
    private Integer reviewCnt;

    @ApiModelProperty(value = "솔루션 추천여부")
    @Column(name = "is_recommend", columnDefinition="tinyint(1) default 0")
    private boolean isRecommend;


    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    public boolean isOnlyOneBoughtChance() {
        return purchaseType.equals(1) && limitQuantity >= 1;
    }
}
