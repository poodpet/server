package com.pood.server.entity.meta;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "promotion_group")
public class PromotionGroup  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "프로모션 항목 번호 :  meta_db.promotion.idx")
    @Column(name = "pr_idx")
    private Integer prIdx;

    @ApiModelProperty(value = "프로모션 그룹 이름")
    @Column(name = "name")
    private String name;

    @ApiModelProperty(value = "그룹당 보여지는 줄 개수")
    @Column(name = "item_cnt")
    private Integer itemCnt;

    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
