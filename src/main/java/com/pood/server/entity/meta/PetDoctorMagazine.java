package com.pood.server.entity.meta;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "pet_doctor_magazine")
public class PetDoctorMagazine {

    @Id
    @ApiModelProperty(value = "idx")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idx;

    @ApiModelProperty(value = "매거진 카테고리")
    @ManyToOne
    @JoinColumn(name = "magazine_category_idx")
    private PetDoctorMagazineCategory petDoctorMagazineCategory;

    @ApiModelProperty(value = "제목")
    private String title;

    @ApiModelProperty(value = "내용")
    private String contents;

    @ApiModelProperty(value = "수정 일자")
    @Column(name = "update_time")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "등록 일자")
    @Column(name = "record_birth")
    private LocalDateTime recordBirth;

    @ApiModelProperty(value = "펫 카테고리")
    @ManyToOne
    @JoinColumn(name = "pc_idx", nullable = false)
    private PetCategory petCategory;

    @ApiModelProperty(value = "활성화 여부")
    @Column(name = "is_visible")
    private Boolean isVisible;

    @ApiModelProperty(value = "삭제 여부")
    @Column(name = "is_deleted")
    private Boolean isDeleted;

}
