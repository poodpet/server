package com.pood.server.entity.meta.mapper.notice;

import com.querydsl.core.annotations.QueryProjection;
import java.time.LocalDate;
import java.time.LocalDateTime;
import lombok.Value;

@Value
public class NoticeDetailMapper {

    Integer idx;

    String title;

    String text;

    LocalDate date;

    @QueryProjection
    public NoticeDetailMapper(Integer idx, String title, String text, LocalDateTime date) {
        this.idx = idx;
        this.title = title;
        this.text = text;
        this.date = date.toLocalDate();
    }

}
