package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "coupon_goods")
public class CouponGoods  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "coupon_idx")
    private Integer couponIdx;

    @Column(name = "goods_idx")
    private Integer goodsIdx;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

}
