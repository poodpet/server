package com.pood.server.entity.meta.dto.user_pet_worry_manaer;

import java.util.List;

public class DiarrheaWorryDog implements UserPetWorryManager {

    private static final String DAILY_LIFE = "하루 소변, 대변을 2번 정도 누는 것이 정상이에요. 산책 중 두 번째 누는 변은 준비가 안 된 채 나오는 변이라 약간 무를 수 있으니 놀라지 마세요. 강아지가 다른 증상 없이 묽은 변을 한 번 정도 본 것은 괜찮아요. 액체처럼 묽은 변을 이틀 이상 지속할 경우 병원에서 검사를 받아보세요. 어린 강아지라면 디스템퍼, 파보, 코로나와 같은 바이러스성 감염으로 무른 변을 보는 경우가 많으며, 나이든 강아지라도 살모넬라 등의 감염성 원인이 있을 수 있고, 식이 알러지의 가능성도 있어요. 또한 식단을 갑작스럽게 바꾸었을 때에도 설사를 할 수 있으니, 기존 식단에서 천천히 일주일간 시간을 두고 변경해주세요.";
    private static final String DIET_1 = "장이 약한 ";
    private static final String DIET_2 = "에게는 식이섬유(차전자피, 이눌린)가 풍부한 식단이 좋아요. 장의 유산균을 늘려주고, 변의 모양을 올바르게 잡아줄 거예요. 유산균 함량이 높은 사료와 영양제도 변의 모양을 잡는데 도움을 줘요.";
    private static final List<String> SOLUTION = List.of("충분한 수분 공급",
        "갑작스러운 식단 변경 금지",
        "식이섬유가 풍부한 식단 챙기기",
        "유산균이 함유된 식단 챙기기",
        "식이 알러지 가능성 챙기기");

    @Override
    public String getDailyLife() {
        return DAILY_LIFE;
    }

    @Override
    public String getDiet(final String petName) {
        return new StringBuilder(DIET_1).append(petName).append(DIET_2).toString();
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }

}
