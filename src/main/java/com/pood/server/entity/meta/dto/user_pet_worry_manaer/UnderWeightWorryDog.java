package com.pood.server.entity.meta.dto.user_pet_worry_manaer;

import java.util.List;

public class UnderWeightWorryDog implements UserPetWorryManager {

    private static final String DAILY_LIFE = "잘 먹어도 살이 찌지 않는다면, 호르몬성 질환, 감염과 같은 전신질환이 있을 가능성이 있어요. 병원에서 정밀 검진을 받아보는 것을 받아보세요.";
    private static final String DIET = "사료는 칼로리가 높은 사료를 추천해요.";
    private static final List<String> SOLUTION = List.of("열량이 높은 식단 챙기기");

    @Override
    public String getDailyLife() {
        return DAILY_LIFE;
    }

    @Override
    public String getDiet(final String petName) {
        return DIET;
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }

}
