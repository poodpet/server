package com.pood.server.entity.meta.dto;

import java.util.List;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;

@Value
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductAndWorryFilterDto {

    List<Long> goodsSubCtIdxList;
    Long goodsCtIdx;
    Long worryIdx;

    public static ProductAndWorryFilterDto of(final List<Long> goodsSubCtIdxList,
        final Long goodsCtIdx, final Long worryIdx) {
        return new ProductAndWorryFilterDto(goodsSubCtIdxList, goodsCtIdx, worryIdx);
    }

    public static ProductAndWorryFilterDto empty() {
        return new ProductAndWorryFilterDto(null, null, null);
    }
}
