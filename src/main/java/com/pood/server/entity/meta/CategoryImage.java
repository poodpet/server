package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "category_image")
public class CategoryImage  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "ct_idx")
    private Integer ctIdx;

    @Column(name = "name")
    private String name;

    @Column(name = "url")
    private String url;

    @Column(name = "visible")
    private Integer visible;

    @Column(name = "type")
    private Integer type;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
