package com.pood.server.entity.meta;

import io.swagger.annotations.ApiModelProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "goods_filter_ct")
public class GoodsFilterCt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Long idx;

    @ManyToOne(fetch = FetchType.LAZY)
    @ApiModelProperty(value = "브랜드 항목 번호 : meta_db.goods_ctx.idx")
    @JoinColumn(name = "goods_ct_idx")
    private GoodsCt goodsCt;

    @ManyToOne
    @ApiModelProperty(value = "브랜드 항목 번호 : meta_db.pet_ct.idx")
    @JoinColumn(name = "pet_ct_idx")
    private PetCategory petCt;

    @Column(name = "url")
    private String url;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private String type;

    @Column(name = "field_key")
    private String fieldKey;

    @Column(name = "field_value")
    private String fieldValue;

    @Column(name = "priority")
    private Integer priority;

}
