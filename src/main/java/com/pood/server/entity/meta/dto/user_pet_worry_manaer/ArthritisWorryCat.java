package com.pood.server.entity.meta.dto.user_pet_worry_manaer;

import java.util.List;

public class ArthritisWorryCat implements UserPetWorryManager {

    private static final String DAILY_LIFE = "너무 높은 곳에서 점프하지 않도록 환경을 만들어주세요. 다이어트가 필요한 경우라면, 지방이 낮고 열량이 낮은 다이어트 사료 혹은 수분이 많은 습식사료를 추천해요.";
    private static final String DIET_1 = "관절질환에 취약한 ";
    private static final String DIET_2 = "에게는 적당한 칼슘과 인 함량을 유지하여 뼈가 튼튼하게 유지되도록 해주는 것이 중요하답니다. 충분한 단백질 섭취를 통하여 근육 손실이 없도록 해주어야 하며, 염증을 줄여주는 높은 함량의 오메가3 식단과 뼈를 튼튼하게 해주는 기능성 영양소인 글루코사민, 콘드로이틴, MSM이 함유된 식단이 도움이 될 거예요.";
    private static final List<String> SOLUTION = List.of("고단백 식단 챙기기",
        "오메가3가 풍부한 식단 챙기기",
        "글루코사민, 콘드로이틴, MSM 챙기기");

    @Override
    public String getDailyLife() {
        return DAILY_LIFE;
    }

    @Override
    public String getDiet(final String petName) {
        return new StringBuilder(DIET_1).append(petName).append(DIET_2).toString();
    }

    @Override
    public List<String> getSolution() {
        return SOLUTION;
    }

}
