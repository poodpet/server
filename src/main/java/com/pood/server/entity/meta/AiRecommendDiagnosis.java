package com.pood.server.entity.meta;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "ai_recommend_diagnosis")
public class AiRecommendDiagnosis  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "ard_name")
    private String ardName;

    @Column(name = "ard_group")
    private String ardGroup;

    @Column(name = "ard_group_code")
    private Integer ardGroupCode;

    @Column(name = "updatetime")
    private String updatetime;

    @Column(name = "recordbirth")
    private String recordbirth;

}
