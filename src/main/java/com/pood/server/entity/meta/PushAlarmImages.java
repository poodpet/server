package com.pood.server.entity.meta;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EntityListeners(AuditingEntityListener.class)
@Table(name = "push_alarm_images")
public class PushAlarmImages implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Long idx;

    @ManyToOne
    @JoinColumn(name = "push_alarm_idx")
    private PushAlarm pushAlarm;

    @Column(name = "url")
    private String url;

    @CreatedDate
    @Column(name = "recordbirth", updatable = false)
    private LocalDateTime recordbirth;

    public void setUrl(String url) {
        this.url = url;
    }

    public PushAlarmImages(String url) {
        this.url = url;
    }

    public void setPushAlarm(PushAlarm pushAlarm) {
        this.pushAlarm = pushAlarm;
    }
}
