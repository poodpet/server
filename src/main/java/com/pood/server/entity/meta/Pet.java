package com.pood.server.entity.meta;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "pet")
public class Pet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "pc_kind")
    private String pcKind;

    @Column(name = "pc_kind_en")
    private String pcKindEn;

    @Column(name = "pc_tag")
    private String pcTag;

    @Column(name = "pc_size")
    private String pcSize;

    @Column(name = "pc_id")
    private Integer pcId;

    @Column(name = "pc_height_male")
    private String pcHeightMale;

    @Column(name = "pc_height_female")
    private String pcHeightFemale;

    @Column(name = "pc_weight_male")
    private String pcWeightMale;

    @Column(name = "pc_weight_female")
    private String pcWeightFemale;

    @Column(name = "pc_country")
    private String pcCountry;

    @Column(name = "pc_life_expectancy")
    private String pcLifeExpectancy;

    @Column(name = "pc_generic_weak")
    private String pcGenericWeak;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    public int getPcSizeCode() {
        if (isPcSmallSize()) {
            return 1;
        }
        if (isMediumPcSize()) {
            return 2;
        }
        return 3;
    }

    public boolean isPcSmallSize() {
        return pcSize.contains("소형");
    }

    public boolean isPcBigSize() {
        return pcSize.contains("대형");
    }

    public boolean isMediumPcSize() {
        return pcSize.contains("중형");
    }
}
