package com.pood.server.entity.meta.mapper.doctor;

import com.querydsl.core.annotations.QueryProjection;
import java.time.LocalDateTime;
import lombok.Value;

@Value
public class PetDoctorMagazineMapper {

    long idx;
    String ctImgUrl;
    String ctName;
    String title;
    LocalDateTime recordBirth;

    @QueryProjection
    public PetDoctorMagazineMapper(final long idx, final String ctImgUrl, final String ctName,
        final String title, final LocalDateTime recordBirth) {
        this.idx = idx;
        this.ctImgUrl = ctImgUrl;
        this.ctName = ctName;
        this.title = title;
        this.recordBirth = recordBirth;
    }
}
