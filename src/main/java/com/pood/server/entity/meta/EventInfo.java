package com.pood.server.entity.meta;

import static com.pood.server.exception.ErrorMessage.EVENT_HAVE_ENDED;
import static com.pood.server.exception.ErrorMessage.EVENT_HAVE_STARTED;

import com.pood.server.exception.ClosedEventException;
import com.pood.server.exception.ErrorMessage;
import com.pood.server.exception.PauseEventException;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "event_info")
public class EventInfo {

    private static final int EVENT_PAUSE = 3;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "쿠폰 항목 번호 : meta_db.coupon.idx")
    @Column(name = "coupon_idx")
    private Integer couponIdx;

    @ApiModelProperty(value = "이벤트 타입 항목 번호 : meta_db.event_type.idx")
    @Column(name = "event_type_idx")
    private Integer eventTypeIdx;

    @ApiModelProperty(value = "이벤트 제목")
    @Column(name = "title")
    private String title;

    @ApiModelProperty(value = "이벤트 설명")
    @Column(name = "intro")
    private String intro;

    @ApiModelProperty(value = "이벤트 어드민용 제목")
    @Column(name = "inner_title")
    private String innerTitle;

    @ApiModelProperty(value = "0:준비, 1:시작, 2:종료, 3: 일시정지")
    @Column(name = "status")
    private Integer status;

    @ApiModelProperty(value = "시작 시간")
    @Column(name = "startdate")
    private LocalDateTime startDate;

    @ApiModelProperty(value = "종료 시간")
    @Column(name = "end_date")
    private LocalDateTime endDate;

    @ApiModelProperty(value = "0:전체, 강아지, 고양이")
    @Column(name = "pc_id")
    private Integer pcId;

    @ApiModelProperty(value = "이동 스키마")
    @Column(name = "scheme_uri")
    private String schemeUri;

    @Column(name = "brand_idx")
    private Integer brandIdx;

    @ApiModelProperty(value = "배경 이미지 색상")
    @Column(name = "bg_color")
    private String bgColor;

    @ApiModelProperty(value = "이벤트 버튼 이름")
    @Column(name = "event_btn")
    private String eventBtn;

    @ApiModelProperty(value = "이벤트 우선 순위")
    @Column(name = "priority")
    private Integer priority;

    @ApiModelProperty(value = "이벤트 참가 제한")
    @Column(name = "attend_limit")
    private Integer attendLimit;

    @ApiModelProperty(value = "사진 이벤트 참가 제한")
    @Column(name = "photo_limit")
    private Integer photoLimit;

    @ApiModelProperty(value = "이벤트 상세 설명")
    @Column(name = "details", columnDefinition = "TEXT")
    private String details;

    @ApiModelProperty(value = "이벤트 삭제 여부")
    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @LastModifiedDate
    @ApiModelProperty(value = "항목 수정 날짜/시각")
    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @CreatedDate
    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth", updatable = false)
    private LocalDateTime recordbirth;

    public void checkValidation() {
        final LocalDateTime now = LocalDateTime.now();

        if (isWaiting(now)) {
            throw new ClosedEventException(EVENT_HAVE_STARTED.getMessage());
        }

        if (isEnded(now)) {
            throw new ClosedEventException(EVENT_HAVE_ENDED.getMessage());
        }

        if (isPaused()) {
            throw new PauseEventException(ErrorMessage.EVENT_PAUSE.getMessage());
        }
    }

    private boolean isWaiting(final LocalDateTime now) {
        return startDate.isAfter(now);
    }

    private boolean isEnded(final LocalDateTime now) {
        return endDate.isBefore(now);
    }

    private boolean isPaused() {
        return status.equals(EVENT_PAUSE);
    }

    public String convertStartDate() {
        return startDate.format(DateTimeFormatter.ofPattern("yyyy.MM.dd"));
    }

    public String convertEndDate() {
        return endDate.format(DateTimeFormatter.ofPattern("yyyy.MM.dd"));
    }

    public boolean isRunning(final LocalDateTime now) {
        return startDate.isBefore(now) && (endDate.isAfter(now));
    }
}
