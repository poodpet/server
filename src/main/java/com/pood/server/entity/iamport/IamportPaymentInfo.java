package com.pood.server.entity.iamport;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Setter;


@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class IamportPaymentInfo{
    private int code;
    private String message;

    @JsonProperty("response")
    private IamportItem iamportItem;

    public boolean isSuccess(){
        return code == 0;
    }

    public long getPaymentAmount(){
        return iamportItem.getAmount();
    }

    public boolean amountCheckFail(Integer orderPrice) {
        return iamportItem.amountCheckFail(orderPrice);
    }

    public long getBalanceAmount(){
        return iamportItem.getBalanceAmount();
    }

}


