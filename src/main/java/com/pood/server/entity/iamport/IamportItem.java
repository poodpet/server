package com.pood.server.entity.iamport;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Setter;

@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class IamportItem {

    private long amount;

    @JsonProperty("buyer_email")
    private String buyerEmail;

    @JsonProperty("pay_method")
    private String payMethod;

    @JsonProperty("buyer_name")
    private String buyerName;

    @JsonProperty("cancel_amount")
    private long cancelAmount;

    public long getAmount() {
        return amount;
    }

    public boolean amountCheckFail(int orderPrice) {
        return amount != orderPrice;
    }

    public long getBalanceAmount(){
        return amount - cancelAmount;
    }
}
