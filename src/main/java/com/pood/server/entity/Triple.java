package com.pood.server.entity;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Deprecated
public class Triple {

    List<String> first ;
    List<String> second ;
    List<String> third;
    public Triple(List<String> first, List<String> second, List<String> third) {
        this.first = first;
        this.second= second;
        this.third = third;
    }
}
