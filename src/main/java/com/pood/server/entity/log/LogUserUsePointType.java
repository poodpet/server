package com.pood.server.entity.log;

import com.pood.server.exception.NotFoundException;
import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LogUserUsePointType {
    USED(12),
    CANCELED(13),
    EXPIRED(21);
    
    private final int code;

    public static LogUserUsePointType ofLegacyStatus(Integer code) {
        return Arrays.stream(LogUserUsePointType.values())
            .filter(value -> value.getCode() == code)
            .findAny()
            .orElseThrow(() -> new NotFoundException(
                String.format("상태코드에 %s가 존재하지 않습니다.", code)));
    }
}
