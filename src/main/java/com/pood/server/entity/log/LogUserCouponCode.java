package com.pood.server.entity.log;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "log_user_coupon_code")
@EntityListeners(AuditingEntityListener.class)
public class LogUserCouponCode {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "code")
    private String code;

    @Column(name = "user_uuid")
    private String userUuid;

    @Column(name = "recordbirth", updatable = false)
    @CreatedDate
    private LocalDateTime recordbirth;

    @Builder
    public LogUserCouponCode(final String code, final String userUuid) {
        this.code = code;
        this.userUuid = userUuid;
    }
}
