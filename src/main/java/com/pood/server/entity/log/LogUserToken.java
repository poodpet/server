package com.pood.server.entity.log;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "log_user_token")
@EntityListeners(AuditingEntityListener.class)
public class LogUserToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "토큰")
    @Column(name = "token")
    private String token;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_info.user_uuid")
    @Column(name = "user_uuid")
    private String userUuid;

    @ApiModelProperty(value = "토큰 발급 날짜/시각")
    @Column(name = "recordbirth")
    @CreatedDate
    private LocalDateTime recordbirth;

    public LogUserToken(final String token, final String userUuid) {
        this.token = token;
        this.userUuid = userUuid;
    }
}
