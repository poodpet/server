package com.pood.server.entity.log;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;


//관리자 로그
@Entity
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "log_admin")
public class LogAdmin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "관리자 UUID")
    @Column(name = "admin_uuid")
    private String adminUuid;

    @ApiModelProperty(value = "카테고리 이름")
    @Column(name = "category")
    private String category;


    @Column(name = "record_idx")
    private Integer recordIdx;

    @ApiModelProperty(value = "0:등록 1:수정 2:삭제 3:(출고/입고API)출고 4:(출고/입고API)출고")
    @Column(name = "manage_type")
    private Integer manageType;

    @ApiModelProperty(value = "로그")
    @Column(name = "text")
    private String text;

    @ApiModelProperty(value = "로그 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
