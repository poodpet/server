package com.pood.server.entity.log;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "log_friend_point")
public class LogFriendPoint {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "point_uuid")
    private String pointUuid;

    @Column(name = "user_uuid")
    private String userUuid;

    @Column(name = "available_time")
    private LocalDateTime availableTime;

    @Column(name = "issue")
    private Integer issue;

    @Column(name = "saved_point")
    private Integer savedPoint;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
