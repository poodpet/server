package com.pood.server.entity.log;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "error_proc")
public class ErrorProc {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "path")
    private String path;

    @Column(name = "stack_trace")
    private String stackTrace;

    @Column(name = "error_msg")
    private String errorMsg;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
