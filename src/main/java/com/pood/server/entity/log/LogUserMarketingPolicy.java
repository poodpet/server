package com.pood.server.entity.log;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "log_user_marketing_policy")
@EntityListeners(AuditingEntityListener.class)
public class LogUserMarketingPolicy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_info.user_idx")
    @Column(name = "user_idx")
    private Integer userIdx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_info.user_uuid")
    @Column(name = "user_uuid")
    private String userUuid;

    @ApiModelProperty(value = "1:마케팅 푸시, 2:이벤트 프로모션 알림 동의, 3:개인정보처리")
    @Column(name = "mk_type")
    private Integer mkType;

    @ApiModelProperty(value = "0:미동의, 1:동의")
    @Column(name = "mk_agree")
    private Boolean mkAgree;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    @CreatedDate
    private LocalDateTime recordbirth;

    public LogUserMarketingPolicy(final Integer userIdx, final String userUuid, final Integer mkType, final Boolean mkAgree) {
        this.userIdx = userIdx;
        this.userUuid = userUuid;
        this.mkType = mkType;
        this.mkAgree = mkAgree;
    }

}
