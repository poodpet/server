package com.pood.server.entity.log;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "error_order_fail")
public class ErrorOrderFail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "order_number")
    private String orderNumber;

    @Column(name = "imp_uid")
    private String impUid;

    @Column(name = "text")
    private String text;

    @Column(name = "pay_type")
    private Integer payType;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
