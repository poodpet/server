package com.pood.server.entity.log;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@AllArgsConstructor
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "log_user_join")
@EntityListeners(AuditingEntityListener.class)
public class LogUserJoin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "user_idx")
    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_info.user_idx")
    private Integer userIdx;

    @Column(name = "user_uuid")
    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_info.user_uuid")
    private String userUuid;

    @Column(name = "login_type")
    @ApiModelProperty(value = "0:구글, 1:페이스북, 2:네이버, 3:카카오, 4:애플 아이디, 5:이메일")
    private Integer loginType;

    @Column(name = "sns_key")
    @ApiModelProperty(value = "SNS 로그인 업체로부터 넘어온 인증키 값")
    private String snsKey;

    @Column(name = "recordbirth")
    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @CreatedDate
    private LocalDateTime recordbirth;

    public LogUserJoin(final Integer userIdx, final String userUuid, final Integer loginType, final String snsKey) {
        this.userIdx = userIdx;
        this.userUuid = userUuid;
        this.loginType = loginType;
        this.snsKey = snsKey;
    }

}
