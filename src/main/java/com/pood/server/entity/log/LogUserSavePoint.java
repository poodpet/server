package com.pood.server.entity.log;

import com.pood.server.dto.user.UserDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@AllArgsConstructor
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "log_user_save_point")
public class LogUserSavePoint {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_info.user_uuid")
    @Column(name = "user_uuid")
    private String userUuid;

    @ApiModelProperty(value = "회원 항목 번호 : meta_db.`point`.idx")
    @Column(name = "point_idx")
    private Integer pointIdx;

    @ApiModelProperty(value = "포인트 이름")
    @Column(name = "point_name")
    private String pointName;

    @ApiModelProperty(value = "0: 적용 금액, 1 : 적용 퍼센트")
    @Column(name = "point_type")
    private Integer pointType;

    @ApiModelProperty(value = "point_type이 0인 경우 금액 수치")
    @Column(name = "point_price")
    private Integer pointPrice;

    @ApiModelProperty(value = "point_type이 1인 경우 구매 금액의 % 수치")
    @Column(name = "point_rate")
    private Integer pointRate;

    @Column(name = "type")
    private Integer type;

    @Column(name = "text")
    private String text;

    @Column(name = "save_point")
    private Integer savePoint;

    @Column(name = "expired_date")
    private LocalDateTime expiredDate;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    public UserDto.PointDto createPointDto() {
        String strRecordBirth = this.recordbirth.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String strExpiredDate = this.expiredDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        return new UserDto.PointDto(this.type,this.pointName, this.savePoint,strExpiredDate,strRecordBirth,"S");
    }
}
