package com.pood.server.entity.log;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class LogUserUsePointTypeConverter implements AttributeConverter<LogUserUsePointType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(final LogUserUsePointType attribute) {
        return attribute.getCode();
    }

    @Override
    public LogUserUsePointType convertToEntityAttribute(final Integer code) {
        return LogUserUsePointType.ofLegacyStatus(code);
    }
}
