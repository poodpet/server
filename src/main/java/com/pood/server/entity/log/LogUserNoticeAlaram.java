package com.pood.server.entity.log;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "log_user_notice_alaram")
public class LogUserNoticeAlaram {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_info.user_idx")
    @Column(name = "user_idx")
    private Integer userIdx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_info.user_uuid")
    @Column(name = "user_uuid")
    private String userUuid;

    @ApiModelProperty(value = "0:공지, 1:광고, 2:주문, 3:리뷰, 4:정기배송, 5:기타")
    @Column(name = "alaram_type")
    private Integer alaramType;

    @ApiModelProperty(value = "알림 타이틀")
    @Column(name = "title")
    private String title;

    @ApiModelProperty(value = "알림 메세지")
    @Column(name = "message")
    private String message;

    @ApiModelProperty(value = "알림 스키마")
    @Column(name = "schem")
    private String schem;

    @ApiModelProperty(value = "이미지 링크")
    @Column(name = "link_url")
    private String linkUrl;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
