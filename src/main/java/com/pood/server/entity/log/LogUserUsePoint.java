package com.pood.server.entity.log;

import com.pood.server.dto.user.UserDto;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "log_user_use_point")
public class LogUserUsePoint {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "user_uuid")
    private String userUuid;

    @Column(name = "type")
    @Convert(converter = LogUserUsePointTypeConverter.class)
    private LogUserUsePointType type;

    @Column(name = "text")
    private String text;

    @Column(name = "use_point")
    private Integer usePoint;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

    public static LogUserUsePoint ofRefundPoint(final String userUuid, final String text,
        final Integer point) {

        return new LogUserUsePoint(null, userUuid, LogUserUsePointType.CANCELED, text, (point * -1),
            LocalDateTime.now());
    }
    public UserDto.PointDto createPointDto() {
        String strRecordBirth = this.recordbirth.format(
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        return new UserDto.PointDto(this.type.getCode(), this.text, this.usePoint, "", strRecordBirth, "U");
    }
}
