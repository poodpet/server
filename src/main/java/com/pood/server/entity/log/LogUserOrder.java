package com.pood.server.entity.log;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "log_user_order")
public class LogUserOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "주문 항목 번호 : order_db.order.idx")
    @Column(name = "order_idx")
    private Integer orderIdx;

    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_info.user_uuid")
    @Column(name = "user_uuid")
    private String userUuid;

    @ApiModelProperty(value = "주문 항목 번호 : order_db.order.order_number")
    @Column(name = "order_number")
    private String orderNumber;

    @ApiModelProperty(value = "주문 타입 항목 번호 : order_db.order_type.idx")
    @Column(name = "order_status")
    private Integer orderStatus;

    @ApiModelProperty(value = "주문 단계별 텍스트")
    @Column(name = "order_text")
    private String orderText;

    @ApiModelProperty(value = "굿즈 항목 번호 : meta_db.goods.idx")
    @Column(name = "goods_idx")
    private Integer goodsIdx;

    @ApiModelProperty(value = "교환/반품/취소/수량")
    @Column(name = "qty")
    private Integer qty;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
