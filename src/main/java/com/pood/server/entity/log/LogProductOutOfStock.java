package com.pood.server.entity.log;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "log_product_out_of_stock")
public class LogProductOutOfStock {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "product_uuid")
    private String productUuid;

    @Column(name = "goods_idx")
    private Integer goodsIdx;

    @Column(name = "product_qty")
    private Integer productQty;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "mail_send")
    private Integer mailSend;

    @Column(name = "updatetime")
    private LocalDateTime updatetime;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
