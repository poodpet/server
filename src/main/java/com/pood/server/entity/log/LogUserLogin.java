package com.pood.server.entity.log;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "log_user_login")
@EntityListeners(AuditingEntityListener.class)
public class LogUserLogin {

    private static final int LOG_OUT = 1;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "user_idx")
    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_info.user_idx")
    private Integer userIdx;

    @Column(name = "user_uuid")
    @ApiModelProperty(value = "회원 항목 번호 : user_db.user_info.user_uuid")
    private String userUuid;

    @Column(name = "login_out")
    @ApiModelProperty(value = "0:로그인, 1: 로그 아웃")
    private Integer loginOut;

    @ApiModelProperty(value = "0:UNKNOWN, 1:ANDROID, 2:IOS, 3:모바일, 4:PC")
    @Column(name = "os_type")
    private Integer osType;

    @ApiModelProperty(value = "접속한 단말의 기기 정보")
    @Column(name = "device")
    private String device;

    @ApiModelProperty(value = "버전 정보")
    @Column(name = "version")
    private String version;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    @CreatedDate
    private LocalDateTime recordbirth;

    @Builder
    private LogUserLogin(final Integer userIdx, final String userUuid, final Integer loginOut, final Integer osType) {
        this.userIdx = userIdx;
        this.userUuid = userUuid;
        this.loginOut = loginOut;
        this.osType = osType;
    }

    public static LogUserLogin create(final Integer userIdx, final String userUuid,
        final Integer osType) {

        return LogUserLogin.builder()
            .userIdx(userIdx)
            .userUuid(userUuid)
            .osType(osType)
            .loginOut(LOG_OUT)
            .build();
    }

    public void updateLogoutStatus() {
        this.loginOut = 1;
    }

    public void updateDormant() {
        this.recordbirth = LocalDateTime.of(2021, 1, 1, 1, 1);
    }
}
