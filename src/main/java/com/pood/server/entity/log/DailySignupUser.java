package com.pood.server.entity.log;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "daily_signup_user")
public class DailySignupUser  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "total")
    private Integer total;

    @Column(name = "time_1")
    private LocalDateTime time1;

    @Column(name = "time_2")
    private LocalDateTime time2;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
