package com.pood.server.entity.log;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "error_api")
public class ErrorApi  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "error_msg")
    private String errorMsg;

    @Column(name = "status_code")
    private Integer statusCode;

    @Column(name = "header")
    private String header;

    @Column(name = "path")
    private String path;

    @Column(name = "recordbirth")
    private String recordbirth;

}
