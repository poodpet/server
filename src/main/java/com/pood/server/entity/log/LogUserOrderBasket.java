package com.pood.server.entity.log;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "log_user_order_basket")
public class LogUserOrderBasket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    private Integer idx;

    @Column(name = "user_uuid")
    private String userUuid;

    @Column(name = "order_number")
    private String orderNumber;

    @Column(name = "goods_idx")
    private Integer goodsIdx;

    @Column(name = "qty")
    private Integer qty;

    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
