package com.pood.server.entity.log;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "log_product_history")
public class LogProductHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "레코드 번호")
    @Column(name = "idx")
    private Integer idx;

    @ApiModelProperty(value = "상품 항목 번호 : meta_db.product.uuid")
    @Column(name = "product_uuid")
    private String productUuid;

    @ApiModelProperty(value = "상품의 현재 재고")
    @Column(name = "count")
    private Integer count;

    @ApiModelProperty(value = "상품의 가용 재고")
    @Column(name = "available_count")
    private Integer availableCount;

    @ApiModelProperty(value = "0: 출고, 1: 입고, 3:취소")
    @Column(name = "type")
    private Integer type;

    @ApiModelProperty(value = "변경된 수량")
    @Column(name = "quantity")
    private Integer quantity;

    @ApiModelProperty(value = "유통 기한")
    @Column(name = "expired_date")
    private LocalDateTime expiredDate;

    @ApiModelProperty(value = "재고 내역")
    @Column(name = "text")
    private String text;

    @ApiModelProperty(value = "항목 삽입 날짜/시각")
    @Column(name = "recordbirth")
    private LocalDateTime recordbirth;

}
