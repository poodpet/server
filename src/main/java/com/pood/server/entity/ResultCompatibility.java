package com.pood.server.entity;

import com.pood.server.entity.aiSolutions.AafcoNrcEntity;
import com.pood.server.entity.aiSolutions.AafcoNrcEntityCat;
import com.pood.server.entity.aiSolutions.AafcoNrcEntityDog;
import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.meta.Product;
import com.pood.server.entity.user.UserPet;
import com.pood.server.entity.user.UserPetAiDiagnosis;
import com.pood.server.entity.user.UserPetAllergy;
import com.pood.server.entity.user.UserPetGrainSize;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModelLegacy;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.util.Pair;

@Deprecated
public class ResultCompatibility {

    // Product 데이터 리스트를받아서 WorryCompatibility,  PetInfoCompatibility ,  NutritionCompatibility 3개의 클래스에서 구한 점수를 합하여 나누기 3
    // 높은 점수순으로 정렬
    // Pair<Integer, Double> => Integer : product_idx, Double : 맞춤분석 점수
    public ArrayList<Pair<Integer, Double>> calc(UserPet userPet, List<Product> productList, AafcoNrc aafcoNrc, List<UserPetAllergy> allergyDataList, List<UserPetAiDiagnosis> userPetAiDiagnosisList, Pet
        pet, List<UserPetGrainSize> userPetGrainSizes) {

        ArrayList<Pair<Integer, Double>> scoreDataList = new ArrayList<>();
        for (Product product : productList) {
            //Product가 맞춤솔루션에 적합한 상품인지 아닌지 체크
            boolean check = new BaseCompatibility().calc(userPet, product, allergyDataList);
            if (!check) continue;
            AafcoNrcEntity nrcEntity = userPet.getPcId() == 2 ? new AafcoNrcEntityCat(product, aafcoNrc, product.getFeed(), userPet)
                : new AafcoNrcEntityDog(product, aafcoNrc, product.getFeed(), userPet);
            List<NutritionDetailModelLegacy> appData = nrcEntity.getAppData();

            double totalScore = 0.0;
            double tempScore = 0.0;
            //아프코데이터 불러올것
            tempScore += new WorryCompatibility().calc(userPet, product, userPetAiDiagnosisList);
            tempScore += new PetProduct().calc(userPet, product, allergyDataList, pet, userPetGrainSizes);
//            float v = nrcEntity.calculateScore(appData);
//            tempScore += new NutritionCompatibility().calc( nrcEntity);
            tempScore += nrcEntity.calculateScore(appData);
            totalScore = Math.round(tempScore / 3);

            scoreDataList.add(Pair.of(product.getIdx(), totalScore));
        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            Collections.sort(scoreDataList, new ScoreSort().reversed());
//        }
        return scoreDataList;
    }

    public ArrayList<Pair<Integer, Double>> calc2(UserPet userPet, List<Product> productList, AafcoNrc aafcoNrc, List<UserPetAllergy> allergyDataList, List<UserPetAiDiagnosis> userPetAiDiagnosisList, Pet
        pet, List<UserPetGrainSize> userPetGrainSizes) {

        ArrayList<Pair<Integer, Double>> scoreDataList = new ArrayList<>();
        for (Product product : productList) {
            //Product가 맞춤솔루션에 적합한 상품인지 아닌지 체크
//            boolean check = new BaseCompatibility().calc(userPet, product, allergyDataList);
//            if (!check) continue;
            AafcoNrcEntity nrcEntity = userPet.getPcId() == 2 ? new AafcoNrcEntityCat(product, aafcoNrc, product.getFeed(), userPet)
                : new AafcoNrcEntityDog(product, aafcoNrc, product.getFeed(), userPet);
            List<NutritionDetailModelLegacy> appData = nrcEntity.getAppData();

            double totalScore = 0.0;
            double tempScore = 0.0;
            //아프코데이터 불러올것
            tempScore += new WorryCompatibility().calc(userPet, product, userPetAiDiagnosisList);
            tempScore += new PetProduct().calc(userPet, product, allergyDataList, pet, userPetGrainSizes);
//            float v = nrcEntity.calculateScore(appData);
//            tempScore += new NutritionCompatibility().calc( nrcEntity);
            tempScore += nrcEntity.calculateScore(appData);
            totalScore = Math.round(tempScore / 3);

            scoreDataList.add(Pair.of(product.getIdx(), totalScore));
        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            Collections.sort(scoreDataList, new ScoreSort().reversed());
//        }
        return scoreDataList;
    }
}

