package com.pood.server.entity;

import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import java.util.Comparator;

public class GoodsDiscountRateSort implements Comparator<SortedGoodsList> {

    @Override
    public int compare(SortedGoodsList t1, SortedGoodsList t2) {
        return t1.getPromotionInfo().getPrDiscountRate()
            .compareTo(t2.getPromotionInfo().getPrDiscountRate());
    }
}
