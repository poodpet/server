package com.pood.server.api.req.header.admin;

public class aes_enc {

    private String enc;

    public String getEnc() {
        return enc;
    }

    public void setEnc(String enc) {
        this.enc = enc;
    }

    @Override
    public String toString() {
        return "aes_enc [enc=" + enc + "]";
    }
}
