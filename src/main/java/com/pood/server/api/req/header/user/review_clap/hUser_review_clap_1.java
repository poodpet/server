package com.pood.server.api.req.header.user.review_clap;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_review_clap_1 {

    private String user_uuid;

    private Integer review_idx;

    @Override
    public String toString() {
        return "hUser_review_clap_1 [review_idx=" + review_idx + ", user_uuid=" + user_uuid + "]";
    }

}