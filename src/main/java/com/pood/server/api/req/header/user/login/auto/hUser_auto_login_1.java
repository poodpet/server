package com.pood.server.api.req.header.user.login.auto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_auto_login_1 {

    private Integer login_type;

    private String email;

    private String uuid;

    @Override
    public String toString() {
        return "hUser_login_1 [email=" + email + ", login_type=" + login_type + ", uuid=" + uuid + "]";
    }

}
