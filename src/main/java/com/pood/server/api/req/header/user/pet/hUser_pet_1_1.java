package com.pood.server.api.req.header.user.pet;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_pet_1_1 {

    private Integer idx;

    @Override
    public String toString() {
        return "hUser_pet_1_1 [idx=" + idx + "]";
    }

}
