package com.pood.server.api.req.header.user.coupon;

import java.util.Arrays;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_coupon_3 {

    private Integer[] idx;

    @Override
    public String toString() {
        return "hUser_coupon_3 [idx=" + Arrays.toString(idx) + "]";
    }

}