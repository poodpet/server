package com.pood.server.api.req.header.pay;

import java.util.List;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderCancelRequest {

    private String  merchant_uid;

    private String  request_text;

    private Boolean isRefund;

    private boolean fianlCancle;

    private List<hPay_cancel_1_2> data;

    @Override
    public String toString() {
        return "hPay_cancel_1_1 [data=" + data + ", merchant_uid=" + merchant_uid + ", request_text=" + request_text
                + "]";
    }

    public boolean isRefund() {
        if(Objects.isNull(isRefund)){
            return false;
        }
        return isRefund;
    }

    public boolean isFinalCancel() {
        return fianlCancle;
    }
}
