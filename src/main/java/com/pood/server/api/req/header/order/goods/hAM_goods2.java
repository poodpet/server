package com.pood.server.api.req.header.order.goods;

import java.util.Arrays;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hAM_goods2 {

    private Integer[] product;

    private Integer[] coupon;

    private Integer[] pr_code;

    private Integer pc_idx;

    private Integer seller_idx;

    private String goods_name;

    private String goods_descv;

    private Integer goods_price;

    private List<hAM_goods_image_1> image;

    private Integer discount_rate;

    private Integer discount_price;

    private Integer quantity;

    private Integer purchase_type;

    private Integer sale_status;

    private String start_date;

    private String end_date;

    private Integer visible;

    private Integer limit_quantity;

    @Override
    public String toString() {
        return "hAM_goods2 [coupon=" + Arrays.toString(coupon) + ", goods_descv=" + goods_descv + ", goods_name="
            + goods_name + ", goods_price=" + goods_price + ", discount_price=" + discount_price + ", discount_rate="
            + discount_rate + ", end_date=" + end_date + ", image=" + image + ", limit_quantity=" + limit_quantity
            + ", pc_idx=" + pc_idx + ", pr_code=" + Arrays.toString(pr_code) + ", product="
            + Arrays.toString(product) + ", purchase_type=" + purchase_type + ", quantity=" + quantity
            + ", sale_status=" + sale_status + ", seller_idx=" + seller_idx + ", start_date=" + start_date
            + ", visible=" + visible + "]";
    }


}
