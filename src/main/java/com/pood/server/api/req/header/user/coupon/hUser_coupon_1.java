package com.pood.server.api.req.header.user.coupon;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_coupon_1 {

    private String user_uuid;

    private Integer coupon_idx;

    @Override
    public String toString() {
        return "hUser_coupon_1 [coupon_idx=" + coupon_idx + ", user_uuid=" + user_uuid + "]";
    }
}   