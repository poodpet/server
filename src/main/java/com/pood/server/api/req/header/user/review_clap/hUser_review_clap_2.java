package com.pood.server.api.req.header.user.review_clap;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_review_clap_2 {

    private String user_uuid;

    @Override
    public String toString() {
        return "hUser_review_clap_2 [user_uuid=" + user_uuid + "]";
    }

}
