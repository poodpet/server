package com.pood.server.api.req.header.order.goods;

import java.util.ArrayList;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hAM_goods_2_2 {

    private ArrayList<Integer> goods_idx_list;

}
