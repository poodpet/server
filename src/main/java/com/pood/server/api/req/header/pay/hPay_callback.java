package com.pood.server.api.req.header.pay;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hPay_callback {

    private String imp_uid;

    private String merchant_uid;

    private String status;

    @Override
    public String toString() {
        return "hPay_callback [imp_uid=" + imp_uid + ", merchant_uid=" + merchant_uid + ", status=" + status + "]";
    }

}
