package com.pood.server.api.req.header.order.refund;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hOrder_refund_delivery {

    private String address;

    private String detail_address;

    private String receiver;

    private String nickname;

    private String  zipcode;

    private String  phone_number;

    @Override
    public String toString() {
        return "hOrder_refund_delivery [address=" + address + ", detail_address=" + detail_address + ", nickname="
            + nickname + ", phone_number=" + phone_number + ", receiver=" + receiver + ", zipcode=" + zipcode + "]";
    }

}
