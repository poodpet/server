package com.pood.server.api.req.header.user.basket;

import com.pood.server.object.user.vo_user_basket;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_basket_1 extends vo_user_basket {

    private Integer user_coupon_idx;

}