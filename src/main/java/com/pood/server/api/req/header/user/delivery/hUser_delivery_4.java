package com.pood.server.api.req.header.user.delivery;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_delivery_4 {

    private Integer idx;

    private Integer user_idx;

    private String address;

    private Boolean default_type;

    private String detail_address;

    private String name;

    private String nickname;

    private String zipcode;

    private Boolean input_type;

    private Integer remote_type;

    private String phone_number;

    @Override
    public String toString() {
        return "hUser_delivery_4 [address=" + address + ", default_type=" + default_type + ", detail_address="
            + detail_address + ", idx=" + idx + ", input_type=" + input_type + ", name=" + name + ", nickname="
            + nickname + ", phone_number=" + phone_number + ", remote_type=" + remote_type + ", user_idx="
            + user_idx + ", zipcode=" + zipcode + "]";
    }

}
