package com.pood.server.api.req.header.order.goods;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hAM_goods_image_1 {

    private Integer image_idx;

    private Integer image_visible;

    private Integer image_type;

    private Integer image_priority;

    @Override
    public String toString() {
        return "hAM_goods_image_1 [image_idx=" + image_idx + ", image_priority=" + image_priority + ", image_type="
            + image_type + ", image_visible=" + image_visible + "]";
    }
}