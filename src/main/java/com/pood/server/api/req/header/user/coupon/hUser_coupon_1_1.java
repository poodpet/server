package com.pood.server.api.req.header.user.coupon;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_coupon_1_1 {

    private String user_uuid;

    private String code;

    @Override
    public String toString() {
        return "hUser_coupon_1_1 [code=" + code + ", user_uuid=" + user_uuid + "]";
    }

}
