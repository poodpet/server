package com.pood.server.api.req.header.user.claim;

import java.util.Arrays;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_claim_3 {

    private Integer[] claim_idx;

    @Override
    public String toString() {
        return "hUser_claim_3 [claim_idx=" + Arrays.toString(claim_idx) + "]";
    }

}