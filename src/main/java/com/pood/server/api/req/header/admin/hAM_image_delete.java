package com.pood.server.api.req.header.admin;

import java.util.Arrays;

public class hAM_image_delete {
    private Integer[] image_idx;

    public Integer[] getImage_idx() {
        return image_idx;
    }

    public void setImage_idx(Integer[] image_idx) {
        this.image_idx = image_idx;
    }

    @Override
    public String toString() {
        return "hAM_image_delete [image_idx=" + Arrays.toString(image_idx) + "]";
    }

    

    
}