package com.pood.server.api.req.header.user.push;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_push_4 {

    private String user_uuid;

    private Boolean pood_push;

    private Boolean order_push;

    private Boolean service_push;

    private Boolean cat_pood_push;

    private Boolean dog_pood_push;

    @Override
    public String toString() {
        return "hUser_push_4 [order_push=" + order_push + ", pood_push=" + pood_push + ", service_push=" + service_push
            + ", user_uuid=" + user_uuid + "]";
    }

}
