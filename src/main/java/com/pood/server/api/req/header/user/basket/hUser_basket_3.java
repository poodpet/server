package com.pood.server.api.req.header.user.basket;

import java.util.Arrays;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_basket_3 {

    Integer[] idx;

    @Override
    public String toString() {
        return "hUser_basket_3 [idx=" + Arrays.toString(idx) + "]";
    }

}