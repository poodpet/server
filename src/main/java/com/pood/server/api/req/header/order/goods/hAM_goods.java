package com.pood.server.api.req.header.order.goods;

import java.util.Arrays;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hAM_goods {

    private Integer[] product_idx;

    private Integer[] coupon_idx;

    private Integer[] pr_code_idx;

    private Integer pc_idx;

    private Integer seller_idx;

    private String goods_name;

    private String goods_descv;

    private Integer goods_price;

    private List<hAM_goods_image_1> image;

    private Integer discount_rate;

    private Integer discount_price;

    private Integer quantity;

    private Integer purchase_type;

    private Integer sale_status;

    private String start_date;

    private String end_date;

    private Integer visible;

    private Integer limit_quantity;

    @Override
    public String toString() {
        return "hAM_goods [coupon_idx=" + Arrays.toString(coupon_idx) + ", goods_descv=" + goods_descv + ", goods_name="
            + goods_name + ", goods_price=" + goods_price + ", discount_price=" + discount_price + ", discount_rate="
            + discount_rate + ", end_date=" + end_date + ", image=" + image + ", limit_quantity=" + limit_quantity
            + ", pc_idx=" + pc_idx + ", pr_code_idx=" + Arrays.toString(pr_code_idx) + ", product_idx="
            + Arrays.toString(product_idx) + ", purchase_type=" + purchase_type + ", quantity=" + quantity
            + ", sale_status=" + sale_status + ", seller_idx=" + seller_idx + ", start_date=" + start_date
            + ", visible=" + visible + "]";
    }

}
