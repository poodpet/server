package com.pood.server.api.req.header.user.login.mail;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_mail_login_1 {

    private String email;

    private String password;

    private Integer login_type;

    @Override
    public String toString() {
        return "hUser_mail_login_1 [email=" + email + ", login_type=" + login_type + ", password=" + password + "]";
    }

}
