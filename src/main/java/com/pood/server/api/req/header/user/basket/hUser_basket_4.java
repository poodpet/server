package com.pood.server.api.req.header.user.basket;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_basket_4 {

    private Integer idx;

    private Integer qty;

    @Override
    public String toString() {
        return "hUser_basket_4 [idx=" + idx + ", qty=" + qty + "]";
    }

}
