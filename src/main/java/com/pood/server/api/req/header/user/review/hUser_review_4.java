package com.pood.server.api.req.header.user.review;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_review_4 {

    private Integer order_idx;

    private Integer review_idx;

    private Integer rating;

    private String review_text;

    private Integer update_count;

    private Boolean isDelete;

    private Integer isVisible;

    private List<hUser_review_image> image;

    @Override
    public String toString() {
        return "hUser_review_4 [isDelete=" + isDelete + ", isVisible=" + isVisible + ", rating=" + rating
            + ", review_idx=" + review_idx + ", review_text=" + review_text + ", update_count=" + update_count
            + "]";
    }

}
