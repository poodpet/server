package com.pood.server.api.req.header.admin.fcm;

public class hAM_fcm_1 {

    private Integer device_type;

    private String user_uuid;

    private String device_key;

    
    public Integer getDevice_type() {
        return device_type;
    }

    public void setDevice_type(Integer device_type) {
        this.device_type = device_type;
    }

    public String getDevice_key() {
        return device_key;
    }

    public void setDevice_key(String device_key) {
        this.device_key = device_key;
    }

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    @Override
    public String toString() {
        return "hAM_fcm_1 [device_key=" + device_key + ", device_type=" + device_type + ", user_uuid=" + user_uuid
                + "]";
    }

    
}
