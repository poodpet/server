package com.pood.server.api.req.header.order.exchange;

import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderExchangeGoods {

    private Integer goods_idx;

    private String text;

    private int qty;

    @NotNull(message = "귀책 사유는 필수 값입니다.")
    private Boolean attr;

    private List<OrderExchangeDeliveryTrack> delivery_track;

    @Override
    public String toString() {
        return "hOrder_exchange_goods [delivery_track=" + delivery_track + ", goods_idx="
            + goods_idx + ", qty=" + qty
            + ", text=" + text + "]";
    }
}
