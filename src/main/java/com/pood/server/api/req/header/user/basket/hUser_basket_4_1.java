package com.pood.server.api.req.header.user.basket;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_basket_4_1 {

    private Integer user_coupon_idx;

    private Integer to_basket_idx;

    @Override
    public String toString() {
        return "hUser_basket_4_1 [to_basket_idx=" + to_basket_idx + ", user_coupon_idx=" + user_coupon_idx + "]";
    }

}
