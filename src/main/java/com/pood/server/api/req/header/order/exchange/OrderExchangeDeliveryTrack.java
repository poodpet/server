package com.pood.server.api.req.header.order.exchange;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderExchangeDeliveryTrack {

    private String courier;

    private String tracking_id;

    private String start_date;

    private Boolean main_address;

    @Override
    public String toString() {
        return "hOrder_exchange_delivery_track [courier=" + courier + ", main_address=" + main_address + ", start_date="
            + start_date + ", tracking_id=" + tracking_id + "]";
    }

}
