package com.pood.server.api.req.header.user.review;

import java.util.Arrays;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_review_3 {

    private Integer[] idx;

    @Override
    public String toString() {
        return "hUser_review_3 [idx=" + Arrays.toString(idx) + "]";
    }

}