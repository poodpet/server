package com.pood.server.api.req.header.user.review;

import com.pood.server.object.user.vo_user_review;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_review_1 extends vo_user_review {

    private List<hUser_review> image;

    @Override
    public String toString() {
        return "hUser_review_1 [image=" + image + "]";
    }

}