package com.pood.server.api.req.header.user.pwd;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_pwd_1 {

    private String user_uuid;

    private String new_passwd;

    private String confirm_passwd;

    @Override
    public String toString() {
        return "hUser_pwd_1 [confirm_passwd=" + confirm_passwd + ", new_passwd=" + new_passwd + ", user_uuid="
            + user_uuid + "]";
    }

}
