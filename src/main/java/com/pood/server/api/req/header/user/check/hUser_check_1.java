package com.pood.server.api.req.header.user.check;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_check_1 {

    private String user_email;

    private String referral_code;

    @Override
    public String toString() {
        return "hUser_check_1 [referral_code=" + referral_code + ", user_email=" + user_email + "]";
    }

}
