package com.pood.server.api.req.header.user.pet;

import com.pood.server.object.user.vo_user_pet;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_pet_4 extends vo_user_pet {

    private Integer idx;

    private List<hUser_pet_image> image;

    private List<hUser_pet_1_1> ai_diagnosis;

    private List<hUser_pet_1_1> grain_size;

    private List<hUser_pet_1_1> allergy;

    private List<hUser_pet_1_1> sick_idx;

    private Integer other_feed_idx;

    @Override
    public String toString() {
        return "hUser_pet_4 [ai_diagnosis=" + ai_diagnosis + ", allergy=" + allergy + ", grain_size=" + grain_size
            + ", idx=" + idx + ", image=" + image + ", other_feed_idx=" + other_feed_idx + ", sick_idx=" + sick_idx
            + "]";
    }

}
