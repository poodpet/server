package com.pood.server.api.req.header.user.card;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_card_4 {

    private String customer_uid;

    private String billing_key;

    private String card_name;

    private String card_number;

    private String card_user;

    private Boolean main_card;

    @Override
    public String toString() {
        return "hUser_card_4 [billing_key=" + billing_key + ", card_name=" + card_name + ", card_number=" + card_number
            + ", card_user=" + card_user + ", customer_uid=" + customer_uid + ", main_card=" + main_card + "]";
    }

}
