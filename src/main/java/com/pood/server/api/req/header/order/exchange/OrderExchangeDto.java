package com.pood.server.api.req.header.order.exchange;

import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class OrderExchangeDto {

    private String order_number;

    private int delivery_type;

    private String text;

    @Valid
    private List<OrderExchangeGoods> goods;

    private OrderExchangeDelivery pickup_delivery;

    private OrderExchangeDelivery send_delivery;

    @Override
    public String toString() {
        return "hOrder_exchange_1 [delivery_type=" + delivery_type + ", goods=" + goods
            + ", order_number="
            + order_number + ", pickup_delivery=" + pickup_delivery + ", send_delivery="
            + send_delivery + ", text="
            + text + "]";
    }

}
