package com.pood.server.api.req.header.user.user_info;

import javax.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_info_1 {

    private Integer login_type;

    private String email;

    private Boolean email_noti;

    private String password;

    private String sns_key;

    private String referral_code;

    private Boolean service_agree;

    private Boolean private_policy;

    @NotEmpty(message = "핸드폰 번혼는 필수 값 입니다.")
    private String phone_number;

    @NotEmpty(message = "사용자 이름은 필수 값 입니다.")
    private String user_name;

    @Override
    public String toString() {
        return "hUser_info_1 [email=" + email + ", email_noti=" + email_noti + ", login_type=" + login_type
            + ", password=" + password + ", phone_number=" + phone_number + ", private_policy=" + private_policy
            + ", referral_code=" + referral_code + ", service_agree=" + service_agree + ", sns_key=" + sns_key
            + ", user_name=" + user_name + "]";
    }

}