package com.pood.server.api.req.header.order.goods;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hAM_goods_2_3 {

    private Integer pc_idx;

    private String keyword;

    @Override
    public String toString() {
        return "hAM_goods_2_3 [keyword=" + keyword + ", pc_idx=" + pc_idx + "]";
    }

}
