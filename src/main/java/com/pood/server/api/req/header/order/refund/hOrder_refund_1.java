package com.pood.server.api.req.header.order.refund;

import java.util.List;
import javax.validation.Valid;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class hOrder_refund_1 {

    private String order_number;

    private String text;

    private Integer send_type;

    private hOrder_refund_delivery delivery_track;

    @Valid
    private List<hOrder_refund_goods> goods;

    @Override
    public String toString() {
        return "hOrder_refund_1 [goods=" + goods + ", order_number=" + order_number + ", send_type="
            + send_type
            + ", text=" + text + "]";
    }

}
