package com.pood.server.api.req.header.user.pet;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_pet_image {

    private Integer image_idx;

    @Override
    public String toString() {
        return "hUser_pet_image [image_idx=" + image_idx + "]";
    }

}
