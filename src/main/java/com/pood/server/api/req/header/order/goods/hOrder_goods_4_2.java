package com.pood.server.api.req.header.order.goods;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hOrder_goods_4_2 {

    private Integer idx;

    private Integer pc_idx;

    private Integer seller_idx;

    private Integer goods_type_state;

    private String goods_type_name;

    private Integer refund_type;

    private String goods_name;

    private String goods_descv;

    private Integer goods_price;

    private Integer discount_rate;

    private Integer discount_price;

    private Integer quantity;

    private Integer purchase_type;

    private Integer sale_status;

    private String start_date;

    private String end_date;

    private Integer visible;

    private Integer limit_quantity;

    @Override
    public String toString() {
        return "hOrder_goods_4_2 [goods_descv=" + goods_descv + ", goods_name=" + goods_name + ", goods_price=" + goods_price
            + ", goods_type_name=" + goods_type_name + ", goods_type_state=" + goods_type_state + ", discount_price="
            + discount_price + ", discount_rate=" + discount_rate + ", end_date=" + end_date + ", idx=" + idx
            + ", limit_quantity=" + limit_quantity + ", pc_idx=" + pc_idx + ", purchase_type=" + purchase_type
            + ", quantity=" + quantity + ", refund_type=" + refund_type + ", sale_status=" + sale_status
            + ", seller_idx=" + seller_idx + ", start_date=" + start_date + ", visible=" + visible + "]";
    }

}
