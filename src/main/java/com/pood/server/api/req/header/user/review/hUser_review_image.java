package com.pood.server.api.req.header.user.review;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_review_image {

    private Integer image_idx;

    private Integer seq;

    private Integer order_idx;

    @Override
    public String toString() {
        return "hUser_review_image [image_idx=" + image_idx + ", order_idx=" + order_idx + ", seq=" + seq + "]";
    }

}
