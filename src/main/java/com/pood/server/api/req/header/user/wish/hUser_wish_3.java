package com.pood.server.api.req.header.user.wish;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_wish_3 {

    private String user_uuid;

    private Integer goods_idx;

    @Override
    public String toString() {
        return "hUser_wish_3 [goods_idx=" + goods_idx + ", user_uuid=" + user_uuid + "]";
    }

}