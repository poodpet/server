package com.pood.server.api.req.header.order.refund;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hOrder_refund_goods {

    private Integer goods_idx;

    private Integer qty;

    private String text;

    @NotNull(message = "귀책 사유는 필수 값입니다.")
    private Boolean attr;

    @Override
    public String toString() {
        return "hOrder_refund_goods [goods_idx=" + goods_idx + ", qty=" + qty + ", text=" + text
            + "]";
    }

}
