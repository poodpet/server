package com.pood.server.api.req.header.user.user_info;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_info_3 {

    private String user_uuid;

    @Override
    public String toString() {
        return "hUser_info_3 [user_uuid=" + user_uuid + "]";
    }

}