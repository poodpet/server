package com.pood.server.api.req.header.user.review;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hUser_review {

    Integer image_idx;

    @Override
    public String toString() {
        return "hUser_review [image_idx=" + image_idx + "]";
    }

}