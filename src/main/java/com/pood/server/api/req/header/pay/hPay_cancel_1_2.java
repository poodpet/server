package com.pood.server.api.req.header.pay;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class  hPay_cancel_1_2 {

    private Integer goods_idx;

    private Integer qty;

    private String refund_uuid;

    private Integer attr_type;

    @Override
    public String toString() {
        return "hPay_cancel_1_2 [attr_type=" + attr_type + ", goods_idx=" + goods_idx + ", qty=" + qty
            + ", refund_uuid=" + refund_uuid + "]";
    }

}
