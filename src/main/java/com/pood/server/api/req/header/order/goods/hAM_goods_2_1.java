package com.pood.server.api.req.header.order.goods;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class hAM_goods_2_1 {

    private Integer goods_idx;

    private Integer goods_type_idx;

    private String display_type;

    private Integer pc_idx;

    private Integer ct_idx;

    private String ct_sub_idx;

    private Integer brand_idx;

    private Integer page_size;

    private Integer page_number;

    private String recordbirth;

    private String keyword;

    private String main_property;

    private String unit_size;

    private Integer life_stage;

    private Integer feed_target;

    private String position;

    private String sort_type;

    @Override
    public String toString() {
        return "hAM_goods_2_1 [brand_idx=" + brand_idx + ", ct_idx=" + ct_idx + ", ct_sub_idx=" + ct_sub_idx
            + ", display_type=" + display_type + ", feed_target=" + feed_target + ", goods_idx=" + goods_idx
            + ", goods_type_idx=" + goods_type_idx + ", keyword=" + keyword + ", life_stage=" + life_stage
            + ", main_property=" + main_property + ", page_number=" + page_number + ", page_size=" + page_size
            + ", pc_idx=" + pc_idx + ", position=" + position + ", recordbirth=" + recordbirth + ", unit_size="
            + unit_size + "]";
    }

}
