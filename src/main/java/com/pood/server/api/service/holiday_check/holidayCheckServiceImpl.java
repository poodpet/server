package com.pood.server.api.service.holiday_check;

import com.pood.server.api.service.error.errorProcService;
import com.pood.server.api.service.time.timeService;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

@Service("holidayCheckService")
public class holidayCheckServiceImpl implements holidayCheckService {

    private static final String DEFAULT_URL = "http://apis.data.go.kr/B090041/openapi/service/SpcdeInfoService/getRestDeInfo?solYear=%d&numOfRows=100&ServiceKey=%s";
    private static final String API_KEY = "c/JQqxGIQgUj8/PNjlROaupkFcrFpePwxwnBFdNhAuTstDYnI/d9q7250hNJZfBF2kNwHptZHb9mwmVHR7sC1g==";
    private static final int NETWORK_TIME_OUT = 10000;

    @Autowired
    @Qualifier("errorProcService")
    errorProcService errorProcService;

    @Autowired
    @Qualifier("timeService")
    timeService timeService;

    public List<String> getHolidayData() throws Exception {

        List<String> holidayMap = new ArrayList<String>();

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        callAPI(year, holidayMap);
        if (month == 12) {
            year++;
            callAPI(year, holidayMap);
        }
 
        return holidayMap;
    }





    public void callAPI(int year, List<String> holidayMap) throws Exception{

        String urlString = String.format(DEFAULT_URL, year, API_KEY);
        StringBuilder buffer = new StringBuilder();

        
        try {


            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            if (conn != null) {
                conn.setConnectTimeout(NETWORK_TIME_OUT);


                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {


                    InputStreamReader isr = new InputStreamReader(conn.getInputStream());
                    BufferedReader br = new BufferedReader(isr);
                    buffer.append(br.readLine());
                    br.close();
                    conn.disconnect();
                    parseXML(buffer.toString(), holidayMap);


                    //이번달 주말 hashMap 저장
                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.DAY_OF_MONTH, 1);
                    addWeekendDate(calendar, holidayMap);



                    //다음달 주말 hashMap 저장
                    calendar.add(Calendar.DAY_OF_MONTH, 1);
                    addWeekendDate(calendar, holidayMap);


                    calendar = null;

                    isr = null;

                    br = null;

                }
            }
        } catch (Exception e) {
            errorProcService.insertRecord("HolidayCheck", e.getMessage(), e.getStackTrace()[0].toString());
        }
    }





    //공휴일 데이터 파싱해서 hashMap에 저장할 것
    public void parseXML(String xml, List<String> holidayMap) throws ParserConfigurationException, IOException, SAXException, ParseException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = factory.newDocumentBuilder();

        ByteArrayInputStream ip = new ByteArrayInputStream(xml.getBytes());

        Document doc = documentBuilder.parse(ip);

        Element element = doc.getDocumentElement();

        NodeList itemList = element.getElementsByTagName("item");

        if (itemList != null && itemList.getLength() > 0) {
            for (int i = 0; i < itemList.getLength(); i++) {
                Node n = itemList.item(i);
                if (n.getNodeType() == Node.ELEMENT_NODE) {
                    Element el = (Element) n;
                    String locdate = el.getElementsByTagName("locdate").item(0).getTextContent();
                    String date = timeService.getDateTime5(locdate);
                    holidayMap.add(date);

                    el = null;
                    locdate = null;
                    date = null;
                }
            }
        }

        factory = null;

        documentBuilder = null;

        ip = null;

        doc = null;

        element = null;

        itemList = null;
    }

    //주말 데이터 hashMap에 저장
    public void addWeekendDate(Calendar calendar, List<String> holidayMap) {
        int lastDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int i = 0; i < lastDay; i++) {
            calendar.set(Calendar.DAY_OF_MONTH, i + 1);
            int dayNum = calendar.get(Calendar.DAY_OF_WEEK);
            if (!checkHoliday(dayNum)) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String date = sdf.format(calendar.getTime());
                holidayMap.add(date);
            }
        }
    }

    public boolean checkHoliday(int dayNum) {
        return dayNum != 1 && dayNum != 7;
    }

}
