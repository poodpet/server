package com.pood.server.api.service.user;

import java.sql.SQLException;
import java.util.List;

public interface userPetFeedService {
    
    /******************* 회원 반려 동물 항목 번호에 해당하는 질병 번호 조회 ********************/
    public List<Integer> getList(Integer USER_PET_IDX) throws SQLException;


    /******************** 회원 동물 AI 질병 정보 등록 ******************************/
    public Integer insertRecord(Integer USER_PET_IDX, Integer AI_DIG_IDX) throws SQLException;


    public void deleteRecordWithUserPetIDX(Integer USER_PET_IDX) throws SQLException;

 
}
