package com.pood.server.api.service.sns;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.CreatePlatformEndpointRequest;
import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.pood.server.config.AWS;
import com.pood.server.config.meta.user.USER_NOTI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.pood.server.entity.DeviceType;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.SnsException;
import software.amazon.awssdk.services.sns.model.SubscribeRequest;
import software.amazon.awssdk.services.sns.model.SubscribeResponse;

public interface snsService {

    Logger logger = LoggerFactory.getLogger(snsService.class);

    /****************** 메세지 발송 *******************/
    public void send(String MESSAGE, String USER_UUID, Integer USER_IDX, Integer ALARM_TYPE, String MESSAGE_TITLE, String SCHEMA) throws Exception;
 
//
//    public void subscribe(String SUBSCRIPTION_TOKEN, String TOPIC_ARN);
//
//    public String createEndpoint(String TOKEN, String ARN);



    default public void confirmSub(SnsClient snsClient, String subscriptionToken, String topicArn ) {

        try {
            SubscribeRequest request = SubscribeRequest.builder()
                    .protocol("application")
                    .endpoint(subscriptionToken)
                    .returnSubscriptionArn(true)
                    .topicArn(topicArn)
                    .build();

            SubscribeResponse result = snsClient.subscribe(request);
            logger.info("[CONFIRM-SUBSCRIPTION]: " + result.subscriptionArn() + ", 상태 : " + result.sdkHttpResponse().statusCode());

        } catch (SnsException e) {
            logger.error(e.awsErrorDetails().errorMessage());
            System.exit(1);
        }
    }

    default public void subscribe(String SUBSCRIPTION_TOKEN, String TOPIC_ARN) {
        // 해당 디바이스 토큰에 대해서 SNS에 구독 설정
        SnsClient SNS_CLIENT = SnsClient.builder()
            .credentialsProvider(
                () -> AwsBasicCredentials.create(AWS.accessKey, AWS.secretKey)
            )
            .region(Region.AP_NORTHEAST_2)
            .build();

        confirmSub(SNS_CLIENT, SUBSCRIPTION_TOKEN, TOPIC_ARN ) ;

        SNS_CLIENT.close();

    }

    default public String createEndpoint(String TOKEN, String PLATFORM_ARN) {


        AWSCredentials credentials = new BasicAWSCredentials(AWS.accessKey, AWS.secretKey);

        AmazonSNS SNS_CLIENT = AmazonSNSClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.AP_NORTHEAST_2).build();

        String endpointArn = null;
        logger.info("[CREATING PLATFORM ENDPOINT WITH TOKEN]TOKEN =" + TOKEN + ", PLATFORM ARN =" + PLATFORM_ARN);

        CreatePlatformEndpointRequest cpeReq = new CreatePlatformEndpointRequest();
        cpeReq.setToken(TOKEN);
        cpeReq.setPlatformApplicationArn(PLATFORM_ARN);

        CreatePlatformEndpointResult cpeRes = SNS_CLIENT.createPlatformEndpoint(cpeReq);
        endpointArn = cpeRes.getEndpointArn();

        return endpointArn;
    }


    default public Boolean checkUserNotification(String USER_UUID, Integer ALARM_TYPE, Integer ORDER_PUSH, Integer SERVICE_PUSH, Integer POOD_PUSH){

        logger.info("USER UUID = " + USER_UUID + " : ORDER_PUSH = " + ORDER_PUSH + ", SERVICE_PUSH = " + SERVICE_PUSH + ", POOD_PUSH=" + POOD_PUSH);
        Boolean     chk = false;

        if(ALARM_TYPE == USER_NOTI.DELIVERY_START){

            logger.info("ALARAM TYPE = DELIVERY_START");
            // 주문 알림 동의시에만 앱 알림 받음
            if(ORDER_PUSH == 1)
                chk = true;
        }

        if(ALARM_TYPE == USER_NOTI.DELIVERY_FINISHED){

            logger.info("ALARAM TYPE = DELIVERY_FINISHED");
            // 주문 알림 동의시에만 앱 알림 받음
            if(ORDER_PUSH == 1)
                chk = true;
        }

        if(ALARM_TYPE == USER_NOTI.TO_BE_REFUNDED){

            logger.info("ALARAM TYPE = TO BE REFUNDED");
            // 주문 알림 동의시에만 앱 알림 받음
            if(ORDER_PUSH == 1)
                chk = true;
        }

        if(ALARM_TYPE == USER_NOTI.TO_BE_EXCHANGED){

            logger.info("ALARAM TYPE = TO BE EXCHANGED");
            // 주문 알림 동의시에만 앱 알림 받음
            if(ORDER_PUSH == 1)
                chk = true;
        }

        if(ALARM_TYPE == USER_NOTI.CANCEL_FINISHED){

            logger.info("ALARAM TYPE = RETREIEVE FINISHED");
            // 주문 알림 동의시에만 앱 알림 받음
            if(ORDER_PUSH == 1)
                chk = true;
        }

        if(ALARM_TYPE == USER_NOTI.PROMO){

            logger.info("ALARAM TYPE = PROMOTION");
            // 서비스 알림 동의시에만 앱 알림 받음
            if(SERVICE_PUSH == 1)
                chk = true;

        }

        if(ALARM_TYPE == USER_NOTI.ANSWER_FINISHED){

            logger.info("ALARAM TYPE = CLAIM ANSWER FINISHED");
            // 문의 알림 동의시에만 앱 알림 받음
            if(SERVICE_PUSH == 1)
                chk = true;

        }

        if(ALARM_TYPE == USER_NOTI.POINT_TO_BE_SAVED){

            logger.info("ALARAM TYPE = POINT TO BE SAVED");
            // 공지 알림 동의시에만 앱 알림 받음
            if(POOD_PUSH == 1)
                chk = true;

        }

        if(ALARM_TYPE == USER_NOTI.POINT_TO_BE_USED){

            logger.info("ALARAM TYPE = POINT TO BE USED");
            // 공지 알림 동의시에만 앱 알림 받음
            if(POOD_PUSH == 1)
                chk = true;

        }

        if(ALARM_TYPE == USER_NOTI.EVENT){
            chk = true;
        }

        if(ALARM_TYPE == USER_NOTI.PROMOTION){
            chk = true;
        }

        if(ALARM_TYPE == USER_NOTI.ORDER_LIST){
            chk = true;
        }

        if(ALARM_TYPE == USER_NOTI.REVIEW_LIST){
            chk = true;
        }

        if(ALARM_TYPE == USER_NOTI.GOODS_LIST){
            chk = true;
        }

        if(ALARM_TYPE == USER_NOTI.COUPON_MESSAGE){
            chk = true;
        }

        if(ALARM_TYPE == USER_NOTI.NOTICE){
            chk = true;
        }

        return chk;

    }

    DeviceType getType();
}
