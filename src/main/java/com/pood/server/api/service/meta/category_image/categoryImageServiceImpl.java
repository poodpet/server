package com.pood.server.api.service.meta.category_image;

import org.springframework.stereotype.Service;

import java.util.List;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.record.*;
import com.pood.server.config.DATABASE;
import com.pood.server.object.meta.vo_category_image;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@Service(value="categoryImageService")
public class categoryImageServiceImpl implements categoryImageService{

    @Autowired
    @Qualifier("listService")
    listService listService;
    

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_CATEGORY_IMAGE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
 
    /************* 상품 종류 항목 리스트를 조회합니다. ********************/
    @SuppressWarnings("unchecked")
    public List<vo_category_image> getList() throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME); 
        return (List<vo_category_image>)listService.getDTOList(SELECT_QUERY.toString(), vo_category_image.class);
    }


}
