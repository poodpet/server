package com.pood.server.api.service.view.view_2;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.req.header.order.goods.hAM_goods_2_1;
import com.pood.server.dto.meta.goods.dto_goods_7;
import com.pood.server.object.pagingSet;

public interface view2Service {

    public SELECT_QUERY getQuery(
        Integer goods_idx, 
        Integer goods_type_idx, 
        Integer pc_idx, 
        Integer ct_idx, 
        String  ct_sub_idx, 
        Integer brand_idx, 
        String  keyword, 
        String  main_property, 
        String recordbirth, 
        String unit_size, 
        Integer life_stage,
        Integer feed_target,
        String  position,
        String display_type) throws SQLException;

    public SELECT_QUERY getQuery2(hAM_goods_2_1 header) throws SQLException;

    public List<dto_goods_7> getGoodsList(pagingSet PAGING_SET, SELECT_QUERY query) throws Exception; 

    public List<dto_goods_7> getGoodsList2(pagingSet PAGING_SET, SELECT_QUERY query, String sort_type) throws Exception; 

    public List<String> getKeywordList(String keyword, Integer pc_idx) throws Exception;

    public Integer getBrandIDX(Integer goods_idx) throws Exception;
    
}
