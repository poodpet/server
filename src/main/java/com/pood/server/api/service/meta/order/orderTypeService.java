package com.pood.server.api.service.meta.order;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.meta.order.dto_order_type;
import com.pood.server.object.pagingSet;

public interface orderTypeService {




    /**************** 주문 타입 목록 조회 *****************/
    public List<dto_order_type> getOrderTypeList(pagingSet PAGING_SET) throws Exception;



    /**************** 전체 목록 개수 조회 ******************/
    public Integer getTotalRecordNumber() throws SQLException;



    /**************** 주문 값에 해당하는 상태 이름 조회 ******************/
    public String getOrderTypeName(Integer value) throws Exception;

 
}
