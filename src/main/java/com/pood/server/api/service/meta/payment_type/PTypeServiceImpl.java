package com.pood.server.api.service.meta.payment_type;

import java.sql.SQLException;
import java.util.List;
 
import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_payment_type;
import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
 
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.record.*;
import com.pood.server.config.DATABASE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("PTypeService")
public class PTypeServiceImpl implements PTypeService {

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    private String DEFINE_TABLE_NAME    = com.pood.server.config.DATABASE.TABLE_PAYMENT_TYPE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
 

    /**************** 결제 타입 이름 조회 *****************/
    public String getPaymentTypeName(Integer order_type) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", order_type);
        SELECT_QUERY.buildEqual("visible", 1);
        String ORDER_TYPE_NAME = "";

        dto_payment_type record = (dto_payment_type)listService.getDTOObject(SELECT_QUERY.toString(), dto_payment_type.class);
        if(record != null)
            ORDER_TYPE_NAME = record.getName();
        record = null;

        return ORDER_TYPE_NAME;
    }



    /**************** 결제 타입 목록 조회 *****************/
    @SuppressWarnings("unchecked")
    public List<dto_payment_type> getPaymentTypeList(pagingSet PAGING_SET) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_payment_type>)listService.getDTOList(SELECT_QUERY.toString(), dto_payment_type.class);
    }

    /**************** 결제 타입 항목 개수 조회 *****************/
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    }



    @Override
    public Integer getOrderType(String payment_method) throws Exception {
        Integer ORDER_TYPE = -1;

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("code", payment_method);

        dto_payment_type record = (dto_payment_type)listService.getDTOObject(SELECT_QUERY.toString(), dto_payment_type.class);
        if(record != null)
            ORDER_TYPE = record.getIdx();
        record = null;

        return ORDER_TYPE;
    }
  
}
