package com.pood.server.api.service.query;
 
import org.springframework.stereotype.Service;
import com.pood.server.api.queryBuilder.INSERT_QUERY;
import com.pood.server.api.queryBuilder.UPDATE_QUERY;
import java.util.Map;
import java.util.Iterator;

@Service("queryService")
public class queryServiceImpl implements queryService{
    

    // 삽입 쿼리 반환
    public String getInsertQuery(String db_name, String table_name, Map<String, Object> hashMap, Boolean isUpdate){
        INSERT_QUERY INSERT_QUERY = new INSERT_QUERY();
        INSERT_QUERY.set(db_name, table_name, hashMap, isUpdate);
        return INSERT_QUERY.toString();
    }


    // 업데이트 쿼리 반환
    public String getUpdateQuery(String db_name, String table_name, Map<String, Object> e, String key, String value){
        UPDATE_QUERY UPDATE_QUERY = new UPDATE_QUERY();
        UPDATE_QUERY.set(db_name, table_name);
                
        Iterator<Map.Entry<String,Object>> it = e.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Object> pair = it.next();
            if(pair.getValue() != null){
                if (pair.getValue().getClass() == Integer.class) {
                    UPDATE_QUERY.build(pair.getKey(), (Integer)pair.getValue());
                }
                if (pair.getValue().getClass() == String.class) {
                    UPDATE_QUERY.build(pair.getKey(), (String)pair.getValue());
                }
                if (pair.getValue().getClass() == Float.class) {
                    UPDATE_QUERY.build(pair.getKey(), (Float)pair.getValue());
                }
                if (pair.getValue().getClass() == Double.class) {
                    UPDATE_QUERY.build(pair.getKey(), (Double)pair.getValue());
                }
                if (pair.getValue().getClass() == Boolean.class) {
                    UPDATE_QUERY.build(pair.getKey(), (Boolean)pair.getValue());
                }
                
            }
                        
            it.remove(); 
        }
                
        UPDATE_QUERY.endPoint();
        UPDATE_QUERY.buildEqual(key,      value);
        return UPDATE_QUERY.toString();
    }


    
    public String getDeductValueQuery(String db_name, String table_name, String key, Integer quantity, String column_key, String column_value){
        UPDATE_QUERY UPDATE_QUERY = new UPDATE_QUERY();
        UPDATE_QUERY.set(db_name, table_name);
        UPDATE_QUERY.subtract(key, quantity.toString());
        UPDATE_QUERY.buildEqual(column_key,      column_value);
        return UPDATE_QUERY.toString();
    }



    public String getAddValueQuery(String db_name, String table_name, String key, Integer quantity, String column_key, String column_value){
        UPDATE_QUERY UPDATE_QUERY = new UPDATE_QUERY();
        UPDATE_QUERY.set(db_name, table_name);
        UPDATE_QUERY.addValue(key, quantity.toString());
        UPDATE_QUERY.buildEqual(column_key,      column_value);
        return UPDATE_QUERY.toString();
    }



}
