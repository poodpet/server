package com.pood.server.api.service.log;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.log.dto_log_user_save_point;

public interface logUserSavePointService {
    
    public List<dto_log_user_save_point> getList(pagingSet PAGING_SET, String user_uuid) throws Exception;

    
    public Integer getTotalRecordNumber(String user_uuid) throws SQLException;


    public Integer getTotalGivenPoint(String user_uuid) throws Exception;


    public Integer getToBeSavedPoint(String user_uuid) throws Exception;


    public Integer getToBeRemovedPoint(String user_uuid) throws Exception;
 
 
    public void init(String user_uuid) throws Exception;


    public Integer insertRecord(String USER_UUID, Integer POINT_IDX, String POINT_NAME, Integer POINT_TYPE, Integer POINT_PRICE,
            Integer POINT_RATE, String TEXT, Integer POINT_STATUS, Integer SAVE_POINT, String EXPIRED_DATE)
            throws SQLException;

        public String GET_QUERY_INSERT_RECORD(String USER_UUID, Integer POINT_IDX, String POINT_NAME, Integer POINT_TYPE, Integer POINT_PRICE,
            Integer POINT_RATE, String TEXT, Integer POINT_STATUS, Integer SAVE_POINT, String EXPIRED_DATE);
}
