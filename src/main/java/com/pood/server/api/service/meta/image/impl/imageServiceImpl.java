package com.pood.server.api.service.meta.image.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.file.uploader.StorageService;
import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.meta.image.imageService;
import com.pood.server.api.service.record.*;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.META_IMAGE_TYPE;
import com.pood.server.dto.dto_image_2;
import com.pood.server.object.image_list;
import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_pood_image;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.pood.server.api.service.query.*;

@Service("imageService")
public class imageServiceImpl implements imageService {

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("storageService")
    StorageService storageService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_POOD_IMAGE_TABLE    = DATABASE.TABLE_POOD_IMAGE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_POOD_IMAGE_TABLE);


    /**************** 특정 데이터베이스와 테이블에 대해서 이미지 목록 조회 ********************/
    @SuppressWarnings("unchecked")
    public image_list getImageList(String db_name, String table_name, Integer INDEX, String column)
            throws Exception {

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        SELECT_QUERY.set(db_name, table_name);
        SELECT_QUERY.buildEqual(column, INDEX);

        // 이미지 리스트 저장
        List<dto_image_2> MAIN_IMAGE_LIST = new ArrayList<dto_image_2>();
        List<dto_image_2> IMAGE_LIST = new ArrayList<dto_image_2>();

        List<dto_image_2> list = (List<dto_image_2>)listService.getDTOImageList2(SELECT_QUERY.toString());
        
        if(list != null){
            for(dto_image_2 e : list){
                if(e.getType() == META_IMAGE_TYPE.MAIN_IMAGE)
                    MAIN_IMAGE_LIST.add(e);
                else IMAGE_LIST.add(e);
            }
        }
 
        MAIN_IMAGE_LIST = orderImageList(MAIN_IMAGE_LIST);
        IMAGE_LIST = orderImageList(IMAGE_LIST);

        image_list image_list = new image_list();
        image_list.setMAIN_IMAGE_LIST(MAIN_IMAGE_LIST);
        image_list.setIMAGE_LIST(IMAGE_LIST);

        return image_list;
    }

    /************* 이미지 우선 순위에 따라 정렬 **************/
    public List<dto_image_2> orderImageList( List<dto_image_2> list) {

        for (int ii = 0; ii < list.size(); ii++) {

            Integer IMAGE_PRIORITY_1 = list.get(ii).getPriority();

            for (int jj = ii + 1; jj < list.size(); jj++) {

                Integer IMAGE_PRIORITY_2 = list.get(jj).getPriority();

                if(IMAGE_PRIORITY_1 != null){
                    if(IMAGE_PRIORITY_2 != null){
                        if (IMAGE_PRIORITY_1 >= IMAGE_PRIORITY_2) 
                            Collections.swap(list, ii, jj);
                    }
                }

            }
        }

        return list;
    }

  
    /*********** 이미지 파일 저장 후에는 데이터베이스 레코드 삽입 **************/
    public void insertImageRecord(String db_name, String table_name, Integer INDEX, String URL) throws SQLException {

        Map<String, Object > hashMap = new HashMap<String, Object>();
        hashMap.put("url", URL);
        hashMap.put("pet_idx", INDEX);
        hashMap.put("visible", 0 );

        // DB 갱신시 오류가 발생하면 커넥션 종료
        String query = queryService.getInsertQuery(db_name, table_name, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        updateService.insert(query);

        query = null;

    }
 
    /**************** 이미지 업로드 후 업로드된 URL을 서버에 저장 **********************/
    public Integer uploadImageURL(String db_name, String table_name, String column_name, Integer idx,
        Integer image_type, String URL) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();       
        hashMap.put("url", URL);
        hashMap.put(column_name, idx);

        String query = queryService.getInsertQuery(db_name, table_name, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }

    /***************** 별도로 쓰는 이미지 업로드 *********************/
    public void insertPoodImage(String DESC, Integer IMAGE_IDX) throws SQLException {
        
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("description", DESC);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_POOD_IMAGE_TABLE, hashMap, "idx", IMAGE_IDX.toString());
        updateService.execute(query);
        query = null;
        
    }

    /**************** 별도로 쓰는 이미지 레코드 총 개수 *****************/
    public Integer getTotalPoodImageRecordNumber(Integer idx) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_POOD_IMAGE_TABLE);
        if(idx != null)
            CNT_QUERY.buildEqual("idx", idx);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    /*************** 별도로 쓰는 이미지 레코드 목록 조회 **********************/
    @SuppressWarnings("unchecked")
    public List<vo_pood_image> getPoodImageList(pagingSet PAGING_SET, Integer idx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_POOD_IMAGE_TABLE);
        if(idx != null)
            SELECT_QUERY.buildEqual("idx", idx);
        if(PAGING_SET != null)
            SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<vo_pood_image>)listService.getDTOList(SELECT_QUERY.toString(), vo_pood_image.class); 
    }

    @SuppressWarnings("unchecked")
    public image_list getImageList(String query) throws Exception {
        
         // 이미지 리스트 저장
         List<dto_image_2> MAIN_IMAGE_LIST = new ArrayList<dto_image_2>();
         List<dto_image_2> IMAGE_LIST = new ArrayList<dto_image_2>();
 
         List<dto_image_2> list = (List<dto_image_2>)listService.getDTOImageList2(query);
         
         if(list != null){
             for(dto_image_2 e : list){
                 if(e.getType() == META_IMAGE_TYPE.MAIN_IMAGE)
                     MAIN_IMAGE_LIST.add(e);
                 else IMAGE_LIST.add(e);
             }
         }
  
         MAIN_IMAGE_LIST = orderImageList(MAIN_IMAGE_LIST);
         IMAGE_LIST = orderImageList(IMAGE_LIST);
 
         image_list image_list = new image_list();
         image_list.setMAIN_IMAGE_LIST(MAIN_IMAGE_LIST);
         image_list.setIMAGE_LIST(IMAGE_LIST);
 
         return image_list;
    }
    
}
