package com.pood.server.api.service.meta.order;

import com.pood.server.dto.meta.order.dto_order_cancel;
import java.sql.SQLException;
import java.util.List;
public interface orderCancelService {

    public String insertRecord(String text, String order_number, Integer goods_idx, Integer qty) throws SQLException;

    String insertRecord(final String text, final String orderNumber, final Integer goodsIdx,
        final Integer qty, final Integer deliveryFee, final Integer refundOrderPrice,
        final Integer refundPoint) throws SQLException;

    List<dto_order_cancel> getList(String ORDER_NUMBER) throws Exception;

    public void updateSuccess(String RETREIVE_UUID) throws Exception;

    public Integer getRetreievedCount(String ORDER_NUMBER, Integer GOODS_IDX) throws Exception;

    public String GET_QUERY_UPDATE_SUCCESS(String RETREIVE_UUID);
}
