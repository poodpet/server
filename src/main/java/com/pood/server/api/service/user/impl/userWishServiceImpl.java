package com.pood.server.api.service.user.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.record.*;

import com.pood.server.api.service.query.*;
import com.pood.server.api.service.user.userWishService;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.user.dto_user_wish;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("userWishService")
public class userWishServiceImpl implements userWishService {
    
    @Autowired
    @Qualifier("listService")
    listService listService;
 
    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_USER_WISH;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
    
    
    /*************** 위시 항목 추가 ******************/
    public Integer insertRecord(Integer USER_IDX, Integer GOODS_IDX) throws SQLException{
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_idx", USER_IDX);
        hashMap.put("goods_idx", GOODS_IDX);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
 
    }
    

    @Override
    public Boolean getWishCheck(Integer USER_IDX, Integer GOODS_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_idx", USER_IDX);
        SELECT_QUERY.buildEqual("goods_idx", GOODS_IDX);

        Boolean isWished = false;
        dto_user_wish record = (dto_user_wish)listService.getDTOObject(SELECT_QUERY.toString(), dto_user_wish.class);
        if(record != null)isWished = true;

        return isWished;
    }

    @SuppressWarnings("unchecked")
    public List<dto_user_wish> getUserWishList(Integer USER_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_idx", USER_IDX);
        return (List<dto_user_wish>)listService.getDTOList(SELECT_QUERY.toString(), dto_user_wish.class);
    }

    @Override
    public void deleteRecord(Integer goods_idx, Integer user_idx) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("goods_idx",   Integer.toString(goods_idx));
        DELETE_QUERY.buildEqual("user_idx",    Integer.toString(user_idx));
        updateService.execute(DELETE_QUERY.toString());  
    }

    @Override
    public void deleteRecordWithUSERIDX(Integer USER_IDX) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_idx", Integer.toString(USER_IDX));
        updateService.execute(DELETE_QUERY.toString());
    }
}
