package com.pood.server.api.service.meta.product.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.meta.product.productCTService;
import com.pood.server.api.service.record.*;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.meta.product.dto_product_ct;

@Service("productCTService")
public class productCTServiceImpl implements productCTService{

    @Autowired
    @Qualifier("listService")
    listService listService;
    

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_PRODUCT_CT;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    /************* 상품 종류 항목 리스트를 조회합니다. ********************/
    @SuppressWarnings("unchecked")
    public List<dto_product_ct> getList() throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME); 
        return (List<dto_product_ct>)listService.getDTOList(SELECT_QUERY.toString(), dto_product_ct.class);
    }

    /************* 상품 종류 수를 조회합니다. ********************/
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME); 
        return listService.getRecordCount(CNT_QUERY.toString());
    }

}
