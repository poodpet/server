package com.pood.server.api.service.meta.goods.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.meta.dto_coupon_2;
import com.pood.server.dto.meta.goods.dto_goods_coupon_2;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.meta.coupon.*;
import com.pood.server.api.service.meta.goods.goodsCouponService;
import com.pood.server.api.service.record.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("goodsCouponService")
public class goodsCouponServiceImpl implements goodsCouponService {


    @Autowired
    @Qualifier("couponService")
    couponService couponService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_GOODS_COUPON;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
    
    /**************** 해당 쿠폰이 특정 굿즈에서 사용 가능한지 확인 ******************/
    @SuppressWarnings("unchecked")
    public Boolean checkCouponAvailableWithgoods(Integer COUPON_IDX, Integer GOODS_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("goods_idx", GOODS_IDX);
        SELECT_QUERY.buildEqual("coupon_idx", COUPON_IDX);
        List<dto_goods_coupon_2> result = (List<dto_goods_coupon_2>)listService.getDTOList(SELECT_QUERY.toString(), dto_goods_coupon_2.class);
        if(result == null)return false;
        return true;
    }
    


    /**
     ********************* 거래에 매핑되어 있는 쿠폰 정보 조회 *********************/
    @SuppressWarnings("unchecked")
    public List<dto_coupon_2> getgoodsCouponList(Integer GOODS_IDX)
            throws Exception {

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        SELECT_QUERY.clear();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("goods_idx", GOODS_IDX);

        ArrayList<dto_coupon_2> COUPON_LIST = new ArrayList<dto_coupon_2>();
        List<dto_goods_coupon_2> result = (List<dto_goods_coupon_2>)listService.getDTOList(SELECT_QUERY.toString(), dto_goods_coupon_2.class);
        if(result != null){
            for(dto_goods_coupon_2 e : result){
                Integer COUPON_IDX = (Integer)e.getCoupon_idx();
                dto_coupon_2 record2 = couponService.getCouponObject(COUPON_IDX);
                COUPON_LIST.add(record2);
            }
        }
  
        return COUPON_LIST;
    }

 
    /************* 쿠폰 굿즈 매핑 레코드 삭제 *********************/
    public void DeleteCoupongoods(Integer COUPON_IDX, Integer GOODS_IDX) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("coupon_idx",   Integer.toString(COUPON_IDX));
        DELETE_QUERY.buildEqual("goods_idx",    Integer.toString(GOODS_IDX));
        updateService.execute(DELETE_QUERY.toString());
    }

    /****************** 쿠폰 굿즈 목록 조회 **************************/
    public List<Integer> getgoodsList(Integer COUPON_IDX) throws SQLException{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("coupon_idx", COUPON_IDX);
        List<Integer> GOODS_LIST = listService.getIDXList(SELECT_QUERY.toString(), "goods_idx");
        return GOODS_LIST;
    }

    /****************** 특정 굿즈 항목 번호에 해당하는 쿠폰 목록 조회 ***************/
    @SuppressWarnings("unchecked")
    public List<dto_goods_coupon_2> getList(Integer GOODS_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("goods_idx", GOODS_IDX);
        return (List<dto_goods_coupon_2>)listService.getDTOList(SELECT_QUERY.toString(), dto_goods_coupon_2.class);
    }

    @SuppressWarnings("unchecked")
    public List<Integer> getCouponIDXList(Integer GOODS_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("goods_idx", GOODS_IDX);

        List<Integer> COUPON_LIST = new ArrayList<Integer>();

        List<dto_goods_coupon_2> result = (List<dto_goods_coupon_2>)listService.getDTOList(SELECT_QUERY.toString(), dto_goods_coupon_2.class);
        if(result != null){
            for(dto_goods_coupon_2 e : result){
                Integer COUPON_IDX = (Integer)e.getCoupon_idx();
                COUPON_LIST.add(COUPON_IDX);
            }
        } 

        return COUPON_LIST;
    }



    @Override
    public List<Integer> getGoodsListWithCouponIDX(Integer COUPON_IDX) throws SQLException {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("coupon_idx", COUPON_IDX);
        List<Integer> GOODS_LIST = listService.getIDXList(SELECT_QUERY.toString(), "goods_idx");
        return GOODS_LIST;
    } 
}
