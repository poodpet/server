package com.pood.server.api.service.noti.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.service.record.*;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.noti.pushService;
import com.pood.server.api.service.query.*;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.log.dto_log_user_notice_alaram;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("pushService")
public class pushServiceImpl implements pushService {

    @Autowired
    @Qualifier("updateService")
    public updateService updateService;
    
    @Autowired
    @Qualifier("listService")
    listService listService;
     
    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_LOG_USER_NOTICE_ALARAM;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    

    /***************** 메세지 발송 기록 삽입 ***************/
    public Integer insertRecord(
            Integer USER_IDX, 
            String USER_UUID,
            Integer ALARAM_TYPE, 
            String TITLE, 
            String MESSAGE, 
            String SCHEMA,
            String LINK_URL) throws SQLException {
                
         Map<String, Object> hashMap = new HashMap<String, Object>();
         hashMap.put("user_idx", USER_IDX);
         hashMap.put("alaram_type", ALARAM_TYPE);
         hashMap.put("title", TITLE);
         hashMap.put("message", MESSAGE);
         hashMap.put("schem", SCHEMA);
         hashMap.put("link_url", LINK_URL);
         hashMap.put("user_uuid", USER_UUID);
     
         String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);

         Integer result_idx = updateService.insert(query);

         query = null;

         return result_idx;
    }

    @SuppressWarnings("unchecked")
    public List<dto_log_user_notice_alaram> getList(Integer user_idx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_idx", user_idx);
        SELECT_QUERY.buildReverseOrder("recordbirth");
     
        List<dto_log_user_notice_alaram> list = 
            (List<dto_log_user_notice_alaram>)listService.getDTOList(SELECT_QUERY.toString(), dto_log_user_notice_alaram.class);
        
        return list;
    }
    
}
