package com.pood.server.api.service.meta.disease_group;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_disease_group;
import com.pood.server.dto.meta.dto_disease_group;

public interface diseaseGroupService {
   
    /************* 질병 그룹 목록 조회 ***************/
    public List<dto_disease_group> getDiseaseGroupList(pagingSet PAGING_SET) throws Exception;

    /**************** 질병 항목 개수 조회 ******************/
    public Integer getTotalRecordNumber() throws SQLException;


    public List<vo_disease_group> getObjectList() throws Exception;
    
}
