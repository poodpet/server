package com.pood.server.api.service.pood_point;
import com.pood.server.dto.meta.dto_point;
import com.pood.server.api.service.meta.point.*;
import com.pood.server.dto.meta.order.dto_order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.pood.server.config.meta.point.META_POINT_TYPE;
import com.pood.server.config.meta.point.POINT_SAVE_STATUS;
import com.pood.server.api.service.meta.order.OrderService;
import com.pood.server.config.meta.point.POINT_USE_STATUS;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import com.pood.server.dto.meta.order.dto_order_point;
import com.pood.server.dto.user.dto_user_point;
import com.pood.server.config.meta.user.USER_POINT;
import com.pood.server.api.service.user.*;
import com.pood.server.api.service.log.*;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.meta.order.*;

@Service(value="poodPointService")
public class poodPointServiceImpl implements poodPointService{

    Logger logger = LoggerFactory.getLogger(poodPointServiceImpl.class);

    @Autowired
    @Qualifier("orderService")
    OrderService orderService;

    @Autowired
    @Qualifier("pointService")
    pointService pointService;

    @Autowired
    @Qualifier("userPointService")
    userPointService userPointService;

    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("logUserPointService")
    logUserPointService logUserPointService;

    @Autowired
    @Qualifier("logUserSavePointService")
    logUserSavePointService logUserSavePointService;

    @Autowired
    @Qualifier("logUserUsePointService")
    logUserUsePointService logUserUsePointService;

    @Autowired
    @Qualifier("orderPointService")
    orderPointService orderPointService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;




    // 적립금 적립
    public List<String> issuePoint(String ORDER_NUMBER, Integer REVIEW_IDX, String USER_UUID, Integer POINT_IDX, Integer POINT_STATUS) throws Exception {


        List<String> transaction_process = new ArrayList<String>();


        /***************************************
        *
        *  (1)   적립예정 항목이 신규로 추가되는 경우 : 회원 포인트에 적립예정 적립금항목을 추가
        *  (2.1) 적립완료 항목이 신규로 추가되는 경우 : 회원 포인트에 적립완료 적립금항목을 추가
        *  (2.2) 적립완료 항목이 신규로 추가되면서 실 적립금내역에도 추가
        *  (3)   적립금 지급 내역에 내역 추가
        *
        **************************************/

        dto_point record = pointService.getPointObject(POINT_IDX);

        dto_order order = orderService.getOrder(ORDER_NUMBER);

        Integer SAVE_POINT = 0;


        Integer  LIMIT = record.getExpired_day();
        DateTime dt = new DateTime(new Date());

        String EXPIRED_DATE = dt.plusDays(LIMIT).toString("yyyy-MM-dd HH:mm:ss");

        // 적립금 적립예정인 경우
        if(POINT_STATUS == POINT_SAVE_STATUS.TO_BE_SAVED){


            Integer POINT_CODE  = record.getIdx();


            // 앱에서 적립예정 로그가 생성되는 경우
            Boolean isNEW = false;


            // 적립금 코드 12 : 구매 적립금
            if(POINT_CODE == META_POINT_TYPE.PURCHASE){
                isNEW = true;

                Integer ORDER_PRICE     = order.getOrder_price();

                Integer DELIVERY_FEE    = order.getDelivery_fee();


                SAVE_POINT =
                    pointService.getPoint(
                    POINT_IDX,
                    ORDER_PRICE,
                    DELIVERY_FEE);

                ORDER_PRICE     = null;

                DELIVERY_FEE    = null;

            }


            if(isNEW){

                logger.info("회원 고유번호[" + USER_UUID + "]의 " + record.getPoint_name() + "" + SAVE_POINT + "원이 [적립예정] 상태로 적립이 됩니다.");


                String POINT_UUID = UUID.randomUUID().toString();

                String process_1 = userPointService.GET_QUERY_INSERT_RECORD(
                        USER_UUID,
                        ORDER_NUMBER,                                   // 구매 적립금인 경우 주문 번호
                        REVIEW_IDX,                                     // 리뷰 적립금인 경우 리뷰 항목 번호
                        POINT_CODE,
                        record.getPoint_name(),
                        record.getPoint_type(),
                        record.getPoint_price(),
                        record.getPoint_rate(),
                        EXPIRED_DATE,
                        USER_POINT.STATUS_READY_TO_BE_SAVED,            // 상태 : 적립금 적립 예정
                        SAVE_POINT,                                     // 적립금액
                        0,                                              // 사용되지 않았으므로 적립금 : 0
                        POINT_UUID);

                transaction_process.add(process_1);

                process_1 = null;
            }

        }

        // 적립금 적립완료인 경우
        if(POINT_STATUS == POINT_SAVE_STATUS.SAVE_COMPLETE){


            // 사용자 적립금 적립됨
            Integer POINT_CODE  = record.getIdx();


            // 앱에서 적립완료 로그가 바로 생성되는 경우
            Boolean isNEW = false;



            // 적립금 코드 21 : 첫 리뷰 적립금
            if(POINT_CODE == META_POINT_TYPE.FIRST_REVIEW){
                SAVE_POINT = record.getPoint_price();
                isNEW = true;
            }



            // 적립금 코드 22 : 텍스트 리뷰 적립금
            if(POINT_CODE == META_POINT_TYPE.TEXT_REVIEW){
                SAVE_POINT = record.getPoint_price();
                isNEW = true;
            }



            // 적립금 코드 23 : 포토 리뷰 적립금
            if(POINT_CODE == META_POINT_TYPE.PHOTO_REVIEW){
                SAVE_POINT = record.getPoint_price();
                isNEW = true;
            }



            // 적립금 코드 41 : 앱 푸시 적립금
            if(POINT_CODE == META_POINT_TYPE.PUSH){

                SAVE_POINT = record.getPoint_price();
                isNEW = true;
            }




            String process_2 = userService.GET_QUERY_SAVE_USER_POINT(USER_UUID, SAVE_POINT);

            transaction_process.add(process_2);





            if(isNEW){

                logger.info("회원 고유번호[" + USER_UUID + "]의 " + record.getPoint_name() + "" + SAVE_POINT + "원이 [적립완료] 상태로 적립이 됩니다.");

                String POINT_UUID = UUID.randomUUID().toString();

                String process_3 = userPointService.GET_QUERY_INSERT_RECORD(
                        USER_UUID,
                        ORDER_NUMBER,                                   // 구매 적립금인 경우 주문 번호
                        REVIEW_IDX,                                     // 리뷰 적립금인 경우 리뷰 항목 번호
                        POINT_CODE,
                        record.getPoint_name(),
                        record.getPoint_type(),
                        record.getPoint_price(),
                        record.getPoint_rate(),
                        EXPIRED_DATE,
                        USER_POINT.STATUS_SAVE_COMPLETE,                // 상태 : 적립금 적립 완료
                        SAVE_POINT,                                     // 적립금액
                        0,
                        POINT_UUID);                                             // 사용되지 않았으므로 적립금 : 0

                transaction_process.add(process_3);

                String process_4 = logUserPointService.GET_QUERY_INSERT_RECORD(USER_UUID, POINT_UUID, SAVE_POINT);

                transaction_process.add(process_4);

                process_3 = null;

                process_4 = null;


            }

        }

        String process_0 = logUserSavePointService.GET_QUERY_INSERT_RECORD(
                USER_UUID,
                record.getIdx(),
                record.getPoint_name(),
                record.getPoint_type(),
                record.getPoint_price(),
                record.getPoint_rate(),
                POINT_SAVE_STATUS.SET_TEXT(POINT_STATUS),
                POINT_STATUS,
                SAVE_POINT,
                EXPIRED_DATE
            );

        transaction_process.add(process_0);

        return transaction_process;
    }






    // 적립금 적립 취소
    public List<String> cancelIssuedPoint(String ORDER_NUMBER, Double RATIO) throws Exception {

        List<String> transaction_process = new ArrayList<String>();

        dto_user_point e = userPointService.getPointWithOrderNumber(ORDER_NUMBER);

        /***************************************
         *
         *  (1) 주문에 매핑된 모든 포인트 적립 취소 상태로 업데이트
         *  (2) 회원 적립 포인트 적립 취소 내역 추가
         *
         **************************************/


        String POINT_UUID       = e.getPoint_uuid();

        Integer POINT_IDX       = e.getPoint_idx();

        String USER_UUID        = e.getUser_uuid();

        Integer SAVED_POINT     = e.getSaved_point();

        String EXPIRED_DATE     = e.getExpired_date();


        dto_point point_record = pointService.getPointObject(POINT_IDX);

        if(e.getStatus() != USER_POINT.STATUS_SAVE_RETRIEVED){

            if(RATIO == null){

                // 전체 취소일 경우 적립금 전체 회수
                String process_1 = userPointService.GET_QUERY_UPDATE_POINT_STATUS(USER_POINT.STATUS_SAVE_RETRIEVED, POINT_UUID);

                transaction_process.add(process_1);

                String process_2 = logUserSavePointService.GET_QUERY_INSERT_RECORD(
                    USER_UUID,
                    POINT_IDX,
                    point_record.getPoint_name(),
                    point_record.getPoint_type(),
                    point_record.getPoint_price(),
                    point_record.getPoint_rate(),
                    POINT_SAVE_STATUS.SET_TEXT(POINT_SAVE_STATUS.SAVE_RETRIEVED),
                    POINT_SAVE_STATUS.SAVE_RETRIEVED, SAVED_POINT, EXPIRED_DATE);

                transaction_process.add(process_2);

                process_1       = null;

                process_2       = null;

                point_record    = null;

            }else{

                Double  TO_BE_REMOVED_POINT = (e.getSaved_point() * RATIO);

                Integer TO_BE_REMOVED = TO_BE_REMOVED_POINT.intValue();

                Integer TO_BE = e.getSaved_point() - TO_BE_REMOVED;

                String process_1 = userPointService.GET_QUERY_UPDATE_SAVED_POINT(TO_BE, POINT_UUID);

                transaction_process.add(process_1);


                String process_2 = logUserSavePointService.GET_QUERY_INSERT_RECORD(
                    USER_UUID,
                    POINT_IDX,
                    point_record.getPoint_name(),
                    point_record.getPoint_type(),
                    point_record.getPoint_price(),
                    point_record.getPoint_rate(),
                    POINT_SAVE_STATUS.SET_TEXT(POINT_SAVE_STATUS.SAVE_RETRIEVED),
                    POINT_SAVE_STATUS.SAVE_RETRIEVED, TO_BE_REMOVED, EXPIRED_DATE);

                transaction_process.add(process_2);

                if(TO_BE <= 0){
                    String process_3 = userPointService.GET_QUERY_UPDATE_POINT_STATUS(USER_POINT.STATUS_SAVE_RETRIEVED, POINT_UUID);
                    transaction_process.add(process_3);
                    process_3 = null;
                }


                String process_4 = orderService.GET_QUERY_UPDATE_SAVED_POINT(TO_BE, ORDER_NUMBER);

                transaction_process.add(process_4);

                process_4 = null;

                process_1   =   null;

                TO_BE                   = null;

                TO_BE_REMOVED           = null;

                TO_BE_REMOVED_POINT     = null;


            }

        }

        EXPIRED_DATE    = null;

        USER_UUID       = null;

        POINT_UUID      = null;

        SAVED_POINT     = null;

        return transaction_process;
    }






    // 적립금 사용
    public Integer usePoint(String ORDER_NUMBER, String USER_UUID, Integer TYPE, String TEXT, Integer USE_POINT) throws Exception{

        Integer RECORD_IDX = -1;


        if(TYPE == POINT_USE_STATUS.USE_COMPLETE){


            /*************************************************
            *
            *   (1) 회원 전체 포인트 중에서 사용한 포인트만큼 차감
            *   (2) 주문에 매핑된 적립금 기록 추가
            *   (3) 적립금 소진 내역에 적립금 소진 내역 추가 (서버용)
            *   (4) 회원 부분 적립금 상태 사용 업데이트
            *   (5) 적립금 소진 내역에 적립금 소진 내역 추가
            *
            ******************************************************/


            Integer CURRENT_POINT = USE_POINT;

            Boolean chk = userPointService.isUsePossible(USER_UUID, USE_POINT);


            logger.info("회원 고유번호[" + USER_UUID + "]이 적립금 " + USE_POINT + "원을 사용합니다. ");

            // 만약에 사용가능한 포인트가 없는 경우 에러 



            if(chk){

                Boolean isPointUsed = true;

                List<dto_user_point> list = userPointService.getListWithUserUUID2(USER_UUID);

                List<tmp0> list2 = new ArrayList<tmp0>();

                while(true){

                    if(CURRENT_POINT == 0)break;

                    Iterator<dto_user_point> iterator = list.iterator();

                    Boolean isPoint = false;

                    while(iterator.hasNext()){

                        if(CURRENT_POINT == 0)break;

                        dto_user_point e = iterator.next();

                        if(e.getStatus() == USER_POINT.STATUS_SAVE_COMPLETE){

                            isPoint = true;

                            String POINT_UUID           = e.getPoint_uuid();

                            Integer SAVED_POINT         = e.getSaved_point();

                            Integer USED_POINT          = e.getUsed_point();

                            Integer DIFF                = SAVED_POINT - USED_POINT;

                            logger.info("적립금[" + POINT_UUID + "] 적립된 " + DIFF + "원을 사용합니다. 현재 소진되어야 하는 적립금 : " + CURRENT_POINT);


//                            if(SAVED_POINT >= CURRENT_POINT){

                                // 사용하고자 하는 적립금이 사용가능한 적립금보다 적은 경우, 해당 사용가능한 적립금의 수치가 차감됨
                            Integer TO_BE_POINT = 0;

                            if (DIFF >= CURRENT_POINT) {

                                // 해당 적립금을 통해서 전부 소진이 가능한 경우, 해당 적립금을 통해서 전부 소진
                                TO_BE_POINT = USED_POINT + CURRENT_POINT;

                                if (TO_BE_POINT.equals(SAVED_POINT)) {
                                    e.setStatus(USER_POINT.STATUS_USED);
                                    list2.add(
                                        new tmp0(POINT_UUID, CURRENT_POINT, USER_POINT.STATUS_USED,
                                            TO_BE_POINT));
                                } else {
                                    list2.add(new tmp0(POINT_UUID, CURRENT_POINT,
                                        USER_POINT.STATUS_SAVE_COMPLETE, TO_BE_POINT));
                                }

                                CURRENT_POINT = 0;
                                logger.info(
                                    "적립금[" + POINT_UUID + "]의 총 " + TO_BE_POINT + "원을 사용하였습니다. ");
                                e.setUsed_point(TO_BE_POINT);

                            } else {

                                // 해당 적립금을 통해서 소진이 전부 불가능한 경우
                                CURRENT_POINT -= DIFF;
                                TO_BE_POINT += USED_POINT + DIFF;
                                if (TO_BE_POINT.equals(SAVED_POINT)) {
                                    e.setStatus(USER_POINT.STATUS_USED);
                                    list2.add(new tmp0(POINT_UUID, DIFF, USER_POINT.STATUS_USED,
                                        TO_BE_POINT));
                                } else {
                                    list2.add(
                                        new tmp0(POINT_UUID, DIFF, USER_POINT.STATUS_SAVE_COMPLETE,
                                            TO_BE_POINT));
                                }
                                logger.info("적립금[" + POINT_UUID + "]의 " + DIFF + "원을 사용하였습니다. ");
                                e.setUsed_point(TO_BE_POINT);

                            }

//                            }else if(SAVED_POINT < CURRENT_POINT){
//
//                                // 사용하고자 하는 적립금이 사용가능한 적립금보다 많은 경우, 해당 적립금은 모두 소진
//                                CURRENT_POINT -= SAVED_POINT;
//                                e.setUsed_point(SAVED_POINT);
//                                e.setStatus(USER_POINT.STATUS_USED);
//                                list2.add(new tmp0(POINT_UUID, SAVED_POINT, USER_POINT.STATUS_USED, SAVED_POINT));
//                                logger.info("적립금[" + POINT_UUID + "]의 " + SAVED_POINT + "원을 사용하였습니다. ");
//
//                            }

                        }


                    }

                    if(!isPoint){
                        isPointUsed= false;
                        break;
                    }

                }


                if(isPointUsed){

                    List<String> transaction_process = new ArrayList<String>();

                    String PROCESS_0 = userService.GET_QUERY_USE_USER_POINT(USER_UUID, USE_POINT);

                    transaction_process.add(PROCESS_0);

                    for(tmp0 e3:list2){

                        String PROCESS_1 = logUserPointService.GET_QUERY_INSERT_RECORD(USER_UUID, e3.getPoint_uuid(), (e3.getUsed_point() * -1));

                        String PROCESS_2 = orderPointService.GET_QUERY_INSERT_RECORD(e3.getPoint_uuid(), ORDER_NUMBER, e3.getUsed_point());
                        
                        String PROCESS_3 = userPointService.GET_QUERY_UPDATE_POINT_STATUS(e3.getStatus(), e3.getPoint_uuid());

                        String PROCESS_4 = userPointService.GET_QUERY_UPDATE_USED_POINT(e3.getTo_be_point(), e3.getPoint_uuid());

                        transaction_process.add(PROCESS_1);

                        transaction_process.add(PROCESS_2);

                        transaction_process.add(PROCESS_3);

                        transaction_process.add(PROCESS_4);

                        PROCESS_1 = null;

                        PROCESS_2 = null;

                        PROCESS_3 = null;

                        PROCESS_4 = null;

                    }

                    String PROCESS_3 = logUserUsePointService.GET_QUERY_INSERT_RECORD(USER_UUID, TYPE, TEXT, USE_POINT);

                    transaction_process.add(PROCESS_3);

                    PROCESS_3 = null;

                    PROCESS_0 = null;

                    list = null;
    
                    list2 = null;


                    updateService.commit(transaction_process);

                    transaction_process = null;

                }else RECORD_IDX = -2;

            }else RECORD_IDX = -2;

        }

        return RECORD_IDX;
    }








    // 주문시 소진된 적립금 반환
    public List<String> rollbackOrderPoint(String USER_UUID, String ORDER_NUMBER, Integer TYPE, String TEXT, Integer RETURN_POINT) throws Exception {




        List<String> transaction_process = new ArrayList<String>();



        // 주문시 사용했던 적립금 리스트 조회
        List<dto_order_point> list = orderPointService.getUsedPointList(ORDER_NUMBER);

        Integer TMP = RETURN_POINT;

        if(list != null){

            for(dto_order_point e : list){

                String      POINT_UUID   = e.getPoint_uuid();

                Integer     RECORD_IDX      = e.getIdx();

                dto_user_point record = userPointService.getRecord(POINT_UUID);

                if(record != null){

                    Integer     TO_BE_POINT         = record.getUsed_point();

                    Integer     THIS_USED_POINT     = record.getUsed_point();

                    if((RETURN_POINT.compareTo(THIS_USED_POINT) == 1) || (RETURN_POINT.compareTo(THIS_USED_POINT) == 0)){


                        // 반환해야 하는 적립금이 해당 적립금보다 클 때
                        TO_BE_POINT = 0;


                        /**********************************************
                         *
                         *   (1) 회원에게 소진 취소한 적립금만큼 지급 
                         *   (2) 적립금 : 소진 취소된 적립금은 다시 적립 완료 상태로 변경
                         *   (3) 적립금 : 소진한 적립금은 다시 계산하여 업데이트
                         *   (4) 적립금 적립 로그 생성
                         *   (5) 주문에 매핑된 적립금 상태를 사용취소로 변경함
                         *
                        **********************************************/
                        String process_1 = userService.GET_QUERY_SAVE_USER_POINT(USER_UUID, THIS_USED_POINT);
                        String process_2 = userPointService.GET_QUERY_UPDATE_POINT_STATUS(USER_POINT.STATUS_SAVE_COMPLETE, POINT_UUID);
                        String process_3 = userPointService.GET_QUERY_UPDATE_USED_POINT(TO_BE_POINT, POINT_UUID);
                        String process_4 = logUserPointService.GET_QUERY_INSERT_RECORD(USER_UUID, POINT_UUID, THIS_USED_POINT);
                        String process_5 = orderPointService.GET_QUERY_UPDATE_RETRIEVED_STATUS(RECORD_IDX);

                        transaction_process.add(process_1);
                        transaction_process.add(process_2);
                        transaction_process.add(process_3);
                        transaction_process.add(process_4);
                        transaction_process.add(process_5);
                        RETURN_POINT -= THIS_USED_POINT;

                        process_1 = null;
                        process_2 = null;
                        process_3 = null;
                        process_4 = null;
                        process_5 = null;



                    }else{



                        /**********************************************
                         *
                         *   (1) 회원에게 소진 취소한 적립금만큼 지급 
                         *   (2) 적립금 : 소진 취소된 적립금은 다시 적립 완료 상태로 변경
                         *   (3) 적립금 : 소진한 적립금은 다시 계산하여 업데이트
                         *   (4) 적립금 적립 로그 생성
                         *   다 사용취소되지 않았으므로 주문에 매핑된 적립금 상태를 회수상태로 변경하지 않음
                         *
                        **********************************************/

                        // 반환해야 하는 적립금이 해당 적립금보다 작을 때
                        TO_BE_POINT = THIS_USED_POINT - RETURN_POINT;
                        String process_1 = userService.GET_QUERY_SAVE_USER_POINT(USER_UUID, RETURN_POINT);
                        String process_2 = userPointService.GET_QUERY_UPDATE_POINT_STATUS(USER_POINT.STATUS_SAVE_COMPLETE, POINT_UUID);
                        String process_3 = userPointService.GET_QUERY_UPDATE_USED_POINT(TO_BE_POINT, POINT_UUID);
                        String process_4 = logUserPointService.GET_QUERY_INSERT_RECORD(USER_UUID, POINT_UUID, RETURN_POINT);

                        transaction_process.add(process_1);
                        transaction_process.add(process_2);
                        transaction_process.add(process_3);
                        transaction_process.add(process_4);

                        process_1 = null;
                        process_2 = null;
                        process_3 = null;
                        process_4 = null;

                        RETURN_POINT = 0;



                    }

                    TO_BE_POINT = null;
                }

                record = null;

                POINT_UUID = null;

            }
        }

        list = null;



        String process_0 = logUserUsePointService.GET_QUERY_INSERT_RECORD(USER_UUID, POINT_USE_STATUS.USE_RETRIEVE, TEXT, (TMP * -1));

        transaction_process.add(process_0);

        process_0 = null;


        return transaction_process;
    }


}
