package com.pood.server.api.service.meta.order.impl;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.meta.order.orderDeliveryService;
import com.pood.server.api.service.meta.order.orderExchangeService;
import com.pood.server.api.service.query.queryService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.order.ORDER_EXCHANGE;
import com.pood.server.dto.meta.order.dto_order_exchange;
import com.pood.server.dto.meta.order.dto_order_exchange_2;
import com.pood.server.object.meta.vo_order_exchange;
import com.pood.server.object.meta.vo_order_exchange_delivery;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service(value="orderExchangeService")
public class orderExchangeServiceImpl implements orderExchangeService{
 
    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;
    
    @Autowired
    @Qualifier("orderDeliveryService")
    orderDeliveryService orderDeliveryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_ORDER_EXCHANGE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @Override
    public String insertRecord(String text, String orderNumber, int goodsIdx, int qty, int sendType,
        boolean causeAttributable) throws SQLException {
        String EXCHANGE_UUID = UUID.randomUUID().toString();
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("order_number", orderNumber);
        hashMap.put("goods_idx", goodsIdx);
        hashMap.put("qty", qty);
        hashMap.put("text", text);
        hashMap.put("type", ORDER_EXCHANGE.TYPE_REQUEST);
        hashMap.put("attr", causeAttributable);
        hashMap.put("uuid", EXCHANGE_UUID);
        hashMap.put("send_type", sendType);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        updateService.insert(query);

        query = null;

        return EXCHANGE_UUID;
    }

    @SuppressWarnings("unchecked")
    public List<dto_order_exchange> getList(String orderNumber) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", orderNumber);
        return (List<dto_order_exchange>)listService.getDTOList(SELECT_QUERY.toString(), dto_order_exchange.class);
    }

    @Override
    public vo_order_exchange getRecord(String uuid) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("uuid", uuid);
        return (vo_order_exchange)listService.getDTOObject(SELECT_QUERY.toString(), vo_order_exchange.class);
    }

    @Override
    public void upddateStatus(int typeRetrieve, String uuid) throws SQLException {
        
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("type", typeRetrieve);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "uuid", uuid);
        updateService.execute(query);
        query = null;

    }


    @SuppressWarnings("unchecked")
    public List<dto_order_exchange_2> getList2(String orderNumber) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", orderNumber);
        List<dto_order_exchange_2> list = (List<dto_order_exchange_2>)listService.getDTOList(SELECT_QUERY.toString(), dto_order_exchange_2.class);

        if(list != null){
            for(dto_order_exchange_2 e : list){
                String exchange_number = e.getUuid();
                List<vo_order_exchange_delivery> delivery_list = orderDeliveryService.getDeliveryListWithExchangeNumber(exchange_number);
                e.setDelivery(delivery_list);
                delivery_list = null;
            }
        }

        return list;
    }

    @Override
    public vo_order_exchange getRecord(Integer goodsIdx, String orderNumber) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", orderNumber);
        SELECT_QUERY.buildEqual("goods_idx", goodsIdx);
        vo_order_exchange record = (vo_order_exchange)listService.getDTOObject(SELECT_QUERY.toString(), vo_order_exchange.class);
        return record;
    }
 
}
