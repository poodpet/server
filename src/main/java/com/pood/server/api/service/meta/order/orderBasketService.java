package com.pood.server.api.service.meta.order;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.meta.goods.dto_goods_3;
import com.pood.server.dto.meta.order.dto_order_basket;
import com.pood.server.object.IMP.*;
import com.pood.server.object.meta.vo_order_basket;
import com.pood.server.object.resp.resp_order_basket;
public interface orderBasketService {
    
    public List<resp_order_basket> getgoodsList(Integer ORDER_IDX) throws Exception;
 
    
    
    /****************** 특정 주문 번호에 대해서 장바구니 목록 조회 *******************/
    public List<dto_order_basket> getOrderBasketList2(Integer ORDER_IDX) throws Exception;





   /********** 주문시도시 장바구니 정보 저장 **************/
    public Integer insertBasketData(IMP_SHOP e1, Integer ORDER_IDX, String ORDER_NUMBER, Integer DISCOUNT_PRICE) throws SQLException;







    /************* 주문에 동원된 쿠폰 사용 처리 **************/
    public List<String> updateBasketCoupon(Integer ORDER_IDX) throws Exception;







    /************** 주문 번호에 해당하는 주문 장바구니 목록 조회 *************/
    public List<vo_order_basket> getOrderBasketList(Integer ORDER_IDX) throws Exception;





    
    /***************** 주문 번호에 해당하는 장바구니 : 굿즈 정보 조회  ***********************/
    public List<dto_goods_3> getgoodsListWithOrderNumber(Integer USER_IDX, Integer ORDER_IDX, String ORDER_NUMBER) throws Exception;







    /***************** 주문 번호에 해당하는 장바구니 리스트 전부 삭제 ******************/
    public void deleteBasketWithOrderINDEX(Integer ORDER_IDX) throws Exception;

 


    public Integer getPrice(String ORDER_NUMBER, Integer GOODS_IDX) throws Exception;





 

    public Integer checkRetrievePossible(String ORDER_NUMBER, Integer CANCEL_GOODS_IDX, Integer CANCEL_GOODS_QTY) throws Exception;
 





    public vo_order_basket getOrderBasket(String ORDER_NUMBER, Integer GOODS_IDX) throws Exception;







    public String GET_QUERY_INSERT_BASKET_DATA(IMP_SHOP e1, Integer ORDER_IDX, String ORDER_NUMBER, Integer DISCOUNT_PRICE, Integer PR_DISCOUNT_RATE);



    public Boolean isFinalRetreieve(String ORDER_NUMBER, Integer GOODS_IDX) throws Exception;
 

    

}
