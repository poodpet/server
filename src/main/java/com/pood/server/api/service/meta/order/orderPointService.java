package com.pood.server.api.service.meta.order;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.meta.order.dto_order_point;

public interface orderPointService {

    @Deprecated(since = "23.02.14")
    List<dto_order_point> getUsedPointList(String ORDER_NUMBER) throws Exception;

    public void insertRecord(String POINT_UUID, String ORDER_NUMBER, Integer USED_POINT) throws SQLException;

    public void updateRetrievedStatus(Integer RECORD_IDX) throws SQLException;

    public void init(String user_uuid) throws Exception;

    public String GET_QUERY_UPDATE_RETRIEVED_STATUS(Integer RECORD_IDX);

    public String GET_QUERY_INSERT_RECORD(String POINT_UUID, String ORDER_NUMBER, Integer USED_POINT);
    
}
