package com.pood.server.api.service.meta.goods_type;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_goods_type;

public interface goodsTypeService {

	public Integer getTotalRecordNumber() throws SQLException;

	public List<vo_goods_type> getList(pagingSet PAGING_SET) throws Exception;
    
}
