package com.pood.server.api.service.pet_rec;

import java.util.ArrayList;

import com.pood.server.object.user.vo_user_pet2;

public interface PETRECService {
 
    public ArrayList<Integer> getResult(vo_user_pet2 userPet) throws Exception;
}
