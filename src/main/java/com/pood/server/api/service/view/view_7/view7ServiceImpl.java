package com.pood.server.api.service.view.view_7;

import java.util.ArrayList;
import java.util.List;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.META_GOODS;
import com.pood.server.object.meta.vo_view_7;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.record.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("view7Service")
public class view7ServiceImpl implements view7Service{
    
    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;
 
    private String DEFINE_TABLE_NAME    = DATABASE.VIEW_7;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);


    @SuppressWarnings("unchecked")
    public List<Integer> getGoodsIDXList(Integer PRODUCT_IDX) throws Exception {
        List<Integer> GOODS_LIST = new ArrayList<Integer>();

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME,
                DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("product_idx", PRODUCT_IDX);

        List<vo_view_7> result = (List<vo_view_7>)listService.getDTOList(SELECT_QUERY.toString(), vo_view_7.class);
 

   
        
        // 판매중인 상품 먼저 조회
        for(vo_view_7 e : result){
            if(e.getSale_status() == META_GOODS.SALE_STATUS_ON){
                Integer tmp = e.getGoods_idx();
                GOODS_LIST.add(tmp);
                tmp = null;
            }

        }




        // 품절 상품 먼저 조회
        for(vo_view_7 e : result){
            if(e.getSale_status() == META_GOODS.SALE_STATUS_HOLD){
                Integer tmp = e.getGoods_idx();
                GOODS_LIST.add(tmp);
                tmp = null;
            }
        }



        // 판매중지 상품 먼저 조회
        for(vo_view_7 e : result){
            if(e.getSale_status() == META_GOODS.SALE_STATUS_STOP){
                Integer tmp = e.getGoods_idx();
                GOODS_LIST.add(tmp);
                tmp = null;
            }
        }
 
        return GOODS_LIST;

    }
 

}
