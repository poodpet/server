package com.pood.server.api.service.user.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.record.updateService;
import com.pood.server.api.service.user.userDeliveryService;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.user.USER_DELIVERY_ADDRESS;
import com.pood.server.dto.user.dto_user_delivery_address;
import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.service.query.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("userDeliveryService")
public class userDeliveryServiceImpl implements userDeliveryService {

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_USER_DELIVERY_ADDRESS;
    
    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    /*************** 회원 배송지 등록 *********************/
    public Integer insertUserDeliveryAddress(Integer USER_IDX, Boolean DEFAULT_TYPE, String DELIVERY_ADDRESS, String DETAIL_ADDRESS, 
        String NAME, String NICKNAME, String ZIPCODE, Boolean INPUT_TYPE, Integer REMOTE_TYPE, String PHONE_NUMBER) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_idx", USER_IDX);
        hashMap.put("default_type", DEFAULT_TYPE);
        hashMap.put("address", DELIVERY_ADDRESS);
        hashMap.put("detail_address", DETAIL_ADDRESS);
        hashMap.put("name", NAME);
        hashMap.put("nickname", NICKNAME);
        hashMap.put("zipcode", ZIPCODE);
        hashMap.put("input_type", INPUT_TYPE);
        hashMap.put("remote_type", REMOTE_TYPE);
        hashMap.put("phone_number", PHONE_NUMBER);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }

    /**************** 특정 배송지를 기본 배송지로 지정 **************/
    public void updateAddressToDefault(Integer RECORD_IDX) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("default_type", 0);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", RECORD_IDX.toString());
        updateService.execute(query);
        query = null;

    }

    /****************** 배송지 정보 업데이트 *********************/
    public void updateDeliveryAddress(String ADDRESS, Boolean DEFAULT_TYPE, String DETAIL_ADDRESS, String NAME,
            String NICKNAME, String ZIPCODE, Boolean INPUT_TYPE, Integer REMOTE_TYPE, String PHONE_NUMBER, Integer IDX)
            throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("address", ADDRESS);
        hashMap.put("default_type", DEFAULT_TYPE);
        hashMap.put("detail_address", DETAIL_ADDRESS);
        hashMap.put("name", NAME);
        hashMap.put("nickname", NICKNAME);
        hashMap.put("zipcode", ZIPCODE);
        hashMap.put("input_type", INPUT_TYPE);
        hashMap.put("remote_type", REMOTE_TYPE);
        hashMap.put("phone_number", PHONE_NUMBER);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", IDX.toString());
        updateService.execute(query);
        query = null;
        
    }


    /**************** 특정 회원의 배송지 목록 조회 *******************/
    @SuppressWarnings("unchecked")
    public List<dto_user_delivery_address> getUserDeliveryList(Integer user_idx) throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_idx", user_idx);     
        return (List<dto_user_delivery_address>)listService.getDTOList(SELECT_QUERY.toString(), dto_user_delivery_address.class);
    }


    /******************* 배송지 항목 번호에 해당하는 회원 항목 번호 조회 ****************/
    public Integer getUserIndex(Integer delivery_idx) throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", delivery_idx);
        dto_user_delivery_address record = (dto_user_delivery_address)listService.getDTOObject(SELECT_QUERY.toString(), dto_user_delivery_address.class);
        return record.getUser_idx();
    }



    /******************* 현재 회원이 가지고 있는 주문 배송지 개수 조회 ************/
    public Integer getCountDeliveryAddress(Integer USER_IDX) throws SQLException{
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        CNT_QUERY.buildEqual("user_idx", USER_IDX);
        return listService.getRecordCount(CNT_QUERY.toString());
    }


    /***************** 만약 남은 카드 개수가 하나일 경우 그 카드는 자동으로 디폴트로 지정 *****************/
	public void updateDefaultAddress(Integer USER_IDX) throws SQLException{
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("default_type", USER_DELIVERY_ADDRESS.DEFAULT_TYPE);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "user_idx", USER_IDX.toString());
        updateService.execute(query);
        query = null;
    
    }

    @Override
    public void deleteRecord(Integer e) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("idx", Integer.toString(e));
        updateService.execute(DELETE_QUERY.toString());
    }

    @Override
    public void deleteRecordWithUserUUID(String USER_UUID) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_uuid", USER_UUID);
        updateService.execute(DELETE_QUERY.toString());
        
    }

    @Override
    public void deleteRecordWithUSER_IDX(Integer USER_IDX) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_idx", Integer.toString(USER_IDX));
        updateService.execute(DELETE_QUERY.toString());
    }
}
