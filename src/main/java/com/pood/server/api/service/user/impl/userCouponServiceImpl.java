package com.pood.server.api.service.user.impl;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.meta.coupon.couponService;
import com.pood.server.api.service.meta.goods.goodsCouponService;
import com.pood.server.api.service.query.queryService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.api.service.time.timeService;
import com.pood.server.api.service.user.userCouponService;
import com.pood.server.api.service.user.userService;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.coupon.COUPON_STATUS;
import com.pood.server.config.meta.user.META_USER_COUPON;
import com.pood.server.dto.meta.dto_coupon_2;
import com.pood.server.dto.user.dto_user_coupon;
import com.pood.server.dto.user.dto_user_coupon_2;
import com.pood.server.object.user.vo_user_coupon;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("userCouponService")
public class userCouponServiceImpl implements userCouponService {

    Logger logger = LoggerFactory.getLogger(userCouponServiceImpl.class);

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("couponService")
    couponService couponService;

    @Autowired
    @Qualifier("goodsCouponService")
    goodsCouponService goodsCouponService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("timeService")
    timeService timeService;

    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_USER_COUPON;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    /******* 회원이 소유하고 있는 쿠폰 항목번호 반환 **************/
    public Integer getUserCouponINDEX(Integer USER_IDX, Integer COUPON_IDX) throws Exception {

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_idx", USER_IDX);
        SELECT_QUERY.buildEqual("coupon_idx", COUPON_IDX);
        SELECT_QUERY.buildEqual("status", com.pood.server.config.meta.coupon.COUPON_STATUS.AVAILABLE);

        Integer USER_COUPON_IDX = -1;

        dto_user_coupon record = (dto_user_coupon)listService.getDTOObject(SELECT_QUERY.toString(), dto_user_coupon.class);
        if(record != null)
            USER_COUPON_IDX = record.getIdx();
        record = null;

        return USER_COUPON_IDX;
    }

    @SuppressWarnings("unchecked")
    public List<vo_user_coupon> getUserCouponINDEXByUuid(String user_uuid) throws Exception {

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", user_uuid);

        List<vo_user_coupon> result = new ArrayList<vo_user_coupon>();

        List<vo_user_coupon> list = 
            (List<vo_user_coupon>)listService.getDTOList(SELECT_QUERY.toString(), vo_user_coupon.class);

        if(list != null){
            for(vo_user_coupon e : list){
                result.add(e);
            }
        }

        return result;
    }

    /***************** 회원 쿠폰 항목 번호에 해당하는 쿠폰 정보 가져오기 ************************/
    public dto_user_coupon_2 getUserCoupon(String key, Integer value) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME,
                DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual(key, value);

        dto_user_coupon_2 record = (dto_user_coupon_2)listService.getDTOObject(SELECT_QUERY.toString(), dto_user_coupon_2.class);

        Integer COUPON_IDX              = record.getCoupon_idx();
        
        dto_coupon_2 record2 = couponService.getCouponObject(COUPON_IDX);
        
        if(record2 != null)
            record.setCoupon(record2);

        /*
        if(record != null){
            Boolean isAvailable = false;
            
            Boolean isExpired = false;


            Integer COUPON_STATUS           = record.getStatus();

            String COUPON_AVAILABLE_TIME    = record.getAvailable_time();

            
            if (COUPON_STATUS == com.pood.server.config.meta.coupon.COUPON_STATUS.AVAILABLE)
                isAvailable = true;
    
            isExpired = timeService.isExpired(COUPON_AVAILABLE_TIME);

            if ((!isExpired) && isAvailable) {
                // 각 쿠폰 항목 번호에 대해서 쿠폰 오브젝트 조회
                
            }else record = null;

            COUPON_IDX      = null;
            
            COUPON_STATUS   = null;

            COUPON_AVAILABLE_TIME = null;
        }
        */

        return record;
    }
 
    /***************** 쿼리에 해당하는 쿠폰 목록 가져오기 ************************/
    @SuppressWarnings("unchecked")
    public List<dto_user_coupon_2> getCouponList(Integer user_idx) throws Exception {

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_idx", user_idx);
        SELECT_QUERY.buildEqual("status", 1);
        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        SELECT_QUERY.buildMoreThan("available_time", now);

        List<dto_user_coupon_2> result = new ArrayList<dto_user_coupon_2>();

        List<vo_user_coupon> list = 
            (List<vo_user_coupon>)listService.getDTOList(SELECT_QUERY.toString(), vo_user_coupon.class);

        if(list != null){
            for(vo_user_coupon e : list){
                dto_user_coupon_2 record = getUserCoupon("idx", e.getIdx());
                if(record != null)
                    result.add(record);
                record = null;
            }
        }

        return result;
    }

    /************* 쿠폰 매핑 정보 초기화 **********/
    public void rollbackUserCoupon(Integer uc_idx) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("status", com.pood.server.config.meta.coupon.COUPON_STATUS.AVAILABLE);
        hashMap.put("apply_cart_idx", null); //장바구니 연결되는 idx키 null세팅
        hashMap.put("used_time", null);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", uc_idx.toString());
        updateService.execute(query);
        query = null;

    }


    /************* 반품시 쿠폰 사용가능으로 돌려놓음 **********/
    public void rollbackUserCoupon2(Integer uc_idx) throws Exception {
        updateService.execute(GET_QUERY_ROLLBACK_USER_COUPON(uc_idx));
    }

    public String GET_QUERY_ROLLBACK_USER_COUPON(Integer uc_idx) throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", uc_idx);

        
        dto_user_coupon_2 record = (dto_user_coupon_2)listService.getDTOObject(SELECT_QUERY.toString(), dto_user_coupon_2.class);
        
        if(record != null){

            String AVAILABLE_TIME = record.getAvailable_time();

            String TIME_2 = timeService.addDay(AVAILABLE_TIME, 7);

            Map<String, Object> hashMap = new HashMap<String, Object>();
            hashMap.put("status",           META_USER_COUPON.POSSIBLE);
            hashMap.put("used_time",        "0000-00-00 00:00:00");
            hashMap.put("available_time",   TIME_2);

            AVAILABLE_TIME  = null;

            TIME_2          = null;
            
            record          = null;

            return queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", uc_idx.toString());
        }else return null;
        
    }


    /************* 쿠폰 적용으로 사용 변경 **********/
    public void toUserCoupon(Integer BASKET_IDX, Integer USER_COUPON_IDX) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("apply_cart_idx", BASKET_IDX);
        updateService.execute(GET_QUERY_TO_USER_COUPON(BASKET_IDX, USER_COUPON_IDX));
    }


    /********************* 장바구니에 매핑된 쿠폰 항목 번호 조회 *****************/
    public Integer getUserCouponIndex(Integer BASKET_IDX) throws Exception {

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("apply_cart_idx", BASKET_IDX);

        Integer USER_COUPON_IDX = -1;

        dto_user_coupon record = (dto_user_coupon)listService.getDTOObject(SELECT_QUERY.toString(), dto_user_coupon.class);
        if(record != null)
            USER_COUPON_IDX = record.getIdx();
        
        return USER_COUPON_IDX;
    }

    /************ 쿠폰 상태 변경 ******************/
    public void updateCouponStatus(Integer status, Integer idx) throws SQLException {
        updateService.execute(GET_QUERY_UPDATE_COUPON_STATUS(status, idx));
    }

    public String GET_QUERY_UPDATE_COUPON_STATUS(Integer status, Integer idx){
        String USED_TIME = timeService.getCurrentTime();
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("status", com.pood.server.config.meta.coupon.COUPON_STATUS.USED);
        hashMap.put("used_time", USED_TIME);
        return  queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", idx.toString());
    }


    /*************** 회원 쿠폰 번호에 해당하는 쿠폰 번호 조회 ***************/
    public Integer getUserCOUPONIDX(Integer USER_COUPON_IDX) throws Exception {

        Integer COUPON_IDX = -1;

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", USER_COUPON_IDX);

        /************** 회원이 사용하는 중복 쿠폰의 쿠폰 항목 번호 조회 ******************/
        dto_user_coupon record = (dto_user_coupon)listService.getDTOObject(SELECT_QUERY.toString(), dto_user_coupon.class);
        if(record != null)COUPON_IDX = record.getCoupon_idx();
        
        return COUPON_IDX;

    }

    /************* 기존에 장바구니에 매핑되어 있는 쿠폰 초기화 ****************/
    @SuppressWarnings("unchecked")
    public void rollbackBasketCoupon(Integer TO_BASKET_IDX) throws Exception {

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("apply_cart_idx", TO_BASKET_IDX);

        List<dto_user_coupon> list = (List<dto_user_coupon>)listService.getDTOList(SELECT_QUERY.toString(), dto_user_coupon.class);

        if(list != null){
            for(dto_user_coupon e : list){
                Integer USER_COUPON_IDX = (Integer)e.getIdx();
                Integer OVER_TYPE = (Integer)e.getOver_type();

                // 중복 쿠폰이 아니면 기존의 장바구니 쿠폰은 -1로
                if (OVER_TYPE != 1)
                    rollbackUserCoupon(USER_COUPON_IDX);
            }
        } 
    }

    /******************* 회원 쿠폰 발급 횟수 조회 ******************/
    public Integer getIssuedCount(Integer COUPON_IDX, Integer USER_IDX) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        CNT_QUERY.buildEqual("coupon_idx", COUPON_IDX);
        CNT_QUERY.buildEqual("user_idx", USER_IDX);

        Integer cnt = listService.getRecordCount(CNT_QUERY.toString());
        return cnt;
    }
  

    /******************** 장바구니 레코드 표시 여부 업데이트 *****************/
    public void updateBasketStatus(Integer BASKET_IDX, Integer STATUS) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("status", STATUS);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", BASKET_IDX.toString());
        updateService.execute(query);
        query = null;
        
    }

    /************* 특정 장바구니에 매핑되어 있는 회원 쿠폰 정보 반환 **************/
    public dto_user_coupon getUserCoupon2(Integer BASKET_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("apply_cart_idx", BASKET_IDX);
        return (dto_user_coupon)listService.getDTOObject(SELECT_QUERY.toString(), dto_user_coupon.class);
    }

    @Override
    public Integer getAVAcouponCount(Integer user_idx) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        CNT_QUERY.buildEqual("user_idx", user_idx);
        CNT_QUERY.buildEqual("status", COUPON_STATUS.AVAILABLE);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @Override
    public void insertRecord(Integer COUPON_IDX, Integer USER_IDX, Integer APPLY_CART_IDX, Integer STATUS, Integer OVER_TYPE,
        String AVAILABLE_TIME, String PUBLISH_TIME) throws Exception {

        String USER_UUID = userService.getUserUUID(USER_IDX);

        logger.info("**************** 쿠폰 발행 ***************");
        logger.info("쿠폰 항목 번호 : " + COUPON_IDX + ", 회원 UUID : " + USER_UUID + ", 사용 가능한 시간 : " + AVAILABLE_TIME);

        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("coupon_idx", COUPON_IDX);
        hashMap.put("user_idx", USER_IDX);
        hashMap.put("user_uuid", USER_UUID);
        hashMap.put("apply_cart_idx", APPLY_CART_IDX);
        hashMap.put("status", STATUS);
        hashMap.put("used_time", null);
        hashMap.put("over_type", OVER_TYPE);
        hashMap.put("publish_time", PUBLISH_TIME);
        hashMap.put("available_time", AVAILABLE_TIME);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap,
            com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        Integer result_idx = updateService.insert(query);

    }


    public void issueWelcomeCoupon(Integer USER_IDX) throws Exception {

        List<dto_coupon_2> list = couponService.getWelcomeCouponList();

        if (list != null) {
            for (dto_coupon_2 e : list) {
                String CURRENT_TIME = timeService.getCurrentTime();

                String COUPON_AVAILABLE_TIME = timeService.addDay(e.getAvailable_day());

                insertRecord(
                    e.getIdx(),                         // 웰컴 쿠폰 항목 번호
                    USER_IDX,                           // 회원 항목 번호
                    null,                                 // 장바구니 항목 번호 (매핑되는 장바구니 없음)
                    COUPON_STATUS.AVAILABLE,            // 사용가능한 상태
                    e.getOver_type(),                   // 중복쿠폰 여부
                    COUPON_AVAILABLE_TIME,              // 사용가능한 시간
                    CURRENT_TIME                        // 발급시간
                );

            }
        }

    }

    @Override
    public void deleteRecord(Integer e) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("idx", Integer.toString(e));
        updateService.execute(DELETE_QUERY.toString());
    }

    @Override
    public void remove(Integer USER_IDX) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_idx", Integer.toString(USER_IDX));
        updateService.execute(DELETE_QUERY.toString());
    }

    @Override
    public void deleteUserCouponWithUserUUID(String USER_UUID) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_uuid", USER_UUID);
        updateService.execute(DELETE_QUERY.toString());
    }

    @Override
    public String GET_QUERY_TO_USER_COUPON(Integer BASKET_IDX, Integer USER_COUPON_IDX) {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("apply_cart_idx", BASKET_IDX);
        return queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", USER_COUPON_IDX.toString());
    }
}
