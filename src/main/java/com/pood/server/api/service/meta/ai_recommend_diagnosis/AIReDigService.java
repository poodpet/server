package com.pood.server.api.service.meta.ai_recommend_diagnosis;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_ai_recommend_diagnosis;

public interface AIReDigService {


    /***************** AI 질병 목록 조회 ****************/
    public List<dto_ai_recommend_diagnosis> getAIRegDigList(pagingSet PAGING_SET) throws Exception;


    
    /**************** 전체 질병 목록 항목 개수 조회 ************/
    public Integer getTotalRecordNumber() throws SQLException;



    public dto_ai_recommend_diagnosis getRecord(Integer aI_DIG_IDX) throws Exception;



 
}
