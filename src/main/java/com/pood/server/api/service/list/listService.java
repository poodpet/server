package com.pood.server.api.service.list;

import com.pood.server.dto.dto_image_3;
import com.pood.server.dto.log.user_order.dto_log_user_order_2;
import com.pood.server.dto.meta.order.dto_order_delivery;
import java.sql.SQLException;
import java.util.List;

public interface listService {

    List<?> getDTOImageList2(String query) throws Exception;

    List<?> getDTOImageList4(String query) throws Exception;

    List<?> getDTOList(String query, Class<?> cls) throws Exception;

    Object getDTOObject(String query, Class<?> cls) throws Exception;

    List<?> getDTOListWithImage(
        String query, String image_column_name, String db_name, String table_name, Class<?> cls, Class<?> cls_image) throws Exception;

    List<?> getDTOListWithImage1(
        String query, String image_column_name, String db_name, String table_name, Class<?> cls, Class<?> cls_image) throws Exception;

    List<?> getDTOListWithImage2(
        String query, String image_column_name, String db_name, String table_name, Class<?> cls, Class<?> cls_image) throws Exception;

    List<dto_image_3> getDTOListReviewImage(String query) throws Exception;

    List<dto_log_user_order_2> getDTOListLogUserOrder(String query) throws Exception;

    List<dto_order_delivery> getDTOListOrderDelivery(String query) throws Exception;

    /*********** ResultSet을 순회하여 해당되는 칼럼이름에 매핑되는 레코드 번호 저장 **********************/
    List<Integer> getIDXList(String query) throws SQLException;

    List<Integer> getIDXList(String query, String column_name) throws SQLException;

    /************* 전체 레코드 수 반환 **********************/
    Integer getRecordCount(String query) throws SQLException;

}