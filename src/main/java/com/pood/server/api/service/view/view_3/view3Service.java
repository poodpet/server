package com.pood.server.api.service.view.view_3;

import java.util.List;

import com.pood.server.object.meta.maintab.vo_maintab_goods;

public interface view3Service {
    
    public List<vo_maintab_goods> getList() throws Exception;
    
}
