package com.pood.server.api.service.delivery_tracker;

public interface deliveryTrackerService {

	public String getTrackingRecord(String CARRIER, String TRACKING_ID) throws Exception;
}
