package com.pood.server.api.service.meta.category;

import com.pood.server.api.queryBuilder.SELECT_QUERY;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

import com.pood.server.config.DATABASE;
import com.pood.server.object.meta.vo_category_field;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.list.*;

@Service("categoryFieldService")
public class categoryFieldServiceImpl implements categoryFieldService{
    
    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("categoryService")
    categoryService categoryService;
 
    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_CATEGORY_FIELD;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @SuppressWarnings("unchecked")
    public List<vo_category_field> getList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        List<vo_category_field> list = (List<vo_category_field>)listService.getDTOList(SELECT_QUERY.toString(), vo_category_field.class);
        return list;
    }


}
