package com.pood.server.api.service.meta.psk;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_psk;
import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.record.*;
import com.pood.server.config.DATABASE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("PSKService")
public class PSKServiceImpl implements PSKService {

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    private String DEFINE_TABLE_NAME = DATABASE.TABLE_PSK;
 
    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @Override
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @SuppressWarnings("unchecked")
    public List<dto_psk> getList(pagingSet PAGING_SET, Integer pc_idx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if(pc_idx != null)
            SELECT_QUERY.buildEqual("pc_idx", pc_idx);
        SELECT_QUERY.buildASC("show_index");
        SELECT_QUERY.setPagination(PAGING_SET, false);
        return (List<dto_psk>)listService.getDTOList(SELECT_QUERY.toString(), dto_psk.class);
    }
 
}
 