package com.pood.server.api.service.meta.event_type;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_event_type;

public interface eventTypeService {

	public Integer getTotalRecordNumber() throws SQLException;

	public List<vo_event_type> getEventTypeList(pagingSet PAGING_SET) throws Exception;
 
	public vo_event_type getEventTypeObject(Integer EVENT_TYPE_IDX) throws Exception;
    
}
