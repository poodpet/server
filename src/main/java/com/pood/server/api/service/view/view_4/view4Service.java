package com.pood.server.api.service.view.view_4;

import java.util.List;

import com.pood.server.object.meta.maintab.vo_maintab_event;

public interface view4Service {

    public List<vo_maintab_event> getList() throws Exception;
    
}
