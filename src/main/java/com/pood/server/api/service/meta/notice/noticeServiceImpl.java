package com.pood.server.api.service.meta.notice;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.dto_image;
import com.pood.server.dto.meta.dto_notice;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.config.DATABASE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("noticeService")
public class noticeServiceImpl implements noticeService{
    
    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_NOTICE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    private String DEFINE_IMAGE_TABLE_NAME = DATABASE.TABLE_NOTICE_IMAGE;

    /**************** 곡물 사이즈 목록 조회 *****************/

    @SuppressWarnings("unchecked")
    public List<dto_notice> getList(pagingSet PAGING_SET) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_notice>)listService.getDTOListWithImage(
                SELECT_QUERY.toString(), "notice_idx", DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME, dto_notice.class, dto_image.class);
    }

    /**************** 곡물 사이즈 목록 항목 개수 조회 *****************/
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    }
    
}
