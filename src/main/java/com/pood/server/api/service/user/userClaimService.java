package com.pood.server.api.service.user;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.api.req.header.user.claim.hUser_claim_1;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.user.dto_user_claim;

public interface userClaimService {
    
    /************* 회원 문의 신규 등록 ****************/
    public Integer insertUserClaim(Integer USER_IDX, String USER_UUID, hUser_claim_1 header) throws SQLException;

 

    /************** 회원 인덱스에 해당하는 회원 문의 목록 조회 ******************/
    public List<dto_user_claim> getUserClaimList(pagingSet PAGING_SET, String user_uuid, Boolean isAnswer) throws Exception;




    /*************** 회원 항목 번호에 해당하는 전체 문의 개수 반환 *****************/
	public Integer getTotalRecordNumber(String user_uuid) throws SQLException;




    /***************** 회원 문의 정보 업데이트 **********************/
	public void updateUserClaim(Integer CLAIM_TYPE, String CLAIM_TEXT, String CLAIM_TITLE, Boolean IS_VISIBLE, Integer CLAIM_IDX) throws SQLException;

 

    public void deleteRecord(Integer e) throws SQLException;


 

}
