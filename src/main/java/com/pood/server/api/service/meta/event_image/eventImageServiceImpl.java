package com.pood.server.api.service.meta.event_image;

import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.record.*;
import com.pood.server.config.DATABASE;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.event.dto_event_image;
import com.pood.server.dto.meta.event.dto_event_image_2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@Service("eventImageService")
public class eventImageServiceImpl implements eventImageService {

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_EVENT_IMAGE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
    

    @Override
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<dto_event_image> getList(pagingSet PAGING_SET) throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_event_image>)listService.getDTOList(SELECT_QUERY.toString(), dto_event_image.class);
    }
 
    @SuppressWarnings("unchecked")
    public List<dto_event_image_2> getImageList(Integer EVENT_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if(EVENT_IDX != null)
            SELECT_QUERY.buildEqual("event_idx", EVENT_IDX);
        return (List<dto_event_image_2>)listService.getDTOList(SELECT_QUERY.toString(), dto_event_image_2.class);
    }
}
