package com.pood.server.api.service.meta.courier;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_courier;

public interface courierService {

	public Integer getTotalRecordNumber() throws SQLException;

	public List<dto_courier> getList(pagingSet PAGING_SET) throws Exception;
    
}
