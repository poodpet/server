package com.pood.server.api.service.meta.event_info;

import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;

import com.pood.server.api.service.list.*;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.meta.event_goods_list.*;
import com.pood.server.api.service.meta.event_image.*;
import com.pood.server.api.service.meta.event_type.*;
import com.pood.server.api.service.time.*;
import com.pood.server.config.DATABASE;
import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_event_type;
import com.pood.server.dto.meta.event.dto_event_goods_list_2;
import com.pood.server.dto.meta.event.dto_event_info;
import com.pood.server.dto.meta.event.dto_event_info_2;
import com.pood.server.dto.meta.event.dto_event_info_3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@Service("eventInfoService")
public class eventInfoServiceImpl implements eventInfoService {

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("eventImageService")
    eventImageService eventImageService;

    @Autowired
    @Qualifier("eventTypeService")
    eventTypeService eventTypeService;

    @Autowired
    @Qualifier("eventgoodsListService")
    eventgoodsListService eventgoodsListService;

    @Autowired
    @Qualifier("timeService")
    timeService timeService;

    private String DEFINE_TABLE_NAME = DATABASE.TABLE_EVENT_INFO;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @Override
    public Integer getTotalRecordNumber(Integer event_type_idx, Integer pc_id, Integer brand_idx) throws SQLException{
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);

        if(event_type_idx != null)
            CNT_QUERY.buildEqual("event_type_idx", event_type_idx);
        if(pc_id != null)
            CNT_QUERY.buildEqual("pc_id", pc_id);
        if(brand_idx != null)
            CNT_QUERY.buildEqual("brand_idx", brand_idx);

        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<dto_event_info> getEventInfoList(pagingSet PAGING_SET) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_event_info>)listService.getDTOList(SELECT_QUERY.toString(), dto_event_info.class);
    }

    @SuppressWarnings("unchecked")
    public List<dto_event_info_2> getEventList(pagingSet PAGING_SET, Integer EVENT_TYPE_IDX, Integer PC_IDX, Integer BRAND_IDX)
            throws Exception {
          
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if(EVENT_TYPE_IDX != null)
            SELECT_QUERY.buildEqual("event_type_idx", EVENT_TYPE_IDX);
        if(PC_IDX != null)
            SELECT_QUERY.buildEqual("pc_id", PC_IDX);
        if(BRAND_IDX != null)
            SELECT_QUERY.buildEqual("brand_idx", BRAND_IDX);

        SELECT_QUERY.setPagination(PAGING_SET, true);
        
        List<dto_event_info_2> list = (List<dto_event_info_2>)listService.getDTOList(SELECT_QUERY.toString(), dto_event_info_2.class);
        if(list != null){
            Iterator<dto_event_info_2> iterator = list.iterator();

            while(iterator.hasNext()){
  
                dto_event_info_2 e = iterator.next();
                Boolean inTime = false;

                inTime = timeService.isIn(e.getStartdate(), e.getEnd_date());
                EVENT_TYPE_IDX = e.getEvent_type_idx();
                if(inTime){
                    if(e.getStatus() == 1){
                        // 이벤트 타입이 null이 아닌 경우 해당 이벤트의 이벤트 타입 조회
                        if(EVENT_TYPE_IDX != null){
                            vo_event_type EVENT_TYPE = eventTypeService.getEventTypeObject(EVENT_TYPE_IDX);
                            e.setType(EVENT_TYPE);
                        }

                        // 특정 이벤트 항목에 대해서 하위 이미지 리스트 조회
                        e.setImage(eventImageService.getImageList(e.getIdx()));

                    }else iterator.remove();
                    
                }
            }

        } 

        
        return list;
    }

    @SuppressWarnings("unchecked")
    public List<dto_event_info_3> getEventList2(pagingSet PAGING_SET, Integer event_idx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", event_idx);
        SELECT_QUERY.setPagination(PAGING_SET, true);


        
        List<dto_event_info_3> result = (List<dto_event_info_3>)listService.getDTOList(SELECT_QUERY.toString(), dto_event_info_3.class);
        if(result != null){
            for(dto_event_info_3 e : result){
 
                Integer EVENT_TYPE_IDX  = e.getEvent_type_idx();
                Integer EVENT_IDX       = e.getIdx();

                // 이벤트 타입이 null이 아닌 경우 해당 이벤트의 이벤트 타입 조회
                if(EVENT_TYPE_IDX != null){
                    vo_event_type EVENT_TYPE = eventTypeService.getEventTypeObject(EVENT_TYPE_IDX);
                    e.setType(EVENT_TYPE);
                    EVENT_TYPE = null;
                }

                // 특정 이벤트 항목에 대해서 하위 이미지 리스트 조회
                e.setImage(eventImageService.getImageList(EVENT_IDX));
 
                // 특정 이벤트 항목 번호에 해당하는 굿즈 리스트 조회
                List<dto_event_goods_list_2> ITEM_LIST = eventgoodsListService.getItemList(EVENT_IDX);
                e.setItem(ITEM_LIST);
                ITEM_LIST = null;
                
                EVENT_TYPE_IDX = null;
                EVENT_IDX = null;
            }

        }

        
        return result;
    }
    
}
