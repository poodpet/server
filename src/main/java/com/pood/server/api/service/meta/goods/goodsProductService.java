package com.pood.server.api.service.meta.goods;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.meta.goods.dto_goods_product;
import com.pood.server.dto.meta.product.dto_product_1;
import com.pood.server.dto.meta.product.dto_product_4;
import com.pood.server.dto.meta.product.dto_product_5;

public interface goodsProductService {
 
    
    /********************** 거래에 매핑되어 있는 상품 정보 조회   *********************/
    public List<dto_product_4> getgoodsProductList2(Integer GOODS_IDX) throws Exception;


    /******************* 굿즈에 매핑된 상품 리스트 조회 ***********************/
    public List<Integer> getgoodsProductList(Integer GOODS_IDX) throws Exception;


	public List<dto_goods_product> getList(Integer GOODS_IDX) throws Exception;


	public List<Integer> getgoodsList(Integer PRODUCT_IDX) throws SQLException;


    public List<dto_product_5> getgoodsProductList3(Integer GOODS_IDX)throws Exception;


	public List<dto_goods_product> getList() throws Exception;

 
    public Boolean isOutOfStock(Integer GOODS_IDX) throws Exception;


    public List<String> deductQtyWithGoodsIDX(Integer GOODS_IDX, Integer PURCHASE_QUANTITY, Integer TYPE, String TEXT) throws Exception;


    public List<String> addQtyWithGoodsIDX(Integer GOODS_IDX, Integer PURCHASE_QUANTITY, Integer TYPE, String TEXT) throws Exception;
 
    public Integer getProductQTY(Integer PRODUCT_IDX) throws Exception;


    public List<Integer> getProductIDXList(Integer GOODS_IDX) throws Exception;


    public List<dto_product_1> getgoodsProductList7(Integer GOODS_IDX) throws Exception;

    public void doOutOfStock(Integer GOODS_IDX, Integer PRODUCT_IDX, Integer PRODUCT_QTY, Integer PRODUCT_QUANTITY) throws Exception;
}
