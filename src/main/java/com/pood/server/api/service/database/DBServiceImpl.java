package com.pood.server.api.service.database;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.APICall.APICallService;
import com.pood.server.api.service.list.listService;
import com.pood.server.dto.dto_column_list;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("DBService")
public class DBServiceImpl implements DBService {

    @Value("${legacy.server-ip}")
    private String serverIp;

    Logger logger = LoggerFactory.getLogger(DBServiceImpl.class);

    @Autowired
    @Qualifier("dataSource")
    private DataSource dataSource;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("APICallService")
    APICallService APICallService;

    /******************* 특정 데이터베이스 테이블의 모든 칼럼 리스트 조회 ************/
    public List<dto_column_list> getColumnList(String db_name, String table_name) throws SQLException {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        SELECT_QUERY.getColumnList(db_name, table_name);
        Connection conn = null;

        List<dto_column_list> result = new ArrayList<dto_column_list>();

        try {

            conn = dataSource.getConnection();
            QueryRunner queryRunner = new QueryRunner();
            ResultSetHandler<List<dto_column_list>> resultSetHandler = new BeanListHandler<dto_column_list>(dto_column_list.class);
            result = queryRunner.query(conn, SELECT_QUERY.toString(), resultSetHandler);
            resultSetHandler = null; 
            queryRunner = null;

        } catch (SQLException error) {
            logger.error("[ERROR]" + error.getMessage() + ", QUERY : " + SELECT_QUERY);
            errorReport(error.toString(), SELECT_QUERY.toString());
        } finally {
            conn.close();
        }

        conn = null; 

        return result;
    }
 



    public List<Object> getdtoList(String query, Class<?> cls) throws SQLException {
        Connection conn = null;

        List<Object> result = new ArrayList<Object>();

        try {

            conn = dataSource.getConnection();
            QueryRunner queryRunner = new QueryRunner();
            ResultSetHandler<List<Object>> resultSetHandler = new BeanListHandler<Object>(cls);
            result = queryRunner.query(conn, query, resultSetHandler);
            resultSetHandler = null;
            queryRunner = null;

        } catch (SQLException error) {
            logger.error("[ERROR]" + error.getMessage() + ", QUERY : " + query);
            errorReport(error.toString(), query);
        } finally {
            conn.close();
        }

        conn = null;

        return result;
    }
      
 



    /*********** 데이터베이스 SQL 구문 실행 *************/
    public void update(String query) throws SQLException {
        Connection conn = null;

        try {

            conn = dataSource.getConnection();
            conn.prepareStatement(query).executeUpdate();
            conn.commit();

        } catch (SQLException error) {
            logger.error("[ERROR]" + error.getMessage() + ", QUERY : " + query);
            errorReport(error.toString(), query);
        } finally {
            conn.close();
        }  

        conn = null;
    }

    /*********** 데이터베이스 SQL 구문 실행 *************/
    public void commit(List<String> query) throws SQLException {

        logger.info("**************** BEGIN MYSQL TRANSACTION PROCESS *****************");
        Connection conn = null;

        try {

            conn = dataSource.getConnection();

            for(String e : query){
                logger.info(e);
                conn.prepareStatement(e).executeUpdate(); 
            }
                
            conn.commit(); 
            logger.info("**********************************");
            logger.info("COMMIT SUCCESS !!!");

        } catch (SQLException error) {
            logger.error("[ERROR]" + error.getMessage() + ", QUERY : " + query);
            errorReport(error.toString(), query.toString());
        } finally {
            conn.close();
        }  

        conn = null;
    }
 

     
    
 
 

    /*************** 레코드 삽입 ********************/
    public Integer insert(String query) throws SQLException {

        Integer idx = -1;

        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
 
            ps.executeUpdate();

            conn.commit();

            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                idx = generatedKeys.getInt(1);
            }

            ps.close();

        }catch (SQLException error) {
            logger.error("[ERROR]" + error.getMessage() + ", QUERY : " + query);
            errorReport(error.toString(), query);
        }

        return idx;
    }
   



    @Override
    public Object getDTOObject(String query, Class<?> cls) throws SQLException {
        Connection conn = null;

        Object result = new Object();

        try {

            conn = dataSource.getConnection();
            QueryRunner queryRunner = new QueryRunner();
            ResultSetHandler<Object> resultSetHandler = new BeanHandler<Object>(cls);
            result = queryRunner.query(conn, query, resultSetHandler);
            resultSetHandler = null;
            queryRunner = null;

        } catch (SQLException error) {
            logger.error("[ERROR]" + error.getMessage() + ", QUERY : " + query);
            errorReport(error.toString(), query);
        } finally {
            conn.close();
        }

        conn = null;

        return result;
    }

    
 
    @Override
    public Integer getRecordCount(String query) throws SQLException {

        Connection conn = null;

        ResultSet RESULT_SET = null;
        
        Integer cnt = 0;

        try {

            conn = dataSource.getConnection();

            RESULT_SET = conn.createStatement().executeQuery(query);

            while (RESULT_SET.next())cnt += RESULT_SET.getInt("cnt");

        } catch (SQLException error) {
            logger.error("[ERROR]" + error.getMessage() + ", QUERY : " + query);
            errorReport(error.toString(), query);
        } finally {
            conn.close();
            RESULT_SET.close();
        }

        conn = null;
        
        return cnt;
    }

    @Override
    public List<Integer> getIDXList(String query) throws SQLException {

        List<Integer> list = new ArrayList<>();

        Connection conn = null;

        ResultSet RESULT_SET = null;

        try {

            conn = dataSource.getConnection();

            RESULT_SET = conn.createStatement().executeQuery(query);

            while (RESULT_SET.next()) {

                Integer idx = RESULT_SET.getInt("idx");
                if(idx != null)
                    list.add(idx);
                idx = null;
            }

        } catch (SQLException error) {
            logger.error("[ERROR]"+error.toString()+", QUERY : " + query);
            errorReport(error.toString(), query);
        } finally {
            conn.close();
            RESULT_SET.close();
        }

        conn = null;

        return list;
    }





    @Override
    public List<Integer> getIDXListWithColumnName(String query, String column_name) throws SQLException {

        List<Integer> list = new ArrayList<Integer>();

        Connection conn = null;

        ResultSet RESULT_SET = null;

        try {

            conn = dataSource.getConnection();

            RESULT_SET = conn.createStatement().executeQuery(query);

            while (RESULT_SET.next()) {

                Integer idx = RESULT_SET.getInt(column_name);
                if(idx != null)
                    list.add(idx);
                idx = null;
            }

        } catch (SQLException error) {
            logger.error("[ERROR]" + error.getMessage() + ", QUERY : " + query);
            errorReport(error.toString(), query);
        } finally {
            conn.close();
            RESULT_SET.close();
        }

        conn = null;

        return list;
    }


    public void errorReport(String error_msg, String query){
        HttpHeaders headers = new HttpHeaders();
        String url = serverIp + "/pood/admin/error/1?error_msg=" + error_msg + "&query=" + query;
        ResponseEntity<String> result = APICallService.getFast(headers, url);
        if(result != null) {
            logger.error("RESPONSE = " + result);
        }
    }
 
}
