package com.pood.server.api.service.meta.coupon;

import java.util.List;

import com.pood.server.dto.meta.dto_coupon_goods;

public interface couponGoodsService {
    
    public List<dto_coupon_goods> getList() throws Exception;

    public List<Integer> getGoodsIDXList(Integer coupon_idx) throws Exception;
    
}
