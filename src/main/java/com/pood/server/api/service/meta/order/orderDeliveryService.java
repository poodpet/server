package com.pood.server.api.service.meta.order;

import com.pood.server.api.req.header.order.exchange.OrderExchangeDelivery;
import com.pood.server.api.req.header.order.refund.hOrder_refund_delivery;
import com.pood.server.object.IMP.IMP;
import com.pood.server.object.meta.vo_order_delivery;
import com.pood.server.object.meta.vo_order_exchange_delivery;
import com.pood.server.object.meta.vo_order_refund_delivery;
import com.pood.server.object.user.vo_user_delivery_2;
import java.sql.SQLException;
import java.util.List;

public interface orderDeliveryService {
    


    /***************** 배송지 정보 등록 **********************/
    public Integer insertOrderDelivery(String EXCHANGE_NUMBER, String REFUND_NUMBER, String ZIPCODE, String ADDRESS, String ADDRESS_DETAIL, Integer REMOTE_TYPE, String RECEIVER, String NICKNAME, Integer INPUT_TYPE, String DELIVERY_MESSAGE, String PHONE_NUMBER, Integer DELIVERY_TYPE, String MEMO) throws SQLException;



    /****************** 주문 배송지 레코드 생성 ******************/
    public Integer insertOrder_Delivery(IMP IAMPORT) throws SQLException;



    /****************** 주문에 대한 배송 이미지 정보 업데이트 ***********************/
    public void updateImageOrderINDEX(Integer ORDER_IDX, Integer IMAGE_IDX)throws SQLException;
    
 
     

    public Integer insertOrderImage(String img_url) throws SQLException;


    public void insertRecord(String exchange_number, String refund_number, String order_number, Integer order_idx,
        OrderExchangeDelivery delivery, Integer delivery_type) throws SQLException;



    public void insertRecord(String exchange_number, String refund_number, String order_number, Integer order_idx, hOrder_refund_delivery delivery, Integer delivery_type) throws SQLException;



    public vo_user_delivery_2 getRecord(String user_uuid, Integer order_idx) throws Exception;



    public vo_order_delivery getRecord2(String order_number) throws Exception;



    public List<vo_order_refund_delivery> getDeliveryListWithRefundNumber(String refund_number) throws Exception;



    public List<vo_order_exchange_delivery> getDeliveryListWithExchangeNumber(String exchange_number) throws Exception;



    public String GET_QUERY_INSERT_ORDER_DELIVERY(IMP IAMPORT);
    
}
