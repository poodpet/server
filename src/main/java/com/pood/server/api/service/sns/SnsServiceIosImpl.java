package com.pood.server.api.service.sns;

import com.pood.server.api.service.APICall.APICallService;
import com.pood.server.api.service.error.errorProcService;
import com.pood.server.api.service.noti.pushService;
import com.pood.server.api.service.user.userNotiService;
import com.pood.server.api.service.user.userService;
import com.pood.server.config.meta.user.USER_NOTI;
import com.pood.server.entity.DeviceType;
import java.util.HashMap;
import lombok.RequiredArgsConstructor;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component(value = "snsServiceIos")
public class SnsServiceIosImpl implements snsService {

    Logger logger = LoggerFactory.getLogger(SnsServiceIosImpl.class);

    private final userService userService;
    private final pushService pushService;
    private final userNotiService userNotiService;
    private final errorProcService errorProcService;
    private final APICallService apiCallService;

    /**************** 메세지 발송 *************************/
    public void send(String PUSH_MESSAGE, String USER_UUID, Integer USER_IDX, Integer ALARM_TYPE, String MESSAGE_TITLE, String SCHEMA) throws Exception {

        try {

            Integer ORDER_PUSH = userService.getOrderPush(USER_UUID);

            Integer SERVICE_PUSH = userService.getServicePush(USER_UUID);

            Integer POOD_PUSH = userService.getPoodPush(USER_UUID);

            if(ALARM_TYPE == USER_NOTI.CANCEL_FINISHED){

                String  MAIN_TYPE       = "orderList";
                String  GOODS_TYPE      = "";
                String  IDX             = "";
                String  NEED_LOGIN      = "";
                String  SUB_TYPE        = "";

                SCHEMA                = "{\"main_type\":\""+MAIN_TYPE+"\",\"goods_type\":\""+ GOODS_TYPE+"\",\"idx\":\""+ IDX +"\",\"needLogin\":\""+ NEED_LOGIN+"\",\"sub_type\":\""+ SUB_TYPE+"\"}";

            }

            Boolean chk = checkUserNotification(USER_UUID, ALARM_TYPE, ORDER_PUSH, SERVICE_PUSH, POOD_PUSH);
            if (chk) {
                String deviceKey = userService.getDeviceKey(USER_IDX);

                HashMap<String, Object> body = new HashMap<String, Object>();
                HashMap<String, Object> notification = new HashMap<String, Object>();

                JSONParser parser = new JSONParser();
                Object obj = parser.parse( SCHEMA );
                JSONObject jsonObj = (JSONObject) obj;
                body.put("to", deviceKey);
                body.put("priority", "high");
                body.put("notification", notification);
                notification.put("title", MESSAGE_TITLE);
                notification.put("body", PUSH_MESSAGE);
                body.put("data", jsonObj);

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                headers.set("Authorization", "key=AAAAxHrR7tU:APA91bH7iUK5WDy_rfAkiiWs5IEXFirlH0nw2LAFbViRx8hQBfVmh4Ho3vFR6HcwaWTEU96-S9ussTysCm02wOqTdSO5tssLdnFoDp2_nrfel8n_ULmECCdjKq1UxEy-RNpxyIdXpwL6");

                String url = "https://fcm.googleapis.com/fcm/send";
                logger.info("호출 주소 : " + url);
                ResponseEntity<String> http_response = apiCallService.post2(MediaType.APPLICATION_JSON, headers, body, url);
                logger.info(http_response.toString());


                // PUSH 전송 후 PUSH 로그 생성
                pushService.insertRecord(USER_IDX, USER_UUID, ALARM_TYPE, MESSAGE_TITLE, PUSH_MESSAGE, SCHEMA, "");


                // PUSH 전송 후 앱 알림
                userNotiService.insertRecord(USER_IDX, USER_UUID, 0, MESSAGE_TITLE, PUSH_MESSAGE, SCHEMA, "", ALARM_TYPE);

            }
        } catch (Exception e) {
            logger.error("[ERROR]" + e.getMessage());
            errorProcService.insertRecord("ADMIN_PUSH", e.getStackTrace()[0].toString(), e.getMessage());
        }

    }

    @Override
    public DeviceType getType() {
        return DeviceType.IOS;
    }
}
