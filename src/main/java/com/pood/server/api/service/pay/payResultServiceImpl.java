package com.pood.server.api.service.pay;

import com.pood.server.api.service.delivery.remoteDeliveryService;
import com.pood.server.api.service.error.errorOrderFailService;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.log.logUserOrderBasketService;
import com.pood.server.api.service.log.logUserOrderService;
import com.pood.server.api.service.log.logUserSavePointService;
import com.pood.server.api.service.log.logUserUsePointService;
import com.pood.server.api.service.meta.coupon.couponService;
import com.pood.server.api.service.meta.goods.goodsCouponService;
import com.pood.server.api.service.meta.goods.goodsProductService;
import com.pood.server.api.service.meta.goods.goodsService;
import com.pood.server.api.service.meta.order.OrderService;
import com.pood.server.api.service.meta.order.orderBasketService;
import com.pood.server.api.service.meta.order.orderDeliveryService;
import com.pood.server.api.service.meta.point.pointService;
import com.pood.server.api.service.meta.promotion.promotionGroupGoodsService;
import com.pood.server.api.service.noti.pushService;
import com.pood.server.api.service.pood_point.poodPointService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.api.service.sns.snsService;
import com.pood.server.api.service.time.timeService;
import com.pood.server.api.service.user.userBasketService;
import com.pood.server.api.service.user.userCouponService;
import com.pood.server.api.service.user.userService;
import com.pood.server.api.service.user.userTokenService;
import com.pood.server.config.meta.META_LOG_PRODUCT_HISTORY;
import com.pood.server.config.meta.coupon.COUPON_STATUS;
import com.pood.server.config.meta.order.ORDER_STATUS;
import com.pood.server.config.meta.order.ORDER_STATUS_MESSAGE;
import com.pood.server.config.meta.point.META_POINT_TYPE;
import com.pood.server.config.meta.point.POINT_SAVE_STATUS;
import com.pood.server.config.meta.point.POINT_USE_STATUS;
import com.pood.server.dto.meta.order.dto_order;
import com.pood.server.object.IMP.IMP;
import com.pood.server.object.IMP.IMP_RESULT_ORDER;
import com.pood.server.object.IMP.IMP_SHOP;
import com.pood.server.object.meta.vo_goods_3;
import com.pood.server.object.meta.vo_order_basket;
import com.pood.server.object.meta.vo_promotion_group_goods;
import com.pood.server.object.resp.resp_pay_result;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("payResultService")
public class payResultServiceImpl implements payResultService{

    Logger logger = LoggerFactory.getLogger(payResultServiceImpl.class);

    @Autowired
    @Qualifier("orderBasketService")
    public orderBasketService orderBasketService;

    @Autowired
    @Qualifier("timeService")
    timeService timeService;

    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;

    @Autowired
    @Qualifier("goodsCouponService")
    goodsCouponService goodsCouponService;

    @Autowired
    @Qualifier("goodsProductService")
    goodsProductService goodsProductService;

    @Autowired
    @Qualifier("couponService")
    couponService couponService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("orderDeliveryService")
    orderDeliveryService orderDeliveryService;
    
    @Autowired
    @Qualifier("orderService")
    OrderService orderService;

    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("userCouponService")
    userCouponService userCouponService;

    @Autowired
    @Qualifier("userBasketService")
    userBasketService userBasketService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("logUserOrderService")
    logUserOrderService logUserOrderService;

    @Autowired
    @Qualifier("userTokenService")
    userTokenService userTokenService;

    @Autowired
    @Qualifier("pushService")
    public pushService pushService;

    @Autowired
    @Qualifier("snsService")
    public snsService snsService;

    @Autowired
    @Qualifier("logUserSavePointService")
    logUserSavePointService logUserSavePointService;

    @Autowired
    @Qualifier("logUserUsePointService")
    logUserUsePointService logUserUsePointService;
   
    @Autowired
    @Qualifier("pointService")
    pointService pointService;

    @Autowired
    @Qualifier("retreieveService")
    retreieveService retreieveService;

    @Autowired
    @Qualifier("poodPointService")
    poodPointService poodPointService;

    @Autowired
    @Qualifier("remoteDeliveryService")
    remoteDeliveryService remoteDeliveryService;

    @Autowired
    @Qualifier("logUserOrderBasketService")
    logUserOrderBasketService logUserOrderBasketService;


    @Autowired
    @Qualifier("errorOrderFailService")
    errorOrderFailService errorOrderFailService;

    @Autowired
    @Qualifier("promotionGroupGoodsService")
    promotionGroupGoodsService promotionGroupGoodsService;



    /****************** 주문 정보 삽입 **********************/
    public Integer insertIMPorder(IMP IAMPORT) throws Exception {

        List<IMP_SHOP> SHOPPINGBAG_DATA = IAMPORT.order.getShoppingbag_data();
        

        
        Integer ORDER_IDX = orderService.insertOrder(IAMPORT);

        if(ORDER_IDX == -1)return -1;


        // 주문 레코드 생성 
        IAMPORT.SET_ORDER_IDX(ORDER_IDX);



        List<String> transaction_process = new ArrayList<String>();


        /******************************
        *
        *   (1) 주문 배송지 레코드 생성
        *   (2) 주문 히스토리 레코드 생성
        *   (3) 쿠폰 장바구니 번호와 매핑
        *   (4) 결제 당시의 장바구니 정보 저장
        *
        *******************************/
        

        String process_1 = orderDeliveryService.GET_QUERY_INSERT_ORDER_DELIVERY(IAMPORT);

        String process_2 = logUserOrderService.GET_QUERY_INSERT_ORDER_HISTORY(
            IAMPORT.ORDER_IDX, ORDER_STATUS.ORDER_READY, IAMPORT.MERCHANT_UID, IAMPORT.MERCHANT_UID,  IAMPORT.USER_UUID, -1, -1);


        transaction_process.add(process_1);

        transaction_process.add(process_2);



        for(IMP_SHOP e1 : SHOPPINGBAG_DATA){
            
            
            // 회원 쿠폰 장바구니 항목 번호 저장 
            Integer USER_COUPON_IDX = userCouponService.getUserCouponINDEX(IAMPORT.USER_IDX, e1.getCoupon_idx());


            String process_3 = userCouponService.GET_QUERY_TO_USER_COUPON(e1.getIdx(), USER_COUPON_IDX);

            vo_promotion_group_goods promotionGoodsInfo = promotionGroupGoodsService.getPromotionGoodsInfo(e1.getPr_code(), e1.getgoods_idx());
            Integer pr_discount_rate = promotionGoodsInfo.getPr_discount_rate();
            String process_4 = orderBasketService.GET_QUERY_INSERT_BASKET_DATA(e1, IAMPORT.ORDER_IDX, IAMPORT.MERCHANT_UID, 0, pr_discount_rate);

            String process_5 = userBasketService.GET_QUERY_DELETE_BASKET(e1.getIdx());

            transaction_process.add(process_3);

            transaction_process.add(process_4);
            
            transaction_process.add(process_5);

            process_3 = null;

            process_4 = null;

            process_5 = null;


        }

        process_1 = null;

        process_2 = null;
 

        updateService.commit(transaction_process);


        return 200;
    }

       




    // 결제 결과 반환
    public resp_pay_result GET_PAYMENT_RESULT(String IMP_UID, String MERCHANT_UID) throws Exception{
        
   

        Integer ORDER_IDX               =   0;

        Boolean VALIDATION_ORDER_STATUS =   true;

         
        dto_order RESULT_ORDER = orderService.getOrder(MERCHANT_UID);

        Integer OVER_COUPON_IDX = RESULT_ORDER.getOver_coupon_idx();
 
        String USER_UUID            = RESULT_ORDER.getUser_uuid();





        // 주문 상태가 정상인지 체크
        IMP_RESULT_ORDER IMP_RESULT_ORDER = orderService.getOrderInfo(MERCHANT_UID);            
        ORDER_IDX = IMP_RESULT_ORDER.getORDER_IDX();

        VALIDATION_ORDER_STATUS = IMP_RESULT_ORDER.getVALIDATION_ORDER_STATUS();
    






        // 주문 상태가 주문 대기가 아닌 경우 중복 결제 방지 
        if(!VALIDATION_ORDER_STATUS)
            return GET_PAY_AFTER_RESULT(MERCHANT_UID, false, "이미 결제가 이루어진 거래입니다. 주문 번호 : "+MERCHANT_UID,  IMP_UID);


        // 구매시 사용 포인트 차감
        Integer USED_POINT = RESULT_ORDER.getUsed_point();

        if(USED_POINT !=0){
                
            // 소진 적립금 로그 처리
            Integer POINT_RESULT = poodPointService.usePoint(
                    MERCHANT_UID,
                    USER_UUID, 
                    POINT_USE_STATUS.USE_COMPLETE,      // 포인트 사용
                    MERCHANT_UID,                       // 주문번호
                    RESULT_ORDER.getUsed_point()        // 포인트 사용
                    );      
            
            if(POINT_RESULT == -2){
                retreieveService.CANCEL_ALL(MERCHANT_UID,   "적립금 부족 환불 처리");                          

                return GET_PAY_AFTER_RESULT(MERCHANT_UID, false, "소진하고자 하는 적립금이 부족합니다. 소진하고자 하는 적립금 : " +USED_POINT,  IMP_UID);
            }

        }



 
        List<String> transaction_process = new ArrayList<String>();


        /************************************
        *
        *   (1) 주문 상태 업데이트  
        *   (2) 주문 성공 히스토리 생성
        *   (3) 회원 전체 쿠폰 사용 후 상태 변경
        *   (4) 결제 완료 후 상품 재고 감소
        *   (5) 장바구니 항목을 주문 번호와 매핑 시킴
        *   (6) 해당 장바구니 안 보임 상태로 변경
        *   (7) 장바구니 삽입 로그 생성
        *   (8) 장바구니에 매핑되어 있는 모든 단품 쿠폰 사용 처리
        *   (9) 구매 적립금 적립예정으로 지급
        *
        ***************************************/

        String process_1 = orderService.GET_QUERY_UPDATE_ORDER_STATUS(ORDER_STATUS.ORDER_SUCCESS, ORDER_IDX);
         
        String process_2 = logUserOrderService.GET_QUERY_INSERT_ORDER_HISTORY(
            ORDER_IDX, ORDER_STATUS.ORDER_SUCCESS, ORDER_STATUS_MESSAGE.PAYMENT_SUCCESS, MERCHANT_UID, USER_UUID, -1, -1);
         
        String process_3 = userCouponService.GET_QUERY_UPDATE_COUPON_STATUS(COUPON_STATUS.USED, OVER_COUPON_IDX);



        transaction_process.add(process_1);
        transaction_process.add(process_2);
        transaction_process.add(process_3);

                


        // 주문에 매핑된 장바구니 정보 조회 
        List<vo_order_basket> ORDER_BASKET_LIST = orderBasketService.getOrderBasketList(ORDER_IDX);

        for(vo_order_basket e : ORDER_BASKET_LIST){
 
            // 장바구니 항목에 해당하는 굿즈 정보 조회
            vo_goods_3 BASKET_GOODS = goodsService.GET_GOODS_OBJECT_1(e.getGoods_idx());
                
                 
            Integer GOODS_IDX = BASKET_GOODS.getIdx();
                
            Integer PURCHASE_QUANTITY = e.getQuantity();



            List<String> process_4 = goodsProductService.deductQtyWithGoodsIDX(
                GOODS_IDX, PURCHASE_QUANTITY, META_LOG_PRODUCT_HISTORY.TYPE_OUT, META_LOG_PRODUCT_HISTORY.ORDER_SUCCESS + "[" + MERCHANT_UID+ "]");
 
        
            String process_5 = userBasketService.GET_QUERY_DELETE_BASKET(e.getBasket_idx());

            String process_7 = logUserOrderBasketService.GET_QUERY_INSERT_RECORD(USER_UUID, MERCHANT_UID, GOODS_IDX, PURCHASE_QUANTITY);

            
            transaction_process.addAll(process_4);
            transaction_process.add(process_5);
            transaction_process.add(process_7);

            process_4 = null;

            process_5 = null;

            process_7 = null;

            BASKET_GOODS = null;

            GOODS_IDX = null;

            PURCHASE_QUANTITY = null;

        }

        

        List<String> process_8 = orderBasketService.updateBasketCoupon(ORDER_IDX);

        List<String> process_9 = poodPointService.issuePoint(MERCHANT_UID, -1, USER_UUID, META_POINT_TYPE.PURCHASE,      POINT_SAVE_STATUS.TO_BE_SAVED);       

        transaction_process.addAll(process_8);
        transaction_process.addAll(process_9);



        updateService.commit(transaction_process);

        transaction_process = null;

        process_1 = null;

        process_2 = null;

        process_3 = null;

        process_8 = null;

        process_9 = null;


        return GET_PAY_AFTER_RESULT(MERCHANT_UID, true, "success.", IMP_UID);
    }

    
 

    public resp_pay_result GET_PAY_AFTER_RESULT(String order_number, Boolean isSuccess, String msg, String IMP_UID) throws SQLException{
        logger.info("결과 : " + msg);
        resp_pay_result result = new resp_pay_result();
        result.setIsSuccess(isSuccess);
        result.setMsg(msg);
        if(!isSuccess)errorOrderFailService.insertRecord(order_number, IMP_UID, msg, 1);
        return result;
    }
 
}
