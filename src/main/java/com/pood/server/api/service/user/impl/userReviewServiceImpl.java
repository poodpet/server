package com.pood.server.api.service.user.impl;

import com.pood.server.api.file.uploader.StorageService;
import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.meta.goods.goodsProductService;
import com.pood.server.api.service.meta.goods.goodsService;
import com.pood.server.api.service.meta.image.imageService;
import com.pood.server.api.service.meta.product.productService;
import com.pood.server.api.service.meta.promotion.promotionGroupGoodsService;
import com.pood.server.api.service.query.queryService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.api.service.user.userPetService;
import com.pood.server.api.service.user.userReviewClapService;
import com.pood.server.api.service.user.userReviewService;
import com.pood.server.api.service.user.userService;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.user.USER_REVIEW_IMAGE;
import com.pood.server.dto.dto_image_2;
import com.pood.server.dto.dto_image_3;
import com.pood.server.dto.user.dto_user_1;
import com.pood.server.dto.user.review.dto_user_review;
import com.pood.server.dto.user.review.dto_user_review_4;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserReviewBlock;
import com.pood.server.object.meta.vo_goods_3;
import com.pood.server.object.pagingSet;
import com.pood.server.object.resp.resp_product_rating;
import com.pood.server.object.resp.resp_user_pet;
import com.pood.server.object.user.vo_user;
import com.pood.server.object.user.vo_user_review_image;
import com.pood.server.service.UserReviewBlockSeparate;
import com.pood.server.service.UserReviewSueSeparate;
import com.pood.server.service.UserSeparate;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("userReviewService")
public class userReviewServiceImpl implements userReviewService {


    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("userReviewClapService")
    userReviewClapService userReviewClapService;

    @Autowired
    @Qualifier("userPetService")
    userPetService userPetService;

    @Autowired
    @Qualifier("imageService")
    imageService imageService;

    @Autowired
    @Qualifier("goodsProductService")
    goodsProductService goodsProductService;

    @Autowired
    @Qualifier("productService")
    productService productService;

    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;


    @Autowired
    @Qualifier("promotionGroupGoodsService")
    promotionGroupGoodsService promotionGroupGoodsService;


    @Autowired
    @Qualifier("userReviewService")
    userReviewService userReviewService;


    @Autowired
    @Qualifier("storageService")
    StorageService storageService;


    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    @Autowired
    UserReviewBlockSeparate userReviewBlockSeparate;

    @Autowired
    UserReviewSueSeparate userReviewSueSeparate;

    @Autowired
    UserSeparate userSeparate;

    private String DEFINE_TABLE_NAME = DATABASE.TABLE_USER_REVIEW;

    private String DEFINE_IMAGE_TABLE_NAME = DATABASE.TABLE_USER_REVIEW_IMAGE;

    private String DEFINE_DB_NAME = DATABASE.getDBName(DEFINE_TABLE_NAME);

    /***************** PRODUCT_IDX, USER_UUID에 해당하는 리뷰 목록 조회 ****************/
    @SuppressWarnings("unchecked")
    public List<dto_user_review_4> getReviewList(pagingSet PAGING_SET, Integer product_idx,
        String user_uuid) throws Exception {

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);

        List<dto_user_review_4> result = new ArrayList<dto_user_review_4>();
        List<Integer> REVIEW_CLAP_LIST = null;
        Integer user_idx = null;

        if (user_uuid != null) {
            user_idx = userService.getUserIDX(user_uuid);
        }
        List<Integer> reviewIdxList = new ArrayList<>();
        if (user_idx != null) {
            REVIEW_CLAP_LIST = userReviewClapService.getReviewClapList(user_idx);
            UserInfo userInfo = userSeparate.getUserByUUID(user_uuid);
            List<UserReviewBlock> userReviewBlocks = userReviewBlockSeparate.getUserReviewBlockByUserInfo(userInfo);
            reviewIdxList.addAll(userReviewBlocks.stream()
                .map(UserReviewBlock::getReviewIdx)
                .collect(Collectors.toList()));

        }

        reviewIdxList.addAll(userReviewSueSeparate.getCheckingReviewIdx());
        if(!reviewIdxList.isEmpty()){
            SELECT_QUERY.buildNotIn("idx", reviewIdxList);
        }
        if (product_idx != null) {
            SELECT_QUERY.buildEqual("product_idx", product_idx);
        }
        SELECT_QUERY.buildEqual("isVisible", com.pood.server.config.meta.user.USER_REVIEW.DEFAULT);

        SELECT_QUERY.setPagination(PAGING_SET, true);

        List<dto_user_review_4> list = (List<dto_user_review_4>) listService.getDTOList(
            SELECT_QUERY.toString(), dto_user_review_4.class);

        if (list != null) {
            for (dto_user_review_4 e : list) {

                Integer USER_IDX = e.getUser_idx();

                Integer REVIEW_IDX = e.getIdx();

                Integer RC_CNT = userReviewClapService.getReviewClapCount(REVIEW_IDX);

                List<dto_image_3> REVIEW_IMAGE_LIST = getUserReviewImageList(REVIEW_IDX);

                e.setImage(REVIEW_IMAGE_LIST);

                if (user_uuid != null) {
                    if (REVIEW_CLAP_LIST.contains(REVIEW_IDX)) {
                        e.setIsClap(1);
                    } else {
                        e.setIsClap(0);
                    }
                }

                HashMap<String, Object> e2 = new HashMap<String, Object>();
                String userBaseImg = "";
                Integer pet_idx = e.getPet_idx();
                resp_user_pet record3 = new resp_user_pet();
                if (!pet_idx.equals(0)) {
                    List<resp_user_pet> record2 = userPetService.getUserPetList(USER_IDX, pet_idx);
//                    resp_user_pet record3 = new resp_user_pet();
                    for (resp_user_pet e1 : record2) {
                        record3 = e1;
                    }
                    e.setPet_info(record3);
                    e.setUser_info(e2);
                }
                vo_user user = userService.getUser("idx", USER_IDX.toString());
                dto_user_1 userRecord = userService.getUserRecord(user.getUser_uuid(), 0);
                e2.put("nickname", userRecord.getUser_nickname());
                userBaseImg = userService.getUserBaseImg(userRecord.getUser_base_image_idx());
                e2.put("userBaseImg", userBaseImg);

                e.setUser_info(e2);
                e.setPet_info(record3);
                e.setRc_cnt(RC_CNT);

                RC_CNT = null;

                result.add(e);
            }
        }

        return result;
    }


    /************** 회원이 해당 주문 번호와 굿즈에 대해서 리뷰를 작성했는지 확인 *************/
    @SuppressWarnings("unchecked")
    public Boolean isReviewed(String ORDER_NUMBER, Integer GOODS_IDX, Integer USER_IDX)
        throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_idx", USER_IDX);
        SELECT_QUERY.buildEqual("order_number", ORDER_NUMBER);
        SELECT_QUERY.buildEqual("goods_idx", GOODS_IDX);

        Boolean isReviewed = false;
        List<dto_user_review> list = (List<dto_user_review>) listService.getDTOList(
            SELECT_QUERY.toString(), dto_user_review.class);
        if (!list.isEmpty()) {
            isReviewed = true;
        }
        return isReviewed;
    }

    /************** 회원이 해당 주문 번호와 굿즈에 대해서 리뷰를 작성했는지 확인 *************/
    @SuppressWarnings("unchecked")
    public Boolean isReviewed(String ORDER_NUMBER, Integer GOODS_IDX, Integer USER_IDX,
        Integer PRODUCT_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_idx", USER_IDX);
        SELECT_QUERY.buildEqual("order_number", ORDER_NUMBER);
        SELECT_QUERY.buildEqual("goods_idx", GOODS_IDX);
        SELECT_QUERY.buildEqual("product_idx", PRODUCT_IDX);

        Boolean isReviewed = false;
        List<dto_user_review> list = (List<dto_user_review>) listService.getDTOList(
            SELECT_QUERY.toString(), dto_user_review.class);
        if (!list.isEmpty()) {
            isReviewed = true;
        }
        return isReviewed;
    }


    /**************** 회원 리뷰를 신규로 등록합니다. ************************/
    public Integer InsertUserReview(Integer USER_IDX, Integer PET_IDX, Integer GOODS_IDX,
        Integer RATING,
        String REVIEW_TEXT, String ORDER_NUMBER, Integer PRODUCT_IDX, String GOODS_NAME)
        throws Exception {

        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_idx", USER_IDX);
        hashMap.put("pet_idx", PET_IDX);
        hashMap.put("goods_idx", GOODS_IDX);
        hashMap.put("rating", RATING);
        hashMap.put("review_text", REVIEW_TEXT);
        hashMap.put("update_count", 1);
        hashMap.put("isDelete", com.pood.server.config.meta.user.USER_REVIEW.NOT_VISIBLE);
        hashMap.put("isVisible", com.pood.server.config.meta.user.USER_REVIEW.DEFAULT);
        hashMap.put("order_number", ORDER_NUMBER);
        hashMap.put("product_idx", PRODUCT_IDX);
        hashMap.put("goods_name", GOODS_NAME);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap,
            com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        // 리뷰 등록시 평점도 업데이트
        updateGoodsReviewRating(GOODS_IDX);

        return result_idx;
    }


    /****************** 회원 리뷰 이미지를 신규로 업데이트합니다. *****************/
    public void updateUserReviewImage(Integer REVIEW_IDX, String ORDER_NUMBER,
        Integer IMAGE_SEQUENCE, Integer IMAGE_IDX) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("review_idx", REVIEW_IDX);
        hashMap.put("order_number", ORDER_NUMBER);
        hashMap.put("visible", USER_REVIEW_IMAGE.DEFAULT);
        hashMap.put("seq", IMAGE_SEQUENCE);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME, hashMap,
            "idx", IMAGE_IDX.toString());
        updateService.execute(query);
        query = null;

    }


    /*************** 리뷰 항목번호에 해당하는 이미지 개수 조회 ****************/
    public Integer getUserReviewImageCount(Integer REVIEW_IDX) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME);
        CNT_QUERY.buildEqual("review_idx", REVIEW_IDX);
        return listService.getRecordCount(CNT_QUERY.toString());
    }


    /***************** 회원 리뷰를 업데이트 합니다. *************************/
    public void updateUserReview(Integer REVIEW_IDX, Integer RATING, String REVIEW_TEXT,
        Integer UPDATE_COUNT,
        Boolean IS_DELETE, Integer IS_VISIBLE) throws Exception {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("rating", RATING);
        hashMap.put("review_text", REVIEW_TEXT);
        hashMap.put("update_count", UPDATE_COUNT);
        hashMap.put("isDelete", IS_DELETE);
        hashMap.put("isVisible", IS_VISIBLE);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap,
            "idx", REVIEW_IDX.toString());
        updateService.execute(query);
        query = null;

        // 리뷰 업데이트시 평점도 업데이트
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", REVIEW_IDX);
        dto_user_review record = (dto_user_review) listService.getDTOObject(SELECT_QUERY.toString(),
            dto_user_review.class);

        Integer GOODS_IDX = record.getGoods_idx();

        updateGoodsReviewRating(GOODS_IDX);

        GOODS_IDX = null;

        record = null;
    }

    /******************** 회원 항목 번호에 해당하는 리뷰 목록 조회 *********************/
    @SuppressWarnings("unchecked")
    public List<dto_user_review> getUserReviewList(Integer USER_IDX) throws Exception {

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_idx", USER_IDX);

        List<dto_user_review> list = (List<dto_user_review>) listService.getDTOList(
            SELECT_QUERY.toString(), dto_user_review.class);

        if (list != null) {
            Iterator<dto_user_review> iterator = list.iterator();

            while (iterator.hasNext()) {

                dto_user_review record = iterator.next();

                Integer REVIEW_IDX = record.getIdx();
                Integer VISIBLE_STATUS = record.getIsVisible();
                Integer REVIEW_CLAP_COUNT = userReviewClapService.getReviewClapCount(REVIEW_IDX);

                List<dto_image_3> REVIEW_IMAGE_LIST = getUserReviewImageList(REVIEW_IDX);

                if (record != null) {

                    Integer GOODS_IDX = record.getGoods_idx();
                    List<dto_image_2> GOODS_IMAGE = goodsService.getGoodsImageList2(GOODS_IDX);
                    vo_goods_3 vo_goods_3 = goodsService.GET_GOODS_OBJECT_1(GOODS_IDX);

                    List<Integer> PRODUCT_IDX_LIST = goodsProductService.getProductIDXList(
                        GOODS_IDX);

                    List<dto_image_2> PRODUCT_IMAGE = new ArrayList<dto_image_2>();

                    if (PRODUCT_IDX_LIST != null) {
                        for (Integer e2 : PRODUCT_IDX_LIST) {
                            List<dto_image_2> tmp = productService.getProductImageList(e2);
                            tmp = tmp.stream()
                                .filter(x -> x.getType().equals(0) && x.getPriority().equals(0))
                                .collect(Collectors.toList());
                            PRODUCT_IMAGE.addAll(tmp);
                        }
                    }

                    record.setGoods_image(GOODS_IMAGE);

                    record.setProduct_image(PRODUCT_IMAGE);

                    String DISPLAY_TYPE = goodsService.getDisplayType(GOODS_IDX);

                    record.setDisplay_type(DISPLAY_TYPE);
                    record.setClap_cnt(REVIEW_CLAP_COUNT);
                    record.setImage(REVIEW_IMAGE_LIST);
                    record.setGoods_pc_idx(vo_goods_3.getPc_idx());
                }

                // 보이는 리뷰에 대해서만 목록에서 내려줄 껏. 
                if (VISIBLE_STATUS == com.pood.server.config.meta.user.USER_REVIEW.NOT_VISIBLE) {
                    iterator.remove();
                }

            }


        }

        return list;

    }

    /****************** 특정 리뷰에 대해서 이미지를 조회 *********************/
    public List<dto_image_3> getUserReviewImageList(Integer REVIEW_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME);
        SELECT_QUERY.buildEqual("review_idx", REVIEW_IDX);
        return (List<dto_image_3>) listService.getDTOListReviewImage(SELECT_QUERY.toString());
    }

    /******************* 전체 리뷰 목록 개수 조회 ************************/
    public Integer getTotalRecordNumber(Integer PRODUCT_IDX) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        CNT_QUERY.buildEqual("product_idx", PRODUCT_IDX);
        return listService.getRecordCount(CNT_QUERY.toString());
    }


    /***************** 특정 항목 번호에 대해서 실제 ROW NUMBER를 반환 ***************/
    public Integer getRowNumber(String recordbirth, Integer PRODUCT_IDX) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if (PRODUCT_IDX != null) {
            CNT_QUERY.buildEqual("product_idx", PRODUCT_IDX);
        }
        CNT_QUERY.buildMoreThan("recordbirth", recordbirth);
        CNT_QUERY.buildReverseOrder("recordbirth");
        Integer ROW_NUMBER = listService.getRecordCount(CNT_QUERY.toString());
        return ROW_NUMBER;
    }


    /*
     ******************** 리뷰의 보임 : 안보임 상태를 업데이트 합니다. **********************/
    public void updateReviewVisibleStatus(Integer REVIEW_IDX, Integer REVIEW_STATUS)
        throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("visible", REVIEW_STATUS);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap,
            "idx", REVIEW_IDX.toString());
        updateService.execute(query);
        query = null;
    }

    /**************** 특정 상품 항목에 대해서 평점별 카운트 수 조회 *******************/
    @SuppressWarnings("unchecked")
    public resp_product_rating getProductRating(Integer PRODUCT_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("product_idx", PRODUCT_IDX);

        //DecimalFDTOat df2 = new DecimalFDTOat("#.##");

        resp_product_rating resp = new resp_product_rating();

        Integer TOTAL_RATING = 0;

        Integer RATING_1_COUNT = 0;
        Integer RATING_2_COUNT = 0;
        Integer RATING_3_COUNT = 0;
        Integer RATING_4_COUNT = 0;
        Integer RATING_5_COUNT = 0;
        Integer TOTAL_REVIEW_COUNT = 0;

        List<dto_user_review> list = (List<dto_user_review>) listService.getDTOList(
            SELECT_QUERY.toString(), dto_user_review.class);

        if (list != null) {
            for (dto_user_review e : list) {
                Integer RATING = e.getRating();

                if (RATING == 1) {
                    RATING_1_COUNT++;
                } else if (RATING == 2) {
                    RATING_2_COUNT++;
                } else if (RATING == 3) {
                    RATING_3_COUNT++;
                } else if (RATING == 4) {
                    RATING_4_COUNT++;
                } else if (RATING == 5) {
                    RATING_5_COUNT++;
                }

                TOTAL_REVIEW_COUNT++;
                TOTAL_RATING += RATING;
            }

        }

        Double RATING_1_RATIO = (double) 0.0;
        Double RATING_2_RATIO = (double) 0.0;
        Double RATING_3_RATIO = (double) 0.0;
        Double RATING_4_RATIO = (double) 0.0;
        Double RATING_5_RATIO = (double) 0.0;
        Double AVERAGE_RATING = (double) 0.0;

        if (TOTAL_REVIEW_COUNT != 0) {
            RATING_1_RATIO = (RATING_1_COUNT / (double) TOTAL_REVIEW_COUNT) * 100;
        }
        if (TOTAL_REVIEW_COUNT != 0) {
            RATING_2_RATIO = (RATING_2_COUNT / (double) TOTAL_REVIEW_COUNT) * 100;
        }
        if (TOTAL_REVIEW_COUNT != 0) {
            RATING_3_RATIO = (RATING_3_COUNT / (double) TOTAL_REVIEW_COUNT) * 100;
        }
        if (TOTAL_REVIEW_COUNT != 0) {
            RATING_4_RATIO = (RATING_4_COUNT / (double) TOTAL_REVIEW_COUNT) * 100;
        }
        if (TOTAL_REVIEW_COUNT != 0) {
            RATING_5_RATIO = (RATING_5_COUNT / (double) TOTAL_REVIEW_COUNT) * 100;
        }

        if (TOTAL_REVIEW_COUNT != 0) {
            AVERAGE_RATING = (TOTAL_RATING / (double) TOTAL_REVIEW_COUNT);
        }

        Double DOUBLE_RATING_1_RATIO = (double) Double.valueOf(
            String.format("%1.2f", RATING_1_RATIO));

        Double DOUBLE_RATING_2_RATIO = (double) Double.valueOf(
            String.format("%1.2f", RATING_2_RATIO));

        Double DOUBLE_RATING_3_RATIO = (double) Double.valueOf(
            String.format("%1.2f", RATING_3_RATIO));

        Double DOUBLE_RATING_4_RATIO = (double) Double.valueOf(
            String.format("%1.2f", RATING_4_RATIO));

        Double DOUBLE_RATING_5_RATIO = (double) Double.valueOf(
            String.format("%1.2f", RATING_5_RATIO));

        Double DOUBLE_AVERAGE_RATING = (double) Double.valueOf(
            String.format("%1.2f", AVERAGE_RATING));

        resp.setRating_1_cnt((int) RATING_1_COUNT);
        resp.setRating_2_cnt((int) RATING_2_COUNT);
        resp.setRating_3_cnt((int) RATING_3_COUNT);
        resp.setRating_4_cnt((int) RATING_4_COUNT);
        resp.setRating_5_cnt((int) RATING_5_COUNT);

        resp.setRating_1_ratio(DOUBLE_RATING_1_RATIO);
        resp.setRating_2_ratio(DOUBLE_RATING_2_RATIO);
        resp.setRating_3_ratio(DOUBLE_RATING_3_RATIO);
        resp.setRating_4_ratio(DOUBLE_RATING_4_RATIO);
        resp.setRating_5_ratio(DOUBLE_RATING_5_RATIO);

        resp.setTotal_review_cnt((int) TOTAL_REVIEW_COUNT);
        resp.setTotal_rating((int) TOTAL_RATING);
        resp.setAverage_rating(DOUBLE_AVERAGE_RATING);

        RATING_1_COUNT = null;
        RATING_2_COUNT = null;
        RATING_3_COUNT = null;
        RATING_4_COUNT = null;
        RATING_5_COUNT = null;

        DOUBLE_RATING_1_RATIO = null;
        DOUBLE_RATING_2_RATIO = null;
        DOUBLE_RATING_3_RATIO = null;
        DOUBLE_RATING_4_RATIO = null;
        DOUBLE_RATING_5_RATIO = null;

        list = null;

        return resp;
    }


    /**************** 특정 상품 항목에 대해서 평점별 카운트 수 조회 *******************/
    @SuppressWarnings("unchecked")
    public Double getGoodsRating(Integer REVIEW_CNT, Integer GOODS_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("goods_idx", GOODS_IDX);

        Integer TOTAL_RATING = 0;

        Integer RATING_1_COUNT = 0;
        Integer RATING_2_COUNT = 0;
        Integer RATING_3_COUNT = 0;
        Integer RATING_4_COUNT = 0;
        Integer RATING_5_COUNT = 0;

        List<dto_user_review> list = (List<dto_user_review>) listService.getDTOList(
            SELECT_QUERY.toString(), dto_user_review.class);

        if (list != null) {
            for (dto_user_review e : list) {
                Integer RATING = e.getRating();

                if (RATING == 1) {
                    RATING_1_COUNT++;
                } else if (RATING == 2) {
                    RATING_2_COUNT++;
                } else if (RATING == 3) {
                    RATING_3_COUNT++;
                } else if (RATING == 4) {
                    RATING_4_COUNT++;
                } else if (RATING == 5) {
                    RATING_5_COUNT++;
                }

                TOTAL_RATING += RATING;
            }

        }

        Double AVERAGE_RATING = (double) 0.0;

        if (REVIEW_CNT != 0) {
            AVERAGE_RATING = (TOTAL_RATING / (double) REVIEW_CNT);
        }

        return (double) Double.valueOf(String.format("%1.2f", AVERAGE_RATING));
    }


    @Override
    public Integer insertImageRecord(String img_url) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("url", img_url);
        hashMap.put("order_idx", null);
        hashMap.put("order_number", null);
        hashMap.put("visible", 1);
        hashMap.put("seq", 0);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME, hashMap,
            com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }


    @Override
    public void deleteImageRecord(Integer e) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", e);
        vo_user_review_image record = (vo_user_review_image) listService.getDTOObject(
            SELECT_QUERY.toString(), vo_user_review_image.class);

        if (record != null) {
            String url = record.getUrl();
            storageService.deleteObject(url);
            DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
            DELETE_QUERY.buildEqual("idx", Integer.toString(e));
            updateService.execute(DELETE_QUERY.toString());
        }

        record = null;
    }


    public Integer getUserReviewCount(Integer USER_IDX) throws SQLException {
        Integer REVIEW_CNT = 0;

        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        CNT_QUERY.buildEqual("user_idx", USER_IDX);

        REVIEW_CNT = listService.getRecordCount(CNT_QUERY.toString());

        return REVIEW_CNT;
    }


    @Override
    public Integer getReviewCnt(Integer GOODS_IDX) throws Exception {
        Integer REVIEW_CNT = 0;

        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        CNT_QUERY.buildEqual("goods_idx", GOODS_IDX);

        REVIEW_CNT = listService.getRecordCount(CNT_QUERY.toString());

        return REVIEW_CNT;
    }

    public void updateGoodsReviewRating(Integer GOODS_IDX) throws Exception {

        Integer REVIEW_CNT = getReviewCnt(GOODS_IDX);

        Double AVERAGE_RATING = getGoodsRating(REVIEW_CNT, GOODS_IDX);

        // 굿즈 평점 업데이트
        goodsService.updateGoodsRating(REVIEW_CNT, AVERAGE_RATING, GOODS_IDX);

        REVIEW_CNT = null;

        AVERAGE_RATING = null;
    }

}
