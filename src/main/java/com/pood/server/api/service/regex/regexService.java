package com.pood.server.api.service.regex;

public interface regexService {

    /******** 이메일 정규식 유효성 체크 *********/
    public boolean validate(String emailStr);
}
  