package com.pood.server.api.service.meta.maintab.impl;

import com.pood.server.api.service.meta.maintab.maintabBannerService;

import org.springframework.stereotype.Service;
import java.util.List;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.config.DATABASE;
import com.pood.server.object.meta.maintab.vo_maintab_banner;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.record.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


@Service("maintabBannerService")
public class maintabBannerServiceImpl implements maintabBannerService{
 
    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_MAINTAB_BANNER;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @SuppressWarnings("unchecked")
    public List<vo_maintab_banner> getList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        List<vo_maintab_banner> list = (List<vo_maintab_banner>)listService.getDTOList(SELECT_QUERY.toString(), vo_maintab_banner.class);
        return list;
    }

}
