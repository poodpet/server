package com.pood.server.api.service.user.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.pood.server.api.service.user.userLogPolicyService;
import com.pood.server.config.DATABASE;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.query.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("userLogPolicyService")
public class userLogPolicyServiceImpl implements userLogPolicyService {
    
    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_LOG_USER_POLICY;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
    
    /************* 회원 정책 로그 삽입 *******************/
	public Integer insert(Integer USER_IDX, Integer MK_TYPE, Boolean EMAIL_NOTI, String USER_UUID) throws SQLException{
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_idx", USER_IDX);
        hashMap.put("mk_type", MK_TYPE);
        hashMap.put("mk_agree", EMAIL_NOTI);
        hashMap.put("user_uuid", USER_UUID);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;

    }
}
