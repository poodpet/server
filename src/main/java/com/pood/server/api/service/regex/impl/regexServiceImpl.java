package com.pood.server.api.service.regex.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.pood.server.api.service.regex.regexService;
import org.springframework.stereotype.Service;
 
@Service("regexService")
public class regexServiceImpl implements regexService{
     
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    
    public boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }
    
}
   