package com.pood.server.api.service.meta.order.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.list.*;
import com.pood.server.object.IMP.*;
import com.pood.server.api.service.meta.coupon.*;
import com.pood.server.api.service.meta.goods.*;
import com.pood.server.api.service.meta.image.*;
import com.pood.server.api.service.meta.order.*;
import com.pood.server.api.service.meta.product.*;
import com.pood.server.object.meta.vo_order_basket;
import com.pood.server.object.resp.resp_order_basket;
import com.pood.server.api.service.user.*;
import com.pood.server.api.service.time.*;
import com.pood.server.api.service.query.*;
import com.pood.server.config.DATABASE;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.config.meta.coupon.COUPON_STATUS;
import com.pood.server.config.meta.order.ORDER_CANCEL;
import com.pood.server.config.meta.order.ORDER_REFUND;
import com.pood.server.config.meta.user.USER_BASKET_STATUS;
import com.pood.server.dto.meta.goods.dto_goods_15;
import com.pood.server.dto.meta.goods.dto_goods_3;
import com.pood.server.dto.meta.order.dto_order_basket;
import com.pood.server.dto.meta.order.dto_order_cancel;
import com.pood.server.dto.meta.order.dto_order_refund;
import com.pood.server.dto.user.dto_user_coupon_2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("orderBasketService")
public class orderBasketServiceImpl implements orderBasketService {

    Logger logger = LoggerFactory.getLogger(orderBasketServiceImpl.class);

    @Autowired
    @Qualifier("userCouponService")
    userCouponService userCouponService;

    @Autowired
    @Qualifier("userBasketService")
    userBasketService userBasketService;
    
    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("imageService")
    imageService imageService;
 
    @Autowired
    @Qualifier("couponService")
    couponService couponService;

    @Autowired
    @Qualifier("userReviewService")
    public userReviewService userReviewService;

    @Autowired
    @Qualifier("goodsProductService")
    goodsProductService goodsProductService;

    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;

    @Autowired
    @Qualifier("timeService")
    timeService timeService;

    @Autowired
    @Qualifier("productService")
    productService productService;

    @Autowired
    @Qualifier("orderCancelService")
    orderCancelService orderCancelService;

    @Autowired
    @Qualifier("orderRefundService")
    orderRefundService orderRefundService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;
    
    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_ORDER_BASKET;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
    
    @SuppressWarnings("unchecked")
    public List<resp_order_basket> getgoodsList(Integer ORDER_IDX)
            throws Exception {

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_idx", ORDER_IDX);
 
        List<resp_order_basket> result = new ArrayList<resp_order_basket>();

        List<dto_order_basket> list = (List<dto_order_basket>)listService.getDTOList(SELECT_QUERY.toString(), dto_order_basket.class);
        if(list != null){
            for(dto_order_basket e : list){

                
                resp_order_basket record = new resp_order_basket();

                dto_user_coupon_2 USER_COUPON_INFO = new dto_user_coupon_2();

                Integer COUPON_IDX = e.getCoupon_idx();

                USER_COUPON_INFO = null;
                if(COUPON_IDX != -1)
                    USER_COUPON_INFO = userCouponService.getUserCoupon("idx", e.getCoupon_idx());

                dto_goods_15 GOODS_INFO = goodsService.GET_GOODS_OBJECT_10(e.getGoods_idx());

                record.setQty(e.getQuantity());
                record.setCoupon(USER_COUPON_INFO);
                record.setGoods_info(GOODS_INFO);
                record.setShoppingbag_idx(e.getBasket_idx());

                result.add(record);

        

            }

        }

        return result;
    }



    /************* 주문시도시 장바구니 정보 저장 **************/
    public Integer insertBasketData(IMP_SHOP e1, Integer ORDER_IDX, String ORDER_NUMBER, Integer DISCOUNT_PRICE) throws SQLException {

        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("basket_idx",   e1.getIdx());
        hashMap.put("coupon_idx",   e1.getCoupon_idx());
        hashMap.put("pr_code_idx",  e1.getPr_code());
        hashMap.put("seller_idx",   e1.getSeller_idx());
        hashMap.put("order_idx",    ORDER_IDX);
        hashMap.put("goods_idx",    e1.getgoods_idx());
        hashMap.put("order_number", ORDER_NUMBER);
        hashMap.put("goods_price",  e1.getgoods_price());
        hashMap.put("quantity",     e1.getQty());
        hashMap.put("status",       USER_BASKET_STATUS.DEFAULT);
        hashMap.put("discount",     DISCOUNT_PRICE);
 
        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }

    /************* 주문에 동원된 쿠폰 사용 처리 **************/
    @SuppressWarnings("unchecked")
    public List<String> updateBasketCoupon(Integer ORDER_IDX) throws Exception {

        List<String> transaction_process = new ArrayList<String>();

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_idx", ORDER_IDX);

        List<vo_order_basket> list = (List<vo_order_basket>)listService.getDTOList(SELECT_QUERY.toString(), vo_order_basket.class);

        if(list != null){
            for(vo_order_basket e : list){
                String process_0 = userCouponService.GET_QUERY_UPDATE_COUPON_STATUS(COUPON_STATUS.USED, e.getCoupon_idx());
                transaction_process.add(process_0);
                process_0 = null;
            }
        } 

        return transaction_process;
    }



    /************** 주문 번호에 해당하는 주문 장바구니 목록 조회 *************/
    @SuppressWarnings("unchecked")
    public List<vo_order_basket> getOrderBasketList(Integer ORDER_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_idx", ORDER_IDX);
        return (List<vo_order_basket>)listService.getDTOList(SELECT_QUERY.toString(), vo_order_basket.class);
    }


    /***************** 주문 번호에 해당하는 장바구니 : 굿즈 정보 조회 ***********************/
    @SuppressWarnings("unchecked")
    public List<dto_goods_3> getgoodsListWithOrderNumber(Integer USER_IDX, Integer ORDER_IDX,
            String ORDER_NUMBER) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_idx", ORDER_IDX);


        List<dto_goods_3> GOODS_LIST = new ArrayList<dto_goods_3>();

        List<vo_order_basket> list = (List<vo_order_basket>)listService.getDTOList(SELECT_QUERY.toString(), vo_order_basket.class);

        if(list != null){
            for(vo_order_basket e : list){
                Integer GOODS_IDX = e.getGoods_idx();
                if (!userReviewService.isReviewed(ORDER_NUMBER, GOODS_IDX, USER_IDX)) {
                    dto_goods_3 record = goodsService.GET_GOODS_OBJECT_2(GOODS_IDX);
                    if(record != null)
                        GOODS_LIST.add(record);
                }
            }
        }

        return GOODS_LIST;
    }

    /***************** 주문 번호에 해당하는 장바구니 리스트 전부 삭제 ******************/
    @SuppressWarnings("unchecked")
    public void deleteBasketWithOrderINDEX(Integer ORDER_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);    
        SELECT_QUERY.buildEqual("order_idx", ORDER_IDX);

        List<vo_order_basket> list = (List<vo_order_basket>)listService.getDTOList(SELECT_QUERY.toString(), vo_order_basket.class);

        if(list != null){
            for(vo_order_basket e : list){
                Integer BASKET_IDX = (Integer)e.getBasket_idx();
                userBasketService.deleteUserBasket(BASKET_IDX);
            }
        }
    }

 

    /****************** 특정 주문 번호에 대해서 장바구니 목록 조회 *******************/
    @SuppressWarnings("unchecked")
    public List<dto_order_basket> getOrderBasketList2(Integer ORDER_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_idx", ORDER_IDX);
        return (List<dto_order_basket>)listService.getDTOList(SELECT_QUERY.toString(), dto_order_basket.class);
    }



    @Override
    public Integer getPrice(String ORDER_NUMBER, Integer GOODS_IDX) throws Exception {
        Integer GOODS_PRICE = 0;

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", ORDER_NUMBER);
        SELECT_QUERY.buildEqual("goods_idx",    GOODS_IDX);
        dto_order_basket record = (dto_order_basket)listService.getDTOObject(SELECT_QUERY.toString(), dto_order_basket.class);
        if(record != null)
            GOODS_PRICE = record.getGoods_price();

        return GOODS_PRICE;
    }
 

    public Integer checkRetrievePossible(String ORDER_NUMBER, Integer GOODS_IDX, Integer CANCEL_GOODS_QTY) throws Exception{

        Integer QUANTITY = -1;

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", ORDER_NUMBER);
        SELECT_QUERY.buildEqual("goods_idx",    GOODS_IDX);
        dto_order_basket record = (dto_order_basket)listService.getDTOObject(SELECT_QUERY.toString(), dto_order_basket.class);

        if(record != null)QUANTITY = record.getQuantity();

        Integer TOTAL_RETREIEVED_UNIT = orderCancelService.getRetreievedCount(ORDER_NUMBER, GOODS_IDX);

        logger.info("주문번호[" + ORDER_NUMBER + "] 총 주문한 굿즈 " + QUANTITY + " 개 중에서 " + CANCEL_GOODS_QTY +" 개 굿즈를 환불처리합니다. 현재까지 총 " + TOTAL_RETREIEVED_UNIT + "개가 환불되었습니다. " );
        
        QUANTITY -= TOTAL_RETREIEVED_UNIT;

        QUANTITY -= CANCEL_GOODS_QTY;

        return QUANTITY;
    }


    public vo_order_basket getOrderBasket(String ORDER_NUMBER, Integer GOODS_IDX)
            throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", ORDER_NUMBER);
        SELECT_QUERY.buildEqual("goods_idx",    GOODS_IDX);
        return (vo_order_basket)listService.getDTOObject(SELECT_QUERY.toString(), vo_order_basket.class);
    }



    @Override
    public String GET_QUERY_INSERT_BASKET_DATA(IMP_SHOP e1, Integer ORDER_IDX, String ORDER_NUMBER, Integer DISCOUNT_PRICE, Integer PR_DISCOUNT_RATE) {
         Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("basket_idx",   e1.getIdx());
        hashMap.put("coupon_idx",   e1.getCoupon_idx());
        hashMap.put("pr_code_idx",  e1.getPr_code());
        hashMap.put("seller_idx",   e1.getSeller_idx());
        hashMap.put("order_idx",    ORDER_IDX);
        hashMap.put("goods_idx",    e1.getgoods_idx());
        hashMap.put("order_number", ORDER_NUMBER);
        hashMap.put("goods_price",  e1.getgoods_price());
        hashMap.put("quantity",     e1.getQty());
        hashMap.put("pr_discount_rate", PR_DISCOUNT_RATE);
        hashMap.put("status",       USER_BASKET_STATUS.DEFAULT);
        hashMap.put("discount",     DISCOUNT_PRICE);
        return queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);
    }



    @SuppressWarnings("unchecked")
    public Boolean isFinalRetreieve(String ORDER_NUMBER, Integer GOODS_IDX) throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", ORDER_NUMBER);
        List<vo_order_basket> list = (List<vo_order_basket>)listService.getDTOList(SELECT_QUERY.toString(), vo_order_basket.class);

        Boolean isFinal = false;

        if(list != null){

            logger.info("해당 굿즈 항목번호[" + GOODS_IDX + "] 취소 건이 주문번호[" + ORDER_NUMBER + "]의 마지막 취소 건인지 조회합니다.");

 
 
            // 주문번호에 해당하는 취소 목록 조회
            List<dto_order_cancel> order_cancel = orderCancelService.getList(ORDER_NUMBER);


            // 주문번호에 해당하는 취소 목록 조회
            List<dto_order_refund> order_refund = orderRefundService.getList(ORDER_NUMBER);



            //장바구니 목록 순회
            for(vo_order_basket e : list){


                Integer ORDER_BASKET_GOODS_IDX = e.getGoods_idx();

                
                if(!ORDER_BASKET_GOODS_IDX.equals(GOODS_IDX)){

                    Boolean isFlag = false;


                    // 장바구니 굿즈별로 취소가 됐는지 안됐는지 확인
                    if(order_cancel != null){

                        for(dto_order_cancel e1 : order_cancel){
                            Integer ORDER_CANCEL_GOODS_IDX = e1.getGoods_idx();
                            if(ORDER_CANCEL_GOODS_IDX.equals(ORDER_BASKET_GOODS_IDX)){
                                if(e1.getType() == ORDER_CANCEL.SUCCESS){
                                    logger.info("굿즈 항목번호[" + e.getGoods_idx() + "]에 대해서 취소 고유 번호[" + e1.getUuid() + "] 성공 내역이 있습니다.");
                                    isFlag = true;
                                }
                            }

                            ORDER_CANCEL_GOODS_IDX = null;
                        }

                    }


                    // 장바구니 굿즈별로 반품이 됐는지 안됐는지 확인
                    if(order_refund != null){

                        for(dto_order_refund e1 : order_refund){
                            Integer ORDER_REFUND_GOODS_IDX = e1.getGoods_idx();
                            if(ORDER_REFUND_GOODS_IDX.equals(ORDER_BASKET_GOODS_IDX)){
                                if(e1.getType() == ORDER_REFUND.TYPE_ACCEPT){
                                    logger.info("굿즈 항목번호[" + e.getGoods_idx() + "]에 대해서 반품 고유 번호[" + e1.getUuid() + "] 성공 내역이 있습니다.");
                                    isFlag = true;
                                }
                            }

                            ORDER_REFUND_GOODS_IDX = null;
                        }

                    }
                    


                    if(!isFlag){
                        logger.info("같은 주문에 대한 다른 굿즈 항목번호[" + e.getGoods_idx() + "]에 대해서 환불 건수가 없으므로 해당 취소는 마지막 환불이 아닙니다.");
                        return isFinal;
                    }
                }

                ORDER_BASKET_GOODS_IDX = null;
            }


            order_cancel = null;

            order_refund = null;

            return true;
        }



        return isFinal;
    }
 
    
}
