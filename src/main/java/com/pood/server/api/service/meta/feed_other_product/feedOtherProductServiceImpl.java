package com.pood.server.api.service.meta.feed_other_product;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.dto_image;
import com.pood.server.dto.meta.dto_feed_other_product;
import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;

import com.pood.server.api.service.list.listService;
import com.pood.server.config.DATABASE;
import com.pood.server.api.service.record.updateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("feedOtherProductService")
public class feedOtherProductServiceImpl implements feedOtherProductService {

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_FEED_OTHER_PRODUCT;

    private String DEFINE_IMAGE_TABLE_NAME = DATABASE.TABLE_FEED_OTHER_PRODUCT_IMAGE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
    
    
    @SuppressWarnings("unchecked")
    public List<dto_feed_other_product> getFeedOtherProductList(pagingSet PAGING_SET, Integer PC_ID, String KEYWORD) throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if(PC_ID != null)
            SELECT_QUERY.buildEqual("category", PC_ID);
        
        if((KEYWORD != null) && (!KEYWORD.equals(""))){
            List<String> clause = new ArrayList<String>();
            clause.add("feed_name");
            clause.add("brand");

            SELECT_QUERY.buildLike(clause, KEYWORD);
            SELECT_QUERY.setPagination(PAGING_SET, true);
        }

        return (List<dto_feed_other_product>)listService.getDTOListWithImage(
                SELECT_QUERY.toString(), "product_idx", DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME, dto_feed_other_product.class, dto_image.class);
    }

    /**************** 결제 요청 텍스트 목록 항목 개수 조회 *****************/
    public Integer getTotalRecordNumber(Integer PC_ID, String KEYWORD) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if(PC_ID != null)
            CNT_QUERY.buildEqual("category", PC_ID);
            
        if((KEYWORD != null) && (!KEYWORD.equals(""))){
            List<String> clause = new ArrayList<String>();
            clause.add("feed_name");
            clause.add("brand");
        
            CNT_QUERY.buildLike(clause, KEYWORD);
        }

        return listService.getRecordCount(CNT_QUERY.toString());
    } 
    
}
