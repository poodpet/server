package com.pood.server.api.service.checkNull;

public interface checkNullService {

    boolean isNull(Object obj) throws Exception;

}
