package com.pood.server.api.service.meta.product;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.meta.product.dto_product_ct;

public interface productCTService {
    

    /************* 상품 종류 항목 리스트를 조회합니다. ********************/
    public List<dto_product_ct> getList() throws Exception;


    /************* 상품 종류 수를 조회합니다. ********************/
    public Integer getTotalRecordNumber() throws SQLException;
    

}
