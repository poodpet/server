package com.pood.server.api.service.user;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.user.vo_user;
import com.pood.server.object.user.vo_user_info;
import com.pood.server.dto.meta.dto_user_info;
import com.pood.server.dto.user.dto_user_1;
import com.pood.server.api.req.header.user.user_info.hUser_info_4;
import com.pood.server.object.pagingSet;

public interface userService {

    /************** 회원 UUID에 해당하는 회원 항목 번호 조회 **************/
    public Integer getUserIDX(String user_uuid) throws Exception;
    
    
    /************** 회원 항목 번호에 해당하는 회원 UUID 조회 **************/
    String getUserUUID(Integer userIdx) throws Exception;



    /************* 회원 이메일에 해당하는 회원 정보 조회 ****************/
    public vo_user getUser(String key, String value) throws Exception;

    public dto_user_1 getUser2(String key, String value, Integer login_type) throws Exception;

    /************* 해당 회원이 있는지 없는지 조회 ****************/
    public boolean isExist(String key, String value) throws Exception;


    /*************** 회원 목록 조회 ************************/
    public List<dto_user_info> getUserList(pagingSet PAGING_SET) throws Exception;


    /*************** 회원 목록 개수 조회 ************************/
    public Integer getTotalRecordNumber() throws SQLException;


    /**********************  회원 항목 추가 *******************/
	public Integer insertRecord(
            String  USER_EMAIL, 
            String  USER_PASSWORD, 
            Boolean USER_SERVICE_AGREE, 
            String  USER_UUID,
            String  ML_NAME, 
            Float   ML_POINT, 
            Integer ML_PRICE,
            Integer ML_MONTH,
            String  USER_PHONE_NUMBER,
            String  USER_NAME) throws Exception;

    

    /************************ 회원 닉네임 발급 ********************/
    public String getUserNickname(String USER_EMAIL);



    /*********************** 회원 항목 정보 업데이트 ****************/
	public void updateUserRecord(hUser_info_4 header) throws SQLException;


     /***************** 회원 비밀번호 업데이트 *************************/
	public void updateUserPassword(Integer USER_IDX, String USER_PASSWORD) throws SQLException;


    /************* 회원 UUID에 해당하는 회원 정보 조회 ****************/
	public List<vo_user_info> getUser2(String user_uuid) throws Exception;

 

	public dto_user_1 getUserRecord(String user_uuid, Integer login_type) throws Exception;


	public void updateUserStatus(String user_uuid, Integer STATUS) throws SQLException;


    // 추천인 코드에 해당하는 회원 조회
	public Integer findReferralUser(String REFERRAL_CODE) throws Exception;


    public Integer validation(dto_user_1 USER, Integer type, String value) throws Exception;
    


    public String generateReferralCode();
    




    public String getUserPhoneNumber(Integer user_idx) throws Exception;





	public String getUserNickname(Integer user_idx) throws Exception;





    public Integer getPoodPush(String USER_UUID) throws Exception;



    public Integer getServicePush(String USER_UUID) throws Exception;


    

    public Integer getOrderPush(String USER_UUID) throws Exception;






    public String getTokenARN(Integer USER_IDX) throws Exception;






    /******************* 특정 회원의 FCM을 신규로 업데이트 합니다. ***********************/
    public void updateUserFCM(Integer USER_IDX, String DEVICE_TOKEN, Integer DEVICE_TYPE) throws SQLException;





    public Integer getUserPoint(String user_uuid) throws Exception;




    public void saveUserPoint(String user_uuid, Integer point) throws Exception;

    


    
    
    public void useUserPoint(String user_uuid, Integer point) throws Exception;




    
    
    public String getUserPassword(String USER_UUID) throws Exception;



    

    public void passwordUpdate(String NEW_PASSWORD, String USER_UUID) throws SQLException;


    public Boolean checkUser(String USER_NAME, String USER_EMAIL, String USER_PHONE_NUMBER) throws Exception;


    public dto_user_info getUserUUID(String user_name, String user_phone) throws Exception;


    public Boolean checkUser(String USER_NAME, String PHONE_NUMBER) throws Exception;


    public void updatePushInfo(Boolean POOD_PUSH, Boolean ORDER_PUSH, Boolean SERVICE_PUSH, Boolean dogPoodPush, Boolean catPoodPush, String USER_UUID) throws SQLException;


    public void userPointInit(String user_uuid) throws SQLException;


    public void userInit(String user_uuid) throws Exception;


    public void deleteRecord(String USER_UUID) throws SQLException;



    public String GET_QUERY_SAVE_USER_POINT(String user_uuid, Integer point);


    public String GET_QUERY_USE_USER_POINT(String user_uuid, Integer use_point);

    public String getUserBaseImg(Long idx) throws Exception;

    public List<Long> getCountUserBaseImg() throws Exception;

    public String getDeviceKey(Integer userIdx) throws Exception;
}
