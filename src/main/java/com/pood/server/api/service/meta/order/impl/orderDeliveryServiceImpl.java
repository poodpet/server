package com.pood.server.api.service.meta.order.impl;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.req.header.order.exchange.OrderExchangeDelivery;
import com.pood.server.api.req.header.order.refund.hOrder_refund_delivery;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.meta.image.imageService;
import com.pood.server.api.service.meta.order.orderDeliveryService;
import com.pood.server.api.service.query.queryService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.order.ORDER_DELIVERY_ADDRESS_TYPE;
import com.pood.server.dto.meta.order.dto_order_delivery;
import com.pood.server.object.IMP.IMP;
import com.pood.server.object.meta.vo_order_delivery;
import com.pood.server.object.meta.vo_order_exchange_delivery;
import com.pood.server.object.meta.vo_order_refund_delivery;
import com.pood.server.object.user.vo_user_delivery_2;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("orderDeliveryService")
public class orderDeliveryServiceImpl implements orderDeliveryService {

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("imageService")
    imageService imageService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_ORDER_DELIVERY;

    private String DEFINE_IMAGE_TABLE_NAME  =  DATABASE.TABLE_ORDER_DELIVERY_IMAGE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

 
    public String GET_QUERY_INSERT_ORDER_DELIVERY(IMP IAMPORT){
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("order_idx",        IAMPORT.ORDER_IDX);
        hashMap.put("order_number",     IAMPORT.MERCHANT_UID);
        hashMap.put("nickname",         IAMPORT.DELIVERY_NICKNAME);
        hashMap.put("zipcode",          IAMPORT.DELIVERY_ZIPCODE);
        hashMap.put("receiver",         IAMPORT.DELIVERY_NAME);
        hashMap.put("address",          IAMPORT.DELIVERY_ADDRESS);
        hashMap.put("address_detail",   IAMPORT.DELIVERY_DETAIL_ADDRESS);
        hashMap.put("courier",          "택배 회사");
        hashMap.put("startdate",        "0000-00-00");
        hashMap.put("phone_number",     IAMPORT.DELIVERY_PHONE_NUMBER);
        hashMap.put("input_type",       IAMPORT.DELIVERY_INPUT_TYPE);
        hashMap.put("default_address",  IAMPORT.DELIVERY_REMOTE_TYPE);
        hashMap.put("regular_date",     "0000-00-00");
        hashMap.put("remote_type",      IAMPORT.DELIVERY_REMOTE_TYPE);
        hashMap.put("delivery_type",    ORDER_DELIVERY_ADDRESS_TYPE.DEFAULT);
        hashMap.put("exchange_number",  "");
        hashMap.put("refund_number",  "");
        return queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);
    }
    
    /******************* 주문 배송지 레코드 생성 ******************/
    public Integer insertOrder_Delivery(IMP IAMPORT) throws SQLException {
        return updateService.insert(GET_QUERY_INSERT_ORDER_DELIVERY(IAMPORT));
    }


    /*  
     ***************** 배송지 정보 등록 **********************/
    public Integer insertOrderDelivery(String EXCHANGE_NUMBER, String REFUND_NUMBER, String ZIPCODE, String ADDRESS, String ADDRESS_DETAIL, Integer REMOTE_TYPE,
            String RECEIVER, String NICKNAME, Integer INPUT_TYPE, String DELIVERY_MESSAGE, String PHONE_NUMBER,
            Integer DELIVERY_TYPE, String MEMO) throws SQLException {

        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("zipcode", ZIPCODE);
        hashMap.put("address", ADDRESS);
        hashMap.put("address_detail", ADDRESS_DETAIL);
        hashMap.put("remote_type", REMOTE_TYPE);
        hashMap.put("receiver", RECEIVER);
        hashMap.put("nickname", NICKNAME);
        hashMap.put("input_type", INPUT_TYPE);
        hashMap.put("delivery_msg", DELIVERY_MESSAGE);
        hashMap.put("phone_number", PHONE_NUMBER);
        hashMap.put("delivery_type", DELIVERY_TYPE);
        hashMap.put("memo", MEMO);
        hashMap.put("exchange_number",  EXCHANGE_NUMBER);
        hashMap.put("refund_number",    REFUND_NUMBER);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }

    /****************** 주문에 대한 배송 이미지 정보 업데이트 ***********************/
    public void updateImageOrderINDEX(Integer ORDER_IDX, Integer IMAGE_IDX) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("order_idx", ORDER_IDX);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME, hashMap, "idx", IMAGE_IDX.toString());
        updateService.execute(query);
        query = null;
    }
 
 

    @Override
    public Integer insertOrderImage(String img_url) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("url", img_url);
        hashMap.put("order_idx", null);
        hashMap.put("visible", 1);
        hashMap.put("type", 0);
        hashMap.put("priority", 0);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME, hashMap,
            com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }

    @Override
    public void insertRecord(String exchange_number, String refund_number, String order_number, Integer order_idx,
        OrderExchangeDelivery delivery, Integer delivery_type) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("order_idx", order_idx);
        hashMap.put("order_number", order_number);
        hashMap.put("delivery_type", delivery_type);
        hashMap.put("receiver", delivery.getReceiver());
        hashMap.put("nickname", delivery.getNickname());
        hashMap.put("zipcode", delivery.getZipcode());
        hashMap.put("address", delivery.getAddress());
        hashMap.put("address_detail", delivery.getDetail_address());
        hashMap.put("phone_number", delivery.getPhone_number());
        hashMap.put("exchange_number",  exchange_number);
        hashMap.put("refund_number",    refund_number);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        updateService.insert(query);

        query = null;
    }

    @Override
    public void insertRecord(String exchange_number, String refund_number, String order_number, Integer order_idx, hOrder_refund_delivery delivery, Integer delivery_type) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("order_idx",        order_idx);
        hashMap.put("order_number",     order_number);
        hashMap.put("delivery_type",    delivery_type);
        hashMap.put("receiver",         delivery.getReceiver());
        hashMap.put("nickname",         delivery.getNickname());
        hashMap.put("zipcode",          delivery.getZipcode());
        hashMap.put("address",          delivery.getAddress());
        hashMap.put("address_detail",   delivery.getDetail_address());
        hashMap.put("phone_number",     delivery.getPhone_number());
        hashMap.put("exchange_number",  exchange_number);
        hashMap.put("refund_number",    refund_number);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        updateService.insert(query);

        query = null;
    }

    public vo_user_delivery_2 getRecord(String user_uuid, Integer order_idx) throws Exception {

        vo_user_delivery_2 record = new vo_user_delivery_2();
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_idx", order_idx);
        SELECT_QUERY.buildEqual("delivery_type", ORDER_DELIVERY_ADDRESS_TYPE.DEFAULT);
        List<dto_order_delivery> list = (List<dto_order_delivery>)listService.getDTOListOrderDelivery(SELECT_QUERY.toString());
        if(list != null){
            for(dto_order_delivery e : list){
                record.setAddress(e.getAddress());
                record.setZipcode(e.getZipcode());
                record.setName(e.getReceiver());
                record.setNickname(e.getNickname());
                record.setDetail_address(e.getAddress_detail());
                record.setPhone_number(e.getPhone_number());
                record.setInput_type(e.getInput_type());
                record.setRemote_type(e.getRemote_type());
                record.setType(null);
            }
        }

        return record;
    }


    @Override
    public vo_order_delivery getRecord2(String ORDER_NUMBER) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", ORDER_NUMBER);
        SELECT_QUERY.buildEqual("delivery_type", ORDER_DELIVERY_ADDRESS_TYPE.DEFAULT);
        return (vo_order_delivery)listService.getDTOObject(SELECT_QUERY.toString(), vo_order_delivery.class);
    }


    @SuppressWarnings("unchecked")
    public List<vo_order_refund_delivery> getDeliveryListWithRefundNumber(String refund_number) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("refund_number", refund_number);
        return (List<vo_order_refund_delivery>)listService.getDTOList(SELECT_QUERY.toString(), vo_order_refund_delivery.class);
    }


    @SuppressWarnings("unchecked")
    public List<vo_order_exchange_delivery> getDeliveryListWithExchangeNumber(String exchange_number) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("exchange_number", exchange_number);
        return (List<vo_order_exchange_delivery>)listService.getDTOList(SELECT_QUERY.toString(), vo_order_exchange_delivery.class);
    }
    
}
