package com.pood.server.api.service.meta.event_goods_type;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.meta.event.dto_event_goods_type;
import com.pood.server.object.pagingSet;

public interface eventgoodsTypeService {

	public Integer getTotalRecordNumber() throws SQLException;

	public List<dto_event_goods_type> getEventgoodsTypeList(pagingSet PAGING_SET) throws Exception;
    
}
