package com.pood.server.api.service.meta.promotion;

import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.pood.server.api.service.record.*;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.meta.dto_promotion_group;

import java.util.List;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.listService;

@Service("promotionGroupService")
public class promotionGroupServiceImpl implements promotionGroupService{

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_PROMOTION_GROUP;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @SuppressWarnings("unchecked")
    public List<dto_promotion_group> getList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return (List<dto_promotion_group>)listService.getDTOList(SELECT_QUERY.toString(), dto_promotion_group.class);
    }


    
}
