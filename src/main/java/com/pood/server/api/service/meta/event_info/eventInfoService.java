package com.pood.server.api.service.meta.event_info;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.event.dto_event_info;
import com.pood.server.dto.meta.event.dto_event_info_2;
import com.pood.server.dto.meta.event.dto_event_info_3;

public interface eventInfoService {

	public Integer getTotalRecordNumber(Integer event_type_idx, Integer pc_id, Integer brand_idx) throws SQLException;

	public List<dto_event_info> getEventInfoList(pagingSet PAGING_SET) throws Exception;

	public List<dto_event_info_2> getEventList(pagingSet PAGING_SET, Integer EVENT_TYPE_IDX, Integer PC_ID, Integer BRAND_IDX) throws Exception;

	public List<dto_event_info_3> getEventList2(pagingSet PAGING_SET, Integer event_idx) throws Exception;
    
}
