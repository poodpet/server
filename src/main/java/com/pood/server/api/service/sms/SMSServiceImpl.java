package com.pood.server.api.service.sms;

import com.pood.server.api.service.APICall.APICallService;
import com.pood.server.api.service.json.jsonService;
import com.pood.server.object.api.API_RESPONSE;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

@Service(value = "SMSService")
public class SMSServiceImpl implements SMSService {

    @Value("${legacy.server-ip}")
    private String serverIp;

    Logger logger = LoggerFactory.getLogger(SMSServiceImpl.class);

    @Autowired
    @Qualifier("jsonService")
    jsonService jsonService;

    @Autowired
    @Qualifier("APICallService")
    APICallService APICallService;

    // 알리고  API 호출
    public Integer send(final String PHONE_NUMBER, final String MESSAGE) throws Exception {
        String result;
        String url =
            serverIp + "/pood/admin/sms/1?phone_number=" + PHONE_NUMBER + "&message=" +
                URLEncoder.encode(MESSAGE, StandardCharsets.UTF_8);

        logger.info("CALL ADMIN SERVER API : " + url);
        try {
            HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
            factory.setConnectTimeout(3 * 1000);
            factory.setReadTimeout(3 * 1000);
            result = new RestTemplate(factory).getForObject(url, String.class);
        } catch (ResourceAccessException e) {
            logger.error(e.getMessage());
            return HttpStatus.SC_OK;
        }

        API_RESPONSE resp = (API_RESPONSE) jsonService.getDecodedObject(result, API_RESPONSE.class);

        return resp.getStatus();
    }

}
