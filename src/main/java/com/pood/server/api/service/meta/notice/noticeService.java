package com.pood.server.api.service.meta.notice;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_notice;

public interface noticeService {
 
    /**************** 항목 목록 조회 *****************/
    public List<dto_notice> getList(pagingSet PAGING_SET) throws Exception;

    /**************** 전체 목록 개수 조회 ******************/
    public Integer getTotalRecordNumber() throws SQLException;
    
}
