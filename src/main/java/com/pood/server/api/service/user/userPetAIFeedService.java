package com.pood.server.api.service.user;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.object.resp.resp_user_pet_ai_feed;

public interface userPetAIFeedService{

    public Integer insertRecord(Integer user_idx, Integer up_idx, List<Integer> product_idx) throws SQLException;

    public Integer getTotalRecordNumber(Integer user_idx) throws SQLException;

    public List<resp_user_pet_ai_feed> getList(pagingSet PAGING_SET, Integer user_idx, Integer up_idx) throws Exception;

    public void deleteRecord(Integer e) throws SQLException;

    public void deleteRecordWithUserPetIDX(Integer e) throws SQLException;
    
}
