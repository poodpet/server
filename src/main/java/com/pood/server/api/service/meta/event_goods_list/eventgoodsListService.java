package com.pood.server.api.service.meta.event_goods_list;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.event.dto_event_goods_list;
import com.pood.server.dto.meta.event.dto_event_goods_list_2;

public interface eventgoodsListService {

	public Integer getTotalRecordNumber() throws SQLException;

	public List<dto_event_goods_list> getEventgoodsList(pagingSet PAGING_SET) throws Exception;

	public List<dto_event_goods_list_2> getItemList(Integer EVENT_IDX) throws Exception;
    
}
