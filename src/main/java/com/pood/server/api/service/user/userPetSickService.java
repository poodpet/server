package com.pood.server.api.service.user;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.user.pet.dto_user_pet_sick;

public interface userPetSickService {
    
    /******************* 회원 반려 동물 항목 번호에 해당하는 질병 번호 조회 ********************/
    public List<dto_user_pet_sick> getList(Integer USER_PET_IDX) throws Exception;


    /******************** 회원 동물 AI 질병 정보 등록 ******************************/
    public Integer insertRecord(Integer USER_PET_IDX, Integer SICK_IDX) throws SQLException;


    public void deleteRecordWithUserPetIDX(Integer USER_PET_IDX) throws SQLException;

 
}
