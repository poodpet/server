package com.pood.server.api.service.database;

import com.pood.server.dto.dto_image;
import com.pood.server.dto.dto_image_1;
import com.pood.server.dto.dto_image_2;
import com.pood.server.dto.dto_image_3;
import com.pood.server.dto.dto_image_4;
import com.pood.server.dto.log.user_order.dto_log_user_order_2;
import com.pood.server.dto.meta.order.dto_order_delivery;
import java.sql.SQLException;
import java.util.List;

public interface DTOService {

    List<dto_image> getdtoImageList(String query) throws SQLException;

    List<dto_image_1> getdtoImageList1(String query) throws SQLException;

    List<dto_image_2> getdtoImageList2(String query) throws SQLException;

    List<dto_image_4> getdtoImageList4(String query) throws SQLException;

    List<dto_image_3> GET_DTO_REVIEW_IMAGE(String query) throws Exception;

    List<dto_log_user_order_2> GET_DTO_LOG_USER_ORDER(String query) throws Exception;

    List<dto_order_delivery> GET_DTO_ORDER_DELIVERY(String query) throws Exception;

}
