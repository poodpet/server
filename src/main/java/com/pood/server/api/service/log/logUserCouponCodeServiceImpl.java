package com.pood.server.api.service.log;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.pood.server.config.DATABASE;
import com.pood.server.object.log.vo_log_user_coupon_code;
import com.pood.server.api.service.record.*;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.query.queryService;

@Service("logUserCouponCodeService")
public class logUserCouponCodeServiceImpl implements logUserCouponCodeService{
 
    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;
    
    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_LOG_USER_COUPON_CODE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @Override
    public vo_log_user_coupon_code getRecord(String user_uuid, String code) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", user_uuid);
        SELECT_QUERY.buildEqual("code", code);
        return (vo_log_user_coupon_code)listService.getDTOObject(SELECT_QUERY.toString(), vo_log_user_coupon_code.class);
    }
 
 
}
