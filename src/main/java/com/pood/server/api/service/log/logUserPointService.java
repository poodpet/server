package com.pood.server.api.service.log;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.log.dto_log_user_point;

public interface logUserPointService {

    public Integer insertRecord(String user_uuid, String point_uuid, Integer point) throws SQLException;

    public void init(String user_uuid) throws Exception;

    public List<dto_log_user_point> getList(String user_uuid) throws Exception;
    
    public String GET_QUERY_INSERT_RECORD(String user_uuid, String point_uuid, Integer point);
    
}
