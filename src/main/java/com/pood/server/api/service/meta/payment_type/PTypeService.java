package com.pood.server.api.service.meta.payment_type;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_payment_type;

public interface PTypeService {

    /**************** 영양소 목록 조회 *****************/
    public List<dto_payment_type> getPaymentTypeList(pagingSet PAGING_SET) throws Exception;

    /**************** 전체 목록 조회 ******************/
    public Integer getTotalRecordNumber() throws SQLException;

    /**************** 특정 타입 이름 조회*****************/
    public String getPaymentTypeName(Integer order_type) throws Exception;

    public Integer getOrderType(String payment_method) throws Exception;
 
    
}
