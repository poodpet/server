package com.pood.server.api.service.meta.pet_category;

import java.sql.SQLException;

import java.util.List;

import com.pood.server.dto.meta.pet.dto_pet_category;
import com.pood.server.object.pagingSet;

public interface petCategoryService {

    /************* 펫 종류 항목 리스트를 조회합니다. ********************/
    public List<dto_pet_category> getPetCategoryList(pagingSet PAGING_SET) throws Exception;

    /************* 펫 항목 수를 조회합니다. ********************/
    public Integer getTotalRecordNumber() throws SQLException;

}
