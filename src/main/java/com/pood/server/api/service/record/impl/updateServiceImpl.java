package com.pood.server.api.service.record.impl;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.api.service.database.DBService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.query.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("updateService")
public class updateServiceImpl implements updateService {

 
    @Autowired
    @Qualifier("DBService")
    DBService DBService;
 
    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;
    
    
    public void execute(String query) throws SQLException {
        DBService.update(query);
    }
 
 
  
 
    @Override
    public Integer insert(String query) throws SQLException {
        return DBService.insert(query);
    }



    public void commit(List<String> query) throws SQLException {
        DBService.commit(query);
    }

    

} 