package com.pood.server.api.service.sns;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.PublishRequest;
import com.pood.server.api.service.error.errorProcService;
import com.pood.server.api.service.noti.pushService;
import com.pood.server.api.service.sms.SMSService;
import com.pood.server.api.service.user.userNotiService;
import com.pood.server.api.service.user.userService;
import com.pood.server.config.AWS;
import com.pood.server.config.meta.user.USER_NOTI;
import com.pood.server.entity.DeviceType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component("snsService")
@RequiredArgsConstructor
public class snsServiceImpl implements snsService{


    private final userService userService;
    private final SMSService SMSService;
    private final pushService pushService;
    private final userNotiService userNotiService;
    private final errorProcService errorProcService;
    
    /**************** 메세지 발송 *************************/
    public void send(String PUSH_MESSAGE, String USER_UUID, Integer USER_IDX, Integer ALARM_TYPE, String MESSAGE_TITLE, String SCHEMA) throws Exception{
        AWSCredentials credentials = new BasicAWSCredentials(AWS.accessKey, AWS.secretKey);
        
    
        if(SCHEMA == null)SCHEMA = "";

        Integer     ORDER_PUSH      = userService.getOrderPush(USER_UUID);

        Integer     POOD_PUSH       = userService.getPoodPush(USER_UUID);

        Integer     SERVICE_PUSH    = userService.getServicePush(USER_UUID);

        Boolean     chk = checkUserNotification(USER_UUID, ALARM_TYPE, ORDER_PUSH, SERVICE_PUSH, POOD_PUSH);

        if(chk){

            if(ALARM_TYPE == USER_NOTI.CANCEL_FINISHED){

                String  MAIN_TYPE       = "orderList";
                String  GOODS_TYPE      = "";
                String  IDX             = "";
                String  NEED_LOGIN      = "";
                String  SUB_TYPE        = "";

                SCHEMA                = "{\"main_type\":\""+MAIN_TYPE+"\",\"goods_type\":\""+ GOODS_TYPE+"\",\"idx\":\""+ IDX +"\",\"needLogin\":\""+ NEED_LOGIN+"\",\"sub_type\":\""+ SUB_TYPE+"\"}";

            }
            
            String noti_msg   = "{\"push\":{\"title\":\""+MESSAGE_TITLE+"\",\"message\":\""+ PUSH_MESSAGE +"\",\"scheme\":"+SCHEMA+"}}";

            
            AmazonSNS   SNS_CLIENT = AmazonSNSClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.AP_NORTHEAST_2).build();
            

            
            // 특정 토큰에 대해서 문자 전송
            String          TOKEN_ARN = userService.getTokenARN(USER_IDX);

            if((TOKEN_ARN != null) && (!TOKEN_ARN.equals(""))){
                
                PublishRequest  publishRequest = new PublishRequest();
                publishRequest.setMessage(noti_msg);

                // 특정 토큰에 대해서 문자 전송
                publishRequest.withTargetArn(TOKEN_ARN);



                String MESSAGE_ID = SNS_CLIENT.publish(publishRequest).getMessageId();
                logger.info("MESSAGE ID : " + MESSAGE_ID);
                MESSAGE_ID = null;

            

                // PUSH 전송 후 PUSH 로그 생성
                pushService.insertRecord(USER_IDX, USER_UUID, ALARM_TYPE, MESSAGE_TITLE, PUSH_MESSAGE, SCHEMA, "");


                // PUSH 전송 후 앱 알림
                userNotiService.insertRecord(USER_IDX, USER_UUID, 0, MESSAGE_TITLE, PUSH_MESSAGE, SCHEMA, "", ALARM_TYPE);

            }
        }

        ORDER_PUSH      = 0;

        SERVICE_PUSH    = 0;

        POOD_PUSH       = 0;


    }


    @Override
    public DeviceType getType() {
        return DeviceType.AOS;
    }

    


}
