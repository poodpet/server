package com.pood.server.api.service.user.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.service.user.userBasketService;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.meta.goods.dto_goods_8;
import com.pood.server.dto.user.dto_user_basket;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.meta.coupon.*;
import com.pood.server.api.service.meta.goods.*;
import com.pood.server.api.service.user.*;
import com.pood.server.api.service.query.*;
import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("userBasketService")
public class userBasketServiceImpl implements userBasketService {

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("userCouponService")
    userCouponService userCouponService;

    @Autowired
    @Qualifier("couponService")
    couponService couponService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_USER_BASKET;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
    
    
    /*************** 회원 항목번호에 해당하는 장바구니 번호 목록 조회 *********************/
    @SuppressWarnings("unchecked")
    public List<Integer> getUserBasketList(String user_uuid) throws Exception {

        List<Integer> USER_BASKET_LIST = new ArrayList<Integer>();

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);

        if (user_uuid != null) {
            SELECT_QUERY.buildEqual("user_uuid", user_uuid);
            SELECT_QUERY.buildReverseOrder("idx");
        }

        List<dto_user_basket> list = (List<dto_user_basket>)listService.getDTOList(SELECT_QUERY.toString(), dto_user_basket.class);

        if(list != null){
            for(dto_user_basket e : list){
                USER_BASKET_LIST.add((Integer)e.getIdx());
            }       
        }

        return USER_BASKET_LIST;
    }

    /********************** 장바구니 항목 번호에 매칭되는 레코드의 주문 번호 업데이트 *************************/
    public void updateOrderNumberBasket(Integer BASKET_IDX, String ORDER_NUMBER) throws SQLException {
        updateService.execute(GET_QUERY_UPDATE_ORDER_NUMBER_BASKET(BASKET_IDX, ORDER_NUMBER));
    }

    public String GET_QUERY_UPDATE_ORDER_NUMBER_BASKET(Integer BASKET_IDX, String ORDER_NUMBER){
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("order_number", ORDER_NUMBER);
        return queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", BASKET_IDX.toString());
    }

    /******************** 장바구니 항목 삽입 *********************/
    public Integer insertUserBasket(String USER_UUID, Integer USER_IDX, Integer GOODS_IDX, Integer PR_CODE_IDX, Integer QTY,
            String REGULAR_DATE, Integer GOODS_PRICE, Integer STATUS) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_idx", USER_IDX);
        hashMap.put("user_uuid", USER_UUID);
        hashMap.put("order_number", "");
        hashMap.put("goods_idx", GOODS_IDX);
        hashMap.put("pr_code_idx", PR_CODE_IDX);
        hashMap.put("qty", QTY);
        hashMap.put("regular_date", REGULAR_DATE);
        hashMap.put("GOODS_PRICE", GOODS_PRICE);
        hashMap.put("status", STATUS);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }

    /****************** 장바구니 항목 수량 업데이트 *********************/
    public void updateBasketQuantity(Integer BASKET_IDX, Integer BASKET_QTY) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("qty", BASKET_QTY);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", BASKET_IDX.toString());
        updateService.execute(query);
        query = null;
    }

    /********************** 굿즈이 이미 장바구니에 있는지 확인 **********************/
    @SuppressWarnings("unchecked")
    public Integer checkIsInBasket(String USER_UUID, Integer GOODS_IDX, Integer QTY) throws Exception {

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", USER_UUID);
        SELECT_QUERY.buildEqual("goods_idx", GOODS_IDX);

        Integer inBasket = -1;

        List<dto_user_basket> list = (List<dto_user_basket>)listService.getDTOList(SELECT_QUERY.toString(), dto_user_basket.class);
        if(list != null){
            for(dto_user_basket e : list){
                Integer BASKET_IDX = e.getIdx();
                Integer BASKET_QTY = e.getQty();
                BASKET_QTY += QTY;
                updateBasketQuantity(BASKET_IDX, BASKET_QTY);
                inBasket = BASKET_IDX;
            }

        } 

        return inBasket;
    }

    /****************** 장바구니 항목번호에 해당하는 장바구니 정보 조회 ********************/
    public dto_user_basket getUserBasket(Integer BASKET_IDX) throws Exception {
 
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", BASKET_IDX);
        dto_user_basket record = (dto_user_basket)listService.getDTOObject(SELECT_QUERY.toString(), dto_user_basket.class);
 
        if(record != null){

            // 장바구니에 매핑된 거래 정보 조회
            Integer GOODS_IDX = record.getGoods_idx();
            dto_goods_8 goods_info = goodsService.GET_GOODS_OBJECT_5(GOODS_IDX);
            
            if(goods_info != null)
                record.setGoods_info(goods_info);
                
            goods_info = null;

            
            
        } 
 
        return record;
    }

    /****************** 장바구니 항목 상태 업데이트 ****************/
    public void updateBasketStatus(Integer BASKET_IDX, Integer STATUS) throws SQLException {
        updateService.execute(GET_QUERY_UPDATE_BASKET_STATUS(BASKET_IDX, STATUS));
    }

    public String GET_QUERY_UPDATE_BASKET_STATUS(Integer BASKET_IDX, Integer STATUS){
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("status", STATUS);
        return queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", BASKET_IDX.toString());
    }

    /*********************** 장바구니 항목번호에 해당하는 장바구니 정보 조회 ************************/
    public dto_user_basket getUserBasket2(Integer BASKET_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", BASKET_IDX);
        return (dto_user_basket)listService.getDTOObject(SELECT_QUERY.toString(), dto_user_basket.class);
    }

    /*************** 회원 항목번호에 해당하는 장바구니 목록 조회 *********************/
    @SuppressWarnings("unchecked")
    public List<dto_user_basket> getUserBasketList2(Integer USER_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_idx", USER_IDX);
        return (List<dto_user_basket>)listService.getDTOList(SELECT_QUERY.toString(), dto_user_basket.class);
    }

    /**************** 특정 장바구니 항목 삭제 ***********************/
	public void deleteUserBasket(Integer BASKET_IDX) throws SQLException{
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("idx", Integer.toString(BASKET_IDX));
        updateService.execute(DELETE_QUERY.toString());
    }

    //  주문 번호에 해당하는 장바구니 항목번호 조회
    public Integer getBasketIDX(String ORDER_NUMBER) throws Exception {

        Integer BASKET_IDX = -1;
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", ORDER_NUMBER);

        dto_user_basket record = (dto_user_basket)listService.getDTOObject(SELECT_QUERY.toString(), dto_user_basket.class);
        BASKET_IDX = record.getIdx();
        record = null;
        
        return BASKET_IDX;
    }

    @Override
    public Integer getUserBasketCount(String user_uuid) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        CNT_QUERY.buildEqual("user_uuid", user_uuid);
        Integer cnt = listService.getRecordCount(CNT_QUERY.toString());
        return cnt;
    }

    @Override
    public void deleteUserBasketWithUserUUID(String USER_UUID) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_uuid", USER_UUID);
        updateService.execute(DELETE_QUERY.toString());
    }

    @Override
    public String GET_QUERY_DELETE_BASKET(Integer BASKET_IDX) {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("idx", BASKET_IDX);
        return DELETE_QUERY.toString();
    }
}

