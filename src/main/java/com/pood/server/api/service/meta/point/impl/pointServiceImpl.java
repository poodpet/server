package com.pood.server.api.service.meta.point.impl;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.point.META_POINT_TYPE;
import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_point;
import com.pood.server.dto.meta.dto_point;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.pood.server.api.service.list.*;
import com.pood.server.api.service.meta.point.pointService;
import com.pood.server.api.service.record.*;
import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;

@Service("pointService")
public class pointServiceImpl implements pointService {

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;
 
    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_POINT;
    
    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
 

    /************ 포인트 정보 가지고 옴 ************/
    public dto_point getPointObject(Integer POINT_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", POINT_IDX);
        return (dto_point)listService.getDTOObject(SELECT_QUERY.toString(), dto_point.class);
    }

    /**************** 포인트 목록 조회 *****************/
    @SuppressWarnings("unchecked")
    public List<dto_point> getPointList(pagingSet PAGING_SET) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_point>)listService.getDTOList(SELECT_QUERY.toString(), dto_point.class);
    }

    /**************** 포인트 목록 개수 조회 ******************/
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @Override
    public Integer getPoint(Integer POINT_IDX, Integer ORDER_PRICE, Integer DELIVERY_FEE) throws Exception {

        Integer point = 0;

        if(POINT_IDX == META_POINT_TYPE.PURCHASE){

            SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
            SELECT_QUERY.buildEqual("idx", POINT_IDX);
            
            vo_point record = (vo_point)listService.getDTOObject(SELECT_QUERY.toString(), vo_point.class);

            Integer price_rate_type = record.getPoint_type();

            // 적립 금액인 경우 적립 금액으로 계산 
            if(price_rate_type == 0)
                point = record.getPoint_price();
                    
            // 적립율인 경우 상품 금액에서 적립율로 계산
            if(price_rate_type == 1){
                Integer tmp1 = ORDER_PRICE - DELIVERY_FEE;
                Double  tmp2 = (record.getPoint_rate() * 0.01) * tmp1;     
                point = tmp2.intValue();
            }

        }else point = META_POINT_TYPE.GET_POINT(POINT_IDX);
         
        return point;
    }

 
}
