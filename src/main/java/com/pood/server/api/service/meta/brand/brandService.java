package com.pood.server.api.service.meta.brand;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.brand.dto_brand;
import com.pood.server.dto.meta.brand.dto_brand_3;
import com.pood.server.dto.meta.brand.dto_brand_4;

public interface brandService {
 
    /************* 브랜드 항목 리스트 조회  ****************/
    public List<dto_brand> getBrandList(pagingSet PAGING_SET, Integer BRAND_IDX) throws Exception;



    /*************** 전체 브랜드 항목 개수 반환 ****************/
    public Integer getTotalRecordNumber() throws SQLException;

  
    public dto_brand_3 getBrandRecord(Integer BRAND_IDX) throws Exception;



    public List<dto_brand_4> getList() throws Exception;



}
  