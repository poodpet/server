package com.pood.server.api.service.user.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.service.user.userPetAIFeedService;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.meta.product.dto_product_8;
import com.pood.server.dto.user.pet.dto_user_pet_ai_feed;
import com.pood.server.object.pagingSet;
import com.pood.server.object.resp.resp_user_pet_ai_feed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.meta.product.*;
import com.pood.server.api.service.meta.brand.*;
import com.pood.server.api.service.query.*;
import org.springframework.stereotype.Service;

@Service("userPetAIFeedService")
public class userPetAIFeedServiceImpl implements userPetAIFeedService{

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("productService")
    productService productService;

    @Autowired
    @Qualifier("brandService")
    brandService brandService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_USER_PET_AI_FEED;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    public Integer insertRecord(Integer user_idx, Integer up_idx, List<Integer> product_idx) throws SQLException{

        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("up_idx", Integer.toString(up_idx));
        updateService.execute(DELETE_QUERY.toString());

        Integer success = 0;

        for(Integer e : product_idx){

            Map<String, Object> hashMap = new HashMap<String, Object>();
            hashMap.put("user_idx",     user_idx);
            hashMap.put("up_idx",       up_idx);
            hashMap.put("product_idx",  e);

            String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);

            Integer result_idx = updateService.insert(query);

            query = null;

            if(result_idx == -1)success = -1;
        }
        
        return success;
    }


    @Override
    public Integer getTotalRecordNumber(Integer user_idx) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME); 
        CNT_QUERY.buildEqual("user_idx", user_idx);
        return listService.getRecordCount(CNT_QUERY.toString());
    }


    @SuppressWarnings("unchecked")
    public List<resp_user_pet_ai_feed> getList(pagingSet PAGING_SET, Integer user_idx, Integer up_idx) throws Exception {

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_idx", user_idx);        

        if(up_idx != null)
            SELECT_QUERY.buildEqual("up_idx", up_idx);

        SELECT_QUERY.setPagination(PAGING_SET, true);
        
        List<resp_user_pet_ai_feed> result = new ArrayList<resp_user_pet_ai_feed>();

        List<dto_user_pet_ai_feed> list = 
            (List<dto_user_pet_ai_feed>)listService.getDTOList(SELECT_QUERY.toString(), dto_user_pet_ai_feed.class);

        if(list != null){
            for(dto_user_pet_ai_feed e : list){

                resp_user_pet_ai_feed record = new resp_user_pet_ai_feed();
                record.setIdx(e.getIdx());
                record.setUser_idx(e.getUser_idx());
                record.setUp_idx(e.getUp_idx());
                record.setRecordbirth(e.getRecordbirth());

                Integer PRODUCT_IDX = e.getProduct_idx();

                dto_product_8 PRODUCT = productService.getProductObject4(PRODUCT_IDX);
                record.setProduct(PRODUCT);

                result.add(record);

                record      = null;

                PRODUCT     = null;

            }
        }
        
        return result;
    }


    @Override
    public void deleteRecord(Integer e) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("idx", Integer.toString(e));
        updateService.execute(DELETE_QUERY.toString());
    }


    @Override
    public void deleteRecordWithUserPetIDX(Integer e) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("up_idx", Integer.toString(e));
        updateService.execute(DELETE_QUERY.toString());
    }
}
