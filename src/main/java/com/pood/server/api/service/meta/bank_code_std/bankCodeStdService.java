package com.pood.server.api.service.meta.bank_code_std;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.meta.dto_bank_code_std;
import com.pood.server.object.pagingSet;

public interface bankCodeStdService {
 

    /******************* 은행 식별 코드 항목 목록 조회 **************/
    public List<dto_bank_code_std> getBankCodeList(pagingSet PAGING_SET) throws Exception;

    
    
    /******************* 전체 항목 목록 개수 반환 *****************/
    public Integer getTotalRecordNumber() throws SQLException;
 
}
