package com.pood.server.api.service.meta.category;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.meta.vo_category;

public interface categoryService {

    public Integer getTotalRecordNumber() throws SQLException;

    public List<vo_category> getList(Integer pc_id) throws Exception;
    
}
