package com.pood.server.api.service.error;

import java.sql.SQLException;

public interface errorAPIService {

    Integer insertRecord(String path, String error_msg, String header, Integer status_code) throws SQLException;
}
