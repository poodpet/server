package com.pood.server.api.service.meta.order.impl;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.meta.order.orderDeliveryService;
import com.pood.server.api.service.meta.order.orderRefundService;
import com.pood.server.api.service.query.queryService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.order.ORDER_REFUND;
import com.pood.server.dto.meta.order.dto_order_refund;
import com.pood.server.dto.meta.order.dto_order_refund_2;
import com.pood.server.object.meta.vo_order_refund;
import com.pood.server.object.meta.vo_order_refund_delivery;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service(value="orderRefundService")
public class orderRefundServiceImpl implements orderRefundService{

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("orderDeliveryService")
    orderDeliveryService orderDeliveryService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_ORDER_REFUND;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @Override
    public String insertRecord(String text, String orderNumber, Integer goodsIdx, Integer quantity,
        Integer sendType, Boolean causeAttributable) throws SQLException {
        String refundUuid = UUID.randomUUID().toString();
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("order_number", orderNumber);
        hashMap.put("goods_idx", goodsIdx);
        hashMap.put("qty", quantity);
        hashMap.put("text", text);
        hashMap.put("type", ORDER_REFUND.TYPE_REQUEST);
        hashMap.put("attr", causeAttributable);
        hashMap.put("uuid", refundUuid);
        hashMap.put("send_type", sendType);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap,
            com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        updateService.insert(query);

        return refundUuid;
    }

    @SuppressWarnings("unchecked")
    public List<dto_order_refund> getList(String ORDER_NUMBER) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", ORDER_NUMBER);
        return (List<dto_order_refund>)listService.getDTOList(SELECT_QUERY.toString(), dto_order_refund.class);
    }

    public vo_order_refund getRecord(String uuid) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("uuid", uuid);
        return (vo_order_refund)listService.getDTOObject(SELECT_QUERY.toString(), vo_order_refund.class);
    }

    @Override
    public void upddateStatus(Integer status, String uuid) throws SQLException {
        updateService.execute(GET_QUERY_UPDATE_STATUS(uuid, status));

    }

    @SuppressWarnings("unchecked")
    public List<dto_order_refund_2> getList2(String ORDER_NUMBER) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", ORDER_NUMBER);
        List<dto_order_refund_2> list = (List<dto_order_refund_2>)listService.getDTOList(SELECT_QUERY.toString(), dto_order_refund_2.class);

        if(list != null){
            for(dto_order_refund_2 e : list){
                String refund_number = e.getUuid();
                List<vo_order_refund_delivery> delivery_list = orderDeliveryService.getDeliveryListWithRefundNumber(refund_number);
                e.setDelivery(delivery_list);
                delivery_list = null;
            }
        }

        return list;
    }

    @Override
    public String GET_QUERY_UPDATE_STATUS(String uuid, Integer status) {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("type", status);
        return queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "uuid", uuid);
    }
    
}
