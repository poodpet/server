package com.pood.server.api.service.meta.psk;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_psk;

public interface PSKService {
 
	public Integer getTotalRecordNumber() throws SQLException;

	public List<dto_psk> getList(pagingSet PAGING_SET, Integer pc_idx) throws Exception;
 
    
}
