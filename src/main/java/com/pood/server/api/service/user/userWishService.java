package com.pood.server.api.service.user;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.user.dto_user_wish;

public interface userWishService {

    /*************** 위시 항목 추가 ******************/
	public Integer insertRecord(Integer USER_IDX, Integer GOODS_IDX) throws SQLException;

	public Boolean getWishCheck(Integer USER_IDX, Integer GOODS_IDX) throws Exception;

	public List<dto_user_wish> getUserWishList(Integer USER_IDX) throws Exception;

    public void deleteRecord(Integer goods_idx, Integer user_idx) throws SQLException;

    public void deleteRecordWithUSERIDX(Integer USER_IDX) throws SQLException;
    
}
