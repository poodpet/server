package com.pood.server.api.service.view.view_8;

public interface view8Service {

    public Boolean isPurchase(String USER_UUID, Integer GOODS_IDX) throws Exception;

    public String isPurchase2(String USER_UUID, Integer GOODS_IDX) throws Exception;
    
}
