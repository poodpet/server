package com.pood.server.api.service.log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.pood.server.config.DATABASE;
import com.pood.server.api.service.record.*;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.pood.server.api.service.list.*;
import com.pood.server.api.service.query.queryService;

@Service("logUserOrderBasketService")
public class logUserOrderBasketServiceImpl implements logUserOrderBasketService{
    
    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;
    
    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_LOG_USER_ORDER_BASKET;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @Override
    public Integer insertRecord(String USER_UUID, String MERCHANT_UID, Integer GOODS_IDX, Integer PURCHASE_QUANTITY) throws SQLException {
        return updateService.insert(GET_QUERY_INSERT_RECORD(USER_UUID, MERCHANT_UID, GOODS_IDX, PURCHASE_QUANTITY));
    }


    public String GET_QUERY_INSERT_RECORD(String USER_UUID, String MERCHANT_UID, Integer GOODS_IDX, Integer PURCHASE_QUANTITY){
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_uuid",        USER_UUID);
        hashMap.put("order_number",     MERCHANT_UID);
        hashMap.put("goods_idx",        GOODS_IDX);
        hashMap.put("qty",              PURCHASE_QUANTITY);
        return queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);
    }
}
