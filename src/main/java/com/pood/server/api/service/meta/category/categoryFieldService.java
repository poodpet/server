package com.pood.server.api.service.meta.category;

import java.util.List;

import com.pood.server.object.meta.vo_category_field;

public interface categoryFieldService {

    public List<vo_category_field> getList() throws Exception;
    
}
