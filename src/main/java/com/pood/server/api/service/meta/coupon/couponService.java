package com.pood.server.api.service.meta.coupon;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.IMP.*;
import com.pood.server.dto.meta.dto_coupon;
import com.pood.server.dto.meta.dto_coupon_2;

public interface couponService {


 
    
    /******************** 쿠폰 목록 조회 *********************/
    public List<dto_coupon_2> getCouponList() throws Exception;




    /******************** 회원 가입시 나눠주는 쿠폰 리스트 조회 ****************/
    public List<dto_coupon_2> getWelcomeCouponList() throws Exception;

     
 
 

    /************* 쿠폰 항목 번호에 해당하는 쿠폰 오브젝트 조회 *******************/
    public dto_coupon_2 getCouponObject(Integer COUPON_IDX) throws Exception;




    /************* 쿠폰 항목 번호에 해당하는 쿠폰 정보 조회 *******************/
    public dto_coupon getCoupon(Integer COUPON_IDX) throws Exception;


 
 


    /*************** 중복 쿠폰 정보 가져오기 *********************/
    public IMP_OVER_COUPON getOverCoupon(Integer over_coupon_idx) throws Exception;

 


     /**************** 쿠폰 항목 개수 조회 ******************/
    public Integer getTotalRecordNumber() throws SQLException;

  
    
    public List<dto_coupon_2> getCouponList3() throws Exception;

    public List<dto_coupon_2> getCouponList5(Integer GOODS_IDX, Integer BRAND_IDX) throws Exception;

    public dto_coupon getRecord(String code) throws Exception ;
}
