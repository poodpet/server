package com.pood.server.api.service.pay;

import com.pood.server.api.req.header.pay.OrderCancelRequest;
import com.pood.server.api.req.header.pay.hPay_cancel_1_2;
import com.pood.server.object.req_retreieve;
import com.pood.server.service.factory.OrderCancelPriceInfo;
import java.util.List;

public interface retreieveService {

    public List<String>     rollbackOrderBasketWithGoodsIDX(Integer CANCEL_GOODS_IDX, Integer PURCHASED_QTY, String MERCHANT_UID) throws Exception;

    public List<String>     rollbackOrderBasket(Integer ORDER_IDX) throws Exception;

    public String           requestRefund(String IAMPORT_ACCESS_TOKEN, Integer amount, String merchant_uid, String requestText) throws Exception;

    public String           requestRefund(String IAMPORT_ACCESS_TOKEN, Long amount, String merchant_uid, String requestText) throws Exception;

    public String           getToken() throws Exception;

    // 전체 취소
    public Integer          CANCEL_ALL(String  MERCHANT_UID, String  REQUEST_TEXT) throws Exception;
    
    
    public Integer          CANCEL_CALLBACK(String  MERCHANT_UID, String  REQUEST_TEXT) throws Exception;


    public Integer          doRetreieve(req_retreieve REQ_REFUND ) throws Exception;


    // 부분 취소
    public req_retreieve getRetreieveObject(
        String  MERCHANT_UID,                   // 주문 번호
        String  REQUEST_TEXT,                   // 환불 요청 사항
        Integer CANCEL_GOODS_IDX,               // 주문시 굿즈 항목 번호
        Integer CANCEL_GOODS_PRICE,             // 주문시 굿즈 가격
        Integer CANCEL_GOODS_QTY,               // 주문시 굿즈 개수
        Boolean IS_REFUND,                      // 0 : 일반 취소, 1: 반품
        String  REFUND_UUID,                    // 환불 고유 번호
        Integer ATTR_TYPE                       // (반품인 경우) 0 : 구매자 귀책, 1 : 판매자 귀책
        ) throws Exception;
 

        
    public Integer REFUND_ALL(
        String  MERCHANT_UID,                   // 주문 번호
        String  REQUEST_TEXT,                   // 환불 요청 사항
        Boolean IS_REFUND,                      // 0 : 일반 취소, 1: 반품
        String  REFUND_UUID,                    // 환불 고유 번호
        Integer ATTR_TYPE                       // (반품인 경우) 0 : 구매자 귀책, 1 : 판매자 귀책
        ) throws Exception;

    int doRetreieve(final OrderCancelRequest orderCancelRequest, final hPay_cancel_1_2 cancelDate,
        final Integer cancelGoodsPrice, final OrderCancelPriceInfo orderCancelPriceInfo,
        final long completePoint) throws Exception;

}
