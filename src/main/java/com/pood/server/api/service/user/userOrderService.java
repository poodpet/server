package com.pood.server.api.service.user;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.object.resp.resp_user_order;

public interface userOrderService {
    
    /************** 회원 주문 목록 조회 *****************/
    public List<resp_user_order> getUserOrderList(pagingSet PAGING_SET, Integer user_idx, String order_number) throws Exception;
    

    /************** 회원 주문 목록 전체 레코드 개수 조회 ************/
    public Integer getTotalRecordNumber(Integer user_idx, String order_number) throws SQLException;
}
