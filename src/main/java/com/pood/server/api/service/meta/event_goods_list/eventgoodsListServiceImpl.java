package com.pood.server.api.service.meta.event_goods_list;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.pood.server.config.DATABASE;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.event.dto_event_goods_list;
import com.pood.server.dto.meta.event.dto_event_goods_list_2;
import com.pood.server.dto.meta.goods.dto_goods_10;
import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.meta.goods.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import org.springframework.stereotype.Service;

@Service("eventgoodsListService")
public class eventgoodsListServiceImpl implements eventgoodsListService {

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;
 
    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_EVENT_GOODS_LIST;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
 
    @Override
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @SuppressWarnings("unchecked")
    public List<dto_event_goods_list> getEventgoodsList(pagingSet PAGING_SET) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_event_goods_list>)listService.getDTOList(SELECT_QUERY.toString(), dto_event_goods_list.class);
    }
 
    @SuppressWarnings("unchecked")
    public List<dto_event_goods_list_2> getItemList(Integer EVENT_IDX) throws Exception {
    

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("event_idx", EVENT_IDX);
        
        List<dto_event_goods_list> list = (List<dto_event_goods_list>)listService.getDTOList(SELECT_QUERY.toString(), dto_event_goods_list.class);

        List<dto_event_goods_list_2> result = new ArrayList<dto_event_goods_list_2>();
        
        if(list != null){
            for(dto_event_goods_list e : list){
                dto_goods_10 GOODS_INFO = goodsService.GET_GOODS_OBJECT_3(e.getGoods_idx());

                if(GOODS_INFO != null){

                    dto_event_goods_list_2 record = new dto_event_goods_list_2();
                    record.setIdx(e.getIdx());
                    record.setEvent_idx(e.getEvent_idx());
                    record.setGoods_info(GOODS_INFO);
                    record.setPriority(e.getPriority());
                    result.add(record);
                    record = null;

                }
                
                GOODS_INFO = null;
            }

        }

        return result;
    }

    
    
}
