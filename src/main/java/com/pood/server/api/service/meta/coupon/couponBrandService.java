package com.pood.server.api.service.meta.coupon;

import java.util.List;

import com.pood.server.dto.meta.dto_coupon_brand;

public interface couponBrandService {

    public List<dto_coupon_brand> getList() throws Exception;

    public List<Integer> getBrandIDXList(Integer OVER_COUPON_IDX) throws Exception;
    
}
