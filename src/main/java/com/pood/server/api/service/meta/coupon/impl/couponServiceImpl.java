package com.pood.server.api.service.meta.coupon.impl;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.meta.coupon.couponBrandService;
import com.pood.server.api.service.meta.coupon.couponGoodsService;
import com.pood.server.api.service.meta.coupon.couponService;
import com.pood.server.api.service.meta.goods.goodsCouponService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.coupon.COUPON_TYPE;
import com.pood.server.dto.meta.dto_coupon;
import com.pood.server.dto.meta.dto_coupon_2;
import com.pood.server.dto.meta.dto_coupon_brand;
import com.pood.server.dto.meta.dto_coupon_goods;
import com.pood.server.object.IMP.IMP_OVER_COUPON;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("couponService")
public class couponServiceImpl implements couponService {

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;
 
    @Autowired
    @Qualifier("goodsCouponService")
    goodsCouponService goodsCouponService;

    @Autowired
    @Qualifier("couponBrandService")
    couponBrandService couponBrandService;

    @Autowired
    @Qualifier("couponGoodsService")
    couponGoodsService couponGoodsService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_COUPON;
   
    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    /************* 쿠폰 항목 번호에 해당하는 쿠폰 오브젝트 조회 *******************/
    public dto_coupon_2 getCouponObject(Integer COUPON_IDX) throws Exception {

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", COUPON_IDX);
        
        dto_coupon_2 record = (dto_coupon_2)listService.getDTOObject(SELECT_QUERY.toString(), dto_coupon_2.class);
 
        List<dto_coupon_brand> COUPON_BRAND_LIST = couponBrandService.getList();
        
        List<dto_coupon_goods> COUPON_GOODS_LIST = couponGoodsService.getList();

        if(record != null){
            List<Integer> GOODS_LIST = goodsCouponService.getgoodsList(COUPON_IDX);
            record.setGoods_idx(GOODS_LIST);
            GOODS_LIST = null;

            Integer coupon_idx  = record.getIdx();

            Integer coupon_type = record.getPublish_type_idx();

            List<Integer> list_cb = new ArrayList<Integer>();

            List<Integer> list_cg = new ArrayList<Integer>();

            // 해당 쿠폰이 브랜드 쿠폰인 경우 사용 가능한 브랜드 리스트 조회
            if(coupon_type == COUPON_TYPE.BRAND_COUPON){
                for(dto_coupon_brand e1 : COUPON_BRAND_LIST){
                    if(coupon_idx.equals(e1.getCoupon_idx())){
                        Integer brand_idx = e1.getBrand_idx();
                        list_cb.add(brand_idx);
                        brand_idx = null;
                    }
                }
            }

            // 해당 쿠폰이 단품 쿠폰인 경우 사용 가능한 굿즈 리스트 조회
            if(coupon_type == COUPON_TYPE.GOODS_COUPON){
                for(dto_coupon_goods e1 : COUPON_GOODS_LIST){
                    if(coupon_idx.equals(e1.getCoupon_idx())){
                        Integer goods_idx = e1.getGoods_idx();
                        list_cg.add(goods_idx);
                        goods_idx = null;
                    }
                }
            }

            record.setBrand_idx(list_cb);
            
            record.setGoods_idx(list_cg);

        }

        COUPON_BRAND_LIST = null;

        COUPON_GOODS_LIST = null;

        
        return record;
    }

  

    /************* 쿠폰 항목 번호에 해당하는 쿠폰 정보 조회 *******************/
    public dto_coupon getCoupon(Integer COUPON_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", COUPON_IDX);
        return (dto_coupon)listService.getDTOObject(SELECT_QUERY.toString(), dto_coupon.class);
    }

    /*************** 쿠폰 정보 가져오기 *********************/
    public IMP_OVER_COUPON getOverCoupon(Integer over_coupon_idx) throws Exception {

        IMP_OVER_COUPON IMP_OVER_COUPON = new IMP_OVER_COUPON();
        IMP_OVER_COUPON.setOVER_COUPON_IDX(over_coupon_idx);

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", over_coupon_idx);

        dto_coupon record = (dto_coupon)listService.getDTOObject(SELECT_QUERY.toString(), dto_coupon.class);

        if(record != null){
            IMP_OVER_COUPON.setOVER_COUPON_TYPE(record.getType());
            IMP_OVER_COUPON.setOVER_COUPON_DISCOUNT_RATE(record.getDiscount_rate());
            IMP_OVER_COUPON.setOVER_COUPON_MAX_PRICE(record.getMax_price());
            IMP_OVER_COUPON.setOVER_COUPON_LIMIT_PRICE(record.getLimit_price());
            IMP_OVER_COUPON.setOVER_COUPON_NAME(record.getName());
            IMP_OVER_COUPON.setOVER_COUPON_PUBLISH_TYPE(record.getPublish_type_idx());
        }
        
        record = null;
        
        return IMP_OVER_COUPON;

    }
    
    /******************** 쿠폰 목록 조회 *********************/
    @SuppressWarnings("unchecked")
    public List<dto_coupon_2> getCouponList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        List<dto_coupon_2> list = (List<dto_coupon_2>)listService.getDTOList(SELECT_QUERY.toString(), dto_coupon_2.class);

        List<dto_coupon_brand> COUPON_BRAND_LIST = couponBrandService.getList();
        
        List<dto_coupon_goods> COUPON_GOODS_LIST = couponGoodsService.getList();

        for(dto_coupon_2 e : list){

            Integer coupon_idx  = e.getIdx();

            Integer coupon_type = e.getPublish_type_idx();

            List<Integer> list_cb = new ArrayList<Integer>();

            List<Integer> list_cg = new ArrayList<Integer>();

            // 해당 쿠폰이 브랜드 쿠폰인 경우 사용 가능한 브랜드 리스트 조회
            if(coupon_type == COUPON_TYPE.BRAND_COUPON){
                for(dto_coupon_brand e1 : COUPON_BRAND_LIST){
                    if(coupon_idx.equals(e1.getCoupon_idx())){
                        Integer brand_idx = e1.getBrand_idx();
                        list_cb.add(brand_idx);
                        brand_idx = null;
                    }
                }
            }

            // 해당 쿠폰이 단품 쿠폰인 경우 사용 가능한 굿즈 리스트 조회
            if(coupon_type == COUPON_TYPE.GOODS_COUPON){
                for(dto_coupon_goods e1 : COUPON_GOODS_LIST){
                    if(coupon_idx.equals(e1.getCoupon_idx())){
                        Integer goods_idx = e1.getGoods_idx();
                        list_cg.add(goods_idx);
                        goods_idx = null;
                    }
                }
            }

            e.setBrand_idx(list_cb);
            
            e.setGoods_idx(list_cg);

            list_cb     = null;

            list_cg     = null;

            coupon_type = null;

            coupon_idx  = null;
        }

        COUPON_BRAND_LIST = null;

        COUPON_GOODS_LIST = null;

        return list;
    }

    /**************** 쿠폰 항목 개수 조회 ******************/
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    

    /******************** 쿠폰 목록 조회 *********************/
    @SuppressWarnings("unchecked")
    public List<dto_coupon_2> getCouponList3()
            throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);

        List<dto_coupon_2> result = new ArrayList<dto_coupon_2>();

        List<dto_coupon_2> list = (List<dto_coupon_2>)listService.getDTOList(SELECT_QUERY.toString(), dto_coupon_2.class);
        if(list != null){
            for(dto_coupon_2 e : list){
                List<Integer> GOODS_LIST = goodsCouponService.getgoodsList(e.getIdx());
                e.setGoods_idx(GOODS_LIST); 
                result.add(e);
            }
        } 

        list = null;


        return result;
    }
 

    /******************** 쿠폰 목록 조회 *********************/
    @SuppressWarnings("unchecked")
    public List<dto_coupon_2> getCouponList5(Integer GOODS_IDX, Integer BRAND_IDX) throws Exception {
        
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);

        List<dto_coupon_2> list = (List<dto_coupon_2>)listService.getDTOList(SELECT_QUERY.toString(), dto_coupon_2.class);

        List<dto_coupon_2> result = new ArrayList<dto_coupon_2>();

        if(list != null){
            for(dto_coupon_2 e : list){
 
                // 쿠폰에 매핑된 굿즈 정보 조회
                List<Integer> GOODS_LIST = goodsCouponService.getgoodsList(e.getIdx());
                e.setGoods_idx(GOODS_LIST);
 
                Boolean isInBrand = false;
                Boolean isIngoods = false;

                // 파라미터로 굿즈 항목번호가 들어올경우, 특정 굿즈에 해당하는 쿠폰만 조회
                if (GOODS_IDX != null)
                    if (GOODS_LIST.contains(GOODS_IDX))
                        isIngoods = true;

                if (isInBrand || isIngoods){
                    result.add(e);
                }
            }
        } 

        list = null;

        return result;
    }

    /******************** 회원 가입시 나눠주는 쿠폰 리스트 조회 ****************/
    @SuppressWarnings("unchecked")
    public List<dto_coupon_2> getWelcomeCouponList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("publish_type_idx", COUPON_TYPE.WELCOME_COUPON);
        SELECT_QUERY.buildASC("max_price");
        return (List<dto_coupon_2>)listService.getDTOList(SELECT_QUERY.toString(), dto_coupon_2.class);
    }
    
    public dto_coupon getRecord(String code) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("code", code);
        return (dto_coupon)listService.getDTOObject(SELECT_QUERY.toString(), dto_coupon.class);
    }
}
