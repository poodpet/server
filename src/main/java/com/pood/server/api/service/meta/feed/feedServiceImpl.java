package com.pood.server.api.service.meta.feed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
 

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;

import com.pood.server.api.service.list.*;
import com.pood.server.api.service.record.*;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_feed;
import com.pood.server.config.DATABASE;

@Service("feedService")
public class feedServiceImpl implements feedService {

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_FEED;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
    

    /********************** 상품 항목 번호에 해당하는 피드 목록 조회 ******************/
    @SuppressWarnings("unchecked")
    public List<dto_feed> getFeedList(Integer PRODUCT_IDX) throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("product_idx", PRODUCT_IDX);
        return (List<dto_feed>)listService.getDTOList(SELECT_QUERY.toString(), dto_feed.class);
    }

    

    /**************** 사료 목록 조회 ***************/
    @SuppressWarnings("unchecked")
    public List<dto_feed> getFeedList(pagingSet PAGING_SET, String feed_name, String brand, Integer product_idx)
            throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if (feed_name != null)
            SELECT_QUERY.buildLike("feed_name", feed_name);
        if (brand != null)
            SELECT_QUERY.buildLike("brand", brand);
        if  (product_idx != null)
            SELECT_QUERY.buildEqual("product_idx", product_idx);
            
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_feed>)listService.getDTOList(SELECT_QUERY.toString(), dto_feed.class);
    }

    /**************** 전체 사료 목록 개수 조회 ******************/
    public Integer getTotalRecordNumber(String feed_name, String brand, Integer product_idx) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if (feed_name != null)
            CNT_QUERY.buildLike("feed_name", feed_name);
        if (brand != null)
            CNT_QUERY.buildLike("brand", brand);
        if (product_idx != null)
            CNT_QUERY.buildEqual("product_idx", product_idx);
        return listService.getRecordCount(CNT_QUERY.toString());
    }



    @Override
    public dto_feed getFeedRecord(Integer PRODUCT_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("product_idx", PRODUCT_IDX);
        return (dto_feed)listService.getDTOObject(SELECT_QUERY.toString(), dto_feed.class);
    }



    @SuppressWarnings("unchecked")
    public List<dto_feed> getList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return (List<dto_feed>)listService.getDTOList(SELECT_QUERY.toString(), dto_feed.class);
    } 
}
