package com.pood.server.api.service.error;

import java.sql.SQLException;

public interface errorProcService {

    Integer insertRecord(String path, String error_msg, String stack_trace) throws SQLException;

}
