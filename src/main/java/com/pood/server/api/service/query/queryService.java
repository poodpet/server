package com.pood.server.api.service.query;

import java.util.Map;

public interface queryService {
 
    public String getInsertQuery(String db_name, String table_name, Map<String, Object> hashMap, Boolean isUpdate);
    
    public String getUpdateQuery(String db_name, String table_name, Map<String, Object> e, String key, String value);

    public String getDeductValueQuery(String db_name, String table_name, String key, Integer quantity, String column_key, String column_value);

    public String getAddValueQuery(String db_name, String table_name, String key, Integer quantity, String column_key, String column_value);
    
}
