package com.pood.server.api.service.meta.pet_doctor_desc;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.pood.server.api.service.meta.goods.*;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.queryBuilder.buildQuery;
import com.pood.server.config.DATABASE;
import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_pet_doctor_desc;
import com.pood.server.dto.meta.goods.dto_goods_5;
import com.pood.server.dto.meta.goods.dto_goods_product;
import com.pood.server.dto.meta.pet.dto_pet_doctor_desc;
import com.pood.server.dto.meta.product.dto_product_5;
import com.pood.server.api.service.meta.product.*;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.record.updateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("PETDoctorDescService")
public class PETDoctorDescServiceImpl implements PETDoctorDescService {

    @Autowired
    @Qualifier("productService")
    productService productService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("goodsProductService")
    goodsProductService goodsProductService;

    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_PET_DOCTOR_FEED_DESC;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);


    /**************** 수의사 사료 설명 목록 조회 *****************/
    @SuppressWarnings("unchecked")
    public List<dto_pet_doctor_desc> getPetDoctorDescList(pagingSet PAGING_SET, Integer product_idx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if(product_idx != null)
            SELECT_QUERY.buildEqual("product_idx", product_idx);
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_pet_doctor_desc>)listService.getDTOList(SELECT_QUERY.toString(), dto_pet_doctor_desc.class);
    }

    /**************** 수의사 사료 설명 목록 조회 *****************/
    @SuppressWarnings("unchecked")
    public List<dto_pet_doctor_desc> getList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return (List<dto_pet_doctor_desc>)listService.getDTOList(SELECT_QUERY.toString(), dto_pet_doctor_desc.class);
    }
    
    /**************** 전체 목록 개수 조회 ******************/
    public Integer getTotalRecordNumber(Integer product_idx) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        CNT_QUERY.buildEqual("product_idx", product_idx);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @Override
    public dto_pet_doctor_desc getRecord(Integer product_idx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if(product_idx != null)
            SELECT_QUERY.buildEqual("product_idx", product_idx);
        return (dto_pet_doctor_desc)listService.getDTOObject(SELECT_QUERY.toString(), dto_pet_doctor_desc.class);
    }
 
 
    @SuppressWarnings("unchecked")
    public List<dto_product_5> getListPositionOrder(Integer page_number, Integer page_size, buildQuery query) throws Exception {
        if((page_size != null) && (page_number != null)){
            Integer offset = (page_number - 1) * page_size;
            query.buildLimit(page_size, offset);
        }

        List<dto_product_5> result = new ArrayList<dto_product_5>();

        List<dto_pet_doctor_desc> list = (List<dto_pet_doctor_desc>)listService.getDTOList(query.toString(), dto_pet_doctor_desc.class);

        if(list != null){
            for(dto_pet_doctor_desc e : list){
                Integer PRODUCT_IDX = (Integer)e.getProduct_idx();
                dto_product_5 product_object = productService.getProductObject2(PRODUCT_IDX);
                result.add(product_object);
                product_object = null;
            }
        }
        
        return result;
    }

    @Override
    public Integer getTotalRecordNumber(String query) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY();
        CNT_QUERY.getQueryCount(query);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @SuppressWarnings("unchecked")
    public HashMap<Integer, vo_pet_doctor_desc> getObjectList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        List<dto_pet_doctor_desc> list = (List<dto_pet_doctor_desc>)listService.getDTOList(SELECT_QUERY.toString(), dto_pet_doctor_desc.class);
        HashMap<Integer, vo_pet_doctor_desc> result = new HashMap<Integer, vo_pet_doctor_desc>();

        Integer IDX = 0;

        if(list != null){
            for(dto_pet_doctor_desc e : list){

                vo_pet_doctor_desc PET_DOCTOR_DESC = new vo_pet_doctor_desc();

                Integer PET_DOCTOR_IDX      = (Integer)e.getPet_doctor_idx();
                Integer PRODUCT_IDX         = (Integer)e.getProduct_idx();
                String  PDFD_TITLE          = (String)e.getPdfd_title();
                String  PDFD_DESC           = (String)e.getPdfd_desc();
                String  PRODUCT_NAME        = (String)e.getProduct_name();
                Integer PC_ID               = (Integer)e.getPc_id();
                String  POSITION_1          = (String)e.getPosition_1();
                String  POSITION_2          = (String)e.getPosition_2();
                String  POSITION_3          = (String)e.getPosition_3();
                Integer ARD_GROUP_421       = (Integer)e.getArd_group_421();
                Integer ARD_GROUP_501       = (Integer)e.getArd_group_501();
                Integer ARD_GROUP_201       = (Integer)e.getArd_group_201();
                Integer ARD_GROUP_301       = (Integer)e.getArd_group_301();
                Integer ARD_GROUP_961       = (Integer)e.getArd_group_961();
                Integer ARD_GROUP_241       = (Integer)e.getArd_group_241();
                Integer ARD_GROUP_121       = (Integer)e.getArd_group_121();
                Integer ARD_GROUP_941       = (Integer)e.getArd_group_941();

                PET_DOCTOR_DESC.setPet_doctor_idx(PET_DOCTOR_IDX);           
                PET_DOCTOR_DESC.setProduct_idx(PRODUCT_IDX);
                PET_DOCTOR_DESC.setPdfd_title(PDFD_TITLE);
                PET_DOCTOR_DESC.setPdfd_desc(PDFD_DESC);
                PET_DOCTOR_DESC.setProduct_name(PRODUCT_NAME);
                PET_DOCTOR_DESC.setPc_id(PC_ID);
                PET_DOCTOR_DESC.setPosition_1(POSITION_1);
                PET_DOCTOR_DESC.setPosition_2(POSITION_2);
                PET_DOCTOR_DESC.setPosition_3(POSITION_3);
                PET_DOCTOR_DESC.setArd_group_421(ARD_GROUP_421);
                PET_DOCTOR_DESC.setArd_group_501(ARD_GROUP_501);
                PET_DOCTOR_DESC.setArd_group_201(ARD_GROUP_201);
                PET_DOCTOR_DESC.setArd_group_301(ARD_GROUP_301);
                PET_DOCTOR_DESC.setArd_group_961(ARD_GROUP_961);
                PET_DOCTOR_DESC.setArd_group_241(ARD_GROUP_241);
                PET_DOCTOR_DESC.setArd_group_121(ARD_GROUP_121);
                PET_DOCTOR_DESC.setArd_group_941(ARD_GROUP_941);

                PET_DOCTOR_IDX = null;
                PRODUCT_IDX = null;
                PDFD_TITLE = null;
                PDFD_DESC = null;
                PRODUCT_NAME = null;
                PC_ID = null;
                POSITION_1 = null;
                POSITION_2 = null;
                POSITION_3 = null;
                ARD_GROUP_421 = null;
                ARD_GROUP_501 = null;
                ARD_GROUP_201 = null;
                ARD_GROUP_301 = null;
                ARD_GROUP_961 = null;
                ARD_GROUP_241 = null;
                ARD_GROUP_121 = null;
                ARD_GROUP_941 = null;

                result.put(IDX++, PET_DOCTOR_DESC);
            }
        }
        
        return result;
    }

    public String getListQuery(String position, Integer ct_idx, Integer ct_sub_idx) throws SQLException {

        buildQuery query = new buildQuery();

        // 포지션 1에 해당되는 사료들 조회 
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildLike("position_1", position);
        if(ct_idx != null)
            SELECT_QUERY.buildEqual("ct_idx", ct_idx);
        if(ct_sub_idx != null)
            SELECT_QUERY.buildEqual("ct_sub_idx", ct_sub_idx);
        query.set(SELECT_QUERY.toString());
        
        
        // 포지션 2에 해당되는 사료들 조회 
        SELECT_QUERY.clear();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildLike("position_2", position);
        if(ct_idx != null)
            SELECT_QUERY.buildEqual("ct_idx", ct_idx);
        if(ct_sub_idx != null)
            SELECT_QUERY.buildEqual("ct_sub_idx", ct_sub_idx);
        query.buildUnion(SELECT_QUERY.toString());
        
        
        
        // 포지션 3에 해당되는 사료들 조회 
        SELECT_QUERY.clear();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildLike("position_3", position);
        if(ct_idx != null)
            SELECT_QUERY.buildEqual("ct_idx", ct_idx);
        if(ct_sub_idx != null)
            SELECT_QUERY.buildEqual("ct_sub_idx", ct_sub_idx);
        query.buildUnion(SELECT_QUERY.toString());


        return query.toString();
    }


    @SuppressWarnings("unchecked")
    public List<dto_goods_5> getList(pagingSet PAGING_SET, String query, Integer pc_idx)
            throws Exception {

        List<dto_pet_doctor_desc> list = (List<dto_pet_doctor_desc>)listService.getDTOList(query, dto_pet_doctor_desc.class);

        List<Integer> PRODUCT_IDX_LIST = new ArrayList<Integer>();
    
        List<Integer> GOODS_IDX_LIST = new ArrayList<Integer>();
    
        if(list != null){
            for(dto_pet_doctor_desc e : list)
                PRODUCT_IDX_LIST.add((Integer)e.getProduct_idx());
        }
    
        // 상품에 매핑된 전체 굿즈 목록 조회
        List<dto_goods_product>  list2 = goodsProductService.getList();
    
        for(Integer e1 : PRODUCT_IDX_LIST)
            for(dto_goods_product e2 : list2){
                if(e1.equals(e2.getProduct_idx())){
                    Integer goods_idx = e2.getGoods_idx(); 
                    if(!GOODS_IDX_LIST.contains(goods_idx))
                        GOODS_IDX_LIST.add(goods_idx);
                    goods_idx = null;
                }
            }

        return goodsService.GET_GOODS_LIST_2(PAGING_SET, GOODS_IDX_LIST, pc_idx);
    }
   
}