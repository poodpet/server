package com.pood.server.api.service.user.impl;

import com.pood.server.api.service.user.userTokenService;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.user.dto_user_token;

import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.record.*; 
import com.pood.server.api.service.log.*;
import com.pood.server.api.service.query.*;

@Service("userTokenService")
public class userTokenServiceImpl implements userTokenService{

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("logUserTokenService")
    logUserTokenService logUserTokenService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_USER_TOKEN;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
    
    @Override
    public String issueToken(String USER_UUID) throws Exception {
        
        String TOKEN = UUID.randomUUID().toString();
        
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", USER_UUID);

        dto_user_token record = (dto_user_token)listService.getDTOObject(SELECT_QUERY.toString(), dto_user_token.class);
        
        if(record == null){

            // 발급된 토큰이 없을 경우 신규로 토큰 생성 
            Map<String, Object> hashMap = new HashMap<String, Object>();
            hashMap.put("user_uuid", USER_UUID);
            hashMap.put("token",     TOKEN);
            
            String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);

            updateService.insert(query);

            query = null;

            logUserTokenService.insertRecord(USER_UUID, TOKEN);

        }else{

            // 토큰이 기존에 있을 경우 해당 토큰을 신규로 업데이트
            Integer TOKEN_IDX = record.getIdx();
            Map<String, Object> hashMap = new HashMap<String, Object>();
            hashMap.put("token",     TOKEN);

            String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", TOKEN_IDX.toString());
            updateService.execute(query);
            query = null;

            logUserTokenService.insertRecord(USER_UUID, TOKEN);

        }

        return TOKEN;
    }

    @Override
    public Boolean authentication(String USER_UUID, String TOKEN) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", USER_UUID);
        dto_user_token record = (dto_user_token)listService.getDTOObject(SELECT_QUERY.toString(), dto_user_token.class);
        if(record != null){
            if(TOKEN.equals(record.getToken()))
                return true;
        }
        
        return false;
    }

    
}
