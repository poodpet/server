package com.pood.server.api.service.noti;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.log.dto_log_user_notice_alaram;

public interface pushService {
    
    

    /***************** 메세지 발송 기록 삽입 ***************/
    public Integer insertRecord(Integer USER_IDX, String USER_UUID, Integer ALARAM_TYPE, String TITLE, String MESSAGE, String SCHEMA, String LINK_URL) throws SQLException;

	public List<dto_log_user_notice_alaram> getList(Integer user_idx) throws Exception;
}
