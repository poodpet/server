package com.pood.server.api.service.user;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.object.resp.resp_product_rating;
import com.pood.server.dto.dto_image_3;
import com.pood.server.dto.user.review.dto_user_review;
import com.pood.server.dto.user.review.dto_user_review_4;

public interface userReviewService {




    

    /***************** 특정 항목 번호에 대해서 실제 ROW NUMBER를 반환 ***************/
    public Integer getRowNumber(String recordbirth, Integer product_idx) throws SQLException;
    






    /******************* 전체 리뷰 목록 개수 조회 ************************/
    public Integer getTotalRecordNumber(Integer PRODUCT_IDX) throws SQLException;








    /********************* 회원 항목 번호에 해당하는 리뷰 목록 조회 *********************/
    public List<dto_user_review> getUserReviewList(Integer USER_IDX) throws Exception;
 







    /***************** 회원 리뷰를 업데이트 합니다. *************************/
    public void updateUserReview(Integer REVIEW_IDX, Integer RATING, String REVIEW_TEXT, Integer UPDATE_COUNT, Boolean IS_DELETE, Integer IS_VISIBLE) throws Exception;




    


    /* ******************** 리뷰의 보임 : 안보임 상태를 업데이트 합니다. **********************/
    public void updateReviewVisibleStatus(Integer REVIEW_IDX, Integer REVIEW_STATUS) throws SQLException;





    /****************** 회원 리뷰 이미지를 신규로 업데이트합니다. *****************/
    public void updateUserReviewImage(Integer REVIEW_IDX, String ORDER_NUMBER, Integer IMAGE_SEQUENCE, Integer IMAGE_IDX) throws SQLException;





    /**************** 회원 리뷰를 신규로 등록합니다. ************************/
    public Integer InsertUserReview(Integer USER_IDX, Integer PET_IDX, Integer GOODS_IDX, Integer RATING, String REVIEW_TEXT, String ORDER_NUMBER, Integer PRODUCT_IDX, String GOODS_NAME) throws Exception;







 
    /* ***************** PRODUCT_IDX, USER_UUID에 해당하는 리뷰 목록 조회 ****************/
    public List<dto_user_review_4> getReviewList(pagingSet PAGING_SET, Integer product_idx, String user_uuid) throws Exception;

    







    public List<dto_image_3> getUserReviewImageList(Integer REVIEW_IDX) throws Exception;






    /**************** 회원이 해당 굿즈을 리뷰 했는지 확인  *******************/ 
    public Boolean isReviewed(String ORDER_NUMBER, Integer GOODS_IDX, Integer USER_IDX) throws Exception;









    /**************** 회원이 해당 굿즈을 리뷰 했는지 확인  *******************/ 
    public Boolean isReviewed(String ORDER_NUMBER, Integer GOODS_IDX, Integer USER_IDX, Integer PRODUCT_IDX) throws Exception;

      








    /*************** 리뷰 항목번호에 해당하는 이미지 개수 조회 ****************/
    public Integer getUserReviewImageCount(Integer REVIEW_IDX) throws SQLException;



    /**************** 특정 상품 항목에 대해서 평점별 카운트 수 조회 *******************/
	public resp_product_rating getProductRating(Integer PRODUCT_IDX) throws Exception;




    public Integer insertImageRecord(String img_url) throws SQLException;






    public void deleteImageRecord(Integer e) throws Exception;
 




    
    public Integer getUserReviewCount(Integer USER_IDX) throws SQLException;






    public Double getGoodsRating(Integer REVIEW_CNT, Integer GOODS_IDX) throws Exception;






    public Integer getReviewCnt(Integer GOODS_IDX) throws Exception;






}
