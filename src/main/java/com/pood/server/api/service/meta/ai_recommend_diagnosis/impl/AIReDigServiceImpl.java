package com.pood.server.api.service.meta.ai_recommend_diagnosis.impl;

import java.sql.SQLException;
import java.util.List;


import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.config.DATABASE;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_ai_recommend_diagnosis;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.meta.ai_recommend_diagnosis.AIReDigService;
import com.pood.server.api.service.record.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("AIReDigService")
public class AIReDigServiceImpl implements AIReDigService {

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_AI_RECOMMEND_DIAGNOSIS;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);


    /***************** AI 질병 목록 조회 ****************/
    @SuppressWarnings("unchecked")
    public List<dto_ai_recommend_diagnosis> getAIRegDigList(pagingSet PAGING_SET) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME); 
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_ai_recommend_diagnosis>)listService.getDTOList(SELECT_QUERY.toString(), dto_ai_recommend_diagnosis.class);
    }


    
    /**************** 전체 질병 목록 항목 개수 조회 ************/
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME); 
        return listService.getRecordCount(CNT_QUERY.toString());
    }



    @Override
    public dto_ai_recommend_diagnosis getRecord(Integer aI_DIG_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", aI_DIG_IDX);
        return (dto_ai_recommend_diagnosis)listService.getDTOObject(SELECT_QUERY.toString(), dto_ai_recommend_diagnosis.class);
    }

}
