package com.pood.server.api.service.user;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.user.review.dto_user_review_clap;
import com.pood.server.object.user.vo_user_review_clap;

public interface userReviewClapService {
    
    /*************** 사용자 리뷰 도움이 있는지 확인 ******************/
    public vo_user_review_clap checkReviewClap(Integer USER_IDX, Integer REVIEW_IDX) throws Exception;

    /*************** 리뷰 도움 레코드 추가 ************************/
    public Integer insertReviewClap(Integer USER_IDX, Integer REVIEW_IDX) throws SQLException;

    /* 
    * 
    ****************** 회원이 도움 누른 리뷰 목록  ************************/
    public List<Integer> getReviewClapList(Integer user_idx) throws SQLException;

    

    /**************** 리뷰에 해당하는 리뷰 좋아요 수 조회 ******************/
    public Integer getReviewClapCount(Integer REVIEW_IDX) throws SQLException;

    public void deleteRecord(Integer REVIEW_CLAP_IDX) throws SQLException;

    public List<dto_user_review_clap> getList(Integer USER_IDX) throws Exception;
    

}
