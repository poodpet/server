package com.pood.server.api.service.meta.order;

import com.pood.server.dto.meta.order.dto_order_exchange;
import com.pood.server.dto.meta.order.dto_order_exchange_2;
import com.pood.server.object.meta.vo_order_exchange;
import java.sql.SQLException;
import java.util.List;

public interface orderExchangeService {

    public String insertRecord(String text, String orderNumber, int goodsIdx, int qty, int sendType,
        boolean causeAttributable) throws SQLException;

    public List<dto_order_exchange> getList(String orderNumber) throws Exception;

    public vo_order_exchange getRecord(String uuid) throws Exception;

    public void upddateStatus(int typeRetrieve, String uuid) throws SQLException;

    public List<dto_order_exchange_2> getList2(String orderNumber) throws Exception;

    public vo_order_exchange getRecord(Integer goodsIdx, String orderNumber) throws Exception;

}
