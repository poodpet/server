package com.pood.server.api.service.user.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.pood.server.api.service.user.*;
import com.pood.server.api.service.user.userOrderService;
import com.pood.server.config.DATABASE;
import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_order_delivery;
import com.pood.server.object.meta.vo_order_delivery_track;
import com.pood.server.object.meta.vo_order_info;
import com.pood.server.object.resp.resp_order_basket;
import com.pood.server.object.resp.resp_user_order;
import com.pood.server.object.user.vo_user_delivery_2;
import com.pood.server.dto.log.user_order.dto_log_user_order;
import com.pood.server.dto.meta.order.dto_order;
import com.pood.server.dto.meta.order.dto_order_cancel;
import com.pood.server.dto.meta.order.dto_order_exchange_2;
import com.pood.server.dto.meta.order.dto_order_history;
import com.pood.server.dto.meta.order.dto_order_refund_2;
import com.pood.server.dto.user.dto_user_coupon_2;
import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.log.logUserOrderService;
import com.pood.server.api.service.meta.order.*;
import com.pood.server.api.service.meta.payment_type.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("userOrderService")
public class userOrderServiceImpl implements userOrderService {

    @Autowired
    @Qualifier("logUserOrderService")
    logUserOrderService logUserOrderService;

    @Autowired
    @Qualifier("userDeliveryService")
    public userDeliveryService userDeliveryService;

    @Autowired
    @Qualifier("orderTypeService")
    public orderTypeService orderTypeService;

    @Autowired
    @Qualifier("orderDeliveryService")
    orderDeliveryService orderDeliveryService;

    @Autowired
    @Qualifier("orderDeliveryTrackService")
    orderDeliveryTrackService orderDeliveryTrackService;

    @Autowired
    @Qualifier("orderBasketService")
    public orderBasketService orderBasketService;

    @Autowired
    @Qualifier("userCouponService")
    public userCouponService userCouponService;

    @Autowired
    @Qualifier("orderService")
    public OrderService orderService;

    @Autowired
    @Qualifier("listService")
    public listService listService;

    @Autowired
    @Qualifier("PTypeService")
    PTypeService PTypeService;

    @Autowired
    @Qualifier("userBasketService")
    userBasketService userBasketService;

    @Autowired
    @Qualifier("orderCancelService")
    orderCancelService orderCancelService;

    @Autowired
    @Qualifier("orderExchangeService")
    orderExchangeService orderExchangeService;

    @Autowired
    @Qualifier("orderRefundService")
    orderRefundService orderRefundService;


    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_ORDER;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
    
    /************** 회원 주문 목록 조회 *****************/
    @SuppressWarnings("unchecked")
    public List<resp_user_order> getUserOrderList(pagingSet PAGING_SET, Integer user_idx, String order_number)
            throws Exception {
 
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_idx", user_idx);
        if (order_number != null)
            SELECT_QUERY.buildEqual("order_number", order_number);
        SELECT_QUERY.buildMoreThan("order_status", com.pood.server.config.meta.order.ORDER_STATUS.ORDER_SUCCESS);
        SELECT_QUERY.setPagination(PAGING_SET, true);

        List<resp_user_order> data = new ArrayList<resp_user_order>();
 
        List<dto_order> list = (List<dto_order>)listService.getDTOList(SELECT_QUERY.toString(), dto_order.class);
        if(list != null){
            for(dto_order e : list){

                resp_user_order record = new resp_user_order();

                record.setOrder_name(e.getOrder_name());
                record.setOrder_number(e.getOrder_number());
                record.setOrder_device(e.getOrder_device());
                record.setOrder_price(e.getOrder_price());
                record.setOrder_status(e.getOrder_status());
                record.setOrder_type(e.getOrder_type());
                record.setTotal_price(e.getTotal_price());
                
                record.setDiscount_coupon_price(e.getDiscount_coupon_price());
                record.setIdx(e.getIdx());
                record.setFree_purchase(e.getFree_purchase());
                record.setAddress_update(e.getAddress_update());
                record.setMemo(e.getMemo());
                record.setDelivery_type(e.getDelivery_type());
                record.setDelivery_number(e.getTracking_id());
                record.setSave_point(e.getSaved_point());

                // 결제 타입 이름 조회
                String ORDER_TYPE_NAME = PTypeService.getPaymentTypeName(e.getOrder_type());
                record.setOrder_type_name(ORDER_TYPE_NAME);

                String ORDER_NUMBER         = e.getOrder_number();


                // 주문 취소 목록 조회
                List<dto_order_cancel>          ORDER_CANCEL_LIST   = orderCancelService.getList(ORDER_NUMBER);

                // 주문 반품 목록 조회
                List<dto_order_refund_2>        ORDER_REFUND_LIST   = orderRefundService.getList2(ORDER_NUMBER);

                // 주문 교환 목록 조회
                List<dto_order_exchange_2>      ORDER_EXCHANGE_LIST = orderExchangeService.getList2(ORDER_NUMBER);

                record.setOrder_cancel(ORDER_CANCEL_LIST);
                record.setOrder_refund(ORDER_REFUND_LIST);
                record.setOrder_exchange(ORDER_EXCHANGE_LIST);
                record.setIdx(e.getIdx());
                record.setOrder_type(e.getOrder_type());
                record.setFree_purchase(e.getFree_purchase());
                record.setOrder_number(e.getOrder_number());
                record.setAddress_update(e.getAddress_update());
                record.setOrder_type(e.getOrder_type());
                record.setOrder_price(e.getOrder_price());
                record.setOrder_name(e.getOrder_name());
                record.setOrder_status(e.getOrder_status());
                record.setOrder_device(e.getOrder_device());
                record.setMemo(e.getMemo());
                record.setSave_point(e.getSaved_point());
 

                Integer OVER_COUPON_IDX = e.getOver_coupon_idx();

                dto_user_coupon_2 ORDER_COUPON = null;
                
                if(OVER_COUPON_IDX != -1)
                    ORDER_COUPON = userCouponService.getUserCoupon("idx", e.getOver_coupon_idx());


                // 주문 트래킹 정보를 포함한 히스토리 조회
                List<dto_order_history> ORDER_HISTORY = getOrderHistory(e.getOrder_number());
                
                vo_order_info ORDER_INFO = new vo_order_info();
                
                ORDER_INFO.setUsed_point(e.getUsed_point());
                ORDER_INFO.setDelivery_fee(e.getDelivery_fee());
                ORDER_INFO.setOrder_history(ORDER_HISTORY);
                ORDER_INFO.setTotal_pr_price(0);
                ORDER_INFO.setCoupon(ORDER_COUPON);
                ORDER_INFO.setTotal_discount_price(e.getDiscount_coupon_price());
                ORDER_INFO.setDelivery_fee(e.getDelivery_fee());


                vo_order_delivery ORDER_DELIVERY = orderDeliveryService.getRecord2(e.getOrder_number());
                ORDER_INFO.setOrder_delivery(ORDER_DELIVERY);

                record.setOrder_info(ORDER_INFO);
                

                // 주문 배송지 조회
                vo_user_delivery_2 USER_DELIVERY = orderDeliveryService.getRecord(e.getUser_uuid(), e.getIdx());
                record.setUser_delivery(USER_DELIVERY);
          

                // 주문에 해당하는 굿즈 정보 조회
                List<resp_order_basket> GOODS_LIST = orderBasketService.getgoodsList(e.getIdx());
                record.setGoods(GOODS_LIST);
                GOODS_LIST = null;


                data.add(record);

                record = null;
                
                USER_DELIVERY           = null;
                ORDER_COUPON            = null;
                ORDER_HISTORY           = null;
                ORDER_CANCEL_LIST       = null;
                ORDER_REFUND_LIST       = null;
                ORDER_EXCHANGE_LIST     = null;

            }
        } 

        return data;
    }

    /************** 회원 주문 목록 전체 레코드 개수 조회 ************/
    public Integer getTotalRecordNumber(Integer user_idx, String order_number) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        CNT_QUERY.buildEqual("user_idx", user_idx);
        if (order_number != null)
            CNT_QUERY.buildEqual("order_number", order_number);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    
    public List<dto_order_history> getOrderHistory(String ORDER_NUMBER) throws Exception{


        List<dto_order_history> list = new ArrayList<dto_order_history>();

        List<vo_order_delivery_track> DELIVERY_TRACK_INFO = 
            orderDeliveryTrackService.getOrderDeliveryInfo2(ORDER_NUMBER);


        List<dto_log_user_order> ORDER_HISTORY = 
            logUserOrderService.getOrderHistoryList2(ORDER_NUMBER);


        if(ORDER_HISTORY != null){
            for(dto_log_user_order e : ORDER_HISTORY){
                dto_order_history record = new dto_order_history();
                record.setObject(e);
                
                List<vo_order_delivery_track> e2 = new ArrayList<vo_order_delivery_track>();

                for(vo_order_delivery_track e1 : DELIVERY_TRACK_INFO){
                    if(e1.getHistory_idx().equals(e.getIdx()))
                        e2.add(e1);
                }

                record.setDelivery_track(e2);
        
                list.add(record);
                record = null;
                e2 = null;
            }
        }

        ORDER_HISTORY = null;

        DELIVERY_TRACK_INFO = null;

        return list;

    }

}
 