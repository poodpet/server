package com.pood.server.api.service.delivery.impl;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.delivery.remoteDeliveryService;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.log.logUserOrderService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.remote.REMOTE_TYPE;
import com.pood.server.dto.meta.dto_delivery_courier;
import com.pood.server.service.factory.DeliveryFeeFactory;
import com.pood.server.service.order.DeliveryFeeCalc;
import com.pood.server.web.mapper.payment.Money;
import java.util.List;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("remoteDeliveryService")
public class remoteDeliveryServiceImpl implements remoteDeliveryService {

    Logger logger = LoggerFactory.getLogger(remoteDeliveryServiceImpl.class);

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("logUserOrderService")
    logUserOrderService logUserOrderService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_DELIVERY_COURIER;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);



    @Override
    public Integer checkDeliveryFee(Integer DELIVERY_REMOTE_TYPE, Integer DELIVERY_FEE, Integer ORDER_PRICE, String ZIPCODE, String USER_UUID) throws Exception{

        logger.info("*************** 배송금액 정산 *************");
        logger.info("주문 금액 : " + ORDER_PRICE+"원, 우편 번호 : "+ZIPCODE);


        Boolean deliveryFeeCheck = false;

        // 해당 고객의 결제 내역중에 결제 완료 내역이 있는 고객인지 체크
        final Boolean isPaySuccess = logUserOrderService.getPaySuccessCnt(USER_UUID);

        // 배송지가 도서 산간 지역 인지
        final boolean isIsNotlandsDelivery = Objects.equals(DELIVERY_REMOTE_TYPE, REMOTE_TYPE.NOT_REMOTE);
        final Integer isIslanddeliveryFee = getDeliveryFee(DELIVERY_REMOTE_TYPE, ZIPCODE);
        final DeliveryFeeCalc deliveryFeeFactory = DeliveryFeeFactory.create(isPaySuccess,
            isIsNotlandsDelivery, ORDER_PRICE);
        final Money deliveryFee = deliveryFeeFactory.calc(new Money(isIslanddeliveryFee));
        return deliveryFee.getIntValue();

    }




    // 도서산간 타입 및 우편 번호에 따라서 배송비를 가지고 옴
    @SuppressWarnings("unchecked")
    public Integer getDeliveryFee(Integer DELIVERY_REMOTE_TYPE, String ZIPCODE)throws Exception{

        Integer DELIVERY_FEE    =   0;

        Integer zipcode = Integer.parseInt(ZIPCODE);

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("area_type", DELIVERY_REMOTE_TYPE);

        List<dto_delivery_courier> list = (List<dto_delivery_courier>)listService.getDTOList(SELECT_QUERY.toString(), dto_delivery_courier.class);

        
        if(list != null){

            for(dto_delivery_courier e : list){
                Integer START_ZIPCODE   =   Integer.parseInt(e.getStart_zipcode());

                Integer END_ZIPCODE     =   Integer.parseInt(e.getEnd_zipcode());

                if((zipcode >= START_ZIPCODE) && (zipcode <= END_ZIPCODE))
                    DELIVERY_FEE =  e.getDelivery_fee();

                START_ZIPCODE   = null;

                END_ZIPCODE     = null;

            }

        }

        return DELIVERY_FEE;
    }


    @SuppressWarnings("unchecked")
    public Integer getRemoteType(String ZIPCODE) throws Exception{

        Integer DELIVERY_REMOTE_TYPE = REMOTE_TYPE.NOT_REMOTE;


        Integer zipcode = Integer.parseInt(ZIPCODE);

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);

        List<dto_delivery_courier> list = (List<dto_delivery_courier>)listService.getDTOList(SELECT_QUERY.toString(), dto_delivery_courier.class);


        if(list != null){

            for(dto_delivery_courier e : list){
                Integer START_ZIPCODE   =   Integer.parseInt(e.getStart_zipcode());

                Integer END_ZIPCODE     =   Integer.parseInt(e.getEnd_zipcode());

                if((zipcode >= START_ZIPCODE) && (zipcode <= END_ZIPCODE))
                    DELIVERY_REMOTE_TYPE =  e.getArea_type();

                START_ZIPCODE   = null;

                END_ZIPCODE     = null;

            }

        }

        return DELIVERY_REMOTE_TYPE;
    }
}
