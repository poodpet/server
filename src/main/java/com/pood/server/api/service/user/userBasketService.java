package com.pood.server.api.service.user;

import java.sql.SQLException;
 
import java.util.List;

import com.pood.server.dto.user.dto_user_basket;

public interface userBasketService {
    


    /*************** 회원 항목번호에 해당하는 장바구니 번호 목록 조회 *********************/
    public List<Integer> getUserBasketList(String user_uuid) throws Exception;



    /*********************** 장바구니 항목번호에 해당하는 장바구니 정보 조회 ************************/
    public dto_user_basket getUserBasket(Integer BASKET_IDX) throws Exception;



    /*********************** 장바구니 항목번호에 해당하는 장바구니 정보 조회 ************************/
    public dto_user_basket getUserBasket2(Integer BASKET_IDX) throws Exception;





    /********************** 굿즈이 이미 장바구니에 있는지 확인 **********************/
    public Integer checkIsInBasket(String USER_UUID, Integer GOODS_IDX, Integer QTY) throws Exception;




    /********************** 장바구니 항목 번호에 매칭되는 레코드의 주문 번호 업데이트 *************************/
    public void updateOrderNumberBasket(Integer BASKET_IDX, String ORDER_NUMBER) throws SQLException;
 


    /****************** 장바구니 항목 수량 업데이트 *********************/
    public void updateBasketQuantity(Integer BASKET_IDX, Integer BASKET_QTY) throws SQLException;


    
    
    /******************** 장바구니 항목 삽입 *********************/
    public Integer insertUserBasket(String USER_UUID, Integer USER_IDX, Integer GOODS_IDX, Integer PR_CODE_IDX, Integer QTY, String REGULAR_DATE, Integer GOODS_PRICE, Integer STATUS) throws SQLException;



    /****************** 장바구니 항목 상태 업데이트 ****************/
	public void updateBasketStatus(Integer BASKET_IDX, Integer STATUS) throws SQLException;



    /*************** 회원 항목번호에 해당하는 장바구니 목록 조회 *********************/
	public List<dto_user_basket> getUserBasketList2(Integer USER_IDX) throws Exception;




    /**************** 특정 장바구니 항목 삭제 ***********************/
	public void deleteUserBasket(Integer BASKET_IDX) throws SQLException;



    //  주문 번호에 해당하는 장바구니 항목번호 조회
	public Integer getBasketIDX(String ORDER_NUMBER) throws Exception;



	public Integer getUserBasketCount(String user_uuid) throws SQLException;



    public void deleteUserBasketWithUserUUID(String uSER_UUID) throws SQLException;


 
    public String GET_QUERY_UPDATE_BASKET_STATUS(Integer BASKET_IDX, Integer STATUS);
    
    public String GET_QUERY_UPDATE_ORDER_NUMBER_BASKET(Integer BASKET_IDX, String ORDER_NUMBER);



    public String GET_QUERY_DELETE_BASKET(Integer BASKET_IDX);

}
