package com.pood.server.api.service.view.view_6;

import java.util.List;

import com.pood.server.object.meta.maintab.vo_maintab_promotion;

public interface view6Service {

    public List<vo_maintab_promotion> getList() throws Exception;
    
}
