package com.pood.server.api.service.meta.promotion;

import java.util.List;

import com.pood.server.dto.meta.dto_promotion_group;

public interface promotionGroupService {

    public List<dto_promotion_group> getList() throws Exception;
    
}
