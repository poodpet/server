package com.pood.server.api.service.view.view_9_1;

import java.util.List;

import com.pood.server.object.meta.vo_promotion_group_goods;

public interface view91Service {
    
    public List<vo_promotion_group_goods> getList() throws Exception;
    
}
