package com.pood.server.api.service.meta.goods.impl;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.log.logProductHistoryService;
import com.pood.server.api.service.log.logProductOutOfStockService;
import com.pood.server.api.service.meta.goods.goodsProductService;
import com.pood.server.api.service.meta.goods.goodsService;
import com.pood.server.api.service.meta.pet_doctor_desc.PETDoctorDescService;
import com.pood.server.api.service.meta.product.productService;
import com.pood.server.api.service.meta.snack.snackService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.META_GOODS;
import com.pood.server.dto.meta.goods.dto_goods_product;
import com.pood.server.dto.meta.product.dto_product_1;
import com.pood.server.dto.meta.product.dto_product_4;
import com.pood.server.dto.meta.product.dto_product_5;
import com.pood.server.object.meta.vo_product;
import com.pood.server.object.meta.vo_snack;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


@Service("goodsProductService")
public class goodsProductServiceImpl implements goodsProductService {

    Logger logger = LoggerFactory.getLogger(goodsProductServiceImpl.class);

    @Autowired
    @Qualifier("listService")
    listService listService;  

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("productService")
    productService productService;

    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;

    @Autowired
    @Qualifier("snackService")
    snackService snackService;

    @Autowired
    @Qualifier("logProductHistoryService")
    logProductHistoryService logProductHistoryService;


    @Autowired
    @Qualifier("logProductOutOfStockService")
    logProductOutOfStockService logProductOutOfStockService;
    
    @Autowired
    @Qualifier("PETDoctorDescService")
    PETDoctorDescService PETDoctorDescService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_GOODS_PRODUCT;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
    
    /******************* 굿즈에 매핑된 상품 리스트 조회 ***********************/
    @SuppressWarnings("unchecked")
    public List<Integer> getgoodsProductList(Integer GOODS_IDX) throws Exception {
        List<Integer> PRODUCT_LIST = new ArrayList<Integer>();

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME,
                DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("goods_idx", GOODS_IDX);

        List<dto_goods_product> result = (List<dto_goods_product>)listService.getDTOList(SELECT_QUERY.toString(), dto_goods_product.class);

        if(result != null){
            for(dto_goods_product e : result){
                Integer PRODUCT_IDX = (Integer)e.getProduct_idx();
                PRODUCT_LIST.add(PRODUCT_IDX);
            }
        } 

        return PRODUCT_LIST;
    }

    /********************** 거래에 매핑되어 있는 상품 정보 조회 *********************/
    @SuppressWarnings("unchecked")
    public List<dto_product_4> getgoodsProductList2(Integer GOODS_IDX) throws Exception {

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME,
                DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("goods_idx", GOODS_IDX);

        ArrayList<dto_product_4> product_list = new ArrayList<dto_product_4>();

        List<dto_goods_product> rs = (List<dto_goods_product>)listService.getDTOList(SELECT_QUERY.toString(), dto_goods_product.class);
 
        if(rs != null){
            for (dto_goods_product e : rs) {
                Integer product_idx = (Integer)e.getProduct_idx();
                dto_product_4 product_object = productService.getProductObject(product_idx);
                product_list.add(product_object);
                product_idx = null;
                product_object = null;
            }
        }


        return product_list;
    }

    /********************** 거래에 매핑되어 있는 상품 정보 조회 *********************/
    @SuppressWarnings("unchecked")
    public List<dto_product_1> getgoodsProductList7(Integer GOODS_IDX) throws Exception {

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME,
                DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("goods_idx", GOODS_IDX);

        ArrayList<dto_product_1> product_list = new ArrayList<dto_product_1>();

        List<dto_goods_product> rs = (List<dto_goods_product>)listService.getDTOList(SELECT_QUERY.toString(), dto_goods_product.class);
 
        if(rs != null){
            for (dto_goods_product e : rs) {
                Integer product_idx = (Integer)e.getProduct_idx();
                dto_product_1 product_object = productService.getProductObject5(product_idx);
                product_list.add(product_object);
                product_idx = null;
                product_object = null;
            }
        }


        return product_list;
    }
 
    /********************** 거래에 매핑되어 있는 상품 정보 조회 *********************/
    @SuppressWarnings("unchecked")
    public List<dto_product_5> getgoodsProductList3(Integer GOODS_IDX)
            throws Exception {

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME,
                DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("goods_idx", GOODS_IDX);

        ArrayList<dto_product_5> product_list = new ArrayList<dto_product_5>();

        List<dto_goods_product> rs = (List<dto_goods_product>)listService.getDTOList(SELECT_QUERY.toString(), dto_goods_product.class);

        for (dto_goods_product e : rs) {
            
            Integer product_qty = e.getProduct_qty();

            Integer product_idx = e.getProduct_idx();

            dto_product_5 product_object = productService.getProductObject2(product_idx);

            if(product_object != null)
                product_object.setProduct_qty(product_qty);
            
            vo_snack record = snackService.getRecord(product_idx);
            if(record != null)
                product_object.setFeedingDetail(record);
            product_list.add(product_object);
            record = null;

            product_qty = null;

            product_idx = null;
            
        }

        return product_list;
    }
    
    @SuppressWarnings("unchecked")
    public List<dto_goods_product> getList(Integer GOODS_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("goods_idx", GOODS_IDX);
        return (List<dto_goods_product>)listService.getDTOList(SELECT_QUERY.toString(), dto_goods_product.class);
    }

    @SuppressWarnings("unchecked")
    public List<dto_goods_product> getListWithProductIDX(Integer PRODUCT_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("product_idx", PRODUCT_IDX);
        return (List<dto_goods_product>)listService.getDTOList(SELECT_QUERY.toString(), dto_goods_product.class);
    }

    @Override
    public List<Integer> getgoodsList(Integer PRODUCT_IDX) throws SQLException {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("product_idx", PRODUCT_IDX);
        List<Integer> result = listService.getIDXList(SELECT_QUERY.toString(), "goods_idx");
        return result;
    }

    @SuppressWarnings("unchecked")
    public List<dto_goods_product> getList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return (List<dto_goods_product>)listService.getDTOList(SELECT_QUERY.toString(), dto_goods_product.class);
    }
  

    @Override
    public Boolean isOutOfStock(Integer GOODS_IDX) throws Exception {

        Boolean result = false;

        List<dto_goods_product> list1 = getList(GOODS_IDX);
        if(list1 != null){
            for(dto_goods_product e1 : list1){

                
                Integer PRODUCT_IDX         = e1.getProduct_idx();              // 상품 항목 번호

                vo_product record = productService.getProduct(PRODUCT_IDX);

                Integer PRODUCT_QTY         = e1.getProduct_qty();              // 상품 구성품 수량
                Integer PRODUCT_QUANTITY    = record.getQuantity();             // 상품 재고
                
                logger.info("굿즈 항목번호 : " + GOODS_IDX + ", 상품 항목번호 : " + PRODUCT_IDX + ", 상품 재고 : " + record.getQuantity() + ", 상품 구성품 수량 : " + e1.getProduct_qty());

                if(e1.getProduct_qty() > record.getQuantity()){
                    doOutOfStock(GOODS_IDX,     PRODUCT_IDX,        PRODUCT_QTY,    PRODUCT_QUANTITY);
                    result = true;
                }

                record      = null;

                PRODUCT_QTY = null;

                PRODUCT_IDX = null;
            }
        }

        return result;
    }


    // 굿즈 수량 및 굿즈에 매핑된 상품 수량 차감
    public List<String> deductQtyWithGoodsIDX(Integer goodsIdx, Integer purchaseQuantity,
        Integer type, String text) throws Exception {

        List<String> transaction_process = new ArrayList<String>();
        // 주문이 되고나서 굿즈에 해당되는 상품의 수량 차감
        logger.info("굿즈 항목 번호 : " + goodsIdx + ", 굿즈 수량 : " + purchaseQuantity + "개 차감");

        List<dto_goods_product> list1 = getList(goodsIdx);
        if (list1 != null) {
            for (dto_goods_product e1 : list1) {

                Integer productIdx = e1.getProduct_idx();
                Integer goodsProductQty = e1.getProduct_qty();
                vo_product record = productService.getProduct(productIdx);
                Integer diff = (e1.getProduct_qty() * purchaseQuantity);
                logger.info(
                    "굿즈에 매핑된 상품[" + productIdx + "]의 개수 : " + goodsProductQty + ", 총 차감되는 재고 :"
                        + diff);

                // 상품 수량 감소, 출고 내역 추가
                String process1 = productService.GET_QUERY_DEDUCT_QTY(diff, productIdx);
                String process2 = logProductHistoryService.GET_QUERY_INSERT_RECORD(record.getUuid(),
                    diff, type, text);
                transaction_process.add(process1);
                transaction_process.add(process2);

                Integer productQty = record.getQuantity();

                if (productQty - diff == 0) {
                    doOutOfStock(goodsIdx, productIdx, goodsProductQty, productQty);
                }

                process1 = null;
                process2 = null;

                diff = null;
                record = null;
                goodsProductQty = null;

            }
        }

        return transaction_process;
    }
    




    // 굿즈 수량 및 굿즈에 매핑된 상품 수량 가감
    public List<String> addQtyWithGoodsIDX(Integer GOODS_IDX, Integer PURCHASE_QUANTITY, Integer TYPE, String TEXT) throws Exception {

        List<String> transaction_process = new ArrayList<String>();

        logger.info("굿즈 항목 번호 : "+ GOODS_IDX +", 굿즈 수량 : " + PURCHASE_QUANTITY + "개 가감");

        List<dto_goods_product> list1 = getList(GOODS_IDX);
        if(list1 != null){
            for(dto_goods_product e1 : list1){

                Integer PRODUCT_IDX = e1.getProduct_idx();
                
                vo_product record = productService.getProduct(PRODUCT_IDX);

                Integer DIFF = (e1.getProduct_qty() * PURCHASE_QUANTITY);

                logger.info("굿즈에 매핑된 상품[" + e1.getProduct_idx() + "]의 개수 : " + e1.getProduct_qty() + ", 총 가감되는 재고 : " + DIFF);

                String process_1 = productService.GET_QUERY_ADD_QTY(DIFF, PRODUCT_IDX);

                String process_2 = logProductHistoryService.GET_QUERY_INSERT_RECORD(record.getUuid(), DIFF, TYPE, TEXT);

                transaction_process.add(process_1);

                transaction_process.add(process_2);
            }
        }

        return transaction_process;
        
    }

 

    @Override
    public Integer getProductQTY(Integer PRODUCT_IDX) throws Exception {
        Integer PRODUCT_QTY = -1;
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("product_idx", PRODUCT_IDX);
        dto_goods_product record = (dto_goods_product)listService.getDTOObject(SELECT_QUERY.toString(), dto_goods_product.class);
        if(record != null)
            PRODUCT_QTY = record.getProduct_qty();
        return PRODUCT_QTY;
    }




    @Override
    public List<Integer> getProductIDXList(Integer GOODS_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("goods_idx", GOODS_IDX);
        List<Integer> result = listService.getIDXList(SELECT_QUERY.toString(), "product_idx");
        return result;
    }
 

    public void doOutOfStock(Integer GOODS_IDX, Integer PRODUCT_IDX, Integer PRODUCT_QTY, Integer PRODUCT_QUANTITY) throws Exception{
        List<String> transaction_process = new ArrayList<String>();

        logger.info("굿즈["+GOODS_IDX+"]의 재고가 모두 소진되어 판매 일시중지 처리합니다. ");

        String process_1 = logProductOutOfStockService.GET_QUERY_INSERT_RECORD(PRODUCT_IDX, GOODS_IDX, PRODUCT_QUANTITY, PRODUCT_QTY);

        String process_2 = goodsService.GET_QUERY_UPDATE_GOODS_SALE_STATUS(GOODS_IDX, META_GOODS.SALE_STATUS_HOLD);
        
        transaction_process.add(process_2);
        
        transaction_process.add(process_1);

        process_1 = null;
        
        process_2 = null;

        updateService.commit(transaction_process);
    }

}
