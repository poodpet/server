package com.pood.server.api.service.log;

public interface logProductOutOfStockService {

    public Integer insertRecord(Integer product_idx, Integer goods_idx, Integer product_qty, Integer quantity)throws Exception;

    public String GET_QUERY_INSERT_RECORD(Integer product_idx, Integer goods_idx, Integer product_qty, Integer quantity) throws Exception ;

}
