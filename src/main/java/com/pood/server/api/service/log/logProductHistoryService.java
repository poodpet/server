package com.pood.server.api.service.log;

public interface logProductHistoryService {

    Integer insertRecord(String uuid, Integer qty, Integer type, String text) throws Exception;

    String GET_QUERY_INSERT_RECORD(String uuid, Integer qty, Integer type, String text) throws Exception;

    
}
