package com.pood.server.api.service.meta.promotion;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.meta.vo_promotion_group_goods;

public interface promotionGroupGoodsService {

    public List<vo_promotion_group_goods> getList() throws Exception;

    public void updateGoodsRating(Integer review_cnt, Double average_rating, Integer goods_idx) throws SQLException;

    public vo_promotion_group_goods getPromotionGoodsInfo(Integer pr_idx, Integer goods_idx) throws Exception;
}
