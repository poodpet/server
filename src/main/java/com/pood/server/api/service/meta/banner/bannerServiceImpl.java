package com.pood.server.api.service.meta.banner;

import java.util.List;

import com.pood.server.api.service.record.*;
import com.pood.server.api.service.list.*;

import com.pood.server.config.DATABASE;
import com.pood.server.dto.dto_image_2;
import com.pood.server.dto.meta.dto_banner;
import com.pood.server.api.queryBuilder.SELECT_QUERY;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


@Service("bannerService")
public class bannerServiceImpl implements bannerService{
 
    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    private String DEFINE_TABLE_NAME        = DATABASE.TABLE_BANNER;

    private String DEFINE_IMAGE_TABLE_NAME  = DATABASE.TABLE_BANNER_IMAGE;
    
    private String DEFINE_DB_NAME           = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @SuppressWarnings("unchecked")
    public List<dto_banner> getList(Integer pc_id) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME); 
        if(pc_id != null)
            SELECT_QUERY.buildEqual("pc_id", pc_id);
        return (List<dto_banner>)listService.getDTOListWithImage2(
                SELECT_QUERY.toString(), "banner_idx", DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME, dto_banner.class, dto_image_2.class);
    }
}
