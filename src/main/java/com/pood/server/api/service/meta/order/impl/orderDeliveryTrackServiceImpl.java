package com.pood.server.api.service.meta.order.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

import com.pood.server.api.service.meta.order.orderDeliveryTrackService;
import com.pood.server.api.service.query.queryService;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.order.ORDER_DELIVERY_TRACK;
import com.pood.server.dto.meta.order.dto_order_delivery_track;
import com.pood.server.object.meta.vo_order_delivery_track;
import com.pood.server.api.queryBuilder.SELECT_QUERY;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.pood.server.api.service.record.updateService;
import com.pood.server.api.service.list.*;

import org.springframework.stereotype.Service;

@Service("orderDeliveryTrackService")
public class orderDeliveryTrackServiceImpl implements orderDeliveryTrackService {

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_ORDER_DELIVERY_TRACK_INFO;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @Override
    public Integer insertRecord(
            Integer GOODS_IDX, 
            Integer HISTORY_IDX, 
            String  ORDER_NUMBER,
            String  COURIER, 
            Integer TRACK_TYPE, 
            String  TRACKING_ID, 
            String  START_DATE,
            Integer SEND_TYPE,
            Integer QTY,
            Integer MAIN_ADDRESS) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("order_number", ORDER_NUMBER);
        hashMap.put("courier",      COURIER);
        hashMap.put("type",         TRACK_TYPE);
        hashMap.put("tracking_id",  TRACKING_ID);
        hashMap.put("startdate",   START_DATE);
        hashMap.put("send_type",    SEND_TYPE);
        hashMap.put("history_idx",  HISTORY_IDX);
        hashMap.put("goods_idx",    GOODS_IDX);
        hashMap.put("send",         ORDER_DELIVERY_TRACK.SEND_START);
        hashMap.put("qty",          QTY);
        hashMap.put("main_address", MAIN_ADDRESS);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }

    /************* 주문 번호에 해당하는 배송지 정보 조회 *************/
    @SuppressWarnings("unchecked")
    public List<dto_order_delivery_track> getOrderDeliveryInfo(String order_number) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", order_number);
        return (List<dto_order_delivery_track>)listService.getDTOList(SELECT_QUERY.toString(), dto_order_delivery_track.class);
    }

    @SuppressWarnings("unchecked")
    public List<vo_order_delivery_track> getOrderDeliveryInfo2(String ORDER_NUMBER) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", ORDER_NUMBER);
        return (List<vo_order_delivery_track>)listService.getDTOList(SELECT_QUERY.toString(), vo_order_delivery_track.class);
    }
    
 
}
