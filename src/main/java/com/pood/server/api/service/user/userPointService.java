package com.pood.server.api.service.user;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.user.dto_user_point;

public interface userPointService {
    
    public String insertRecord(
        String  USER_UUID,          // 회원 UUID
        String  ORDER_NUMBER,       // 구매 적립금인 경우 주문 번호
        Integer REVIEW_IDX,         // 리뷰 적립금인 경우 리뷰 항목 번호
        Integer POINT_IDX,          // 포인트 항목 번호
        String  POINT_NAME,         // 포인트 이름
        Integer POINT_TYPE,         // 0:적용 금액, 1: 적용 퍼센트
        Integer POINT_PRICE,        // 포인트 금액 
        Integer POINT_RATE,         // 포인트 비율
        String  EXPIRED_DATE,       // 포인트 만료기간
        Integer STATUS,             // 포인트 상태 
        Integer SAVED_POINT,        // 적립된 포인트
        Integer USED_POINT          // 사용되었으면 사용된 포인트
        ) throws SQLException;
 
    public dto_user_point getDueDatePoint(String USER_UUID) throws Exception;

	public void updateUsedPoint(Integer USED_POINT, String POINT_UUID) throws SQLException;

	public void updatePointStatus(Integer STATUS, String POINT_UUID) throws SQLException;

    public Boolean isUsePossible(String USER_UUID, Integer USE_POINT) throws Exception;

    public dto_user_point getRecord(String POINT_UUID) throws Exception;

    public dto_user_point getPointWithOrderNumber(String ORDER_NUMBER) throws Exception;

    public List<dto_user_point> getListWithUserUUID(String USER_UUID) throws Exception;

    public List<dto_user_point> getListWithUserUUID2(String USER_UUID) throws Exception;

    public void init(String user_uuid) throws Exception;

    public void deleteRecordWithUserUUID(String USER_UUID) throws SQLException;

    public Integer getToBeRemovedPoint(String USER_UUID) throws Exception;

    public Integer getFriendInvitationPoint(String USER_UUID) throws Exception;


    public String GET_QUERY_UPDATE_POINT_STATUS(Integer STATUS, String POINT_UUID);

    public String GET_QUERY_UPDATE_USED_POINT(Integer USED_POINT, String POINT_UUID);

    public String GET_QUERY_INSERT_RECORD(
        String  USER_UUID,          // 회원 UUID
        String  ORDER_NUMBER,       // 구매 적립금인 경우 주문 번호
        Integer REVIEW_IDX,         // 리뷰 적립금인 경우 리뷰 항목 번호
        Integer POINT_IDX,          // 포인트 항목 번호
        String  POINT_NAME,         // 포인트 이름
        Integer POINT_TYPE,         // 0:적용 금액, 1: 적용 퍼센트
        Integer POINT_PRICE,        // 포인트 금액 
        Integer POINT_RATE,         // 포인트 비율
        String  EXPIRED_DATE,       // 포인트 만료기간
        Integer STATUS,             // 포인트 상태 
        Integer SAVED_POINT,        // 적립된 포인트
        Integer USED_POINT,         // 사용되었으면 사용된 포인트
        String  POINT_UUID          // 적립금 UUID
        );

    public String GET_QUERY_UPDATE_SAVED_POINT(Integer TO_BE, String POINT_UUID);
}
