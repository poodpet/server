package com.pood.server.api.service.meta.coupon;

import java.sql.SQLException;

import com.pood.server.dto.meta.dto_coupon_code;

public interface couponCodeService {

    public Integer getTotalRecordNumber() throws SQLException;
 
    public dto_coupon_code getRecord(String code) throws Exception ;
    
}
