package com.pood.server.api.service.pay;

import java.sql.SQLException;

import com.pood.server.object.resp.resp_pay_result;

public interface payResultService {
  
    
    // 결제 결과 반환
	public resp_pay_result GET_PAYMENT_RESULT(String IMP_UID, String MERCHANT_UID) throws Exception;
 


    public resp_pay_result GET_PAY_AFTER_RESULT(String order_number, Boolean isSuccess, String msg, String IMP_UID) throws SQLException;


}
