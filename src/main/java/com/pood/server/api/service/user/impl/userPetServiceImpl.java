package com.pood.server.api.service.user.impl;

import com.pood.server.api.file.uploader.StorageService;
import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.meta.ai_recommend_diagnosis.AIReDigService;
import com.pood.server.api.service.meta.image.imageService;
import com.pood.server.api.service.meta.pet.PETService;
import com.pood.server.api.service.meta.sick_info.sickInfoService;
import com.pood.server.api.service.query.queryService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.api.service.user.userPetAIService;
import com.pood.server.api.service.user.userPetAllergyService;
import com.pood.server.api.service.user.userPetFeedService;
import com.pood.server.api.service.user.userPetGrainSizeService;
import com.pood.server.api.service.user.userPetService;
import com.pood.server.api.service.user.userPetSickService;
import com.pood.server.api.service.user.userService;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.dto_image_1;
import com.pood.server.dto.meta.dto_ai_recommend_diagnosis_2;
import com.pood.server.dto.meta.dto_allergy_data;
import com.pood.server.dto.meta.dto_grain_size;
import com.pood.server.dto.meta.dto_sick_info;
import com.pood.server.dto.meta.pet.dto_pet_2;
import com.pood.server.dto.user.pet.dto_user_pet;
import com.pood.server.dto.user.pet.dto_user_pet_ai_dig;
import com.pood.server.dto.user.pet.dto_user_pet_grain_size;
import com.pood.server.dto.user.pet.dto_user_pet_sick;
import com.pood.server.object.meta.vo_pet_act;
import com.pood.server.object.pagingSet;
import com.pood.server.object.resp.resp_user_pet;
import com.pood.server.object.user.vo_user_pet_allergy;
import com.pood.server.object.user.vo_user_pet_image;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("userPetService")
public class userPetServiceImpl implements userPetService {

    @Autowired
    @Qualifier("PETService")
    PETService PETService;

    @Autowired
    @Qualifier("AIReDigService")
    AIReDigService AIReDigService;
 
    @Autowired
    @Qualifier("sickInfoService")
    sickInfoService sickInfoService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;
    
    @Autowired
    @Qualifier("imageService")
    imageService imageService;

    @Autowired
    @Qualifier("userPetAIService")
    userPetAIService userPetAIService;

    @Autowired
    @Qualifier("userPetService")
    userPetService userPetService;
    
    @Autowired
    @Qualifier("userPetAllergyService")
    userPetAllergyService userPetAllergyService;

    @Autowired
    @Qualifier("userPetFeedService")
    userPetFeedService userPetFeedService;

    @Autowired
    @Qualifier("userPetGrainSizeService")
    userPetGrainSizeService userPetGrainSizeService;

    @Autowired
    @Qualifier("userPetSickService")
    userPetSickService userPetSickService;

    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    @Autowired
    @Qualifier("storageService")
    StorageService storageService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_USER_PET;

    private String DEFINE_IMAGE_TABLE_NAME = DATABASE.TABLE_USER_PET_IMAGE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    /***************** 회원 반려동물 정보 삽입 *********************/
    public Integer insertUserPet(Integer USER_IDX, Integer PET_ACTIVITY,
            Integer PC_ID, Integer PSC_ID, String PET_NAME, String PET_BIRTH, Integer PET_GENDER, Float PET_WEIGHT,
            Integer PET_STATUS) throws SQLException {
 
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_idx", USER_IDX);
    
        hashMap.put("pet_activity", PET_ACTIVITY);
        hashMap.put("pc_id", PC_ID);
        hashMap.put("psc_id", PSC_ID);
        hashMap.put("pet_name", PET_NAME);
        hashMap.put("pet_birth", PET_BIRTH);
        hashMap.put("pet_gender", PET_GENDER);
        hashMap.put("pet_weight", PET_WEIGHT);
        hashMap.put("pet_status", PET_STATUS);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }



    /***************** 회원 반려동물 정보 업데이트 *********************/
    public void updateUserPet(Integer USER_PET_IDX, Integer PET_ACTIVITY,
            String PET_NAME, String PET_BIRTH, Integer PET_GENDER, Float PET_WEIGHT, Integer PET_STATUS, Integer PC_ID, Integer PSC_ID)
            throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("pet_activity", PET_ACTIVITY);
        hashMap.put("pet_name", PET_NAME);
        hashMap.put("pet_birth", PET_BIRTH);
        hashMap.put("pet_gender", PET_GENDER);
        hashMap.put("pet_weight", PET_WEIGHT);
        hashMap.put("pet_status", PET_STATUS);
        hashMap.put("pc_id", PC_ID);
        hashMap.put("psc_id", PSC_ID);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", USER_PET_IDX.toString());
        updateService.execute(query);
        query = null;
    }


    
    /***************** 회원의 반려동물 목록 반환 *******************/
    @SuppressWarnings("unchecked")
    public List<resp_user_pet> getUserPetList(Integer user_idx, Integer user_pet_idx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_idx", user_idx);   
        if(user_pet_idx != null)
            SELECT_QUERY.buildEqual("idx", user_pet_idx);

        List<resp_user_pet> result = new ArrayList<resp_user_pet>();

        List<dto_user_pet> list =  
            (List<dto_user_pet>)listService.getDTOListWithImage1(
                SELECT_QUERY.toString(), "user_pet_idx", DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME, dto_user_pet.class, dto_image_1.class);
 
        List<dto_sick_info> SICK_LIST = sickInfoService.getSickInfoList(new pagingSet());

        for(dto_user_pet e :list){

            resp_user_pet record = new resp_user_pet();

            Integer USER_PET_IDX = e.getIdx();

            List<dto_user_pet_ai_dig> USER_PET_AI_DIG_LIST              = userPetAIService.getList(USER_PET_IDX);

            List<dto_user_pet_grain_size> USER_PET_GRAIN_SIZE_LIST      = userPetGrainSizeService.getList(USER_PET_IDX);

            List<vo_user_pet_allergy> USER_PET_ALLERGY_LIST                = userPetAllergyService.getList(USER_PET_IDX);

            List<dto_user_pet_sick> USER_PET_SICK_LIST                  = userPetSickService.getList(USER_PET_IDX);


            List<dto_ai_recommend_diagnosis_2> LIST_1   = new ArrayList<dto_ai_recommend_diagnosis_2>();

            List<dto_grain_size> LIST_2                 = new ArrayList<dto_grain_size>();

            List<dto_allergy_data> LIST_3               = new ArrayList<dto_allergy_data>();

            List<dto_sick_info> LIST_4                  = new ArrayList<dto_sick_info>();


            if(USER_PET_AI_DIG_LIST != null){
                for(dto_user_pet_ai_dig e1:USER_PET_AI_DIG_LIST){
                    dto_ai_recommend_diagnosis_2 tmp = new dto_ai_recommend_diagnosis_2();
                    tmp.setIdx(e1.getAi_diagnosis_idx());
                    tmp.setArd_name(e1.getArd_name());
                    tmp.setArd_group(e1.getArd_group());
                    tmp.setArd_group_code(e1.getArd_group_code());
                    LIST_1.add(tmp); 
                    tmp = null;

                }

                USER_PET_AI_DIG_LIST = null;
            }

    
            if(USER_PET_GRAIN_SIZE_LIST != null){
                for(dto_user_pet_grain_size e1:USER_PET_GRAIN_SIZE_LIST){
                    dto_grain_size tmp = new dto_grain_size();
                    tmp.setIdx(e1.getGrain_size_idx());
                    tmp.setName(e1.getName());
                    tmp.setSize_min(e1.getSize_min());
                    tmp.setSize_max(e1.getSize_max());
                    LIST_2.add(tmp); 
                    tmp = null;
                }

                USER_PET_GRAIN_SIZE_LIST = null;
            }

            if(USER_PET_ALLERGY_LIST != null){
                for(vo_user_pet_allergy e1 :USER_PET_ALLERGY_LIST){
                    dto_allergy_data tmp = new dto_allergy_data();
                    tmp.setIdx(e1.getAllergy_idx());
                    tmp.setName(e1.getName());
                    tmp.setType(e1.getType());
                    LIST_3.add(tmp); 
                    tmp = null;
                }

                USER_PET_ALLERGY_LIST = null;
            }


            if(USER_PET_SICK_LIST != null){
                for(dto_user_pet_sick e1:USER_PET_SICK_LIST){
                    for(dto_sick_info e2:SICK_LIST){
                        if(e1.getSick_idx().equals(e2.getIdx())){
                            LIST_4.add(e2);
                        }
                    }
                }
            }

            vo_pet_act USER_PET_ACT = new vo_pet_act();
            USER_PET_ACT.setPet_activity(e.getPet_activity());
            USER_PET_ACT.setPet_weight(e.getPet_weight());
            
            record.setIdx(USER_PET_IDX);
            record.setUser_idx(e.getUser_idx());
            record.setPc_id(e.getPc_id());
            record.setPet_act(USER_PET_ACT);
            record.setAi_diagnosis(LIST_1);
            record.setSick_info(LIST_4);
            record.setAllergy(LIST_3);
            record.setGrain_size(LIST_2);
            record.setPet_birth(e.getPet_birth());
            record.setPet_status(e.getPet_status());
            record.setPet_gender(e.getPet_gender());
            record.setPet_name(e.getPet_name());
            record.setImage(e.getImage());
            record.setUpdatetime(e.getUpdatetime());
            record.setRecordbirth(e.getRecordbirth());
            
            Integer PSC_IDX = e.getPsc_id();
            dto_pet_2 PET_INFO = PETService.getPetRecord(PSC_IDX);
            record.setPsc_info(PET_INFO);

            result.add(record);

        }
        
        return result;
    }
    

    /******************* 회원 펫 이미지를 신규로 등록 *********************************/
    public void updateUserPetImage(Integer USER_PET_IDX, Integer IMAGE_IDX)
            throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_pet_idx", USER_PET_IDX);
        hashMap.put("visible", com.pood.server.config.meta.user.USER_REVIEW.DEFAULT);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME, hashMap, "idx", IMAGE_IDX.toString());
        updateService.execute(query);
        query = null;
    }



    @Override
    public void deleteImage(Integer e) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", e);
        vo_user_pet_image record = (vo_user_pet_image)listService.getDTOObject(SELECT_QUERY.toString(), vo_user_pet_image.class);

        if(record != null){
            String url = record.getUrl();
            storageService.deleteObject(url);
            DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
            DELETE_QUERY.buildEqual("idx", Integer.toString(e));
            updateService.execute(DELETE_QUERY.toString());
        }

        record = null;
        
    }



    @Override
    public Integer insertImageRecord(String img_url) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("url", img_url);
        hashMap.put("visible", 0);
        hashMap.put("user_pet_idx", null);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME, hashMap,
            com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }

    @Override
    public void deleteRecord(Integer e) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("idx", Integer.toString(e));
        updateService.execute(DELETE_QUERY.toString());
    }



    @Override
    public void deleteImageWithUserPetIDX(Integer e) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_pet_idx", e);
        vo_user_pet_image record = (vo_user_pet_image)listService.getDTOObject(SELECT_QUERY.toString(), vo_user_pet_image.class);

        if(record != null){
        
            Integer idx = record.getIdx();

            String url = record.getUrl();
            storageService.deleteObject(url);
            DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
            DELETE_QUERY.buildEqual("idx", Integer.toString(idx));
            updateService.execute(DELETE_QUERY.toString());

        }


        record = null;
    } 
   
    @Override
    public Integer getUserPetCount(String USER_UUID) throws Exception {
        Integer USER_IDX = userService.getUserIDX(USER_UUID);
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        CNT_QUERY.buildEqual("user_idx", USER_IDX);
        Integer cnt = listService.getRecordCount(CNT_QUERY.toString());
        return cnt;
    }



    @Override
    public void deleteRecordWithUserUUID(String USER_UUID) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_uuid", USER_UUID);
        updateService.execute(DELETE_QUERY.toString());
    }



    @SuppressWarnings("unchecked")
    public List<dto_user_pet> getList(Integer USER_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_idx", USER_IDX);   
        List<dto_user_pet> list =  (List<dto_user_pet>)listService.getDTOList(SELECT_QUERY.toString(), dto_user_pet.class);
        return list;
    }
}
