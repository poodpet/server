package com.pood.server.api.service.meta.coupon.impl;

import com.pood.server.api.service.meta.coupon.couponBrandService;

import java.util.List;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.meta.dto_coupon_brand;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.record.*;

import org.springframework.stereotype.Service;

@Service("couponBrandService")
public class couponBrandServiceImpl implements couponBrandService{
 
    
    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_COUPON_BRAND;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @Override
    @SuppressWarnings("unchecked")
    public List<dto_coupon_brand> getList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return (List<dto_coupon_brand>)listService.getDTOList(SELECT_QUERY.toString(), dto_coupon_brand.class);
    }

    @Override
    public List<Integer> getBrandIDXList(Integer coupon_idx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("coupon_idx", coupon_idx);
        return listService.getIDXList(SELECT_QUERY.toString(), "brand_idx");
    }

}
