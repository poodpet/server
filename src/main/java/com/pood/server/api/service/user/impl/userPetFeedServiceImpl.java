package com.pood.server.api.service.user.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.user.userPetFeedService;
import com.pood.server.config.DATABASE;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.query.queryService;
import com.pood.server.api.service.record.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("userPetFeedService")
public class userPetFeedServiceImpl implements userPetFeedService{
    

    @Autowired
    @Qualifier("updateService")
    updateService updateService;
    
    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;
    
    private String DEFINE_TABLE_NAME = DATABASE.TABLE_USER_PET_FEED;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
    
     /******************* 회원 반려 동물 항목 번호에 해당하는 질병 번호 조회 ********************/
    public List<Integer> getList(Integer USER_PET_IDX) throws SQLException{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_pet_idx", USER_PET_IDX);
        return listService.getIDXList(SELECT_QUERY.toString(), "of_idx");
    }

    /******************** 회원 동물 AI 질병 정보 등록 ******************************/
    public Integer insertRecord(Integer USER_PET_IDX, Integer OF_IDX) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_pet_idx", USER_PET_IDX);
        hashMap.put("of_idx", OF_IDX);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }

    @Override
    public void deleteRecordWithUserPetIDX(Integer USER_PET_IDX) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_pet_idx", Integer.toString(USER_PET_IDX));
        updateService.execute(DELETE_QUERY.toString());
    }
}
