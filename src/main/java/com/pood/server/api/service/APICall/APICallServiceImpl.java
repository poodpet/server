package com.pood.server.api.service.APICall;

import java.time.Duration;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service("APICallService")
public class APICallServiceImpl implements APICallService {

    Logger logger = LoggerFactory.getLogger(APICallServiceImpl.class);

    @Override
    public ResponseEntity<String> post(MediaType header_type, HttpHeaders headers, MultiValueMap<String, String> body, String url) {
        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(body, headers);
        headers.setContentType(header_type);
        RestTemplate restTemplate = new RestTemplate();

        logger.info(entity.toString());
        ResponseEntity<String> HTTP_RESPONSE = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        return HTTP_RESPONSE;
    }

    @Override
    public ResponseEntity<String> post2(MediaType header_type, HttpHeaders headers, HashMap<String, Object> body, String url) {
        HttpEntity<HashMap<String, Object>> entity = new HttpEntity<HashMap<String, Object>>(body, headers);
        headers.setContentType(header_type);
        logger.info(entity.toString());
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> HTTP_RESPONSE = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        return HTTP_RESPONSE;
    }

    public ResponseEntity<String> get(HttpHeaders headers, String url) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        return result;
    }

    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder.setConnectTimeout(Duration.ofSeconds(1)).build();
    }

    public ResponseEntity<String> getFast(HttpHeaders headers, String url) {

        ResponseEntity<String> result = null;

        try {
            RestTemplate restTemplate = restTemplate(new RestTemplateBuilder());
            HttpEntity<String> entity = new HttpEntity<String>(null, headers);
            result = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return result;
    }
}
