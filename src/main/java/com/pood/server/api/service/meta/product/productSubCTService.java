package com.pood.server.api.service.meta.product;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.meta.product.dto_product_sub_ct;
import com.pood.server.object.pagingSet;

public interface productSubCTService {
    
     /************* 상품 하위 종류 항목 리스트를 조회합니다. ********************/
    public List<dto_product_sub_ct> getProductSubCTList(pagingSet PAGING_SET) throws Exception;

    /************* 상품 하위 종류 수를 조회합니다. ********************/
    public Integer  getTotalRecordNumber() throws SQLException;

	public String   getCTSubName(Integer CT_SUB_IDX) throws Exception;
    
    public Integer  getShowIndex(Integer CT_SUB_IDX) throws Exception;
    
}
