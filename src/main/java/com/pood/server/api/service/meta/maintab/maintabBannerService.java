package com.pood.server.api.service.meta.maintab;

import java.util.List;

import com.pood.server.object.meta.maintab.vo_maintab_banner;

public interface maintabBannerService {

    public List<vo_maintab_banner> getList() throws Exception;
    
}
