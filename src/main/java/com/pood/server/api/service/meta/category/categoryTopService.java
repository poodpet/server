package com.pood.server.api.service.meta.category;

import java.sql.SQLException;

import com.pood.server.object.resp.resp_category;

public interface categoryTopService {

    resp_category getList(Integer pc_id) throws Exception;

    Integer getTotalRecordNumber() throws SQLException;
    
}
