package com.pood.server.api.service.pet_rec;
import java.util.ArrayList;
import java.util.Calendar;

import java.util.HashMap;

import java.util.List;
import java.util.Collections;
import java.util.TreeMap;
import java.util.HashSet;
import java.util.Iterator;
import com.pood.server.object.meta.vo_grain_size;
//import com.pood.server.object.meta.sick_info;
import com.pood.server.object.meta.vo_ai_recommend_diagnosis;
//import com.pood.server.object.meta.allergy_data;

import com.pood.server.object.meta.vo_disease_group;

import com.pood.server.object.meta.vo_pet_doctor_desc;
import com.pood.server.object.meta.vo_product;

import com.pood.server.object.meta.vo_disease;
import com.pood.server.object.user.vo_user_pet2;

import com.pood.server.api.service.meta.product.*;
import com.pood.server.api.service.meta.disease.*;
import com.pood.server.api.service.meta.nuti.*;
import com.pood.server.api.service.meta.pet_doctor_desc.*;
import com.pood.server.api.service.meta.disease_group.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


public class PETRECServiceImpl implements PETRECService{

    @Autowired
    @Qualifier("diseaseService")
    diseaseService diseaseService;

    @Autowired
    @Qualifier("diseaseGroupService")
    diseaseGroupService diseaseGroupService;

    @Autowired
    @Qualifier("PETDoctorDescService")
    PETDoctorDescService PETDoctorDescService;

    @Autowired
    @Qualifier("productService")
    productService productService;

    @Autowired
    @Qualifier("nutiService")
    nutiService nutiService;

    @Override
    public ArrayList<Integer> getResult(vo_user_pet2 userPet) throws Exception{

        List<vo_product>                       productList         = productService.getObjectList();

        List<vo_disease>                       petDiseaseList      = diseaseService.getObjectList();

        List<vo_disease_group>                 diseaseGroupList    = diseaseGroupService.getObjectList();

        HashMap<Integer, vo_pet_doctor_desc>   productVetDesc      = PETDoctorDescService.getObjectList();

        ArrayList<Integer> result = new ArrayList<>();

        ArrayList<String> disease = new ArrayList<>();
        String dSp[] = userPet.getPsc_info().getPc_generic_weak().split(",");

        for (vo_disease pe : petDiseaseList){
            if(userPet.getPc_id() == pe.getPet_idx() && pe.getPc_ds_hospital() == 0){
                for (String d : dSp){
                    String pdsp[] = pe.getPc_ds_name().split(",");
                    for (String tmp : pdsp){
                        if(tmp.equals(d)){
                            disease.add(pe.getPc_ds_group());
                        }
                    }
                }
            }
        }
        for (vo_ai_recommend_diagnosis pad : userPet.getAi_diagnosis()) disease.add(pad.getArd_group());

        HashSet<String> nutrition = new HashSet<>();
        for (vo_disease_group dn : diseaseGroupList){
            if(userPet.getPc_id() == dn.getPet_idx()){
                for (String d : disease){
                    if(d.equals(dn.getDgn_group())) {
                        String sp [] = dn.getDgn_nutrition_core().split(",");
                        for (String ss : sp){
                            if(!ss.equals("-")) nutrition.add(ss);
                        }
                    }
                }
            }
        }

        int month = getNowDateMonth(userPet.getPet_birth().replace("-",""));
        ArrayList<vo_product> rsProduct = new ArrayList<>();
        for (vo_product p : productList){
            if(p.getPc_idx() == userPet.getPc_id() && p.getIs_recommend() == 1 && p.getCt_idx() == 0 && p.getBrand_idx() != 15){
                int matchCnt = 0;
                String itg[] = p.getIngredients_search().split(",");
                for (String ig : itg){
                    if(nutrition.contains(ig)) matchCnt++;
                }
                if(matchCnt>0){
                    boolean isFlag = true;
                    /*
                    for(allergy_data at : userPet.getAllergy()){
                        
                        if(at.getName().equals(p.getMain_property()) || p.getMoisture() >= 50f){
                            isFlag = false;
                            break;
                        }
                        
                    }
                    */
                    if(isFlag && p.getPc_idx() == 1){
                        switch (p.getFeed_type()){
                            case "P":
                                if(month > 12) isFlag = false;
                                break;
                            case "A":
                                if(month < 12 || month > 84) isFlag = false;
                                break;
                            case "S":
                                if(month < 84) isFlag = false;
                                break;
                            case "PA":
                                if(month > 84) isFlag = false;
                                break;
                            case "PAL":
                                if(month > 84 && userPet.getPsc_info().getPc_size().contains("대형")) isFlag = false;
                                break;
                            case "AS":
                                if(month < 12) isFlag = false;
                                break;
                        }
                    }else{
                        switch (p.getFeed_type()){
                            case "P":
                                if(month > 12) isFlag = false;
                                break;
                            case "A":
                                if(month < 12 || month > 84) isFlag = false;
                                break;
                            case "S":
                                if(month < 84) isFlag = false;
                                break;
                            case "PA":
                                if(month > 84) isFlag = false;
                            case "AS":
                                if(month < 12) isFlag = false;
                                break;
                        }
                    }
                    if(isFlag){
                        p.setMatchCnt(matchCnt);
                        rsProduct.add(p);
                    }
                }
            }
        }



        HashSet<String> diagnosis = new HashSet<>();

        for(String d : disease){
            if(d.contains("정형")) diagnosis.add("뼈/관절/근육");
            if(d.contains("피부")) diagnosis.add("피부/피모");
            if(d.contains("안과질환")) diagnosis.add("눈건강");
            if(d.contains("알레르기")) diagnosis.add("알러지/아토피");
            if(d.contains("비만")) diagnosis.add("다이어트");
            if(d.contains("활력")) diagnosis.add("활력증진");
            if(d.contains("치아")) diagnosis.add("치아건강");
            if(d.contains("비뇨기")) diagnosis.add("배변/배뇨");
        }

        TreeMap<Integer, ArrayList<vo_product>> aiDiagnosis = new TreeMap<>(Collections.reverseOrder());
        for (vo_product p : rsProduct){
            int matchCount = 0;
            if(diagnosis.contains(productVetDesc.get(p.getIdx()).getPosition_1())) matchCount++;
            if(diagnosis.contains(productVetDesc.get(p.getIdx()).getPosition_2())) matchCount++;
            if(diagnosis.contains(productVetDesc.get(p.getIdx()).getPosition_3())) matchCount++;

            if(matchCount > 0){
                if(aiDiagnosis.containsKey(matchCount)){
                    aiDiagnosis.get(matchCount).add(p);
                }else{
                    ArrayList<vo_product> list = new ArrayList<>();
                    list.add(p);
                    aiDiagnosis.put(matchCount, list);
                }
            }
        }

        ArrayList<vo_product> resultProduct = new ArrayList<>();
        for(int i=2;i>=0;--i){
            if(aiDiagnosis.containsKey(i)){
                for (vo_product pp : aiDiagnosis.get(i)){
                    boolean isFlag = false;
                    for (vo_grain_size pf : userPet.getGrain_size()){
                        if(pf.getSize_min() <= pp.getUnit_size() && pf.getSize_max() >= pp.getUnit_size()){
                            isFlag = true;
                            break;
                        }
                    }
                    if(isFlag){
                        resultProduct.add(pp);
                    }
                }
            }
        }



        ArrayList<vo_product> resultProduct2 = new ArrayList<>();
        if(userPet.getPsc_info().getPc_size().contains("소형")){
            for(vo_product pp : resultProduct){
                if (pp.getFeed_target() == 1){
                    resultProduct2.add(pp);
                }
            }
        }else if(userPet.getPsc_info().getPc_size().contains("중형")){
            for(vo_product pp : resultProduct){
                if (pp.getFeed_target() != 1 || pp.getFeed_target() == 2){
                    resultProduct2.add(pp);
                }
            }
        }else if(userPet.getPsc_info().getPc_size().contains("대형")){
            for(vo_product pp : resultProduct){
                if (pp.getFeed_target() == 0 || pp.getFeed_target() == 3){
                    resultProduct2.add(pp);
                }
            }
        }

        ArrayList<vo_product> resultProduct3 = new ArrayList<>();
        try{
            if(resultProduct.size() > 0 && userPet.getSick_info().size() > 0){

                /*
                for (sick_info psi : userPet.getSick_info()){
                    
                    
                    String proteinRange[] = psi.getProtein_range().split("-");
                    String fiberRange[] = psi.getProtein_range().split("-");
                    String fatRange[] = psi.getProtein_range().split("-");
                    String carboRange[] = psi.getProtein_range().split("-");
                    for (product pp : resultProduct){
                        resultProduct3.add(pp);
                        
                        if(proteinRange.length > 1 && Integer.parseInt(proteinRange[0]) >= pp.getProtein_dm() && Integer.parseInt(proteinRange[1]) <= pp.getProtein_dm()
                                && fiberRange.length > 1 && Integer.parseInt(fiberRange[0]) >= pp.getFiber_dm() && Integer.parseInt(fiberRange[1]) <= pp.getFiber_dm()
                                && fatRange.length > 1 && Integer.parseInt(fatRange[0]) >= pp.getFat_dm() && Integer.parseInt(fatRange[1]) <= pp.getFat_dm()
                                && carboRange.length > 1 && Integer.parseInt(carboRange[0]) >= pp.getCarbohydrate_dm() && Integer.parseInt(carboRange[1]) <= pp.getCarbohydrate_dm()){
                            
                        }
                        
                    }

                    
                }
                */
            }
        }catch (Exception e){
            e.getStackTrace();
        }

        HashMap<Integer, vo_product> rMap = new HashMap<>();
        for(vo_product pp : resultProduct3){
            if(!rMap.containsKey(pp.getBrand_idx())) rMap.put(pp.getBrand_idx(), pp);
        }
        if(rMap.size() < 3){
            for(vo_product pp : resultProduct2){
                if(!rMap.containsKey(pp.getBrand_idx()) && rMap.size() < 3) rMap.put(pp.getBrand_idx(), pp);
            }
        }
        if(rMap.size() < 3){
            for(vo_product pp : resultProduct){
                if(!rMap.containsKey(pp.getBrand_idx()) && rMap.size() < 3) rMap.put(pp.getBrand_idx(), pp);
            }
        }
        if(rMap.size() < 3){
            Iterator<Integer> AKeys = aiDiagnosis.keySet().iterator();
            do{
                int key = AKeys.next();
                if(aiDiagnosis.get(key).size() > 0){
                    for (vo_product pp1 : aiDiagnosis.get(key)){
                        if(!rMap.containsKey(pp1.getBrand_idx())) rMap.put(pp1.getBrand_idx(), pp1);
                    }
                }
            }while (AKeys.hasNext());
        }

        if(rMap.size() < 3){
            for (vo_product pp1 : rsProduct){
                if(!rMap.containsKey(pp1.getBrand_idx())) rMap.put(pp1.getBrand_idx(), pp1);
            }
        }

        Iterator<Integer> keys = rMap.keySet().iterator();
        do{
            int key = keys.next();
            if(result.size()<3) result.add(rMap.get(key).getIdx());
            else break;
        }while (keys.hasNext());

        return result;
    }

    private int getNowDateMonth(String date) {
        if (date.equals("") || date.equals("-")) return 0;
        Calendar value = Calendar.getInstance();
        value.set(Calendar.YEAR, Integer.parseInt(date.substring(0,4)));
        value.set(Calendar.MONTH, (Integer.parseInt(date.substring(4,6))-1));
        value.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date.substring(6,8)));

        int MINUTE_MS = 60 * 1000;
        int HOUR_MS = MINUTE_MS * 60;
        //long DAY_MS = HOUR_MS * 24;
        long MONTH_MS = HOUR_MS * 24L * 30L;
        Calendar today = Calendar.getInstance();
        long month = (today.getTimeInMillis() - value.getTimeInMillis()) / MONTH_MS;
        return (int) month;
    }
}
