package com.pood.server.api.service.meta.grain_size;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_grain_size;

public interface grainSizeService {
    
    /**************** 항목 목록 조회 *****************/
    public List<dto_grain_size> getList(pagingSet PAGING_SET) throws Exception;

    /**************** 전체 목록 개수 조회 ******************/
    public Integer getTotalRecordNumber() throws SQLException;

    public dto_grain_size getRecord(Integer idx) throws Exception;
}
