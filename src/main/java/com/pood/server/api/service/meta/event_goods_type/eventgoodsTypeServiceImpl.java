package com.pood.server.api.service.meta.event_goods_type;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;

import com.pood.server.api.service.list.*;
import com.pood.server.api.service.record.*;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.meta.event.dto_event_goods_type;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.pood.server.object.pagingSet;

import org.springframework.stereotype.Service;
@Service("eventgoodsTypeService")
public class eventgoodsTypeServiceImpl implements eventgoodsTypeService {

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;
    
    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_EVENT_GOODS_TYPE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
 
    @Override
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @SuppressWarnings("unchecked")
    public List<dto_event_goods_type> getEventgoodsTypeList(pagingSet PAGING_SET) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_event_goods_type>)listService.getDTOList(SELECT_QUERY.toString(), dto_event_goods_type.class);
    } 
}
