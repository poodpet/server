package com.pood.server.api.service.log;

import java.util.HashMap;
import java.util.Map;
import com.pood.server.config.DATABASE;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.meta.product.*;
import com.pood.server.api.service.query.queryService;
import com.pood.server.api.service.record.updateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("logProductOutOfStockService")
public class logProductOutOfStockServiceImpl implements logProductOutOfStockService{

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("productService")
    productService productService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_LOG_PRODUCT_OUT_OF_STOCK;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
    
    @Override
    public Integer insertRecord(Integer product_idx, Integer goods_idx, Integer product_qty, Integer quantity)
            throws Exception {
        return updateService.insert(GET_QUERY_INSERT_RECORD(product_idx, goods_idx, product_qty, quantity));
    }

    @Override
    public String GET_QUERY_INSERT_RECORD(Integer product_idx, Integer goods_idx, Integer product_qty, Integer quantity) throws Exception {
        
        String uuid = productService.getProductUUID(product_idx);

        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("product_uuid",     uuid);
        hashMap.put("goods_idx",        goods_idx);
        hashMap.put("product_qty",      product_qty);
        hashMap.put("quantity",         quantity);
        hashMap.put("mail_send",        0);
        return queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);
    }
 

}
