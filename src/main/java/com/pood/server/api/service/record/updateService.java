package com.pood.server.api.service.record;

import java.sql.SQLException;
import java.util.List;

public interface updateService {
 

    public Integer  insert(String query) throws SQLException;


    public void     execute(String query) throws SQLException;


    public void     commit(List<String> query) throws SQLException;
  
    
} 