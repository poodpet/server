package com.pood.server.api.service.meta.attributable;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_attributable;

public interface attrService {

    List<dto_attributable> getList(pagingSet PAGING_SET) throws Exception;

    public Integer getTotalRecordNumber() throws SQLException;
    
}
