package com.pood.server.api.service.delivery;

public interface remoteDeliveryService {
    
    Integer checkDeliveryFee(Integer DELIVERY_REMOTE_TYPE, Integer DELIVERY_FEE, Integer ORDER_PRICE, String ZIPCODE, String USER_UUID)
        throws Exception;

    Integer getRemoteType(String ZIPCODE) throws Exception;

}
