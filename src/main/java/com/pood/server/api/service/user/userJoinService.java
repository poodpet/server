package com.pood.server.api.service.user;

import java.sql.SQLException;

import com.pood.server.dto.log.dto_log_user_join;

public interface userJoinService {

    // 회원 가입 로그 삽입 
	public Integer insertRecord(Integer LOGIN_TYPE, Integer USER_IDX, String USER_UUID, String SNS_KEY) throws SQLException;
    
    // 특정 SNS 키에 해당하는 회원 조회 
    public dto_log_user_join getUserJoinInfo(String sns_key) throws Exception;

    public void deleteRecord(String USER_UUID) throws Exception;

    public void updateSnsKey(String userUuid, String snsKey) throws Exception;
}
