package com.pood.server.api.service.view.view_7;

import java.util.List;

public interface view7Service {

    public List<Integer> getGoodsIDXList(Integer PRODUCT_IDX) throws Exception;
    
}
