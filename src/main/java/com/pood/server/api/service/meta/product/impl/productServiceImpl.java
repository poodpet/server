package com.pood.server.api.service.meta.product.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
 
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.meta.feed.feedService;
import com.pood.server.api.service.meta.goods.goodsProductService;
import com.pood.server.api.service.meta.goods.goodsService;
import com.pood.server.api.service.meta.image.*;
import com.pood.server.api.service.meta.product.*;
import com.pood.server.api.service.meta.pet_doctor_desc.*;
import com.pood.server.api.service.meta.brand.*;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.query.*;
import com.pood.server.config.DATABASE;
import com.pood.server.object.image_list;
import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_pet_doctor_desc;
import com.pood.server.object.meta.vo_product;
import com.pood.server.object.meta.vo_product_image;
import com.pood.server.dto.dto_image_2;
import com.pood.server.dto.meta.dto_feed;
import com.pood.server.dto.meta.brand.dto_brand_3;
import com.pood.server.dto.meta.pet.dto_pet_doctor_desc;
import com.pood.server.dto.meta.product.dto_product_1;
import com.pood.server.dto.meta.product.dto_product_2;
import com.pood.server.dto.meta.product.dto_product_4;
import com.pood.server.dto.meta.product.dto_product_5;
import com.pood.server.dto.meta.product.dto_product_6;
import com.pood.server.dto.meta.product.dto_product_7;
import com.pood.server.dto.meta.product.dto_product_8;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("productService")
public class productServiceImpl implements productService {

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("imageService")
    imageService imageService;

    @Autowired
    @Qualifier("feedService")
    feedService feedService;
    
    @Autowired
    @Qualifier("goodsProductService")
    goodsProductService goodsProductService;

    @Autowired
    @Qualifier("PETDoctorDescService")
    PETDoctorDescService PETDoctorDescService;

    @Autowired
    @Qualifier("brandService")
    brandService brandService;

    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;


    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_PRODUCT;

    private String DEFINE_IMAGE_TABLE_NAME = DATABASE.TABLE_PRODUCT_IMAGE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    /******************** 상품 오브젝트 조회 *********************/
    public dto_product_4 getProductObject(Integer PRODUCT_IDX) throws Exception {

        dto_product_4 record = new dto_product_4();

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", PRODUCT_IDX);

        record = (dto_product_4)listService.getDTOObject(SELECT_QUERY.toString(), dto_product_4.class);

        /***** 상품 이미지 조회 *****/
        SELECT_QUERY.clear();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME);
        SELECT_QUERY.buildEqual("product_idx", PRODUCT_IDX);

        if(record != null){
            image_list image_list =         imageService.getImageList(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME, PRODUCT_IDX, "product_idx");
            record.setMain_image(image_list.getMAIN_IMAGE_LIST());
            record.setProduct_image(image_list.getIMAGE_LIST());
            record.setProduct_feed(feedService.getFeedList(PRODUCT_IDX));
        }

        return record;
    }

    /******************** 상품 오브젝트 조회 *********************/
    public dto_product_1 getProductObject5(Integer PRODUCT_IDX) throws Exception {
        dto_product_1 record = new dto_product_1();
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", PRODUCT_IDX);
        record = (dto_product_1)listService.getDTOObject(SELECT_QUERY.toString(), dto_product_1.class);
        return record;
    }

    public dto_product_5 getProductObject2(Integer PRODUCT_IDX) throws Exception{
        
        dto_product_5 record = new dto_product_5();

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", PRODUCT_IDX);

        record = (dto_product_5)listService.getDTOObject(SELECT_QUERY.toString(), dto_product_5.class);
        
        Integer PRODUCT_QTY = goodsProductService.getProductQTY(PRODUCT_IDX);

        if(record != null)
            record.setProduct_qty(PRODUCT_QTY);

        /***** 상품 이미지 조회 *****/
        SELECT_QUERY.clear();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME);
        SELECT_QUERY.buildEqual("product_idx", PRODUCT_IDX);

        if(record != null){
            image_list image_list =         imageService.getImageList(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME, PRODUCT_IDX, "product_idx");
            record.setMain_image(image_list.getMAIN_IMAGE_LIST());
            record.setProduct_image(image_list.getIMAGE_LIST());
            record.setProduct_feed(feedService.getFeedList(PRODUCT_IDX));

            dto_pet_doctor_desc pet_doctor_desc = PETDoctorDescService.getRecord(PRODUCT_IDX);
            record.setPet_doctor_desc(pet_doctor_desc);
            pet_doctor_desc = null;
        }

        return record;
    }
    
    public dto_product_6 getProductObject3(Integer PRODUCT_IDX) throws Exception {

        vo_product tmp = new vo_product();

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", PRODUCT_IDX);

        tmp = (vo_product)listService.getDTOObject(SELECT_QUERY.toString(), vo_product.class);

        Integer BRAND_IDX = tmp.getBrand_idx();

        dto_product_6 record = (dto_product_6)listService.getDTOObject(SELECT_QUERY.toString(), dto_product_6.class);

        if(record != null){
            image_list image_list =         imageService.getImageList(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME, PRODUCT_IDX, "product_idx");
            record.setMain_image(image_list.getMAIN_IMAGE_LIST());
            record.setProduct_image(image_list.getIMAGE_LIST());
            record.setProduct_feed(feedService.getFeedList(PRODUCT_IDX));

            dto_brand_3 brand = brandService.getBrandRecord(BRAND_IDX);
            record.setBrand(brand);

            brand = null;
        }

        tmp = null;

        return record;
    }

    public dto_product_8 getProductObject4(Integer PRODUCT_IDX) throws Exception {

        vo_product tmp = new vo_product();

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", PRODUCT_IDX);

        tmp = (vo_product)listService.getDTOObject(SELECT_QUERY.toString(), vo_product.class);

        Integer BRAND_IDX = tmp.getBrand_idx();

        dto_product_8 record = (dto_product_8)listService.getDTOObject(SELECT_QUERY.toString(), dto_product_8.class);

        if(record != null){
            
            image_list image_list =         imageService.getImageList(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME, PRODUCT_IDX, "product_idx");
            record.setMain_image(image_list.getMAIN_IMAGE_LIST());
            record.setProduct_image(image_list.getIMAGE_LIST());
            record.setProduct_feed(feedService.getFeedList(PRODUCT_IDX));


            // 상품에 해당하는 브랜드 정보 조회
            dto_brand_3 brand = brandService.getBrandRecord(BRAND_IDX);
            record.setBrand(brand);


            // 상품에 해당하는 수의사 사료 설명 조회
            vo_pet_doctor_desc record2 = PETDoctorDescService.getRecord(PRODUCT_IDX);
            record.setPet_doctor_desc(record2);

            record2 = null;

            brand = null;
        }

        tmp = null;

        return record;
    }
 

    /******************** 상품 목록 조회 *********************/
    @SuppressWarnings("unchecked")
    public List<dto_product_7> getProductList(pagingSet PAGING_SET) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);

        SELECT_QUERY.setPagination(PAGING_SET, true);
        List<dto_product_7> list = (List<dto_product_7>)listService.getDTOList(SELECT_QUERY.toString(), dto_product_7.class);


        List<dto_feed> feed_list            = feedService.getList();

        List<dto_pet_doctor_desc> pdfd_list = PETDoctorDescService.getList();

        List<vo_product_image> image_list        = getImageList();

        if(list != null){
            for(dto_product_7 e : list){

                Integer product_idx = e.getIdx();

                // 상품 항목번호에 해당하는 수의사 사료 설명 조회
                for(dto_pet_doctor_desc e1 : pdfd_list){
                    if(product_idx.equals(e1.getProduct_idx()))
                        e.setPet_doctor_desc(e1);
                }


                // 상품 항목번호에 해당하는 사료 설명 조회
                List<dto_feed> record2 = new ArrayList<dto_feed>();

                for(dto_feed e1 : feed_list){
                    if(product_idx.equals(e1.getProduct_idx()))
                        record2.add(e1);

                }

                // 상품 항목번호에 해당하는 상품 이미지 조회
                List<vo_product_image> record3 = new ArrayList<vo_product_image>();

                for(vo_product_image e1 : image_list)
                    if(product_idx.equals(e1.getProduct_idx()))
                        record3.add(e1);
                        

                e.setProduct_feed(record2);

                e.setImage(record3);

                record2 = null;

                record3 = null;

                product_idx = null;
            }

        }

        feed_list = null;

        pdfd_list = null;

        return list;
    }
    /******************** 상품 목록 조회 *********************/
    @SuppressWarnings("unchecked")
    public List<dto_product_2> getProductList2(pagingSet PAGING_SET) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_product_2>)listService.getDTOListWithImage2(
                SELECT_QUERY.toString(), "product_idx", DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME, dto_product_2.class, dto_image_2.class);
    }

    /**************** 상품 항목 개수 조회 ******************/
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    } 
 
    /******************* 상품 항목 번호에 대한 상품 정보 조회 *****************/
    public vo_product getProduct(Integer PRODUCT_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", PRODUCT_IDX);
        return (vo_product)listService.getDTOObject(SELECT_QUERY.toString(), vo_product.class);
    }

    /******************* 상품 항목 번호에 대한 상품 정보 조회 *****************/
    public vo_product getProduct(String uuid) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("uuid", uuid);
        return (vo_product)listService.getDTOObject(SELECT_QUERY.toString(), vo_product.class);
    }

    @SuppressWarnings("unchecked")
    public List<Integer> getProductIDXLIST(Integer brand_idx) throws Exception {
        List<Integer> result = new ArrayList<Integer>();
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("brand_idx", brand_idx);
        
        List<vo_product> list = (List<vo_product>)listService.getDTOList(SELECT_QUERY.toString(), vo_product.class);
        if(list != null){
            for(vo_product e : list)
                result.add((Integer)e.getIdx());
        }
    
        return result;
    }

    @SuppressWarnings("unchecked")
    public List<vo_product> getObjectList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return (List<vo_product>)listService.getDTOList(SELECT_QUERY.toString(), vo_product.class);
    }

    @Override
    public void updateRecord5(String CT_SUB_NAME, Integer SHOW_INDEX, Integer PRODUCT_IDX) throws Exception {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("ct_sub_name", CT_SUB_NAME);
        hashMap.put("show_index",   SHOW_INDEX);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx",   PRODUCT_IDX.toString());     
        updateService.execute(query);
        query = null;
    }

    @Override
    public dto_image_2 getImage(Integer product_image_idx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", product_image_idx);
        dto_image_2 record = (dto_image_2)listService.getDTOObject(SELECT_QUERY.toString(), dto_image_2.class);
        return record;
    }


    
    @Override
    public void addQTY(Integer DIFF, Integer PRODUCT_IDX) throws Exception {
        updateService.execute(GET_QUERY_ADD_QTY(DIFF, PRODUCT_IDX));
    }

    @Override
    public void deductQTY(Integer DIFF, Integer PRODUCT_IDX) throws Exception {
        updateService.execute(GET_QUERY_DEDUCT_QTY(DIFF, PRODUCT_IDX));
    }

    public String GET_QUERY_ADD_QTY(Integer DIFF, Integer PRODUCT_IDX){
        return queryService.getAddValueQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, "quantity", DIFF, "idx", PRODUCT_IDX.toString());
    }

    public String GET_QUERY_DEDUCT_QTY(Integer DIFF, Integer PRODUCT_IDX){
        return queryService.getDeductValueQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, "quantity", DIFF, "idx", PRODUCT_IDX.toString());
    }

    

    @Override
    public Integer getQuantity(Integer PRODUCT_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", PRODUCT_IDX);
        vo_product record = (vo_product)listService.getDTOObject(SELECT_QUERY.toString(), vo_product.class);
        return record.getQuantity();
    }

    @SuppressWarnings("unchecked")
    public List<dto_image_2> getProductImageList(Integer PRODUCT_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME);
        if(PRODUCT_IDX != null)
            SELECT_QUERY.buildEqual("product_idx", PRODUCT_IDX);
        return (List<dto_image_2>)listService.getDTOImageList2(SELECT_QUERY.toString());
    }

    @SuppressWarnings("unchecked")
    public List<vo_product_image> getImageList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME);
        return (List<vo_product_image>)listService.getDTOList(SELECT_QUERY.toString(), vo_product_image.class);
    }

    @Override
    public String getProductUUID(Integer product_idx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", product_idx);
        vo_product record = (vo_product)listService.getDTOObject(SELECT_QUERY.toString(), vo_product.class);
        return record.getUuid();
    }

    @Override
    public Integer getBrandIDX(Integer product_idx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", product_idx);
        vo_product record = (vo_product)listService.getDTOObject(SELECT_QUERY.toString(), vo_product.class);
        return record.getBrand_idx();
    }
}
