package com.pood.server.api.service.user.impl;

import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.query.queryService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.api.service.user.userLoginService;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.user.META_USER_LOGIN_LOG;
import com.pood.server.dto.log.dto_log_user_login;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service("userLoginService")
public class userLoginServiceImpl implements userLoginService {

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_LOG_USER_LOGIN;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @Override
    public Integer insertRecord(Integer user_idx, String user_uuid, Integer in_out) throws SQLException {
        
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_idx",     user_idx);
        hashMap.put("user_uuid",    user_uuid);
        hashMap.put("login_out",    in_out);
        hashMap.put("os_type",      META_USER_LOGIN_LOG.OS_TYPE_ANDROID);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;

    }

    @SuppressWarnings("unchecked")
    public String getLastDateTime(Integer user_idx) throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(
            DEFINE_DB_NAME,
            DEFINE_TABLE_NAME);
            
        SELECT_QUERY.buildEqual("user_idx", user_idx);
        SELECT_QUERY.buildReverseOrder("idx");

        String DATETIME = "";

        List<dto_log_user_login> result = (List<dto_log_user_login>)listService.getDTOList(SELECT_QUERY.toString(), dto_log_user_login.class);

        if (!ObjectUtils.isEmpty(result)) {
            return (String) result.get(0).getRecordbirth();
        }
        return DATETIME;
    }

    @Override
    public void deleteRecord(String USER_UUID) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_uuid", USER_UUID);
        updateService.execute(DELETE_QUERY.toString());
        
    }
}
