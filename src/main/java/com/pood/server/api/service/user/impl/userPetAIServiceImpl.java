package com.pood.server.api.service.user.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.user.userPetAIService;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.meta.dto_ai_recommend_diagnosis;
import com.pood.server.dto.user.pet.dto_user_pet_ai_dig;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.meta.ai_recommend_diagnosis.*;
import com.pood.server.api.service.query.queryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("userPetAIService")
public class userPetAIServiceImpl implements userPetAIService{
    

    @Autowired
    @Qualifier("updateService")
    updateService updateService;
    
    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("AIReDigService")
    AIReDigService AIReDigService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    
    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_USER_PET_AI_DIAGNOSIS;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

     /******************* 회원 반려 동물 항목 번호에 해당하는 질병 번호 조회 ********************/
     @SuppressWarnings("unchecked")
    public List<dto_user_pet_ai_dig> getList(Integer USER_PET_IDX) throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_pet_idx", USER_PET_IDX);
        return (List<dto_user_pet_ai_dig>)listService.getDTOList(SELECT_QUERY.toString(), dto_user_pet_ai_dig.class);
    }

    /******************** 회원 동물 AI 질병 정보 등록 ******************************/
    public Integer insertRecord(Integer USER_PET_IDX, Integer AI_DIG_IDX) throws Exception {
        dto_ai_recommend_diagnosis record = AIReDigService.getRecord(AI_DIG_IDX);

        // 삽입 쿼리를 실행하기 위한 빌더 생성
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_pet_idx", USER_PET_IDX);
        hashMap.put("ai_diagnosis_idx", AI_DIG_IDX);
        hashMap.put("ard_name", record.getArd_name());
        hashMap.put("ard_group", record.getArd_group());
        hashMap.put("ard_group_code", record.getArd_group_code());

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;

    }

    @Override
    public void deleteRecordWithUserPetIDX(Integer USER_PET_IDX) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_pet_idx", Integer.toString(USER_PET_IDX));
        updateService.execute(DELETE_QUERY.toString());
    }
}
