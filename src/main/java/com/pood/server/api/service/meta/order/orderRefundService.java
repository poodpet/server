package com.pood.server.api.service.meta.order;

import com.pood.server.dto.meta.order.dto_order_refund;
import com.pood.server.dto.meta.order.dto_order_refund_2;
import com.pood.server.object.meta.vo_order_refund;
import java.sql.SQLException;
import java.util.List;

public interface orderRefundService {

    public String insertRecord(String text, String order_number, Integer goods_idx, Integer qty,
        Integer send_type, Boolean attr) throws SQLException;

    public List<dto_order_refund> getList(String ORDER_NUMBER) throws Exception;

    public vo_order_refund getRecord(String uuid) throws Exception;

    public void upddateStatus(Integer status, String uuid) throws SQLException;

    public List<dto_order_refund_2> getList2(String ORDER_NUMBER) throws Exception;

    public String GET_QUERY_UPDATE_STATUS(String uuid, Integer status);

}
