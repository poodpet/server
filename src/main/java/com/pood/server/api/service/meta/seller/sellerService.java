package com.pood.server.api.service.meta.seller;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_seller;

public interface sellerService {

    /************* 셀러 항목 리스트를 조회합니다. ********************/
    public List<dto_seller> getSellerList(pagingSet PAGING_SET) throws Exception;
    
    /************* 셀러 항목 개수를 조회합니다. ********************/
    public Integer getTotalRecordNumber() throws SQLException;
 

}
