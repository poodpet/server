package com.pood.server.api.service.user.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.service.user.userClaimService;
import com.pood.server.config.DATABASE;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.user.dto_user_claim;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.req.header.user.claim.hUser_claim_1;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.meta.goods.*;
import com.pood.server.api.service.meta.image.*;
import com.pood.server.api.service.query.queryService;

@Service("userClaimService")
public class userClaimServiceImpl implements userClaimService {

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("imageService")
    imageService imageService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;
    

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_USER_CLAIM;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    /************* 회원 문의 신규 등록 ****************/
    public Integer insertUserClaim(Integer USER_IDX, String USER_UUID, hUser_claim_1 header) throws SQLException {
        
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_idx",             USER_IDX);
        hashMap.put("user_uuid",            USER_UUID);
        hashMap.put("claim_type",           header.getClaim_type());
        hashMap.put("claim_text",           header.getClaim_text());
        hashMap.put("visible",              header.getVisible());
        hashMap.put("claim_title",          header.getClaim_title());
        hashMap.put("claim_goods_name",     header.getClaim_goods_name());
        hashMap.put("claim_goods_idx",      header.getClaim_goods_idx());
        hashMap.put("claim_answer",         "-");
        hashMap.put("isAnswer",             0);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }

 


    /************** 회원 인덱스에 해당하는 회원 문의 목록 조회 ******************/
    @SuppressWarnings("unchecked")
    public List<dto_user_claim> getUserClaimList(pagingSet PAGING_SET, String user_uuid, Boolean isAnswer) throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", user_uuid);
        if(isAnswer != null){
            if(isAnswer)SELECT_QUERY.buildEqual("isAnswer", 1);
            else SELECT_QUERY.buildEqual("isAnswer", 0);
        }
            
        SELECT_QUERY.setPagination(PAGING_SET, true);

        List<dto_user_claim> list = (List<dto_user_claim>)listService.getDTOList(SELECT_QUERY.toString(), dto_user_claim.class);
        for(dto_user_claim e : list){
            String DISPLAY_TYPE = goodsService.getDisplayType(e.getClaim_goods_idx());
            e.setDisplay_type(DISPLAY_TYPE);
            DISPLAY_TYPE = null;
        }

        return list;
    }

    /*************** 회원 항목 번호에 해당하는 전체 문의 개수 반환 *****************/
	public Integer getTotalRecordNumber(String user_uuid) throws SQLException{
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME); 
        CNT_QUERY.buildEqual("user_uuid", user_uuid);
        return listService.getRecordCount(CNT_QUERY.toString());

    }



    /***************** 회원 문의 정보 업데이트 **********************/
    public void updateUserClaim(Integer CLAIM_TYPE, String CLAIM_TEXT, String CLAIM_TITLE, Boolean IS_VISIBLE, Integer CLAIM_IDX) throws SQLException {
        // 회원 문의 정보 업데이트
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("claim_type",        CLAIM_TYPE);
        hashMap.put("claim_text",        CLAIM_TEXT);
        hashMap.put("claim_title",       CLAIM_TITLE);
        hashMap.put("visible",           IS_VISIBLE);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", CLAIM_IDX.toString());
        updateService.execute(query);
        query = null;

    }
 

    @Override
    public void deleteRecord(Integer e) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("idx", Integer.toString(e));
        updateService.execute(DELETE_QUERY.toString());
    }
 
}
