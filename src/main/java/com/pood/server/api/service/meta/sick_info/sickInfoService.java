package com.pood.server.api.service.meta.sick_info;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_sick_info;

public interface sickInfoService {

    
    /************* 앓고 있는 질병 항목 리스트를 조회합니다. ********************/
    public List<dto_sick_info> getSickInfoList(pagingSet PAGING_SET) throws Exception;

    /************* 펫 항목 수를 조회합니다. ********************/
    public Integer getTotalRecordNumber() throws SQLException;

 
    
}
