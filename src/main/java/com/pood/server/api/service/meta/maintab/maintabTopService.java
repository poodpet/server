package com.pood.server.api.service.meta.maintab;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.meta.dto_maintab_top;

public interface maintabTopService {

    public Integer getTotalRecordNumber() throws SQLException;
    
    public List<dto_maintab_top> getList() throws Exception;

}
