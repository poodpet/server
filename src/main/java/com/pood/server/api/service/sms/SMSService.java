package com.pood.server.api.service.sms;

public interface SMSService {
    
    public Integer send(String PHONE_NUMBER, String MESSAGE) throws Exception;
}
