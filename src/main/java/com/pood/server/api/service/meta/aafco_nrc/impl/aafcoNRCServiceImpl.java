package com.pood.server.api.service.meta.aafco_nrc.impl;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.api.service.record.*;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.meta.aafco_nrc.aafcoNRCService;
import com.pood.server.config.DATABASE;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_aafco_nrc;
import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("aafcoNRCService")
public class aafcoNRCServiceImpl implements aafcoNRCService {

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;
    
    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_AAFCO_NRC;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
     
    /************* AAFCO-NRC 항목 리스트를 조회합니다. ********************/
    @SuppressWarnings("unchecked")
    public List<dto_aafco_nrc> getAafcoNRCList(pagingSet PAGING_SET) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME); 
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_aafco_nrc>)listService.getDTOList(SELECT_QUERY.toString(), dto_aafco_nrc.class);
    }




    /************* AAFCO-NRC 항목 전체 개수를 조회합니다. ********************/
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME); 
        return listService.getRecordCount(CNT_QUERY.toString());
    }

}
