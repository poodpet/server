package com.pood.server.api.service.error;

import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.query.queryService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.config.DATABASE;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("errorProcService")
public class errorProcServiceImpl implements errorProcService {

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME = DATABASE.TABLE_ERROR_PROC;

    private String DEFINE_DB_NAME = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @Override
    public Integer insertRecord(String path, String error_msg, String stack_trace) throws SQLException {
        // 결제 실패 로그 생성
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("path", path);
        hashMap.put("error_msg", error_msg);
        hashMap.put("stack_trace", stack_trace);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap,
            com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }

}
