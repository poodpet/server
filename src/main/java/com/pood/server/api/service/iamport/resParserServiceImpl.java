package com.pood.server.api.service.iamport;

import org.springframework.stereotype.Service;

@Service("resParserService")
public class resParserServiceImpl implements resParserService{

    @Override
    public String getStatusCode(String result) {
        String RESPONSE_PARAMS[] = result.split(":");
        String RESPONSE_STATUS_CODE[] = RESPONSE_PARAMS[1].split(",");
        return RESPONSE_STATUS_CODE[0];
    }
    
}
