package com.pood.server.api.service.user.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.service.user.userNotiService;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.dto_image_2;
import com.pood.server.dto.user.dto_user_noti;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.query.*;

@Service("userNotiService")
public class userNotiServiceImpl implements userNotiService {

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME = DATABASE.TABLE_USER_NOTI;

    private String DEFINE_IMAGE_TABLE_NAME = DATABASE.TABLE_USER_NOTI_IMAGE;
    
    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @Override
    public Integer insertRecord(Integer USER_IDX, String USER_UUID, int NOTI_READ, String TITLE, String TEXT, String SCHEMA, String URL,
            Integer NOTI_STATUS) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_idx", USER_IDX);
        hashMap.put("user_uuid", USER_UUID);
        hashMap.put("noti_read", NOTI_READ);
        hashMap.put("noti_text", TEXT);
        hashMap.put("noti_title", TITLE);
        hashMap.put("noti_scheme", SCHEMA);
        hashMap.put("noti_url", URL);
        hashMap.put("noti_status", NOTI_STATUS);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }

    @Override
    public Integer getUnreadMsg(Integer USER_IDX) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        CNT_QUERY.buildEqual("user_idx", USER_IDX);
        CNT_QUERY.buildEqual("noti_read", 0);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @Override
    public void readAllMessage(Integer user_idx) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("noti_read", 1);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "user_idx", user_idx.toString());
        updateService.execute(query);
        query = null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<dto_user_noti> getList(Integer user_idx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_idx", user_idx);
        SELECT_QUERY.buildReverseOrder("idx");
        return (List<dto_user_noti>)listService.getDTOListWithImage2(
                SELECT_QUERY.toString(), "noti_idx", DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME, dto_user_noti.class, dto_image_2.class);
    }   
    @Override
    public void updateReadStatus(Integer e) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("noti_read", 1);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", e.toString());
        updateService.execute(query);
        query = null;
    }

    @Override
    public void deleteRecordWithUserUUID(String USER_UUID) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_uuid", USER_UUID);
        updateService.execute(DELETE_QUERY.toString());
    }
    
}
