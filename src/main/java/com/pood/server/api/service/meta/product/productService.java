package com.pood.server.api.service.meta.product;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_product;
import com.pood.server.dto.dto_image_2;
import com.pood.server.dto.meta.product.dto_product_1;
import com.pood.server.dto.meta.product.dto_product_2;
import com.pood.server.dto.meta.product.dto_product_4;
import com.pood.server.dto.meta.product.dto_product_5;
import com.pood.server.dto.meta.product.dto_product_6;
import com.pood.server.dto.meta.product.dto_product_7;
import com.pood.server.dto.meta.product.dto_product_8;

public interface productService {
    


    /******************** 특정 상품 항목 번호에 대한 상품 정보 조회 *********************/
     public vo_product getProduct(Integer PRODUCT_IDX) throws Exception;


     
     public vo_product getProduct(String uuid) throws Exception;
    
 


    /******************** 상품 오브젝트 조회 *********************/
    public dto_product_4 getProductObject(Integer product_idx) throws Exception;




    /******************** 상품 오브젝트 조회 *********************/
    public dto_product_5 getProductObject2(Integer product_idx) throws Exception;




    public dto_product_6 getProductObject3(Integer product_idx) throws Exception;



    public dto_product_8 getProductObject4(Integer product_idx) throws Exception;



    /******************** 상품 목록 조회 *********************/
    public List<dto_product_7> getProductList(pagingSet PAGING_SET) throws Exception;
    



    /**************** 상품 항목 개수 조회 ******************/
    public Integer getTotalRecordNumber() throws SQLException;

 

	public List<dto_product_2> getProductList2(pagingSet PAGING_SET) throws Exception;



	public List<Integer> getProductIDXLIST(Integer brand_idx) throws Exception;



    
    public List<vo_product> getObjectList() throws Exception;

 

	public void updateRecord5(String CT_SUB_NAME, Integer SHOW_INDEX, Integer PRODUCT_IDX) throws Exception;



    public dto_image_2 getImage(Integer product_image_idx) throws Exception;



    public void addQTY(Integer DIFF, Integer PRODUCT_IDX) throws Exception;



    public void deductQTY(Integer DIFF, Integer PRODUCT_IDX) throws Exception;



    public Integer getQuantity(Integer PRODUCT_IDX) throws Exception;



    public List<dto_image_2> getProductImageList(Integer PRODUCT_IDX) throws Exception;




    public String GET_QUERY_ADD_QTY(Integer DIFF, Integer PRODUCT_IDX);


    
    public String GET_QUERY_DEDUCT_QTY(Integer DIFF, Integer PRODUCT_IDX);


    public dto_product_1 getProductObject5(Integer PRODUCT_IDX) throws Exception;



    public String getProductUUID(Integer product_idx) throws Exception;



    public Integer getBrandIDX(Integer main_producInteger) throws Exception;
    
}
