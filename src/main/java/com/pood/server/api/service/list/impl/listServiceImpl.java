package com.pood.server.api.service.list.impl;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.List;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.database.*;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.meta.image.*;
import com.pood.server.dto.dto_image;
import com.pood.server.dto.dto_image_1;
import com.pood.server.dto.dto_image_2;
import com.pood.server.dto.dto_image_3;
import com.pood.server.dto.log.user_order.dto_log_user_order_2;
import com.pood.server.dto.meta.order.dto_order_delivery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("listService")
public class listServiceImpl implements listService {


    @Autowired
    @Qualifier("imageService") 
    imageService imageService;

    @Autowired
    @Qualifier("DBService")
    DBService DBService;

    @Autowired
    @Qualifier("DTOService")
    DTOService DTOService;

    @Autowired
    @Qualifier("listService")
    listService listService;
    


 
    /************* HashMap리스트 : 테이블의 모든 칼럼에 대해서 데이터를 조회하여 저장 **********************/
    public List<?> getDTOList(String query, Class<?> cls) throws Exception {
        return DBService.getdtoList(query, cls);
    }
    public List<Integer> getIDXList(String query, String column_name) throws SQLException {
        return DBService.getIDXListWithColumnName(query,column_name);
    }
    @Override
    public Integer getRecordCount(String query) throws SQLException {
        return DBService.getRecordCount(query);
    }
    @Override
    public Object getDTOObject(String query, Class<?> cls) throws Exception {
        return DBService.getDTOObject(query, cls);
    }
    @Override
    public List<Integer> getIDXList(String query) throws SQLException {
        return DBService.getIDXList(query);
    }

    public List<dto_image_3> getDTOListReviewImage(String query) throws Exception{
        return DTOService.GET_DTO_REVIEW_IMAGE(query);
    }
    public List<dto_log_user_order_2> getDTOListLogUserOrder(String query) throws Exception{
        return DTOService.GET_DTO_LOG_USER_ORDER(query);
    }
    public List<dto_order_delivery> getDTOListOrderDelivery(String query) throws Exception{
        return DTOService.GET_DTO_ORDER_DELIVERY(query);
    }

    
    public List<?> getDTOImageList2(String query) throws Exception{
        return DTOService.getdtoImageList2(query);
    }
    
    public List<?> getDTOImageList4(String query) throws Exception{
        return DTOService.getdtoImageList4(query);
    }

    public List<?> getDTOListWithImage(String query, String image_column_name, String db_name, String table_name, Class<?> cls, Class<?> cls_image) throws Exception{

        List<Object> result = DBService.getdtoList(query, cls);


        for(Object e: result){

            Field field = e.getClass().getDeclaredField("idx");    
            field.setAccessible(true);
            Integer IMAGE_IDX = (Integer)field.get(e);

            SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(db_name, table_name);
            SELECT_QUERY.buildEqual(image_column_name, IMAGE_IDX);

            List<dto_image> IMAGE_LIST = DTOService.getdtoImageList(SELECT_QUERY.toString());
            Field image_field = e.getClass().getDeclaredField("image");
            image_field.setAccessible(true);
            image_field.set(e, IMAGE_LIST);

            IMAGE_LIST = null;
            
        }

        return result;
    }

    public List<?> getDTOListWithImage1(String query, String image_column_name, String db_name, String table_name, Class<?> cls, Class<?> cls_image) throws Exception{

        List<Object> result = DBService.getdtoList(query, cls);


        for(Object e: result){

            Field field = e.getClass().getDeclaredField("idx");    
            field.setAccessible(true);
            Integer IMAGE_IDX = (Integer)field.get(e);

            SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(db_name, table_name);
            SELECT_QUERY.buildEqual(image_column_name, IMAGE_IDX);
        
            List<dto_image_1> IMAGE_LIST = DTOService.getdtoImageList1(SELECT_QUERY.toString());
            Field image_field = e.getClass().getDeclaredField("image");
            image_field.setAccessible(true);
            image_field.set(e, IMAGE_LIST);

            IMAGE_LIST = null;

        }

        return result;
    }

    public List<?> getDTOListWithImage2(String query, String image_column_name, String db_name, String table_name, Class<?> cls, Class<?> cls_image) throws Exception{

        List<Object> result = DBService.getdtoList(query, cls);


        for(Object e: result){

            Field field = e.getClass().getDeclaredField("idx");    
            field.setAccessible(true);
            Integer IMAGE_IDX = (Integer)field.get(e);

            SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(db_name, table_name);
            SELECT_QUERY.buildEqual(image_column_name, IMAGE_IDX);
        
            List<dto_image_2> IMAGE_LIST = DTOService.getdtoImageList2(SELECT_QUERY.toString());
            Field image_field = e.getClass().getDeclaredField("image");
            image_field.setAccessible(true);
            image_field.set(e, IMAGE_LIST);

            IMAGE_LIST = null;

        }

        return result;
    }
 
 
}