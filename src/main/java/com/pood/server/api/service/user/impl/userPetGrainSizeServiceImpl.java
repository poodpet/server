package com.pood.server.api.service.user.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.user.userPetGrainSizeService;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.meta.dto_grain_size;
import com.pood.server.dto.user.pet.dto_user_pet_grain_size;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.meta.grain_size.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.pood.server.api.service.query.*;

@Service("userPetGrainSizeService")
public class userPetGrainSizeServiceImpl implements userPetGrainSizeService{
    

    @Autowired
    @Qualifier("updateService")
    updateService updateService;
    
    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("grainSizeService")
    grainSizeService grainSizeService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME = DATABASE.TABLE_USER_PET_GRAIN_SIZE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    
     /******************* 회원 반려 동물 항목 번호에 해당하는 질병 번호 조회 ********************/
     @SuppressWarnings("unchecked")
    public List<dto_user_pet_grain_size> getList(Integer USER_PET_IDX) throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_pet_idx", USER_PET_IDX);
        return (List<dto_user_pet_grain_size>)listService.getDTOList(SELECT_QUERY.toString(), dto_user_pet_grain_size.class);
    }

    /******************** 회원 동물 AI 질병 정보 등록 ******************************/
    public Integer insertRecord(Integer USER_PET_IDX, Integer GRAIN_SIZE_IDX) throws Exception {
        dto_grain_size record = grainSizeService.getRecord(GRAIN_SIZE_IDX);

        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_pet_idx", USER_PET_IDX);
        hashMap.put("grain_size_idx", GRAIN_SIZE_IDX);
        hashMap.put("name", record.getName());
        hashMap.put("size_min", record.getSize_min());
        hashMap.put("size_max", record.getSize_max());

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }

    @Override
    public void deleteRecordWithUserPetIDX(Integer USER_PET_IDX) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_pet_idx", Integer.toString(USER_PET_IDX));
        updateService.execute(DELETE_QUERY.toString());
    }
}
