package com.pood.server.api.service.log;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.log.user_order.dto_log_user_order;
import com.pood.server.dto.log.user_order.dto_log_user_order_2;

public interface logUserOrderService {

    
    /********** 주문 기록 생성 : log_order_history ***********/
    public Integer insertOrderHistory(
        Integer ORDER_IDX, 
        Integer STATUS, 
        String  MESSAGE, 
        String  ORDER_NUMBER, 
        String  USER_UUID,
        Integer GOODS_IDX,
        Integer QTY) throws SQLException;


    /******************* 특정 주문 번호에 대한 주문 기록 조회 *********************/
    public List<dto_log_user_order_2> getOrderHistoryList(Integer ORDER_IDX) throws Exception;


    /******************* 특정 주문 번호에 대한 주문 기록 조회2 *********************/
    public List<dto_log_user_order> getOrderHistoryList2(String ORDER_NUMBER) throws Exception;


    public Integer getPayCount(String user_uuid) throws SQLException;

    
    public Integer getPurchasedCnt(String user_uuid) throws SQLException;

    public dto_log_user_order getGoodsBought(Integer GOODS_IDX) throws Exception;


    public Boolean isUserOrdered(String USER_UUID) throws Exception;


    public Boolean getPaySuccessCnt(String USER_UUID) throws Exception;

    public String GET_QUERY_INSERT_ORDER_HISTORY(Integer ORDER_IDX, Integer STATUS, String  MESSAGE, String  ORDER_NUMBER, String  USER_UUID,Integer GOODS_IDX, Integer QTY);
 

}
