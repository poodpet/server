package com.pood.server.api.service.meta.image;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.dto_image_2;
import com.pood.server.object.image_list;
import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_pood_image;

public interface imageService {
    





    /**************** 특정 데이터베이스와 테이블에 대해서 이미지 목록 조회 ********************/
    public image_list getImageList(String db_name, String table_name, Integer INDEX, String column) throws Exception;







    /**************** 특정 데이터베이스와 테이블에 대해서 이미지 목록 조회 ********************/
    public image_list getImageList(String query) throws Exception;
    






    /******************** 이미지를 우선순위에 따라서 저장 ********************/
    public List<dto_image_2> orderImageList( List<dto_image_2> list);



 





    /**************** 이미지 업로드 후 업로드된 URL을 서버에 저장 **********************/
    public Integer uploadImageURL(String db_name, String table_name, String column_name, Integer idx, Integer image_type, String URL) throws SQLException;







 
    /***************** 별도로 쓰는 이미지 업로드 *********************/
	public void insertPoodImage(String desc, Integer image_idx) throws SQLException;








    /**************** 별도로 쓰는 이미지 레코드 총 개수 *****************/
    public Integer getTotalPoodImageRecordNumber(Integer idx) throws SQLException;



    




    /*************** 별도로 쓰는 이미지 레코드 목록 조회 **********************/
	public List<vo_pood_image> getPoodImageList(pagingSet PAGING_SET, Integer idx) throws Exception;





}
