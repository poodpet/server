package com.pood.server.api.service.meta.order.impl;

import java.sql.SQLException;
import java.util.List;
 

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
 
import com.pood.server.config.DATABASE;
import com.pood.server.dto.meta.order.dto_order;
import com.pood.server.dto.meta.order.dto_order_type;
import com.pood.server.object.pagingSet;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.meta.order.orderTypeService;
import com.pood.server.api.service.record.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("orderTypeService")
public class orderTypeServiceImpl implements orderTypeService {

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_ORDER_TYPE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    /**************** 주문 타입 목록 조회 *****************/
    @SuppressWarnings("unchecked")
    public List<dto_order_type> getOrderTypeList(pagingSet PAGING_SET) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_order_type>)listService.getDTOList(SELECT_QUERY.toString(), dto_order_type.class);
   }

    /**************** 전체 목록 개수 조회 ******************/
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    /**************** 주문 값에 해당하는 상태 이름 조회 ******************/
    public String getOrderTypeName(Integer value) throws Exception {

        String ORDER_TEXT = "";
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("value", value);

        dto_order_type record = (dto_order_type)listService.getDTOObject(SELECT_QUERY.toString(), dto_order.class);
        ORDER_TEXT = record.getText();

        return ORDER_TEXT;
    } 
}
