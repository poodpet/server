package com.pood.server.api.service.checkNull;

import java.lang.reflect.Field;

import org.springframework.stereotype.Service;

@Service("checkNullService")
public class checkNullServiceImpl implements checkNullService {

    @Override
    public boolean isNull(Object obj) throws Exception {
        for (Field f : obj.getClass().getDeclaredFields())
            if (f.get(this) != null)
                return false;
        return true;            
    }
    
}
