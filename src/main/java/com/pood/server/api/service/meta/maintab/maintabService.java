package com.pood.server.api.service.meta.maintab;

import java.util.List;

import com.pood.server.dto.meta.dto_maintab;
import com.pood.server.object.resp.resp_main;

public interface maintabService {

    public Integer getTotalRecordNumber() throws Exception;

    public List<resp_main> getMainList(Integer pc_id) throws Exception;

    public List<dto_maintab> getList(Integer pc_id) throws Exception;
    
}
