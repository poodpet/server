package com.pood.server.api.service.meta.feed_other_product;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_feed_other_product;

public interface feedOtherProductService {

    /**************** 다른 사료 항목 목록 조회  *****************/
    public List<dto_feed_other_product> getFeedOtherProductList(pagingSet PAGING_SET, Integer PC_ID, String KEYWORD) throws Exception;

    /**************** 전체 목록 개수 조회 ******************/
    public Integer getTotalRecordNumber(Integer pc_id, String keyword) throws SQLException;
 
    
}
