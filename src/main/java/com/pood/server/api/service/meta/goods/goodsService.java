package com.pood.server.api.service.meta.goods;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.dto_image_2;
import com.pood.server.dto.meta.goods.dto_goods_10;
import com.pood.server.dto.meta.goods.dto_goods_12;
import com.pood.server.dto.meta.goods.dto_goods_15;
import com.pood.server.dto.meta.goods.dto_goods_3;
import com.pood.server.dto.meta.goods.dto_goods_5;
import com.pood.server.dto.meta.goods.dto_goods_7;
import com.pood.server.dto.meta.goods.dto_goods_8;
import com.pood.server.dto.meta.goods.dto_goods_9;
import com.pood.server.dto.meta.goods.dto_goods_image;
import com.pood.server.object.image_list;
import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_goods_3;

public interface goodsService {
    
 

    /************ 굿즈 항목 번호에 해당하는 굿즈 오브젝트 조회 *************/

    /**** <판매중지 필터링> ****/

    // 결제시 조회되는 굿즈
    public vo_goods_3              GET_GOODS_OBJECT_1(Integer GOODS_IDX) throws Exception;         
    // 찜 목록, 이벤트 조회시 조회되는 굿즈
    public dto_goods_10         GET_GOODS_OBJECT_3(Integer GOODS_IDX) throws Exception;         
    // 장바구니 목록 조회시 조회되는 굿즈
    public dto_goods_8          GET_GOODS_OBJECT_5(Integer GOODS_IDX) throws Exception;         
    // 굿즈 2/1
    public List<dto_goods_7>    GET_GOODS_LIST(List<Integer> list) throws Exception;            

    public List<dto_goods_7>    GET_GOODS_LIST_BY_SORT_TYPE(List<Integer> list, String sort_type , Boolean is_ascending) throws Exception;            


    /**** <판매중지 필터링 안됨> ****/

    // 주문시 조회시 조회되는 굿즈    
    public dto_goods_3          GET_GOODS_OBJECT_2(Integer GOODS_IDX) throws Exception;                                                         
    // 주문 목록시 조회되는 굿즈
    public dto_goods_9          GET_GOODS_OBJECT_4(Integer GOODS_IDX) throws Exception;                                                         

    public dto_goods_15         GET_GOODS_OBJECT_10(Integer GOODS_IDX) throws Exception;


    // 수의사 사료 설명 
    public List<dto_goods_5>    GET_GOODS_LIST_2(pagingSet PAGING_SET, List<Integer> GOODS_IDX_LIST,  Integer PC_IDX) throws Exception;         
    // 메인 굿즈
    public List<dto_goods_12>   getList() throws Exception;

 
 
    

    public dto_image_2              getImage(Integer goods_image_idx) throws Exception;

    public List<dto_goods_image>    getImageList() throws Exception;

    public image_list               getGoodsImageList(Integer GOODS_IDX) throws Exception;

    public List<dto_image_2>        getGoodsImageList2(Integer GOODS_IDX) throws Exception;

    public List<dto_image_2>        getMainImageList(Integer GOODS_IDX) throws Exception;

    public List<dto_image_2>        getGoodsMainImageList() throws Exception;

  






    /**************** 전체 목록 개수 조회 ******************/
    public Integer      getTotalRecordNumber(Integer GOODS_IDX, Integer goods_type_idx, String keyword) throws SQLException;
 
  
  

     public Integer     getTotalRecordNumber(Integer goods_idx) throws SQLException;
    
     



     
     public String getgoodsName(Integer GOODS_IDX) throws Exception;




    public void updateGoodsSaleStatus(Integer e, Integer sale_status) throws SQLException;


    
    public void updateGoodsRating(Integer review_cnt, Double average_rating, Integer goods_idx) throws SQLException;

    public String GET_QUERY_UPDATE_GOODS_SALE_STATUS(Integer goods_idx, Integer status);

    public Boolean isOnSale(Integer goods_idx) throws Exception;

    public String getDisplayType(Integer GOODS_IDX) throws Exception;  



 
    
}
