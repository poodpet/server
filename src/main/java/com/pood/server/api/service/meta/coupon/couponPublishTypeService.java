package com.pood.server.api.service.meta.coupon;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_coupon_publish_type;

public interface couponPublishTypeService {

	public Integer getTotalRecordNumber() throws SQLException;

	public List<dto_coupon_publish_type> getList(pagingSet PAGING_SET) throws Exception;
    
}
