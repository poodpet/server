package com.pood.server.api.service.meta.promotion;

import java.sql.SQLException;
import java.util.List;


import com.pood.server.dto.meta.dto_promotion_image;
import com.pood.server.object.meta.vo_promotion_data;
import com.pood.server.object.meta.vo_promotion_data_image;

public interface promotionService {

    List<dto_promotion_image> getImageList() throws Exception;

    List<vo_promotion_data_image> getPRImageList(Integer pr_idx) throws Exception;

    Integer getTotalRecordNumber(Integer pc_id, Integer pr_idx) throws SQLException;

    List<vo_promotion_data> getList(Integer pc_id, Integer pr_idx) throws Exception;
    
}
