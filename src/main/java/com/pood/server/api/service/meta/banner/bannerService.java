package com.pood.server.api.service.meta.banner;

import java.util.List;

import com.pood.server.dto.meta.dto_banner;

public interface bannerService {

    public List<dto_banner> getList(Integer pc_id) throws Exception;
    
}
