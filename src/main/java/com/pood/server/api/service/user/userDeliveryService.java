package com.pood.server.api.service.user;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.user.dto_user_delivery_address;

public interface userDeliveryService {


    /******************* 배송지 항목 번호에 해당하는 회원 항목 번호 조회 ****************/
    public Integer getUserIndex(Integer delivery_idx) throws Exception;
    
    /**************** 특정 회원의 배송지 목록 조회 *******************/
    public List<dto_user_delivery_address> getUserDeliveryList(Integer user_idx) throws Exception;

    
    

    /*************** 회원 배송지 등록  *********************/
    public Integer insertUserDeliveryAddress(Integer USER_IDX, Boolean DEFAULT_TYPE, String DELIVERY_ADDRESS, String DETAIL_ADDRESS, 
        String NAME, String NICKNAME, String ZIPCODE, Boolean INPUT_TYPE, Integer REMOTE_TYPE, String PHONE_NUMBER) throws SQLException;
 
    
    /**************** 특정 배송지를 기본 배송지로 지정 **************/
    public void updateAddressToDefault(Integer RECORD_IDX) throws SQLException;

   

    /****************** 배송지 정보 업데이트 *********************/
    public void updateDeliveryAddress(String ADDRESS, Boolean DEFAULT_TYPE, String DETAIL_ADDRESS, String NAME, String NICKNAME, String ZIPCODE, Boolean INPUT_TYPE, Integer REMOTE_TYPE, String PHONE_NUMBER, Integer IDX) throws SQLException;




    /******************* 현재 회원이 가지고 있는 주문 배송지 개수 조회 ************/
    public Integer getCountDeliveryAddress(Integer USER_IDX) throws SQLException;

    /***************** 만약 남은 카드 개수가 하나일 경우 그 카드는 자동으로 디폴트로 지정 *****************/
	public void updateDefaultAddress(Integer USER_IDX)  throws SQLException;

    public void deleteRecord(Integer e) throws SQLException;

    public void deleteRecordWithUserUUID(String USER_UUID) throws SQLException;

    public void deleteRecordWithUSER_IDX(Integer USER_IDX) throws SQLException;  
}
