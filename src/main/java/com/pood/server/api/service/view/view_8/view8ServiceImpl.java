package com.pood.server.api.service.view.view_8;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.order.ORDER_STATUS;
import com.pood.server.object.view.view_8;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.record.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("view8Service")
public class view8ServiceImpl implements view8Service{
 
        
    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;
 
    private String DEFINE_TABLE_NAME    = DATABASE.VIEW_8;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @Override
    public Boolean isPurchase(String USER_UUID, Integer GOODS_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid",        USER_UUID);
        SELECT_QUERY.buildEqual("goods_idx",        GOODS_IDX);
        SELECT_QUERY.buildEqual("order_status",     ORDER_STATUS.ORDER_SUCCESS);        
        view_8 record = (view_8)listService.getDTOObject(SELECT_QUERY.toString(), view_8.class);
        if(record == null)return false;
        record = null;
        return true;
    }

    @Override
    public String isPurchase2(String USER_UUID, Integer GOODS_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid",        USER_UUID);
        SELECT_QUERY.buildEqual("goods_idx",        GOODS_IDX);
        SELECT_QUERY.buildEqual("order_status",     ORDER_STATUS.ORDER_SUCCESS);        
        view_8 record = (view_8)listService.getDTOObject(SELECT_QUERY.toString(), view_8.class);
        if(record == null)return null;
        return record.getOrder_number();
        
    }


}
