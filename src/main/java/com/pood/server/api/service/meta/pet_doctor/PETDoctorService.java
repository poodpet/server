package com.pood.server.api.service.meta.pet_doctor;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.meta.pet.dto_pet_doctor;
import com.pood.server.object.pagingSet;

public interface PETDoctorService {

    /**************** 수의사 목록 조회 *****************/
    public List<dto_pet_doctor> getPetDoctorList(pagingSet PAGING_SET) throws Exception;

    /**************** 전체 목록 개수 조회 ******************/
    public Integer getTotalRecordNumber() throws SQLException;
 
}
