package com.pood.server.api.service.meta.delivery_courier;

import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;


import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.config.DATABASE;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_delivery_courier;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.record.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


@Service("deliveryCourierService")
public class deliveryCourierServiceImpl implements deliveryCourierService{

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_DELIVERY_COURIER;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @Override
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME); 
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @SuppressWarnings("unchecked")
    public List<dto_delivery_courier> getList(pagingSet PAGING_SET) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME); 
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_delivery_courier>)listService.getDTOList(SELECT_QUERY.toString(), dto_delivery_courier.class);
    }

}
