package com.pood.server.api.service.iamport;

import com.pood.server.object.IMP.IMP;
import org.springframework.http.ResponseEntity;

public interface iamportService {

    // 아임포트 액세스 토큰 발급
    String getAccessToken() throws Exception;

    // 빌링키 발급
    ResponseEntity<String> doSimplePay(String CUSTOMER_UID, IMP IAMPORT) throws Exception;


    // 특정 주문 번호에 대해서 주문 정보 조회
    String getPayInfo(String ORDER_NUMBER) throws Exception;


    ResponseEntity<String> checkBank(String bank_code, String bank_num) throws Exception;


}
