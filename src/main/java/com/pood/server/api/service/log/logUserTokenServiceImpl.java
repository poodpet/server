package com.pood.server.api.service.log;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.pood.server.api.service.list.*;
import com.pood.server.api.service.query.queryService;
import com.pood.server.api.service.record.*;
import com.pood.server.config.DATABASE; 

@Service("logUserTokenService")
public class logUserTokenServiceImpl implements logUserTokenService{

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_LOG_USER_TOKEN;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
    
    @Override
    public void insertRecord(String USER_UUID, String TOKEN) throws SQLException {
        // 발급된 토큰이 없을 경우 신규로 토큰 생성 
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_uuid", USER_UUID);
        hashMap.put("token",     TOKEN);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);

        updateService.insert(query);

        query = null;
    }

}
