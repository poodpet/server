package com.pood.server.api.service.pood_point;

public class tmp0 {

    private String  point_uuid;

    private Integer used_point;

    private Integer status;

    private Integer to_be_point;

    public tmp0(String point_uuid, Integer used_point, Integer status, Integer to_be_point){
        this.point_uuid = point_uuid;
        this.used_point = used_point;
        this.status = status;
        this.to_be_point = to_be_point;
    }

    public String getPoint_uuid() {
        return point_uuid;
    }

    public void setPoint_uuid(String point_uuid) {
        this.point_uuid = point_uuid;
    }

    public Integer getUsed_point() {
        return used_point;
    }

    public void setUsed_point(Integer used_point) {
        this.used_point = used_point;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getTo_be_point() {
        return to_be_point;
    }

    public void setTo_be_point(Integer to_be_point) {
        this.to_be_point = to_be_point;
    }

    @Override
    public String toString() {
        return "tmp0 [point_uuid=" + point_uuid + ", status=" + status + ", to_be_point=" + to_be_point
                + ", used_point=" + used_point + "]";
    } 

    

    

    
}
