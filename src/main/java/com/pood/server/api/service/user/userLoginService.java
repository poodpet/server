package com.pood.server.api.service.user;

import java.sql.SQLException;

public interface userLoginService {
    

    public Integer insertRecord(Integer user_idx, String user_uuid, Integer in_out) throws SQLException;

	public String getLastDateTime(Integer user_idx) throws Exception;

    public void deleteRecord(String uSER_UUID) throws SQLException;
    
}
