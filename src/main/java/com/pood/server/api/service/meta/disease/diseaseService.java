package com.pood.server.api.service.meta.disease;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_disease;
import com.pood.server.dto.meta.dto_disease;

public interface diseaseService {
 

    /**************** 질병 목록 조회 ***************/
    public List<dto_disease> getDiseaseList(pagingSet PAGING_SET) throws Exception;

    /**************** 전체 목록 개수 조회 ******************/
    public Integer getTotalRecordNumber() throws SQLException;

    public List<vo_disease> getObjectList() throws Exception;
 
}
