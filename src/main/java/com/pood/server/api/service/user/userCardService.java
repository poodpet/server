package com.pood.server.api.service.user;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.user.dto_user_simple_card;

public interface userCardService {
    

    /*************** 간편결제 카드를 신규 등록합니다. *****************/
    public Integer insertRecord(Integer user_idx, String billing_key, String card_name, String card_number, String card_user, Boolean main_card) throws SQLException;

    public Integer insertRecord(Integer user_idx, String billing_key, String customer_uid, String card_name, String card_number, String card_user, Boolean main_card) throws SQLException;


    /******************* 간편 카드 항목 목록 조회 **************/
    public List<dto_user_simple_card> getUserCardList(pagingSet PAGING_SET, Integer user_idx) throws Exception;
 
 
    /************* 간편 카드 항목 전체 개수를 조회합니다. ********************/
    public Integer getTotalRecordNumber(Integer user_idx) throws SQLException;


    /*************** 회원 간편결제 카드 항목을 업데이트 합니다. **************/
	public void updateUserCard(String BILLING_KEY, String CARD_NAME, String CARD_NUMBER, String CARD_USER,
			Boolean MAIN_CARD, Integer CARD_IDX) throws SQLException;

	public void updateRecord(String CARD_NAME, String CARD_NUMBER, String CARD_USER, Boolean MAIN_CARD, String CUSTOMER_UID) throws SQLException;

    public void deleteRecord(Integer e) throws SQLException;

    public void deleteRecordWithUserIDX(Integer USER_IDX) throws SQLException;

}
