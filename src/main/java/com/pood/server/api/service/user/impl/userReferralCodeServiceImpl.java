package com.pood.server.api.service.user.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.service.user.userReferralCodeService;
import com.pood.server.config.DATABASE;
import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.query.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("userReferralCodeService")
public class userReferralCodeServiceImpl implements userReferralCodeService {
    
    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_USER_REFERRAL_CODE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
    
    // 회원 추천인 코드 삽입
    public Integer insertRecord(Integer USER_IDX, Integer RECEIVED_USER_IDX, String REFERRAL_CODE) throws SQLException{
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_idx", USER_IDX);
        hashMap.put("received_user_idx", RECEIVED_USER_IDX);
        hashMap.put("referral_code", REFERRAL_CODE);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }

    public Integer getSignUPUserCount(Integer USER_IDX) throws SQLException{
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        CNT_QUERY.buildEqual("received_user_idx", USER_IDX);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @Override
    public List<Integer> getInvitedList(Integer user_idx) throws SQLException {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("received_user_idx", user_idx);
        return listService.getIDXList(SELECT_QUERY.toString(), "user_idx");
    }

    @Override
    public void deleteRecordWithUSERIDX(Integer USER_IDX) throws SQLException {
        if(USER_IDX != null){
            DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
            DELETE_QUERY.buildEqual("received_user_idx", Integer.toString(USER_IDX));
            updateService.execute(DELETE_QUERY.toString());

            DELETE_QUERY DELETE_QUERY2 = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
            DELETE_QUERY2.buildEqual("user_idx", Integer.toString(USER_IDX));
            updateService.execute(DELETE_QUERY2.toString());
        }
    }
}
