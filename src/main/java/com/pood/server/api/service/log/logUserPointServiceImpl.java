package com.pood.server.api.service.log;

import com.pood.server.config.DATABASE;
import com.pood.server.dto.log.dto_log_user_point;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.user.*;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.meta.point.*;
import com.pood.server.api.service.query.*;

@Service(value="logUserPointService")
public class logUserPointServiceImpl implements logUserPointService{

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;
    
    @Autowired
    @Qualifier("userPointService")
    userPointService userPointService;

    @Autowired
    @Qualifier("pointService")
    pointService pointService;

    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_LOG_USER_POINT;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    public Integer insertRecord(String user_uuid, String point_uuid, Integer point) throws SQLException{
        return updateService.insert(GET_QUERY_INSERT_RECORD(user_uuid, point_uuid, point));
    }

    public String GET_QUERY_INSERT_RECORD(String user_uuid, String point_uuid, Integer point){
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_uuid",    user_uuid);
        hashMap.put("point_uuid",   point_uuid);
        hashMap.put("point",        point);
        return queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);
    }

    @Override
    public void init(String USER_UUID) throws Exception {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_uuid", USER_UUID);
        updateService.execute(DELETE_QUERY.toString());
    }

    @SuppressWarnings("unchecked")
    public List<dto_log_user_point> getList(String user_uuid) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", user_uuid);
        List<dto_log_user_point> list = (List<dto_log_user_point>)listService.getDTOList(SELECT_QUERY.toString(), dto_log_user_point.class);
        return list;
    }

}
