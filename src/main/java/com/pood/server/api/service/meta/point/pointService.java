package com.pood.server.api.service.meta.point;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_point;

public interface pointService {
    
 
    /************ 포인트 정보 가지고 옴 ************/
    public dto_point getPointObject(Integer POINT_IDX) throws Exception;

    /**************** 포인트 목록 조회 *****************/
    public List<dto_point> getPointList(pagingSet PAGING_SET) throws Exception;

    /**************** 포인트 목록 개수 조회 ******************/
    public Integer getTotalRecordNumber() throws SQLException;

 
    public Integer getPoint(Integer point_type, Integer order_price, Integer delivery_fee) throws Exception;
}
