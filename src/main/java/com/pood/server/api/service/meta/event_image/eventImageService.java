package com.pood.server.api.service.meta.event_image;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.event.dto_event_image;
import com.pood.server.dto.meta.event.dto_event_image_2;

public interface eventImageService {

	public Integer getTotalRecordNumber() throws SQLException;

	public List<dto_event_image> getList(pagingSet PAGING_SET) throws Exception;

	public List<dto_event_image_2> getImageList(Integer EVENT_IDX) throws Exception;
    
}
