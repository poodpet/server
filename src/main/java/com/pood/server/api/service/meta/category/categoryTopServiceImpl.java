package com.pood.server.api.service.meta.category;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.pood.server.api.service.record.*;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.meta.category_image.categoryImageService;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.meta.dto_category_top;
import com.pood.server.object.meta.vo_category;
import com.pood.server.object.meta.vo_category_data;
import com.pood.server.object.meta.vo_category_data_field;
import com.pood.server.object.meta.vo_category_field;
import com.pood.server.object.meta.vo_category_image;
import com.pood.server.object.meta.vo_category_list;
import com.pood.server.object.resp.resp_category;

@Service("categoryTopService")
public class categoryTopServiceImpl implements categoryTopService{
    
    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("categoryService")
    categoryService categoryService;

    @Autowired
    @Qualifier("categoryFieldService")
    categoryFieldService categoryFieldService;

    @Autowired
    @Qualifier("categoryImageService")
    categoryImageService categoryImageService;
 
    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_CATEGORY_TOP;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @SuppressWarnings("unchecked")
    public resp_category getList(Integer pc_id) throws Exception{
        resp_category result = new resp_category();


        // 카테고리 상단 별로 리스트 호출
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);

        List<dto_category_top> list = (List<dto_category_top>)listService.getDTOList(SELECT_QUERY.toString(), dto_category_top.class);

        result.setPc_id(pc_id);
        

        
        List<vo_category> CATEGORY_LIST                    = categoryService.getList(pc_id);

        List<vo_category_field> CATEGORY_FIELD_LIST    = categoryFieldService.getList();

        List<vo_category_list> RESULT_CATEGORY_LIST = new ArrayList<vo_category_list>();

        List<vo_category_image> CATEGORY_IMAGE_LIST    = categoryImageService.getList();


        if(list != null){
            for(dto_category_top e : list){

                Integer category_top_idx = e.getIdx();



                // 카테고리 리스트별 데이터 저장
                vo_category_list record = new vo_category_list();
                record.setTitle(e.getName());
                record.setPriority(e.getPriority());

                Boolean vi = e.getVisible();
                if(vi == false)
                    record.setVisible(0);
                else record.setVisible(1);




                // 카테고리 리스트 생성
                List<vo_category_data> CATEGORY_DATA_LIST = new ArrayList<vo_category_data>();




                // 카테고리 상단에 해당되는 카테고리에 대해서 저장
                for(vo_category e1 : CATEGORY_LIST){

                    Integer CT_IDX = e1.getIdx();

                    if(e1.getTp_idx().equals(category_top_idx)){

                        vo_category_data record2 = new vo_category_data();
                        record2.setPriority(e1.getPriority());
                        record2.setType(category_top_idx);
                        record2.setTitle(e1.getCt_name());
                        record2.setSub_title(e1.getSub_title());

                        String IMAGE_URL = "";

                        if(CATEGORY_IMAGE_LIST != null)
                            for(vo_category_image e3 : CATEGORY_IMAGE_LIST)
                                if(e3.getCt_idx().equals(CT_IDX))
                                    IMAGE_URL = e3.getUrl();

                        record2.setUrl(IMAGE_URL);
                        record2.setVisible(e1.getVisible());


                        

                        // 카테고리에 해당하는 필드 리스트 조회 및 저장
                        List<vo_category_data_field> CATEGORY_DATA_FIELD_LIST = new ArrayList<vo_category_data_field>();
                        
                        for(vo_category_field e2 : CATEGORY_FIELD_LIST){
                            if(CT_IDX.equals(e2.getCt_idx())){
                                vo_category_data_field record3 = new vo_category_data_field();
                                record3.setKey(e2.getField_key());
                                record3.setValue(e2.getField_value());
                                CATEGORY_DATA_FIELD_LIST.add(record3);

                                record3 = null;
                            }
                        }

                        record2.setField(CATEGORY_DATA_FIELD_LIST);
                        
                        CATEGORY_DATA_LIST.add(record2);


                        record2 = null;
                    }

                    CT_IDX = null;
                }
                

                record.setCategory(CATEGORY_DATA_LIST);

                RESULT_CATEGORY_LIST.add(record); 

                record = null;
            }
        }

        result.setData(RESULT_CATEGORY_LIST);

        return result;
    }

    @Override
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(
            DEFINE_DB_NAME, 
            DEFINE_TABLE_NAME); 
        return listService.getRecordCount(CNT_QUERY.toString());
    }

}
