package com.pood.server.api.service.user;

import java.sql.SQLException;
import java.util.List;

public interface userReferralCodeService {
 
    // 회원 추천인 코드 삽입
    public Integer insertRecord(Integer USER_IDX, Integer RECEIVED_USER_IDX, String REFERRAL_CODE) throws SQLException;

    public Integer getSignUPUserCount(Integer USER_IDX) throws SQLException;

    public List<Integer> getInvitedList(Integer user_idx) throws SQLException;

    public void deleteRecordWithUSERIDX(Integer USER_IDX) throws SQLException;

 }
