package com.pood.server.api.service.delivery_tracker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service("deliveryTrackerService")
public class deliveryTrackerServiceImpl implements deliveryTrackerService {

    private static final String BASE_URL = "https://apis.tracker.delivery/carriers/";

    Logger logger = LoggerFactory.getLogger(deliveryTrackerServiceImpl.class);

    @Override
    public String getTrackingRecord(String CARRIER, String TRACKING_ID) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        String HTTP_URL = BASE_URL + CARRIER + "/tracks/" + TRACKING_ID;
        logger.info(HTTP_URL);
        String result = null;
        try{
            result = restTemplate.getForObject(HTTP_URL, String.class);
        }catch(HttpClientErrorException e){
            
        }
        return result;
    }

}
