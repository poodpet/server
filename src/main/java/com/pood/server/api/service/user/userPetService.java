package com.pood.server.api.service.user;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.user.pet.dto_user_pet;
import com.pood.server.object.resp.resp_user_pet;

public interface userPetService {
    




    /***************** 회원 반려동물 정보 삽입 *********************/
    public Integer insertUserPet(Integer USER_IDX, Integer PET_ACTIVITY, Integer PC_ID, Integer PSC_ID, String PET_NAME, String PET_BIRTH, Integer PET_GENDER, Float PET_WEIGHT, Integer PET_STATUS) throws SQLException;









    /***************** 회원 반려동물 정보 업데이트 *********************/
    public void updateUserPet(Integer USER_PET_IDX, Integer PET_ACTIVITY, String PET_NAME, String PET_BIRTH, Integer PET_GENDER, Float PET_WEIGHT, Integer PET_STATUS, Integer PC_ID, Integer PSC_ID) throws SQLException;







    
    /***************** 회원의 반려동물 목록 반환 *******************/
    public List<resp_user_pet> getUserPetList(Integer user_idx, Integer user_pet_idx) throws Exception;









    /******************* 회원 펫 이미지를 신규로 등록 *********************************/
    public void updateUserPetImage(Integer USER_PET_IDX, Integer IMAGE_IDX) throws SQLException;








    public void deleteImage(Integer e) throws Exception;







    public Integer insertImageRecord(String img_url) throws SQLException;







    public void deleteRecord(Integer e) throws SQLException;






    public void deleteImageWithUserPetIDX(Integer e) throws Exception;







    public Integer getUserPetCount(String USER_UUID) throws Exception;






    public void deleteRecordWithUserUUID(String USER_UUID) throws SQLException;






    public List<dto_user_pet> getList(Integer USER_IDX) throws Exception;





}
