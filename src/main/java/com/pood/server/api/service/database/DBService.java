package com.pood.server.api.service.database;

import com.pood.server.dto.dto_column_list;
import java.sql.SQLException;
import java.util.List;

public interface DBService {

    List<dto_column_list> getColumnList(String db_name, String table_name) throws SQLException;

    List<Object> getdtoList(String query, Class<?> clazz) throws Exception;

    void commit(List<String> query) throws SQLException;

    void update(String query) throws SQLException;

    Integer insert(String query) throws SQLException;

    Object getDTOObject(String query, Class<?> cls) throws SQLException;

    Integer getRecordCount(String query) throws SQLException;

    List<Integer> getIDXList(String query) throws SQLException;

    List<Integer> getIDXListWithColumnName(String query, String column_name) throws SQLException;

}
