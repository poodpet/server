package com.pood.server.api.service.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Service;

@Service("jsonService")
public class jsonServiceImpl implements jsonService{

    // 해제된 오브젝트 반환
    public Object getDecodedObject(String text, Class<?> value_type) throws Exception {
        JSONParser parser = new JSONParser();
        Object obj = parser.parse( text ); 
        JSONObject jsonObj = (JSONObject) obj;
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(jsonObj.toString(), value_type); 
    }
}
