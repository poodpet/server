package com.pood.server.api.service.meta.maintab;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.meta.dto_maintab_ct;

public interface maintabCTService {

    public Integer getTotalRecordNumber() throws SQLException;

    public List<dto_maintab_ct> getList() throws Exception;
    
}
