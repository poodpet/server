package com.pood.server.api.service.log;

import com.pood.server.object.log.vo_log_user_coupon_code;

public interface logUserCouponCodeService{

    public vo_log_user_coupon_code getRecord(String user_uuid, String code) throws Exception;
    
}
