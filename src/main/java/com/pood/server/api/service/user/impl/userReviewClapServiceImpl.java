package com.pood.server.api.service.user.impl;

import java.sql.SQLException;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.object.user.vo_user_review_clap;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.user.review.dto_user_review_clap;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.user.*;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.query.*;
import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.DELETE_QUERY;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("userReviewClapService")
public class userReviewClapServiceImpl implements userReviewClapService {

    
    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;
    
    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_USER_REVIEW_CLAP;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
     
    /****************** 회원이 도움 누른 리뷰 목록 ************************/
    public List<Integer> getReviewClapList(Integer user_idx) throws SQLException {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_idx", user_idx);
        return listService.getIDXList(SELECT_QUERY.toString(), "review_idx");
    }

 

    /*************** 사용자 리뷰 도움이 있는지 확인 ******************/
    @SuppressWarnings("unchecked")
    public vo_user_review_clap checkReviewClap(Integer USER_IDX, Integer REVIEW_IDX) throws Exception{
        Integer REVIEW_CLAP_IDX = 0;
        Boolean isReviewed = false;
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_idx", USER_IDX);
        SELECT_QUERY.buildEqual("review_idx", REVIEW_IDX);

        
        List<dto_user_review_clap> result = (List<dto_user_review_clap>)listService.getDTOList(SELECT_QUERY.toString(), dto_user_review_clap.class);

        if(result != null){
            for(dto_user_review_clap e : result){
                isReviewed = true;
                REVIEW_CLAP_IDX = (Integer)e.getIdx();
            }
        }

        vo_user_review_clap review_clap = new vo_user_review_clap();
        review_clap.setIdx(REVIEW_CLAP_IDX);
        review_clap.setIsReviewed(isReviewed);
        
        return review_clap;
    }

 
    

     /*************** 리뷰 도움 레코드 추가 ************************/
    public Integer insertReviewClap(Integer USER_IDX, Integer REVIEW_IDX) throws SQLException {
        // 삽입 쿼리를 실행하기 위한 빌더 생성 
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_idx", USER_IDX);
        hashMap.put("review_idx", REVIEW_IDX);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }
 


    /**************** 리뷰에 해당하는 리뷰 좋아요 수 조회 ******************/
    public Integer getReviewClapCount(Integer REVIEW_IDX) throws SQLException{
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        CNT_QUERY.buildEqual("review_idx", REVIEW_IDX);
        return listService.getRecordCount(CNT_QUERY.toString());
    }



    @Override
    public void deleteRecord(Integer REVIEW_CLAP_IDX) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("idx", Integer.toString(REVIEW_CLAP_IDX));
        updateService.execute(DELETE_QUERY.toString());
    }



    @SuppressWarnings("unchecked")
    public List<dto_user_review_clap> getList(Integer USER_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_idx", USER_IDX);
        List<dto_user_review_clap> list = (List<dto_user_review_clap>)listService.getDTOList(SELECT_QUERY.toString(), dto_user_review_clap.class);
        return list;
    }
}
