package com.pood.server.api.service.meta.disease.impl;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.api.service.record.updateService;
import com.pood.server.config.DATABASE;
import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_disease;
import com.pood.server.dto.meta.dto_disease;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.meta.disease.diseaseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("diseaseService")
public class diseaseServiceImpl implements diseaseService {

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_PET_DISEASE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
 
    /**************** 질병 목록 조회 ***************/
    @SuppressWarnings("unchecked")
    public List<dto_disease> getDiseaseList(pagingSet PAGING_SET) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_disease>)listService.getDTOList(SELECT_QUERY.toString(), dto_disease.class);
    }

    /**************** 전체 목록 개수 조회 ******************/
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @SuppressWarnings("unchecked")
    public List<vo_disease> getObjectList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return (List<vo_disease>)listService.getDTOList(SELECT_QUERY.toString(), vo_disease.class);
    }
 
}
