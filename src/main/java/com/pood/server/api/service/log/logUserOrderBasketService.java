package com.pood.server.api.service.log;

import java.sql.SQLException;

public interface logUserOrderBasketService {

    public Integer insertRecord(String USER_UUID, String MERCHANT_UID, Integer GOODS_IDX, Integer PURCHASE_QUANTITY) throws SQLException;
    
    public String GET_QUERY_INSERT_RECORD(String USER_UUID, String MERCHANT_UID, Integer GOODS_IDX, Integer PURCHASE_QUANTITY);
}
