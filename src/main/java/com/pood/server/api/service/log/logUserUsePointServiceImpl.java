package com.pood.server.api.service.log;

import com.pood.server.config.DATABASE;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.log.dto_log_user_use_point;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.user.*;
import com.pood.server.api.service.record.*; 
import com.pood.server.api.service.meta.order.*;
import com.pood.server.api.service.meta.point.*;
import com.pood.server.api.service.query.*
;
@Service("logUserUsePointService")
public class logUserUsePointServiceImpl implements logUserUsePointService{
    
    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;
    
    @Autowired
    @Qualifier("userPointService")
    userPointService userPointService;

    @Autowired
    @Qualifier("pointService")
    pointService pointService;

    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("logUserPointService")
    logUserPointService  logUserPointService;

    @Autowired
    @Qualifier("orderPointService")
    orderPointService orderPointService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_LOG_USER_USE_POINT;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
    
    @SuppressWarnings("unchecked")
    public List<dto_log_user_use_point> getList(pagingSet PAGING_SET, String user_uuid) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", user_uuid);
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_log_user_use_point>)listService.getDTOList(SELECT_QUERY.toString(), dto_log_user_use_point.class);
    }

    @Override
    public Integer getTotalRecordNumber(String user_uuid) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @Override
    public void init(String USER_UUID) throws Exception {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_uuid", USER_UUID);
        updateService.execute(DELETE_QUERY.toString());
    }

    @Override
    public Integer insertRecord(String USER_UUID, Integer TYPE, String TEXT, Integer USE_POINT) throws Exception {
        return updateService.insert(GET_QUERY_INSERT_RECORD(USER_UUID, TYPE, TEXT, USE_POINT));
    }

    public String GET_QUERY_INSERT_RECORD(String USER_UUID, Integer TYPE, String TEXT, Integer USE_POINT){
        Map<String, Object> hashMap = new HashMap<String, Object>();    
        hashMap.put("user_uuid",    USER_UUID);
        hashMap.put("type",         TYPE);
        hashMap.put("text",         TEXT);
        hashMap.put("use_point",    USE_POINT);
        return queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);
    }
 
}
