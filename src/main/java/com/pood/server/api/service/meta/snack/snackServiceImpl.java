package com.pood.server.api.service.meta.snack;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.record.*;
import com.pood.server.config.DATABASE;
import com.pood.server.object.meta.vo_snack;

import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


@Service("snackService")
public class snackServiceImpl implements snackService{

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    private String DEFINE_TABLE_NAME    = com.pood.server.config.DATABASE.TABLE_SNACK;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);


    /****************  ******************/
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        Integer total_cnt = listService.getRecordCount(CNT_QUERY.toString());
        return total_cnt;
    }


    @Override
    public vo_snack getRecord(Integer product_idx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("product_idx", product_idx);
        return (vo_snack)listService.getDTOObject(SELECT_QUERY.toString(), vo_snack.class);
    }

}
