package com.pood.server.api.service.meta.order.impl;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.delivery_tracker.deliveryTrackerService;
import com.pood.server.api.service.holiday_check.holidayCheckService;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.log.logUserOrderService;
import com.pood.server.api.service.meta.goods.goodsService;
import com.pood.server.api.service.meta.image.imageService;
import com.pood.server.api.service.meta.order.OrderService;
import com.pood.server.api.service.meta.order.orderBasketService;
import com.pood.server.api.service.meta.order.orderCancelService;
import com.pood.server.api.service.meta.order.orderDeliveryService;
import com.pood.server.api.service.meta.order.orderRefundService;
import com.pood.server.api.service.meta.payment_type.PTypeService;
import com.pood.server.api.service.meta.point.pointService;
import com.pood.server.api.service.query.queryService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.api.service.time.timeService;
import com.pood.server.api.service.user.userReviewService;
import com.pood.server.api.service.user.userService;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.order.ORDER_CANCEL;
import com.pood.server.config.meta.order.ORDER_REFUND;
import com.pood.server.config.meta.order.ORDER_STATUS;
import com.pood.server.config.meta.point.META_POINT_TYPE;
import com.pood.server.dto.meta.goods.dto_goods_3;
import com.pood.server.dto.meta.order.dto_order;
import com.pood.server.dto.meta.order.dto_order_cancel;
import com.pood.server.dto.meta.order.dto_order_refund;
import com.pood.server.object.IMP.IMP;
import com.pood.server.object.IMP.IMP_CANCEL;
import com.pood.server.object.IMP.IMP_RESULT_ORDER;
import com.pood.server.object.resp.resp_ava_list;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("orderService")
public class OrderServiceImpl implements OrderService {

    Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

    private static final String NAVER_PAY = "naverpay";

    @Autowired
    @Qualifier("holidayCheckService")
    holidayCheckService holidayCheckService;

    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("imageService")
    public imageService imageService;

    @Autowired
    @Qualifier("updateService")
    public updateService updateService;

    @Autowired
    @Qualifier("goodsService")
    public goodsService goodsService;

    @Autowired
    @Qualifier("listService")
    public listService listService;

    @Autowired
    @Qualifier("orderBasketService")
    orderBasketService orderBasketService;

    @Autowired
    @Qualifier("orderDeliveryService")
    orderDeliveryService orderDeliveryService;

    @Autowired
    @Qualifier("orderRefundService")
    orderRefundService orderRefundService;

    @Autowired
    @Qualifier("orderCancelService")
    orderCancelService orderCancelService;

    @Autowired
    @Qualifier("logUserOrderService")
    logUserOrderService logUserOrderService;

    @Autowired
    @Qualifier("userReviewService")
    userReviewService userReviewService;

    @Autowired
    @Qualifier("deliveryTrackerService")
    deliveryTrackerService userDeliveryTrackerService;

    @Autowired
    @Qualifier("timeService")
    timeService timeService;

    @Autowired
    @Qualifier("pointService")
    pointService pointService;

    @Autowired
    @Qualifier("PTypeService")
    PTypeService PTypeService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_ORDER;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    /***************** 주문 상태 업데이트 *******************/
    public void updateOrderStatus(Integer orderStatus, Integer orderIdx) throws SQLException {
        updateService.execute(GET_QUERY_UPDATE_ORDER_STATUS(orderStatus, orderIdx));
    }


    public String GET_QUERY_UPDATE_ORDER_STATUS(Integer status, Integer orderIdx){
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("order_status", status);
        return queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", orderIdx.toString());
    }



    /************* 주문 번호에 해당하는 레코드 항목번호 조회 *************/
    @SuppressWarnings("unchecked")
    public Integer getOrderINDEX(String orderNumber) throws Exception {

        Integer ORDER_IDX = 0;

        //select * from order_db.order where order_number = orderNumber;
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", orderNumber);

        List<dto_order> list = (List<dto_order>) listService.getDTOList(SELECT_QUERY.toString(), dto_order.class);

        if (list != null) {
            for (dto_order e : list) {
                ORDER_IDX = (Integer) e.getIdx();
            }
        }

        return ORDER_IDX;
    }




    public String GET_QUERY_INSRT_ORDER(IMP IAMPORT) throws Exception{
        Integer POINT_IDX = 0;
        
        POINT_IDX = META_POINT_TYPE.PURCHASE;

        Integer SAVED_POINT     = pointService.getPoint(POINT_IDX, IAMPORT.PAYMENT_ORDER_PRICE, IAMPORT.ORDER_DELIVERY_FEE);

        String PAYMENT_METHOD   = IAMPORT.PAYMENT_PAY_METHOD;

        String code = NAVER_PAY.equals(IAMPORT.PAYMENT_PG) ? NAVER_PAY : PAYMENT_METHOD;
        Integer ORDER_TYPE      = PTypeService.getOrderType(code);

        logger.info("최종 주문 금액 : " + IAMPORT.PAYMENT_ORDER_PRICE + "원, 배송지 금액 : " + IAMPORT.ORDER_DELIVERY_FEE + "원");

        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_idx", IAMPORT.USER_IDX);
        hashMap.put("order_name", IAMPORT.PAYMENT_ORDER_NAME);
        hashMap.put("order_device", IAMPORT.ORDER_DEVICE);
        hashMap.put("order_type", ORDER_TYPE);
        hashMap.put("total_price", IAMPORT.ORDER_TOTAL_PRICE);
        hashMap.put("order_price", IAMPORT.PAYMENT_ORDER_PRICE);
        hashMap.put("order_price_2", IAMPORT.PAYMENT_ORDER_PRICE);
        hashMap.put("used_point", IAMPORT.ORDER_USED_POINT);
        hashMap.put("saved_point", SAVED_POINT);
        hashMap.put("discount_coupon_price", IAMPORT.OVER_COUPON_DISCOUNT_PRICE);
        hashMap.put("total_discount_price", IAMPORT.ORDER_TOTAL_DISCOUNT_PRICE);
        hashMap.put("free_purchase", IAMPORT.ORDER_FREE_PURCHASE);
        hashMap.put("is_delete", 0);
        hashMap.put("address_update", 0);
        hashMap.put("order_number", IAMPORT.MERCHANT_UID);
        hashMap.put("order_status", com.pood.server.config.meta.order.ORDER_STATUS.ORDER_READY);
        hashMap.put("user_uuid", IAMPORT.USER_UUID);
        hashMap.put("delivery_fee", IAMPORT.ORDER_DELIVERY_FEE);
        hashMap.put("tracking_id", null);
        hashMap.put("delivery_type", com.pood.server.config.meta.order.ORDER_DELIVERY_TYPE.DEFAULT);
        hashMap.put("memo", IAMPORT.ORDER_MEMO);
        hashMap.put("over_coupon_idx", IAMPORT.ORDER_OVER_COUPON_IDX);
        hashMap.put("release_date", null);

        logger.info("*** 적립금 " + SAVED_POINT + "원이 주문번호[" + IAMPORT.MERCHANT_UID + "]에 대해서 적립될 예정입니다.");

        SAVED_POINT = null;

        return queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);
    }



    /****************** 주문 레코드 생성 ******************/
    public Integer insertOrder(IMP iamPort) throws Exception {
        return updateService.insert(GET_QUERY_INSRT_ORDER(iamPort));
    }



    

    /******************** 취소 정보 가져오기 ****************/
    @SuppressWarnings("unchecked")
    public IMP_CANCEL getCancelUnit(String orderNumber) throws Exception {

        IMP_CANCEL IMP_CANCEL = new IMP_CANCEL();

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", orderNumber);

        Integer ORDER_IDX = 0;
        Integer OVER_COUPON_IDX = 0;
        Integer USER_IDX = 0;
        Integer FROM_ORDER_STATUS = 0;
        String ORDER_NAME = "";

        List<dto_order> list = (List<dto_order>)listService.getDTOList(SELECT_QUERY.toString(), dto_order.class);

        if (list != null) {
            for (dto_order e : list) {
                ORDER_IDX = (Integer) e.getIdx();
                OVER_COUPON_IDX = (Integer) e.getOver_coupon_idx();
                USER_IDX = (Integer) e.getUser_idx();
                ORDER_NAME = (String) e.getOrder_name();
                FROM_ORDER_STATUS = (Integer) e.getOrder_status();
            }
        }

        IMP_CANCEL.setORDER_IDX(ORDER_IDX);
        IMP_CANCEL.setORDER_NUMBER(orderNumber);
        IMP_CANCEL.setOVER_COUPON_IDX(OVER_COUPON_IDX);
        IMP_CANCEL.setUSER_IDX(USER_IDX);
        IMP_CANCEL.setORDER_NAME(ORDER_NAME);
        IMP_CANCEL.setFROM_ORDER_STATUS(FROM_ORDER_STATUS);

        return IMP_CANCEL;
    }

    /**************** 주문 번호에 해당하는 주문 정보 가져오기 ****************/
    @SuppressWarnings("unchecked")
    public IMP_RESULT_ORDER getOrderInfo(String orderNumber) throws Exception {

        Integer ORDER_IDX = 0;
        Integer ORDER_STATUS = 0;
        Boolean isStatusCorrect = true;

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", orderNumber);

        List<dto_order> list = (List<dto_order>)listService.getDTOList(SELECT_QUERY.toString(), dto_order.class);
        if (list != null) {
            for (dto_order e : list) {
                ORDER_IDX = (Integer) e.getIdx();
                ORDER_STATUS = (Integer) e.getOrder_status();
                if (ORDER_STATUS >= com.pood.server.config.meta.order.ORDER_STATUS.ORDER_SUCCESS)
                    isStatusCorrect = false;
            }
        }

        IMP_RESULT_ORDER IMP_RESULT_ORDER = new IMP_RESULT_ORDER();
        IMP_RESULT_ORDER.setORDER_IDX(ORDER_IDX);
        IMP_RESULT_ORDER.setORDER_STATUS(ORDER_STATUS);
        IMP_RESULT_ORDER.setVALIDATION_ORDER_STATUS(isStatusCorrect);

        return IMP_RESULT_ORDER;
    }

    /*********** 트래킹 정보 업데이트 **********/
    public void updateTrackingInfo(Integer orderIdx, String trackingId) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("tracking_id", trackingId);
        
        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", orderIdx.toString());
        updateService.execute(query);
        query = null;

    }

    /************* 주문 번호에 해당하는 주문 리스트 조회 ****************/
    public dto_order getOrder(String orderNumber) throws Exception {
        // select * from order_db.order where order_number = orderNumber;
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", orderNumber);
        return (dto_order)listService.getDTOObject(SELECT_QUERY.toString(), dto_order.class);
    }

    /************* 주문 번호에 해당하는 주문 리스트 조회 ****************/
    public dto_order getOrder(Integer orderIdx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", orderIdx);
        return (dto_order)listService.getDTOObject(SELECT_QUERY.toString(), dto_order.class);
    }

    /**************** 회원 항목 번호에 해당하는 주문 리스트 조회 ************/
    @SuppressWarnings("unchecked")
    public List<dto_order> getDeliverySuccessList(Integer userIdx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_idx", userIdx);
        SELECT_QUERY.buildEqual("order_status", ORDER_STATUS.DELIVERY_SUCCESS);
        return (List<dto_order>)listService.getDTOList(SELECT_QUERY.toString(), dto_order.class);
    }

    /* 

    /**************** 주문 항목 개수 조회 ******************/
    public Integer getTotalRecordNumber(Integer orderIdx) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if (orderIdx != null)
            CNT_QUERY.buildEqual("idx", orderIdx);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    /************* ORDER IDX에 해당하는 ORDER NUMBER 조회 *************/
    @SuppressWarnings("unchecked")
    public String getOrderNumber(Integer orderIdx) throws Exception {
        String ORDER_NUMBER = "";
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", orderIdx);

        List<dto_order> list = (List<dto_order>)listService.getDTOList(SELECT_QUERY.toString(), dto_order.class);

        if (list != null) {
            for (dto_order e : list) {
                ORDER_NUMBER = (String) e.getOrder_number();
            }
        }

        return ORDER_NUMBER;
    }

    @SuppressWarnings("unchecked")
    public String getTrackingID(Integer orderIdx) throws Exception {

        String TRACKING_ID = "";

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_idx", orderIdx);

        List<dto_order> list = (List<dto_order>)listService.getDTOList(SELECT_QUERY.toString(), dto_order.class);

        if (list != null) {
            for (dto_order e : list) {
                TRACKING_ID = (String) e.getTracking_id();
            }
        }

        return TRACKING_ID;
    }

    @Override
    /* *************** MERCHANT UID 생성 ***************/
    public String generateMerchantUID(String orderDevice) {

        String ORDER_NUMBER = new SimpleDateFormat("yyyyMMddHHmmssmmm").format(Calendar.getInstance().getTime());

        switch (orderDevice) {
            case "ios":
                ORDER_NUMBER += "I";
                break;
            case "android":
                ORDER_NUMBER += "A";
                break;
            case "pcweb":
                ORDER_NUMBER += "P";
                break;
            case "mobileweb":
                ORDER_NUMBER += "M";
                break;
        }

        String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random rnd = new Random();
        char FIRST_CHARACTER = chars.charAt(rnd.nextInt(chars.length()));
        char SECOND_CHARACTER = chars.charAt(rnd.nextInt(chars.length()));

        ORDER_NUMBER += FIRST_CHARACTER;
        ORDER_NUMBER += SECOND_CHARACTER;

        return ORDER_NUMBER;
    }

    @Override
    public Integer getTotalUserOrderNumber(Integer userIdx) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        CNT_QUERY.buildEqual("user_idx", userIdx);
        CNT_QUERY.buildMoreThan("order_status", ORDER_STATUS.DELIVERY_SUCCESS);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @Override
    public Integer getShippingOrderCount(Integer userIdx) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        CNT_QUERY.buildEqual("user_idx", userIdx);
        CNT_QUERY.buildEqual("order_status", ORDER_STATUS.DELIVERY_START);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @Override
    public List<resp_ava_list> getAVAReviewList(Integer userIdx) throws Exception {

        List<resp_ava_list> result = new ArrayList<resp_ava_list>();


        

        // 회원의주문 배송완료 목록 전체 조회
        List<dto_order> USER_ORDER_LIST = getDeliverySuccessList(userIdx);

        for(dto_order e: USER_ORDER_LIST){

    
            Integer ORDER_IDX = (Integer)e.getIdx();

            String  ORDER_NUMBER = e.getOrder_number();



            // 주문번호에 해당하는 반품 목록 조회
            List<dto_order_refund> order_refund = orderRefundService.getList(ORDER_NUMBER);
    

            // 주문번호에 해당하는 취소 목록 조회
            List<dto_order_cancel> order_cancel = orderCancelService.getList(ORDER_NUMBER);

 



            resp_ava_list record = new resp_ava_list();

            
            List<dto_goods_3> GOODS_INFO = orderBasketService.getgoodsListWithOrderNumber(userIdx, ORDER_IDX, ORDER_NUMBER);

            
            List<dto_goods_3> GOODS_INFO_2 = new ArrayList<dto_goods_3>();

            for(dto_goods_3 e1 : GOODS_INFO){
                Integer GOODS_IDX = e1.getIdx();


                Boolean isRefund = false;

                Boolean isCancel = false;

 
                // 해당 물품이 반품이 되었는지 되지 않았는지 확인
                if(order_refund != null){
                    for(dto_order_refund e2 : order_refund){

                        Integer REFUND_GOODS_IDX = e2.getGoods_idx();
    
                        Integer REFUND_SUCCESS   = e2.getType();

                        if(REFUND_GOODS_IDX.equals(GOODS_IDX)){
                            if(REFUND_SUCCESS == ORDER_REFUND.TYPE_ACCEPT){
                                isRefund = true; 
                                break;
                            }
    
                        }
    
                        REFUND_GOODS_IDX = null;
    
                        REFUND_SUCCESS = null;
                    }
                }

 


                // 해당 물품이 취소되었는지 되지 않았는지 확인
                if(order_cancel != null){
                    for(dto_order_cancel e2 : order_cancel){

                        Integer CANCEL_GOODS_IDX = e2.getGoods_idx();

                        Integer CANCEL_SUCCESS  = e2.getType();

                        if(CANCEL_GOODS_IDX.equals(GOODS_IDX)){
                            if(CANCEL_SUCCESS == ORDER_CANCEL.SUCCESS){
                                isCancel = true;
                                break;
                            }
                        }

                        CANCEL_GOODS_IDX = null;

                        CANCEL_SUCCESS = null;

                    }
                }

 

                // 취소되거나 반품되지 않은 목록만 내려줌
                if((!isRefund) && (!isCancel)){

                    
                    if(e1.getProduct() != null){ 

                        Boolean isReviewed = false;

                        /************ 이미 리뷰가 되었는지 확인 *******************/
                        List<Integer> PRODUCT_IDX_LIST = e1.getProduct();

                        Iterator<Integer> iterator = PRODUCT_IDX_LIST.iterator();

                        while(iterator.hasNext()) {

                            // 이미 리뷰가 되었으면 목록에서 제거
                            Integer PRODUCT_IDX = iterator.next();
                            isReviewed = userReviewService.isReviewed(ORDER_NUMBER, GOODS_IDX,
                                userIdx, PRODUCT_IDX);
                            if(isReviewed)iterator.remove();

                        }    
                        
                        e1.setProduct(PRODUCT_IDX_LIST);

                        if(!PRODUCT_IDX_LIST.isEmpty())GOODS_INFO_2.add(e1);                    

                    }

                }
 
              
            }


            // 리뷰 작성가능한 목록이 있을 경우
            if(!GOODS_INFO_2.isEmpty()){
                record.setOrder_number(e.getOrder_number());
                record.setOrder_date(e.getRecordbirth());
                record.setGoods_info(GOODS_INFO_2);

                result.add(record);

                // 취소되거나 반품되지 않은 물품들에 한해서만 리뷰 작성 가능
                record = null;
            }

            order_refund = null;

            order_cancel = null;


            ORDER_NUMBER = null;

            ORDER_IDX = null;

        }

        return result;
    }
 

    @Override
    public Boolean isFirstUserPurchase(Integer userIdx) throws SQLException {
        
        Boolean isFirst = true;

        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME); 
        if(userIdx != null)
            CNT_QUERY.buildEqual("user_idx", userIdx);

        Integer total_cnt = listService.getRecordCount(CNT_QUERY.toString());

        if(total_cnt != 0)isFirst = false;
        
        return isFirst;
    }


    @Override
    public String GET_QUERY_UPDATE_REFUND_AMOUNT(Integer refundAmount, String orderNumber) {
        return queryService.getDeductValueQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, "order_price_2",
            refundAmount, "order_number", orderNumber);
    }


    @Override
    public String GET_QUERY_UPDATE_SAVED_POINT(Integer toBe, String orderNumber) {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("saved_point", toBe);
        return queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "order_number",
            orderNumber);
    }


    @Override
    public Boolean checkRetreiveTimeLimit(String orderNumber) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", orderNumber);


        // 공휴일 비교
        String CURRENT_DATE = timeService.getCurrentDate();

        List<String> holiday_data = holidayCheckService.getHolidayData();



        
        for(String e : holiday_data){
            // 공휴일에 해당하면 하루종일 취소 가능
            if(e.equals(CURRENT_DATE)){
                logger.info("금일 " + CURRENT_DATE + "은 공휴일로 하루종일 취소가 가능합니다.");
                return true;
            }
        }

 

        int TODAY = LocalDate.now().getDayOfWeek();




        
        // 토요일이거나 일요일인 경우 
        if((TODAY == 6) || (TODAY == 7))return true;

        dto_order record = (dto_order)listService.getDTOObject(SELECT_QUERY.toString(), dto_order.class);

        String recordbirth              = record.getRecordbirth();

        String retreieve_time           = timeService.getRetreieveTimeLimit();

        String current_time             = timeService.getCurrentTime();

        String retreieve_time_ys        = timeService.getRetreieveTimeYesterday();


        DateTime YESTERDAY_LIMIT_TIME   = timeService.getDateTime2(retreieve_time_ys);

        DateTime DATETIME_RECORDBIRTH   = timeService.getDateTime2(recordbirth);

        logger.info("주문 시간 : " + recordbirth + ", 현재 시간 : " + current_time + ", 취소 기준 시간 : " + retreieve_time);

        // 만약에 주문 시각이 금일에 해당한다면
        if(timeService.isInToday(DATETIME_RECORDBIRTH)){

            logger.info("해당 주문은 금일 주문된 주문 건입니다.");

            // 주문 시간이 두시를 넘었으면
            if(timeService.isExpired(recordbirth, retreieve_time)){
                logger.info("주문 시간이 두시를 경과했으므로 당일 하루동안 취소가 가능합니다.");
                return true;    // 그날 하루종일 철회 가능
            }

            // 주문 시간이 두시를 넘지 않았으면 
            if(!timeService.isExpired(current_time, retreieve_time))
                return true;
            else {
                logger.info("두시 이전에 주문된 주문건, 취소 시간이 두시를 초과하였으므로 취소가 불가합니다.");
                return false; // 두시 이후에 취소 철회를 하려고 하면 오류
            }

        } 






        // 어제 주문한 주문인 경우
        if(timeService.isInYesterDay(DATETIME_RECORDBIRTH)){

            if(!DATETIME_RECORDBIRTH.isBefore(YESTERDAY_LIMIT_TIME)){

                // 주문 시간이 두시를 넘지 않았으면 
                if(!timeService.isExpired(current_time, retreieve_time)){
                    logger.info("어제 두시 이후로 주문된 건. 금일 두시 전에 취소를 시도하였으므로 취소가 가능합니다.");
                    return true;
                }else {
                    logger.info("어제 두시 이후로 주문된 건. 금일 두시 이후에 취소를 시도하였으므로 취소가 불가능합니다.");
                    return false; // 두시 이후에 취소 철회를 하려고 하면 오류
                }

            }else{
                logger.info("주문 시간 : " + recordbirth + "이 어제일자로 취소 시간을 경과했습니다. : "+retreieve_time_ys);
                return false;
            }
        }

        return false;
    }

  
}
