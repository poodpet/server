package com.pood.server.api.service.meta.promotion;

import org.springframework.stereotype.Service;
import com.pood.server.api.service.record.*;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.meta.dto_promotion;
import com.pood.server.dto.meta.dto_promotion_group;
import com.pood.server.dto.meta.dto_promotion_image;
import com.pood.server.object.meta.vo_promotion_data;
import com.pood.server.object.meta.vo_promotion_data_image;
import com.pood.server.object.meta.vo_promotion_data_prgroup;
import com.pood.server.object.meta.vo_promotion_data_prgroup_goods;
import com.pood.server.object.meta.vo_promotion_group_goods;
import com.pood.server.api.service.user.*;
import com.pood.server.api.service.view.view_9.*;
import com.pood.server.api.service.view.view_9_1.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.listService;

@Service("promotionService")
public class promotionServiceImpl implements promotionService{
 
    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("view9Service")
    view9Service view9Service;

    @Autowired
    @Qualifier("view91Service")
    view91Service view91Service;

    @Autowired
    @Qualifier("promotionGroupService")
    promotionGroupService promotionGroupService;

    @Autowired
    @Qualifier("promotionGroupGoodsService")
    promotionGroupGoodsService promotionGroupGoodsService;

    @Autowired
    @Qualifier("userReviewService")
    userReviewService userReviewService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_PROMOTION;

    private String DEFINE_IMAGE_TABLE_NAME = DATABASE.TABLE_PROMOTION_IMAGE;
    
    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @SuppressWarnings("unchecked")
    public List<dto_promotion_image> getImageList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME);
        return (List<dto_promotion_image>)listService.getDTOList(SELECT_QUERY.toString(), dto_promotion_image.class);
    }

    @SuppressWarnings("unchecked")
    public List<vo_promotion_data_image> getPRImageList(Integer pr_idx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME);
        SELECT_QUERY.buildEqual("pr_idx", pr_idx);
        return (List<vo_promotion_data_image>)listService.getDTOList(SELECT_QUERY.toString(), vo_promotion_data_image.class);
    }

    @Override
    public Integer getTotalRecordNumber(Integer pc_id, Integer pr_idx) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if(pr_idx != null)
            CNT_QUERY.buildEqual("idx",     pr_idx);
        if(pc_id != null)
            CNT_QUERY.buildEqual("pc_id",   pc_id);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @SuppressWarnings("unchecked")
    public List<vo_promotion_data> getList(Integer pc_id, Integer pr_idx) throws Exception {

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if(pr_idx != null)
            SELECT_QUERY.buildEqual("idx",  pr_idx);
        if(pc_id != null)
            SELECT_QUERY.buildEqual("pc_id",  pc_id);


        // 전체 프로모션 그룹 리스트 조회
        List<dto_promotion_group>        PROMOTION_GROUP_LIST        = promotionGroupService.getList();

        // 전체 프로모션 그룹 굿즈 리스트 조회
        List<vo_promotion_group_goods>  PROMOTION_GROUP_GOODS_LIST  = view9Service.getList();

        // 전체 프로모션 그룹 굿즈 리스트 조회
        List<vo_promotion_group_goods>  PROMOTION_GROUP_GOODS_LIST_2  = view91Service.getList();
        

        List<dto_promotion>  list = (List<dto_promotion>)listService.getDTOList(SELECT_QUERY.toString(), dto_promotion.class);
        
        List<vo_promotion_data> promotion_data = new ArrayList<vo_promotion_data>();

        if(list != null){
            for(dto_promotion e : list){

                Integer PROMOTION_IDX = e.getIdx();



                // 프로모션 정보 저장
                vo_promotion_data record = new vo_promotion_data();
                record.setTitle(e.getName());
                record.setType(e.getType());
                record.setPriority(e.getPriority());
                record.setPc_id(e.getPc_id());



                // 프로모션 이미지 저장
                List<vo_promotion_data_image>       PROMOTION_IMAGE_LIST = getPRImageList(PROMOTION_IDX);
                record.setImage(PROMOTION_IMAGE_LIST);
                PROMOTION_IMAGE_LIST = null;

                List<vo_promotion_data_prgroup> PROMOTION_DATA_PRGROUP_LIST = new ArrayList<vo_promotion_data_prgroup>();


                

                // 프로모션에 매핑된 그룹 정보 저장
                if(PROMOTION_GROUP_LIST != null){
                    for(dto_promotion_group e1 : PROMOTION_GROUP_LIST){

                        Integer PR_GROUP_IDX = e1.getIdx();
                        
                        Integer tmp = e1.getPr_idx();

                        if(tmp.equals(PROMOTION_IDX)){
                            vo_promotion_data_prgroup record2 = new vo_promotion_data_prgroup();
                            record2.setName(e1.getName());


                            // 프로모션 그룹에 매핑된 굿즈 정보 조회
                            List<vo_promotion_data_prgroup_goods> PROMOTION_DATA_PRGROUP_GOODS = new ArrayList<vo_promotion_data_prgroup_goods>();

                            for(vo_promotion_group_goods e2 : PROMOTION_GROUP_GOODS_LIST){
                                Integer A_PR_GROUP_IDX           = e2.getPr_group_idx();

                                Integer A_PR_GROUP_GOODS_IDX    = e2.getGoods_idx();

                                if(A_PR_GROUP_IDX.equals(PR_GROUP_IDX)){

                                    vo_promotion_data_prgroup_goods record3 = new vo_promotion_data_prgroup_goods();

                                    Boolean isFlag = true;


                                    // 만약 굿즈 이미지가 있을 경우 굿즈 이미지를 가지고 옴
                                    for(vo_promotion_group_goods e3 : PROMOTION_GROUP_GOODS_LIST_2){

                                        Integer B_PR_GROUP_IDX          = e3.getPr_group_idx();

                                        Integer B_PR_GROUP_GOODS_IDX    = e3.getGoods_idx();

                                        if((A_PR_GROUP_IDX.equals(B_PR_GROUP_IDX)) && (A_PR_GROUP_GOODS_IDX.equals(B_PR_GROUP_GOODS_IDX))){
                                            record3 = getRecord(e3);
                                            isFlag = false;
                                        }

                                        B_PR_GROUP_IDX  = null;

                                        B_PR_GROUP_GOODS_IDX = null;

                                    } 


                                    // 만약 굿즈 이미지가 없을 경우 상품 이미지를 가지고 옴
                                    if(isFlag)record3 = getRecord(e2);

                                    PROMOTION_DATA_PRGROUP_GOODS.add(record3);
                                    record3 = null;

                                }

                                A_PR_GROUP_GOODS_IDX    =   null;
                                
                                A_PR_GROUP_IDX          =   null;

                            }

                            record2.setGoods(PROMOTION_DATA_PRGROUP_GOODS);
                            PROMOTION_DATA_PRGROUP_LIST.add(record2);
                            PROMOTION_DATA_PRGROUP_GOODS = null;

                            record2 = null;
                        }

                        tmp = null;

                    }

                }

                record.setGroup(PROMOTION_DATA_PRGROUP_LIST);

                promotion_data.add(record);

                PROMOTION_DATA_PRGROUP_LIST = null;

                PROMOTION_IDX = null;

            }
        }

        PROMOTION_GROUP_LIST = null;

        PROMOTION_GROUP_GOODS_LIST = null;

        return promotion_data;

    }

    public vo_promotion_data_prgroup_goods getRecord(vo_promotion_group_goods e2){
        vo_promotion_data_prgroup_goods record3 = new vo_promotion_data_prgroup_goods();

        record3.setDiscount_rate(e2.getDiscount_rate());
        record3.setGoods_idx(e2.getGoods_idx());
        record3.setGoods_name(e2.getGoods_name());
        record3.setGoods_price(e2.getGoods_price());
        record3.setImage(e2.getImage());
        record3.setPriority(e2.getPriority());
        record3.setVisible(e2.getVisible());
        record3.setAverage_rating(e2.getAverage_rating());
        record3.setReview_cnt(e2.getReview_cnt());
        record3.setDisplay_type(e2.getDisplay_type());

        return record3;
    }
    
}
