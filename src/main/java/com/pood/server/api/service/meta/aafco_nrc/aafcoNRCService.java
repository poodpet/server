package com.pood.server.api.service.meta.aafco_nrc;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_aafco_nrc;

public interface aafcoNRCService {
  

    /************* AAFCO-NRC 항목 리스트를 조회합니다. ********************/
    public List<dto_aafco_nrc> getAafcoNRCList(pagingSet PAGING_SET) throws Exception;



    /************* AAFCO-NRC 항목 전체 개수를 조회합니다. ********************/
    public Integer getTotalRecordNumber() throws SQLException;
}
