package com.pood.server.api.service.meta.goods.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.meta.coupon.*;
import com.pood.server.api.service.meta.goods.*;
import com.pood.server.api.service.meta.feed.*;
import com.pood.server.api.service.meta.image.*;
import com.pood.server.api.service.meta.pet_doctor_desc.*;
import com.pood.server.api.service.meta.order.*;
import com.pood.server.api.service.meta.product.*;
import com.pood.server.api.service.query.queryService;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.user.*;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.META_GOODS;
import com.pood.server.dto.dto_image_2;
import com.pood.server.dto.meta.goods.dto_goods_10;
import com.pood.server.dto.meta.goods.dto_goods_12;
import com.pood.server.dto.meta.goods.dto_goods_15;
import com.pood.server.dto.meta.goods.dto_goods_3;
import com.pood.server.dto.meta.goods.dto_goods_5;
import com.pood.server.dto.meta.goods.dto_goods_7;
import com.pood.server.dto.meta.goods.dto_goods_8;
import com.pood.server.dto.meta.goods.dto_goods_9;
import com.pood.server.dto.meta.goods.dto_goods_image;
import com.pood.server.dto.meta.product.dto_product_1;
import com.pood.server.dto.meta.product.dto_product_5;
import com.pood.server.object.image_list;
import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_goods_3;
import com.pood.server.object.meta.vo_goods_pr_code;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("goodsService")
public class goodsServiceImpl implements goodsService {

    Logger logger = LoggerFactory.getLogger(goodsServiceImpl.class);

    @Autowired
    @Qualifier("productService")
    public productService productService;

    @Autowired
    @Qualifier("feedService")
    feedService feedService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("imageService")
    public imageService imageService;

    @Autowired
    @Qualifier("couponService")
    public couponService couponService;

    @Autowired
    @Qualifier("orderBasketService")
    public orderBasketService orderBasketService;

    @Autowired
    @Qualifier("userReviewService")
    public userReviewService userReviewService;

    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;

    @Autowired
    @Qualifier("goodsCouponService")
    goodsCouponService goodsCouponService;

    @Autowired
    @Qualifier("goodsProductService")
    goodsProductService goodsProductService;

    @Autowired
    @Qualifier("PETDoctorDescService")
    PETDoctorDescService PETDoctorDescService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;
 
    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_GOODS;

    private String DEFINE_IMAGE_TABLE_NAME = DATABASE.TABLE_GOODS_IMAGE;
    
    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

 
    /************ 굿즈 항목 번호에 해당하는 굿즈 오브젝트 조회 *************/
    public vo_goods_3  GET_GOODS_OBJECT_1(Integer GOODS_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        SELECT_QUERY.set(DEFINE_DB_NAME,        DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx",          GOODS_IDX);
        return (vo_goods_3)listService.getDTOObject(SELECT_QUERY.toString(), vo_goods_3.class); 
    }
  




    
    /************ 굿즈 항목 번호에 해당하는 굿즈 오브젝트 조회 *************/
    public dto_goods_3 GET_GOODS_OBJECT_2(Integer GOODS_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", GOODS_IDX);

        dto_goods_3 record = (dto_goods_3)listService.getDTOObject(SELECT_QUERY.toString(), dto_goods_3.class); 

        List<dto_image_2> GOODS_IMAGE   = getGoodsImageList2(GOODS_IDX);

        List<dto_image_2> PRODUCT_IMAGE = new ArrayList<dto_image_2>();

        List<Integer> PRODUCT_IDX_LIST = goodsProductService.getgoodsProductList(GOODS_IDX);
        if(record != null){
            record.setProduct(PRODUCT_IDX_LIST);
            
            if(PRODUCT_IDX_LIST != null){
                for(Integer PRODUCT_IDX : PRODUCT_IDX_LIST){
                    List<dto_image_2> tmp = productService.getProductImageList(PRODUCT_IDX);
                    PRODUCT_IMAGE.addAll(tmp);
                }
            }
            PRODUCT_IMAGE = PRODUCT_IMAGE.stream().filter(x->x.getType().equals(0) && x.getPriority().equals(0)).collect(Collectors.toList());
            record.setGoods_image(GOODS_IMAGE);

            record.setProduct_image(PRODUCT_IMAGE);
        }
            
        GOODS_IMAGE     =   null;

        PRODUCT_IMAGE   =   null;

        return record;
    }








    /************ 굿즈 항목 번호에 해당하는 굿즈 오브젝트 조회 *************/
    public dto_goods_10 GET_GOODS_OBJECT_3(Integer GOODS_IDX) throws Exception {
 
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", GOODS_IDX);
        
        dto_goods_10 record = (dto_goods_10)listService.getDTOObject(SELECT_QUERY.toString(), dto_goods_10.class); 

        // 굿즈에 대한 추가 매핑 정보 조회
        List<vo_goods_pr_code> GOODS_PR_LIST = new ArrayList<vo_goods_pr_code>();

        List<dto_product_5> GOODS_PRODUCT_LIST = goodsProductService.getgoodsProductList3(GOODS_IDX);

        if(record != null){
            record.setCoupon_list(goodsCouponService.getgoodsCouponList(GOODS_IDX));
            record.setPr_code(GOODS_PR_LIST);
            record.setProduct(GOODS_PRODUCT_LIST);

            List<dto_image_2> GOODS_IMAGE   = getGoodsImageList2(GOODS_IDX);

            List<dto_image_2> PRODUCT_IMAGE = new ArrayList<dto_image_2>();

            if(GOODS_PRODUCT_LIST != null){
                for(dto_product_5 e : GOODS_PRODUCT_LIST){
                    Integer PRODUCT_IDX = e.getIdx();
                    List<dto_image_2> tmp = productService.getProductImageList(PRODUCT_IDX);
                    PRODUCT_IMAGE.addAll(tmp);
                }
            }
            record.setGoods_image(GOODS_IMAGE);

            record.setProduct_image(PRODUCT_IMAGE);
            
            GOODS_IMAGE     =   null;

            PRODUCT_IMAGE   =   null;
        }
        
        return record;
    }

    public image_list getGoodsImageList(Integer GOODS_IDX) throws Exception{
        return imageService.getImageList(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME, GOODS_IDX, "goods_idx");
    }

    /************ 굿즈 항목 번호에 해당하는 굿즈 오브젝트 조회 *************/
    public dto_goods_9 GET_GOODS_OBJECT_4(Integer GOODS_IDX) throws Exception {
 
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", GOODS_IDX);

        dto_goods_9 record = (dto_goods_9)listService.getDTOObject(SELECT_QUERY.toString(), dto_goods_9.class); 
 

        // 굿즈에 대한 추가 매핑 정보 조회
        List<vo_goods_pr_code> GOODS_PR_LIST = new ArrayList<vo_goods_pr_code>();
        if(record != null){
            record.setCoupon_list(goodsCouponService.getgoodsCouponList(GOODS_IDX));
            record.setPr_code(GOODS_PR_LIST);

            List<dto_image_2> GOODS_IMAGE   = getGoodsImageList2(GOODS_IDX);

            List<Integer> PRODUCT_IDX_LIST = goodsProductService.getProductIDXList(GOODS_IDX);

            List<dto_image_2> PRODUCT_IMAGE = new ArrayList<dto_image_2>();

            if(PRODUCT_IDX_LIST != null){
                for(Integer e2 : PRODUCT_IDX_LIST){
                    List<dto_image_2> tmp = productService.getProductImageList(e2);
                    PRODUCT_IMAGE.addAll(tmp);
                }
             }

            record.setGoods_image(GOODS_IMAGE);

            record.setProduct_image(PRODUCT_IMAGE);
            
            GOODS_IMAGE     =   null;

            PRODUCT_IMAGE   =   null;

        }

        
        return record;
    }

     /************ 굿즈 항목 번호에 해당하는 굿즈 오브젝트 조회 *************/
     public dto_goods_15 GET_GOODS_OBJECT_10(Integer GOODS_IDX) throws Exception {
 
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", GOODS_IDX);

        dto_goods_15 record = (dto_goods_15)listService.getDTOObject(SELECT_QUERY.toString(), dto_goods_15.class); 
 

        // 굿즈에 대한 추가 매핑 정보 조회
        List<vo_goods_pr_code> GOODS_PR_LIST = new ArrayList<vo_goods_pr_code>();
        if(record != null){

            Integer MAIN_PRODUCT = record.getMain_product();

            Integer brand_idx    = productService.getBrandIDX(MAIN_PRODUCT);

            record.setBrand_idx(brand_idx);

            record.setCoupon_list(goodsCouponService.getgoodsCouponList(GOODS_IDX));
            record.setPr_code(GOODS_PR_LIST);

            List<dto_image_2> GOODS_IMAGE   = getGoodsImageList2(GOODS_IDX);

            List<Integer> PRODUCT_IDX_LIST = goodsProductService.getProductIDXList(GOODS_IDX);

            List<dto_image_2> PRODUCT_IMAGE = new ArrayList<dto_image_2>();

            if(PRODUCT_IDX_LIST != null){
                for(Integer e2 : PRODUCT_IDX_LIST){
                    List<dto_image_2> tmp = productService.getProductImageList(e2);
                    PRODUCT_IMAGE.addAll(tmp);
                }
             }

            record.setGoods_image(GOODS_IMAGE);

            record.setProduct_image(PRODUCT_IMAGE);
            
            GOODS_IMAGE     =   null;

            PRODUCT_IMAGE   =   null;

        }

        
        return record;
    }

    /************* 굿즈 이미지 중에 메인 이미지만 가지고 옴 *******************/
    @SuppressWarnings("unchecked")
    public List<dto_image_2> getMainImageList(Integer GOODS_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME);
        SELECT_QUERY.buildEqual("goods_idx", GOODS_IDX);
        List<dto_image_2> list = (List<dto_image_2>)listService.getDTOImageList2(SELECT_QUERY.toString());
        if(list != null){
            Iterator<dto_image_2> iterator = list.iterator();

            while(iterator.hasNext()) {

                dto_image_2 record = iterator.next();
                if(record.getType() != 0){
                    iterator.remove();
                }

            }    
        }    

        return list;
    }

    /************ 굿즈 항목 번호에 해당하는 굿즈 오브젝트 조회 *************/
    public dto_goods_8 GET_GOODS_OBJECT_5(Integer GOODS_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", GOODS_IDX);
        dto_goods_8 record = (dto_goods_8)listService.getDTOObject(SELECT_QUERY.toString(), dto_goods_8.class);
        if(record != null){
            List<dto_image_2> GOODS_IMAGE   = getGoodsImageList2(GOODS_IDX);

            List<Integer> PRODUCT_IDX_LIST = goodsProductService.getProductIDXList(GOODS_IDX);

            List<dto_image_2> PRODUCT_IMAGE = new ArrayList<dto_image_2>();

            List<dto_product_1> GOODS_PRODUCT_LIST = goodsProductService.getgoodsProductList7(GOODS_IDX);

            record.setProduct(GOODS_PRODUCT_LIST);

            if(PRODUCT_IDX_LIST != null){
                for(Integer e2 : PRODUCT_IDX_LIST){
                    List<dto_image_2> tmp = productService.getProductImageList(e2);
                    PRODUCT_IMAGE.addAll(tmp);
                }
             }

            record.setGoods_image(GOODS_IMAGE);

            record.setProduct_image(PRODUCT_IMAGE);
            
            GOODS_IMAGE     =   null;

            PRODUCT_IMAGE   =   null;

            GOODS_PRODUCT_LIST = null;
        }

        return record;
    }

    /**************** 전체 목록 개수 조회 ******************/
    public Integer getTotalRecordNumber(Integer GOODS_IDX) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if (GOODS_IDX != null)
            CNT_QUERY.buildEqual("idx", GOODS_IDX);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    /**************** 전체 목록 개수 조회 ******************/
    public Integer getTotalRecordNumber(Integer GOODS_IDX, Integer GOODS_TYPE_IDX, String KEYWORD) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if (GOODS_IDX != null)
            CNT_QUERY.buildEqual("idx", GOODS_IDX);
        if (GOODS_TYPE_IDX != null)
            CNT_QUERY.buildEqual("goods_type_idx", GOODS_TYPE_IDX);
        if (KEYWORD != null)
            CNT_QUERY.buildLike("goods_name", KEYWORD);

        return listService.getRecordCount(CNT_QUERY.toString());
    }
   

    
    @SuppressWarnings("unchecked")
    public List<dto_goods_5> GET_GOODS_LIST_2(pagingSet PAGING_SET, List<Integer> GOODS_IDX_LIST, Integer PC_IDX) throws Exception {
        
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("pc_idx", PC_IDX);
        SELECT_QUERY.buildAndWhereIn_Integer("idx", GOODS_IDX_LIST);
        SELECT_QUERY.setPagination(PAGING_SET, false);
 
        List<dto_goods_5> result = (List<dto_goods_5>)listService.getDTOList(SELECT_QUERY.toString(), dto_goods_5.class);
                
        if(result != null){
            for(dto_goods_5 e : result){
                Integer GOODS_IDX = e.getIdx();
                List<vo_goods_pr_code> GOODDS_PR_LIST = new ArrayList<vo_goods_pr_code>();

                List<dto_product_5> GOODS_PRODUCT_LIST = goodsProductService.getgoodsProductList3(GOODS_IDX);
                e.setCoupon(goodsCouponService.getgoodsCouponList(GOODS_IDX));
                e.setPr_code(GOODDS_PR_LIST);
                e.setProduct(GOODS_PRODUCT_LIST);
        
                List<dto_image_2> GOODS_IMAGE   = getGoodsImageList2(GOODS_IDX);

                List<dto_image_2> PRODUCT_IMAGE = new ArrayList<dto_image_2>();

                if(GOODS_PRODUCT_LIST != null){
                    for(dto_product_5 e1 : GOODS_PRODUCT_LIST){
                        Integer PRODUCT_IDX = e1.getIdx();
                        List<dto_image_2> tmp = productService.getProductImageList(PRODUCT_IDX);
                        PRODUCT_IMAGE.addAll(tmp);
                    }
                }
    
                e.setGoods_image(GOODS_IMAGE);
    
                e.setProduct_image(PRODUCT_IMAGE);
                
                GOODS_IMAGE     =   null;
    
                PRODUCT_IMAGE   =   null;
            }
        }  
        return result;
    }

    @SuppressWarnings("unchecked")
    public String getgoodsName(Integer GOODS_IDX) throws Exception{
        String GOODS_NAME = "";
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", GOODS_IDX);

        List<vo_goods_3> list = (List<vo_goods_3>)listService.getDTOList(SELECT_QUERY.toString(), vo_goods_3.class);

        if(list != null){
            for(vo_goods_3 e : list)
                GOODS_NAME = (String)e.getGoods_name();
        }

        return GOODS_NAME;
    }

    @SuppressWarnings("unchecked")
    public List<dto_goods_7> GET_GOODS_LIST(List<Integer> GOODS_IDX_LIST) throws Exception {
    
        List<dto_goods_7> result =  new ArrayList<dto_goods_7>();

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if(!GOODS_IDX_LIST.isEmpty()){
            SELECT_QUERY.buildWhereIn_Integer("idx", GOODS_IDX_LIST);
        }else return result;

        SELECT_QUERY.buildReverseOrder("idx");

        result = (List<dto_goods_7>)listService.getDTOList(SELECT_QUERY.toString(), dto_goods_7.class);
 
        Iterator<dto_goods_7> iterator = result.iterator();

        while(iterator.hasNext()){

            dto_goods_7 e = iterator.next();

            Integer GOODS_IDX = e.getIdx();

            // 굿즈에 대한 추가 매핑 정보 조회
            List<vo_goods_pr_code> GOODS_PR_LIST = new ArrayList<vo_goods_pr_code>();

            List<dto_product_5> GOODS_PRODUCT_LIST = goodsProductService.getgoodsProductList3(GOODS_IDX);

            e.setCoupon_list(goodsCouponService.getgoodsCouponList(GOODS_IDX));
            e.setPr_code(GOODS_PR_LIST);
            e.setProduct(GOODS_PRODUCT_LIST);


            List<dto_image_2> GOODS_IMAGE   = getGoodsImageList2(GOODS_IDX);

            List<dto_image_2> PRODUCT_IMAGE = new ArrayList<dto_image_2>();

            if(GOODS_PRODUCT_LIST != null){
                for(dto_product_5 e1 : GOODS_PRODUCT_LIST){
                    Integer PRODUCT_IDX = e1.getIdx();
                    List<dto_image_2> tmp = productService.getProductImageList(PRODUCT_IDX);
                    PRODUCT_IMAGE.addAll(tmp);
                }
            }
        
            e.setGoods_image(GOODS_IMAGE);
        
            e.setProduct_image(PRODUCT_IMAGE);
                
            GOODS_PRODUCT_LIST = null;
                    
            GOODS_IMAGE     =   null;
        
            PRODUCT_IMAGE   =   null;
    
        }  

        return result;
    }

    @SuppressWarnings("unchecked")
    public List<dto_goods_7> GET_GOODS_LIST_BY_SORT_TYPE(List<Integer> GOODS_IDX_LIST, String sort_type , Boolean is_ascending) throws Exception {
        logger.info("!!!!!sort_type : " + sort_type);
        List<dto_goods_7> result =  new ArrayList<dto_goods_7>();

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if(!GOODS_IDX_LIST.isEmpty()){
            SELECT_QUERY.buildWhereIn_Integer("idx", GOODS_IDX_LIST);
        }else return result;

        if(is_ascending) {
            SELECT_QUERY.buildASC(sort_type);
        } else {
            SELECT_QUERY.buildDESC(sort_type);
        }
        

        logger.info("GET_GOODS_LIST : " + SELECT_QUERY.toString());

        result = (List<dto_goods_7>)listService.getDTOList(SELECT_QUERY.toString(), dto_goods_7.class);
 
        Iterator<dto_goods_7> iterator = result.iterator();

        while(iterator.hasNext()){

            dto_goods_7 e = iterator.next();

            Integer GOODS_IDX = e.getIdx();

            // 굿즈에 대한 추가 매핑 정보 조회
            List<vo_goods_pr_code> GOODS_PR_LIST = new ArrayList<vo_goods_pr_code>();

            List<dto_product_5> GOODS_PRODUCT_LIST = goodsProductService.getgoodsProductList3(GOODS_IDX);

            e.setCoupon_list(goodsCouponService.getgoodsCouponList(GOODS_IDX));
            e.setPr_code(GOODS_PR_LIST);
            e.setProduct(GOODS_PRODUCT_LIST);


            List<dto_image_2> GOODS_IMAGE   = getGoodsImageList2(GOODS_IDX);

            List<dto_image_2> PRODUCT_IMAGE = new ArrayList<dto_image_2>();

            if(GOODS_PRODUCT_LIST != null){
                for(dto_product_5 e1 : GOODS_PRODUCT_LIST){
                    Integer PRODUCT_IDX = e1.getIdx();
                    List<dto_image_2> tmp = productService.getProductImageList(PRODUCT_IDX);
                    PRODUCT_IMAGE.addAll(tmp);
                }
            }
        
            e.setGoods_image(GOODS_IMAGE);
        
            e.setProduct_image(PRODUCT_IMAGE);
                
            GOODS_PRODUCT_LIST = null;
                    
            GOODS_IMAGE     =   null;
        
            PRODUCT_IMAGE   =   null;
    
        }  

        return result;
    }

    @Override
    public void updateGoodsSaleStatus(Integer e, Integer sale_status) throws SQLException {
        updateService.execute(GET_QUERY_UPDATE_GOODS_SALE_STATUS(e,  sale_status));
    }


    @Override
    public dto_image_2 getImage(Integer goods_image_idx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", goods_image_idx);
        dto_image_2 record = (dto_image_2)listService.getDTOObject(SELECT_QUERY.toString(), dto_image_2.class);
        return record;
    }

    @SuppressWarnings("unchecked")
    public List<dto_goods_12> getList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return (List<dto_goods_12>)listService.getDTOList(SELECT_QUERY.toString(), dto_goods_12.class);
    }


    @SuppressWarnings("unchecked")
    public List<dto_goods_image> getImageList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return (List<dto_goods_image>)listService.getDTOList(SELECT_QUERY.toString(), dto_goods_image.class);
    }
 

    @SuppressWarnings("unchecked")
    public List<dto_image_2> getGoodsImageList2(Integer GOODS_IDX) throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME);
        SELECT_QUERY.buildEqual("goods_idx", GOODS_IDX);
        return (List<dto_image_2>)listService.getDTOImageList2(SELECT_QUERY.toString());
    }


    @SuppressWarnings("unchecked")
    public List<dto_image_2> getGoodsMainImageList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME);
        SELECT_QUERY.buildEqual("type", META_GOODS.GOODS_IMAGE_TYPE_MAIN);
        return (List<dto_image_2>)listService.getDTOImageList2(SELECT_QUERY.toString());
    }






    @Override
    public void updateGoodsRating(Integer review_cnt, Double average_rating, Integer goods_idx) throws SQLException {
        
        logger.info("REVIEW CNT : " + review_cnt + ", AVERAGE RATING : " + average_rating + ", GOODS IDX : " + goods_idx);
        
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("average_rating",   average_rating);
        hashMap.put("review_cnt",       review_cnt);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", goods_idx.toString());
        updateService.execute(query);
        query = null;
        
    }






    @Override
    public String GET_QUERY_UPDATE_GOODS_SALE_STATUS(Integer goods_idx, Integer sale_status) {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("sale_status", sale_status);
        return queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", goods_idx.toString());
    }

    public Boolean isOnSale(Integer goods_idx) throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY( DEFINE_DB_NAME,        DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx",          goods_idx);
        vo_goods_3 record = (vo_goods_3)listService.getDTOObject(SELECT_QUERY.toString(), vo_goods_3.class); 
        if(record != null){

            Integer SLAE_STATUS = record.getSale_status();

            if(SLAE_STATUS == META_GOODS.SALE_STATUS_ON)return true;
            else return false;

        }else return false;
    }






    @Override
    public String getDisplayType(Integer GOODS_IDX) throws Exception {
        String DISPLAY_TYPE = null;
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx",          GOODS_IDX);        
        vo_goods_3 record = (vo_goods_3)listService.getDTOObject(SELECT_QUERY.toString(), vo_goods_3.class); 
        if(record != null)
            DISPLAY_TYPE = record.getDisplay_type();
        return DISPLAY_TYPE;
    }
}
