package com.pood.server.api.service.meta.category;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.api.service.meta.category_image.*;

import com.pood.server.api.service.record.*;
import com.pood.server.api.service.list.*;


import com.pood.server.config.DATABASE;
import com.pood.server.object.meta.vo_category;

@Service("categoryService")
public class categoryServiceImpl implements categoryService{
    
    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("categoryImageService")
    categoryImageService categoryImageService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_CATEGORY;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @Override
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(
            DEFINE_DB_NAME, 
            DEFINE_TABLE_NAME); 
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @SuppressWarnings("unchecked")
    public List<vo_category> getList(Integer pc_id) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if(pc_id != null)
            SELECT_QUERY.buildEqual("pc_id", pc_id);
        List<vo_category> list = (List<vo_category>)listService.getDTOList(SELECT_QUERY.toString(), vo_category.class);        
        return list;
    }

}
