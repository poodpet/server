package com.pood.server.api.service.log;

import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.meta.product.productService;
import com.pood.server.api.service.query.queryService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.config.DATABASE;
import com.pood.server.object.meta.vo_product;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("logProductHistoryService")
public class logProductHistoryServiceImpl implements logProductHistoryService{

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("productService")
    productService productService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_LOG_PRODUCT_HISTORY;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @Override
    public Integer insertRecord(String uuid, Integer qty, Integer type, String text)
        throws Exception {
        return updateService.insert(GET_QUERY_INSERT_RECORD(uuid, qty, type, text));
    }

    @Override
    public String GET_QUERY_INSERT_RECORD(String uuid, Integer qty, Integer type, String text) throws Exception {

        vo_product record = productService.getProduct(uuid);

        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("product_uuid", uuid);
        hashMap.put("type", type);
        hashMap.put("quantity", qty);
        hashMap.put("expired_date", null);
        hashMap.put("text", text);
        hashMap.put("count", record.getQuantity());
        hashMap.put("available_count", record.getAva_quantity());

        record = null;

        return queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);
    }
 
}
