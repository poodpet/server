package com.pood.server.api.service.meta.nuti.impl;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
 
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.meta.nuti.nutiService;
import com.pood.server.api.service.record.*;
import com.pood.server.config.DATABASE;
import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_nuti;
import com.pood.server.dto.meta.dto_nuti;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("nutiService")
public class nutiServiceImpl implements nutiService {

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_NUTI;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
    

    /**************** 영양소 목록 조회 *****************/
    @SuppressWarnings("unchecked")
    public List<dto_nuti> getNutiList(pagingSet PAGING_SET) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_nuti>)listService.getDTOList(SELECT_QUERY.toString(), dto_nuti.class);
    }

    /**************** 전체 목록 조회 ******************/
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    } 
 
    @SuppressWarnings("unchecked")
    public List<vo_nuti> getObjectList() throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return (List<vo_nuti>)listService.getDTOList(SELECT_QUERY.toString(), vo_nuti.class);
    }
}
