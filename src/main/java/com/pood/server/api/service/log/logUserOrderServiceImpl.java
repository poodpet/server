package com.pood.server.api.service.log;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.order.ORDER_STATUS;
import com.pood.server.dto.log.user_order.dto_log_user_order;
import com.pood.server.dto.log.user_order.dto_log_user_order_2;
import com.pood.server.object.log.vo_log_user_order;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.query.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("logUserOrderService")
public class logUserOrderServiceImpl implements logUserOrderService {

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_LOG_USER_ORDER;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    /********** 주문 기록 생성 : log_order_history ***********/
    public Integer insertOrderHistory(
        Integer ORDER_IDX, 
        Integer STATUS, 
        String  MESSAGE, 
        String  ORDER_NUMBER, 
        String  USER_UUID,
        Integer GOODS_IDX,
        Integer QTY) throws SQLException {
        return updateService.insert(GET_QUERY_INSERT_ORDER_HISTORY(ORDER_IDX, STATUS,  MESSAGE, ORDER_NUMBER, USER_UUID, GOODS_IDX, QTY));
    }


    public String GET_QUERY_INSERT_ORDER_HISTORY(Integer ORDER_IDX, Integer STATUS, String  MESSAGE, String  ORDER_NUMBER, String  USER_UUID,Integer GOODS_IDX, Integer QTY){
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("order_idx",    ORDER_IDX);
        hashMap.put("order_status", STATUS);
        hashMap.put("order_text",   MESSAGE);
        hashMap.put("order_number", ORDER_NUMBER);
        hashMap.put("user_uuid",    USER_UUID);   
        hashMap.put("goods_idx",    GOODS_IDX);
        hashMap.put("qty",          QTY);     
        return queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);
    }




    /******************* 특정 주문 번호에 대한 주문 기록 조회 *********************/
    public List<dto_log_user_order_2> getOrderHistoryList(Integer ORDER_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_idx", ORDER_IDX);
        return (List<dto_log_user_order_2>)listService.getDTOListLogUserOrder(SELECT_QUERY.toString());
    }


    /******************* 특정 주문 번호에 대한 주문 기록 조회2 *********************/
    @SuppressWarnings("unchecked")
    public List<dto_log_user_order> getOrderHistoryList2(String ORDER_NUMBER) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY (DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number",    ORDER_NUMBER);
        return (List<dto_log_user_order>)listService.getDTOList(SELECT_QUERY.toString(), dto_log_user_order.class);
    }

    public Integer getPayCount(String user_uuid) throws SQLException{
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        CNT_QUERY.buildEqual("user_uuid", user_uuid);
        CNT_QUERY.buildEqual("order_status", ORDER_STATUS.ORDER_SUCCESS);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    
    public Integer getPurchasedCnt(String user_uuid) throws SQLException{
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        CNT_QUERY.buildEqual("user_uuid", user_uuid);
        CNT_QUERY.buildEqual("order_status", ORDER_STATUS.DELIVERY_SUCCESS);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    public dto_log_user_order getGoodsBought(Integer GOODS_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("goods_idx",        GOODS_IDX);
        SELECT_QUERY.buildEqual("order_status",     ORDER_STATUS.DELIVERY_SUCCESS);
        dto_log_user_order record = (dto_log_user_order)listService.getDTOObject(SELECT_QUERY.toString(), dto_log_user_order.class);
        return record;
    }

    @Override
    public Boolean isUserOrdered(String USER_UUID) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid",        USER_UUID);
        SELECT_QUERY.buildEqual("order_status",     ORDER_STATUS.DELIVERY_SUCCESS);        
        dto_log_user_order record = (dto_log_user_order)listService.getDTOObject(SELECT_QUERY.toString(), dto_log_user_order.class);
        if(record == null)return false;
        return true;
    }

    @Override
    public Boolean getPaySuccessCnt(String USER_UUID) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid",        USER_UUID);
        SELECT_QUERY.buildEqual("order_status",     ORDER_STATUS.ORDER_SUCCESS);        
        vo_log_user_order record = (vo_log_user_order)listService.getDTOObject(SELECT_QUERY.toString(), vo_log_user_order.class);
        if(record == null)return false;
        return true;
    }


 
}
