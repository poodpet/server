package com.pood.server.api.service.holiday_check;

import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public interface holidayCheckService {

    List<String> getHolidayData() throws Exception;

    void callAPI(int year, List<String> holidayMap) throws Exception;

    void parseXML(String xml, List<String> holidayMap) throws ParserConfigurationException, IOException, SAXException, ParseException;

    void addWeekendDate(Calendar calendar, List<String> holidayMap);

    boolean checkHoliday(int dayNum);

}
