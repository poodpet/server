package com.pood.server.api.service.log;

import java.sql.SQLException;

public interface logUserTokenService {

    public void insertRecord(String USER_UUID, String TOKEN) throws SQLException;
    
}
