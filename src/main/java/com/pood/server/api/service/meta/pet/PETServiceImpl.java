package com.pood.server.api.service.meta.pet;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;

import com.pood.server.config.DATABASE;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.dto_image_1;
import com.pood.server.dto.dto_image_4;
import com.pood.server.dto.meta.pet.dto_pet;
import com.pood.server.dto.meta.pet.dto_pet_2;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.record.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("PETService")
public class PETServiceImpl implements PETService {

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_PET;

    private String DEFINE_IMAGE_TABLE_NAME = DATABASE.TABLE_PET_IMAGE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    /**************** 펫 목록 조회 *****************/
    @SuppressWarnings("unchecked")
    public List<dto_pet> getPetList(pagingSet PAGING_SET, Integer pc_id, String keyword) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if(pc_id != null)
            SELECT_QUERY.buildEqual("pc_id", pc_id);
        if(keyword != null){
            SELECT_QUERY.buildLike("pc_kind_en", keyword, "pc_tag", keyword);   
        }
    

        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_pet>)listService.getDTOListWithImage1(
                SELECT_QUERY.toString(), "pet_idx", DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME, dto_pet.class, dto_image_1.class);
    }

    /**************** 전체 목록 개수 조회 ******************/
    public Integer getTotalRecordNumber(Integer pc_id, String keyword) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    }
 

 
    @SuppressWarnings("unchecked")
    public dto_pet_2 getPetRecord(Integer PET_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", PET_IDX);

        dto_pet_2 record = (dto_pet_2)listService.getDTOObject(SELECT_QUERY.toString(), dto_pet_2.class);
        if(record != null){
            SELECT_QUERY SELECT_QUERY2 = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME);
            SELECT_QUERY2.buildEqual("pet_idx", record.getIdx());
            List<dto_image_4> image_list = (List<dto_image_4>)listService.getDTOImageList4(SELECT_QUERY2.toString());
            record.setImage(image_list);
            SELECT_QUERY2 = null;
            image_list = null;
        }
     
        return record;
 
    }


    
}
