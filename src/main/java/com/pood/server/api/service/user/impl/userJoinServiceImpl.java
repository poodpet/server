package com.pood.server.api.service.user.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.user.userJoinService;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.log.dto_log_user_join;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.query.*;

@Service("userJoinService")
public class userJoinServiceImpl implements userJoinService {

    Logger logger = LoggerFactory.getLogger(userJoinServiceImpl.class);

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;
    
    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_LOG_USER_JOIN;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
    
    // 회원 가입 로그 삽입 
    public Integer insertRecord(Integer LOGIN_TYPE, Integer USER_IDX, String USER_UUID, String SNS_KEY) throws SQLException {
        if(SNS_KEY == null)SNS_KEY = "";
        
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_idx",     USER_IDX);
        hashMap.put("login_type",   LOGIN_TYPE);
        hashMap.put("sns_key",      SNS_KEY);
        hashMap.put("user_uuid",    USER_UUID);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }

    // 특정 SNS 키에 해당하는 회원 조회 
    public dto_log_user_join getUserJoinInfo(String SNS_KEY) throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("sns_key", SNS_KEY);       
        return (dto_log_user_join)listService.getDTOObject(SELECT_QUERY.toString(), dto_log_user_join.class);
        
    }

    @Override
    public void deleteRecord(String USER_UUID) throws Exception {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_uuid", USER_UUID);
        updateService.execute(DELETE_QUERY.toString());
    }

    public void updateSnsKey(String userUuid, String snsKey) throws Exception {
//        Map<String, Object> hashMap = new HashMap<String, Object>();

//        hashMap.put("sns_key", snsKey);

//        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "user_uuid",userUuid);

        String query = "UPDATE log_db.log_user_join SET sns_key='" + snsKey+"' WHERE user_uuid='" +userUuid +"'";

        updateService.execute(query);
        logger.info("user uuid = " + userUuid + ", sns key =" + snsKey);
        query = null;
    }

}
