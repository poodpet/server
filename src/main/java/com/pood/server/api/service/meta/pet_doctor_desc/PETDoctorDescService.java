package com.pood.server.api.service.meta.pet_doctor_desc;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import com.pood.server.api.queryBuilder.buildQuery;
import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_pet_doctor_desc;
import com.pood.server.dto.meta.goods.dto_goods_5;
import com.pood.server.dto.meta.pet.dto_pet_doctor_desc;
import com.pood.server.dto.meta.product.dto_product_5;

public interface PETDoctorDescService {
 


    /**************** 수의사 사료 설명 목록 조회 *******/
    public List<dto_pet_doctor_desc> getPetDoctorDescList(pagingSet PAGING_SET, Integer product_idx) throws Exception;



    /**************** 전체 목록 개수 조회 ******************/ 
    public Integer getTotalRecordNumber(Integer product_idx) throws SQLException;



	public dto_pet_doctor_desc getRecord(Integer product_idx) throws Exception;

 
    
    public List<dto_pet_doctor_desc> getList() throws Exception;
 

    public List<dto_product_5> getListPositionOrder(Integer page_number, Integer page_size, buildQuery query) throws Exception;


    
    /**************** 전체 목록 개수 조회 ******************/ 
    public Integer getTotalRecordNumber(String query) throws SQLException;



    public HashMap<Integer, vo_pet_doctor_desc> getObjectList() throws Exception;



    public List<dto_goods_5> getList(pagingSet PAGING_SET, String query, Integer pc_idx) throws Exception;



    public String getListQuery(String position, Integer ct_idx, Integer ct_sub_idx) throws SQLException ;

    
}
