package com.pood.server.api.service.log;

import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.point.POINT_SAVE_STATUS;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.log.dto_log_user_save_point;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.user.*;
import com.pood.server.api.service.time.*;
import com.pood.server.api.service.meta.point.*;
import com.pood.server.api.service.meta.order.OrderService;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.query.*;

@Service("logUserSavePointService")
public class logUserSavePointServiceImpl implements logUserSavePointService{
    
    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("timeService")
    timeService timeService;

    @Autowired
    @Qualifier("pointService")
    pointService pointService;

    @Autowired
    @Qualifier("userPointService")
    userPointService userPointService;

    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    @Autowired
    @Qualifier("logUserPointService")
    logUserPointService logUserPointService;

    @Autowired
    @Qualifier("orderService")
    OrderService orderService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_LOG_USER_SAVE_POINT;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @SuppressWarnings("unchecked")
    public List<dto_log_user_save_point> getList(pagingSet PAGING_SET, String user_uuid) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", user_uuid);
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_log_user_save_point>)listService.getDTOList(SELECT_QUERY.toString(), dto_log_user_save_point.class);
    }

    @Override
    public Integer getTotalRecordNumber(String user_uuid) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        CNT_QUERY.buildEqual("user_uuid", user_uuid);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @SuppressWarnings("unchecked")
    public Integer getTotalGivenPoint(String user_uuid) throws Exception {
        Integer GIVEN_POINT = 0;

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", user_uuid);
        List<dto_log_user_save_point> list = (List<dto_log_user_save_point>)listService.getDTOList(SELECT_QUERY.toString(), dto_log_user_save_point.class);

        if(list != null){
            for(dto_log_user_save_point e : list){
                Integer TYPE        = e.getType();
                Integer SAVE_POINT  = e.getSave_point();
                if(TYPE.equals(POINT_SAVE_STATUS.SAVE_COMPLETE))
                    GIVEN_POINT += SAVE_POINT;
    
                TYPE = null;
                SAVE_POINT = null;
            }
        }

        return GIVEN_POINT;
    }

    @SuppressWarnings("unchecked")
    public Integer getToBeSavedPoint(String user_uuid) throws Exception {
        Integer TO_BE_SAVED_POINT = 0;

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", user_uuid);
        List<dto_log_user_save_point> result = (List<dto_log_user_save_point>)listService.getDTOList(SELECT_QUERY.toString(), dto_log_user_save_point.class);

        if(result != null){
            for(dto_log_user_save_point e : result){

                Integer TYPE        = (Integer)e.getType();
                Integer SAVE_POINT  = (Integer)e.getSave_point();
                if(TYPE.equals(POINT_SAVE_STATUS.TO_BE_SAVED))
                    TO_BE_SAVED_POINT += SAVE_POINT;

                TYPE = null;
                SAVE_POINT = null;

            }
        }

        return TO_BE_SAVED_POINT;
    }

    @SuppressWarnings("unchecked")
    public Integer getToBeRemovedPoint(String user_uuid) throws Exception {
        Integer TO_BE_REMOVED_POINT = 0;

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", user_uuid);
        List<dto_log_user_save_point> result = (List<dto_log_user_save_point>)listService.getDTOList(SELECT_QUERY.toString(), dto_log_user_save_point.class);

        if(result != null){
            for(dto_log_user_save_point e : result){

                Integer SAVE_POINT   = e.getSave_point();

                String  EXPIRED_DATE = e.getExpired_date();

                String  CURRENT_TIME = timeService.getCurrentTime2();

                if(SAVE_POINT > 0){

                    DateTimeFormatter fDTOatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
                    DateTime exp = fDTOatter.parseDateTime(EXPIRED_DATE);
                    DateTime cur = fDTOatter.parseDateTime(CURRENT_TIME);

                    Days diffInDays = Days.daysBetween(cur, exp);
        
                    if(diffInDays.getDays()<30)
                        TO_BE_REMOVED_POINT += SAVE_POINT;

                }

                CURRENT_TIME = null;

                EXPIRED_DATE = null;

                SAVE_POINT = null;

            }
        }

        return TO_BE_REMOVED_POINT;
    }

    @Override
    public void init(String USER_UUID) throws Exception {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_uuid", USER_UUID);
        updateService.execute(DELETE_QUERY.toString());
    }

    public String GET_QUERY_INSERT_RECORD(String USER_UUID, Integer POINT_IDX, String POINT_NAME, Integer POINT_TYPE, Integer POINT_PRICE,
        Integer POINT_RATE, String TEXT, Integer POINT_STATUS, Integer SAVE_POINT, String EXPIRED_DATE){
            Map<String, Object> hashMap = new HashMap<String, Object>();
            hashMap.put("user_uuid",        USER_UUID);
            hashMap.put("point_idx",        POINT_IDX);
            hashMap.put("point_name",       POINT_NAME);
            hashMap.put("point_type",       POINT_TYPE);
            hashMap.put("point_price",      POINT_PRICE);
            hashMap.put("point_rate",       POINT_RATE);
            hashMap.put("text",             TEXT);
            hashMap.put("type",             POINT_STATUS);
            hashMap.put("save_point",       SAVE_POINT);
            hashMap.put("expired_date",     EXPIRED_DATE);
            return queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);   
        }

    @Override
    public Integer insertRecord(String USER_UUID, Integer POINT_IDX, String POINT_NAME, Integer POINT_TYPE, Integer POINT_PRICE,
            Integer POINT_RATE, String TEXT, Integer POINT_STATUS, Integer SAVE_POINT, String EXPIRED_DATE)
            throws SQLException {                
        return updateService.insert(GET_QUERY_INSERT_RECORD(USER_UUID, POINT_IDX, POINT_NAME, POINT_TYPE, POINT_PRICE, POINT_RATE, TEXT, POINT_STATUS, SAVE_POINT, EXPIRED_DATE));
    }
}
