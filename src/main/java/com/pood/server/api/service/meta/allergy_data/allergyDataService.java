package com.pood.server.api.service.meta.allergy_data;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_allergy_data;

public interface allergyDataService {
    
    /**************** 항목 목록 조회 *****************/
    public List<dto_allergy_data> getList(pagingSet PAGING_SET) throws Exception;

    /**************** 전체 목록 개수 조회 ******************/
    public Integer getTotalRecordNumber() throws SQLException;

    public dto_allergy_data getRecord(Integer aLLERGY_IDX) throws Exception;

}
