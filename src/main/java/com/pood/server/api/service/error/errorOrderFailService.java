package com.pood.server.api.service.error;

import java.sql.SQLException;

public interface errorOrderFailService {

    Integer insertRecord(String order_number, String imp_uid, String text, Integer pay_type) throws SQLException;
}
