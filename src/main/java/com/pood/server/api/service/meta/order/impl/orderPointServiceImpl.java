package com.pood.server.api.service.meta.order.impl;

import com.pood.server.api.service.meta.order.orderPointService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.pood.server.api.service.record.*;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.meta.order.dto_order_point;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.*;

import com.pood.server.api.service.query.*;

@Service("orderPointService")
public class orderPointServiceImpl implements orderPointService{
    
    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_ORDER_POINT;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @SuppressWarnings("unchecked")
    public List<dto_order_point> getUsedPointList(String ORDER_NUMBER) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", ORDER_NUMBER);
        SELECT_QUERY.buildEqual("type",         0);
        return (List<dto_order_point>)listService.getDTOList(SELECT_QUERY.toString(), dto_order_point.class);
    }

    @Override
    public void insertRecord(String POINT_UUID, String ORDER_NUMBER, Integer USED_POINT) throws SQLException {
        updateService.insert(GET_QUERY_INSERT_RECORD(POINT_UUID, ORDER_NUMBER, USED_POINT));
    }

    @Override
    public void updateRetrievedStatus(Integer RECORD_IDX) throws SQLException {
        updateService.execute(GET_QUERY_UPDATE_RETRIEVED_STATUS(RECORD_IDX));
        
    }

    public String GET_QUERY_UPDATE_RETRIEVED_STATUS(Integer RECORD_IDX){
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("type",        true);
        return queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", RECORD_IDX.toString());
    }

    @Override
    public void init(String USER_UUID) throws Exception {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_uuid", USER_UUID);
        updateService.execute(DELETE_QUERY.toString());
        
    }

    @Override
    public String GET_QUERY_INSERT_RECORD(String POINT_UUID, String ORDER_NUMBER, Integer USED_POINT) {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("order_number",     ORDER_NUMBER);
        hashMap.put("point_uuid",       POINT_UUID);
        hashMap.put("used_point",       USED_POINT);
        hashMap.put("type",             0);
        return queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);
    }

}
