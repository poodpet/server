package com.pood.server.api.service.meta.event_type;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.config.DATABASE;
import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_event_type;

import org.springframework.stereotype.Service;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.record.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@Service("eventTypeService")
public class eventTypeServiceImpl implements eventTypeService {

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;
    
    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_EVENT_TYPE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @Override
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @SuppressWarnings("unchecked")
    public List<vo_event_type> getEventTypeList(pagingSet PAGING_SET) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<vo_event_type>)listService.getDTOList(SELECT_QUERY.toString(), vo_event_type.class);
    } 

    @Override
    public vo_event_type getEventTypeObject(Integer EVENT_TYPE_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", EVENT_TYPE_IDX);
        return (vo_event_type)listService.getDTOObject(SELECT_QUERY.toString(), vo_event_type.class);
    }

    
    
}
