package com.pood.server.api.service.meta.allergy_data;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.config.DATABASE;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_allergy_data;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.record.updateService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("allergyDataService")
public class allergyDataServiceImpl implements allergyDataService {

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_ALLERGY_DATA;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
    
    @SuppressWarnings("unchecked")
    /**************** 알러지 사이즈 목록 조회 *****************/
    public List<dto_allergy_data> getList(pagingSet PAGING_SET) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_allergy_data>)listService.getDTOList(SELECT_QUERY.toString(), dto_allergy_data.class);
    }

    /**************** 알러지 사이즈 목록 항목 개수 조회 *****************/
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @Override
    public dto_allergy_data getRecord(Integer idx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", idx);
        return (dto_allergy_data)listService.getDTOObject(SELECT_QUERY.toString(), dto_allergy_data.class);
    }
    
}
