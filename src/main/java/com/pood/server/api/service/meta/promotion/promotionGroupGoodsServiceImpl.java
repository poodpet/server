package com.pood.server.api.service.meta.promotion;

import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.pood.server.api.service.record.*;
import com.pood.server.config.DATABASE;
import com.pood.server.object.meta.vo_promotion_group_goods;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.query.*;

@Service("promotionGroupGoodsService")
public class promotionGroupGoodsServiceImpl implements promotionGroupGoodsService{
 
    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_PROMOTION_GROUP_GOODS;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @SuppressWarnings("unchecked")
    public List<vo_promotion_group_goods> getList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return (List<vo_promotion_group_goods>)listService.getDTOList(SELECT_QUERY.toString(), vo_promotion_group_goods.class);
    }

    @Override
    public void updateGoodsRating(Integer review_cnt, Double average_rating, Integer goods_idx) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("average_rating",   average_rating);
        hashMap.put("review_cnt",       review_cnt);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "goods_idx", goods_idx.toString());
        updateService.execute(query);
        query = null;
        
    }

    @Override
    public vo_promotion_group_goods getPromotionGoodsInfo(Integer pr_idx, Integer goods_idx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("pr_idx", pr_idx);
        SELECT_QUERY.buildEqual("goods_idx",goods_idx );
        return (vo_promotion_group_goods)listService.getDTOObject(SELECT_QUERY.toString(), vo_promotion_group_goods.class);
    }

}
