package com.pood.server.api.service.user;

public interface userTokenService {

    public String issueToken(String USER_UUID) throws Exception;

    public Boolean authentication(String USER_UUID, String TOKEN) throws Exception;

}
 