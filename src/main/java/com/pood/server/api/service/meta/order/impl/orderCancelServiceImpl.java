package com.pood.server.api.service.meta.order.impl;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.meta.order.orderCancelService;
import com.pood.server.api.service.query.queryService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.order.ORDER_CANCEL;
import com.pood.server.dto.meta.order.dto_order_cancel;
import com.pood.server.object.meta.vo_order_cancel;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service(value="orderCancelService")
public class orderCancelServiceImpl implements orderCancelService {
    
    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_ORDER_CANCEL;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    public String insertRecord(String text, String order_number, Integer goods_idx, Integer qty) throws SQLException{
        String RETRIEVE_UUID = UUID.randomUUID().toString();
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("order_number",     order_number);
        hashMap.put("goods_idx",        goods_idx);
        hashMap.put("qty",              qty);
        hashMap.put("type",             ORDER_CANCEL.REQUEST);
        hashMap.put("text",             text);
        hashMap.put("uuid",             RETRIEVE_UUID);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        updateService.insert(query);

        query = null;

        return RETRIEVE_UUID;
    }

    @Override
    public String insertRecord(final String text, final String orderNumber, final Integer goodsIdx,
        final Integer qty, final Integer deliveryFee, final Integer refundOrderPrice,
        final Integer refundPoint) throws SQLException {
        String RETRIEVE_UUID = UUID.randomUUID().toString();
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("order_number", orderNumber);
        hashMap.put("goods_idx", goodsIdx);
        hashMap.put("qty", qty);
        hashMap.put("type", ORDER_CANCEL.REQUEST);
        hashMap.put("text", text);
        hashMap.put("refund_delivery_fee", deliveryFee);
        hashMap.put("refund_order_price", refundOrderPrice);
        hashMap.put("refund_point", refundPoint);
        hashMap.put("uuid", RETRIEVE_UUID);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap,
            com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        updateService.insert(query);

        query = null;

        return RETRIEVE_UUID;
    }

    @SuppressWarnings("unchecked")
    public List<dto_order_cancel> getList(String ORDER_NUMBER) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", ORDER_NUMBER);
        return (List<dto_order_cancel>)listService.getDTOList(SELECT_QUERY.toString(), dto_order_cancel.class);
    }

    @Override
    public void updateSuccess(String RETREIVE_UUID) throws Exception {
        updateService.execute(GET_QUERY_UPDATE_SUCCESS(RETREIVE_UUID));
        
    }

    public String GET_QUERY_UPDATE_SUCCESS(String RETREIVE_UUID){
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("type", ORDER_CANCEL.SUCCESS);
        return queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "uuid", RETREIVE_UUID);
    }

    @SuppressWarnings("unchecked")
    public Integer getRetreievedCount(String ORDER_NUMBER, Integer GOODS_IDX) throws Exception{
        Integer retreieve_cnt = 0;

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number",     ORDER_NUMBER);
        SELECT_QUERY.buildEqual("goods_idx",        GOODS_IDX);
        SELECT_QUERY.buildEqual("type",             ORDER_CANCEL.SUCCESS);
        List<vo_order_cancel> list = (List<vo_order_cancel>)listService.getDTOList(SELECT_QUERY.toString(), vo_order_cancel.class);

        if(list != null)
            for(vo_order_cancel e : list)
                retreieve_cnt += e.getQty();
 
        return retreieve_cnt;
    }

}
