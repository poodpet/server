package com.pood.server.api.service.meta.order;

import java.util.List;

import com.pood.server.dto.meta.order.dto_order_delivery_track;
import com.pood.server.object.meta.vo_order_delivery_track;

import java.sql.SQLException;

public interface orderDeliveryTrackService {

    public Integer insertRecord(
            Integer GOODS_IDX, 
            Integer HISTORY_IDX, 
            String  ORDER_NUMBER,
            String  COURIER, 
            Integer TRACK_TYPE, 
            String  TRACKING_ID, 
            String  START_DATE,
            Integer SEND_TYPE,
            Integer QTY,
            Integer MAIN_ADDRESS) throws SQLException;
    
    /************* 주문 번호에 해당하는 배송지 정보 조회 *************/
    public List<dto_order_delivery_track> getOrderDeliveryInfo(String order_number) throws Exception;

    public List<vo_order_delivery_track> getOrderDeliveryInfo2(String oRDER_NUMBER) throws Exception;

    
}
