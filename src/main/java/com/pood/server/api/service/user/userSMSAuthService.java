package com.pood.server.api.service.user;

import java.sql.SQLException;

import com.pood.server.dto.user.dto_user_sms_auth;

public interface userSMSAuthService {



    /*************** 회원 인증번호 항목 추가 ******************/
	public Integer insertRecord(String PHONE_NUMBER, String SMS_AUTH) throws SQLException;
 


    /*************** 회원 인증 번호 조회 ******************/
    public dto_user_sms_auth getSMSAuth(String PHONE_NUMBER) throws Exception;




    /*************** 인증 번호 업데이트******************/
	public void updateRecord(String PHONE_NUMBER, String AUTH_NUMBER) throws SQLException;
    


    /*************** SMS 발송 *********************/
    public Integer sendSMS(String PHONE_NUMBER, String MESSAGE) throws Exception;



    /************** 특정 레코드에 대해서 인증 성공으로 업데이트 *************/
	public void updateAuthSuccess(Integer idx) throws SQLException;



    public Boolean isRecordbirthValid(String PHONE_NUMBER) throws Exception;


}
