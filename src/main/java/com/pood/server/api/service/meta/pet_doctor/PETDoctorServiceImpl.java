package com.pood.server.api.service.meta.pet_doctor;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_pet_doctor_image;
import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.record.*;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.meta.pet.dto_pet_doctor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("PETDoctorService")
public class PETDoctorServiceImpl implements PETDoctorService {

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_PET_DOCTOR;

    private String DEFINE_TABLE_IMAGE_NAME = DATABASE.TABLE_PET_DOCTOR_IMAGE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    /**************** 결제 요청 텍스트 목록 조회 *****************/
    @SuppressWarnings("unchecked")
    public List<dto_pet_doctor> getPetDoctorList(pagingSet PAGING_SET) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_pet_doctor>)listService.getDTOListWithImage(
                SELECT_QUERY.toString(), "pd_idx", DEFINE_DB_NAME, DEFINE_TABLE_IMAGE_NAME, dto_pet_doctor.class, vo_pet_doctor_image.class);
    }

    /**************** 결제 요청 텍스트 목록 항목 개수 조회 *****************/
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    }
}
