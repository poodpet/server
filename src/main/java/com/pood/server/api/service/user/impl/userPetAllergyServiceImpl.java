package com.pood.server.api.service.user.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.user.userPetAllergyService;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.user.pet.dto_user_pet_allergy;
import com.pood.server.object.meta.vo_allergy_data;
import com.pood.server.object.user.vo_user_pet_allergy;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.meta.allergy_data.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.pood.server.api.service.query.*;

@Service("userPetAllergyService")
public class userPetAllergyServiceImpl implements userPetAllergyService{
    

    @Autowired
    @Qualifier("updateService")
    updateService updateService;
    
    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("allergyDataService")
    allergyDataService allergyDataService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME = DATABASE.TABLE_USER_PET_ALLERGY;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    
     /******************* 회원 반려 동물 항목 번호에 해당하는 질병 번호 조회 ********************/
     @SuppressWarnings("unchecked")
    public List<vo_user_pet_allergy> getList(Integer USER_PET_IDX) throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_pet_idx", USER_PET_IDX);
        List<dto_user_pet_allergy> list = (List<dto_user_pet_allergy>)listService.getDTOList(SELECT_QUERY.toString(), dto_user_pet_allergy.class);

        List<vo_user_pet_allergy> result = new ArrayList<vo_user_pet_allergy>();
        
        if(list != null){
            for(dto_user_pet_allergy e : list){
                vo_user_pet_allergy record = new vo_user_pet_allergy();
                record.setAllergy_idx((Integer)e.getAllergy_idx());
                record.setName((String)e.getName());
                record.setType((Integer)e.getType());
                record.setUser_pet_idx((Integer)e.getUser_pet_idx());

                result.add(record);
            }
        }
        return result;
    }

    /******************** 회원 동물 AI 질병 정보 등록 ******************************/
    public Integer insertRecord(Integer USER_PET_IDX, Integer ALLERGY_IDX) throws Exception {
        vo_allergy_data record = allergyDataService.getRecord(ALLERGY_IDX);
        
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_pet_idx",     USER_PET_IDX);
        hashMap.put("allergy_idx",      ALLERGY_IDX);
        hashMap.put("name",             record.getName());
        hashMap.put("type",             record.getType());

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_NOT_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }

    @Override
    public void deleteRecordWithUserPetIDX(Integer USER_PET_IDX) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_pet_idx", Integer.toString(USER_PET_IDX));
        updateService.execute(DELETE_QUERY.toString());
    }
}
