package com.pood.server.api.service.time;

import com.pood.server.config.meta.META_ORDER_REFUND_EXCHANGE;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.stereotype.Service;

@Service("timeService")
public class timeServiceImpl implements timeService {

    @Override
    public long getTimeDifference(String DATE1, String DATE2) throws Exception {
        Date DATETIME1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(DATE1);
        Date DATETIME2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(DATE2);
        long DIFF = DATETIME2.getTime() - DATETIME1.getTime();
        long DIFF_SECONDS = DIFF / 1000;
        return DIFF_SECONDS;
    }

    public Integer getDayDifference(String DATE1, String DATE2) throws Exception {
        Date DATETIME1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(DATE1);
        Date DATETIME2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(DATE2);
        DateTime start = new DateTime(DATETIME1.getTime());
        DateTime end = new DateTime(DATETIME2.getTime());
        return Days.daysBetween(start, end).getDays();
    }

    public String getCurrentTime() {
        java.util.Date date = new Date();
        SimpleDateFormat fDTOatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        fDTOatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
        return fDTOatter.format(date);
    }

    public String getCurrentTime2() {
        java.util.Date date = new Date();
        SimpleDateFormat fDTOatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        fDTOatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
        return fDTOatter.format(date);
    }

    public String getCurrentDate() {
        java.util.Date date = new Date();
        SimpleDateFormat fDTOatter = new SimpleDateFormat("yyyy-MM-dd");
        fDTOatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
        return fDTOatter.format(date);
    }

    @Override
    public Boolean isExpired(String DATETIME) throws ParseException {
        SimpleDateFormat SIMPLE_DATE_FDTOAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return SIMPLE_DATE_FDTOAT.parse(DATETIME)
            .before(SIMPLE_DATE_FDTOAT.parse(getCurrentTime()));
    }

    @Override
    public String getDateTime5(String DATETIME) throws ParseException {
        SimpleDateFormat SIMPLE_DATE_FDTOAT = new SimpleDateFormat("yyyyMMdd");
        Date date = SIMPLE_DATE_FDTOAT.parse(DATETIME);
        SimpleDateFormat fDTOatter = new SimpleDateFormat("yyyy-MM-dd");
        fDTOatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
        return fDTOatter.format(date);
    }


    public Boolean isExpired(String DATETIME1, String DATETIME2) throws ParseException {
        SimpleDateFormat SIMPLE_DATE_FDTOAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return SIMPLE_DATE_FDTOAT.parse(DATETIME2).before(SIMPLE_DATE_FDTOAT.parse(DATETIME1));
    }


    @Override
    public Boolean isIn(String START_DATE, String END_DATE) throws ParseException {
        SimpleDateFormat SIMPLE_DATE_FDTOAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Boolean CHK_START_DATE = SIMPLE_DATE_FDTOAT.parse(START_DATE)
            .before(SIMPLE_DATE_FDTOAT.parse(getCurrentTime()));

        Boolean CHK_END_DATE = SIMPLE_DATE_FDTOAT.parse(getCurrentTime())
            .before(SIMPLE_DATE_FDTOAT.parse(END_DATE));

        if ((CHK_START_DATE) && (CHK_END_DATE)) {
            return true;
        }

        return false;
    }

    @Override
    public Integer EXCHANGE_REFUND_EXPIRED_HOURS() throws Exception {
        LocalTime CURRENT_TIME = new LocalTime();
        LocalTime EXPIRED_TIME = new LocalTime(META_ORDER_REFUND_EXCHANGE.EXPIRED_TIME);

        return CURRENT_TIME.compareTo(EXPIRED_TIME);
    }

    @Override
    public String addDay(String CURRENT_TIME, int day) throws Exception {
        Date DATETIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(CURRENT_TIME);
        DateTime time = new DateTime(DATETIME.getTime());
        time = time.plusDays(day);
        return time.toString("yyyy-MM-dd HH:mm:ss");
    }


    @Override
    public String addDay(int day) {
        LocalDateTime plusDay = LocalDateTime.now().plusDays(day)
            .with(java.time.LocalTime.MAX).minusSeconds(1);
        return plusDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    public DateTime getDateTime(String datetime) {
        return DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS").parseDateTime(datetime);
    }

    public DateTime getDateTime2(String datetime) {
        return DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(datetime);
    }

    public String getRetreieveTimeLimit() {
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
        int day = now.get(Calendar.DAY_OF_MONTH);
        return String.format("%d-%02d-%02d 14:00:00", year, month, day);
    }

    @Override
    public Boolean isInToday(DateTime time) {
        if ((time.toLocalDate()).equals(new LocalDate())) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean isInYesterDay(DateTime recordbirth) throws Exception {
        if ((recordbirth.toLocalDate()).equals(new LocalDate().minusDays(1))) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String getRetreieveTimeYesterday() throws Exception {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.DAY_OF_MONTH, -1);
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
        int day = now.get(Calendar.DAY_OF_MONTH);
        return String.format("%d-%02d-%02d 14:00:00", year, month, day);
    }
}
