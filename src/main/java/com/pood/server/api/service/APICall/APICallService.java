package com.pood.server.api.service.APICall;

import java.util.HashMap;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;

public interface APICallService {

    ResponseEntity<String> post(MediaType header_type, HttpHeaders headers, MultiValueMap<String, String> body, String url);

    ResponseEntity<String> post2(MediaType header_type, HttpHeaders headers, HashMap<String, Object> body, String url);

    ResponseEntity<String> get(HttpHeaders headers, String url);

    ResponseEntity<String> getFast(HttpHeaders headers, String url);
}
