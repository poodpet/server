package com.pood.server.api.service.json;

public interface jsonService {

    // 해제된 오브젝트 반환
    Object getDecodedObject(String text, Class<?> value_type) throws Exception;

}
