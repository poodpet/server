package com.pood.server.api.service.pood_point;

import java.util.List;

public interface poodPointService {
    
    public List<String> issuePoint(String ORDER_NUMBER, Integer REVIEW_IDX, String USER_UUID, Integer POINT_IDX, Integer POINT_STATUS) throws Exception ;

    public List<String> cancelIssuedPoint(String ORDER_NUMBER, Double RATIO) throws Exception;

    public Integer usePoint(String ORDER_NUMBER, String USER_UUID, Integer TYPE, String TEXT, Integer USE_POINT) throws Exception;

    public List<String> rollbackOrderPoint(String USER_UUID, String ORDER_NUMBER, Integer TYPE, String TEXT, Integer RETURN_POINT) throws Exception;

}
