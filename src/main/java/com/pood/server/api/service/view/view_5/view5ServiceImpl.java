package com.pood.server.api.service.view.view_5;

import java.util.List;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.config.DATABASE;
import com.pood.server.object.meta.maintab.vo_maintab_brand;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.record.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import org.springframework.stereotype.Service;

@Service("view5Service")
public class view5ServiceImpl implements view5Service{
    
    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    private String DEFINE_TABLE_NAME    = DATABASE.VIEW_5;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @SuppressWarnings("unchecked")
    public List<vo_maintab_brand> getList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        List<vo_maintab_brand> list = (List<vo_maintab_brand>)listService.getDTOList(SELECT_QUERY.toString(), vo_maintab_brand.class);
        return list;
    }

}
