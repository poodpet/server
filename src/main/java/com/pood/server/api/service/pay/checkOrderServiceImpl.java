package com.pood.server.api.service.pay;

import com.pood.server.dto.meta.goods.PromotionDto;
import com.pood.server.repository.meta.PromotionRepository;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.pood.server.object.meta.vo_goods;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.user.*;
import com.pood.server.api.service.delivery.remoteDeliveryService;
import com.pood.server.api.service.list.*;
import com.pood.server.object.IMP.*;
import com.pood.server.api.service.meta.coupon.*;
import com.pood.server.api.service.meta.goods.*;
import com.pood.server.api.service.meta.order.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.pood.server.api.service.time.*;
import com.pood.server.api.service.sns.*;
import com.pood.server.api.service.log.*;
import com.pood.server.api.service.error.*;
import com.pood.server.api.service.noti.*;
import com.pood.server.api.service.pood_point.*;
import com.pood.server.api.service.meta.point.*;
import com.pood.server.api.service.view.view_2.*;
import com.pood.server.api.service.view.view_8.*;
import com.pood.server.object.resp.resp_pay_success;
import com.pood.server.dto.log.user_order.dto_log_user_order;
import com.pood.server.dto.meta.dto_coupon;
import com.pood.server.dto.user.dto_user_coupon_2;
import com.pood.server.config.meta.META_GOODS;
import com.pood.server.config.meta.coupon.COUPON_TYPE;
import com.pood.server.config.meta.META_COUPON;

@Service("checkOrderService")
public class checkOrderServiceImpl implements checkOrderService{

    Logger logger = LoggerFactory.getLogger(checkOrderServiceImpl.class);

    @Autowired
    @Qualifier("orderBasketService")
    public orderBasketService orderBasketService;

    @Autowired
    @Qualifier("timeService")
    timeService timeService;

    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;

    @Autowired
    @Qualifier("goodsCouponService")
    goodsCouponService goodsCouponService;

    @Autowired
    @Qualifier("goodsProductService")
    goodsProductService goodsProductService;

    @Autowired
    @Qualifier("couponService")
    couponService couponService;

    @Autowired
    @Qualifier("couponBrandService")
    couponBrandService couponBrandService;

    @Autowired
    @Qualifier("couponGoodsService")
    couponGoodsService couponGoodsService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("orderDeliveryService")
    orderDeliveryService orderDeliveryService;
    
    @Autowired
    @Qualifier("orderService")
    OrderService orderService;

    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("userCouponService")
    userCouponService userCouponService;

    @Autowired
    @Qualifier("userBasketService")
    userBasketService userBasketService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("logUserOrderService")
    logUserOrderService logUserOrderService;

    @Autowired
    @Qualifier("userTokenService")
    userTokenService userTokenService;

    @Autowired
    @Qualifier("pushService")
    public pushService pushService;

    @Autowired
    @Qualifier("snsService")
    public snsService snsService;

    @Autowired
    @Qualifier("logUserSavePointService")
    logUserSavePointService logUserSavePointService;

    @Autowired
    @Qualifier("logUserUsePointService")
    logUserUsePointService logUserUsePointService;
   
    @Autowired
    @Qualifier("pointService")
    pointService pointService;

    @Autowired
    @Qualifier("retreieveService")
    retreieveService retreieveService;

    @Autowired
    @Qualifier("poodPointService")
    poodPointService poodPointService;

    @Autowired
    @Qualifier("remoteDeliveryService")
    remoteDeliveryService remoteDeliveryService;

    @Autowired
    @Qualifier("logUserOrderBasketService")
    logUserOrderBasketService logUserOrderBasketService;


    @Autowired
    @Qualifier("errorOrderFailService")
    errorOrderFailService errorOrderFailService;


    @Autowired
    @Qualifier("view2Service")
    view2Service view2Service;

    @Autowired
    @Qualifier("view8Service")
    view8Service view8Service;

    @Autowired
    PromotionRepository promotionRepository;

    // 주문 데이터 유효성 검증
    public resp_pay_success getOrderCheck(IMP IAMPORT) throws Exception {
    
        


        Boolean is100Goods = false;



       
        // 주문 번호 생성
        List<IMP_SHOP> SHOPPINGBAG_DATA = IAMPORT.order.getShoppingbag_data();



        String MERCHANT_UID = IAMPORT.MERCHANT_UID;


        
        // 적립금 할인 금액에 포함됨
        logger.info("소진한 적립금 " + IAMPORT.ORDER_USED_POINT+"원이 총 할인금액에 반영됩니다. ");

        IAMPORT.ADD_CALC_TOTAL_DISCOUNT_PRICE(IAMPORT.ORDER_USED_POINT);

        

        Integer DELIVERY_REMOTE_TYPE = remoteDeliveryService.getRemoteType(IAMPORT.DELIVERY_ZIPCODE);



        if(!(IAMPORT.DELIVERY_REMOTE_TYPE.equals(DELIVERY_REMOTE_TYPE)))
            return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "원격 배송지 타입이 올바르지 않습니다. 원격배송지 타입은 (" +DELIVERY_REMOTE_TYPE+")이 되어야 합니다.", false);




        Integer ORDER_CALC_AMOUNT   = 0;


        Integer GOODS_100_COUNT     = 0;

        

        List<IMP_BASKET> ORDER_BASKET = new ArrayList<IMP_BASKET>();
        



       /*******************  장바구니 데이터 레코드 별로 가격 검증  *****************************/
        for(IMP_SHOP e1 : SHOPPINGBAG_DATA){

            IMP_BASKET BASKET_DATA = new IMP_BASKET(e1);

            logger.info(BASKET_DATA.toString());


            /****************** 장바구니 쿠폰 요효성 검증 **************************/
            if(BASKET_DATA.USER_COUPON_IDX != -1){
                
                Integer COUPON_IDX = userCouponService.getUserCOUPONIDX(BASKET_DATA.USER_COUPON_IDX);




                
                BASKET_DATA.setCouponIDX(COUPON_IDX);

                



                // 회원 쿠폰 번호에 해당하는 쿠폰의 유효성 체크
                if(COUPON_IDX == -1)
                    return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "회원의 쿠폰[" + BASKET_DATA.USER_COUPON_IDX + "]은 더 이상 사용할 수 없는 쿠폰입니다. ", false);
            






                // 쿠폰 항목 번호에 해당하는 쿠폰 정보 조회
                dto_coupon coupon = new dto_coupon();
                coupon = couponService.getCoupon(BASKET_DATA.COUPON_IDX);

                BASKET_DATA.setCouponAvailableTime(coupon.getAvailable_time());
                BASKET_DATA.setCouponLimitPrice(coupon.getLimit_price());
                BASKET_DATA.setCouponName(coupon.getName());
                BASKET_DATA.setCouponType(coupon.getPublish_type_idx());
                BASKET_DATA.setCouponMaxPrice(coupon.getMax_price());
                BASKET_DATA.setCouponDiscountRate(coupon.getDiscount_rate());





                // 해당 쿠폰이 만료된 쿠폰인지 확인
                if(!timeService.isExpired(BASKET_DATA.COUPON_AVAILABLE_TIME))
                    return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "쿠폰[" + BASKET_DATA.COUPON_NAME + "]이 만료 되었습니다. ", false);






                // 쿠폰이 굿즈과 매핑되는 쿠폰인지 확인
                if(!goodsCouponService.checkCouponAvailableWithgoods(BASKET_DATA.COUPON_IDX, BASKET_DATA.GOODS_IDX))
                    return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "쿠폰[" +BASKET_DATA.COUPON_NAME+ "]은 굿즈[" + BASKET_DATA.GOODS_NAME + "]와 사용할 수 없습니다. ", false);
   



                
                
                // 일점 금액 이상으로 구입이 되는지 확인
                if(!BASKET_DATA.checkPurchaseLimit())
                    return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "굿즈[" + BASKET_DATA.GOODS_IDX + "] 구매 가격이 " + BASKET_DATA.PURCHASED_AMOUNT + " 쿠폰 제한 금액인 " + BASKET_DATA.COUPON_NAME + " LIMIT PRICE " + BASKET_DATA.COUPON_LIMIT_PRICE + "원 보다 적습니다. ", false);  
                


                // 할인 금액 계산
                BASKET_DATA.setDiscountPrice();



            }

            /****************** 기획전 유효성 검증 **************************/
            if(BASKET_DATA.PROMOTION_IDX != -1){
                PromotionDto.PromotionGoodsListInfo promotionInfo = promotionRepository.findListInfoById(BASKET_DATA.PROMOTION_IDX);
                if(promotionInfo == null)
                    return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "굿즈[" + BASKET_DATA.GOODS_IDX + "]는 현재 사용할 수 없는 프로모션 가격이 적용되어 있습니다. 상품 정보를 확인 부탁드립니다.", false);
            }


            Integer BRAND_IDX = view2Service.getBrandIDX(BASKET_DATA.GOODS_IDX);

            BASKET_DATA.setBrandIDX(BRAND_IDX);

            /************** 굿즈별 유효성 검증 *******************/
            // 굿즈 항목 번호에 해당하는 굿즈 정보 조회
            vo_goods goods = goodsService.GET_GOODS_OBJECT_1(BASKET_DATA.GOODS_IDX);


            if(goods == null)
                return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "굿즈[" + BASKET_DATA.GOODS_IDX + "]는 현재 사용할 수 없는 굿즈 입니다. 판매중이지 않거나 없는 굿즈입니다.", false);  



            if(goods.getSale_status() != META_GOODS.SALE_STATUS_ON)
            return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "굿즈[" + BASKET_DATA.GOODS_IDX + "]는 현재 판매중인 굿즈가 아닙니다.", false);  


            Integer GOODS_TYPE     = goods.getGoods_type_idx();

            Integer GOODS_PRICE    = goods.getGoods_price();

            if(BASKET_DATA.PROMOTION_IDX != -1){
                PromotionDto.PromotionGroupGoods2 promotionGroupGoods2 = promotionRepository.promotionGroupGoodsInfo(BASKET_DATA.GOODS_IDX, BASKET_DATA.PROMOTION_IDX);
                GOODS_PRICE = promotionGroupGoods2.getPrPrice();
            }


            // 100원 굿즈 포함여부
            if(GOODS_TYPE.equals(META_GOODS.GOODS_100)){

                GOODS_100_COUNT++;

                if(GOODS_100_COUNT > 1)
                    return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "100원 딜은 한 개만 구매 가능합니다. ", false);                             

                String HISTORY = view8Service.isPurchase2(IAMPORT.USER_UUID, BASKET_DATA.GOODS_IDX);

                if(HISTORY != null)
                    return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "회원은 해당 굿즈["+BASKET_DATA.GOODS_NAME+"]에 대해서 구매한 기록이 있습니다. 주문번호 : "+HISTORY, false);  
            
                HISTORY = null;

            }









            // 굿즈 구매가능한 수량
            Integer PURCHASE_TYPE       = goods.getPurchase_type();

            Integer LIMIT_QUANTITY      = goods.getLimit_quantity();

            Integer GOODS_QUANTITY      = goods.getQuantity();

            Integer PURCHASE_QUANTITY   = BASKET_DATA.PURCHASED_QTY;






            // 한 계정당 한번만 구매 기록
            if(PURCHASE_TYPE == META_GOODS.PURCHASE_TYPE_BUY_ONE){   
                dto_log_user_order buy_check = logUserOrderService.getGoodsBought(BASKET_DATA.GOODS_IDX);
                if(buy_check != null){
                    String BUY_CHECK_RECORDBIRTH    = buy_check.getRecordbirth();
                    String BUY_CHECK_ORDER_NUMBER   = buy_check.getOrder_number(); 
                    return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "해당 제품은 1회 구매 가능 제품입니다. 굿즈[" + goods.getGoods_name()+"]에 대해서 " + BUY_CHECK_RECORDBIRTH+" 구매한 기록이 있습니다. 주문 번호 : " + BUY_CHECK_ORDER_NUMBER, false);  
                }
            }



            if(LIMIT_QUANTITY == META_GOODS.LIMIT_QUANTITY_ONE){

                // 굿즈에 수량 구매제한이 걸려있는 경우
                if(PURCHASE_QUANTITY != 1)
                    return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "굿즈의 구매가능한 수량 이상으로 굿즈 구매를 시도하여 에러가 발생하였습니다. 굿즈[" + goods.getGoods_name()+"]는 1개만 구매가능한 제품입니다.", false);  

                
            }

            // 구매 제한이 아닌 경우 
            if(LIMIT_QUANTITY == META_GOODS.LIMIT_QUANTITY_LESS_THAN){

                // 굿즈에 수량 구매제한이 걸려있는 경우
                if(PURCHASE_QUANTITY > GOODS_QUANTITY)
                    return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "굿즈의 구매가능한 수량 이상으로 굿즈 구매를 시도하여 에러가 발생하였습니다. 굿즈[" + goods.getGoods_name()+"]의 구매 가능 제한 개수 : " + GOODS_QUANTITY, false);                      
                
            }
            
            
            BASKET_DATA.setgoodsName(goods.getGoods_name());
            BASKET_DATA.setgoodsPurchaseType(goods.getPurchase_type());
            BASKET_DATA.setgoodsPrice(GOODS_PRICE);
            BASKET_DATA.setgoodsSaleStatus(goods.getSale_status());


            logger.info("BASKET_DATA.GOODS_PRICE" + BASKET_DATA.GOODS_PRICE);
            logger.info("BASKET_DATA.PURCHASED_GOODS_PRICE" + BASKET_DATA.PURCHASED_GOODS_PRICE);
            // 구매시 가격 변동이 있을 경우 페일
            if(!BASKET_DATA.checkgoodsPriceChanged())
                return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "굿즈[" + BASKET_DATA.GOODS_NAME+ "]의 가격이 맞지 않습니다. 굿즈 가격 : " + BASKET_DATA.GOODS_PRICE+ ", 구매시 가격 = " + BASKET_DATA.PURCHASED_GOODS_PRICE+ ". ", false);                      

                







            // 판매중이지 않은 상태인 경우 구매가 안되도록    
            if(!BASKET_DATA.checkgoodsSaleStatus())
                return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "굿즈[" + BASKET_DATA.GOODS_NAME+ "]가 판매중이 아닙니다. ", false);                      











            // 재고가 없는 상품인 경우 구매가 안되도록
            if(goodsProductService.isOutOfStock(BASKET_DATA.GOODS_IDX)){
                return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "굿즈[" + BASKET_DATA.GOODS_NAME+ "]의 재고가 모두 소진되었습니다. ", false);                      
            }
    









            // 구매 제한 수량 계산
            if(!BASKET_DATA.checkPurchasePriceLimit())
                return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "쿠폰[" + BASKET_DATA.COUPON_NAME+ "] 이 제한 금액을 충족시키지 못했습니다. ", false);                      
 
 



            // 장바구니 별로 금액 총 주문 금액에 합산하여 처리 
            ORDER_CALC_AMOUNT += BASKET_DATA.PURCHASED_AMOUNT;






            logger.info("\n*********** 장바구니 데이터 : " + BASKET_DATA.toString());

            IAMPORT.ADD_CALC_TOTAL_DISCOUNT_PRICE(BASKET_DATA.DISCOUNT_PRICE);
            
            logger.info("장바구니별 할인금액 " + BASKET_DATA.DISCOUNT_PRICE+"원이 최종 할인 금액에 적용되었습니다. ");


            ORDER_BASKET.add(BASKET_DATA);
        }

 


        // 무료배송 쿠폰 썼을 때 배송비 무료
        Boolean DELIVERY_COUPON_FEE = false;

        logger.info("################### 0");


        // 중복 쿠폰 할인 적용
        if(IAMPORT.ORDER_OVER_COUPON_IDX != -1){

            logger.info("################### 1");

            // 중복 쿠폰 사용 가능 여부 검증 
            dto_user_coupon_2 record = userCouponService.getUserCoupon("idx", IAMPORT.ORDER_OVER_COUPON_IDX);

            if(record == null)
                return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "해당 회원 쿠폰["+IAMPORT.ORDER_OVER_COUPON_IDX+"]은 사용할 수 없는 쿠폰입니다. ", false);                      
                



                
            record = null; 




            // 중복 쿠폰 할인 금액 정산
            // OVER_COUPON_IDX : 쿠폰 항목 번호
            Integer OVER_COUPON_IDX = userCouponService.getUserCOUPONIDX(IAMPORT.ORDER_OVER_COUPON_IDX);


            // 배송비 무료 쿠폰 적용
            if(OVER_COUPON_IDX == META_COUPON.DELIVERY_FEE_FREE){
                logger.info("배송비 무료 쿠폰이 적용되었습니다.");

                DELIVERY_COUPON_FEE = true;

            }else {

                IMP_OVER_COUPON IMP_OVER_COUPON = couponService.getOverCoupon(OVER_COUPON_IDX);
                
                IAMPORT.SET_OVER_COUPON(IMP_OVER_COUPON);

                Integer COUPON_PUBLISH_TYPE = IMP_OVER_COUPON.getOVER_COUPON_PUBLISH_TYPE();




                // 만약 해당 쿠폰이 브랜드 쿠폰인 경우 처리
                if(COUPON_PUBLISH_TYPE == COUPON_TYPE.BRAND_COUPON){
                    logger.info("브랜드 쿠폰이 적용되었습니다. ");

                    List<Integer> BRAND_IDX_LIST = couponBrandService.getBrandIDXList(OVER_COUPON_IDX);


                    Integer CHK_BRAND_DISCOUNT_PRICE = 0;

                    for(IMP_BASKET e5 : ORDER_BASKET){
                        // 주문 굿즈 중에서 브 랜드에 매칭되는 굿즈가 있을 경우
                        if(BRAND_IDX_LIST != null){
                            for(Integer e6 : BRAND_IDX_LIST){
                                if(e5.BRAND_IDX.equals(e6)){
                                    CHK_BRAND_DISCOUNT_PRICE += e5.PURCHASED_GOODS_PRICE * e5.PURCHASED_QTY;
                                }
                            }
                        }
                    }

                    if(CHK_BRAND_DISCOUNT_PRICE < IMP_OVER_COUPON.getOVER_COUPON_MAX_PRICE()){
                        return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "해당 브랜드 쿠폰 적용을 위한 제품 구매가 충분하지 않습니다. 브랜드 제품 합산 금액 : "+CHK_BRAND_DISCOUNT_PRICE+"원, 브랜드 쿠폰 할인 금액 : "+IMP_OVER_COUPON.getOVER_COUPON_MAX_PRICE(), false);   
                    }
                    
                }

                // 만약 해당 쿠폰이 단품 쿠폰인 경우 처리
                if(COUPON_PUBLISH_TYPE == COUPON_TYPE.GOODS_COUPON){
                    logger.info("단품 쿠폰이 적용되었습니다. ");

                    List<Integer> GOODS_IDX_LIST = couponGoodsService.getGoodsIDXList(OVER_COUPON_IDX);


                    Integer CHK_GOODS_COUPON_DISCOUNT_PRICE = 0;

                    for(IMP_BASKET e5 : ORDER_BASKET){

                        // 주문 단품 중에서 단품에 매칭되는 단품이 있을 경우
                        if(GOODS_IDX_LIST != null){
                            for(Integer e6 : GOODS_IDX_LIST){
                                if(e5.GOODS_IDX.equals(e6)){
                                    CHK_GOODS_COUPON_DISCOUNT_PRICE += e5.PURCHASED_GOODS_PRICE;
                                }
                            }
                        }
                    }

                    if(CHK_GOODS_COUPON_DISCOUNT_PRICE < IMP_OVER_COUPON.getOVER_COUPON_MAX_PRICE()){
                        return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "해당 단품 쿠폰 적용을 위한 제품 구매가 충분하지 않습니다. 단품 합산 금액 : "+CHK_GOODS_COUPON_DISCOUNT_PRICE+"원, 단품 쿠폰 할인 금액 : "+IMP_OVER_COUPON.getOVER_COUPON_MAX_PRICE(), false);   
                    }

                }

                Integer OVER_COUPON_DISCOUNT = overCouponDiscount(
                    IAMPORT.OVER_COUPON_TYPE, 
                    IAMPORT.CALC_TOTAL_DISCOUNT_PRICE, 
                    IAMPORT.ORDER_TOTAL_PRICE, 
                    IAMPORT.OVER_COUPON_MAX_PRICE, 
                    IAMPORT.OVER_COUPON_DISCOUNT_RATE);

                logger.info("전체 쿠폰금액 " + OVER_COUPON_DISCOUNT+"이 총 할인 금액에 추가가 되었습니다.");

                IAMPORT.ADD_CALC_TOTAL_DISCOUNT_PRICE(OVER_COUPON_DISCOUNT);
                IAMPORT.SET_OVER_COUPON_DISCOUNT_PRICE(OVER_COUPON_DISCOUNT);
                
 
                // 전체 쿠폰이 적용이 되어있을 경우에만 할인 금액 적용
                Boolean CHK_COUPON_LIMIT    = checkCouponLimit(IMP_OVER_COUPON, ORDER_CALC_AMOUNT);

                if(!CHK_COUPON_LIMIT) {
                    return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "쿠폰이 유효하지 않습니다. : " + IMP_OVER_COUPON.getOVER_COUPON_NAME()+", 해당 쿠폰은 최소 " + IMP_OVER_COUPON.getOVER_COUPON_LIMIT_PRICE()+"원 이상 주문시 사용할 수 있습니다. 주문 금액 : "+ORDER_CALC_AMOUNT+"원", false);
                }
            }
            
        }
        // 전체 할인 금액 비교
        if(!checkDiscountPrice(IAMPORT))
            return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "할인금액이 유효하지 않습니다. 할인 금액은 " + IAMPORT.CALC_TOTAL_DISCOUNT_PRICE + "원이 되어야 합니다. ", false);







        
        // 총 주문 금액에서 총 할인 금액 빼줌
        ORDER_CALC_AMOUNT -= IAMPORT.CALC_TOTAL_DISCOUNT_PRICE;

 
 
        



        // 전체 주문 금액 비교        
        logger.info("총 계산된 할인 금액 : " + IAMPORT.CALC_TOTAL_DISCOUNT_PRICE+"원");


        if(is100Goods){
            if(ORDER_CALC_AMOUNT < 10000)
                return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "100원 굿즈는 장바구니 금액이 총 10,000원 이상일 때만 구매 가능합니다, 배송비 제외한 주문 금액 : "+ORDER_CALC_AMOUNT+"원", false);
        }
        

        Integer DELIVERY_FEE        = IAMPORT.ORDER_DELIVERY_FEE;


        if(!DELIVERY_COUPON_FEE){

            // 주문 금액에 따라 배송지 금액 부과 : 19800원 이상일 경우 무료배송 ( 주문 금액에서 배송금액을 뺀 가격으로 계산해야 함 )
            Integer CHK_DELIVERY_FEE    = remoteDeliveryService.checkDeliveryFee(
                IAMPORT.DELIVERY_REMOTE_TYPE,       // 배송지 도서 산간 타입
                DELIVERY_FEE,                       // 배송비
                ORDER_CALC_AMOUNT,                  // 주문금액
                IAMPORT.DELIVERY_ZIPCODE,           // 배송지 우편번호
                IAMPORT.USER_UUID                   // 회원 고유 번호
                );
 
            if(!Objects.equals(CHK_DELIVERY_FEE, DELIVERY_FEE))
                return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "입력받은 배송금액이 유효하지 않습니다. : " + DELIVERY_FEE+"원, 배송 금액은 " + CHK_DELIVERY_FEE+"원이 되어야 합니다. ", false);
    
        }else if(DELIVERY_FEE != 0)
            return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "입력받은 배송금액이 유효하지 않습니다. 무료 배송쿠폰이 사용되었으므로 배송비는 0원이 되어야 합니다. ", false);
 

        logger.info("배송금액 : " + DELIVERY_FEE+"원");
        
        ORDER_CALC_AMOUNT += DELIVERY_FEE;

        IAMPORT.SET_CALC_AMOUNT(ORDER_CALC_AMOUNT);

        
        logger.info("최종 계산된 주문 금액 : " + ORDER_CALC_AMOUNT+"원");


        if(!checkOrderPrice(IAMPORT))
            return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "주문 금액이 맞지 않습니다. 주문금액은 " + IAMPORT.CALC_AMOUNT + "원이 되어야 합니다. ", false);
     

        ORDER_BASKET = null;



        return GET_PAY_BEFORE_RESULT(MERCHANT_UID, "주문번호[" + IAMPORT.MERCHANT_UID+"] 결제 요청 성공", true);
    }



     
   

    /*********************** 중복 쿠폰 할인 금액 계산 ****************/
    public Integer overCouponDiscount(Integer coupon_type, Integer discount_price, Integer total_price, Integer max_price, Integer coupon_rate){

        // 중복 쿠폰 할인 금액 계산 : 정액, 정률 
        if(coupon_type == 0){
            logger.info("전체 쿠폰이 정액 쿠폰인 관계로 쿠폰 할인 금액 : " + max_price + "원이 할인 적용됩니다.");
            discount_price = max_price;
        }else if(coupon_type == 1){       
            Double price = (double)((coupon_rate * 0.01) * (total_price - discount_price));         
            if(price < max_price)
                discount_price = price.intValue();   
            else discount_price = max_price;                            // 최대 할인금액 넘지 않도록
        }

        return discount_price;
    }

    

    /*********** 쿠폰 제한 금액이 특정 금액 이상인지 체크 *******************/
    public Boolean checkCouponLimit(IMP_OVER_COUPON IMP_OVER_COUPON, Integer ORDER_CALC_AMOUNT){
        logger.info("\n*********** 쿠폰 정보 ***********");
        logger.info(IMP_OVER_COUPON.toString());
        logger.info("배송비 제외한 주문 금액 : " + ORDER_CALC_AMOUNT);
        if(IMP_OVER_COUPON.getOVER_COUPON_LIMIT_PRICE() > ORDER_CALC_AMOUNT)
            return false;
        return true;
    }






    /*********** 전체 주문 금액 계산 *******************/
    public Boolean checkDiscountPrice(IMP IAMPORT){

        Integer DIFF_DISCOUNT_PRICE = 0;

        logger.info("IAMPORT.CALC_TOTAL_DISCOUNT_PRICE" + IAMPORT.CALC_TOTAL_DISCOUNT_PRICE);
        logger.info("IAMPORT.ORDER_TOTAL_DISCOUNT_PRICE" + IAMPORT.ORDER_TOTAL_DISCOUNT_PRICE);
        

        DIFF_DISCOUNT_PRICE = IAMPORT.CALC_TOTAL_DISCOUNT_PRICE - IAMPORT.ORDER_TOTAL_DISCOUNT_PRICE;

        logger.info("DIFF_DISCOUNT_PRICE" + DIFF_DISCOUNT_PRICE);

        if(DIFF_DISCOUNT_PRICE != 0)return false;
        
        return true;
    }


   /************* 전체 주문 금액 계산 *******************/
    public Boolean checkOrderPrice(IMP IAMPORT){

        logger.info("\n*********** 입력 받은 주문금액 ***********");
        logger.info("+ [입력]배송 금액 : " + IAMPORT.ORDER_DELIVERY_FEE +"원, [입력]총 굿즈 가격 : " + IAMPORT.ORDER_TOTAL_PRICE+"원");
        logger.info("- [입력]주문시 소진 적립금 : " + IAMPORT.ORDER_USED_POINT + "원, 총 할인 금액 : " + IAMPORT.CALC_TOTAL_DISCOUNT_PRICE+"원");
        

        Integer DIFF_AMOUNT = 0;
        DIFF_AMOUNT = IAMPORT.PAYMENT_ORDER_PRICE - IAMPORT.CALC_AMOUNT;

        logger.info("* [입력]주문 금액 : " + IAMPORT.PAYMENT_ORDER_PRICE+"원");

        if(DIFF_AMOUNT != 0)return false;

        return true;
    }
 
    
 

    public resp_pay_success GET_PAY_BEFORE_RESULT(String order_number, String msg, Boolean isSuccess) throws SQLException{
        logger.info("결제 결과 : "+msg);
        resp_pay_success result = new resp_pay_success();
        result.setIsSuccess(isSuccess);
        result.setMsg(msg);
        if(!isSuccess)errorOrderFailService.insertRecord(order_number, "", msg, 0);
        return result;

    }

 
}
