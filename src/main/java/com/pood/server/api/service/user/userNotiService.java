package com.pood.server.api.service.user;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.user.dto_user_noti;

public interface userNotiService {

    public Integer insertRecord(Integer USER_IDX, String USER_UUID, int NOTI_READ, String TITLE, String TEXT, String SCHEMA, String URL, Integer NOTI_STATUS) throws SQLException;

	public Integer getUnreadMsg(Integer user_idx) throws SQLException;

    public void readAllMessage(Integer user_idx) throws SQLException;

	public List<dto_user_noti> getList(Integer user_idx) throws Exception;

    public void updateReadStatus(Integer e) throws SQLException;

    public void deleteRecordWithUserUUID(String USER_UUID) throws SQLException;
    
}
