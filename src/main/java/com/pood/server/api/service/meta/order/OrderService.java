package com.pood.server.api.service.meta.order;

import com.pood.server.dto.meta.order.dto_order;
import com.pood.server.object.IMP.IMP;
import com.pood.server.object.IMP.IMP_CANCEL;
import com.pood.server.object.IMP.IMP_RESULT_ORDER;
import com.pood.server.object.resp.resp_ava_list;
import java.sql.SQLException;
import java.util.List;

public interface OrderService {


    /************* ORDER IDX에 해당하는 ORDER NUMBER 조회 *************/
    String getOrderNumber(Integer orderIdx) throws Exception;


    /************* 주문 번호에 해당하는 레코드 항목번호 조회 *************/
    Integer getOrderINDEX(String orderNumber) throws Exception;


    /***************** 주문 상태 업데이트 *******************/
    void updateOrderStatus(Integer orderStatus, Integer orderIdx) throws SQLException;


    /****************** 주문 레코드 생성 ******************/
    Integer insertOrder(IMP iamPort) throws Exception;


    /******************** 주문 번호에 대해서 취소 정보 가져오기 ****************/
    IMP_CANCEL getCancelUnit(String orderNumber) throws Exception;

    /**************** 주문 번호에 해당하는 주문 정보 가져오기 ****************/
    IMP_RESULT_ORDER getOrderInfo(String orderNumber) throws Exception;

    /*********** 트래킹 정보 업데이트 **********/
    void updateTrackingInfo(Integer orderIdx, String trackingId) throws SQLException;

    /************* 주문 번호에 해당하는 주문 리스트 조회 ****************/
    dto_order getOrder(String orderNumber) throws Exception;


    /************* 주문 번호에 해당하는 주문 리스트 조회 ****************/
    dto_order getOrder(Integer orderIdx) throws Exception;

    /**************** 회원 항목 번호에 해당하는 주문 리스트 조회 ************/
    List<dto_order> getDeliverySuccessList(Integer userIdx) throws Exception;

    /**************** 주문 항목 개수 조회 ******************/
    Integer getTotalRecordNumber(Integer orderIdx) throws SQLException;


    // 주문 번호에 해당되는 추적 번호 조회 
    String getTrackingID(Integer orderIdx) throws Exception;

    String generateMerchantUID(String orderDevice);


    Integer getTotalUserOrderNumber(Integer userIdx) throws SQLException;

    Integer getShippingOrderCount(Integer userIdx) throws SQLException;

    List<resp_ava_list> getAVAReviewList(Integer userIdx) throws Exception;

    Boolean isFirstUserPurchase(Integer userIdx) throws SQLException;

    String GET_QUERY_UPDATE_ORDER_STATUS(Integer status, Integer orderIdx);

    String GET_QUERY_UPDATE_REFUND_AMOUNT(Integer refundAmount, String orderNumber);

    String GET_QUERY_UPDATE_SAVED_POINT(Integer toBe, String orderNumber);

    Boolean checkRetreiveTimeLimit(String orderNumber) throws Exception;
}
