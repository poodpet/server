package com.pood.server.api.service.time;

import java.text.ParseException;
import org.joda.time.DateTime;

public interface timeService {

    public long getTimeDifference(String DATE1, String DATE2) throws Exception;

    public String getCurrentTime();

    public String getCurrentTime2();

    public Boolean isExpired(String DATETIME) throws ParseException;

    public Boolean isExpired(String DATETIME1, String DATETIME2) throws ParseException;

    public Boolean isIn(String START_DATE, String END_DATE) throws ParseException;

    public Integer getDayDifference(String DATE1, String DATE2) throws Exception;

    public Integer EXCHANGE_REFUND_EXPIRED_HOURS() throws Exception;

    public String addDay(String CURRENT_TIME, int day) throws Exception;

    public String addDay(int day);

    public DateTime getDateTime(String datetime);

    public DateTime getDateTime2(String datetime);

    public String getDateTime5(String DATETIME) throws ParseException;

    public String getCurrentDate();

    public String getRetreieveTimeLimit();

    public Boolean isInToday(DateTime time);

    public boolean isInYesterDay(DateTime recordbirth) throws Exception;

    public String getRetreieveTimeYesterday() throws Exception;

}
