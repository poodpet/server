package com.pood.server.api.service.user;

import com.pood.server.dto.user.dto_user_coupon;
import com.pood.server.dto.user.dto_user_coupon_2;
import com.pood.server.object.user.vo_user_coupon;
import java.sql.SQLException;
import java.util.List;

public interface userCouponService {
    

    /* ************ 회원이 소유하고 있는 쿠폰 항목번호 반환 **************/
    public Integer getUserCouponINDEX(Integer USER_IDX, Integer COUPON_IDX) throws Exception;

    /* ************ 회원이 소유하고 있는 쿠폰 항목번호 반환 2**************/
    public List<vo_user_coupon> getUserCouponINDEXByUuid(String user_uuid) throws Exception;
    
    /******************* 회원 쿠폰 발급 횟수 조회 ******************/
    public Integer getIssuedCount(Integer COUPON_IDX, Integer USER_IDX) throws SQLException;

    
 
    /***************** 회원 쿠폰 항목 번호에 해당하는 쿠폰 정보 가져오기 ************************/
    public dto_user_coupon_2 getUserCoupon(String key, Integer value) throws Exception;




    /***************** 쿼리에 해당하는 쿠폰 목록 가져오기 ************************/
    public List<dto_user_coupon_2> getCouponList(Integer user_idx) throws Exception;




   /************* 쿠폰 매핑 정보 초기화 **********/
    public void rollbackUserCoupon(Integer uc_idx) throws SQLException;


    /************* 기존에 장바구니에 매핑되어 있는 쿠폰 초기화 ****************/
    public void rollbackBasketCoupon(Integer BASKET_IDX) throws Exception;



    /************* 주문 취소시 쿠폰 사용가능으로 돌려놓음 **********/
    public void rollbackUserCoupon2(Integer uc_idx) throws Exception;



    public String GET_QUERY_ROLLBACK_USER_COUPON(Integer uc_idx) throws Exception;



   /* ******************* 장바구니에 매핑된 쿠폰 항목 번호 조회 *****************/
    public Integer getUserCouponIndex(Integer BASKET_IDX) throws Exception;




    

    /************* 쿠폰 적용으로 사용 변경 **********/
    public void toUserCoupon(Integer BASKET_IDX, Integer USER_COUPON_IDX) throws SQLException;




   /************ 쿠폰 상태 변경 ******************/
    public void updateCouponStatus(Integer status, Integer idx) throws SQLException;




   
    /*************** 회원 쿠폰 번호에 해당하는 쿠폰 번호 조회 ***************/
    public Integer getUserCOUPONIDX(Integer USER_COUPON_IDX) throws Exception;




    
    /* ****************** 장바구니 레코드 표시 여부 업데이트 *****************/
    public void updateBasketStatus(Integer BASKET_IDX, Integer STATUS) throws SQLException;



    /* ******* 특정 장바구니에 매핑되어 있는 회원 쿠폰 정보 반환 **************/
    public dto_user_coupon getUserCoupon2(Integer BASKET_IDX) throws Exception;



	public Integer getAVAcouponCount(Integer user_idx) throws SQLException;


    public void insertRecord(Integer COUPON_IDX, Integer USER_IDX, Integer APPLY_CART_IDX, Integer STATUS, Integer OVER_TYPE,
        String AVAILABLE_TIME, String PUBLISH_TIME) throws Exception;


    public void issueWelcomeCoupon(Integer USER_IDX) throws Exception;

    
    public void deleteRecord(Integer e) throws SQLException;

    
    public void remove(Integer USER_IDX) throws SQLException;

    public void deleteUserCouponWithUserUUID(String USER_UUID) throws SQLException;

    public String GET_QUERY_TO_USER_COUPON(Integer idx, Integer uc_idx);

    public String GET_QUERY_UPDATE_COUPON_STATUS(Integer status, Integer idx);
    
}
