package com.pood.server.api.service.meta.brand;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.brand.BRAND_VISIBLE;
import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_brand_image;
import com.pood.server.dto.meta.brand.dto_brand;
import com.pood.server.dto.meta.brand.dto_brand_3;
import com.pood.server.dto.meta.brand.dto_brand_4;
import com.pood.server.dto.meta.brand.dto_brand_image;
import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.list.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("brandService")
public class brandServiceImpl implements brandService {

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    private String DEFINE_TABLE_NAME        = DATABASE.TABLE_BRAND;

    private String DEFINE_IMAGE_TABLE_NAME  = DATABASE.TABLE_BRAND_IMAGE;

    private String DEFINE_DB_NAME           = DATABASE.getDBName(DEFINE_TABLE_NAME);

 

    /************* 브랜드 항목 리스트 조회 *******************/
    @SuppressWarnings("unchecked")
    public List<dto_brand> getBrandList(pagingSet PAGING_SET, Integer BRAND_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME); 
        if(BRAND_IDX != null)
            SELECT_QUERY.buildEqual("idx", BRAND_IDX);
        SELECT_QUERY.buildEqual("brand_visible",    BRAND_VISIBLE.IS_VISIBLE);
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_brand>)listService.getDTOListWithImage2(
                SELECT_QUERY.toString(), "brand_idx", DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME, dto_brand.class, vo_brand_image.class);
          
    }

    /************* 브랜드 항목 리스트 조회 *******************/
    @SuppressWarnings("unchecked")
    public dto_brand_3 getBrandRecord(Integer BRAND_IDX) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", BRAND_IDX);
        SELECT_QUERY.buildEqual("brand_visible",    BRAND_VISIBLE.IS_VISIBLE);
        dto_brand_3 record = (dto_brand_3)listService.getDTOObject(SELECT_QUERY.toString(), dto_brand_3.class);

        if(record != null){
            SELECT_QUERY.clear();
            SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME);
            SELECT_QUERY.buildEqual("brand_idx",    BRAND_IDX);

            List<dto_brand_image> image_list = (List<dto_brand_image>)listService.getDTOList(SELECT_QUERY.toString(), dto_brand_image.class);
        
            List<vo_brand_image> brand_image_list = new ArrayList<vo_brand_image>();
                    
            for(dto_brand_image e1 : image_list){
                Integer tmp = e1.getBrand_idx();
                if(tmp.equals(BRAND_IDX)){
                    vo_brand_image record2 = new vo_brand_image();
                    record2.setImage_idx(e1.getIdx());
                    record2.setPriority(e1.getPriority());
                    record2.setRecordbirth(e1.getRecordbirth());
                    record2.setType(e1.getType());
                    record2.setUpdatetime(e1.getUpdatetime());
                    record2.setUrl(e1.getUrl());
                    record2.setVisible(e1.getVisible());
                    brand_image_list.add(record2);
                }
            }

            record.setImage(brand_image_list);

            brand_image_list = null;
        }
  
        return record;
    }



    /*************** 전체 브랜드 항목 개수 반환 ****************/
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        CNT_QUERY.buildEqual("brand_visible",    BRAND_VISIBLE.IS_VISIBLE); 
        return listService.getRecordCount(CNT_QUERY.toString());
    }


    @SuppressWarnings("unchecked")
    public List<dto_brand_4> getList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        List<dto_brand_4> list = (List<dto_brand_4>)listService.getDTOList(SELECT_QUERY.toString(), dto_brand_4.class);
        if(list != null){
            SELECT_QUERY SELECT_QUERY2 = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_IMAGE_TABLE_NAME);
            List<dto_brand_image> image_list = (List<dto_brand_image>)listService.getDTOList(SELECT_QUERY2.toString(), dto_brand_image.class);
            for(dto_brand_4 e : list){
                Integer BRAND_IDX = e.getIdx();
                
                List<vo_brand_image> brand_image_list = new ArrayList<vo_brand_image>();

                for(dto_brand_image e1 : image_list){
                    Integer tmp = e1.getBrand_idx();
                    if(tmp.equals(BRAND_IDX)){
                        vo_brand_image record = new vo_brand_image();
                        record.setImage_idx(e1.getIdx());
                        record.setPriority(e1.getPriority());
                        record.setRecordbirth(e1.getRecordbirth());
                        record.setType(e1.getType());
                        record.setUpdatetime(e1.getUpdatetime());
                        record.setUrl(e1.getUrl());
                        record.setVisible(e1.getVisible());
                        brand_image_list.add(record);
                    }
                }

                e.setBrand_image(brand_image_list);

                brand_image_list = null;
            }
        }
        return list;
    }
 
 
} 
 