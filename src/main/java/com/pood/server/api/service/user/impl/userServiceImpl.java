package com.pood.server.api.service.user.impl;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.req.header.user.user_info.hUser_info_4;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.log.logUserPointService;
import com.pood.server.api.service.log.logUserSavePointService;
import com.pood.server.api.service.log.logUserUsePointService;
import com.pood.server.api.service.query.queryService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.api.service.sns.snsService;
import com.pood.server.api.service.time.timeService;
import com.pood.server.api.service.user.userBasketService;
import com.pood.server.api.service.user.userCardService;
import com.pood.server.api.service.user.userCouponService;
import com.pood.server.api.service.user.userDeliveryService;
import com.pood.server.api.service.user.userLoginService;
import com.pood.server.api.service.user.userNotiService;
import com.pood.server.api.service.user.userPetAIFeedService;
import com.pood.server.api.service.user.userPetAIService;
import com.pood.server.api.service.user.userPetAllergyService;
import com.pood.server.api.service.user.userPetFeedService;
import com.pood.server.api.service.user.userPetGrainSizeService;
import com.pood.server.api.service.user.userPetService;
import com.pood.server.api.service.user.userPetSickService;
import com.pood.server.api.service.user.userPointService;
import com.pood.server.api.service.user.userReferralCodeService;
import com.pood.server.api.service.user.userService;
import com.pood.server.api.service.user.userWishService;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.user.USER_STATUS;
import com.pood.server.dto.meta.dto_user_info;
import com.pood.server.dto.user.dto_user_1;
import com.pood.server.dto.user.pet.dto_user_pet;
import com.pood.server.object.pagingSet;
import com.pood.server.object.user.vo_user;
import com.pood.server.object.user.vo_user_base_image;
import com.pood.server.object.user.vo_user_info;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("userService")
public class userServiceImpl implements userService {

    Logger logger = LoggerFactory.getLogger(userServiceImpl.class);

    @Autowired
    @Qualifier("snsService")
    snsService snsService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("userLoginService")
    userLoginService userLoginService;

    @Autowired
    @Qualifier("userBasketService")
    userBasketService userBasketService;

    @Autowired
    @Qualifier("userCouponService")
    userCouponService userCouponService;

    @Autowired
    @Qualifier("userDeliveryService")
    userDeliveryService userDeliveryService;

    @Autowired
    @Qualifier("userNotiService")
    userNotiService userNotiService;

    @Autowired
    @Qualifier("userCardService")
    userCardService userCardService;

    @Autowired
    @Qualifier("userWishService")
    userWishService userWishService;

    @Autowired
    @Qualifier("userReferralCodeService")
    userReferralCodeService userReferralCodeService;

    @Autowired
    @Qualifier("userPointService")
    userPointService userPointService;

    @Autowired
    @Qualifier("userPetService")
    userPetService userPetService;

    @Autowired
    @Qualifier("userPetAIService")
    userPetAIService userPetAIService;

    @Autowired
    @Qualifier("userPetAllergyService")
    userPetAllergyService userPetAllergyService;

    @Autowired
    @Qualifier("userPetFeedService")
    userPetFeedService userPetFeedService;

    @Autowired
    @Qualifier("userPetGrainSizeService")
    userPetGrainSizeService userPetGrainSizeService;

    @Autowired
    @Qualifier("userPetSickService")
    userPetSickService userPetSickService;

    @Autowired
    @Qualifier("userPetAIFeedService")
    userPetAIFeedService userPetAIFeedService;

    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("timeService")
    timeService timeService;

    @Autowired
    @Qualifier("logUserSavePointService")
    logUserSavePointService logUserSavePointService;

    @Autowired
    @Qualifier("logUserUsePointService")
    logUserUsePointService logUserUsePointService;

    @Autowired
    @Qualifier("logUserPointService")
    logUserPointService logUserPointService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME = DATABASE.TABLE_USER_INFO;

    private String DEFINE_DB_NAME = DATABASE.getDBName(DEFINE_TABLE_NAME);

    /************** 회원 UUID에 해당하는 회원 항목 번호 조회 **************/
    public Integer getUserIDX(String user_uuid) throws Exception {

        Integer USER_IDX = null;

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", user_uuid);

        vo_user record = (vo_user) listService.getDTOObject(SELECT_QUERY.toString(), vo_user.class);
        if (record != null) {
            USER_IDX = record.getIdx();
        }
        record = null;

        return USER_IDX;
    }

    /************** 회원 항목 번호에 해당하는 회원 UUID 조회 **************/
    public String getUserUUID(Integer userIdx) throws Exception {
        String USER_UUID = "";
        //select * from user_db.user_info where idx = userIdx;
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", userIdx);

        vo_user record = (vo_user) listService.getDTOObject(SELECT_QUERY.toString(), vo_user.class);
        if (record != null) {
            USER_UUID = record.getUser_uuid();
        }
        record = null;

        return USER_UUID;
    }

    /************* 회원 이메일에 해당하는 회원 정보 조회 ****************/
    public vo_user getUser(String key, String value) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual(key, value);
        return (vo_user) listService.getDTOObject(SELECT_QUERY.toString(), vo_user.class);
    }

    public dto_user_1 getUser2(String key, String value, Integer login_type) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual(key, value);
        dto_user_1 record = (dto_user_1) listService.getDTOObject(SELECT_QUERY.toString(),
            dto_user_1.class);
        if (record != null) {
            record.setUser_login_type(login_type);
        }
        return record;
    }

    /*************** 회원 목록 조회 ************************/
    @SuppressWarnings("unchecked")
    public List<dto_user_info> getUserList(pagingSet PAGING_SET) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_user_info>) listService.getDTOList(SELECT_QUERY.toString(),
            dto_user_info.class);
    }

    /*************** 회원 목록 개수 조회 ************************/
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    /********************** 회원 항목 추가 *********************/
    public Integer insertRecord(
        String USER_EMAIL,
        String USER_PASSWORD,
        Boolean USER_SERVICE_AGREE,
        String USER_UUID,
        String ML_NAME,
        Float ML_RATE,
        Integer ML_PRICE,
        Integer ML_MONTH,
        String USER_PHONE_NUMBER,
        String USER_NAME) throws Exception {

        // 회원 가입시 추천인 코드 생성
        String REFERRAL_CODE = "";
        while (true) {
            String text = generateReferralCode();
            Integer REFERRAL_USER = findReferralUser(text);
            if (REFERRAL_USER == null) {
                REFERRAL_CODE = text;
                break;
            }
        }

        String CURRENT_TIME = timeService.getCurrentTime();

        /////////////////////////////////////////////
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_email", USER_EMAIL);
        hashMap.put("user_password", USER_PASSWORD);
        hashMap.put("user_service_agree", USER_SERVICE_AGREE);
        hashMap.put("user_uuid", USER_UUID);
        hashMap.put("user_nickname", USER_NAME);
        hashMap.put("ml_name", ML_NAME);
        hashMap.put("ml_rate", ML_RATE);
        hashMap.put("ml_price", ML_PRICE);
        hashMap.put("ml_month", ML_MONTH);
        hashMap.put("user_phone", USER_PHONE_NUMBER);
        hashMap.put("referral_code", REFERRAL_CODE);
        hashMap.put("user_point", 0);
        hashMap.put("user_name", USER_NAME);
        hashMap.put("user_status", USER_STATUS.DEFAULT);
        hashMap.put("pood_push", 1);
        hashMap.put("pood_push_cg_time", CURRENT_TIME);
        hashMap.put("order_push", 1);
        hashMap.put("order_push_cg_time", CURRENT_TIME);
        hashMap.put("service_push", 1);
        hashMap.put("service_push_cg_time", CURRENT_TIME);
        hashMap.put("status_update_date", CURRENT_TIME);
        //랜덤 1~9
        List<Long> countUserBaseImg = userService.getCountUserBaseImg();

        Collections.shuffle(countUserBaseImg);
        Long aLong = countUserBaseImg.stream().findFirst().orElse(null);
        hashMap.put("user_base_image_idx", aLong);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap,
            com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }

    /************************ 회원 닉네임 발급 ********************/
    public String getUserNickname(String USER_EMAIL) {
        String USER_NICKNAME = "";
        String EMAIL_PARSER[] = USER_EMAIL.split("@");
        String FRONT_STRING = EMAIL_PARSER[0];
        int FRONT_STRING_LENGTH = FRONT_STRING.length();

        if (FRONT_STRING_LENGTH == 1) {
            USER_NICKNAME = FRONT_STRING;
        } else if (FRONT_STRING_LENGTH >= 2) {

            if (FRONT_STRING_LENGTH >= 6) {
                FRONT_STRING = FRONT_STRING.substring(0, 6);
                FRONT_STRING_LENGTH = 6;
            }

            StringBuilder str = new StringBuilder(FRONT_STRING);
            Integer MASK_DELIMITER = (Integer) (FRONT_STRING_LENGTH / 2);
            Integer pointer = FRONT_STRING_LENGTH - 1;
            Integer cnt = 0;

            while (true) {
                str.setCharAt(pointer--, '*');
                if (++cnt == MASK_DELIMITER) {
                    break;
                }
            }

            USER_NICKNAME = str.toString();
        }

        return USER_NICKNAME;
    }

    /*********************** 회원 항목 정보 업데이트 ****************/
    public void updateUserRecord(hUser_info_4 header) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();

        hashMap.put("user_nickname", header.getUser_nickname());
        hashMap.put("user_email", header.getUser_email());
        hashMap.put("user_password", header.getUser_password());
        hashMap.put("user_service_agree", header.getUser_service_agree());
        hashMap.put("user_status", header.getUser_status());
        hashMap.put("user_name", header.getUser_name());
        hashMap.put("ml_name", header.getMl_name());
        hashMap.put("ml_rate", header.getMl_rate());
        hashMap.put("user_phone", header.getUser_phone());

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap,
            "user_uuid", header.getUser_uuid());
        updateService.execute(query);
        query = null;

    }

    /****************** 회원 비밀번호 업데이트 *************************/
    public void updateUserPassword(Integer USER_IDX, String USER_PASSWORD) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_password", USER_PASSWORD);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap,
            "idx", USER_IDX.toString());
        updateService.execute(query);
        query = null;

    }

    /************* 회원 UUID에 해당하는 회원 정보 조회 ****************/
    @SuppressWarnings("unchecked")
    public List<vo_user_info> getUser2(String user_uuid) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if (user_uuid != null) {
            SELECT_QUERY.buildEqual("user_uuid", user_uuid);
        }
        return (List<vo_user_info>) listService.getDTOList(SELECT_QUERY.toString(),
            vo_user_info.class);
    }


    /************* 해당 회원이 있는지 없는지 조회 ****************/
    public boolean isExist(String key, String value) throws Exception {
        Boolean isExist = false;
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual(key, value);
        vo_user record = (vo_user) listService.getDTOObject(SELECT_QUERY.toString(), vo_user.class);
        if (record != null) {
            isExist = true;
        } else {
            isExist = false;
        }
        return isExist;
    }

    @Override
    public dto_user_1 getUserRecord(String user_uuid, Integer login_type) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        if (user_uuid != null) {
            SELECT_QUERY.buildEqual("user_uuid", user_uuid);
        }
        dto_user_1 record = (dto_user_1) listService.getDTOObject(SELECT_QUERY.toString(),
            dto_user_1.class);
        record.setUser_login_type(login_type);
        return record;
    }

    // 회원 상태 업데이트
    public void updateUserStatus(String USER_UUID, Integer STATUS) throws SQLException {
        String CURRENT_TIME = timeService.getCurrentTime();
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_status", STATUS);
        hashMap.put("status_update_date", CURRENT_TIME);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap,
            "user_uuid", USER_UUID);
        updateService.execute(query);
        query = null;

    }

    @Override
    public Integer findReferralUser(String REFERRAL_CODE) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("referral_code", REFERRAL_CODE);
        vo_user record = (vo_user) listService.getDTOObject(SELECT_QUERY.toString(), vo_user.class);
        if (Objects.isNull(record)) {
            return null;
        }
        return record.getIdx();
    }

    @Override
    public Integer validation(dto_user_1 USER, Integer type, String value) throws Exception {
        // 회원이 없는 회원인 경우 203 에러
        if (USER == null) {
            return 203;
        }

        // 만약 타입이 1인 경우 UUID로 비교
        if (type == 1) {
            if (!value.equals(USER.getUser_uuid())) {
                return 205; // UUID 매칭이 안되는 경우 205 에러
            }
        }

        // 만약 타입이 2인 경우 패스워드로 비교
        if (type == 2) {
            String USER_PASSWORD = getUserPassword(USER.getUser_uuid());
            logger.info("USER_PASSWORD" + USER_PASSWORD);
            logger.info("value" + value);
            if (!value.equals(USER_PASSWORD)) {
                return 206; // 패스워드 매칭이 안되는 경우 206 에러
            }
        }

        Integer user_status = USER.getUser_status();

        String USER_UUID = USER.getUser_uuid();

        if (user_status.equals(USER_STATUS.TO_RESIGN) || user_status.equals(USER_STATUS.RESIGNED)
            || user_status.equals(USER_STATUS.DEACTIVE)) {
            return 224;
        }

        if (user_status == USER_STATUS.POLICY_VIOLATION) {
            return 225;
        }

        String DATE_LAST_LOGIN = userLoginService.getLastDateTime(USER.getIdx());

        String DATE_CURRENT_TIME = timeService.getCurrentTime();

        if (DATE_LAST_LOGIN.equals("") || DATE_LAST_LOGIN == null) {
            DATE_LAST_LOGIN = timeService.getCurrentTime();
        }

        long DIFF_SECONDS = timeService.getTimeDifference(DATE_LAST_LOGIN, DATE_CURRENT_TIME);

        if (DIFF_SECONDS >= 31536000) {

            // 마지막 로그인 시접과 1년 이상 차이가 날 경우 휴면 계정으로 전환
            userService.updateUserStatus(USER_UUID, USER_STATUS.DORMANT_ACCOUNT);

            return 226;

        }

        return 200;
    }

    @Override
    public String generateReferralCode() {
        StringBuilder recommendCode = new StringBuilder();

        ThreadLocalRandom random = ThreadLocalRandom.current();
        final int numNumber = random.nextInt(6) + 1;
        int alphabetNumber = (6 - numNumber);

        String number = "0123456789";
        String alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        for (int i = 1; i <= numNumber; i++) {
            recommendCode.append(number.charAt(random.nextInt(number.length())));
        }

        for (int i = 0; i < alphabetNumber; i++) {
            recommendCode.append(alphabet.charAt(random.nextInt(alphabet.length())));
        }

        return recommendCode.toString();
    }

    // 특정 회원의 닉네임 조회
    public String getUserNickname(Integer user_idx) throws Exception {
        String user_nickname = "";
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", user_idx);
        vo_user record = (vo_user) listService.getDTOObject(SELECT_QUERY.toString(), vo_user.class);
        if (record != null) {
            user_nickname = record.getUser_nickname();
        }
        record = null;
        return user_nickname;
    }

    // 특정 회원의 핸드폰 번호 조회
    public String getUserPhoneNumber(Integer user_idx) throws Exception {
        String user_phone = "";
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", user_idx);
        vo_user record = (vo_user) listService.getDTOObject(SELECT_QUERY.toString(), vo_user.class);
        if (record != null) {
            user_phone = record.getUser_phone();
        }
        record = null;
        return user_phone;
    }

    @Override
    public Integer getOrderPush(String USER_UUID) throws Exception {
        Integer order_push = 0;
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", USER_UUID);
        vo_user record = (vo_user) listService.getDTOObject(SELECT_QUERY.toString(), vo_user.class);
        if (record != null) {
            if (record.getOrder_push() == true) {
                order_push = 1;
            } else {
                order_push = 0;
            }
        }
        record = null;
        return order_push;
    }


    public String getTokenARN(Integer USER_IDX) throws Exception {
        String TOKEN_ARN = "";
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", USER_IDX);
        vo_user record = (vo_user) listService.getDTOObject(SELECT_QUERY.toString(), vo_user.class);
        if (record != null) {
            TOKEN_ARN = record.getToken_arn();
        }
        record = null;
        return TOKEN_ARN;
    }

    /******************* 특정 회원의 FCM을 신규로 업데이트 합니다. ***********************/
    public void updateUserFCM(Integer USER_IDX, String DEVICE_TOKEN, Integer DEVICE_TYPE)
        throws SQLException {
        String SNS_TOKEN_ARN = snsService.createEndpoint(DEVICE_TOKEN,
            com.pood.server.config.AWS.SNS_PLATFORM_ARN);
        snsService.subscribe(SNS_TOKEN_ARN, com.pood.server.config.AWS.SNS_TOPIC_ARN);

        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("device_key", DEVICE_TOKEN);
        hashMap.put("device_type", DEVICE_TYPE);
        hashMap.put("token_arn", SNS_TOKEN_ARN);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap,
            "idx", USER_IDX.toString());
        updateService.execute(query);
        query = null;
    }

    @Override
    public Integer getUserPoint(String user_uuid) throws Exception {
        Integer user_point = 0;
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", user_uuid);
        vo_user record = (vo_user) listService.getDTOObject(SELECT_QUERY.toString(), vo_user.class);
        if (record != null) {
            user_point = record.getUser_point();
        }
        record = null;
        return user_point;
    }

    @Override
    public void saveUserPoint(String user_uuid, Integer point) throws Exception {
        updateService.execute(GET_QUERY_SAVE_USER_POINT(user_uuid, point));
    }


    @Override
    public void useUserPoint(String user_uuid, Integer user_point) throws Exception {
        updateService.execute(GET_QUERY_USE_USER_POINT(user_uuid, user_point));
    }

    @Override
    public String getUserPassword(String USER_UUID) throws Exception {
        String USER_PASSWORD = "";
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", USER_UUID);
        vo_user record = (vo_user) listService.getDTOObject(SELECT_QUERY.toString(), vo_user.class);
        if (record != null) {
            USER_PASSWORD = record.getUser_password();
        }
        record = null;

        return USER_PASSWORD;
    }

    @Override
    public void passwordUpdate(String NEW_PASSWORD, String USER_UUID) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_password", NEW_PASSWORD);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap,
            "user_uuid", USER_UUID);
        updateService.execute(query);
        query = null;
    }

    @Override
    public Boolean checkUser(String USER_NAME, String USER_EMAIL, String USER_PHONE)
        throws Exception {
        Boolean chk = false;

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildLike("user_name", USER_NAME);
        SELECT_QUERY.buildEqual("user_email", USER_EMAIL);
        SELECT_QUERY.buildEqual("user_phone", USER_PHONE);
        vo_user record = (vo_user) listService.getDTOObject(SELECT_QUERY.toString(), vo_user.class);
        if (record != null) {
            chk = true;
        }
        record = null;

        return chk;
    }

    public dto_user_info getUserUUID(String user_name, String user_phone) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_name", user_name);
        SELECT_QUERY.buildEqual("user_phone", user_phone);
        return (dto_user_info) listService.getDTOObject(SELECT_QUERY.toString(),
            dto_user_info.class);
    }

    @Override
    public Boolean checkUser(String USER_NAME, String USER_PHONE) throws Exception {
        Boolean chk = false;

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildLike("user_name", USER_NAME);
        SELECT_QUERY.buildEqual("user_phone", USER_PHONE);

        vo_user record = (vo_user) listService.getDTOObject(SELECT_QUERY.toString(), vo_user.class);
        if (record != null) {
            chk = true;
        }
        record = null;

        return chk;
    }

    @Override
    public void updatePushInfo(Boolean POOD_PUSH, Boolean ORDER_PUSH, Boolean SERVICE_PUSH,
        Boolean dogPoodPush, Boolean catPoodPush, String USER_UUID)
        throws SQLException {
        String CURRENT_TIME = timeService.getCurrentTime();

        if (dogPoodPush && catPoodPush) {
            POOD_PUSH = Boolean.TRUE;
        } else if (!dogPoodPush && !catPoodPush) {
            POOD_PUSH = Boolean.FALSE;
        }
        // 회원 푸쉬 알림 설정 업데이트
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("pood_push", POOD_PUSH);
        hashMap.put("pood_push_cg_time", CURRENT_TIME);
        hashMap.put("order_push", ORDER_PUSH);
        hashMap.put("order_push_cg_time", CURRENT_TIME);
        hashMap.put("service_push", ORDER_PUSH);
        hashMap.put("service_push_cg_time", CURRENT_TIME);

        hashMap.put("dog_pood_push", dogPoodPush);
        hashMap.put("dog_pood_push_cg_time", CURRENT_TIME);
        hashMap.put("cat_pood_push", catPoodPush);
        hashMap.put("cat_pood_push_cg_time", CURRENT_TIME);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap,
            "user_uuid", USER_UUID);
        updateService.execute(query);
        query = null;

        CURRENT_TIME = null;

    }

    @Override
    public Integer getPoodPush(String USER_UUID) throws Exception {
        Integer pood_push = 0;
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", USER_UUID);
        vo_user record = (vo_user) listService.getDTOObject(SELECT_QUERY.toString(), vo_user.class);
        if (record != null) {
            if (record.getPood_push() == true) {
                pood_push = 1;
            } else {
                pood_push = 0;
            }
        }
        record = null;
        return pood_push;
    }

    @Override
    public Integer getServicePush(String USER_UUID) throws Exception {
        Integer service_push = 0;
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", USER_UUID);
        vo_user record = (vo_user) listService.getDTOObject(SELECT_QUERY.toString(), vo_user.class);
        if (record != null) {
            if (record.getService_push() == true) {
                service_push = 1;
            } else {
                service_push = 0;
            }
        }
        record = null;
        return service_push;
    }

    @Override
    public void userPointInit(String USER_UUID) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_point", 0);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap,
            "user_uuid", USER_UUID);
        updateService.execute(query);
        query = null;

    }


    public void deleteRecord(String USER_UUID) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_uuid", USER_UUID);
        updateService.execute(DELETE_QUERY.toString());
    }

    @Override
    public void userInit(String USER_UUID) throws Exception {
        Integer USER_IDX = getUserIDX(USER_UUID);

        if (USER_IDX != null) {

            // 회원 포인트 지급 내역 삭제 0으로 업데이트
            userService.userPointInit(USER_UUID);
            // 회원 장바구니 목록 삭제 삭제
            userBasketService.deleteUserBasketWithUserUUID(USER_UUID);
            // 회원 쿠폰 목록 삭제
            userCouponService.deleteUserCouponWithUserUUID(USER_UUID);
            // 회원 배송지 삭제
            userDeliveryService.deleteRecordWithUSER_IDX(USER_IDX);
            // 회원 알림 삭제
            userNotiService.deleteRecordWithUserUUID(USER_UUID);
            // 회원 카드 삭제
            userCardService.deleteRecordWithUserIDX(USER_IDX);
            // 회원 찜 삭제
            userWishService.deleteRecordWithUSERIDX(USER_IDX);
            // 회원 추천인 코드 삭제
            userReferralCodeService.deleteRecordWithUSERIDX(USER_IDX);
            // 회원 포인트 삭제
            userPointService.deleteRecordWithUserUUID(USER_UUID);
            // 회원 포인트 0원
            userService.userPointInit(USER_UUID);
            // 회원 포인트 발급 내역 삭제
            userPointService.init(USER_UUID);
            // 회원 펫 삭제
            List<dto_user_pet> list = userPetService.getList(USER_IDX);

            if (list != null) {
                for (dto_user_pet e : list) {
                    Integer up_idx = e.getIdx();

                    // 테이블 : USER_PET
                    userPetService.deleteRecord(up_idx);

                    // 테이블 : USER_PET_IMAGE
                    userPetService.deleteImageWithUserPetIDX(up_idx);

                    // 테이블 : USER_PET_AI_DIAGNOSIS
                    userPetAIService.deleteRecordWithUserPetIDX(up_idx);

                    // 테이블 : USER_PET_SICK
                    userPetSickService.deleteRecordWithUserPetIDX(up_idx);

                    // 테이블 : USER_PET_FEED
                    userPetFeedService.deleteRecordWithUserPetIDX(up_idx);

                    // 테이블 : USER_PET_GRAIN_SIZE
                    userPetGrainSizeService.deleteRecordWithUserPetIDX(up_idx);

                    // 테이블 : USER_PET_ALLERGY
                    userPetAllergyService.deleteRecordWithUserPetIDX(up_idx);

                    // 테이블 : USER_PET_AI_FEED
                    userPetAIFeedService.deleteRecordWithUserPetIDX(up_idx);

                }
            }
        }

    }

    public String GET_QUERY_SAVE_USER_POINT(String user_uuid, Integer point) {
        return queryService.getAddValueQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, "user_point", point,
            "user_uuid", user_uuid);
    }

    @Override
    public String GET_QUERY_USE_USER_POINT(String user_uuid, Integer user_point) {
        return queryService.getDeductValueQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, "user_point",
            user_point, "user_uuid", user_uuid);
    }

    @Override
    public String getUserBaseImg(Long idx) throws Exception {

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME,
            DATABASE.TABLE_USER_BASE_IMAGE);
        SELECT_QUERY.buildEqual("idx", idx.intValue());

        vo_user_base_image record = (vo_user_base_image) listService.getDTOObject(
            SELECT_QUERY.toString(), vo_user_base_image.class);
        if (record != null) {
            return record.getUrl();
        }
        return null;
    }

    ;

    @Override
    public List<Long> getCountUserBaseImg() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME,
            DATABASE.TABLE_USER_BASE_IMAGE);
        List<vo_user_base_image> result = (List<vo_user_base_image>) listService.getDTOList(
            SELECT_QUERY.toString(), vo_user_base_image.class);
//        result.stream().map(x -> x.getIdx()).collect(Collectors.toList())
        return result.stream().map(x -> x.getIdx()).collect(Collectors.toList());
    }

    @Override
    public String getDeviceKey(Integer userIdx) throws Exception {
        String deviceKey = "";

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", userIdx);

        vo_user record = (vo_user) listService.getDTOObject(SELECT_QUERY.toString(), vo_user.class);
        if (record != null) {
            deviceKey = record.getDevice_key();
        }
        record = null;

        return deviceKey;
    }

}
