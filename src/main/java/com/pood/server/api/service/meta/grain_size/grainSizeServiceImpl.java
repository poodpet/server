package com.pood.server.api.service.meta.grain_size;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_grain_size;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.config.DATABASE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("grainSizeService")
public class grainSizeServiceImpl implements grainSizeService{
    
    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_GRAIN_SIZE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    /**************** 곡물 사이즈 목록 조회 *****************/
    @SuppressWarnings("unchecked")
    public List<dto_grain_size> getList(pagingSet PAGING_SET) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return (List<dto_grain_size>)listService.getDTOList(SELECT_QUERY.toString(), dto_grain_size.class);
    }

    /**************** 곡물 사이즈 목록 항목 개수 조회 *****************/
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    @Override
    public dto_grain_size getRecord(Integer idx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("idx", idx);
        return (dto_grain_size)listService.getDTOObject(SELECT_QUERY.toString(), dto_grain_size.class);
    }
    
}
