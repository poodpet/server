package com.pood.server.api.service.meta.nuti;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_nuti;
import com.pood.server.dto.meta.dto_nuti;

public interface nutiService {

    /**************** 영양소 목록 조회 *****************/
    public List<dto_nuti> getNutiList(pagingSet PAGING_SET) throws Exception;

    /**************** 전체 목록 개수 조회 ******************/
    public Integer getTotalRecordNumber() throws SQLException;

    public List<vo_nuti> getObjectList() throws Exception;
    
}
