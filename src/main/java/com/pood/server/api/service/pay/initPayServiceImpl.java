package com.pood.server.api.service.pay;

import com.pood.server.api.service.meta.promotion.promotionGroupGoodsService;
import com.pood.server.object.meta.vo_promotion_group_goods;
import com.pood.server.service.OrderBasketServiceV2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.pood.server.config.PROTOCOL;

import java.util.ArrayList;
import java.util.List;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.user.*;
import com.pood.server.api.service.delivery.remoteDeliveryService;
import com.pood.server.api.service.list.*;
import com.pood.server.object.IMP.*;
import com.pood.server.api.service.meta.coupon.*;
import com.pood.server.api.service.meta.goods.*;
import com.pood.server.api.service.meta.order.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.pood.server.api.service.time.*;
import com.pood.server.api.service.sns.*;
import com.pood.server.api.service.log.*;
import com.pood.server.api.service.error.*;
import com.pood.server.api.service.noti.*;
import com.pood.server.api.service.pood_point.*;
import com.pood.server.api.service.meta.point.*;
import com.pood.server.config.meta.order.ORDER_STATUS;

@Service("initPayService")
public class initPayServiceImpl implements initPayService{

    Logger logger = LoggerFactory.getLogger(initPayServiceImpl.class);

    @Autowired
    @Qualifier("orderBasketService")
    public orderBasketService orderBasketService;

    @Autowired
    @Qualifier("timeService")
    timeService timeService;

    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;

    @Autowired
    @Qualifier("goodsCouponService")
    goodsCouponService goodsCouponService;

    @Autowired
    @Qualifier("goodsProductService")
    goodsProductService goodsProductService;

    @Autowired
    @Qualifier("couponService")
    couponService couponService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("orderDeliveryService")
    orderDeliveryService orderDeliveryService;
    
    @Autowired
    @Qualifier("orderService")
    OrderService orderService;

    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("userCouponService")
    userCouponService userCouponService;

    @Autowired
    @Qualifier("userBasketService")
    userBasketService userBasketService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("logUserOrderService")
    logUserOrderService logUserOrderService;

    @Autowired
    @Qualifier("userTokenService")
    userTokenService userTokenService;

    @Autowired
    @Qualifier("pushService")
    public pushService pushService;

    @Autowired
    @Qualifier("snsService")
    public snsService snsService;

    @Autowired
    @Qualifier("logUserSavePointService")
    logUserSavePointService logUserSavePointService;

    @Autowired
    @Qualifier("logUserUsePointService")
    logUserUsePointService logUserUsePointService;
   
    @Autowired
    @Qualifier("pointService")
    pointService pointService;

    @Autowired
    @Qualifier("retreieveService")
    retreieveService retreieveService;

    @Autowired
    @Qualifier("poodPointService")
    poodPointService poodPointService;

    @Autowired
    @Qualifier("remoteDeliveryService")
    remoteDeliveryService remoteDeliveryService;

    @Autowired
    @Qualifier("logUserOrderBasketService")
    logUserOrderBasketService logUserOrderBasketService;


    @Autowired
    @Qualifier("errorOrderFailService")
    errorOrderFailService errorOrderFailService;

    @Autowired
    @Qualifier("promotionGroupGoodsService")
    promotionGroupGoodsService promotionGroupGoodsService;

    @Autowired
    OrderBasketServiceV2 orderBasketServiceV2;

    /****************** 주문 정보 삽입 **********************/
    public Integer insertIMPorder(IMP IAMPORT) throws Exception {

        List<IMP_SHOP> SHOPPINGBAG_DATA = IAMPORT.order.getShoppingbag_data();
        

        
        Integer ORDER_IDX = orderService.insertOrder(IAMPORT);

        if(ORDER_IDX == -1)return -1;


        // 주문 레코드 생성 
        IAMPORT.SET_ORDER_IDX(ORDER_IDX);



        List<String> transaction_process = new ArrayList<String>();


        

        // 주문 배송지 레코드 생성 
        String process_1 = orderDeliveryService.GET_QUERY_INSERT_ORDER_DELIVERY(IAMPORT);

        // 주문 히스토리 레코드 생성  
        String process_2 = logUserOrderService.GET_QUERY_INSERT_ORDER_HISTORY(
            IAMPORT.ORDER_IDX, ORDER_STATUS.ORDER_READY, IAMPORT.MERCHANT_UID, IAMPORT.MERCHANT_UID,  IAMPORT.USER_UUID, -1, -1);

        transaction_process.add(process_1);

        transaction_process.add(process_2);




        for(IMP_SHOP e1 : SHOPPINGBAG_DATA){
            
            
            // 회원 쿠폰 장바구니 항목 번호 저장 
            Integer USER_COUPON_IDX = userCouponService.getUserCouponINDEX(IAMPORT.USER_IDX, e1.getCoupon_idx());


            String process_3 = userCouponService.GET_QUERY_TO_USER_COUPON(e1.getIdx(), USER_COUPON_IDX);
            Integer pr_discount_rate = 0;
            if(e1.getPr_code().equals(-1)){
                pr_discount_rate = 0;
            }else{
                vo_promotion_group_goods promotionGoodsInfo = promotionGroupGoodsService.getPromotionGoodsInfo(e1.getPr_code(), e1.getgoods_idx());
                 pr_discount_rate = promotionGoodsInfo.getPr_discount_rate();
            }
            orderBasketServiceV2.insertBasketInfo(e1, IAMPORT.ORDER_IDX, IAMPORT.MERCHANT_UID,0, pr_discount_rate);

            String process_4 = orderBasketService.GET_QUERY_INSERT_BASKET_DATA(e1, IAMPORT.ORDER_IDX, IAMPORT.MERCHANT_UID,0, pr_discount_rate);

            transaction_process.add(process_3);

//            transaction_process.add(process_4);

            process_3 = null;

//            process_4 = null;


        }

        process_1 = null;

        process_2 = null;
 

        updateService.commit(transaction_process);


        return 200;
    }

       
 

    @Override
    public IMP initIMP(String param) throws Exception {
        IMP IAMPORT = new IMP();
        IAMPORT.setIMP(param);

        String MERCHANT_UID         = orderService.generateMerchantUID(IAMPORT.ORDER_DEVICE);

        Integer REMOTE_TYPE_VALUE   = remoteDeliveryService.getRemoteType(IAMPORT.DELIVERY_ZIPCODE);

        Integer USER_IDX            = userService.getUserIDX(IAMPORT.USER_UUID);

        IAMPORT.SET_DELIVERY_REMOTE_TYPE(REMOTE_TYPE_VALUE);
        IAMPORT.setUser_idx(USER_IDX);
        IAMPORT.SET_MERCHANT_UID(MERCHANT_UID);

        return IAMPORT;
    }


    @Override
    public Integer tokenCheck(String TOKEN, String USER_UUID) throws Exception{

        logger.info("*************** PAYMENT PROCESS ***************");
        logger.info("PAYMENT TOKEN AUTHENTICATION - USER UUID : " + USER_UUID+", TOKEN : " + TOKEN);
        
        if(TOKEN == null)
            return 248;

        if(TOKEN.equals(PROTOCOL.ADMIN_TOKEN))
            return 200;

        if(USER_UUID == null)
            return 248;
        

        Boolean chk = userTokenService.authentication(USER_UUID, TOKEN);

        if(!chk)return 248;
            
        return 200;
    }
  
 
}
