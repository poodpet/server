package com.pood.server.api.service.user.impl;

import com.pood.server.api.service.user.userPointService;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Qualifier;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List; 
import java.util.Map;
import java.util.UUID;

import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.query.*;
import com.pood.server.api.service.time.*;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.point.META_POINT_TYPE;
import com.pood.server.config.meta.user.USER_POINT;
import com.pood.server.dto.user.dto_user_point;

@Service("userPointService")
public class userPointServiceImpl implements userPointService{

    Logger logger = LoggerFactory.getLogger(userPointServiceImpl.class);

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;
    
    @Autowired
    @Qualifier("timeService")
    timeService timeService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;
    
    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_USER_POINT;
    
    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @Override
    public String insertRecord(
        String  USER_UUID,          // 회원 UUID
        String  ORDER_NUMBER,       // 구매 적립금인 경우 주문 번호
        Integer REVIEW_IDX,         // 리뷰 적립금인 경우 리뷰 항목 번호
        Integer POINT_IDX,          // 포인트 항목 번호
        String  POINT_NAME,         // 포인트 이름
        Integer POINT_TYPE,         // 0:적용 금액, 1: 적용 퍼센트
        Integer POINT_PRICE,        // 포인트 금액 
        Integer POINT_RATE,         // 포인트 비율
        String  EXPIRED_DATE,       // 포인트 만료기간
        Integer STATUS,             // 포인트 상태 
        Integer SAVED_POINT,        // 적립된 포인트
        Integer USED_POINT          // 사용되었으면 사용된 포인트
        ) throws SQLException {
        
        String POINT_UUID = UUID.randomUUID().toString();
        updateService.insert(GET_QUERY_INSERT_RECORD(USER_UUID, ORDER_NUMBER, REVIEW_IDX, POINT_IDX, POINT_NAME, POINT_TYPE, POINT_PRICE, POINT_RATE, EXPIRED_DATE, STATUS, SAVED_POINT, USED_POINT, POINT_UUID));
        return POINT_UUID;
    } 

    public String GET_QUERY_INSERT_RECORD(
        String  USER_UUID,          // 회원 UUID
        String  ORDER_NUMBER,       // 구매 적립금인 경우 주문 번호
        Integer REVIEW_IDX,         // 리뷰 적립금인 경우 리뷰 항목 번호
        Integer POINT_IDX,          // 포인트 항목 번호
        String  POINT_NAME,         // 포인트 이름
        Integer POINT_TYPE,         // 0:적용 금액, 1: 적용 퍼센트
        Integer POINT_PRICE,        // 포인트 금액 
        Integer POINT_RATE,         // 포인트 비율
        String  EXPIRED_DATE,       // 포인트 만료기간
        Integer STATUS,             // 포인트 상태 
        Integer SAVED_POINT,        // 적립된 포인트
        Integer USED_POINT,         // 사용되었으면 사용된 포인트
        String  POINT_UUID          // 적립금 UUID
        )  {
            
        
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_uuid",        USER_UUID);
        hashMap.put("order_number",     ORDER_NUMBER);
        hashMap.put("review_idx",       REVIEW_IDX);
        hashMap.put("point_idx",        POINT_IDX);
        hashMap.put("point_name",       POINT_NAME);
        hashMap.put("point_type",       POINT_TYPE);
        hashMap.put("point_price",      POINT_PRICE);
        hashMap.put("point_rate",       POINT_RATE);
        hashMap.put("expired_date",     EXPIRED_DATE);
        hashMap.put("status",           STATUS);
        hashMap.put("saved_point",      SAVED_POINT);
        hashMap.put("used_point",       USED_POINT);
        hashMap.put("point_uuid",       POINT_UUID);
        
        return queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);
    }


 

    @Override
    public dto_user_point getDueDatePoint(String USER_UUID) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", USER_UUID);
        SELECT_QUERY.buildEqual("status", USER_POINT.STATUS_SAVE_COMPLETE);
        SELECT_QUERY.buildASC("expired_date");

        return (dto_user_point)listService.getDTOObject(SELECT_QUERY.toString(), dto_user_point.class);
    }

    @Override
    public void updateUsedPoint(Integer USED_POINT, String POINT_UUID) throws SQLException {
        updateService.execute(GET_QUERY_UPDATE_USED_POINT(USED_POINT, POINT_UUID));
    }

    @Override
    public void updatePointStatus(Integer STATUS, String POINT_UUID) throws SQLException {
        updateService.execute(GET_QUERY_UPDATE_POINT_STATUS(STATUS, POINT_UUID));
    }

    public String GET_QUERY_UPDATE_USED_POINT(Integer USED_POINT, String POINT_UUID){
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("used_point",        USED_POINT);
        return queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "point_uuid", POINT_UUID);
    }

    public String GET_QUERY_UPDATE_POINT_STATUS(Integer STATUS, String POINT_UUID){
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("status",        STATUS);
        return queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "point_uuid", POINT_UUID);
    }

    @SuppressWarnings("unchecked")
    public Boolean isUsePossible(String USER_UUID, Integer USE_POINT) throws Exception {
        logger.info("... 회원[" + USER_UUID + "]의 사용 가능한 적립금을 조회중입니다. ");
        Integer total_point = 0;

        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", USER_UUID);
        SELECT_QUERY.buildEqual("status",    USER_POINT.STATUS_SAVE_COMPLETE);
        List<dto_user_point> list = (List<dto_user_point>)listService.getDTOList(SELECT_QUERY.toString(), dto_user_point.class);
        if(list != null){
            for(dto_user_point e : list){
                logger.info("적립금 고유번호[" + e.getPoint_uuid() + "], SAVED_POINT : " + e.getSaved_point() + ", USED_POINT : " + e.getUsed_point());
                Integer tmp = e.getSaved_point() - e.getUsed_point();
                total_point += tmp;
                tmp = null;
            }
        }

        logger.info("회원[" + USER_UUID +"]가 소진가능한 적립금은 총 " + total_point + "원 입니다. ");
        
        if(total_point >= USE_POINT)return true;

        return false;
    }

    @Override
    public dto_user_point getRecord(String POINT_UUID) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("point_uuid", POINT_UUID);
        return (dto_user_point)listService.getDTOObject(SELECT_QUERY.toString(), dto_user_point.class);
    }

    public dto_user_point getPointWithOrderNumber(String ORDER_NUMBER) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("order_number", ORDER_NUMBER);
        SELECT_QUERY.buildEqual("status", USER_POINT.STATUS_READY_TO_BE_SAVED);
        return (dto_user_point)listService.getDTOObject(SELECT_QUERY.toString(), dto_user_point.class);
    }

    @SuppressWarnings("unchecked")
    public List<dto_user_point> getListWithUserUUID(String USER_UUID) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", USER_UUID);
        List<dto_user_point> list = (List<dto_user_point>)listService.getDTOList(SELECT_QUERY.toString(), dto_user_point.class);
        return list;
    }

    @SuppressWarnings("unchecked")
    public List<dto_user_point> getListWithUserUUID2(String USER_UUID) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid",    USER_UUID);
        SELECT_QUERY.buildEqual("status",       USER_POINT.STATUS_SAVE_COMPLETE);
        SELECT_QUERY.buildASC("expired_date");
        List<dto_user_point> list = (List<dto_user_point>)listService.getDTOList(SELECT_QUERY.toString(), dto_user_point.class);
        return list;
    }

    @Override
    public void init(String USER_UUID) throws Exception {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_uuid", USER_UUID);
        updateService.execute(DELETE_QUERY.toString());
    }

    @Override
    public void deleteRecordWithUserUUID(String USER_UUID) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_uuid", USER_UUID);
        updateService.execute(DELETE_QUERY.toString());
        
    }

    @SuppressWarnings("unchecked")
    public Integer getToBeRemovedPoint(String user_uuid) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid", user_uuid);
        SELECT_QUERY.buildEqual("status", USER_POINT.STATUS_SAVE_COMPLETE);

        Integer TO_BE_REMOVED_POINT = 0;

        List<dto_user_point> list = (List<dto_user_point>)listService.getDTOList(SELECT_QUERY.toString(), dto_user_point.class);
        if(list != null){
            for(dto_user_point e : list){

                Integer SAVE_POINT   = e.getSaved_point();

                String  EXPIRED_DATE = e.getExpired_date();

                String  CURRENT_TIME = timeService.getCurrentTime2();

                if(SAVE_POINT > 0){

                    DateTimeFormatter fDTOatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
                    DateTime exp = fDTOatter.parseDateTime(EXPIRED_DATE);
                    DateTime cur = fDTOatter.parseDateTime(CURRENT_TIME);

                    Days diffInDays = Days.daysBetween(cur, exp);
        
                    if(diffInDays.getDays()<30)
                        TO_BE_REMOVED_POINT += SAVE_POINT;

                    fDTOatter = null;

                    exp = null;

                    cur = null;

                }
            }
        }


        return TO_BE_REMOVED_POINT;
    }

    @SuppressWarnings("unchecked")
    public Integer getFriendInvitationPoint(String USER_UUID) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_uuid",    USER_UUID);
        SELECT_QUERY.buildEqual("point_idx",    META_POINT_TYPE.FRIEND_INVITATION);
//        SELECT_QUERY.buildEqual("status",       USER_POINT.STATUS_SAVE_COMPLETE);

        Integer SAVED_POINT = 0;
        List<dto_user_point> list = (List<dto_user_point>)listService.getDTOList(SELECT_QUERY.toString(), dto_user_point.class);
        if(list != null)
            for(dto_user_point e : list)
                SAVED_POINT += e.getSaved_point();

        return SAVED_POINT;
    }

    @Override
    public String GET_QUERY_UPDATE_SAVED_POINT(Integer TO_BE, String POINT_UUID) {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("saved_point",        TO_BE);
        return queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "point_uuid", POINT_UUID);
    } 
 
    
}
