package com.pood.server.api.service.meta.payment_request_text;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_payment_request_text;

public interface PRequestTextService {



    /**************** 영양소 목록 조회 *****************/
    public List<dto_payment_request_text> getPaymentRequestTextList(pagingSet PAGING_SET) throws Exception;




    /**************** 전체 목록 조회 ******************/
    public Integer getTotalRecordNumber() throws SQLException;


    
    
}
