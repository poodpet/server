package com.pood.server.api.service.pay;

import java.sql.SQLException;

import com.pood.server.object.IMP.*;
import com.pood.server.object.resp.resp_pay_success;

public interface checkOrderService {
 
    // 주문 데이터 유효성 검증
    public resp_pay_success     getOrderCheck(IMP IAMPORT) throws Exception;


    // 중복 쿠폰 할인 금액 계산 
    public Integer              overCouponDiscount(Integer coupon_type, Integer discount_price, Integer total_price, Integer max_price, Integer coupon_rate);


    // 전체 할인 금액 계산 
    public Boolean              checkDiscountPrice(IMP IAMPORT);


    // 전체 주문 금액 계산
    public Boolean              checkOrderPrice(IMP IAMPORT);
    

    public resp_pay_success     GET_PAY_BEFORE_RESULT(String order_number, String msg, Boolean isSuccess) throws SQLException;

    
}
