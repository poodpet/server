package com.pood.server.api.service.meta.coupon.impl;

import com.pood.server.api.service.meta.coupon.couponCodeService;

import java.sql.SQLException;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.config.DATABASE;

import com.pood.server.dto.meta.dto_coupon_code;
import com.pood.server.dto.meta.dto_coupon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.record.*;

import org.springframework.stereotype.Service;

@Service("couponCodeService")
public class couponCodeServiceImpl implements couponCodeService{
    

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_COUPON_CODE;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @Override
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    }
 
    public dto_coupon_code getRecord(String code) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("code", code);
        return (dto_coupon_code)listService.getDTOObject(SELECT_QUERY.toString(), dto_coupon_code.class);
    }

    
}
