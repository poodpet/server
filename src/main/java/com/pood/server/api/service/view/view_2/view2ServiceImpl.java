package com.pood.server.api.service.view.view_2;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.req.header.order.goods.hAM_goods_2_1;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.record.*;
import com.pood.server.config.DATABASE;
import com.pood.server.object.pagingSet;
import com.pood.server.api.service.meta.goods.*;
import com.pood.server.config.meta.META_UNIT_SIZE;
import com.pood.server.dto.meta.goods.dto_goods_7;
import com.pood.server.dto.view.dto_view_2;
import com.pood.server.config.meta.META_FEED_TARGET;
import com.pood.server.config.meta.META_FEED_TYPE;
import com.pood.server.config.meta.META_GOODS;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


@Service("view2Service")
public class view2ServiceImpl implements view2Service{

    Logger logger = LoggerFactory.getLogger(view2ServiceImpl.class);

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;

    private final String DEFINE_TABLE_NAME    = com.pood.server.config.DATABASE.VIEW_2;

    private final String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);


    @Override
    public SELECT_QUERY getQuery(
            Integer     goods_idx,                    // 굿즈 항목 번호
            Integer     goods_type_idx,               // 굿즈 타입 항목 번호 
            Integer     pc_idx,                       // 반려동물 타입     
            Integer     ct_idx,                       // 카테고리 항목 번호
            String      ct_sub_idx,                   // 하위 카테고리 항목 번호  
            Integer     brand_idx,                    // 브랜드 항목 번호 
            String      keyword,                      // 검색어  
            String      main_property,                // 주요 성분 
            String      recordbirth, 
            String      unit_size, 
            Integer     life_stage,
            Integer     feed_target,                  // 1:소형, 2:중형, 3:대형
            String      position,
            String      display_type
            ) throws SQLException {
        
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        
        SELECT_QUERY.buildNotEqual("sale_status",  META_GOODS.SALE_STATUS_STOP);
        
        if(goods_idx != null)
            SELECT_QUERY.buildEqual("goods_idx", goods_idx.toString());
        if(goods_type_idx != null)
            SELECT_QUERY.buildEqual("goods_type_idx",  goods_type_idx.toString());
        if(pc_idx != null){
            // 강아지 고양이 둘다 일경우 제외
            if(pc_idx != 0){
                List<Integer> column = new ArrayList<Integer>();
                column.add(pc_idx);
                column.add(0);
                SELECT_QUERY.buildORKey2("pc_idx", column);
            }
        }
        if(ct_idx != null){
            SELECT_QUERY.buildEqual("ct_idx",  ct_idx.toString());
        }
        if(ct_sub_idx != null){
            if(!ct_sub_idx.equals("all"))
                SELECT_QUERY.buildEqual("ct_sub_idx",  ct_sub_idx);
        }
        if(brand_idx != null)
            SELECT_QUERY.buildEqual("brand_idx",   brand_idx.toString());

        if(display_type != null)
            SELECT_QUERY.buildEqual("display_type", display_type);

        if(keyword != null)
            SELECT_QUERY.buildLike("goods_name",   keyword);
        if(main_property != null)
            SELECT_QUERY.buildLike("main_property",    main_property);
        if(feed_target != null){

            if(feed_target == META_FEED_TARGET.SMALL){
                List<String> column = new ArrayList<String>();
                column.add(META_FEED_TARGET.ALL.toString());
                column.add(META_FEED_TARGET.SMALL.toString());
                SELECT_QUERY.buildAndWhereIn("feed_target", column);
            }

            if(feed_target == META_FEED_TARGET.MEDIUM){
                List<String> column = new ArrayList<String>();
                column.add(META_FEED_TARGET.ALL.toString());
                column.add(META_FEED_TARGET.MEDIUM.toString());
                SELECT_QUERY.buildAndWhereIn("feed_target", column);
            }

            if(feed_target == META_FEED_TARGET.BIG){
                List<String> column = new ArrayList<String>();
                column.add(META_FEED_TARGET.ALL.toString());
                column.add(META_FEED_TARGET.BIG.toString());
                SELECT_QUERY.buildAndWhereIn("feed_target", column);
            }
        }
                    
        // 유닛 사이즈
        if(unit_size != null){
            if(unit_size.equals("A"))
                SELECT_QUERY.buildMoreAndLessThan("unit_size", META_UNIT_SIZE.A_MIN, "unit_size", META_UNIT_SIZE.A_MAX);
            if(unit_size.equals("B"))
                SELECT_QUERY.buildMoreAndLessThan("unit_size", META_UNIT_SIZE.B_MIN, "unit_size", META_UNIT_SIZE.B_MAX);
            if(unit_size.equals("C"))
                SELECT_QUERY.buildMoreAndLessThan("unit_size", META_UNIT_SIZE.C_MIN, "unit_size", META_UNIT_SIZE.C_MAX);
        }
                
        if(life_stage != null){
            switch(life_stage){
                case 0:SELECT_QUERY.buildAndWhereIn("feed_type", META_FEED_TYPE.ALL);break;
                case 1:SELECT_QUERY.buildAndWhereIn("feed_type", META_FEED_TYPE.PUPPY);break;
                case 2:SELECT_QUERY.buildAndWhereIn("feed_type", META_FEED_TYPE.ADULT);break;
                case 3:SELECT_QUERY.buildAndWhereIn("feed_type", META_FEED_TYPE.SENIOR);break;
            }
                
        }

        if(position != null){
            List<String> column = new ArrayList<String>();
            column.add("position_1");
            column.add("position_2");
            column.add("position_3");
            SELECT_QUERY.buildLikeOR(column, position);
            column = null;
        }
                
        if(recordbirth != null)
            SELECT_QUERY.buildLessThan("recordbirth", recordbirth);

    
        logger.info(SELECT_QUERY.toString());
        
        return SELECT_QUERY;
    }



    @Override
    public SELECT_QUERY getQuery2(hAM_goods_2_1 header) throws SQLException {
        
        Integer     goods_idx           = header.getGoods_idx();
        
        Integer     goods_type_idx      = header.getGoods_type_idx();
        
        Integer     pc_idx              = header.getPc_idx();

        Integer     ct_idx              = header.getCt_idx();

        String      ct_sub_idx          = header.getCt_sub_idx();

        Integer     brand_idx           = header.getBrand_idx();

        String      keyword             = header.getKeyword();

        String      main_property       = header.getMain_property();

        String      recordbirth         = header.getRecordbirth();

        String      unit_size           = header.getUnit_size();

        Integer     life_stage          = header.getLife_stage();

        Integer     feed_target         = header.getFeed_target();

        String      position            = header.getPosition();


        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY();
        
        SELECT_QUERY.set(DEFINE_DB_NAME, DEFINE_TABLE_NAME);



        if(goods_idx != null)
            SELECT_QUERY.buildEqual("goods_idx", goods_idx.toString());

        if(goods_type_idx != null)
            SELECT_QUERY.buildEqual("goods_type_idx",  goods_type_idx.toString());

        if(pc_idx != null){

            // 강아지 고양이 둘다 일경우 제외
            if(pc_idx != 0){
                List<Integer> column = new ArrayList<Integer>();
                column.add(pc_idx);
                column.add(0);
                SELECT_QUERY.buildORKey2("pc_idx", column);
            }

        }

        if(ct_idx != null){
            SELECT_QUERY.buildEqual("ct_idx",  ct_idx.toString());
        }

        if(ct_sub_idx != null){
            if(!ct_sub_idx.equals("all")){
                if(ct_sub_idx.equals("16") || ct_sub_idx.equals("7")){
                    SELECT_QUERY.buildIn("ct_sub_idx", Arrays.asList(54,Integer.valueOf( ct_sub_idx)));
                }else if(ct_sub_idx.equals("13") || ct_sub_idx.equals("10")){
                    SELECT_QUERY.buildIn("ct_sub_idx", Arrays.asList(52,Integer.valueOf( ct_sub_idx)));
                }else if(ct_sub_idx.equals("14") || ct_sub_idx.equals("50")){
                    SELECT_QUERY.buildIn("ct_sub_idx", Arrays.asList(53,Integer.valueOf( ct_sub_idx)));
                }else
                    SELECT_QUERY.buildEqual("ct_sub_idx",  ct_sub_idx);

            }
        }

        if(brand_idx != null)
            SELECT_QUERY.buildEqual("brand_idx",   brand_idx.toString());

        if(keyword != null)
            SELECT_QUERY.buildLike("goods_name",   keyword);

        if(main_property != null)
            SELECT_QUERY.buildLike("main_property",    main_property);

        if(feed_target != null){

            if(feed_target == META_FEED_TARGET.SMALL){
                List<String> column = new ArrayList<String>();
                column.add(META_FEED_TARGET.ALL.toString());
                column.add(META_FEED_TARGET.SMALL.toString());
                SELECT_QUERY.buildAndWhereIn("feed_target", column);
            }

            if(feed_target == META_FEED_TARGET.MEDIUM){
                List<String> column = new ArrayList<String>();
                column.add(META_FEED_TARGET.ALL.toString());
                column.add(META_FEED_TARGET.MEDIUM.toString());
                SELECT_QUERY.buildAndWhereIn("feed_target", column);
            }

            if(feed_target == META_FEED_TARGET.BIG){
                List<String> column = new ArrayList<String>();
                column.add(META_FEED_TARGET.ALL.toString());
                column.add(META_FEED_TARGET.BIG.toString());
                SELECT_QUERY.buildAndWhereIn("feed_target", column);
            }
        }
                  
        

        // 유닛 사이즈
        if(unit_size != null){
            if(unit_size.equals("A"))
                SELECT_QUERY.buildMoreAndLessThan("unit_size", META_UNIT_SIZE.A_MIN, "unit_size", META_UNIT_SIZE.A_MAX);
            if(unit_size.equals("B"))
                SELECT_QUERY.buildMoreAndLessThan("unit_size", META_UNIT_SIZE.B_MIN, "unit_size", META_UNIT_SIZE.B_MAX);
            if(unit_size.equals("C"))
                SELECT_QUERY.buildMoreAndLessThan("unit_size", META_UNIT_SIZE.C_MIN, "unit_size", META_UNIT_SIZE.C_MAX);
        }
                



        if(life_stage != null){
            switch(life_stage){
                case 0:SELECT_QUERY.buildAndWhereIn("feed_type", META_FEED_TYPE.ALL);break;
                case 1:SELECT_QUERY.buildAndWhereIn("feed_type", META_FEED_TYPE.PUPPY);break;
                case 2:SELECT_QUERY.buildAndWhereIn("feed_type", META_FEED_TYPE.ADULT);break;
                case 3:SELECT_QUERY.buildAndWhereIn("feed_type", META_FEED_TYPE.SENIOR);break;
            }
                
        }




        if(position != null){
            List<String> column = new ArrayList<String>();
            column.add("position_1");
            column.add("position_2");
            column.add("position_3");
            SELECT_QUERY.buildLikeOR(column, position);
            column = null;
        }
                


        if(recordbirth != null)
            SELECT_QUERY.buildLessThan("recordbirth", recordbirth);
        
        logger.info(SELECT_QUERY.toString());

        
        
        goods_idx           = null;
        
        goods_type_idx      = null;
        
        pc_idx              = null;
        
        ct_idx              = null;
        
        ct_sub_idx          = null;
        
        brand_idx           = null;
        
        keyword             = null;
        
        main_property       = null;
        
        recordbirth         = null;
        
        unit_size           = null;
        
        life_stage          = null;
        
        feed_target         = null;

        position            = null;

        return SELECT_QUERY;
    }


    
    

    @Override
    public List<dto_goods_7> getGoodsList(pagingSet PAGING_SET, SELECT_QUERY query) throws Exception{
        query.setPagination(PAGING_SET, true, "goods_idx");
        List<Integer> GOODS_IDX_LIST = listService.getIDXList(query.toString(), "goods_idx");
        return goodsService.GET_GOODS_LIST(GOODS_IDX_LIST);  
    }

    @Override
    public List<dto_goods_7> getGoodsList2(pagingSet PAGING_SET, SELECT_QUERY query, String sort_type) throws Exception{
        
        String sort_column = "recordbirth";
        Boolean is_ascending = true;
        if(sort_type.equals("recently")) {
            sort_column = "recordbirth";
            is_ascending = false;
        } else if (sort_type.equals("priceLow")) {
            sort_column = "goods_price";
        } else if (sort_type.equals("priceHigh")) {
            sort_column = "goods_price"; 
            is_ascending = false;
        }
        
        query.setPagination2(PAGING_SET, is_ascending, sort_column);
        
        List<Integer> GOODS_IDX_LIST = listService.getIDXList(query.toString(), "goods_idx");
        return goodsService.GET_GOODS_LIST_BY_SORT_TYPE(GOODS_IDX_LIST, sort_column, is_ascending);  
    }

    @SuppressWarnings("unchecked")
    public List<String> getKeywordList(String keyword, Integer pc_idx) throws Exception {
        
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME, "goods_name");
        
        if(pc_idx != null){
            // 강아지 고양이 둘다 일경우 제외
            if(pc_idx != 0){
                List<Integer> column = new ArrayList<Integer>();
                column.add(pc_idx);
                column.add(0);
                SELECT_QUERY.buildORKey2("pc_idx", column);
            }
        }

        if(keyword != null){
            if(!keyword.equals(""))
                SELECT_QUERY.buildLike("goods_name", keyword);
        }

        SELECT_QUERY.buildLimit(20);

        List<dto_view_2> list = (List<dto_view_2>)listService.getDTOList(SELECT_QUERY.toString(), dto_view_2.class);

        List<String> result = new ArrayList<String>();

        if(list != null)
            for(dto_view_2 e : list)
                result.add(e.getGoods_name());

        return result;
    }

    @Override
    public Integer getBrandIDX(Integer goods_idx) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("goods_idx", goods_idx);

        dto_view_2 record = (dto_view_2)listService.getDTOObject(SELECT_QUERY.toString(), dto_view_2.class);
        Integer brand_idx = record.getBrand_idx();
        record = null;

        return brand_idx;
    }
    
}
