package com.pood.server.api.service.view.view_5;

import java.util.List;

import com.pood.server.object.meta.maintab.vo_maintab_brand;

public interface view5Service {

    public List<vo_maintab_brand> getList() throws Exception;
    
}
