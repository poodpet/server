package com.pood.server.api.service.pay;

import com.pood.server.object.IMP.*;

public interface initPayService {
  

    public Integer  insertIMPorder(IMP IAMPORT) throws Exception;


    public IMP      initIMP(String param) throws Exception;


    public Integer  tokenCheck(String TOKEN, String USER_UUID) throws Exception;

    
  
}
