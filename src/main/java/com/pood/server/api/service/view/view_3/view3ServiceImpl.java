package com.pood.server.api.service.view.view_3;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.record.*;
import com.pood.server.config.DATABASE;
import com.pood.server.object.meta.maintab.vo_maintab_goods;
import com.pood.server.api.service.meta.goods.*;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


@Service("view3Service")
public class view3ServiceImpl implements view3Service{
    
    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;

    private String DEFINE_TABLE_NAME    = com.pood.server.config.DATABASE.VIEW_3;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @SuppressWarnings("unchecked")
    public List<vo_maintab_goods> getList() throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        List<vo_maintab_goods> list = (List<vo_maintab_goods>)listService.getDTOList(SELECT_QUERY.toString(), vo_maintab_goods.class);
        return list;
    }
}
