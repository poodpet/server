package com.pood.server.api.service.user;

import java.sql.SQLException;

public interface userLogPolicyService {

    /************* 회원 정책 로그 삽입 *******************/
	public Integer insert(Integer USER_IDX, Integer MK_TYPE, Boolean EMAIL_NOTI, String USER_UUID) throws SQLException;
    
}
