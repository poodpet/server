package com.pood.server.api.service.pay;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.pood.server.api.req.header.pay.OrderCancelRequest;
import com.pood.server.api.req.header.pay.hPay_cancel_1_2;
import com.pood.server.api.service.iamport.resParserService;
import com.pood.server.api.service.json.jsonService;
import com.pood.server.api.service.log.logUserOrderService;
import com.pood.server.api.service.log.logUserSavePointService;
import com.pood.server.api.service.log.logUserUsePointService;
import com.pood.server.api.service.meta.goods.goodsProductService;
import com.pood.server.api.service.meta.order.OrderService;
import com.pood.server.api.service.meta.order.orderBasketService;
import com.pood.server.api.service.meta.order.orderCancelService;
import com.pood.server.api.service.meta.order.orderRefundService;
import com.pood.server.api.service.pood_point.poodPointService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.api.service.user.userBasketService;
import com.pood.server.api.service.user.userCouponService;
import com.pood.server.api.service.user.userService;
import com.pood.server.config.meta.META_LOG_PRODUCT_HISTORY;
import com.pood.server.config.meta.order.ORDER_REFUND;
import com.pood.server.config.meta.order.ORDER_STATUS;
import com.pood.server.config.meta.order.ORDER_STATUS_MESSAGE;
import com.pood.server.dto.meta.order.dto_order;
import com.pood.server.entity.iamport.IamportPaymentInfo;
import com.pood.server.entity.log.LogUserPoint;
import com.pood.server.entity.order.dto.OrderBaseDataDto;
import com.pood.server.entity.order.dto.OrderPointRefundDto;
import com.pood.server.entity.order.group.OrderPointMapperGroup;
import com.pood.server.entity.order.mapper.OrderPointMapper;
import com.pood.server.entity.user.UserPoint;
import com.pood.server.entity.user.group.UserPointGroup;
import com.pood.server.exception.SearchPaymentInfoFailException;
import com.pood.server.facade.OrderFacade;
import com.pood.server.object.IMP.IMP_CANCEL;
import com.pood.server.object.IMP.IMP_TOKEN;
import com.pood.server.object.IMP.IMP_TOKEN_RESPONSE;
import com.pood.server.object.meta.vo_order_basket;
import com.pood.server.object.req_retreieve;
import com.pood.server.service.UserPointSeparate;
import com.pood.server.service.UserSeparate;
import com.pood.server.service.factory.OrderCancelFactory;
import com.pood.server.service.factory.OrderCancelPriceInfo;
import com.pood.server.service.log.LogUserPointSeparate;
import com.pood.server.service.log.LogUserUsePointSeparate;
import com.pood.server.service.order.OrderPointSeparate;
import com.pood.server.web.mapper.payment.Money;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service(value = "retreieveService")
public class retreieveServiceImpl implements retreieveService {

    private static final Map<String, String> EXTRA = Map.of("requester", "customer");

    Logger logger = LoggerFactory.getLogger(retreieveServiceImpl.class);

    @Autowired
    @Qualifier("resParserService")
    resParserService resParserService;

    @Autowired
    @Qualifier("jsonService")
    jsonService jsonService;

    @Autowired
    @Qualifier("goodsProductService")
    goodsProductService goodsProductService;

    @Autowired
    @Qualifier("userBasketService")
    userBasketService userBasketService;

    @Autowired
    @Qualifier("logUserOrderService")
    logUserOrderService logUserOrderService;

    @Autowired
    @Qualifier("logUserSavePointService")
    logUserSavePointService logUserSavePointService;

    @Autowired
    @Qualifier("logUserUsePointService")
    logUserUsePointService logUserUsePointService;

    @Autowired
    @Qualifier("orderBasketService")
    orderBasketService orderBasketService;

    @Autowired
    @Qualifier("orderService")
    OrderService orderService;

    @Autowired
    @Qualifier("orderCancelService")
    orderCancelService orderCancelService;

    @Autowired
    @Qualifier("orderRefundService")
    orderRefundService orderRefundService;

    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("userCouponService")
    userCouponService userCouponService;

    @Autowired
    @Qualifier("poodPointService")
    poodPointService poodPointService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    OrderFacade orderFacade;

    @Autowired
    @Qualifier("iamportService")
    com.pood.server.api.service.iamport.iamportService iamportService;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    OrderPointSeparate orderPointSeparate;

    @Autowired
    UserPointSeparate userPointSeparate;

    @Autowired
    UserSeparate userSeparate;

    @Autowired
    LogUserPointSeparate logUserPointSeparate;

    @Autowired
    LogUserUsePointSeparate logUserUsePointSeparate;

    public List<String> rollbackOrderBasketWithGoodsIDX(
        Integer GOODS_IDX, Integer PURCHASED_QTY, String ORDER_NUMBER) throws Exception {

        // 부분 취소한 수량만큼 굿즈 재고 증가
        logger.info(
            "주문번호[" + ORDER_NUMBER + "]의 부분 취소가 진행되어 굿즈[" + GOODS_IDX + "]의 수량 " + PURCHASED_QTY
                + "가 다시 입고됩니다.");

        List<String> process = goodsProductService.addQtyWithGoodsIDX(
            GOODS_IDX,
            PURCHASED_QTY,
            META_LOG_PRODUCT_HISTORY.TYPE_PAY_RETRIEVE,
            META_LOG_PRODUCT_HISTORY.ORDER_RETRIEVE + "[" + ORDER_NUMBER + "]");

        return process;
    }


    @Override
    public List<String> rollbackOrderBasket(Integer ORDER_IDX) throws Exception {

        List<String> transaction_process = new ArrayList<String>();

        logger.info("주문번호[" + ORDER_IDX + "]가 전체 취소됩니다. ");

        List<vo_order_basket> ORDER_BASKET_LIST = orderBasketService.getOrderBasketList(ORDER_IDX);

        for (vo_order_basket e : ORDER_BASKET_LIST) {

            String ORDER_NUMBER = e.getOrder_number();

            Integer GOODS_IDX = e.getGoods_idx();

            Integer BASKET_QUANTITY = e.getQuantity();

            logger.info("주문번호[" + ORDER_NUMBER + "]중 굿즈[" + GOODS_IDX + "]의 수량 " + BASKET_QUANTITY
                + "만큼 다시 입고됩니다.");

            List<String> process = goodsProductService.addQtyWithGoodsIDX(
                GOODS_IDX,
                BASKET_QUANTITY,
                META_LOG_PRODUCT_HISTORY.TYPE_PAY_RETRIEVE,
                META_LOG_PRODUCT_HISTORY.ORDER_RETRIEVE + "[" + ORDER_NUMBER + "]");

            transaction_process.addAll(process);

            process = null;

            BASKET_QUANTITY = null;

            GOODS_IDX = null;

            ORDER_NUMBER = null;

        }

        return transaction_process;
    }


    @Override
    public String requestRefund(String IAMPORT_ACCESS_TOKEN, Long amount, String merchant_uid,
        String requestText)
        throws Exception {
        Map<String, Object> hashMap = new HashMap<String, Object>();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        hashMap.put("merchant_uid", merchant_uid);
        hashMap.put("amount", amount);
        hashMap.put("extra", EXTRA);
        hashMap.put("reason", requestText);
        headers.add("Authorization", IAMPORT_ACCESS_TOKEN);

        HttpEntity<String> HTTP_REFUND_REQUEST = new HttpEntity<String>(new Gson().toJson(hashMap),
            headers);

        RestTemplate restTemplate = new RestTemplate();

        logger.info("********* 아임포트 환불 요청 ************");
        logger.info("[REQUEST]" + com.pood.server.config.host.IAMPORT.REFUND_URL + ":"
            + HTTP_REFUND_REQUEST);

        String RESPONSE = restTemplate.postForObject(com.pood.server.config.host.IAMPORT.REFUND_URL,
            HTTP_REFUND_REQUEST, String.class);

        logger.info("[RESPONSE]" + com.pood.server.config.host.IAMPORT.REFUND_URL + ":" + RESPONSE);
        return RESPONSE;

    }

    @Override
    public String requestRefund(String IAMPORT_ACCESS_TOKEN, Integer amount, String merchant_uid,
        String requestText)
        throws Exception {
        Map<String, Object> hashMap = new HashMap<String, Object>();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        hashMap.put("merchant_uid", merchant_uid);
        hashMap.put("amount", amount);
        hashMap.put("extra", EXTRA);
        hashMap.put("reason", requestText);
        headers.add("Authorization", IAMPORT_ACCESS_TOKEN);

        HttpEntity<String> HTTP_REFUND_REQUEST = new HttpEntity<String>(new Gson().toJson(hashMap),
            headers);

        RestTemplate restTemplate = new RestTemplate();

        logger.info("********* 아임포트 환불 요청 ************");
        logger.info("[REQUEST]" + com.pood.server.config.host.IAMPORT.REFUND_URL + ":"
            + HTTP_REFUND_REQUEST);

        String RESPONSE = restTemplate.postForObject(com.pood.server.config.host.IAMPORT.REFUND_URL,
            HTTP_REFUND_REQUEST, String.class);

        logger.info("[RESPONSE]" + com.pood.server.config.host.IAMPORT.REFUND_URL + ":" + RESPONSE);
        return RESPONSE;

    }

    public static String convertString(final String val) {
        // 변환할 문자를 저장할 버퍼 선언
        StringBuffer sb = new StringBuffer();
        // 글자를 하나하나 탐색한다.
        for (int i = 0; i < val.length(); i++) {
            if ('\\' == val.charAt(i) && 'u' == val.charAt(i + 1)) {
                Character r = (char) Integer.parseInt(val.substring(i + 2, i + 6), 16);
                sb.append(r);
                i += 5;
            } else {
                sb.append(val.charAt(i));
            }
        }
        // 결과 리턴
        return sb.toString();
    }

    @Override
    public String getToken() throws Exception {

        JSONParser parser = new JSONParser();

        RestTemplate restTemplate = new RestTemplate();

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("imp_key", com.pood.server.config.PAYMENT.REST_API_KEY);
        map.add("imp_secret", com.pood.server.config.PAYMENT.REST_API_SECRET);

        // 아임포트 토큰 요청
        logger.info("********* 아임포트 토큰발급 API 호출 ************");
        logger.info("[REQUEST]" + com.pood.server.config.host.IAMPORT.TOKEN_URL + ":");

        String IAMPORT_TOKEN_RESPONSE = restTemplate.postForObject(
            com.pood.server.config.host.IAMPORT.TOKEN_URL, map, String.class);

        logger.info("[RESPONSE]" + com.pood.server.config.host.IAMPORT.TOKEN_URL + ":"
            + IAMPORT_TOKEN_RESPONSE);

        // 아임포트 환불을 위한 액세스 토큰 추출
        JSONObject jsonObj2 = (JSONObject) parser.parse(IAMPORT_TOKEN_RESPONSE);

        IMP_TOKEN IAMPORT_AUTH = (IMP_TOKEN) jsonService.getDecodedObject(jsonObj2.toString(),
            IMP_TOKEN.class);
        IMP_TOKEN_RESPONSE IAMPORT_AUTH_RESPONSE = IAMPORT_AUTH.getResponse();

        String IAMPORT_ACCESS_TOKEN = IAMPORT_AUTH_RESPONSE.getAccess_token();
        logger.info("IAMPORT ACCESS_TOKEN = " + IAMPORT_ACCESS_TOKEN);

        return IAMPORT_ACCESS_TOKEN;
    }


    public Integer CANCEL_ALL(String MERCHANT_UID, String REQUEST_TEXT) throws Exception {
        req_retreieve record = new req_retreieve();
        record.setMerchant_uid(MERCHANT_UID);
        record.setRequest_text(REQUEST_TEXT);
        record.setCancel_goods_idx(-1);
        record.setCancel_goods_price(-1);
        record.setCancel_goods_qty(-1);
        record.setIs_partial(false);
        record.setIs_refund(false);
        record.setRefund_uuid(null);
        record.setAttr_type(null);
        record.setCallback(false);
        return doRetreieve(record);
    }


    public Integer CANCEL_CALLBACK(String MERCHANT_UID, String REQUEST_TEXT) throws Exception {
        req_retreieve record = new req_retreieve();
        record.setMerchant_uid(MERCHANT_UID);
        record.setRequest_text(REQUEST_TEXT);
        record.setCancel_goods_idx(-1);
        record.setCancel_goods_price(-1);
        record.setCancel_goods_qty(-1);
        record.setIs_partial(false);
        record.setIs_refund(false);
        record.setRefund_uuid(null);
        record.setAttr_type(null);
        record.setCallback(true);
        return doRetreieve(record);
    }


    public Integer REFUND_ALL(
        String MERCHANT_UID,                   // 주문 번호
        String REQUEST_TEXT,                   // 환불 요청 사항
        Boolean IS_REFUND,                      // 0 : 일반 취소, 1: 반품
        String REFUND_UUID,                    // 환불 고유 번호
        Integer ATTR_TYPE                       // (반품인 경우) 0 : 구매자 귀책, 1 : 판매자 귀책
    ) throws Exception {

        req_retreieve record = new req_retreieve();
        record.setMerchant_uid(MERCHANT_UID);
        record.setRequest_text(REQUEST_TEXT);
        record.setCancel_goods_idx(-1);
        record.setCancel_goods_price(-1);
        record.setCancel_goods_qty(-1);
        record.setIs_partial(false);
        record.setIs_refund(IS_REFUND);
        record.setRefund_uuid(REFUND_UUID);
        record.setAttr_type(ATTR_TYPE);
        record.setCallback(false);
        return doRetreieve(record);
    }


    public req_retreieve getRetreieveObject(
        String MERCHANT_UID,                   // 주문 번호
        String REQUEST_TEXT,                   // 환불 요청 사항
        Integer CANCEL_GOODS_IDX,               // 주문시 굿즈 항목 번호
        Integer CANCEL_GOODS_PRICE,             // 주문시 굿즈 가격
        Integer CANCEL_GOODS_QTY,               // 주문시 굿즈 개수
        Boolean IS_REFUND,                      // 0 : 일반 취소, 1: 반품
        String REFUND_UUID,                    // 환불 고유 번호
        Integer ATTR_TYPE                       // (반품인 경우) 0 : 구매자 귀책, 1 : 판매자 귀책
    ) throws Exception {

        req_retreieve record = new req_retreieve();
        record.setMerchant_uid(MERCHANT_UID);
        record.setRequest_text(REQUEST_TEXT);
        record.setCancel_goods_idx(CANCEL_GOODS_IDX);
        record.setCancel_goods_price(CANCEL_GOODS_PRICE);
        record.setCancel_goods_qty(CANCEL_GOODS_QTY);
        record.setIs_partial(true);
        record.setIs_refund(IS_REFUND);
        record.setRefund_uuid(REFUND_UUID);
        record.setAttr_type(ATTR_TYPE);
        record.setCallback(false);
        return record;
    }

    @Override
    public int doRetreieve(final OrderCancelRequest orderCancelRequest,
        final hPay_cancel_1_2 cancelDate, final Integer cancelGoodsPrice,
        final OrderCancelPriceInfo orderCancelPriceInfo, final long completePoint)
        throws Exception {
        req_retreieve retreieveDto = req_retreieve.createRetreieve(
            orderCancelRequest, cancelDate, cancelGoodsPrice
        );

        // 결제 취소를 위한 초기값 지정
        IMP_CANCEL impCancel = orderService.getCancelUnit(retreieveDto.getMerchant_uid());
        String userUUID = userService.getUserUUID(impCancel.USER_IDX);
        // 주문 레코드 조회
        dto_order order = orderService.getOrder(retreieveDto.getMerchant_uid());

        if (order != null) {
            // 결제 취소 요청 로그를 남김
            logUserOrderService.insertOrderHistory(impCancel.getORDER_IDX(),

                com.pood.server.config.meta.order.ORDER_STATUS.ORDER_CANCEL_READY,
                retreieveDto.getRequest_text(),
                impCancel.ORDER_NUMBER,
                userUUID,
                retreieveDto.getCancel_goods_idx(),
                retreieveDto.getCancel_goods_qty());

            String iamportAccessToken = getToken();

            // 주문 취소 테이블에 레코드 삽입
            String retreiveUUID = "";

            /**************** 아임포트 환불 요청 ******************/
            logger.info("초기 주문 금액 = " + orderCancelPriceInfo.getOrgGoodsPrice() + "원.");

            Integer remainQuantity = orderBasketService.checkRetrievePossible(
                retreieveDto.getMerchant_uid(),
                retreieveDto.getCancel_goods_idx(), retreieveDto.getCancel_goods_qty());

            if (remainQuantity < 0) {
                logger.info("환불하고자 하는 굿즈 수량이 구매한 굿즈 수량을 초과하였습니다.");
                return 253;
            }

            // 환불 금액에서 반품 배송비 계산해서 차감
            Money refundAmount = orderCancelPriceInfo.getRetreievePrice();
            Money refundPoint = new Money(orderCancelPriceInfo.getRetreievePoint());
            // 환불 정산 로그
            logger.info("굿즈 가격 X 굿즈 개수 = "
                + retreieveDto.getCancel_goods_price() + " X "
                + retreieveDto.getCancel_goods_qty() + " = "
                + (retreieveDto.getCancel_goods_price() * retreieveDto.getCancel_goods_qty())
                + "원, 굿즈 총 가격 : "
                + orderCancelPriceInfo.totalPrice() + "원");

            logger.info(
                "반환 비율 = " + orderCancelPriceInfo.getRetreieveRatio()
                    + ", 반환 금액 = 반환 비율 X 주문 금액 = "
                    + orderCancelPriceInfo.getRetreievePrice().getIntValue()
                    + "원");

            logger.info("적립금 " + orderCancelPriceInfo.getRetreievePoint() + "원이 부분적으로 반환될 예정입니다.");

            if (orderCancelRequest.isFinalCancel()) {
                /// 마지막 상품일 경우 모든 Iamport 금액을 환불한다.
                String payInfo = iamportService.getPayInfo(retreieveDto.getMerchant_uid());
                IamportPaymentInfo iamportPaymentInfo = objectMapper
                    .readValue(payInfo, IamportPaymentInfo.class);
                if (!iamportPaymentInfo.isSuccess()) {
                    throw new SearchPaymentInfoFailException("결제 정보를 찾을수 없습니다.");
                }
                long balanceAmount = iamportPaymentInfo.getBalanceAmount();
                refundAmount = new Money(balanceAmount);
                refundPoint = new Money(order.getUsed_point()).minus(new Money(completePoint));
            }

            logger.info("** 최종 환불 금액 : " + refundAmount.getLongValue() + "원");
            logger.info("** 최종 환불 포인트 : " + refundPoint.getLongValue() + "원");

            // 만약에 반품이 아닌 경우
            if (!retreieveDto.getIs_refund()) {
                retreiveUUID = orderCancelService.insertRecord(
                    retreieveDto.getRequest_text(),
                    retreieveDto.getMerchant_uid(),
                    retreieveDto.getCancel_goods_idx(),
                    retreieveDto.getCancel_goods_qty(),
                    orderCancelPriceInfo.getDeliveryFee(),
                    refundAmount.getIntValue(),
                    refundPoint.getIntValue()
                );
            }

            String STATUS_CODE = "0";
            if (refundAmount.getLongValue() == 0) {
                logger.info("** 최종 환불 금액이 0원으로 아임포트에 요청하지 않음");
            } else if (!retreieveDto.getCallback()) {

                String API_REFUND_RESPONSE = requestRefund(iamportAccessToken,
                    refundAmount.getLongValue(),
                    impCancel.ORDER_NUMBER,
                    retreieveDto.getRequest_text());

                logger.info(API_REFUND_RESPONSE);
                STATUS_CODE = resParserService.getStatusCode(API_REFUND_RESPONSE);
            } else {
                STATUS_CODE = "0";
            }

            // 아임포트 환불 신청 성공 실패 기준 : 아임포트 API 호출이 정상적으로 이루어지지 않을 경우 DB 업데이트는 이루어지지 않음
            ///////////////////////////////////////////////////////////////////////////////////////////////////
//            Money minus = refundAmount.minus(new Money(orderCancelPriceInfo.getDeliveryFee()));

            if (STATUS_CODE.equals("0")) {

                pointReturnProcess(orderCancelRequest, retreieveDto, impCancel, userUUID, order,
                    retreiveUUID, refundAmount, refundPoint.getIntValue(),
                    orderCancelPriceInfo.getRetreieveRatio());
            } else {

                // 주문 취소 요청 실패 로그를 알림
                logUserOrderService.insertOrderHistory(
                    impCancel.ORDER_IDX,
                    ORDER_STATUS.ORDER_CANCEL_READY,
                    ORDER_STATUS_MESSAGE.ORDER_CANCEL_FAILED,
                    impCancel.ORDER_NUMBER,
                    userUUID,
                    retreieveDto.getCancel_goods_idx(),
                    retreieveDto.getCancel_goods_qty());
                return 207;
            }
        }
        return 200;
    }

    private void pointReturnProcess(OrderCancelRequest orderCancelRequest,
        req_retreieve retreieveDto,
        IMP_CANCEL impCancel, String userUUID, dto_order order, String retreiveUUID,
        Money refundAmount, int retreievePoint, Double retreieveRatio) throws Exception {
        logger.info("성공적으로 아임포트 환불 요청이 완료 되었습니다. ");

        /*****************************************
         *
         *  (1) 주문 히스토리에 취소 성공 레코드 생성
         *  (2) 주문에 매핑되었던 적립금 전부 반환
         *  (3) 주문 상태를 주문 취소 완료로 변경함
         *  (4-1) 전체 취소/반품인 경우 장바구니 레코드 전체 취소처리
         *  (4-2) 부분 취소/반품인 경우 장바구니 중에서 일부분만 취소 처리
         *  (5) 취소 : 해당 취소 레코드에 대해서 취소 완료 처리
         *  (6) 반품 : 쿠폰 반납 및 연장 처리
         *
         *******************************************/
        // 주문 취소가 완료 되면 로그를 남김
        List<String> transaction_process = new ArrayList<String>();

        String process_00 = orderService.GET_QUERY_UPDATE_REFUND_AMOUNT(
            refundAmount.getIntValue(),
            impCancel.ORDER_NUMBER);
        transaction_process.add(process_00);

        //주문 취소시 포인트 반환
        retrieveUserOrderPoint(impCancel.ORDER_NUMBER, impCancel.ORDER_NAME, userUUID, retreievePoint);

        // 장바구니 상태 롤백
        if (retreieveDto.getIs_partial()) {
            logger.info("해당 환불은 부분 환불에 해당합니다. ");
            //장바구니 부분 환불
            List<String> process_1 = rollbackOrderBasketWithGoodsIDX(
                retreieveDto.getCancel_goods_idx(),
                retreieveDto.getCancel_goods_qty(), retreieveDto.getMerchant_uid());
            transaction_process.addAll(process_1);
        } else {
            logger.info("해당 환불은 전체 환불에 해당합니다. ");
            List<String> process_1 = rollbackOrderBasket(impCancel.ORDER_IDX);
            transaction_process.addAll(process_1);
        }

        if (orderCancelRequest.isFinalCancel()) {
            logger.info("***** 해당 환불은 전체 중 마지막 환불에 해당됩니다.");
            if (!retreieveDto.getIs_refund()) {
                String process_3 = orderService.GET_QUERY_UPDATE_ORDER_STATUS(
                    ORDER_STATUS.ORDER_CANCEL_SUCCESS, impCancel.ORDER_IDX);
                transaction_process.add(process_3);
            }
        }

        if (!(retreieveDto.getIs_refund())) {

            logger.info("해당 환불은 일반 결제 취소입니다. ");
            // 주문 내역에 취소 성공 로그 추가
            String process_0 = logUserOrderService.GET_QUERY_INSERT_ORDER_HISTORY(
                impCancel.ORDER_IDX,
                ORDER_STATUS.ORDER_CANCEL_SUCCESS,
                ORDER_STATUS_MESSAGE.REFUND_SUCCESS_MESSAGE(impCancel.ORDER_NUMBER),
                impCancel.ORDER_NUMBER,
                userUUID,
                retreieveDto.getCancel_goods_idx(),
                retreieveDto.getCancel_goods_qty());

            transaction_process.add(process_0);
            logger.info("주문[" + order.getOrder_number() + "]이 정상적으로 취소되었습니다.");

            // 취소 성공하면 해당 취소 레코드에 대해서 취소완료 처리
            String process_4 = orderCancelService.GET_QUERY_UPDATE_SUCCESS(retreiveUUID);
            transaction_process.add(process_4);

            // 취소 성공하면 해당 취소 레코드에 대해서 지급된 적립금 단계적 회수
            List<String> process_6 = poodPointService.cancelIssuedPoint(
                impCancel.ORDER_NUMBER, retreieveRatio);
            transaction_process.addAll(process_6);

        } else {

            logger.info("해당 환불은 반품입니다. ");

            // 주문 내역에 반품 승인 로그 추가
            String process_0 = logUserOrderService.GET_QUERY_INSERT_ORDER_HISTORY(
                impCancel.ORDER_IDX,
                ORDER_STATUS.REJECT_ACCEPT,
                ORDER_STATUS_MESSAGE.REFUND_SUCCESS_MESSAGE(impCancel.ORDER_NUMBER),
                impCancel.ORDER_NUMBER,
                userUUID,
                retreieveDto.getCancel_goods_idx(),
                retreieveDto.getCancel_goods_qty());

            transaction_process.add(process_0);

            Integer userCouponIdx = order.getOver_coupon_idx();

            if (userCouponIdx != -1) {
                logger.info("반품 주문 취소인 관계로, 사용한 쿠폰[" + userCouponIdx + "]을 반환합니다. ");
            }

            // 반품인 경우 쿠폰 반환해줘야 함 : 기간 7일 늘림
            String process_4 = userCouponService.GET_QUERY_ROLLBACK_USER_COUPON(
                userCouponIdx);
            transaction_process.add(process_4);

            // 관리자가 반품 신청을 승인 : 반품 상태값 승인으로 변경
            String process_5 = orderRefundService.GET_QUERY_UPDATE_STATUS(
                retreieveDto.getRefund_uuid(),
                ORDER_REFUND.TYPE_ACCEPT);

            transaction_process.add(process_5);

        }

        transaction_process.removeAll(Collections.singletonList(null));
        updateService.commit(transaction_process);
    }

    private void retrieveUserOrderPoint(final String orderNumber, final String orderName,
        final String userUuid, final int retrievePoint) {

        if (!isExistToReturnPoint(retrievePoint)) {
            return;
        }

        final OrderPointMapperGroup orderPointMapperGroup = orderPointSeparate
            .getUsePointList(orderNumber);
        final UserPointGroup userUsedPointGroup = userPointSeparate
            .findAllByPointUuidIn(orderPointMapperGroup.getPointUuidList());

        final Map<UserPoint, Integer> refundUserPointMap = new HashMap<>();
        final List<OrderPointRefundDto> orderPointRefundList = new LinkedList<>();
        final List<LogUserPoint> insertLogUserPointList = new ArrayList<>();

        int returnPoint = retrievePoint;

        for(int index = 0; returnPoint > 0 ; index ++ ){

            final OrderPointMapper orderPointMapper = orderPointMapperGroup.getIndex(index);
            final UserPoint userPoint = userUsedPointGroup.getUserPoint(
                orderPointMapper.getPointUuid());

            final Integer singleRefundPoint = calculationRefundPoint(returnPoint,
                orderPointMapper.getUsedPoint());

            refundUserPointMap.put(userPoint, singleRefundPoint);
            orderPointRefundList.add(OrderPointRefundDto.of(orderPointMapper, singleRefundPoint));
            insertLogUserPointList.add(
                LogUserPoint.of(userUuid, userPoint.getPointUuid(), singleRefundPoint)
            );

            returnPoint -= singleRefundPoint;
        }

        userPointSeparate.refundAllPoint(refundUserPointMap);
        orderPointSeparate.refundPoint(orderPointRefundList);
        userSeparate.addPoint(userUuid, retrievePoint);

        logUserPointSeparate.insertAll(insertLogUserPointList);
        logUserUsePointSeparate.insertCancelLog(userUuid, "[" + orderName + "] 취소 적립금 반환",
            retrievePoint);
    }

    private boolean isExistToReturnPoint(final int retrievePoint) {
        return retrievePoint > 0;
    }

    private Integer calculationRefundPoint(final Integer returnPoint, final Integer useOrderPoint) {

        if (returnPoint < useOrderPoint) {
            return returnPoint;
        }

        return useOrderPoint;
    }

    public Integer doRetreieve(req_retreieve REQ_REFUND) throws Exception {

        // 주문 번호
        String MERCHANT_UID = REQ_REFUND.getMerchant_uid();

        // 환불 요청 문구
        String REQUEST_TEXT = REQ_REFUND.getRequest_text();

        // 굿즈 항목 번호
        Integer CANCEL_GOODS_IDX = REQ_REFUND.getCancel_goods_idx();

        // 굿즈 가격
        Integer CANCEL_GOODS_PRICE = REQ_REFUND.getCancel_goods_price();

        // 굿즈 수량
        Integer CANCEL_GOODS_QTY = REQ_REFUND.getCancel_goods_qty();

        Boolean IS_REFUND = REQ_REFUND.getIs_refund();

        Boolean IS_PARTIAL = REQ_REFUND.getIs_partial();

        // 환불 고유 번호
        String REFUND_UUID = REQ_REFUND.getRefund_uuid();

        Integer ATTR_TYPE = REQ_REFUND.getAttr_type();

        Boolean CALLBACK = REQ_REFUND.getCallback();

        // TOTAL_USED_POINT : 사용한 전체포인트
        Double RETREIEVE_RATIO = null;

        Double RETREIEVE_POINT = null;

        Double RETREIEVE_PRICE = null;

        // 결제 취소를 위한 초기값 지정
        IMP_CANCEL IMP_CANCEL = orderService.getCancelUnit(MERCHANT_UID);

        String USER_UUID = userService.getUserUUID(IMP_CANCEL.USER_IDX);

        // 주문 레코드 조회
        dto_order ORDER = orderService.getOrder(MERCHANT_UID);

        if (ORDER != null) {

            Integer TOTAL_PRICE = ORDER.getTotal_price();

            Integer TOTAL_USED_POINT = ORDER.getUsed_point();

            Integer DELIVERY_FEE = ORDER.getDelivery_fee();

            Integer ORDER_PRICE = ORDER.getOrder_price();

            String IAMPORT_ACCESS_TOKEN = getToken();

            // 주문 취소 테이블에 레코드 삽입
            String RETREIVE_UUID = "";
            /**************** 아임포트 환불 요청 ******************/
            Integer REFUND_AMOUNT = ORDER_PRICE;

            Integer RETURN_POINT = TOTAL_USED_POINT;

            logger.info("초기 주문 금액 = " + ORDER_PRICE + "원.");

            final OrderBaseDataDto orderbaseData = orderFacade.getOrderInfoByOrderNumberAndUserUUID(
                MERCHANT_UID, USER_UUID);
            final List<hPay_cancel_1_2> payCancelInfoList = orderbaseData.getPayCancelInfoList();
            final List<Integer> goodsIdxList = orderbaseData.getNonCancelledGoodsIdxList();
            long completePoint = orderbaseData.refundCompletePoint();
            int retreiveStatus = 0;

            OrderCancelRequest orderCancelRequest = createOrderCancelRequest(MERCHANT_UID,
                payCancelInfoList);

            for (hPay_cancel_1_2 cancelDate : payCancelInfoList) {
                OrderCancelPriceInfo orderCancelPriceInfo = new OrderCancelFactory()
                    .create(orderbaseData, goodsIdxList, cancelDate.getGoods_idx());

                Integer cancelGoodsPrice = orderBasketService.getPrice(MERCHANT_UID,
                    cancelDate.getGoods_idx());

                req_retreieve retreieveDto = req_retreieve.createRetreieve(
                    orderCancelRequest, cancelDate, cancelGoodsPrice
                );

                boolean isFinal = orderBasketService.isFinalRetreieve(
                    retreieveDto.getMerchant_uid(), retreieveDto.getCancel_goods_idx());
                orderCancelRequest.setFianlCancle(isFinal);
                retreiveStatus = doRetreieve(orderCancelRequest,
                    cancelDate, cancelGoodsPrice, orderCancelPriceInfo, completePoint);
                completePoint += orderCancelPriceInfo.getRetreievePoint();
                if (200 != retreiveStatus) {
                    throw new IllegalArgumentException("error code : " + retreiveStatus);
                }
            }
            return 200;
        } else {
            throw new IllegalArgumentException("주문을 찾을수 없습니다.");
        }
    }

    public Integer GET_REFUND_DELIVERY_FEE(Integer DELIVERY_FEE, Integer ATTR_TYPE) {

        Integer REFUND_DELIVERY_FEE = 0;

        // 구매자 귀책시 반품 처리
        if (ATTR_TYPE == ORDER_REFUND.ATTR_BUYER) {
            logger.info("[구매자 귀책]반품 배송비가 2500원이 부과됩니다. ");
            REFUND_DELIVERY_FEE = 2500;
        }

        // 판매자 귀책시 반품 처리
        if (ATTR_TYPE == ORDER_REFUND.ATTR_SELLER) {
            logger.info("[판매자 귀책]반품 배송비가 0원이 부과됩니다.");
            REFUND_DELIVERY_FEE = 0;
        }

        return REFUND_DELIVERY_FEE;
    }

    public Integer getRefundDeliveryFee(final Integer attrType) {
        // 구매자 귀책시 반품 처리
        if (Objects.equals(attrType, ORDER_REFUND.ATTR_BUYER)) {
            logger.info("[구매자 귀책]반품 배송비가 2500원이 부과됩니다. ");
            return 2500;
        }
        // 판매자 귀책시 반품 처리
        if (Objects.equals(attrType, ORDER_REFUND.ATTR_SELLER)) {
            logger.info("[판매자 귀책]반품 배송비가 0원이 부과됩니다.");
            return 0;
        }
        return 0;
    }

    private OrderCancelRequest createOrderCancelRequest(final String merchantUUID,
        final List<hPay_cancel_1_2> payCancelInfoList) {
        OrderCancelRequest orderCancelRequest = new OrderCancelRequest();
        orderCancelRequest.setIsRefund(null);
        orderCancelRequest.setRequest_text("관리자에 의한 취소");
        orderCancelRequest.setMerchant_uid(merchantUUID);
        orderCancelRequest.setData(payCancelInfoList);
        return orderCancelRequest;
    }
}
