package com.pood.server.api.service.meta.delivery_courier;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.meta.dto_delivery_courier;
import com.pood.server.object.pagingSet;

public interface deliveryCourierService {

    public Integer getTotalRecordNumber() throws SQLException;

    List<dto_delivery_courier> getList(pagingSet PAGING_SET) throws Exception;
    
}
