package com.pood.server.api.service.user.impl;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.database.DBService;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.query.queryService;
import com.pood.server.api.service.record.updateService;
import com.pood.server.api.service.sms.SMSService;
import com.pood.server.api.service.time.timeService;
import com.pood.server.api.service.user.userSMSAuthService;
import com.pood.server.config.DATABASE;
import com.pood.server.dto.user.dto_user_sms_auth;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("userSMSAuthService")
public class userSMSAuthServiceImpl implements userSMSAuthService {

    private static final int TOKEN_EXPIRED = 5;
    @Autowired
    @Qualifier("timeService")
    timeService timeService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("DBService")
    DBService DBService;

    @Autowired
    @Qualifier("SMSService")
    SMSService SMSService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_USER_SMS_AUTH;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);


    /*************** 회원 인증번호 항목 추가 ******************/
    public Integer insertRecord(String PHONE_NUMBER, String SMS_AUTH) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("phone_number", PHONE_NUMBER);
        hashMap.put("sms_auth", SMS_AUTH);
        hashMap.put("sms_status", com.pood.server.config.meta.user.USER_SMS_AUTH.ISSUED);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;

    }

    /*************** 특정 폰 번호에 해당하는 인증번호 조회 ******************/
    public dto_user_sms_auth getSMSAuth(String PHONE_NUMBER) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("phone_number", PHONE_NUMBER);
        SELECT_QUERY.buildReverseOrder("idx");
        return (dto_user_sms_auth)listService.getDTOObject(SELECT_QUERY.toString(), dto_user_sms_auth.class);
    }

    /*************** 인증 번호 업데이트 ******************/
    public void updateRecord(String PHONE_NUMBER, String AUTH_NUMBER) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("sms_auth", AUTH_NUMBER);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "phone_number", PHONE_NUMBER);
        updateService.execute(query);
        query = null;

    }

    /*************** SMS 발송 *********************/
    public Integer sendSMS(String PHONE_NUMBER, String MESSAGE) throws Exception {
        return SMSService.send(PHONE_NUMBER, MESSAGE);

    }



	/* ************** 특정 레코드에 대해서 인증 성공으로 업데이트 *************/
	public void updateAuthSuccess(Integer IDX) throws SQLException{
		Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("sms_status", com.pood.server.config.meta.user.USER_SMS_AUTH.SUCCESS);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", IDX.toString());
        updateService.execute(query);
        query = null;

	}

    @Override
    public Boolean isRecordbirthValid(String PHONE_NUMBER) throws Exception {

        Boolean chk = true;
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("phone_number", PHONE_NUMBER);
        SELECT_QUERY.buildReverseOrder("idx");
        
        dto_user_sms_auth record = (dto_user_sms_auth)listService.getDTOObject(SELECT_QUERY.toString(), dto_user_sms_auth.class);

        if(record != null){
            String recordbirth = record.getRecordbirth();
            String CURRENT_DATETIME = timeService.getCurrentTime();
            long DIFF_SECONDS = timeService.getTimeDifference(recordbirth, CURRENT_DATETIME);

            if (DIFF_SECONDS < TOKEN_EXPIRED) {
                chk = false;
            }
        }

        return chk;
    }
}
