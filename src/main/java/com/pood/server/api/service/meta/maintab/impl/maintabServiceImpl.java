package com.pood.server.api.service.meta.maintab.impl;

import java.util.ArrayList;
import java.util.List;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.config.DATABASE;
import com.pood.server.config.meta.META_CATEGORY_TYPE;
import com.pood.server.config.meta.META_MAIN_TAB_TOP;
import com.pood.server.dto.meta.dto_maintab;
import com.pood.server.dto.meta.dto_maintab_top;
import com.pood.server.dto.meta.dto_promotion_image;
import com.pood.server.dto.meta.event.dto_event_image_2;
import com.pood.server.api.service.meta.event_image.*;
import com.pood.server.object.meta.vo_main_data;
import com.pood.server.object.meta.vo_main_data_banner;
import com.pood.server.object.meta.vo_main_data_brand;
import com.pood.server.object.meta.vo_main_data_event;
import com.pood.server.object.meta.vo_main_data_goods;
import com.pood.server.object.meta.vo_main_data_promotion;
import com.pood.server.object.meta.maintab.vo_maintab_banner;
import com.pood.server.object.meta.maintab.vo_maintab_brand;
import com.pood.server.object.meta.vo_category;
import com.pood.server.object.meta.vo_category_image;
import com.pood.server.object.meta.maintab.vo_maintab_event;
import com.pood.server.object.meta.maintab.vo_maintab_promotion;
import com.pood.server.object.meta.maintab.vo_maintab_goods;
import com.pood.server.object.resp.resp_main;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.meta.category.*;
import com.pood.server.api.service.meta.goods.goodsService;
import com.pood.server.api.service.meta.maintab.*;
import com.pood.server.api.service.meta.promotion.*;
import com.pood.server.api.service.meta.product.*;
import com.pood.server.api.service.meta.category_image.*;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.view.view_3.view3Service;
import com.pood.server.api.service.view.view_4.*;
import com.pood.server.api.service.view.view_5.*;
import com.pood.server.api.service.view.view_6.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("maintabService")
public class maintabServiceImpl implements maintabService{

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("promotionService")
    promotionService promotionService;

    @Autowired
    @Qualifier("maintabBannerService")
    maintabBannerService maintabBannerService;

 
 
    @Autowired
    @Qualifier("maintabTopService")
    maintabTopService maintabTopService;

    @Autowired
    @Qualifier("maintabCTService")
    maintabCTService maintabCTService;
 
    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;

    @Autowired
    @Qualifier("eventImageService")
    eventImageService eventImageService;

    @Autowired
    @Qualifier("categoryImageService")
    categoryImageService categoryImageService;

    @Autowired
    @Qualifier("productCTService")
    productCTService productCTService;

    @Autowired
    @Qualifier("categoryService")
    categoryService categoryService;

    @Autowired
    @Qualifier("view3Service")
    view3Service view3Service;

    @Autowired
    @Qualifier("view4Service")
    view4Service view4Service;

    @Autowired
    @Qualifier("view5Service")
    view5Service view5Service;

    @Autowired
    @Qualifier("view6Service")
    view6Service view6Service;
 
    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_MAINTAB;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    @Override
    public Integer getTotalRecordNumber() throws Exception {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return listService.getRecordCount(CNT_QUERY.toString());
    }

    public List<resp_main> getMainList(Integer pc_id) throws Exception {


        List<vo_category>               CATEGORY_LIST           = categoryService.getList(pc_id);
        
        List<vo_category_image>         CATEGORY_IMAGE_LIST     = categoryImageService.getList();
        
        List<vo_maintab_banner>         MAINTAB_BANNER_LIST     = maintabBannerService.getList();

        List<vo_maintab_brand>          MAINTAB_BRAND_LIST      = view5Service.getList();

        List<vo_maintab_event>          MAINTAB_EVENT_LIST      = view4Service.getList();

        List<vo_maintab_promotion>      MAINTAB_PROMOTION_LIST  = view6Service.getList();

        List<vo_maintab_goods>          MAINTAB_GOODS_LIST      = view3Service.getList();

        List<dto_maintab>               MAINTAB_LIST            = getList(pc_id);

        List<dto_promotion_image>       PROMOTION_IMAGE_LIST    = promotionService.getImageList();

        List<dto_event_image_2>         EVENT_IMAGE_LIST        = eventImageService.getImageList(null);        

        List<dto_maintab_top> list = (List<dto_maintab_top>)maintabTopService.getList();

        List<resp_main> result = new ArrayList<resp_main>();

        if(list != null){
            for(dto_maintab_top e : list){
                
                Integer MT_IDX = e.getIdx();

                // 메인 탑 설정
                resp_main record = new resp_main();
                record.setMain_title(e.getMain_title());
                record.setPriority(e.getPriority());
                record.setStatus(e.getStatus());
                record.setVisible(e.getVisible());


                // 메인 카테고리 이미지 선별해서 조회
                List<vo_category_image>         MAINTAB_CATEGORY_IMAGE_LIST     = new ArrayList<vo_category_image>();

                for(vo_category e2 : CATEGORY_LIST){
                    
                    Integer CT_IDX = e2.getIdx();

                    Integer TP_IDX = e2.getTp_idx();


                    // 강아지 고양이 분류하여 카테고리 보여줌
                    if(pc_id.equals(e2.getPc_id())){
                        
                        Boolean isFlag = false;

                        
                        // 만약 해당 카테고리가 메인에 쓰이는 카테고리인 경우
                        if(MT_IDX.equals(META_MAIN_TAB_TOP.MAIN) && TP_IDX.equals(META_CATEGORY_TYPE.MAIN))isFlag = true;

                        if(MT_IDX.equals(META_MAIN_TAB_TOP.MAIN_POOD_ITEM) && TP_IDX.equals(META_CATEGORY_TYPE.MAIN_POOD_ITEM))isFlag = true;

                        if(MT_IDX.equals(META_MAIN_TAB_TOP.MAIN_POOD_BEST) && TP_IDX.equals(META_CATEGORY_TYPE.MAIN_POOD_BEST))isFlag = true;

                        if(MT_IDX.equals(META_MAIN_TAB_TOP.MAIN_EVENT) && TP_IDX.equals(META_CATEGORY_TYPE.MAIN_EVENT))isFlag = true;

                        if(MT_IDX.equals(META_MAIN_TAB_TOP.MAIN_PET_DOCTOR) && TP_IDX.equals(META_CATEGORY_TYPE.MAIN_PET_DOCTOR))isFlag = true;

                        if(MT_IDX.equals(META_MAIN_TAB_TOP.MAIN_POOD_BRAND) && TP_IDX.equals(META_CATEGORY_TYPE.MAIN_POOD_BRAND))isFlag = true;

                        if(MT_IDX.equals(META_MAIN_TAB_TOP.MAIN_PROMOTION) && TP_IDX.equals(META_CATEGORY_TYPE.MAIN_PROMOTION))isFlag = true;
                            

                        if(isFlag){
                            for(vo_category_image e4 : CATEGORY_IMAGE_LIST)
                                if(e4.getCt_idx().equals(CT_IDX))
                                    MAINTAB_CATEGORY_IMAGE_LIST.add(e4);
                        }
                        
                        CT_IDX = null;

                        TP_IDX = null;
                    }
                }



                // 탭별로 카테고리 이미지 저장
                record.setCategory(MAINTAB_CATEGORY_IMAGE_LIST);

                MAINTAB_CATEGORY_IMAGE_LIST = null;


                List<vo_main_data> MAIN_DATA = new ArrayList<vo_main_data>();
                

                // 메인 탭 정보 조회
                for(dto_maintab e2 : MAINTAB_LIST){
                    Integer MAINTAB_PC_ID   =   e2.getPc_id();
                    Integer MAINTAB_IDX     =   e2.getIdx();
                    Integer MAINTAB_TYPE    =   e2.getType();

                    if((MT_IDX.equals(MAINTAB_TYPE)) && (MAINTAB_PC_ID.equals(pc_id))){


                        // 메인 탭 별로 정보 조회
                        vo_main_data record2 = new vo_main_data();
                        record2.setMain_type(e2.getType_name());
                        record2.setDisplayType(e2.getDisplay_type());
                        record2.setTitle(e2.getTitle());
                        record2.setDescription(e2.getTitle_desc());
                        record2.setPriority(e2.getPriority());
                        record2.setMain_tab_start_date(e2.getStart_date());
                        record2.setMain_tab_end_date(e2.getEnd_date());
                        record2.setCt_name(e2.getCt_name());
                        record2.setVisible(e2.getVisible());
                        record2.setNumber_tag(e2.getNumber_tag());


                        // 메인 탭 별로 배너 정보 조회
                        if(MAINTAB_BANNER_LIST != null){
                            List<vo_main_data_banner>      MAIN_DATA_BANNER_LIST = getBannerList(
                                MAINTAB_IDX, 
                                MAINTAB_BANNER_LIST, 
                                PROMOTION_IMAGE_LIST,
                                EVENT_IMAGE_LIST);
                                record2.setBanner(MAIN_DATA_BANNER_LIST);
                                MAIN_DATA_BANNER_LIST = null;
                        }

 

                        // 메인 탭 별로 굿즈 정보 조회
                        if(MAINTAB_GOODS_LIST != null){
                            List<vo_main_data_goods>       MAIN_DATA_GOODS_LIST = getGoodsList(MAINTAB_IDX, MAINTAB_GOODS_LIST);
                            record2.setGoods(MAIN_DATA_GOODS_LIST);
                            MAIN_DATA_GOODS_LIST = null;
                        }   


                        

                        // 메인 탭 별로 프로모션 정보 조회
                        if(MAINTAB_PROMOTION_LIST != null){
                            List<vo_main_data_promotion>   MAIN_DATA_PROMOTION_LIST = getPromotionList(MAINTAB_IDX, MAINTAB_PROMOTION_LIST);
                            record2.setPromotion(MAIN_DATA_PROMOTION_LIST);
                            MAIN_DATA_PROMOTION_LIST = null;
                        }




                        // 메인 탭 별로 이벤트 정보 조회
                        if(MAINTAB_EVENT_LIST != null){
                            List<vo_main_data_event>       MAIN_DATA_EVENT_LIST        = getEventList(MAINTAB_IDX, MAINTAB_EVENT_LIST);
                            record2.setEvent(MAIN_DATA_EVENT_LIST);
                            MAIN_DATA_EVENT_LIST = null;
                        }




                        // 메인 탭 별로 브랜드 정보 조회
                        if(MAINTAB_BRAND_LIST != null){
                            List<vo_main_data_brand>       MAIN_DATA_BRAND_LIST        = getBrandLIst(MAINTAB_IDX, MAINTAB_BRAND_LIST);
                            record2.setBrand(MAIN_DATA_BRAND_LIST);
                            MAIN_DATA_BRAND_LIST = null;
                        }


 
                        MAIN_DATA.add(record2);
                    }

                }

                record.setMain_data(MAIN_DATA);
                result.add(record);
            }
 
        }

        MAINTAB_BANNER_LIST = null;

        MAINTAB_BRAND_LIST  = null;

        MAINTAB_EVENT_LIST  = null;

        MAINTAB_PROMOTION_LIST  = null;


        return result;
    }


    // 메인 탭 항목번호에 해당하는 브랜드  레코드들을 가지고 옴
    private List<vo_main_data_brand> getBrandLIst(
        Integer maintab_idx, 
        List<vo_maintab_brand> MAINTAB_BRAND_LIST) {
        List<vo_main_data_brand> result = new ArrayList<vo_main_data_brand>();
        for(vo_maintab_brand e : MAINTAB_BRAND_LIST){

            Integer tmp2 = e.getMaintab_idx();

            if(tmp2.equals(maintab_idx)){

                vo_main_data_brand  record = new vo_main_data_brand();

                record.setTitle(e.getTitle());
                record.setDescription(e.getDescription());
                record.setImage(e.getImage());
                record.setPriority(e.getPriority());
                if(!e.getVisible())
                    record.setVisible(0);
                else record.setVisible(1);
                record.setBrand_idx(e.getBrand_idx());

                result.add(record);
            }
        }

        return result;
    }

    // 메인 탭 항목번호에 해당하는 이벤트 레코드들을 가지고 옴
    private List<vo_main_data_event> getEventList(
        Integer maintab_idx, 
        List<vo_maintab_event> MAINTAB_EVENT_LIST) {
        List<vo_main_data_event> result = new ArrayList<vo_main_data_event>();
        for(vo_maintab_event e : MAINTAB_EVENT_LIST){

            Integer tmp2 = e.getMaintab_idx();

            if(tmp2.equals(maintab_idx)){
                vo_main_data_event  record = new vo_main_data_event();
                record.setTitle(e.getTitle());
                record.setImage(e.getImage());
                record.setPriority(e.getPriority());
                if(!e.getVisible())
                    record.setVisible(0);
                else record.setVisible(1);
                record.setStartTime(e.getStart_date());
                record.setEndTime(e.getEnd_date());
                record.setPage_landing_idx(e.getPage_landing_idx());
                result.add(record);
            }
        }

        return result;
    }



    


    // 메인 탭 항목번호에 해당하는 굿즈 레코드들을 가지고 옴
    private List<vo_main_data_goods> getGoodsList(
        Integer maintab_idx, 
        List<vo_maintab_goods> MAINTAB_GOODS_LIST) {
        List<vo_main_data_goods> result = new ArrayList<vo_main_data_goods>();
        for(vo_maintab_goods e : MAINTAB_GOODS_LIST){

            Integer tmp2 = e.getMaintab_idx();

            if(tmp2.equals(maintab_idx)){
                vo_main_data_goods record = new vo_main_data_goods();
                record.setGoods_idx(e.getGoods_idx());
                record.setGoods_name(e.getGoods_name());
                record.setDiscount_rate(e.getDiscount_rate());
                record.setGoods_price(e.getGoods_price());
                record.setImage(e.getImage());
                record.setPriority(e.getPriority());
                record.setVisible(e.getVisible());
                record.setAverage_rating(e.getAverage_rating());
                record.setReview_cnt(e.getReview_cnt());
                result.add(record);
            }
        }

        return result;
    }
 
    // 메인 탭 항목번호에 해당하는 프로모션 레코드들을 가지고 옴
    private List<vo_main_data_promotion> getPromotionList(Integer maintab_idx, List<vo_maintab_promotion> MAINTAB_PROMOTION_LIST){
        List<vo_main_data_promotion> result = new ArrayList<vo_main_data_promotion>();
        for(vo_maintab_promotion e : MAINTAB_PROMOTION_LIST){

            Integer tmp2 = e.getMaintab_idx();

            if(tmp2.equals(maintab_idx)){
                vo_main_data_promotion record = new vo_main_data_promotion();
                record.setEndTime(e.getEnd_date());
                record.setImage(e.getUrl());
                record.setPriority(e.getPriority());
                record.setStartTime(e.getStart_date());
                record.setPage_landing_idx(e.getPage_landing_idx());
                record.setTitle(e.getName());
                
                if(!e.getVisible())
                    record.setVisible(0);
                else record.setVisible(1);
                
                result.add(record);
            }
        }

        return result;
    }

    // 메인 탭 항목번호에 해당하는 배너 레코드들을 가지고 옴 
    public List<vo_main_data_banner> getBannerList(
        Integer maintab_idx, 
        List<vo_maintab_banner> MAINTAB_BANNER_LIST, 
        List<dto_promotion_image> PROMOTION_IMAGE_LIST,
        List<dto_event_image_2> EVENT_IMAGE_LIST){

        List<vo_main_data_banner> result = new ArrayList<vo_main_data_banner>();

        for(vo_maintab_banner e : MAINTAB_BANNER_LIST){
            Integer tmp2 = e.getMaintab_idx();
            
            if(tmp2.equals(maintab_idx)){   
                vo_main_data_banner record = new vo_main_data_banner();
                record.setImage(e.getUrl());
                record.setPage_landing_idx(e.getPage_landing_idx());
                record.setPriority(e.getPriority());
                record.setType(e.getBanner_type());
                result.add(record);
            }
        }

        return result;
    }


    @SuppressWarnings("unchecked")
    public List<dto_maintab> getList(Integer pc_id) throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("pc_id", pc_id);
        SELECT_QUERY.buildEqual("visible", 1);
        return (List<dto_maintab>)listService.getDTOList(SELECT_QUERY.toString(), dto_maintab.class);
    }

}
