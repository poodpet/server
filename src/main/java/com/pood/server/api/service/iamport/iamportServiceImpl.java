package com.pood.server.api.service.iamport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.pood.server.api.service.json.*;
import com.pood.server.object.IMP.*;

import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.pood.server.api.service.APICall.*;

@Service("iamportService")
public class iamportServiceImpl implements iamportService {

    Logger logger = LoggerFactory.getLogger(iamportServiceImpl.class);

    @Autowired
    @Qualifier("jsonService")
    jsonService jsonService;

    @Autowired
    @Qualifier("APICallService")
    APICallService APICallService;

    // 아임포트 액세스 토큰 발급
    public String getAccessToken() throws Exception {
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("imp_key", com.pood.server.config.PAYMENT.REST_API_KEY);
        body.add("imp_secret", com.pood.server.config.PAYMENT.REST_API_SECRET);

        String url = com.pood.server.config.host.IAMPORT.TOKEN_URL;
        ResponseEntity<String> http_response = APICallService.post(MediaType.APPLICATION_JSON, new HttpHeaders(), body,
                url);
        IMP_TOKEN TOKEN = (IMP_TOKEN) jsonService.getDecodedObject(http_response.getBody().toString(), IMP_TOKEN.class);
        IMP_TOKEN_RESPONSE TOKEN_RESPONSE = TOKEN.getResponse();

        return TOKEN_RESPONSE.getAccess_token();
    }

    // 특정 CUSTOMER_ID에 대한 빌링키 발급
    public ResponseEntity<String> doSimplePay(String CUSTOMER_UID, IMP IAMPORT) throws Exception {
        String ACCESS_TOKEN = getAccessToken();

        logger.info("*********** REQUEST SIMPLE PAY TO IAMPORT");
        logger.info("IAMPORT TOKEN : " + ACCESS_TOKEN);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", ACCESS_TOKEN);

        MultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>();
        body.add("customer_uid",        CUSTOMER_UID);
        body.add("merchant_uid",        IAMPORT.MERCHANT_UID);
        body.add("amount",              IAMPORT.CALC_AMOUNT.toString());
        body.add("name",                "월간 이용권 정기결제");

        String url = com.pood.server.config.host.IAMPORT.DO_SIMPLE_PAY;
        ResponseEntity<String> http_response = APICallService.post(MediaType.APPLICATION_JSON, headers, body, url);
        return http_response;
    }

    @Override
    public String getPayInfo(String ORDER_NUMBER) throws Exception {
        String ACCESS_TOKEN = getAccessToken();

        logger.info("IAMPORT TOKEN : " + ACCESS_TOKEN);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", ACCESS_TOKEN);

        String url = com.pood.server.config.host.IAMPORT.FIND_PAYMENT + ORDER_NUMBER;

        RestTemplate restTemplate = new RestTemplate();

        return APICallService.get(headers, url).getBody();
    }

    @Override
    public ResponseEntity<String> checkBank(String bank_code, String bank_num) throws Exception {
        String ACCESS_TOKEN = getAccessToken();

        logger.info("IAMPORT TOKEN : " + ACCESS_TOKEN);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", ACCESS_TOKEN);

        String url = com.pood.server.config.host.IAMPORT.VBANK_HOLDER + "?bank_code="+bank_code+"&bank_num="+bank_num;

        ResponseEntity<String> result = null;
        try{
             result = APICallService.get(headers, url);
        }catch(HttpClientErrorException e){
        }

        return result;
    }
    
}
