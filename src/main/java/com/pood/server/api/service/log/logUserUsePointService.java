package com.pood.server.api.service.log;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.log.dto_log_user_use_point;

public interface logUserUsePointService {

    public List<dto_log_user_use_point> getList(pagingSet PAGING_SET, String user_uuid) throws Exception;

    public Integer  getTotalRecordNumber(String user_uuid) throws SQLException;

    public void     init(String user_uuid) throws Exception;

    public Integer  insertRecord(String USER_UUID, Integer TYPE, String TEXT, Integer USE_POINT) throws Exception;

    public String   GET_QUERY_INSERT_RECORD(String USER_UUID, Integer TYPE, String TEXT, Integer USE_POINT);
}
