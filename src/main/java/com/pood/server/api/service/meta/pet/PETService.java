package com.pood.server.api.service.meta.pet;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.meta.pet.dto_pet;
import com.pood.server.dto.meta.pet.dto_pet_2;
import com.pood.server.object.pagingSet;

public interface PETService {
 
    /**************** 반려동물 목록 조회 *****************/
    public List<dto_pet> getPetList(pagingSet PAGING_SET, Integer pc_id, String keyword) throws Exception;

    /**************** 전체 목록 개수 조회 ******************/
    public Integer getTotalRecordNumber(Integer pc_id, String keyword) throws SQLException;

    
    public dto_pet_2 getPetRecord(Integer PET_IDX) throws Exception;

    
}
