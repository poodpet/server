package com.pood.server.api.service.meta.feed;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_feed;

public interface feedService {
    

    /********************** 상품 항목 번호에 해당하는 피드 목록 조회 ******************/
    public List<dto_feed> getFeedList(Integer PRODUCT_IDX) throws Exception;

    
    /**************** 사료 목록 조회 ***************/
    public List<dto_feed> getFeedList(pagingSet PAGING_SET, String feed_name, String brand, Integer product_idx) throws Exception;
    

    /**************** 전체 사료 목록 개수 조회 ******************/
    public Integer getTotalRecordNumber(String feed_name, String brand, Integer product_idx) throws SQLException;


    public dto_feed getFeedRecord(Integer PRODUCT_IDX) throws Exception;


    public List<dto_feed> getList() throws Exception;

 
}
