package com.pood.server.api.service.meta.feed_dm;

import org.springframework.stereotype.Service;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.record.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@Service("feedDMService")
public class feedDMServiceImpl implements feedDMService{

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

}
