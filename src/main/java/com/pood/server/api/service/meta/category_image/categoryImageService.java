package com.pood.server.api.service.meta.category_image;

import java.util.List;

import com.pood.server.object.meta.vo_category_image;

public interface categoryImageService {

    public List<vo_category_image> getList() throws Exception;
    
}
 