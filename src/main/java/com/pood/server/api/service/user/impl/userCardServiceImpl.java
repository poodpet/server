package com.pood.server.api.service.user.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pood.server.api.service.user.userCardService;
import com.pood.server.config.DATABASE;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.user.dto_user_simple_card;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.pood.server.api.service.record.*;
import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.DELETE_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.query.*;

@Service("userCardService")
public class userCardServiceImpl implements userCardService {

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("queryService")
    queryService queryService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_USER_SIMPLE_CARD;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);

    
    
    /*************** 간편결제 카드를 신규 등록합니다. *****************/
    public Integer insertRecord(Integer user_idx, String billing_key, String card_name, String card_number,
            String card_user, Boolean main_card) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_idx", user_idx);
        hashMap.put("billing_key", billing_key);
        hashMap.put("card_name", card_name);
        hashMap.put("card_number", card_number);
        hashMap.put("card_user", card_user);
        hashMap.put("main_card", main_card);
        hashMap.put("card_bg", "");
        
        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }

    /******************* 간편 카드 항목 목록 조회 **************/
    @SuppressWarnings("unchecked")
    public List<dto_user_simple_card> getUserCardList(pagingSet PAGING_SET, Integer user_idx) throws Exception{
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        SELECT_QUERY.buildEqual("user_idx", user_idx);        
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_user_simple_card>)listService.getDTOList(SELECT_QUERY.toString(), dto_user_simple_card.class);
    }

    /************* 간편 카드 항목 전체 개수를 조회합니다. ********************/
    public Integer getTotalRecordNumber(Integer user_idx) throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME); 
        CNT_QUERY.buildEqual("user_idx", user_idx);
        return listService.getRecordCount(CNT_QUERY.toString());
    }



    /*************** 회원 간편결제 카드 항목을 업데이트 합니다. **************/
    public void updateUserCard(String BILLING_KEY, String CARD_NAME, String CARD_NUMBER, String CARD_USER,
            Boolean MAIN_CARD, Integer CARD_IDX) throws SQLException {
                
        // 간편 결제 카드 정보 업데이트
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("billing_key",   BILLING_KEY);
        hashMap.put("card_name",     CARD_NAME);
        hashMap.put("card_number",   CARD_NUMBER);
        hashMap.put("card_user",     CARD_USER);
        hashMap.put("main_card",     MAIN_CARD);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "idx", CARD_IDX.toString());
        updateService.execute(query);
        query = null;
        
    }
 
    public Integer insertRecord(Integer user_idx, String billing_key, String customer_uid, String card_name, String card_number, String card_user, Boolean main_card) throws SQLException{
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("user_idx", user_idx);
        hashMap.put("billing_key", billing_key);
        hashMap.put("customer_uid", customer_uid);
        hashMap.put("card_name", card_name);
        hashMap.put("card_number", card_number);
        hashMap.put("card_user", card_user);
        hashMap.put("main_card", main_card);

        String query = queryService.getInsertQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, com.pood.server.config.PROTOCOL.RECORD_UPDATE);

        Integer result_idx = updateService.insert(query);

        query = null;

        return result_idx;
    }

    @Override
    public void updateRecord(String CARD_NAME, String CARD_NUMBER, String CARD_USER, Boolean MAIN_CARD, String CUSTOMER_UID) throws SQLException {
        Map<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("card_name",     CARD_NAME);
        hashMap.put("card_number",   CARD_NUMBER);
        hashMap.put("card_user",     CARD_USER);
        hashMap.put("main_card",     MAIN_CARD);

        String query = queryService.getUpdateQuery(DEFINE_DB_NAME, DEFINE_TABLE_NAME, hashMap, "customer_uid", CUSTOMER_UID);
        updateService.execute(query);
        query = null;

    }

    @Override
    public void deleteRecord(Integer e) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("idx", Integer.toString(e));
        updateService.execute(DELETE_QUERY.toString());
    }

    @Override
    public void deleteRecordWithUserIDX(Integer USER_IDX) throws SQLException {
        DELETE_QUERY DELETE_QUERY = new DELETE_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        DELETE_QUERY.buildEqual("user_idx", Integer.toString(USER_IDX));
        updateService.execute(DELETE_QUERY.toString());
    }

}
