package com.pood.server.api.service.meta.sick_info.impl;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.config.DATABASE;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.meta.sick_info.sickInfoService;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_sick_info;
import com.pood.server.api.service.record.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("sickInfoService")
public class sickInfoServiceImpl implements sickInfoService {

    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;

    private String DEFINE_TABLE_NAME    = DATABASE.TABLE_SICK_INFO;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);
    
    
    /************* 펫 종류 항목 리스트를 조회합니다. ********************/
    @SuppressWarnings("unchecked")
    public List<dto_sick_info> getSickInfoList(pagingSet PAGING_SET) throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME); 
        SELECT_QUERY.setPagination(PAGING_SET, true);
        return (List<dto_sick_info>)listService.getDTOList(SELECT_QUERY.toString(), dto_sick_info.class);
    }

    /************* 펫 항목 수를 조회합니다. ********************/
    public Integer getTotalRecordNumber() throws SQLException {
        CNT_QUERY CNT_QUERY = new CNT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME); 
        return listService.getRecordCount(CNT_QUERY.toString());
    }
 
    
}
