package com.pood.server.api.service.meta.goods;

import java.sql.SQLException;
import java.util.List;

import com.pood.server.dto.meta.dto_coupon_2;
import com.pood.server.dto.meta.goods.dto_goods_coupon_2;

public interface goodsCouponService {
    
    /**************** 해당 쿠폰이 특정 굿즈에서 사용 가능한지 확인 ******************/
    Boolean checkCouponAvailableWithgoods(Integer COUPON_IDX, Integer GOODS_IDX) throws Exception;



    /********************** 거래에 매핑되어 있는 쿠폰 정보 조회   *********************/
    public List<dto_coupon_2> getgoodsCouponList(Integer GOODS_IDX) throws Exception;
 


    /****************** 쿠폰 굿즈 목록 조회 **************************/
    public List<Integer> getgoodsList(Integer COUPON_IDX) throws SQLException;



    /****************** 특정 굿즈 항목 번호에 해당하는 쿠폰 목록 조회 ***************/
	public List<dto_goods_coupon_2> getList(Integer GOODS_IDX) throws Exception;



	List<Integer> getCouponIDXList(Integer GOODS_IDX) throws Exception;



    List<Integer> getGoodsListWithCouponIDX(Integer COUPON_IDX) throws SQLException;
 
}
