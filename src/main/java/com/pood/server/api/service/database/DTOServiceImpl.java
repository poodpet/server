package com.pood.server.api.service.database;

import com.pood.server.api.service.list.listService;
import com.pood.server.dto.dto_image;
import com.pood.server.dto.dto_image_1;
import com.pood.server.dto.dto_image_2;
import com.pood.server.dto.dto_image_3;
import com.pood.server.dto.dto_image_4;
import com.pood.server.dto.log.user_order.dto_log_user_order_2;
import com.pood.server.dto.meta.order.dto_order_delivery;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("DTOService")
public class DTOServiceImpl implements DTOService {

    @Autowired
    @Qualifier("dataSource")
    private DataSource dataSource;

    @Autowired
    @Qualifier("listService")
    listService listService;

    public List<dto_image> getdtoImageList(String query) throws SQLException {
        Connection conn = null;

        List<dto_image> result = new ArrayList<dto_image>();

        ResultSet RESULT_SET = null;

        try {

            conn = dataSource.getConnection();
            RESULT_SET = conn.createStatement().executeQuery(query);
 
            while (RESULT_SET.next()) {
                dto_image record = new dto_image();
                record.setImage_idx((Integer)RESULT_SET.getInt("idx"));
                record.setRecordbirth((String)RESULT_SET.getString("recordbirth"));
                record.setVisible((Integer)RESULT_SET.getInt("visible"));
                record.setType((Integer)RESULT_SET.getInt("type"));
                record.setUpdatetime((String)RESULT_SET.getString("updatetime"));
                record.setUrl((String)RESULT_SET.getString("url"));
                result.add(record);
                
                record = null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conn.close();
        }

        conn = null;

        return result;
    }

    public List<dto_image_1> getdtoImageList1(String query) throws SQLException {
        Connection conn = null;

        List<dto_image_1> result = new ArrayList<dto_image_1>();

        ResultSet RESULT_SET = null;

        try {

            conn = dataSource.getConnection();
            RESULT_SET = conn.createStatement().executeQuery(query);
 
            while (RESULT_SET.next()) {
                dto_image_1 record = new dto_image_1();
                record.setImage_idx((Integer)RESULT_SET.getInt("idx"));
                record.setRecordbirth((String)RESULT_SET.getString("recordbirth"));
                record.setVisible((Integer)RESULT_SET.getInt("visible"));
                record.setUpdatetime((String)RESULT_SET.getString("updatetime"));
                record.setUrl((String)RESULT_SET.getString("url"));
                result.add(record);
                
                record = null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conn.close();
        }

        conn = null;

        return result;
    }

    public List<dto_image_2> getdtoImageList2(String query) throws SQLException {
        Connection conn = null;

        List<dto_image_2> result = new ArrayList<dto_image_2>();

        ResultSet RESULT_SET = null;

        try {

            conn = dataSource.getConnection();
            RESULT_SET = conn.createStatement().executeQuery(query);
 
            while (RESULT_SET.next()) {
                dto_image_2 record = new dto_image_2();
                record.setImage_idx((Integer)RESULT_SET.getInt("idx"));
                record.setRecordbirth((String)RESULT_SET.getString("recordbirth"));
                record.setVisible((Integer)RESULT_SET.getInt("visible"));
                record.setUpdatetime((String)RESULT_SET.getString("updatetime"));
                record.setUrl((String)RESULT_SET.getString("url"));
                record.setType((Integer)RESULT_SET.getInt("type"));
                record.setPriority((Integer)RESULT_SET.getInt("priority"));
                result.add(record);
                
                record = null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conn.close();
        }

        conn = null;

        return result;
    }
    
    public List<dto_image_4> getdtoImageList4(String query) throws SQLException {
        Connection conn = null;

        List<dto_image_4> result = new ArrayList<dto_image_4>();

        ResultSet RESULT_SET = null;

        try {

            conn = dataSource.getConnection();
            RESULT_SET = conn.createStatement().executeQuery(query);
 
            while (RESULT_SET.next()) {
                dto_image_4 record = new dto_image_4();
                record.setImage_idx((Integer)RESULT_SET.getInt("idx"));
                record.setVisible((Integer)RESULT_SET.getInt("visible"));
                record.setUrl((String)RESULT_SET.getString("url"));
                result.add(record);
                
                record = null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conn.close();
        }

        conn = null;

        return result;
    }

     
 



    /*********** 데이터베이스 SQL 구문 실행 *************/
    public void update(String query) throws SQLException {
        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            conn.prepareStatement(query).executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conn.close();
        }
    }
 
 

    @Override
    public List<dto_image_3> GET_DTO_REVIEW_IMAGE(String query) throws Exception {
        Connection conn = null;

        List<dto_image_3> result = new ArrayList<dto_image_3>();

        ResultSet RESULT_SET = null;

        try {

            conn = dataSource.getConnection();
            RESULT_SET = conn.createStatement().executeQuery(query);
 
            while (RESULT_SET.next()) {
                dto_image_3 record = new dto_image_3();
                record.setImage_idx((Integer)RESULT_SET.getInt("idx"));
                record.setRecordbirth((String)RESULT_SET.getString("recordbirth"));
                record.setVisible((Integer)RESULT_SET.getInt("visible"));
                record.setUpdatetime((String)RESULT_SET.getString("updatetime"));
                record.setUrl((String)RESULT_SET.getString("url"));
                record.setOrder_idx((Integer)RESULT_SET.getInt("order_idx"));
                record.setSeq((Integer)RESULT_SET.getInt("seq"));
                result.add(record);
                
                record = null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conn.close();
        }

        conn = null;

        return result;
    }

    public List<dto_log_user_order_2> GET_DTO_LOG_USER_ORDER(String query) throws Exception{
        Connection conn = null;

        List<dto_log_user_order_2> result = new ArrayList<dto_log_user_order_2>();

        ResultSet RESULT_SET = null;

        try {

            conn = dataSource.getConnection();
            RESULT_SET = conn.createStatement().executeQuery(query);
 
            while (RESULT_SET.next()) {
                dto_log_user_order_2 record = new dto_log_user_order_2();

                record.setGoods_idx((Integer)RESULT_SET.getInt("goods_idx"));
                record.setOrder_status((Integer)RESULT_SET.getInt("order_status"));
                record.setHistory_idx((Integer)RESULT_SET.getInt("idx"));
                record.setUser_uuid((String)RESULT_SET.getString("user_uuid"));
                record.setOrder_number((String)RESULT_SET.getString("order_number"));
                record.setQty((Integer)RESULT_SET.getInt("qty"));
                record.setOrder_text((String)RESULT_SET.getString("order_text"));
                record.setRecordbirth((String)RESULT_SET.getString("recordbirth"));
                
                result.add(record);
                
                record = null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conn.close();
        }

        conn = null;

        return result;
        
    }

    public List<dto_order_delivery> GET_DTO_ORDER_DELIVERY(String query) throws Exception{
        Connection conn = null;

        List<dto_order_delivery> result = new ArrayList<dto_order_delivery>();

        ResultSet RESULT_SET = null;

        try {

            conn = dataSource.getConnection();
            RESULT_SET = conn.createStatement().executeQuery(query);
 
            while (RESULT_SET.next()) {
                dto_order_delivery record = new dto_order_delivery();

                record.setAddress((String)RESULT_SET.getString("address"));
                record.setReceiver((String)RESULT_SET.getString("receiver"));
                record.setRemote_type((Integer)RESULT_SET.getInt("remote_type"));
                record.setRegular_date((String)RESULT_SET.getString("regular_date"));
                record.setInput_type((Integer)RESULT_SET.getInt("input_type"));
                record.setStartdate((String)RESULT_SET.getString("startdate"));
                record.setZipcode((String)RESULT_SET.getString("zipcode"));
                record.setAddress_detail((String)RESULT_SET.getString("address_detail"));
                record.setDefault_address((Integer)RESULT_SET.getInt("default_address"));
                record.setCourier((String)RESULT_SET.getString("courier"));
                record.setDelivery_idx((Integer)RESULT_SET.getInt("idx"));
                record.setNickname((String)RESULT_SET.getString("nickname"));
                record.setPhone_number((String)RESULT_SET.getString("phone_number"));
                record.setUpdatetime((String)RESULT_SET.getString("updatetime"));
                record.setRecordbirth((String)RESULT_SET.getString("recordbirth"));
                
                result.add(record);
                
                record = null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conn.close();
        }

        conn = null;

        return result;
        
    }

  
 
 
}
