package com.pood.server.api.service.view.view_9_1;

import java.util.List;

import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.config.DATABASE;
import com.pood.server.object.meta.vo_promotion_group_goods;
import com.pood.server.api.service.list.listService;
import com.pood.server.api.service.record.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("view91Service")
public class view91ServiceImpl implements view91Service{
    
      
    @Autowired
    @Qualifier("listService")
    listService listService;

    @Autowired
    @Qualifier("updateService")
    updateService updateService;
 
    private String DEFINE_TABLE_NAME    = DATABASE.VIEW_9_1;

    private String DEFINE_DB_NAME       = DATABASE.getDBName(DEFINE_TABLE_NAME);


    @SuppressWarnings("unchecked")
    public List<vo_promotion_group_goods> getList() throws Exception {
        SELECT_QUERY SELECT_QUERY = new SELECT_QUERY(DEFINE_DB_NAME, DEFINE_TABLE_NAME);
        return (List<vo_promotion_group_goods>)listService.getDTOList(SELECT_QUERY.toString(), vo_promotion_group_goods.class);
    }
}
