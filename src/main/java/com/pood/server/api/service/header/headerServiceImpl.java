package com.pood.server.api.service.header;

import org.springframework.stereotype.Service;

@Service("headerService")
public class headerServiceImpl implements headerService {

    @Override
    public Integer checkPagingSet(Integer page_size, Integer page_number, String recordbirth) {
        if ((page_size == null) && (page_number == null)) {
            return 200;
        }

        if (page_size <= 0) {
            return 300;
        }

        if (page_number <= 0) {
            return 300;
        }

        return 200;
    }

}
