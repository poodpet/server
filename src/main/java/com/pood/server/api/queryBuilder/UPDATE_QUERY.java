package com.pood.server.api.queryBuilder;

public class UPDATE_QUERY extends buildQuery{

    private Boolean isWhere;

    public void set(String db_name, String DATABASE){
        this.isWhere = false;
        this.isNULL = true;
        this.query = "UPDATE "+db_name+"."+DATABASE+" SET";
    }

    public void clear(){
        this.isWhere = false;
        this.isNULL = true;
        this.query = null;
    }
    
    /************** update 구문 실행시 updatetime으로 현재 시각 갱신 **********************/
    public void endPoint(){
        if(isNULL) 
            query += " updatetime=now() ";
        else query += ", updatetime=now() ";
    }

    
    /************* update SQL 쿼리 생성 : 인자가 Integer일 때 **********************/
    public void build(String params, Integer object){

        if(object != null){

            if(isNULL) 
                query += " " + params + "=" + object.toString();
            else
                query += ", " + params + "=" + object.toString();

            isNULL = false;
        }
    }



    /*************** update SQL 쿼리 생성 : 인자가 String일 때 **********************/
    public void build(String params, String object){

        if(object != null){

            if(isNULL) 
                query += " " + params + "='" + object.toString() + "'";
            else
                query += ", " + params + "='" + object.toString() + "'";

            isNULL = false;
        }
    }

    /*************** update SQL 쿼리 생성 : 인자가 String일 때 **********************/
    public void build(String params, Double object){

        if(object != null){

            if(isNULL) 
                query += " " + params + "='" + object.toString() + "'";
            else
                query += ", " + params + "='" + object.toString() + "'";

            isNULL = false;
        }
    }
 

    /*************** update SQL 쿼리 생성 : 인자가 Boolean일 때 **********************/
    public void build(String params, Boolean object){

        if(object != null){

            if(isNULL)
                query += " " + params + "=" + object.toString();
            else
                query += ", " + params + "=" + object.toString();

            isNULL = false;
        }
    }
  


    /**************** update SQL 쿼리 생성 : 인자가 Float일 때 **********************/
    public void build(String params, Float object){

        if(object != null){

            if(isNULL) 
                query += " " + params + "=" + object.toString();
            else
                query += ", " + params + "=" + object.toString();

            isNULL = false;
        }
    }



    /************** update SQL 쿼리 생성 : 특정 idx에 대한 항목만 변경할 때 **********************/
    public void buildEqual(String key, Integer value){
        if(!isWhere)
            query += " WHERE " +key+ "=" + value;
        else query += " AND " +key+ "=" + value;
        this.isWhere = true;
    }




    /************* update SQL 쿼리 생성 : 지금 시각으로 변경하고자 할 때 **********************/
    public void buildUpdateTime(String params){
        if(isNULL) 
            query += " " + params + "=now()";
        else
            query += ", " + params + "=now()";

        isNULL = false;
    }


	public void buildEqual(String key, String value) {
        if(!isWhere)
            query += " WHERE " +key+ "='" + value + "'";
        else query += " AND " +key+ "=" + value;
        this.isWhere = true;
	}

    public void addValue(String key, String value){
        if(isNULL) 
            query += " " + key +" = " + key + " + " + value;
        else query += ", " + key +" = " + key + " + " + value;
    }

    public void subtract(String key, String value){
        if(isNULL) 
            query += " " + key +" = " + key + " - " + value;
        else query += ", " + key +" = " + key + " - " + value;
    }
}