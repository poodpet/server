package com.pood.server.api.queryBuilder;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import com.pood.server.config.PROTOCOL;

public class INSERT_QUERY extends buildQuery{
   
    
    public void set(String db_name, String table_name, Map<String, Object> hashMap, Boolean isUpdate){
        

        /****************** 삽입쿼리 칼럼 페어 생성 **********************/
        List<String> KEY_SET = new ArrayList<String>();
        
        List<Object> VALUE_SET = new ArrayList<Object>();
   
   
        Iterator<Map.Entry<String,Object>> it = hashMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Object> pair = it.next();
            KEY_SET.add(pair.getKey());
            VALUE_SET.add(pair.getValue());
        }
    
        String tmp = "";
   
            
   
   
        /****************** 변경 시간이 삽입되는 경우 **********************/
        Integer columns_count = 0;

        tmp = "insert into " + db_name + "." + table_name + " (";
   
        for(String col : KEY_SET){
            columns_count ++;
            tmp += col + ", ";
        }
   
        if(isUpdate == PROTOCOL.RECORD_UPDATE)tmp +="updatetime, ";
               
        tmp += "recordbirth) values (";
               
        if(columns_count != 0){
            for(Object col1 : VALUE_SET){
                if(col1 != null){
                    if (col1.getClass() == String.class) {
                        tmp += "'" + col1.toString() + "', ";
                    }else tmp += col1.toString() + ", ";
                }else tmp +=  "null, ";
            }
        }
   

        if(isUpdate == PROTOCOL.RECORD_UPDATE)
            tmp += "now(), now())";
        else tmp += "now())";
        

        this.query = tmp;

    }
     
}