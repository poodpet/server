package com.pood.server.api.queryBuilder;

import java.util.List;

import com.pood.server.object.pagingSet;

public class SELECT_QUERY extends buildQuery{
 
    private Integer page_size;

    private Integer page_number;

    private String recordbirth;

    public void buildFrom(String table, String identifier){
        this.query = "select * from (" + table + ") " + identifier;
    }

    public void buildFrom(String table, String identifier, String column){
        this.query = "select " +column+ " from (" + table + ") " + identifier;
    }

    public void set(String db_name, String DATABASE){
        this.isNULL = true;
        this.query = "select * from " + db_name + "." + DATABASE;
    }

    public void set(String db_name, String DATABASE, String column){
        this.isNULL = true;
        this.query = "select " +column+ " from " + db_name + "." + DATABASE;
    }

    public void set(String db_name, String DATABASE, List<String> columns){
        this.isNULL = true;
        this.query = "select ";
        
        Boolean isSet = false;
        for(String col : columns){

            if(!isSet){
                this.query += col;
                isSet = true;
            }else
                this.query += ", " + col;
        }

        this.query += " from " + db_name + "." + DATABASE;
    }




    //************ 테이블 별로 레코드 수 조회 ************/
    public void count(String db_name, String DATABASE){
        this.isNULL = true;
        this.query = "select count(*) as cnt from " + db_name + "." + DATABASE;
    }
 
    

    //************ 데이터베이스 테이블 리스트 조회 ************/
    public void showTable(String db_name){
        this.isNULL = true;
        this.query = "SELECT TABLE_NAME FROM INFDTOATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_SCHEMA='"+db_name+"' ";
    }

    //************ 데이터베이스 테이블 칼럼 리스트 조회 ************/
    public void getColumnList(String db, String table){
        this.isNULL = true;
        this.query = "show columns from " + db + "." + table;
    }
    

    
    public SELECT_QUERY(){};

    
    public SELECT_QUERY(String db_name, String DATABASE){
        this.isNULL = true;
        this.query = "select * from " + db_name + "." + DATABASE;
    }

    public SELECT_QUERY(String db_name, String DATABASE, String column){
        this.isNULL = true;
        this.query = "select " + column + " from " + db_name + "." + DATABASE;
    }

    public SELECT_QUERY(String db_name, String DATABASE, List<String> columns){
        this.isNULL = true;
        this.query = "select ";
        
        Boolean isSet = false;
        for(String col : columns){

            if(!isSet){
                this.query += col;
                isSet = true;
            }else
                this.query += ", " + col;
        }

        this.query += " from " + db_name + "." + DATABASE;
    }

    public void setPagination(pagingSet PAGING_SET, Boolean isReverse) {
        this.page_size   = PAGING_SET.getPage_size();
        this.page_number = PAGING_SET.getPage_number();
        this.recordbirth  = PAGING_SET.getRecordbirth();

        if(this.recordbirth != null)
            buildLessThan("recordbirth", recordbirth);

        if(isReverse)
            buildReverseOrder("idx");
        
        if((this.page_size != null) && (this.page_number != null)){
            Integer offset = (this.page_number - 1) * this.page_size;
            buildLimit(this.page_size, offset);
        }
    }

    // PAGING 처리 : KEY 칼럼으로 REVERSE
    public void setPagination(pagingSet PAGING_SET, Boolean isReverse, String key) {
        this.page_size   = PAGING_SET.getPage_size();
        this.page_number = PAGING_SET.getPage_number();
        this.recordbirth  = PAGING_SET.getRecordbirth();

        if(this.recordbirth != null)
            buildLessThan("recordbirth", recordbirth);

        if(isReverse)
            buildReverseOrder(key);
        
        if((this.page_size != null) && (this.page_number != null)){
            Integer offset = (this.page_number - 1) * this.page_size;
            buildLimit(this.page_size, offset);
        }
    }

    public void setPagination2(pagingSet PAGING_SET, Boolean is_ascending, String key) {
        this.page_size   = PAGING_SET.getPage_size();
        this.page_number = PAGING_SET.getPage_number();
        this.recordbirth  = PAGING_SET.getRecordbirth();

        if(this.recordbirth != null)
            buildLessThan("recordbirth", recordbirth);
        
        if(is_ascending) buildASC(key);
        else buildDESC(key);

        if((this.page_size != null) && (this.page_number != null)){
            Integer offset = (this.page_number - 1) * this.page_size;
            buildLimit(this.page_size, offset);
        }
    }


    public void setPagination(String recordbirth, Boolean isReverse){
        this.recordbirth  = recordbirth;
        if(this.recordbirth != null)
            buildLessThan("recordbirth", recordbirth);      
        if(isReverse)
            buildReverseOrder("idx");
    }



} 