package com.pood.server.api.queryBuilder;

import java.util.List;

public class buildQuery {
   
    public Boolean isNULL= true;

    public String query;

    public buildQuery(){

    }

    public void buildSelect(String key, String db_name, String table_name){
        this.query = "SELECT " +key+ ".* FROM "  +db_name+ "." +table_name+ " " +key;
    }

    public void set(String query) {
        this.query = query;
    }

    public void clear(){
        this.isNULL = true;
        this.query = null;
    }

    public void buildJoin(String db_name, String table_name, String key1, String value1, String key2, String value2){
        this.query += " LEFT JOIN " +db_name+ "."  +table_name+ " " +key2+" ON " +key1+ "." +value1+ " = " +key2+ "." +value2;
    }

    public void buildUnion(String sub_query){
        this.query += " UNION " + sub_query;
    }

    public void buildEqual(String table_name, String key, String value){
        if(this.isNULL){
            this.query += " WHERE " +table_name+ "." +key+ "=" +value;
            this.isNULL = false;
        }else this.query+= " and " +table_name+ "." +key+ "=" +value;
    }

    public void buildLike(String table_name, String key, String value){
        if(this.isNULL){
            this.query += " WHERE " +table_name+ "." +key+ " like '%" +value+ "%'";
            this.isNULL = false;
        }else this.query+= " and " +table_name+ "." +key+ " like '%" +value+ "%'";
    }



    /****************** SQL where절 쿼리 생성 : like로 검색 **********************/
    public void buildLimit(Integer limit, Integer offset){
        this.query += " limit " + limit + " offset " + offset;
    }



    /****************** SQL where절 쿼리 생성 : like로 검색 **********************/
    public void buildLike(String key, String value){
        if(isNULL == true){
            this.query += " where " + key + " like '%" +value + "%'";
            isNULL = false;
        }else
            this.query += " and " + key + " like '%" +value + "%'";
    }
    
    public void buildLike(List<String> columns, String value){
        
        String clause = "(";
        Boolean isFirst = true;
        for(String key : columns){
            if(isFirst){
                clause += "( " +key+ " like '%" +value+ "%' )";
                isFirst = false;
            }else{
                clause += " or ( " +key+ " like '%" +value+ "%' )";
            }
        }

        clause += ")";

        if(isNULL == true){
            this.query += " where " + clause;
            isNULL = false;
        }else this.query += " and " + clause;
    }

    
    /****************** SQL where절 쿼리 생성 : 정확히 일치하는 대상만 검색 **********************/
    public void buildEqual(String key, String value){        
        if(isNULL == true){
            this.query += " where " + key + "='" +value+ "'";
            isNULL = false;
        }else
            this.query += " and " + key + "='" +value+ "'";
    }


    /******************  SQL where절 쿼리 생성 : 정확히 일치하는 대상만 검색 **********************/
    public void buildEqual(String key, Integer value){        
        if(isNULL == true){
            this.query += " where " + key + "=" + value;
            isNULL = false;
        }else
            this.query += " and " + key + "=" + value;
    }

    
    /****************** SQL where절 쿼리 생성 : 정확히 일치하는 대상만 검색 **********************/
    public void buildNotEqual(String key, String value){        
        if(isNULL == true){
            this.query += " where " + key + " != '" +value+ "'";
            isNULL = false;
        }else
            this.query += " and " + key + " != '" +value+ "'";
    }


    /******************  SQL where절 쿼리 생성 : 정확히 일치하는 대상만 검색 **********************/
    public void buildNotEqual(String key, Integer value){        
        if(isNULL == true){
            this.query += " where " + key + " != " + value;
            isNULL = false;
        }else
            this.query += " and " + key + " != " + value;
    }

    /****************** SQL where절 쿼리 생성 : key가 value 이상 **********************/
    public void buildMoreThan(String key, Integer value){
        if(isNULL == true){
            this.query += " where " + key + ">=" + value;
            isNULL = false;
        }else
            this.query += " and " + key + ">=" + value;
    }
    public void buildMoreThan(String key, String value){
        if(isNULL == true){
            this.query += " where " + key + ">='" + value + "'";
            isNULL = false;
        }else
            this.query += " and " + key + ">='" + value + "'";
    }
    public void buildMoreThan(String table, String key, String value){
        if(isNULL == true){
            this.query += " where " +table+"." + key + ">=" + value;
            isNULL = false;
        }else
            this.query += " and " +table+"." + key + ">=" + value;
    }




    /******************  SQL where절 쿼리 생성 : key가 value 이하 **********************/
    public void buildLessThan(String key, Integer value){
        if(isNULL == true){
            this.query += " where " + key + "<=" + value;
            isNULL = false;
        }else
            this.query += " and " + key + "<=" + value;

    }
    public void buildLessThan(String key, String value){
        if(isNULL == true){
            this.query += " where " + key + "<='" + value + "'";
            isNULL = false;
        }else
            this.query += " and " + key + "<='" + value + "'";
    }
    public void buildLessThan(String table, String key, String value){
        if(isNULL == true){
            this.query += " where " +table+"." + key + "<=" + value;
            isNULL = false;
        }else
            this.query += " and " +table+"." + key + "<=" + value;

    }


    
    
    /****************** SQL where절 쿼리 생성 : [중괄호]key1이 value1이상 && key2가 value2 이하 **********************/
    public void buildMoreAndLessThan(String key1, Integer value1, String key2, Integer value2){
        if(isNULL == true){
            this.query += " where " + key1 + ">=" + value1 + " and " + key2 + "<=" + value2;
            isNULL = false;
        }else this.query += " and (" + key1 + ">=" + value1 + " and " + key2 + "<=" + value2 + ")";
    }

    public void buildMoreAndLessThan(String key1, String value1, String key2, String value2){
        if(isNULL == true){
            this.query += " where " + key1 + ">='" + value1 + "' and " + key2 + "<='" + value2 + "'";
            isNULL = false;
        }else this.query += " and (" + key1 + ">='" + value1 + "' and " + key2 + "<='" + value2 + "')";
    }

    


    /******************  SQL where절 쿼리 생성 : [중괄호]key1이 value1이상 이거나 key2가 value2 이하 **********************/
    public void buildMoreOrLess(String key1, Integer value1, String key2, Integer value2){
        if(isNULL == true){
            this.query += " where (" + key1 + "<=" + value1 + " or " + key2 + ">=" + value2 + ")";
            isNULL = false;
        }else{
            this.query += " and (" + key1 + "<=" + value1 + " or " + key2 + ">=" + value2 + ")";
        }
    }

    public void buildMoreOrLess(String key1, String value1, String key2, String value2){
        if(isNULL == true){
            this.query += " where (" + key1 + "<='" + value1 + "' or " + key2 + ">='" + value2 + "')";
            isNULL = false;
        }else{
            this.query += " and (" + key1 + "<='" + value1 + "' or " + key2 + ">='" + value2 + "')";
            isNULL = false;
        }
    }


    


    /****************** 역순으로 보이기 ***********************/
    public void buildReverseOrder(String key){
        this.query += " order by " + key + " desc";
    } 

    /****************** ASCEND 보이기 ***********************/
    public void buildASC(String key){
        this.query += " order by " + key + " asc";
    }
    
    public void buildDESC(String key){
        this.query += " order by " + key + " desc";
    }

    /****************** 페이징 ***********************/
    public void buildLimit(Integer LIMIT){
        this.query += " limit " + LIMIT;
    } 
 

    public void buildWhereIn(String column, List<String> list){

        
        Boolean isFirst = true;

        String clause = " where " +column+ " in (";

        for(String str : list){
            String tmp = "";

            if(isFirst){
                tmp += "'" + str + "'";
                isFirst = false;
            }else tmp += ",'" + str + "'";

            clause += tmp;
        }

        clause += ")";

        this.query += clause;
    }

    public void buildWhereIn_Integer(String column, List<Integer> list) {

        Boolean isFirst = true;

        String clause = " where " +column+ " in (";

        for(Integer integer : list){
            String tmp = "";

            if(isFirst){
                tmp += integer.toString();
                isFirst = false;
            }else tmp += "," + integer.toString();

            clause += tmp;
        }

        clause += ")";

        this.query += clause;
	}

    public void buildAndWhereIn_Integer(String column, List<Integer> list) {

        Boolean isFirst = true;

        String clause = " and " +column+ " in (";

        for(Integer str : list){
            String tmp = "";

            if(isFirst){
                tmp += str.toString();
                isFirst = false;
            }else tmp += ", " + str.toString() + "";

            clause += tmp;
        }

        clause += ")";

        this.query += clause;
	}

    public void buildAndWhereIn(String column, List<String> list) {

        Boolean isFirst = true;

        String clause = " and " +column+ " in (";

        for(String str : list){
            String tmp = "";

            if(isFirst){
                tmp += "'" + str + "'";
                isFirst = false;
            }else tmp += ",'" + str + "'";

            clause += tmp;
        }

        clause += ")";

        this.query += clause;
	}


	public void buildMoreLessThan(String table, String column, String min_value, String max_value) {
        buildMoreThan(table, "unit_size", min_value);
        buildLessThan(table, "unit_size", max_value);
	}

    public void buildGroupBy(String column){
        this.query += " group by " +column;
    }
    
    public String toString(){
        return this.query;
    }


    public void buildLikeOR(List<String> column, String key) {

        if(isNULL == true){
            this.query += " where (";
            Boolean isFirst = true;
            for(String e : column){

                if(isFirst){
                    this.query +=  e + " like '%"+key+"%'";
                    isFirst = false;
                }

                else this.query += " or " + e +" like '%"+key+"%'";
            }

            this.query += ")";
            isNULL = false;
        }else{

            this.query += " and (";
            Boolean isFirst = true;
            for(String e : column){

                if(isFirst){
                    this.query +=  e + " like '%"+key+"%'";
                    isFirst = false;
                }

                else this.query += " or " + e +" like '%"+key+"%'";
            }

            this.query += ")";
            isNULL = false;
            
        }
    }

    public void buildOR(List<String> column, String key) {

        if(isNULL == true){
            this.query += " where (";
            Boolean isFirst = true;
            for(String e : column){

                if(isFirst){
                    this.query +=  e + " = "+key;
                    isFirst = false;
                }

                else this.query += " or " + e +" = "+key;
            }

            this.query += ")";
            isNULL = false;
        }else{

            this.query += " and (";
            Boolean isFirst = true;
            for(String e : column){

                if(isFirst){
                    this.query +=  e + " = "+key;
                    isFirst = false;
                }

                else this.query += " or " + e +" = "+key;
            }

            this.query += ")";
            isNULL = false;
            
        }
    }


	public void buildIn(String key, List<Integer> INTEGER_LIST) {
        if(this.isNULL){
            this.query+= " where "+key+" in (";
            this.isNULL = false;
        }else this.query+= " and "+key+" in (";

        Boolean isFirst = true;
        for(Integer e : INTEGER_LIST){
            if(!isFirst)this.query += ", " + e.toString();
            else {
                isFirst = false;
                this.query += e.toString();
            }
        }

        this.query += ")";

	}

    public void buildNotIn(String key, List<Integer> INTEGER_LIST) {
        if(this.isNULL){
            this.query+= " where "+key+" not in (";
            this.isNULL = false;
        }else this.query+= " and "+key+" not in (";

        Boolean isFirst = true;
        for(Integer e : INTEGER_LIST){
            if(!isFirst)this.query += ", " + e.toString();
            else {
                isFirst = false;
                this.query += e.toString();
            }
        }

        this.query += ")";

    }

	public void buildLike(String key, String value, String key1, String value1) {
        if(this.isNULL){
            this.query += " where ((" + key + " like " + "'%" + value + "%') or (" + key1 + " like '%" + value1 + "%' ))";
            this.isNULL = false;
        }else this.query += " and ((" + key + " like " + "'%" + value + "%') or (" + key1 + " like '%" + value1 + "%' ))";

	}


    public void buildORKey(String key, List<String> column) {
        if(this.isNULL){
            this.query+= " where "+key+" in (";
            this.isNULL = false;
        }this.query+= " and " +key+ " in (";

        Boolean isFirst = true;
        for(String e : column){
            if(!isFirst)this.query += ", '" + e.toString()+"'";
            else {
                isFirst = false;
                this.query += "'" + e.toString() + "'";
            }
        }

        this.query += ")";

    }

    public void buildORKey2(String key, List<Integer> column) {
        if(this.isNULL){
            this.query+= " where "+key+" in (";
            this.isNULL = false;
        }else this.query+= " and " +key+" in (";

        Boolean isFirst = true;
        for(Integer e : column){
            if(!isFirst)this.query += ", " + e.toString();
            else {
                isFirst = false;
                this.query += e.toString();
            }
        }

        this.query += ")";

    }

}
