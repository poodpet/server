package com.pood.server.api.queryBuilder;

public class CNT_QUERY extends buildQuery{

 
    public CNT_QUERY(){}

    public void set(String db_name, String DATABASE){
        this.query = "SELECT COUNT(*) as cnt FROM " +db_name+ "." + DATABASE;
    }


    public void getQueryCount(String query){
        this.query = "SELECT COUNT(*) as cnt FROM (" +query+ ") t";
    }

    
    public CNT_QUERY(String db_name, String DATABASE){
        this.query = "SELECT COUNT(*) as cnt FROM " +db_name+ "." + DATABASE;
    }

 
} 