package com.pood.server.api.mapper.user.login;

import java.util.HashMap;

import com.pood.server.api.service.user.*;
import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.user.login.mail.hUser_mail_login_1;
import com.pood.server.config.*;
import com.pood.server.config.meta.user.META_USER_LOGIN_LOG;
import com.pood.server.dto.user.dto_user_1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class mail_1 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(mail_1.class);
    
    @Autowired
    @Qualifier("userLoginService")
    userLoginService userLoginService;
    
    @Autowired
    @Qualifier("userTokenService")
    userTokenService userTokenService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/login/mail/1", method = RequestMethod.POST)
    public HashMap<String, Object> USER_MAIL_LOGIN(@RequestBody hUser_mail_login_1 header, HttpServletRequest request)
        throws Exception {
        

        try{

            procInit(request);
            
            response = getResponse(200);
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_LOGIN_MAIL_1){
                
                    response = getResponse(200);


                    logger.info(header.toString());
                    
                    String email        = header.getEmail();
                    
                    String password     = header.getPassword();

                    Integer login_type  = header.getLogin_type();
                    
                
                    // 이메일이 유효하지 않으면 에러 반환
                    if(!regexService.validate(email)){
                        response = getResponse(201);
                        return response;
                    }

                    // 회원 조회 쿼리문 생성
                    dto_user_1 record = userService.getUser2("user_email", email, login_type);

                    if(record== null){
                        response = getResponse(203);
                        return response;
                    }

                    String userBaseImg = "";
                    Long user_base_image_idx = record.getUser_base_image_idx();
                    if(user_base_image_idx != null){
                        userBaseImg = userService.getUserBaseImg(user_base_image_idx);
                    }else{
                        userBaseImg = userService.getUserBaseImg(1L);
                    }
                    record.setUserBaseImg(userBaseImg);

                    Integer STATUS_CODE = userService.validation(record, 2, password);

                    if(STATUS_CODE != 200){
                        response = getResponse(STATUS_CODE);
                        return response;
                    }
                            
                    
                    Integer USER_IDX = record.getIdx();

                    String USER_UUID = record.getUser_uuid();


                    // 회원 로그인 기록 삽입
                    userLoginService.insertRecord(USER_IDX, USER_UUID, META_USER_LOGIN_LOG.LOG_IN);
                    String TOKEN = userTokenService.issueToken(USER_UUID);
                    record.setToken(TOKEN);
                    response.put("result", record);
                    record = null; 
            
            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }


        
        return response;
    }
}