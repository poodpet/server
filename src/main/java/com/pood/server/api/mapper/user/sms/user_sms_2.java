package com.pood.server.api.mapper.user.sms;

import com.pood.server.api.service.time.timeService;
import com.pood.server.api.service.user.userSMSAuthService;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import com.pood.server.dto.user.dto_user_sms_auth;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_sms_2 extends PROTOCOL {

    Logger logger = LoggerFactory.getLogger(user_sms_2.class);

    @Autowired
    @Qualifier("userSMSAuthService")
    userSMSAuthService userSMSAuthService;

    @Autowired
    @Qualifier("timeService")
    timeService timeService;


    @ResponseBody
    @RequestMapping(value = "/pood/user/sms/2", method = RequestMethod.GET)
    public HashMap<String, Object> SEND_MESSAGE(HttpServletRequest request,
            @RequestParam(value = "phone_number", required = false) String PHONE_NUMBER,
            @RequestParam(value = "auth_number", required = false) String AUTH_NUMBER)
            throws Exception {

        try{

            procInit(request);
            
            response = getResponse(200);

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_SMS_2){        
                dto_user_sms_auth USER_SMS_AUTH = userSMSAuthService.getSMSAuth(PHONE_NUMBER);

                if(USER_SMS_AUTH.getRecordbirth() != null){
                    String PREVIOUS_DATETIME = USER_SMS_AUTH.getUpdatetime();
                    // String CURRENT_DATETIME = timeService.getCurrentTime();

                    logger.info("PHONE NUMBER : " + PHONE_NUMBER);
                    logger.info("THE TIME AUTHENTICATION TOKEN CREATED : " + PREVIOUS_DATETIME);
                    //long DIFF_SECONDS = timeService.getTimeDifference(PREVIOUS_DATETIME, CURRENT_DATETIME);

                    /*  215 처리
                        if(DIFF_SECONDS >= 180)
                            response = getResponse(215);
                    */
                    // 유저 인증 코드 임시 패스
                    logger.info(USER_SMS_AUTH.toString());

                    logger.info(AUTH_NUMBER);
                    if(AUTH_NUMBER.equals("9751")) {
                        userSMSAuthService.updateAuthSuccess(USER_SMS_AUTH.getIdx());
                    } else {
                        if(!USER_SMS_AUTH.getSms_auth().equals(AUTH_NUMBER))
                            response = getResponse(213);
                        else if(USER_SMS_AUTH.getSms_status() == com.pood.server.config.meta.user.USER_SMS_AUTH.SUCCESS)
                            response = getResponse(216);
                        else userSMSAuthService.updateAuthSuccess(USER_SMS_AUTH.getIdx());           // 인증 성공했을 경우 인증 성공 업데이트
                    }

                    }else response = getResponse(217);
                } else response = getResponse(219);

                procClose(request, response.toString());

            }catch(Exception e){
                thExecption(request.getServletPath(), e);
            }


            return response;
        }


    }
