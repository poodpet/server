package com.pood.server.api.mapper.order.exchange;

import com.pood.server.api.req.header.order.exchange.OrderExchangeDelivery;
import com.pood.server.api.req.header.order.exchange.OrderExchangeDto;
import com.pood.server.api.req.header.order.exchange.OrderExchangeGoods;
import com.pood.server.api.service.log.logUserOrderService;
import com.pood.server.api.service.meta.order.OrderService;
import com.pood.server.api.service.meta.order.orderDeliveryService;
import com.pood.server.api.service.meta.order.orderDeliveryTrackService;
import com.pood.server.api.service.meta.order.orderExchangeService;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import com.pood.server.config.meta.order.ORDER_DELIVERY_ADDRESS_TYPE;
import com.pood.server.config.meta.order.ORDER_STATUS;
import com.pood.server.config.meta.order.ORDER_STATUS_MESSAGE;
import com.pood.server.dto.meta.order.dto_order;
import com.pood.server.object.meta.vo_order_exchange;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class order_exchange_1 extends PROTOCOL {

    @Autowired
    @Qualifier("orderExchangeService")
    orderExchangeService orderExchangeService;


    @Autowired
    @Qualifier("orderDeliveryService")
    orderDeliveryService orderDeliveryService;


    @Autowired
    @Qualifier("logUserOrderService")
    logUserOrderService logUserOrderService;


    @Autowired
    @Qualifier("orderService")
    OrderService orderService;

    @Autowired
    @Qualifier("orderDeliveryTrackService")
    orderDeliveryTrackService orderDeliveryTrackService;

    private final Logger logger = LoggerFactory.getLogger(order_exchange_1.class);

    @ResponseBody
    @PostMapping(value = "/pood/order/exchange/1")
    public HashMap<String, Object> ADD_ORDER_EXCHANGE_1(
        @Valid @RequestBody OrderExchangeDto orderExchangeDto, HttpServletRequest request)
        throws Exception {

        try {

            Integer resp_status = procInit(request);

            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if (resp_status != 200) {
                procClose(request, response.toString());
                return response;
            }

            logger.info("*********** 교환 신청 로그 *************");

            if (PROTOCOL_IDENTIFIER.PROTOCOL_ORDER_EXCHANGE_1) {
                String orderNumber = orderExchangeDto.getOrder_number();
                //order Index 쿼리
                int orderIdx = orderService.getOrderINDEX(orderNumber);

                dto_order order = orderService.getOrder(orderNumber);

                int userIdx = order.getUser_idx();

                String userUuid = userService.getUserUUID(userIdx);

                List<OrderExchangeGoods> orderExchangeGoodsList = orderExchangeDto.getGoods();

                OrderExchangeDelivery PICKUP_DELIVERY = orderExchangeDto.getPickup_delivery();

                OrderExchangeDelivery SEND_DELIVERY = orderExchangeDto.getSend_delivery();

                // 전에 교환 요청 기록이 있으면 에러 반환
                if (orderExchangeGoodsList != null) {
                    for (OrderExchangeGoods e : orderExchangeGoodsList) {
                        Integer GOODS_IDX = e.getGoods_idx();
                        vo_order_exchange tmp = orderExchangeService.getRecord(GOODS_IDX,
                            orderNumber);

                        if (tmp != null) {

                            // 있으면 252 에러
                            response = getResponse(252);
                            procClose(request, response.toString());
                            return response;
                        }

                        GOODS_IDX = null;
                    }

                } else {
                    response = getResponse(251);
                    procClose(request, response.toString());
                    return response;
                }

                Integer SEND_TYPE = orderExchangeDto.getDelivery_type();

                Integer order_status = order.getOrder_status();

                // 배송 완료가 되지 않았는데 교환 신청이 이루어질 경우 
                if (!order_status.equals(ORDER_STATUS.DELIVERY_SUCCESS)) {
                    response = getResponse(244);
                    procClose(request, response.toString());
                    return response;
                }

                order_status = null;

                String DELIVERY_TYPE_TEXT = "";

                if (SEND_TYPE == 0) {
                    DELIVERY_TYPE_TEXT = "고객이 보내는 경우";
                }

                if (SEND_TYPE == 1) {
                    DELIVERY_TYPE_TEXT = "상품 회수가 필요한 경우";
                }

                logger.info("교환 신청 택배 타입 : " + DELIVERY_TYPE_TEXT + ", 주문 번호 : " + orderNumber
                    + ", 회원 UUID : " + userUuid);

                /******************* 굿즈 리스트 **********************/
                if (orderExchangeGoodsList != null) {

                    for (OrderExchangeGoods e : orderExchangeGoodsList) {

                        Integer GOODS_IDX = e.getGoods_idx();

                        Integer QTY = e.getQty();

                        String TEXT = e.getText();

                        // 교환 요청시 상품별로 교환 레코드 추가
                        String EXCHANGE_NUMBER = orderExchangeService.insertRecord(TEXT,
                            orderNumber, GOODS_IDX, QTY, SEND_TYPE, e.getAttr());

                        logUserOrderService.insertOrderHistory(
                            order.getIdx(),
                            ORDER_STATUS.EXCHANGE_REQUEST,
                            ORDER_STATUS_MESSAGE.EXCHANGE_REQUEST_MESSAGE(EXCHANGE_NUMBER),
                            orderNumber,
                            userUuid,
                            GOODS_IDX,
                            QTY
                        );

                        logger.info(
                            "굿즈 항목번호[" + GOODS_IDX + "], 굿즈 수량[" + QTY + "], 굿즈 교환 신청 사유 : " + TEXT
                                + " - 교환번호[" + EXCHANGE_NUMBER + "] 발급");

                        /******************* 교환 배송지 정보 등록 **********************/
                        // 배송지 타입이 집하를 해야하는 경우
                        if (SEND_TYPE == 1) {
                            if (PICKUP_DELIVERY != null) {
                                orderDeliveryService.insertRecord(EXCHANGE_NUMBER, "", orderNumber,
                                    orderIdx, PICKUP_DELIVERY,
                                    ORDER_DELIVERY_ADDRESS_TYPE.EXCHANGE_SENDER);
                                logger.info("집하 배송 정보 : " + PICKUP_DELIVERY);
                            }
                        }

                        /******************* 교환 배송지 정보 등록 **********************/
                        // 배송지 타입이 고객이 보내는 경우
                        if (SEND_TYPE == 0) {
                            if (SEND_DELIVERY != null) {
                                orderDeliveryService.insertRecord(EXCHANGE_NUMBER, "", orderNumber,
                                    orderIdx, SEND_DELIVERY,
                                    ORDER_DELIVERY_ADDRESS_TYPE.EXCHANGE_PICK_UP);
                                logger.info("집하 배송 정보 : " + SEND_DELIVERY);
                            }
                        }

                        // 배송지 타입이 집하를 해야하는 경우
                        if (SEND_TYPE == 1) {
                            if (SEND_DELIVERY != null) {
                                orderDeliveryService.insertRecord(EXCHANGE_NUMBER, "", orderNumber,
                                    orderIdx, SEND_DELIVERY,
                                    ORDER_DELIVERY_ADDRESS_TYPE.EXCHANGE_SENDER);
                                logger.info("집하 배송 정보 : " + SEND_DELIVERY);
                            }
                        }

                    }
                }

            } else {
                response = getResponse(219);
            }

            procClose(request, response.toString());

        } catch (Exception e) {
            thExecption(request.getServletPath(), e);
        }

        return response;
    }

}