package com.pood.server.api.mapper.order.delivery_image;

import com.pood.server.api.service.meta.order.orderDeliveryService;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class delivery_image_1 extends PROTOCOL {

    Logger logger = LoggerFactory.getLogger(delivery_image_1.class);
 
    @Autowired
    @Qualifier("orderDeliveryService")
    orderDeliveryService orderDeliveryService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/order/delivery/image/1", method = RequestMethod.POST)
    public HashMap<String, Object> ADD_ORDER_DELIVERY_IMAGE_1(
        @RequestParam("file") MultipartFile[] file, HttpServletRequest request) throws Exception {
        

        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_ORDER_DELIVERY_IMAGE_1){
                List<HashMap<String, Object>> result = new ArrayList<HashMap<String, Object>>();

                // 이미지 파일 업로드
                for(MultipartFile f:file){

                    HashMap<String, Object> record = new HashMap<>();
                    String img_url = storageService.multiPartFileStore(f, "order_delivery");
                    record.put("url", img_url);

                    logger.info(img_url);
                    
                    Integer image_idx = orderDeliveryService.insertOrderImage(img_url);
                    
                    record.put("image_idx", image_idx);
                    result.add(record);   
                }
            
                response.put("result", result);
            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }


        return response;
    }
 
}
