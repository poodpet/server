package com.pood.server.api.mapper.user.claim;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.user.claim.hUser_claim_1;
import com.pood.server.api.service.user.userClaimService;
import com.pood.server.config.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController 
public class user_claim_1 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_claim_1.class);

    @Autowired
    @Qualifier("userClaimService")
    userClaimService userClaimService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/claim/1", method = RequestMethod.POST)
    public HashMap<String, Object> ADD_USER_CLAIM(@RequestBody hUser_claim_1 header, HttpServletRequest request)
            throws Exception {
        
        
        try{

            Integer resp_status = procInit(request);
            
            logger.info("회원 문의 등록 : "+header.toString());
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_CLAIM_1){
                String user_uuid = header.getUser_uuid();

                Integer user_idx = userService.getUserIDX(user_uuid);

                if(user_idx == null){
                    response = getResponse(203);
                    return response;
                }
        
                Integer claim_idx = userClaimService.insertUserClaim(user_idx, user_uuid, header);
                    
                if(claim_idx == -1){
                    // 삽입시 NULL이 들어갈 경우 231 에러
                    response = getResponse(231);
                    procClose(request, (String)response.get("msg"));
                    return response;
                }

            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        

       return response;
    }
}