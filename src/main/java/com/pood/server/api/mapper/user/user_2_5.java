package com.pood.server.api.mapper.user;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.api.service.log.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_2_5 extends PROTOCOL{
 
    @Autowired
    @Qualifier("logUserOrderService")
    logUserOrderService logUserOrderService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/2/5", method = RequestMethod.GET)
    public HashMap<String, Object> GET_PAY_SUCCESS(
        @RequestParam(value="user_uuid", required=false) String user_uuid,
        HttpServletRequest request) throws Exception {
    
        try{

            procInit(request);

            response = getResponse(200);
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_2_5){        
                Boolean isPaySuccess = logUserOrderService.getPaySuccessCnt(user_uuid);
                response.put("pay_success", isPaySuccess);
//                response.put("pay_success", Boolean.FALSE);  //테스트를 위해 잠시 변경 절대 커밋 하지 말것!!!!!
                isPaySuccess = null;
            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        
        return response;
    }
}  