package com.pood.server.api.mapper.user.login;

import com.pood.server.api.service.user.*;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.admin.aes_enc;
import com.pood.server.api.req.header.user.login.auto.hUser_auto_login_1;
import com.pood.server.config.*;
import com.pood.server.config.meta.user.META_USER_LOGIN_LOG;
import com.pood.server.dto.user.dto_user_1;
import com.pood.server.api.service.json.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class auto_1_1 extends PROTOCOL {

    Logger logger = LoggerFactory.getLogger(auto_1_1.class);

    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("userLoginService")
    userLoginService userLoginService;

    @Autowired
    @Qualifier("jsonService")
    jsonService jsonService;

    @Autowired
    @Qualifier("userTokenService")
    userTokenService userTokenService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/login/auto/1/1", method = RequestMethod.POST)
    public HashMap<String, Object> USER_AUTO_LOGIN(@RequestBody aes_enc encoded_header, HttpServletRequest request) throws Exception{
     

        try{

            procInit(request);
            
            response = getResponse(200);
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_LOGIN_AUTO_1_1){
                
                String AES_DECODER = AES256.aesDecode(encoded_header.getEnc());

                hUser_auto_login_1 header = 
                    (hUser_auto_login_1) jsonService.getDecodedObject(AES_DECODER, hUser_auto_login_1.class);

                logger.info(header.toString());

                String email        = header.getEmail();

                Integer login_type  = header.getLogin_type();



                // 이메일이 유효하지 않으면 에러 반환
                if(!regexService.validate(email)) {
                    response = getResponse(201);
                    return response;
                }
        
                

                dto_user_1 record = userService.getUser2("user_email", email, login_type);

                Integer STATUS_CODE = userService.validation(record, 1, header.getUuid());
                        
                if(STATUS_CODE != 200){
                    response = getResponse(STATUS_CODE);
                    return response;
                }



                String USER_UUID = record.getUser_uuid();

                Integer USER_IDX = record.getIdx();
                
                // 회원 로그인 기록 삽입
                userLoginService.insertRecord(USER_IDX, USER_UUID, META_USER_LOGIN_LOG.LOG_AUTO_IN);

                String TOKEN = userTokenService.issueToken(USER_UUID);
                record.setToken(TOKEN);
                response.put("result", record);
                record = null;

                        
            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        
        
        return response;
    }

}