package com.pood.server.api.mapper.user.sms;

import com.pood.server.api.service.json.jsonService;
import com.pood.server.api.service.time.timeService;
import com.pood.server.api.service.user.userSMSAuthService;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import com.pood.server.object.api.API_RESPONSE;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class user_sms_1 extends PROTOCOL {

    @Value("${legacy.server-ip}")
    private String serverIp;

    Logger logger = LoggerFactory.getLogger(user_sms_1.class);

    @Autowired
    @Qualifier("userSMSAuthService")
    userSMSAuthService userSMSAuthService;

    @Autowired
    @Qualifier("timeService")
    timeService timeService;

    @Autowired
    @Qualifier("jsonService")
    jsonService jsonService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/sms/1", method = RequestMethod.GET)
    public HashMap<String, Object> SEND_MESSAGE(HttpServletRequest request,
        @RequestParam(value = "phone_number", required = false) String PHONE_NUMBER)
        throws Exception {

        try {

            procInit(request);

            response = getResponse(200);

            if (PROTOCOL_IDENTIFIER.PROTOCOL_USER_SMS_1) {

                Boolean chk = userSMSAuthService.isRecordbirthValid(PHONE_NUMBER);

                if (chk) {

                    String url = serverIp + "/pood/user/sms/1?phone_number=" + PHONE_NUMBER;
                    logger.info("CALL ADMIN SERVER API : " + url);
                    String result = new RestTemplate().getForObject(url, String.class);
                    API_RESPONSE resp = (API_RESPONSE) jsonService.getDecodedObject(result,
                        API_RESPONSE.class);
                    if (!(resp.getStatus() == 200)) {
                        response = getResponse(resp.getStatus());
                    }

                } else {
                    response = getResponse(235);
                }

            } else {
                response = getResponse(219);
            }

            procClose(request, response.toString());

        } catch (Exception e) {
            thExecption(request.getServletPath(), e);
        }

        return response;
    }


} 
