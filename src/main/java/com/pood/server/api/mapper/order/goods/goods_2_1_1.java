package com.pood.server.api.mapper.order.goods;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.req.header.order.goods.hAM_goods_2_1;
import com.pood.server.api.service.view.view_2.*;
import com.pood.server.config.*;
import com.pood.server.object.pagingSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController; 
import com.pood.server.dto.meta.goods.dto_goods_7;

@RestController
public class goods_2_1_1 extends PROTOCOL {

    Logger logger = LoggerFactory.getLogger(goods_2_1_1.class);

    @Autowired
    @Qualifier("view2Service")
    view2Service view2Service;
  
    @ResponseBody
    @RequestMapping(value = "/pood/order/goods/2/1/1", method = RequestMethod.POST)
    public HashMap<String, Object> LIST_goods(
        @RequestBody hAM_goods_2_1 header, HttpServletRequest request) throws Exception {

        try{

            procInit(request);
                
            response = getResponse(200);

            Integer headerCheck = headerService.checkPagingSet(header.getPage_size(), header.getPage_number(), header.getRecordbirth());

            if(headerCheck != 200){
                response = getResponse(headerCheck);
                procClose(request, response.toString());
                return response;
            } 

            logger.info(header.toString());
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_ORDER_GOODS_2_1_1){

                SELECT_QUERY query = view2Service.getQuery2(header);

                Integer total_cnt = getListTotalCount(query.toString());
                
                pagingSet PAGING_SET = new pagingSet(header.getPage_size(), header.getPage_number(), header.getRecordbirth(), total_cnt);  
                List<dto_goods_7> result =  view2Service.getGoodsList(PAGING_SET, query);
                response.put("result", result);
                pagingResult(PAGING_SET);
                result = null;
                

            }else response = getResponse(219);
            
            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        
            

        return response;
    }


    @ResponseBody
    @RequestMapping(value = "/pood/order/goods/getSortedGoodsList", method = RequestMethod.POST)
    public HashMap<String, Object> Sorted_List_goods(
        @RequestBody hAM_goods_2_1 header, HttpServletRequest request) throws Exception {

        try{

            procInit(request);
                
            response = getResponse(200);

            Integer headerCheck = headerService.checkPagingSet(header.getPage_size(), header.getPage_number(), header.getRecordbirth());

            if(headerCheck != 200){
                response = getResponse(headerCheck);
                procClose(request, response.toString());
                return response;
            } 

            logger.info(header.toString());
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_ORDER_GOODS_2_1_1){

                SELECT_QUERY query = view2Service.getQuery2(header);
                logger.info("##########" + query.toString());
                Integer total_cnt = getListTotalCount(query.toString());
                
                pagingSet PAGING_SET = new pagingSet(header.getPage_size(), header.getPage_number(), header.getRecordbirth(), total_cnt);  
                List<dto_goods_7> result =  view2Service.getGoodsList2(PAGING_SET, query, header.getSort_type());
                response.put("result", result);
                pagingResult(PAGING_SET);
                result = null;
                

            }else response = getResponse(219);
            
            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        
            

        return response;
    }

    // 리스트 총 개수 반환 
    public Integer getListTotalCount(String query) throws SQLException{
        CNT_QUERY CNT_QUERY = new CNT_QUERY();
        CNT_QUERY.getQueryCount(query);
        logger.info("######### CNT_QUERY " + CNT_QUERY.toString());
        Integer total_cnt = listService.getRecordCount(CNT_QUERY.toString());
        if(total_cnt == null)total_cnt = 0; 
        return total_cnt;
    }
}  