package com.pood.server.api.mapper.admin.brand;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.meta.brand.brandService;
import com.pood.server.config.*;
import com.pood.server.dto.meta.brand.dto_brand;
import com.pood.server.object.pagingSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class brand_2 extends PROTOCOL {
     
    
    @Autowired
    @Qualifier("brandService")
    brandService brandService; 

    @ResponseBody
    @RequestMapping(value = "/pood/admin/brand/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_POINT(HttpServletRequest request,
    @RequestParam(value="idx", required=false) Integer idx,
    @RequestParam(value="page_size", required=false) Integer page_size, 
    @RequestParam(value="page_number", required=false) Integer page_number,
    @RequestParam(value="recordbirth", required=false) String recordbirth) throws Exception {
    
    
        try{
                
            procInit(request);
            
            response = getResponse(200);

            Integer headerCheck = headerService.checkPagingSet(page_size, page_number, recordbirth);

            if(headerCheck != 200){
                response = getResponse(headerCheck);
                procClose(request, response.toString());
                return response; 
            }

            if(PROTOCOL_IDENTIFIER.PROTOCOL_ADMIN_BRAND_2){
                pagingSet PAGING_SET = new pagingSet(
                        page_size, 
                        page_number, 
                        recordbirth,
                        brandService.getTotalRecordNumber());
                        
                List<dto_brand> result = brandService.getBrandList(PAGING_SET, idx);
                response.put("result", result);
                result = null;

                pagingResult(PAGING_SET); 

            }else response = getResponse(219);
            
            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }


        
        return response;
    }
}  