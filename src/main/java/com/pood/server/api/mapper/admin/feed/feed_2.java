package com.pood.server.api.mapper.admin.feed;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.meta.feed.*;
import com.pood.server.config.*;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_feed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class feed_2 extends PROTOCOL {


    @Autowired
    @Qualifier("feedService")
    feedService feedService;
     
    @ResponseBody
    @RequestMapping(value = "/pood/admin/feed/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_FEED(
        HttpServletRequest request,
        @RequestParam(value="page_size", required=false) Integer page_size, 
        @RequestParam(value="page_number", required=false) Integer page_number,
        @RequestParam(value="feed_name", required=false) String feed_name,
        @RequestParam(value="recordbirth", required=false) String recordbirth,
        @RequestParam(value="product_idx", required=false) Integer product_idx,
        @RequestParam(value="brand", required=false) String brand) throws Exception {


        try{
                
            procInit(request);

            response = getResponse(200);

            Integer headerCheck = headerService.checkPagingSet(page_size, page_number, recordbirth);

            if(headerCheck != 200){
                response = getResponse(headerCheck);
                procClose(request, response.toString());
                return response;
            }
    
            if(PROTOCOL_IDENTIFIER.PROTOCOL_ADMIN_FEED_2){
                pagingSet PAGING_SET = new pagingSet(
                        page_size, 
                        page_number, 
                        recordbirth,
                        feedService.getTotalRecordNumber(feed_name, brand, product_idx));
                
                // 사료 조회 쿼리문 생성
                List<dto_feed> result = feedService.getFeedList(PAGING_SET, feed_name, brand, product_idx);                
                response.put("result", result);
                pagingResult(PAGING_SET); 
                result = null;

            }else response = getResponse(219);

            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }


        
        return response;
    }

}  