package com.pood.server.api.mapper.user.noti;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.user.noti.hUser_noti_1_1;
import com.pood.server.api.service.user.userNotiService;
import com.pood.server.config.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_noti_1_1 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_noti_1_1.class);

    @Autowired
    @Qualifier("userNotiService")
    userNotiService userNotiService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/noti/1/1", method = RequestMethod.POST)
    public HashMap<String, Object> CHANGE_USER_NOTI_STATUS(@RequestBody hUser_noti_1_1 header, HttpServletRequest request)
            throws Exception {
        

        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_NOTI_1_1){

                logger.info(header.toString());

                List<Integer> NOTI_IDX_LIST = header.getIdx();

                if(NOTI_IDX_LIST != null)
                    for(Integer e : NOTI_IDX_LIST)
                        userNotiService.updateReadStatus(e);
                
                
            }else response = getResponse(219);
    

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        
        
        
        return response;
    }
}