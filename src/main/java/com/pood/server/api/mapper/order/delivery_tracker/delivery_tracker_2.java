package com.pood.server.api.mapper.order.delivery_tracker;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.api.service.delivery_tracker.*;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class delivery_tracker_2 extends PROTOCOL{
 

    @Autowired
    @Qualifier("deliveryTrackerService")
    deliveryTrackerService deliveryTrackerService;

    @ResponseBody
    @RequestMapping(value = "/pood/order/delivery-tracker/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_ORDER_DELIVERY_TRACKER(
        @RequestParam("carrier") String carrier, 
        @RequestParam("tracking_id") String tracking_id, HttpServletRequest request)
            throws Exception {
    

        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_DELIVERY_TRACKER_2){
                String result = deliveryTrackerService.getTrackingRecord(carrier, tracking_id);
                if(result != null){
                    Map<String, Object> record = getJsonMap(result);
                    response.put("result", record);
                    record = null;
                }else response = getResponse(250);
            }else response = getResponse(219);


            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        

        return response;
    }
}