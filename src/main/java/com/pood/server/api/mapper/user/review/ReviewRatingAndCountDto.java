package com.pood.server.api.mapper.user.review;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ReviewRatingAndCountDto {

    private Double avgRating;

    private Long totalCnt;

    public ReviewRatingAndCountDto convertAvgRating() {
        avgRating = Math.round(avgRating * 10) / 10.0;
        return this;
    }

    public ReviewRatingAndCountDto(final Double avgRating, final Long totalCnt) {
        this.avgRating = avgRating;
        this.totalCnt = totalCnt;
    }
}
