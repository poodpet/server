package com.pood.server.api.mapper.user;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.object.user.vo_user_info;
import com.pood.server.api.service.user.userService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_2_1 extends PROTOCOL{
 
    @Autowired
    @Qualifier("userService")
    userService userService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/2/1", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_USER(
        @RequestParam(value="user_uuid", required=false) String user_uuid,
        HttpServletRequest request) throws Exception {
    
        try{

            procInit(request);

            response = getResponse(200);
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_2_1){        
                List<vo_user_info> result = userService.getUser2(user_uuid);
                response.put("result", result);
                result = null;
            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        
        return response;
    }
}  