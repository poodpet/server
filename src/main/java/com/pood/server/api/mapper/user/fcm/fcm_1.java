package com.pood.server.api.mapper.user.fcm;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.admin.fcm.hAM_fcm_1;
import com.pood.server.config.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class fcm_1 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(fcm_1.class);
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/fcm/1", method = RequestMethod.POST)
    public HashMap<String, Object> ADD_USER_FCM_TOKEN(@RequestBody hAM_fcm_1 header, HttpServletRequest request)
            throws Exception {


        try{

            logger.info(header.toString());
            
            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_FCM_1){
                Integer USER_IDX    = userService.getUserIDX(header.getUser_uuid());
                String  DEVICE_KEY  = header.getDevice_key();
                Integer DEVICE_TYPE = header.getDevice_type();
                userService.updateUserFCM(USER_IDX, DEVICE_KEY, DEVICE_TYPE);
            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        
        
        return response;
    }
}