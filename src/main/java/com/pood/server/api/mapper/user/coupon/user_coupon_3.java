package com.pood.server.api.mapper.user.coupon;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.user.coupon.hUser_coupon_3;
import com.pood.server.config.*;
import com.pood.server.api.service.user.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_coupon_3 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_coupon_3.class);

    @Autowired
    @Qualifier("userCouponService")
    userCouponService userCouponService;
  
    @ResponseBody
    @RequestMapping(value = "/pood/user/coupon/3", method = RequestMethod.POST)
    public HashMap<String, Object> DELETE_USER_COUPON(@RequestBody hUser_coupon_3 header, HttpServletRequest request)
            throws Exception {
         

        try{

            Integer resp_status = procInit(request);

            logger.info(header.toString());
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(header.getIdx() == null){
                response = getResponse(300);
                procClose(request, response.toString());
                return response;
            }

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_COUPON_3){   
                // 회원 쿠폰 삭제
                for(Integer e:header.getIdx())
                    userCouponService.deleteRecord(e);
            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        
        return response;
    }
}