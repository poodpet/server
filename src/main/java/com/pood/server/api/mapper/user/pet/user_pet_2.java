package com.pood.server.api.mapper.user.pet;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.user.userPetService;
import com.pood.server.config.*;
import com.pood.server.object.resp.resp_user_pet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_pet_2 extends PROTOCOL{

    @Autowired
    @Qualifier("userPetService")
    userPetService userPetService;
 
    @ResponseBody
    @RequestMapping(value = "/pood/user/pet/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_USER_PET(@RequestParam("user_uuid") String user_uuid, HttpServletRequest request)
            throws Exception {
       
        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_PET_2){        
                Integer user_idx = userService.getUserIDX(user_uuid);
        
                // 회원 동물 조회
                List<resp_user_pet> result = userPetService.getUserPetList(user_idx, null);
                response.put("result", result);
                result = null;

            }else response = getResponse(219);

            procClose(request, null);

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        
        

        return response;
    }

}