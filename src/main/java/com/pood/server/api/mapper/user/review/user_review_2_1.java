package com.pood.server.api.mapper.user.review;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.object.resp.resp_ava_list;
import com.pood.server.api.service.meta.goods.*;
import com.pood.server.api.service.meta.order.*;
import com.pood.server.api.service.user.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_review_2_1 extends PROTOCOL {

    @Autowired
    @Qualifier("orderBasketService")
    public orderBasketService orderBasketService;

    @Autowired
    @Qualifier("userReviewService")
    public userReviewService userReviewService;

    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/review/2/1", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_USER_AVAILABLE_REVIEW(@RequestParam("user_uuid") String user_uuid,
            HttpServletRequest request) throws Exception {
         
        
        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_REVIEW_2_1){        

                Integer USER_IDX = 0;
            
            
                USER_IDX = userService.getUserIDX(user_uuid);

                //---- 작성가능한 회원 리뷰 조회
                List<resp_ava_list> review_available_info = orderService.getAVAReviewList(USER_IDX);
                response.put("review_available_info", review_available_info);
                review_available_info = null;

            }else response = getResponse(219);

            procClose(request, null);

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        

        return response;
    }
 
}