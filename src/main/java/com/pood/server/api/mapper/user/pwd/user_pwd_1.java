package com.pood.server.api.mapper.user.pwd;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.user.pwd.hUser_pwd_1;
import com.pood.server.api.service.user.userService;
import com.pood.server.config.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_pwd_1 extends PROTOCOL{
 
    @Autowired
    @Qualifier("userService")
    userService userService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/password/1", method = RequestMethod.POST)
    public HashMap<String, Object> USER_REPASSWORD(@RequestBody hUser_pwd_1 header, HttpServletRequest request)
            throws Exception {

        
        try{

            procInit(request);

            response = getResponse(200);
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_PASSWORD_1){

                String USER_UUID            = header.getUser_uuid();

                String NEW_PASSWORD         = header.getNew_passwd();

                String CONFIRM_PASSWORD     = header.getConfirm_passwd();
            
                // 새 비밀번호와 확인 비밀번호가 동일하지 않을 경우 에러 반환
                if(!CONFIRM_PASSWORD.equals(NEW_PASSWORD)){
                    response = getResponse(234);
                    procClose(request, response.toString());
                    return response;
                }
                
                // 패스워드 변경
                userService.passwordUpdate(NEW_PASSWORD, USER_UUID);


                NEW_PASSWORD        = null;
                
                CONFIRM_PASSWORD    = null;

                USER_UUID           = null;

                
            }else response = getResponse(219);
    

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        
        
        
        return response;
    }
}