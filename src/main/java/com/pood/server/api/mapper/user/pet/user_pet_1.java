package com.pood.server.api.mapper.user.pet;

import com.pood.server.api.req.header.user.pet.hUser_pet_1;
import com.pood.server.api.req.header.user.pet.hUser_pet_1_1;
import com.pood.server.api.req.header.user.pet.hUser_pet_image;
import com.pood.server.api.service.user.userPetAIService;
import com.pood.server.api.service.user.userPetAllergyService;
import com.pood.server.api.service.user.userPetFeedService;
import com.pood.server.api.service.user.userPetGrainSizeService;
import com.pood.server.api.service.user.userPetService;
import com.pood.server.api.service.user.userPetSickService;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_pet_1 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_pet_1.class);

    @Autowired
    @Qualifier("userPetService")
    userPetService userPetService;

    @Autowired
    @Qualifier("userPetAIService")
    userPetAIService userPetAIService;
    
    @Autowired
    @Qualifier("userPetAllergyService")
    userPetAllergyService userPetAllergyService;

    @Autowired
    @Qualifier("userPetFeedService")
    userPetFeedService userPetFeedService;

    @Autowired
    @Qualifier("userPetGrainSizeService")
    userPetGrainSizeService userPetGrainSizeService;

    @Autowired
    @Qualifier("userPetSickService")
    userPetSickService userPetSickService;
 
    @ResponseBody
    @RequestMapping(value = "/pood/user/pet/1", method = RequestMethod.POST)
    public HashMap<String, Object> ADD_USER_PET(@RequestBody hUser_pet_1 header, HttpServletRequest request)
            throws Exception {
 

        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_PET_1){        

                logger.info(header.toString());

                Integer user_idx = userService.getUserIDX(header.getUser_uuid());

                // 테이블 : USER_PET
                Integer user_pet_idx = 
                    userPetService.insertUserPet(user_idx, 
                        header.getPet_activity(), 
                        header.getPc_id(),
                        header.getPsc_id(),
                        header.getPet_name(), 
                        header.getPet_birth(), 
                        header.getPet_gender(), 
                        header.getPet_weight(), 
                        header.getPet_status());

                if(user_pet_idx == -1){
                    // 삽입시 NULL이 들어갈 경우 231 에러
                    response = getResponse(231);
                    procClose(request, (String)response.get("msg"));
                    return response;
                }
                

                // 테이블 : USER_PET_IMAGE
                for(hUser_pet_image e:header.getImage())
                    userPetService.updateUserPetImage(user_pet_idx, e.getImage_idx());

                // 테이블 : USER_PET_AI_DIAGNOSIS
                for(hUser_pet_1_1 e : header.getAi_diagnosis())
                    userPetAIService.insertRecord(user_pet_idx, e.getIdx());

                // 테이블 : USER_PET_ALLERGY
                for(hUser_pet_1_1 e : header.getAllergy()){
                    if(e.getIdx() == -1) continue;
                    userPetAllergyService.insertRecord(user_pet_idx, e.getIdx());
                }

                // 테이블 : USER_PET_GRAIN_SIZE
                for(hUser_pet_1_1 e : header.getGrain_size())
                    userPetGrainSizeService.insertRecord(user_pet_idx, e.getIdx());

                // 테이블 : USER_PET_SICK
                for(hUser_pet_1_1 e : header.getSick_idx())
                    userPetSickService.insertRecord(user_pet_idx, e.getIdx());

                    
                // 테이블 : USER_PET_FEED
                userPetFeedService.insertRecord(user_pet_idx, header.getOther_feed_idx());

                response.put("user_pet_idx", user_pet_idx);
                
            }else response = getResponse(219);
                
            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        

        
        return response;
    }

}