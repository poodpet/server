package com.pood.server.api.mapper.admin;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import com.pood.server.api.service.database.*;
import com.pood.server.config.PROTOCOL;
import com.pood.server.dto.dto_column_list;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class column extends PROTOCOL {

    @Autowired
    @Qualifier("DBService")
    DBService DBService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/admin/column-list/", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_AAFCO_NRC(HttpServletRequest request,
        @RequestParam(value="db", required=false) String db_name,    
        @RequestParam(value="table", required=false) String table_name) throws Exception {

        Integer resp_status = procInit(request);
        
        // 인증토큰에 대해서 유효성 검증에 실패한 경우
        if(resp_status != 200){
            procClose(request, response.toString());
            return response;
        }
        
        List<dto_column_list> result = DBService.getColumnList(db_name, table_name);

        // 특정 데이터베이스 테이블 칼럼 목록 조회 
        response.put("result", result);
            
        procClose(request, (String)response.get("msg"));

        
        return response;
    }
 
}  