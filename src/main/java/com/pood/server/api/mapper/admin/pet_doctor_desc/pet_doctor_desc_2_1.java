package com.pood.server.api.mapper.admin.pet_doctor_desc;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.meta.pet_doctor_desc.PETDoctorDescService;
import com.pood.server.config.*;
import com.pood.server.dto.meta.goods.dto_goods_5;
import com.pood.server.object.pagingSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class pet_doctor_desc_2_1 extends PROTOCOL {
    
    
    @Autowired
    @Qualifier("PETDoctorDescService")
    PETDoctorDescService PETDoctorDescService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/admin/pet-doctor-desc/2/1", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_PET_DOCTOR_DESCRIPTION(
        @RequestParam(value="page_size", required=false) Integer page_size, 
        @RequestParam(value="page_number", required=false) Integer page_number,
        @RequestParam(value="recordbirth", required=false) String recordbirth,
        @RequestParam(value="pc_idx", required=false) Integer pc_idx,
        @RequestParam(value="ct_idx", required=false) Integer ct_idx,
        @RequestParam(value="ct_sub_idx", required=false) Integer ct_sub_idx,
        @RequestParam(value="position") String position,
        HttpServletRequest request) throws Exception {
        
        
        try{
                
            procInit(request);
            
            response = getResponse(200); 

            Integer headerCheck = headerService.checkPagingSet(page_size, page_number, recordbirth);

            if(headerCheck != 200){
                response = getResponse(headerCheck);
                procClose(request, response.toString());
                return response;
            }
    
            if(PROTOCOL_IDENTIFIER.PROTOCOL_ADMIN_PET_DOCTOR_DESC_2_1){

                String query = PETDoctorDescService.getListQuery(position, ct_idx, ct_sub_idx);

                Integer total_cnt = PETDoctorDescService.getTotalRecordNumber(query);

                // 상품 번호 추출
                pagingSet PAGING_SET = new pagingSet(
                        page_size,  
                        page_number, 
                        recordbirth,
                        total_cnt);

                List<dto_goods_5> result = PETDoctorDescService.getList(PAGING_SET, query, pc_idx);
                response.put("result", result);
                pagingResult(PAGING_SET); 

                result = null;

            }else response = getResponse(219);
        
            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        
        
        return response;
    }
 
} 