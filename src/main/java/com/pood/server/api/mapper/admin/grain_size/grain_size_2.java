package com.pood.server.api.mapper.admin.grain_size;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.meta.grain_size.*;
import com.pood.server.config.*;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_grain_size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class grain_size_2 extends PROTOCOL {

    
    @Autowired
    @Qualifier("grainSizeService")
    grainSizeService grainSizeService;

    @ResponseBody
    @RequestMapping(value = "/pood/admin/grain-size/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_GRAIN_SIZE(
        @RequestParam(value="page_size", required=false) Integer page_size, 
        @RequestParam(value="page_number", required=false) Integer page_number,
        @RequestParam(value="recordbirth", required=false) String recordbirth,
        HttpServletRequest request) throws Exception {
    

        try{
                
            Integer resp_status = procInit(request);

            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            Integer headerCheck = headerService.checkPagingSet(page_size, page_number, recordbirth);

            if(headerCheck != 200){
                response = getResponse(headerCheck);
                procClose(request, response.toString());
                return response;
            }
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_ADMIN_GRAIN_SIZE_2){
                
                pagingSet PAGING_SET = new pagingSet(
                        page_size, 
                        page_number, 
                        recordbirth,
                        grainSizeService.getTotalRecordNumber());

                List<dto_grain_size> result = grainSizeService.getList(PAGING_SET);            
                response.put("result", result);    
                pagingResult(PAGING_SET); 

                result = null;
                
            }else response = getResponse(219);
            
            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        
        return response;
    }
 
}  