package com.pood.server.api.mapper.user.wish;

import com.pood.server.api.req.header.user.wish.hUser_wish_3;
import com.pood.server.api.service.user.userWishService;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_wish_3 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_wish_3.class);

    @Autowired
    @Qualifier("userWishService")
    userWishService userWishService;
      
    @ResponseBody
    @RequestMapping(value = "/pood/user/wish/3", method = RequestMethod.POST)
    public HashMap<String, Object> DELETE_WISH(@RequestBody hUser_wish_3 header, HttpServletRequest request)
            throws Exception {
        
        
        try{

            Integer resp_status = procInit(request);

            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_WISH_3){        

                logger.info(header.toString());
                if(header.getUser_uuid() == null){
                    response = getResponse(300);
                    return response;
                }

                if (header.getGoods_idx() == null) {
                    response = getResponse(300);
                    return response;
                }

                Integer user_idx = userService.getUserIDX(header.getUser_uuid());
                Integer goods_idx = header.getGoods_idx();
                
                userWishService.deleteRecord(goods_idx, user_idx);
                
            }else response = getResponse(219);
            
            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        

        
        return response;
    }
}