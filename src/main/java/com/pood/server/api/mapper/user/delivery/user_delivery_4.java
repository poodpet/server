package com.pood.server.api.mapper.user.delivery;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.user.delivery.hUser_delivery_4;
import com.pood.server.config.*;
import com.pood.server.dto.user.dto_user_delivery_address;
import com.pood.server.api.service.user.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_delivery_4 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_delivery_4.class);
 
    @Autowired
    @Qualifier("userDeliveryService")
    userDeliveryService userDeliveryService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/delivery/4", method = RequestMethod.POST)
    public HashMap<String, Object> UPDATE_USER_DELIVERY(
        @RequestBody hUser_delivery_4 header,
         HttpServletRequest request)
            throws Exception {
         
        try{

            logger.info(header.toString());

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_DELIVERY_4){
                // 회원 배송지 정보 업데이트 
                Integer idx = header.getIdx();
                Boolean default_type = header.getDefault_type();


                // 만약 특정 배송지를 기본 배송지로 지정할 경우 다른 모든 배송지는 기본 배송지가 아닌 걸로 자동 지정
                if(default_type == true){
                    Integer user_idx = userDeliveryService.getUserIndex(idx);
                    List<dto_user_delivery_address> list = userDeliveryService.getUserDeliveryList(user_idx);
                    for(dto_user_delivery_address e : list)
                        userDeliveryService.updateAddressToDefault(e.getIdx()); 
                    list = null;
                }

                userDeliveryService.updateDeliveryAddress(header.getAddress(), 
                    default_type, 
                    header.getDetail_address(), 
                    header.getName(), 
                    header.getNickname(), 
                    header.getZipcode(), 
                    header.getInput_type(), 
                    header.getRemote_type(), 
                    header.getPhone_number(), 
                    idx);
            }else response = getResponse(219);
            
            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

            
        return response;
    }
} 