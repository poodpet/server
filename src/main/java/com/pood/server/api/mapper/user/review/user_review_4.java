package com.pood.server.api.mapper.user.review;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.user.review.hUser_review_4;
import com.pood.server.api.req.header.user.review.hUser_review_image;
import com.pood.server.config.*;
import com.pood.server.api.service.meta.order.OrderService;
import com.pood.server.api.service.user.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_review_4 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_review_4.class);
    
    @Autowired
    @Qualifier("userReviewService")
    userReviewService userReviewService;

    @Autowired
    @Qualifier("orderService")
    OrderService orderService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/review/4", method = RequestMethod.POST)
    public HashMap<String, Object> UPDATE_USER_REVIEW(
        @RequestBody hUser_review_4 header,
         HttpServletRequest request)
            throws Exception {
    
        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_REVIEW_4){        
                
                logger.info(header.toString());
                
                // 회원 리뷰 업데이트
                userReviewService.updateUserReview(
                    header.getReview_idx(), 
                    header.getRating(), 
                    header.getReview_text(), 
                    header.getUpdate_count(), 
                    header.getIsDelete(), 
                    header.getIsVisible());


                if(header.getImage() != null){

                    Integer image_cnt = userReviewService.getUserReviewImageCount(header.getReview_idx());
                    if(image_cnt >= 6){

                        // 이미지 6장 업로드했을 때는 더 업로드되면 안 됨. 
                        response = getResponse(211);

                    }else{
                        for(hUser_review_image e: header.getImage()){
                            
                            Integer IMAGE_IDX       = e.getImage_idx();
                            Integer IMAGE_SEQUENCE  = e.getSeq();
                            Integer ORDER_IDX       = e.getOrder_idx();
                            String  ORDER_NUMBER    = orderService.getOrderNumber(ORDER_IDX);

                            userReviewService.updateUserReviewImage(header.getReview_idx(), ORDER_NUMBER, IMAGE_SEQUENCE, IMAGE_IDX);

                            ORDER_NUMBER = null;
                        }   
                    }
                }
            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        
        
        return response;
    }
} 