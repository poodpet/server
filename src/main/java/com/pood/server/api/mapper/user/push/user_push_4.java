package com.pood.server.api.mapper.user.push;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.user.push.hUser_push_4;
import com.pood.server.config.*;
import com.pood.server.api.service.user.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_push_4 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_push_4.class);
 
    @Autowired
    @Qualifier("userService")
    userService userService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/push/4", method = RequestMethod.POST)
    public HashMap<String, Object> UPDATE_USER_PUSH(@RequestBody hUser_push_4 header,
            HttpServletRequest request) throws Exception {
         

        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_PUSH_4){     

                logger.info(header.toString());
                
                String  USER_UUID       =   header.getUser_uuid();

                Boolean POOD_PUSH       =   header.getPood_push();

                Boolean ORDER_PUSH      =   header.getOrder_push();

                Boolean SERVICE_PUSH    =   header.getService_push();

                Boolean dogPoodPush    =   header.getDog_pood_push();

                Boolean catPoodPush    =   header.getCat_pood_push();

                userService.updatePushInfo(POOD_PUSH, ORDER_PUSH, SERVICE_PUSH, dogPoodPush, catPoodPush, USER_UUID);

                POOD_PUSH       =   null;

                ORDER_PUSH      =   null;
                
                SERVICE_PUSH    =   null;

            }else response = getResponse(219);
            
            
            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        

        return response;
    }
} 