package com.pood.server.api.mapper.admin.event_info;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.meta.event_info.*;
import com.pood.server.config.*;
import com.pood.server.dto.meta.event.dto_event_info;
import com.pood.server.object.pagingSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class event_info_2 extends PROTOCOL {

    
    @Autowired
    @Qualifier("eventInfoService")
    eventInfoService eventInfoService;

    @ResponseBody
    @RequestMapping(value = "/pood/admin/event-info/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_EVENT_INFO(
        @RequestParam(value="page_size", required=false) Integer page_size, 
        @RequestParam(value="page_number", required=false) Integer page_number,
        @RequestParam(value="recordbirth", required=false) String recordbirth,
        HttpServletRequest request) throws Exception {
    

        try{
                
            Integer resp_status = procInit(request);

            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            Integer headerCheck = headerService.checkPagingSet(page_size, page_number, recordbirth);

            if(headerCheck != 200){
                response = getResponse(headerCheck);
                procClose(request, response.toString());
                return response;
            }
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_ADMIN_EVENT_TYPE_2){
                pagingSet PAGING_SET = new pagingSet(
                        page_size, 
                        page_number, 
                        recordbirth,
                        eventInfoService.getTotalRecordNumber(null, null, null));

                List<dto_event_info> result = eventInfoService.getEventInfoList(PAGING_SET);            
                response.put("result", result);    
                pagingResult(PAGING_SET); 
                result = null;

            }else response = getResponse(219);
            
            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        
        return response;
    }
 
}  