package com.pood.server.api.mapper.order.goods;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.queryBuilder.CNT_QUERY;
import com.pood.server.api.queryBuilder.SELECT_QUERY;
import com.pood.server.api.service.view.view_2.*;
import com.pood.server.config.*;
import com.pood.server.object.pagingSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController; 
import com.pood.server.dto.meta.goods.dto_goods_7;

@RestController
public class goods_2_1 extends PROTOCOL {

    @Autowired
    @Qualifier("view2Service")
    view2Service view2Service;
  
    @ResponseBody
    @RequestMapping(value = "/pood/order/goods/2/1", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_goods(
        @RequestParam(value="goods_idx", required=false) Integer goods_idx,
        @RequestParam(value="goods_type_idx", required=false) Integer goods_type_idx,
        @RequestParam(value="pc_idx", required=false) Integer pc_idx,
        @RequestParam(value="ct_idx", required=false) Integer ct_idx,
        @RequestParam(value="ct_sub_idx", required=false) String ct_sub_idx,
        @RequestParam(value="brand_idx", required=false) Integer brand_idx,
        @RequestParam(value="page_size", required=false) Integer page_size, 
        @RequestParam(value="page_number", required=false) Integer page_number,
        @RequestParam(value="recordbirth", required=false) String recordbirth,
        @RequestParam(value="keyword", required=false) String keyword,
        @RequestParam(value="main_property", required=false) String main_property,
        @RequestParam(value="unit_size", required=false)String unit_size,
        @RequestParam(value="life_stage", required=false)Integer life_stage,
        @RequestParam(value="feed_target", required=false)Integer feed_target,
        @RequestParam(value="position", required=false)String position,
        @RequestParam(value="display_type", required=false)String display_type,
        HttpServletRequest request) throws Exception {


        try{

            procInit(request);
                
            response = getResponse(200);

            Integer headerCheck = headerService.checkPagingSet(page_size, page_number, recordbirth);

            if(headerCheck != 200){
                response = getResponse(headerCheck);
                procClose(request, response.toString());
                return response;
            } 
                    
            if(PROTOCOL_IDENTIFIER.PROTOCOL_ORDER_GOODS_2_1){

                SELECT_QUERY query = view2Service.getQuery(
                    goods_idx,              // 굿즈 항목 번호
                    goods_type_idx,         // 굿즈 타입 항목 번호
                    pc_idx,                 // 반려동물 타입     
                    ct_idx,                 // 카테고리 항목 번호
                    ct_sub_idx,             // 하위 카테고리 항목 번호 
                    brand_idx,              // 브랜드 항목 번호
                    keyword,                // 검색어 
                    main_property,          // 주요 성분 
                    recordbirth,            // 알갱이 크기
                    unit_size,              // 페이지 크기 
                    life_stage,             // 라이프 스테이지
                    feed_target,            // 1: 소형, 2:중형, 3:대혀
                    position,               // 적합 포지션
                    display_type);              

                Integer total_cnt = getListTotalCount(query.toString());
                
                pagingSet PAGING_SET = new pagingSet(page_size, page_number, recordbirth, total_cnt);  
                List<dto_goods_7> result =  view2Service.getGoodsList(PAGING_SET, query);
                response.put("result", result);
                pagingResult(PAGING_SET);
                result = null;
                

            }else response = getResponse(219);
            
            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        
            

        return response;
    }


    // 리스트 총 개수 반환 
    public Integer getListTotalCount(String query) throws SQLException{
        CNT_QUERY CNT_QUERY = new CNT_QUERY();
        CNT_QUERY.getQueryCount(query);
        Integer total_cnt = listService.getRecordCount(CNT_QUERY.toString());
        if(total_cnt == null)total_cnt = 0; 
        return total_cnt;
    }
}  