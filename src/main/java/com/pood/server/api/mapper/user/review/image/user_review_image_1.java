package com.pood.server.api.mapper.user.review.image;

import com.pood.server.api.file.uploader.StorageService;
import com.pood.server.api.service.user.userReviewService;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class user_review_image_1 extends PROTOCOL{
     

    @Autowired
    @Qualifier("storageService")
    StorageService storageService;

    @Autowired
    @Qualifier("userReviewService")
    userReviewService userReviewService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/review/image/1", method = RequestMethod.POST)
    public HashMap<String, Object> ADD_USER_REVIEW_IMAGE(
        @RequestParam("file") MultipartFile[] file, HttpServletRequest request) throws Exception {
        

        try{
        
            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_REVIEW_IMAGE_1){        
                    
                List<HashMap<String, Object>> result = new ArrayList<HashMap<String, Object>>();

                for(MultipartFile f:file){

                    HashMap<String, Object> record = new HashMap<>();
                    String img_url = storageService.multiPartFileStore(f, "user_review");
                    record.put("url", img_url);

                    Integer image_idx = userReviewService.insertImageRecord(img_url);
                    
                    record.put("image_idx", image_idx);
                    result.add(record);

                    
                }

            
                response.put("result", result);
            }else response = getResponse(219);
            
            
            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

            
        return response;
    }
 
}
