package com.pood.server.api.mapper.order.refund;

import com.pood.server.api.req.header.order.refund.hOrder_refund_1;
import com.pood.server.api.req.header.order.refund.hOrder_refund_delivery;
import com.pood.server.api.req.header.order.refund.hOrder_refund_goods;
import com.pood.server.api.service.log.logUserOrderService;
import com.pood.server.api.service.meta.order.OrderService;
import com.pood.server.api.service.meta.order.orderDeliveryService;
import com.pood.server.api.service.meta.order.orderDeliveryTrackService;
import com.pood.server.api.service.meta.order.orderRefundService;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import com.pood.server.config.meta.order.ORDER_DELIVERY_ADDRESS_TYPE;
import com.pood.server.config.meta.order.ORDER_STATUS;
import com.pood.server.config.meta.order.ORDER_STATUS_MESSAGE;
import com.pood.server.dto.meta.order.dto_order;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class order_refund_1 extends PROTOCOL {

    private static final Logger LOGGER = LoggerFactory.getLogger(order_refund_1.class);

    @Autowired
    @Qualifier("orderRefundService")
    orderRefundService orderRefundService;


    @Autowired
    @Qualifier("orderDeliveryService")
    orderDeliveryService orderDeliveryService;


    @Autowired
    @Qualifier("logUserOrderService")
    logUserOrderService logUserOrderService;


    @Autowired
    @Qualifier("orderService")
    OrderService orderService;

    @Autowired
    @Qualifier("orderDeliveryTrackService")
    orderDeliveryTrackService orderDeliveryTrackService;

    @ResponseBody
    @RequestMapping(value = "/pood/order/refund/1", method = RequestMethod.POST)
    public HashMap<String, Object> ADD_ORDER_REFUND_1(@Valid @RequestBody hOrder_refund_1 header,
        HttpServletRequest request)
        throws Exception {

        try {

            Integer resp_status = procInit(request);

            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if (resp_status != 200) {
                procClose(request, response.toString());
                return response;
            }

            LOGGER.info("*********** 반품 신청 로그 *************");
            if (PROTOCOL_IDENTIFIER.PROTOCOL_ORDER_REFUND_1) {

                Integer sendType = header.getSend_type();
                String orderNumber = header.getOrder_number();
                dto_order orderDto = orderService.getOrder(orderNumber);
                Integer userIdx = orderDto.getUser_idx();
                String userUuid = userService.getUserUUID(userIdx);

                String sendTypeText = "";

                if (sendType == 0) {
                    sendTypeText = "고객이 보낸 경우";
                }

                if (sendType == 1) {
                    sendTypeText = "상품 회수가 필요한 경우";
                }

                LOGGER.info(
                    "반품 신청 택배 타입 : " + sendTypeText + ", 주문 번호 : " + orderNumber + ", 회원 UUID : "
                        + userUuid);

                Integer orderStatus = orderDto.getOrder_status();

                // 배송 완료가 되지 않았는데 반품 신청이 이루어질 경우 
                if (!orderStatus.equals(ORDER_STATUS.DELIVERY_SUCCESS)) {
                    response = getResponse(249);
                    procClose(request, response.toString());
                    return response;
                }

                List<hOrder_refund_goods> orderRefundGoods = header.getGoods();

                // 굿즈 리스트
                if (orderRefundGoods != null) {

                    for (hOrder_refund_goods e : orderRefundGoods) {

                        Integer goodsIdx = e.getGoods_idx();

                        Integer quantity = e.getQty();

                        String text = e.getText();

                        LOGGER.info(
                            "굿즈 항목번호[" + goodsIdx + "], 굿즈 수량 : " + quantity + ", 신청 텍스트 : "
                                + text);

                        // 반품 요청시 상품별로 반품 레코드 추가
                        String refundNumber = orderRefundService.insertRecord(text, orderNumber,
                            goodsIdx, quantity, sendType, e.getAttr());

                        logUserOrderService.insertOrderHistory(
                            orderDto.getIdx(),
                            ORDER_STATUS.REFUND_REQUEST,
                            ORDER_STATUS_MESSAGE.REFUND_REQUEST_MESSAGE(refundNumber),
                            orderNumber,
                            userUuid,
                            goodsIdx,
                            quantity);

                        // SEND TYPE이 '상품 회수해주세요'인 경우에만 추적 정보 입력받도록
                        if (sendType == 1) {
                            hOrder_refund_delivery DELIVERY = header.getDelivery_track();
                            LOGGER.info("회수 시 배송 정보 : " + DELIVERY.toString());
                            LOGGER.info(DELIVERY.toString());
                            orderDeliveryService.insertRecord("", refundNumber, orderNumber,
                                orderDto.getIdx(), DELIVERY,
                                ORDER_DELIVERY_ADDRESS_TYPE.REFUND_PICK_UP);
                        }

                    }
                }

            } else {
                response = getResponse(219);
            }

            procClose(request, response.toString());

        } catch (Exception e) {
            thExecption(request.getServletPath(), e);
        }

        return response;
    }

}