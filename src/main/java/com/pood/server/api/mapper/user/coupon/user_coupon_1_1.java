package com.pood.server.api.mapper.user.coupon;

import com.pood.server.api.req.header.user.coupon.hUser_coupon_1_1;
import com.pood.server.api.service.APICall.APICallService;
import com.pood.server.api.service.log.logUserCouponCodeService;
import com.pood.server.api.service.meta.coupon.couponCodeService;
import com.pood.server.api.service.meta.coupon.couponService;
import com.pood.server.api.service.time.timeService;
import com.pood.server.api.service.user.userCouponService;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import com.pood.server.dto.meta.dto_coupon;
import com.pood.server.dto.meta.dto_coupon_code;
import com.pood.server.object.log.vo_log_user_coupon_code;
import com.pood.server.object.user.vo_user_coupon;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_coupon_1_1 extends PROTOCOL {

    @Value("${legacy.server-ip}")
    private String serverIp;

    Logger logger = LoggerFactory.getLogger(user_coupon_1_1.class);

    @Autowired
    @Qualifier("userCouponService")
    userCouponService userCouponService;

    @Autowired
    @Qualifier("couponCodeService")
    couponCodeService couponCodeService;

    @Autowired
    @Qualifier("couponService")
    couponService couponService;

    @Autowired
    @Qualifier("APICallService")
    APICallService APICallService;

    @Autowired
    @Qualifier("timeService")
    timeService timeService;


    @Autowired
    @Qualifier("logUserCouponCodeService")
    logUserCouponCodeService logUserCouponCodeService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/coupon/1/1", method = RequestMethod.POST)
    public HashMap<String, Object> ADD_USER_COUPON(@RequestBody hUser_coupon_1_1 header,
        HttpServletRequest request)
        throws Exception {

        try {

            Integer resp_status = procInit(request);

            logger.info(header.toString());

            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if (resp_status != 200) {
                procClose(request, response.toString());
                return response;
            }

            if (PROTOCOL_IDENTIFIER.PROTOCOL_USER_COUPON_1_1) {

                String code = header.getCode();
                dto_coupon_code record = couponCodeService.getRecord(code);

                if (record == null) {
                    // 쿠폰 없을 경우 253 에러
                    response = getResponse(253);
                    procClose(request, response.toString());
                    return response;
                }

                dto_coupon coupon_info = couponService.getCoupon(record.getCoupon_idx());

                Integer SELETED_COUPON_IDX = record.getCoupon_idx();

                logger.info(" coupon_idx : " + SELETED_COUPON_IDX);
                if (coupon_info != null) {
                    String CURRENT_TIME = timeService.getCurrentTime();
                    Boolean IS_EXPITED = timeService.isExpired(coupon_info.getAvailable_time());

                    if (IS_EXPITED) {
                        response = getResponse(256);
                        procClose(request, response.toString());
                        return response;
                    }
                }

                String user_uuid = header.getUser_uuid();
                vo_log_user_coupon_code tmp = logUserCouponCodeService.getRecord(user_uuid, code);

                List<vo_user_coupon> result = new ArrayList<vo_user_coupon>();
                result = userCouponService.getUserCouponINDEXByUuid(user_uuid);

                if (result != null) {
                    for (vo_user_coupon e : result) {
                        // 특정 쿠폰 발급 되어 있을 시에 더 이상 발급 받지 못하도록 막음 error 255
                        logger.info("result : " + e.getCoupon_idx());
                        if (e.getCoupon_idx() == 19 || e.getCoupon_idx() == 20
                            || e.getCoupon_idx() == 21) {
                            logger.info("result 2 : " + e.getCoupon_idx());
                            response = getResponse(255);
                            procClose(request, response.toString());
                            return response;
                        }
                    }
                }

                if (tmp != null) {
                    // 이미 등록된 쿠폰 코드인 경우 254 에러
                    response = getResponse(254);
                    procClose(request, response.toString());
                    return response;
                }

                // 관리자 프록시를 통해서 쿠폰 발급
                HashMap<String, Object> body = new HashMap<String, Object>();
                body.put("code", code);
                body.put("user_uuid", user_uuid);

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);

                String url = serverIp + "/pood/user/coupon/1/1/1";
                logger.info("호출 주소 : " + url);
                ResponseEntity<String> http_response = APICallService.post2(
                    MediaType.APPLICATION_JSON, headers, body, url);
                logger.info(http_response.toString());

                record = null;

                code = null;

            } else {
                response = getResponse(219);
            }

            procClose(request, response.toString());

        } catch (Exception e) {
            thExecption(request.getServletPath(), e);
        }

        logger.info(response.toString());
        return response;
    }
}