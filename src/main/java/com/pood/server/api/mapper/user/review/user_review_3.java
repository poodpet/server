package com.pood.server.api.mapper.user.review;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.user.review.hUser_review_3;
import com.pood.server.config.*;
import com.pood.server.api.service.user.userReviewService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController 
public class user_review_3 extends PROTOCOL{
  
    @Autowired
    @Qualifier("userReviewService")
    userReviewService userReviewService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/review/3", method = RequestMethod.POST)
    public HashMap<String, Object> DELETE_REVIEW(@RequestBody hUser_review_3 header, HttpServletRequest request)
            throws Exception {
       
        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_REVIEW_3){        
                for(Integer e:header.getIdx())
                    userReviewService.updateReviewVisibleStatus(e, 
                        com.pood.server.config.meta.user.USER_REVIEW.NOT_VISIBLE);
            }else response = getResponse(219);
    
            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        

        return response;
    }
}