package com.pood.server.api.mapper.user;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.api.service.view.view_8.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_2_6 extends PROTOCOL{
 
    @Autowired
    @Qualifier("view8Service")
    view8Service view8Service;
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/2/6", method = RequestMethod.GET)
    public HashMap<String, Object> GET_PAY_SUCCESS(
        @RequestParam(value="user_uuid", required=false) String user_uuid,
        @RequestParam(value="goods_idx", required=false) Integer goods_idx,
        HttpServletRequest request) throws Exception {
    
        try{

            procInit(request);

            response = getResponse(200);
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_2_6){        
                Boolean isPurchase = view8Service.isPurchase(user_uuid, goods_idx);
                response.put("isPurchase", isPurchase);
                isPurchase = null;
            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        
        return response;
    }
}  