package com.pood.server.api.mapper.user.sms;

import com.pood.server.api.service.json.jsonService;
import com.pood.server.api.service.time.timeService;
import com.pood.server.api.service.user.userSMSAuthService;
import com.pood.server.config.AES256;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import com.pood.server.object.api.API_RESPONSE;
import com.pood.server.object.user.vo_user;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class user_sms_1_1 extends PROTOCOL {

    @Value("${legacy.server-ip}")
    private String serverIp;

    Logger logger = LoggerFactory.getLogger(user_sms_1_1.class);

    @Autowired
    @Qualifier("userSMSAuthService")
    userSMSAuthService userSMSAuthService;

    @Autowired
    @Qualifier("timeService")
    timeService timeService;

    @Autowired
    @Qualifier("jsonService")
    jsonService jsonService;


    @ResponseBody
    @RequestMapping(value = "/pood/user/sms/1/1", method = RequestMethod.GET)
    public HashMap<String, Object> SEND_MESSAGE(HttpServletRequest request,
        @RequestParam(value = "user_email", required = false) String USER_EMAIL,
        @RequestParam(value = "phone_number", required = false) String PHONE_NUMBER,
        @RequestParam(value = "user_name", required = false) String encoded_name) throws Exception {

        try {

            procInit(request);

            response = getResponse(200);

            String USER_NAME = AES256.aesDecode(encoded_name);

            logger.info("DECODED : " + USER_NAME);

            Boolean chkUser = userService.checkUser(USER_NAME, USER_EMAIL, PHONE_NUMBER);

            if (!chkUser) {
                response = getResponse(242);
                procClose(request, response.toString());
                return response;
            }

            chkUser = false;

            vo_user USER = userService.getUser("user_email", USER_EMAIL);

            // 없는 회원인 경우 241 에러
            if (USER == null) {
                response = getResponse(203);
                procClose(request, response.toString());
                return response;
            }

            if (PROTOCOL_IDENTIFIER.PROTOCOL_USER_SMS_1_1) {

                Boolean chk = userSMSAuthService.isRecordbirthValid(PHONE_NUMBER);

                if (chk) {

                    String url = serverIp + "/pood/user/sms/1?phone_number=" + PHONE_NUMBER;
                    logger.info("CALL ADMIN SERVER API : " + url);
                    String result = new RestTemplate().getForObject(url, String.class);
                    API_RESPONSE resp = (API_RESPONSE) jsonService.getDecodedObject(result,
                        API_RESPONSE.class);
                    if (!(resp.getStatus() == 200)) {
                        response = getResponse(resp.getStatus());
                    }

                    response.put("user_uuid", USER.getUser_uuid());

                } else {
                    response = getResponse(235);
                }

            } else {
                response = getResponse(219);
            }

            procClose(request, response.toString());

            USER = null;

            USER_NAME = null;

        } catch (Exception e) {
            response = getResponse(242);
            thExecption(request.getServletPath(), e);
        }

        return response;
    }


} 
