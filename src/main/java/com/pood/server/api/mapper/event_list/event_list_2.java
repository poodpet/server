package com.pood.server.api.mapper.event_list;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.meta.event_info.*;
import com.pood.server.config.*;
import com.pood.server.dto.meta.event.dto_event_info_2;
import com.pood.server.object.pagingSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class event_list_2 extends PROTOCOL {
   
    @Autowired
    @Qualifier("eventInfoService")
    eventInfoService eventInfoService;
     
    @ResponseBody
    @RequestMapping(value = "/pood/event-list/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_MAIN_LIST(
        @RequestParam(value="type", required=false) Integer type_idx, 
        @RequestParam(value="pc_id", required=false) Integer pc_id,
        @RequestParam(value="brand_idx", required=false) Integer brand_idx, 
        @RequestParam(value="page_size", required=false) Integer page_size, 
        @RequestParam(value="page_number", required=false) Integer page_number, 
        @RequestParam(value="recordbirth", required=false) String recordbirth,
        HttpServletRequest request) throws Exception {
    
                
        try{

            procInit(request);
            
            response = getResponse(200);
            
            Integer headerCheck = headerService.checkPagingSet(page_size, page_number, recordbirth);

            if(headerCheck != 200){
                response = getResponse(headerCheck);
                procClose(request, response.toString());
                return response;
            }
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_EVENT_LIST_2){

                pagingSet PAGING_SET = new pagingSet(
                        page_size, 
                        page_number, 
                        recordbirth,
                        eventInfoService.getTotalRecordNumber(type_idx, pc_id, brand_idx));
                
                List<dto_event_info_2> result = eventInfoService.getEventList(PAGING_SET, type_idx, pc_id, brand_idx);
                response.put("result", result);
                pagingResult(PAGING_SET); 
                result = null;
            }else response = getResponse(219);

            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        
        
        return response;
    }
}  