package com.pood.server.api.mapper.user.card;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.user.card.hUser_card_4;
import com.pood.server.api.service.user.userCardService;
import com.pood.server.config.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_card_4 extends PROTOCOL{
 
    @Autowired
    @Qualifier("userCardService")
    userCardService userCardService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/simple-card/4", method = RequestMethod.POST)
    public HashMap<String, Object> UPDATE_USER_REVIEW(
        @RequestBody hUser_card_4 header,
         HttpServletRequest request)
            throws Exception {
        

        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_CARD_4){
                String CARD_NAME        =   header.getCard_name();
                String CARD_NUMBER      =   header.getCard_number();
                String CARD_USER        =   header.getCard_user();
                Boolean MAIN_CARD       =   header.getMain_card();
                String  CUSTOMER_UID    =   header.getCustomer_uid();
                userCardService.updateRecord(CARD_NAME, CARD_NUMBER, CARD_USER, MAIN_CARD, CUSTOMER_UID);
            }else response = getResponse(219);
                    
            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

            
        return response;
    }
} 