package com.pood.server.api.mapper.user.noti;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.dto.log.dto_log_user_notice_alaram;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_noti_2 extends PROTOCOL {
 
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/noti/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_USER_COUPON(@RequestParam("user_uuid") String user_uuid,
            HttpServletRequest request) throws Exception {


        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                    
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_NOTI_2){
                Integer user_idx = userService.getUserIDX(user_uuid);
                if(user_idx != null){
                    
                    List<dto_log_user_notice_alaram> result = pushService.getList(user_idx);
                    response.put("result", result);
                    result = null;

                }else response = getResponse(203);
                
            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        
        return response;
    }
}