package com.pood.server.api.mapper.user;

import com.pood.server.api.req.header.user.user_info.hUser_info_1;
import com.pood.server.api.service.log.logUserSavePointService;
import com.pood.server.api.service.meta.coupon.couponService;
import com.pood.server.api.service.time.timeService;
import com.pood.server.api.service.user.userCouponService;
import com.pood.server.api.service.user.userJoinService;
import com.pood.server.api.service.user.userLogPolicyService;
import com.pood.server.api.service.user.userReferralCodeService;
import com.pood.server.api.service.user.userService;
import com.pood.server.config.AES256;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_1 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_1.class);

    @Autowired
    @Qualifier("couponService")
    couponService couponService;

    @Autowired
    @Qualifier("userReferralCodeService")
    userReferralCodeService userReferralCodeService;

    @Autowired
    @Qualifier("userLogPolicyService")
    userLogPolicyService userLogPolicyService;

    @Autowired
    @Qualifier("userCouponService")
    userCouponService userCouponService;
     
    @Autowired
    @Qualifier("userJoinService")
    userJoinService userJoinService;

    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("timeService")
    timeService timeService;

    @Autowired
    @Qualifier("logUserSavePointService")
    logUserSavePointService logUserSavePointService;


    @ResponseBody
    @RequestMapping(value = "/pood/user/1", method = RequestMethod.POST)
    public HashMap<String, Object> ADD_USER(@RequestBody hUser_info_1 header, HttpServletRequest request)
            throws Exception {
        
        try{

            procInit(request);

            response = getResponse(200);

            logger.info(header.toString());
            

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_1){        
                    



                // 이메일이 유효하지 않으면 에러 반환
                if(!regexService.validate(header.getEmail())) {
                    response = getResponse(201);
                    return response;
                }



    
                // 닉네임 정책에 따라 닉네임 발급
                String USER_EMAIL           = header.getEmail();
                String USER_PASSWORD        = header.getPassword();
                Boolean USER_SERVICE_AGREE  = header.getService_agree();
                




                // 회원 식별자 UUID 발급 
                String USER_UUID            = UUID.randomUUID().toString();
                // String USER_NICKNAME        = userService.getUserNickname(USER_EMAIL);
                String REFERRAL_CODE        = header.getReferral_code();
                String USER_PHONE_NUMBER    = header.getPhone_number();
                String USER_NAME            = header.getUser_name();


                if(USER_PHONE_NUMBER == null){
                    response = getResponse(223);          
                    procClose(request, (String)response.get("msg"));
                    return response;        

                }else{

                    // 이미 가입되어 있는 핸드폰 번호가 있으면 227 에러 
                    Boolean isExist = userService.isExist("user_phone", USER_PHONE_NUMBER);

                    if(isExist){
                        response = getResponse(227);       
                        procClose(request, (String)response.get("msg"));   
                        return response;        
                    }

                }

                Integer LOGIN_TYPE = 0;



                // 파라미터에 패스워드가 삽입이 되어 있을 경우 이메일을 통한 가입
                if(header.getPassword() != null){
                    
                    if(header.getLogin_type() != 5){
                    
                    
                        // 이메일 회원 가입인 경우 로그인 타입은 무조건 5로 오게
                        response = getResponse(202);          
                        procClose(request, (String)response.get("msg"));
                        return response;        



                    }
                    
                    LOGIN_TYPE = com.pood.server.config.meta.user.USER_LOGIN_TYPE.EMAIL;
                }else  {
                    LOGIN_TYPE = com.pood.server.config.meta.user.USER_LOGIN_TYPE.SNS;
                    USER_PASSWORD = AES256.aesEncode(USER_PHONE_NUMBER);
                }




                // 이메일이 혹시 가입되어 있는지 확인
                if(USER_EMAIL != null){
                    Boolean isExist = userService.isExist("user_email", USER_EMAIL);

                    if(isExist){
                        response = getResponse(221);    
                        procClose(request, (String)response.get("msg"));      
                        return response;        
                    }
                }

                Integer REFERRAL_USER_IDX = null;


                // 파라미터에 추천인 코드가 있을 경우 해당 추천인 코드가 유효한지 확인
                if(REFERRAL_CODE != null && !REFERRAL_CODE.equals("")){
                    REFERRAL_USER_IDX = userService.findReferralUser(REFERRAL_CODE);

                }else{
                    REFERRAL_CODE = null;
                }

                
                // 이메일, 비밀번호, 이용약관동의 여부를 포함한 회원 정보 등록 
                Integer USER_IDX    =
                    userService.insertRecord(
                    USER_EMAIL, 
                    USER_PASSWORD, 
                    USER_SERVICE_AGREE, 
                    USER_UUID, 
                    com.pood.server.config.meta.user.USER_MEMBERSHIP_LEVEL.ML_WELCOME_NAME,
                    com.pood.server.config.meta.user.USER_MEMBERSHIP_LEVEL.ML_WELCOME_POINT,
                    com.pood.server.config.meta.user.USER_MEMBERSHIP_LEVEL.ML_WELCOME_PRICE,
                    com.pood.server.config.meta.user.USER_MEMBERSHIP_LEVEL.ML_WELCOME_MONTH,
                    USER_PHONE_NUMBER,
                    USER_NAME);
                


                if(USER_IDX == -1){
                    // 삽입시 NULL이 들어갈 경우 231 에러
                    response = getResponse(231);
                    procClose(request, (String)response.get("msg"));
                    return response;
                }
                


                
                // 추천인 코드 입력시 추천인 코드 테이블에 저장 
                if (Objects.nonNull(REFERRAL_USER_IDX)) {
                    userReferralCodeService.insertRecord(USER_IDX, REFERRAL_USER_IDX, REFERRAL_CODE);
                }
        



                // 회원 가입 테이블에 로그인 타입에 따라 가입 기록 갱신
                String  SNS_KEY     =   header.getSns_key();

                if(SNS_KEY != null)
                    userJoinService.insertRecord(LOGIN_TYPE, USER_IDX, USER_UUID, SNS_KEY);

        


                // 개인정보 처리 방침에 따라 동의여부 기록 갱신
                userLogPolicyService.insert(
                    USER_IDX, 
                    com.pood.server.config.meta.user.LOG_USER_POLICY.PRIVATE_POLICY, 
                    header.getPrivate_policy(),
                    USER_UUID);
                



                
                // 회원 가입 테이블에 이메일 알림 유무에 따라 이메일 알림 기록 갱신
                if(header.getEmail_noti() != null)
                    userLogPolicyService.insert(
                        USER_IDX, 
                        com.pood.server.config.meta.user.LOG_USER_POLICY.EMAIL_NOTI, 
                        header.getEmail_noti(),
                        USER_UUID);



                        
                // 웰컴쿠폰 신규 발급
                userCouponService.issueWelcomeCoupon(USER_IDX);




                // 회원 가입 API 응답 값 반환 
                HashMap<String, Object> result = new HashMap<>();        
            
                result.put("user_email", USER_EMAIL);
                result.put("user_login_type", header.getLogin_type());
                result.put("user_uuid", USER_UUID);

                response.put("result", result);

            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        

        
        return response;
    }
}