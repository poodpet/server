package com.pood.server.api.mapper.order.goods;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pood.server.api.req.header.order.goods.hAM_goods_2_3;
import com.pood.server.api.service.view.view_2.*;

@RestController
public class goods_2_3 extends PROTOCOL {

    Logger logger = LoggerFactory.getLogger(goods_2_3.class);
  
    @Autowired
    @Qualifier("view2Service")
    view2Service view2Service;

    @ResponseBody
    @RequestMapping(value = "/pood/order/goods/2/3", method = RequestMethod.POST)
    public HashMap<String, Object> LIST_KEYWORD(
        @RequestBody hAM_goods_2_3 header, HttpServletRequest request) throws Exception {
    

        try{

            procInit(request);
                
            response = getResponse(200);
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_ORDER_GOODS_2_3){
                
                logger.info("KEYWORD : "+header.getKeyword()+", PC_IDX : "+header.getPc_idx());
                List<String> list = view2Service.getKeywordList(header.getKeyword(), header.getPc_idx());
                response.put("result", list);
                list = null;

            }else response = getResponse(219);

            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
         
            

        return response;
    }
}  