package com.pood.server.api.mapper.admin.coupon;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.meta.coupon.*;
import com.pood.server.config.*;

import com.pood.server.dto.meta.dto_coupon_2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class coupon_2 extends PROTOCOL {
 
    @Autowired
    @Qualifier("couponService")
    couponService couponService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/admin/coupon/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_POINT(HttpServletRequest request) throws Exception {
        
        try{
            
            procInit(request);

            response = getResponse(200);
  
            if(PROTOCOL_IDENTIFIER.PROTOCOL_ADMIN_COUPON_2){                    
                List<dto_coupon_2> result = couponService.getCouponList();
                response.put("result", result);
                result = null;

            }else response = getResponse(219);
            
            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        

        return response;
    }
} 