package com.pood.server.api.mapper.user.review_clap;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.user.review_clap.hUser_review_clap_1;
import com.pood.server.config.*;
import com.pood.server.api.service.user.*;
import com.pood.server.object.user.vo_user_review_clap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController 
public class user_review_clap_1 extends PROTOCOL{
   

    @Autowired
    @Qualifier("userReviewService")
    userReviewService userReviewService;

    @Autowired
    @Qualifier("userReviewClapService")
    userReviewClapService userReviewClapService;
    

    @ResponseBody
    @RequestMapping(value = "/pood/user/review/clap/1", method = RequestMethod.POST)
    public HashMap<String, Object> ADD_REVIEW_CLAP(@RequestBody hUser_review_clap_1 header, HttpServletRequest request)
            throws Exception {
                 
        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_REVIEW_CLAP_1){        

                Integer USER_IDX    = userService.getUserIDX(header.getUser_uuid());
                Integer REVIEW_IDX  = header.getReview_idx();

                vo_user_review_clap review_clap = new vo_user_review_clap();
                review_clap  = userReviewClapService.checkReviewClap(USER_IDX, REVIEW_IDX);
                Boolean isReviewed = review_clap.getIsReviewed();
        
                Integer REVIEW_CLAP_IDX = review_clap.getIdx();


                /*********** 도움이 있을 경우 도움 해제 ************/
                if(isReviewed){
                    userReviewClapService.deleteRecord(REVIEW_CLAP_IDX);
                    response.put("isClap", 0);
                }else {
                    Integer RESULT_IDX = userReviewClapService.insertReviewClap(USER_IDX, REVIEW_IDX);
                    if(RESULT_IDX == -1){
                        // 삽입시 NULL이 들어갈 경우 231 에러
                        response = getResponse(231);
                        procClose(request, (String)response.get("msg"));
                        return response;
                    }
                    response.put("isClap", 1);
                }
            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
         

        
        return response;
    }
 
}