package com.pood.server.api.mapper.user.review;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.dto.user.review.dto_user_review_4;
import com.pood.server.object.pagingSet;
import com.pood.server.api.service.user.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController 
public class user_review_2_2 extends PROTOCOL{


    @Autowired
    @Qualifier("userReviewService")
    userReviewService userReviewService;
    


    @ResponseBody
    @RequestMapping(value = "/pood/user/review/2/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_USER_REVIEW(
        @RequestParam("product_idx") Integer product_idx,
        @RequestParam(value="recordbirth", required=false) String recordbirth,
        @RequestParam(value="user_uuid", required=false) String user_uuid,
        @RequestParam(value="page_size", required=false) Integer page_size,
        @RequestParam(value="page_number", required=false) Integer page_number, 
        HttpServletRequest request)
            throws Exception {
       
        try{

            procInit(request);
            
            response = getResponse(200);

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_REVIEW_2_2){        

                pagingSet PAGING_SET = new pagingSet(
                    page_size, 
                    page_number, 
                    recordbirth, 
                    userReviewService.getTotalRecordNumber(product_idx));

            
                List<dto_user_review_4> result = 
                        userReviewService.getReviewList(PAGING_SET, product_idx, user_uuid);
                pagingResult(PAGING_SET);
                response.put("result", result);

                result = null;
            }else response = getResponse(219);


            procClose(request, null);

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        

        return response;
    }

   
}