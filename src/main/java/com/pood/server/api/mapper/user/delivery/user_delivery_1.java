package com.pood.server.api.mapper.user.delivery;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.object.user.vo_user_delivery;
import com.pood.server.dto.user.dto_user_delivery_address;
import com.pood.server.api.service.user.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
 
@RestController
public class user_delivery_1 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_delivery_1.class);

    @Autowired
    @Qualifier("userDeliveryService")
    userDeliveryService userDeliveryService;
    
    @ResponseBody 
    @RequestMapping(value = "/pood/user/delivery/1", method = RequestMethod.POST)
    public HashMap<String, Object> ADD_USER_DELIVERY(@RequestBody vo_user_delivery header, HttpServletRequest request)
            throws Exception {
         
        
        try{

            Integer resp_status = procInit(request);

            logger.info(header.toString());
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_DELIVERY_1){

                Integer user_idx = userService.getUserIDX(header.getUser_uuid());
        
                if(header.getDefault_type() == null)
                    header.setDefault_type(false);

                /*************** 만약 특정 배송지를 기본 배송지로 지정할 경우 다른 모든 배송지는 기본 배송지가 아닌 걸로 자동 지정 *************/
                if(header.getDefault_type() == true){
                    List<dto_user_delivery_address> list = userDeliveryService.getUserDeliveryList(user_idx);
                    if(list != null){
                        for(dto_user_delivery_address e : list)
                            userDeliveryService.updateAddressToDefault(e.getIdx()); 
                    }
                    list = null;
                }

                
                String DELIVERY_ADDRESS         = header.getAddress();

                String DELIVERY_ZIPCODE         = header.getZipcode();

                Integer DELIVERY_REMOTE_TYPE = remoteDeliveryService.getRemoteType(DELIVERY_ZIPCODE);
                

                Integer delivery_idx = userDeliveryService.insertUserDeliveryAddress(
                    user_idx, 
                    header.getDefault_type(), 
                    DELIVERY_ADDRESS, 
                    header.getDetail_address(), 
                    header.getName(), 
                    header.getNickname(), 
                    DELIVERY_ZIPCODE, 
                    header.getInput_type(), 
                    DELIVERY_REMOTE_TYPE, 
                    header.getPhone_number()); 

                if(delivery_idx == -1){
                    // 삽입시 NULL이 들어갈 경우 231 에러
                    response = getResponse(231);
                    return response;
                }
                
            }else response = getResponse(219);
        
            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }


        
        return response;
    }

}