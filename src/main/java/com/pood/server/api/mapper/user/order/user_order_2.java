
package com.pood.server.api.mapper.user.order;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.object.pagingSet;
import com.pood.server.object.resp.resp_user_order;
import com.pood.server.api.service.user.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_order_2 extends PROTOCOL {

    /**************** 회원 주문 **************/
    @Autowired
    @Qualifier("userOrderService")
    public userOrderService userOrderService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/order/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_ORDER(@RequestParam(value = "user_uuid") String user_uuid,
            @RequestParam(value = "order_number", required = false) String ORDER_NUMBER,
            @RequestParam(value = "page_size", required = false) Integer page_size,
            @RequestParam(value = "page_number", required = false) Integer page_number, 
            @RequestParam(value = "recordbirth", required = false) String recordbirth, HttpServletRequest request)
            throws Exception{

        

        try{

            // 데이터베이스 커넥션 시작
            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_ORDER_2){

                
                Integer user_idx = userService.getUserIDX(user_uuid);


                pagingSet PAGING_SET = new pagingSet(
                    page_size, 
                    page_number, 
                    recordbirth,
                    userOrderService.getTotalRecordNumber(user_idx, ORDER_NUMBER));

        

                //---- 주문 테이블에서 주문 기록 조회
                List<resp_user_order> data = userOrderService.getUserOrderList(PAGING_SET, user_idx, ORDER_NUMBER);
                response.put("result", data);
                pagingResult(PAGING_SET);    
                data = null;

            }else response = getResponse(219);

            procClose(request, null);

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }



        return response;
    }

}