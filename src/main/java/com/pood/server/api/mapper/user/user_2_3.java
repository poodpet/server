package com.pood.server.api.mapper.user;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.api.service.log.*;
import com.pood.server.api.service.user.*;
import com.pood.server.api.service.user.userService;
import com.pood.server.api.service.user.userReferralCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_2_3 extends PROTOCOL{
 
    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("userReferralCodeService")
    userReferralCodeService userReferralCodeService;

    @Autowired
    @Qualifier("logUserSavePointService")
    logUserSavePointService logUserSavePointService;

    @Autowired
    @Qualifier("logUserOrderService")
    logUserOrderService logUserOrderService;

    @Autowired
    @Qualifier("userPointService")
    userPointService userPointService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/2/3", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_USER(
        @RequestParam(value="user_uuid", required=false) String user_uuid,
        HttpServletRequest request) throws Exception {
    
        
        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_2_3){        

                Integer user_idx = userService.getUserIDX(user_uuid);

                HashMap<String, Object> result = new HashMap<String, Object>();


                // 가입한 친구
                Integer signup_cnt = userReferralCodeService.getSignUPUserCount(user_idx);
                result.put("signup_cnt", signup_cnt);
                response.put("result", result);



                // 구매가 한 번 이상이라도 이루어진 회원의 숫자
                Integer buy_friend_cnt = 0;

                List<Integer> invited_list = userReferralCodeService.getInvitedList(user_idx);

                for(Integer e : invited_list){

                    String  FRIEND_USER_UUID    = userService.getUserUUID(e);

                    Boolean chk                 = logUserOrderService.isUserOrdered(FRIEND_USER_UUID);

                    if(chk)buy_friend_cnt++;
                }

                result.put("first_buyer", buy_friend_cnt);


                // 총 받은 포인트 금액
                Integer given_point = 0;

                given_point = userPointService.getFriendInvitationPoint(user_uuid);

                result.put("given_point", given_point);

                
                
                buy_friend_cnt  =   null;

                result          = null;

                user_idx        = null;

                given_point     = null;

            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        
        return response;
    }
}  