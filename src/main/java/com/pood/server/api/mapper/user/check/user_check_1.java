package com.pood.server.api.mapper.user.check;

import com.pood.server.api.req.header.user.check.hUser_check_1;
import com.pood.server.api.service.user.userClaimService;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import java.util.HashMap;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController 
public class user_check_1 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_check_1.class);

    @Autowired
    @Qualifier("userClaimService")
    userClaimService userClaimService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/check/1", method = RequestMethod.POST)
    public HashMap<String, Object> USER_CHECK_1(@RequestBody hUser_check_1 header, HttpServletRequest request)
            throws Exception {
        
        
        try{
        
            procInit(request);
            
            response = getResponse(200);

            logger.info("회원 이메일 추천인 코드 체크 : " +header.toString());

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_CHECK_1){
                String USER_EMAIL       = header.getUser_email();
                String REFERRAL_CODE    = header.getReferral_code();

                // 이메일이 혹시 가입되어 있는지 확인
                if(USER_EMAIL != null){
                    Boolean isExist = userService.isExist("user_email", USER_EMAIL);

                    if(isExist){
                        response = getResponse(221);          
                        return response;        
                    }
                }

                // 파라미터에 추천인 코드가 있을 경우 해당 추천인 코드가 유효한지 확인
                if(REFERRAL_CODE != null) {
                    if (Objects.isNull(userService.findReferralUser(REFERRAL_CODE))) {
                        response = getResponse(222);
                    }
                }
                
            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        

       return response;
    }
}