package com.pood.server.api.mapper.order.goods;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pood.server.api.req.header.order.goods.hAM_goods_2_2;

import com.pood.server.api.service.meta.goods.*;

@RestController
public class goods_2_2 extends PROTOCOL {
  
    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;
    

    @ResponseBody
    @RequestMapping(value = "/pood/order/goods/2/2", method = RequestMethod.POST)
    public HashMap<String, Object> LIST_PRODUCT_OUT_OF_STOCK(
        @RequestBody hAM_goods_2_2 header, HttpServletRequest request) throws Exception {
    

        try{

            procInit(request);
                
            response = getResponse(200);
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_ORDER_GOODS_2_2){
                
                List<HashMap<String, Object>> result = new ArrayList<HashMap<String, Object>>();
                
                List<Integer> list = header.getGoods_idx_list();

                if(list != null){
                
                    for(Integer e :list){

                        HashMap<String, Object> record = new HashMap<String, Object>();

                        Boolean isPurchase = goodsService.isOnSale(e);

                        record.put("goods_idx",     e);
                        record.put("result",        isPurchase);

                        result.add(record);

                        isPurchase  = null;

                        record      = null;
                    }
                }

                response.put("result", result);

                result = null;

            }else response = getResponse(219);

            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
         
            

        return response;
    }
}  