package com.pood.server.api.mapper.user.wish;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.user.userWishService;
import com.pood.server.config.*;
import com.pood.server.dto.meta.goods.dto_goods_10;
import com.pood.server.dto.user.dto_user_wish;
import com.pood.server.api.service.meta.goods.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_wish_2_1 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_wish_2_1.class);

    @Autowired
    @Qualifier("userWishService")
    userWishService userWishService;

    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/wish/2/1", method = RequestMethod.GET)
    public HashMap<String, Object> GET_USER_WISH_LIST(@RequestParam(value="user_uuid", required=false) String user_uuid, HttpServletRequest request)
            throws Exception{
         

        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_WISH_2){        
                Integer USER_IDX    = userService.getUserIDX(user_uuid);
                if(USER_IDX == null){
                    response = getResponse(203);
                    return response;
                }

                logger.info("GOODS_IDX : ");
                List<dto_user_wish> list = userWishService.getUserWishList(USER_IDX);

                List<HashMap<String, Object>> result = new ArrayList<HashMap<String, Object>>();
                
                if(list != null){
                    for(dto_user_wish e : list){
                        HashMap<String, Object> record = new HashMap<String, Object>();
                        Integer GOODS_IDX = e.getGoods_idx();
                        dto_goods_10 goods_info = goodsService.GET_GOODS_OBJECT_3(GOODS_IDX);                
                        if(goods_info != null){
                            record.put("goods_info", goods_info);
                            result.add(record);
                            logger.info("[" + GOODS_IDX + "]");
                        }
                    }
                }
                response.put("result", result);

                list = null;
                result = null;

            }else response = getResponse(219);

            procClose(request, "");

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }


        
        return response;
    }
}