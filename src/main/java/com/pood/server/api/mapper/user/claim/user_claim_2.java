package com.pood.server.api.mapper.user.claim;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.dto_image_2;
import com.pood.server.dto.meta.product.dto_product_5;
import com.pood.server.dto.user.dto_user_claim;
import com.pood.server.api.service.user.*;
import com.pood.server.api.service.meta.goods.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_claim_2 extends PROTOCOL{
 
    @Autowired
    @Qualifier("userClaimService")
    userClaimService userClaimService;

    @Autowired
    @Qualifier("goodsProductService")
    goodsProductService goodsProductService;

    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/claim/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_USER_CLAIM(
        @RequestParam("user_uuid") String user_uuid, 
        @RequestParam(value="isAnswer", required=false) Boolean isAnswer,
        @RequestParam(value="page_size", required=false) Integer page_size, 
        @RequestParam(value="page_number", required=false) Integer page_number,
        @RequestParam(value="recordbirth", required=false) String recordbirth,  
        HttpServletRequest request)
            throws Exception {


        try{
        
            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_CLAIM_2){

                pagingSet PAGING_SET = new pagingSet(
                    page_size, 
                    page_number, 
                    recordbirth,
                    userClaimService.getTotalRecordNumber(user_uuid));


                List<dto_user_claim> result = userClaimService.getUserClaimList(PAGING_SET, user_uuid, isAnswer);

                // 회원 문의 굿즈 이미지
                if(result != null){
                    for(dto_user_claim e : result){

                        Integer GOODS_IDX = e.getClaim_goods_idx();
                        List<dto_product_5>     GOODS_PRODUCT_LIST  = goodsProductService.getgoodsProductList3(GOODS_IDX);
                        List<dto_image_2>       GOODS_IMAGE         = goodsService.getGoodsImageList2(GOODS_IDX);
                        List<dto_image_2>       PRODUCT_IMAGE       = new ArrayList<dto_image_2>();
                
                        if(GOODS_PRODUCT_LIST != null){
                            for(dto_product_5 e1 : GOODS_PRODUCT_LIST){
                                Integer PRODUCT_IDX = e1.getIdx();
                                List<dto_image_2> tmp = productService.getProductImageList(PRODUCT_IDX);
                                PRODUCT_IMAGE.addAll(tmp);
                            }
                        }
                        
                        e.setGoods_image(GOODS_IMAGE);
                        e.setMain_image(PRODUCT_IMAGE);
                        GOODS_IMAGE     =   null;
                        PRODUCT_IMAGE   =   null;

                    }
                }

                response.put("result", result);
                result = null;
                
            }else response = getResponse(219);
    
            procClose(request, null);

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

         
        return response;
    }

}