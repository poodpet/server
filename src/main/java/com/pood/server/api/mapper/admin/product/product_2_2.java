package com.pood.server.api.mapper.admin.product;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.view.view_7.*;
import com.pood.server.config.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class product_2_2 extends PROTOCOL {
 
     
    @Autowired
    @Qualifier("view7Service")
    view7Service view7Service;
    
    @ResponseBody
    @RequestMapping(value = "/pood/admin/product/2/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_PRODUCT(
        @RequestParam(value="product_idx", required=false) Integer product_idx, 
        HttpServletRequest request) throws Exception {
        

        response = getResponse(200);

        try{

            procInit(request);
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_ADMIN_PRODUCT_2_2){
                
                List<Integer> result = view7Service.getGoodsIDXList(product_idx);
                response.put("result",result);
                result = null;

            }else response = getResponse(219);

            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

            
        return response;
    }
}  