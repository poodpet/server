package com.pood.server.api.mapper.user.delivery;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.user.delivery.hUser_delivery_3;
import com.pood.server.config.*;
import com.pood.server.api.service.user.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_delivery_3 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_delivery_3.class);

    @Autowired
    @Qualifier("userDeliveryService")
    userDeliveryService userDeliveryService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/delivery/3", method = RequestMethod.POST)
    public HashMap<String, Object> DELETE_USER_DELIVERY(@RequestBody hUser_delivery_3 header, HttpServletRequest request)
            throws Exception {
        
        try{

            logger.info(header.toString());
                    
            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(header.getIdx() == null){
                response = getResponse(300);
                procClose(request, response.toString());
                return response;
            }
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_DELIVERY_3){

                Integer USER_IDX = null;
                // 회원 배송지 삭제
                for(Integer e:header.getIdx()){
                    // 해당 카드 항목 번호의 회원 번호 조회
                    USER_IDX = userDeliveryService.getUserIndex(e);
                    logger.info(e.toString());
                    userDeliveryService.deleteRecord(e);
                }

                // 현재 회원이 카드가 몇개 남았는지 조회
                if(USER_IDX != null){
                    Integer CARD_COUNT = userDeliveryService.getCountDeliveryAddress(USER_IDX);
                    // 만약 남은 카드 개수가 하나일 경우 그 카드는 자동으로 디폴트로 지정
                    if(CARD_COUNT == 1){
                        userDeliveryService.updateDefaultAddress(USER_IDX);
                    }
                }       

            }else response = getResponse(219);
            
            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        
        

        return response;
    }

}