package com.pood.server.api.mapper.user.my_page;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.meta.order.OrderService;
import com.pood.server.api.service.user.*;
import com.pood.server.config.*;
import com.pood.server.object.resp.resp_ava_list;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class my_page_2 extends PROTOCOL{
 
    @Autowired
    @Qualifier("orderService")
    OrderService orderService;
 
    @Autowired
    @Qualifier("userCouponService")
    userCouponService userCouponService;

    @Autowired
    @Qualifier("userNotiService")
    userNotiService userNotiService;

    @Autowired
    @Qualifier("userPetService")
    userPetService userPetService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/my-page/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_USER_MY_PAGE(@RequestParam("user_uuid") String USER_UUID, HttpServletRequest request)
            throws Exception {
       

        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_MY_PAGE_2){        

                Integer USER_IDX        = userService.getUserIDX(USER_UUID);                    // 회원 고유 번호

                Integer ORDER_CNT       = orderService.getShippingOrderCount(USER_IDX);         // 주문 개수

                Integer COUPON_CNT      = userCouponService.getAVAcouponCount(USER_IDX);        // 쿠폰 개수   
                
                Integer MY_POOD_POINT   = userService.getUserPoint(USER_UUID);                  // 내 푸드 포인트

                Integer USER_PET_CNT    = userPetService.getUserPetCount(USER_UUID);               // 반려동물 개수

                List<resp_ava_list> ava_review  = orderService.getAVAReviewList(USER_IDX);

                Integer unread_noti = userNotiService.getUnreadMsg(USER_IDX);
    
                HashMap<String, Object> result = new HashMap<String, Object>();

                Integer REVIEW_CNT = ava_review.size();

                result.put("user_pet_cnt",  USER_PET_CNT);
                result.put("order_cnt",     ORDER_CNT);
                result.put("coupon_cnt",    COUPON_CNT);
                result.put("total_point",   MY_POOD_POINT);

                if(unread_noti == 0)
                    result.put("unread_noti",  false);
                else result.put("unread_noti", true);

                result.put("review_cnt",    REVIEW_CNT);

                response.put("result",      result);

            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        
        

        return response;
    }

}