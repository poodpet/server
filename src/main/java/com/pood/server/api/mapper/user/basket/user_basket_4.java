package com.pood.server.api.mapper.user.basket;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.user.basket.hUser_basket_4;
import com.pood.server.api.service.user.userBasketService;
import com.pood.server.config.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_basket_4 extends PROTOCOL {

    Logger logger = LoggerFactory.getLogger(user_basket_4.class);

    @Autowired
    @Qualifier("userBasketService")
    userBasketService userBasketService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/basket/4", method = RequestMethod.POST)
    public HashMap<String, Object> UPDATE_USER_BASKET_QTY(
        @RequestBody hUser_basket_4 header,
         HttpServletRequest request)
            throws Exception {


        try{

            logger.info(header.toString());

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_BASKET_4){
                userBasketService.updateBasketQuantity(header.getIdx(), header.getQty());
            }else response = getResponse(219);
    
            
            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

            
        return response;
    }
} 