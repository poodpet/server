package com.pood.server.api.mapper.user.pet_ai_feed;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.user.pet_ai_feed.hUser_pet_ai_feed_1;
import com.pood.server.api.service.user.userPetAIFeedService;
import com.pood.server.config.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_pet_ai_feed_1 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_pet_ai_feed_1.class);
 
    @Autowired
    @Qualifier("userPetAIFeedService")
    userPetAIFeedService userPetAIFeedService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/pet-ai-feed/1", method = RequestMethod.POST)
    public HashMap<String, Object> USER_PET_AI_FEED_1(@RequestBody hUser_pet_ai_feed_1 header, HttpServletRequest request)
            throws Exception {
        

        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_PET_AI_FEED_1){

                logger.info(header.toString());
                
                Integer         user_idx    = userService.getUserIDX(header.getUser_uuid());

                Integer         up_idx      = header.getUp_idx();
                
                List<Integer>   product_idx = header.getProduct_idx();

                Integer RESULT_IDX = userPetAIFeedService.insertRecord(user_idx, up_idx, product_idx);
            
                if(RESULT_IDX == -1){
                    response = getResponse(231);
                    procClose(request, (String)response.get("msg"));
                    return response;
                }

                user_idx = null;

                up_idx = null;

                product_idx = null;
                
            }else response = getResponse(219);
    

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        
        
        
        return response;
    }
}