package com.pood.server.api.mapper.user;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.object.IMP.IMP_VBANK;
import com.pood.server.api.service.iamport.*;
import com.pood.server.api.service.json.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_2_2 extends PROTOCOL{
 
    @Autowired
    @Qualifier("iamportService")
    iamportService iamportService;

    @Autowired
    @Qualifier("jsonService")
    jsonService jsonService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/2/2", method = RequestMethod.GET)
    public HashMap<String, Object> CHECK_USER_VBANK(
        @RequestParam(value="bank_code", required=false) String bank_code,
        @RequestParam(value="bank_num", required=false) String bank_num,
        HttpServletRequest request) throws Exception {
    
        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_2_1){        
                ResponseEntity<String> result = iamportService.checkBank(bank_code, bank_num);

                if(result != null){
                    Integer STATUS_CODE = result.getStatusCodeValue();
                    if(STATUS_CODE == 200){

                        IMP_VBANK IMP_VBANK = 
                            (IMP_VBANK)jsonService.getDecodedObject(result.getBody().toString(), IMP_VBANK.class);

                        response.put("bank_holder", IMP_VBANK.getResponse().getBank_holder());
                    }
                }else response = getResponse(233);
                
                
            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        
        return response;
    }
}  