package com.pood.server.api.mapper.user.pet;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.api.service.user.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_pet_3 extends PROTOCOL{
  
    @Autowired
    @Qualifier("userPetService")
    userPetService userPetService;

    @Autowired
    @Qualifier("userPetAIService")
    userPetAIService userPetAIService;

    @Autowired
    @Qualifier("userPetAllergyService")
    userPetAllergyService userPetAllergyService;

    @Autowired
    @Qualifier("userPetFeedService")
    userPetFeedService userPetFeedService;

    @Autowired
    @Qualifier("userPetGrainSizeService")
    userPetGrainSizeService userPetGrainSizeService;

    @Autowired
    @Qualifier("userPetSickService")
    userPetSickService userPetSickService;

    @Autowired
    @Qualifier("userPetAIFeedService")
    userPetAIFeedService userPetAIFeedService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/pet/3", method = RequestMethod.GET)
    public HashMap<String, Object> DELETE_USER_PET(@RequestParam("up_idx") Integer up_idx, HttpServletRequest request)
            throws Exception {


        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(up_idx == null){
                response = getResponse(300);
                procClose(request, response.toString());
                return response;
            }
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_PET_3){          

                // 테이블 : USER_PET
                userPetService.deleteRecord(up_idx);

                // 테이블 : USER_PET_IMAGE
                userPetService.deleteImageWithUserPetIDX(up_idx);

                // 테이블 : USER_PET_AI_DIAGNOSIS
                userPetAIService.deleteRecordWithUserPetIDX(up_idx);

                // 테이블 : USER_PET_SICK
                userPetSickService.deleteRecordWithUserPetIDX(up_idx);

                // 테이블 : USER_PET_FEED
                userPetFeedService.deleteRecordWithUserPetIDX(up_idx);

                // 테이블 : USER_PET_GRAIN_SIZE
                userPetGrainSizeService.deleteRecordWithUserPetIDX(up_idx);

                // 테이블 : USER_PET_ALLERGY
                userPetAllergyService.deleteRecordWithUserPetIDX(up_idx);

                // 테이블 : USER_PET_AI_FEED
                userPetAIFeedService.deleteRecordWithUserPetIDX(up_idx);

            }else response = getResponse(219);
        
            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        
        return response;
    }

}