package com.pood.server.api.mapper.user.pet_ai_feed;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.object.pagingSet;
import com.pood.server.object.resp.resp_user_pet_ai_feed;
import com.pood.server.api.service.user.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_pet_ai_feed_2 extends PROTOCOL{
 
    @Autowired
    @Qualifier("userPetAIFeedService")
    userPetAIFeedService userPetAIFeedService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/pet-ai-feed/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_USER_PET_AI_FEED(
        @RequestParam(value="page_size", required=false) Integer page_size, 
        @RequestParam(value="page_number", required=false) Integer page_number,
        @RequestParam(value="recordbirth", required=false) String recordbirth,    
        @RequestParam(value="up_idx", required=false) Integer up_idx,
        @RequestParam("user_uuid") String user_uuid, HttpServletRequest request)
            throws Exception {
       

        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_PET_AI_FEED_2){

                Integer user_idx = userService.getUserIDX(user_uuid);
            
                pagingSet PAGING_SET = new pagingSet(
                    page_size, 
                    page_number, 
                    recordbirth,
                    userPetAIFeedService.getTotalRecordNumber(user_idx));

                List<resp_user_pet_ai_feed> result = userPetAIFeedService.getList(PAGING_SET, user_idx, up_idx);
                response.put("result", result);
                result = null;

            }else response = getResponse(219);

            procClose(request, "");

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
 

        return response;
    } 
}