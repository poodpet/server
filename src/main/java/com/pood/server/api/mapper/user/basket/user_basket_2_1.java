package com.pood.server.api.mapper.user.basket;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.meta.coupon.*;
import com.pood.server.api.service.user.*;
import com.pood.server.config.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_basket_2_1 extends PROTOCOL {

    
    @Autowired
    @Qualifier("couponService")
    public couponService couponService;


    @Autowired
    @Qualifier("userBasketService")
    public userBasketService userBasketService;




    
    @ResponseBody
    @RequestMapping(value = "/pood/user/basket/2/1", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_USER_BASKET(
        @RequestParam(value="user_uuid", required=false) String user_uuid,
        HttpServletRequest request)
            throws Exception {
         

        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_BASKET_2){
                if(user_uuid == null){
                    response = getResponse(300);
                    return response;
                }

                Integer cnt = userBasketService.getUserBasketCount(user_uuid);

                response.put("total_cnt", cnt);
            }else response = getResponse(219);
        

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        

        
        return response;
    }
 
}