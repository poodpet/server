package com.pood.server.api.mapper.main;

import com.pood.server.api.service.holiday_check.holidayCheckService;
import com.pood.server.api.service.meta.category.categoryService;
import com.pood.server.api.service.meta.category.categoryTopService;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import com.pood.server.object.pagingSet;
import com.pood.server.object.resp.resp_category;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class main_2_2 extends PROTOCOL {

    @Autowired
    @Qualifier("categoryService")
    categoryService categoryService;

    @Autowired
    @Qualifier("categoryTopService")
    categoryTopService categoryTopService;

    @Autowired
    @Qualifier("holidayCheckService")
    holidayCheckService holidayCheckService;

    @GetMapping(value = "/pood/main/2/2")
    public Map<String, Object> CATEGORY_LIST(
        @RequestParam(value = "pc_id", required = false) Integer pc_id,
        @RequestParam(value = "page_size", required = false) Integer page_size,
        @RequestParam(value = "page_number", required = false) Integer page_number,
        @RequestParam(value = "recordbirth", required = false) String recordbirth,
        HttpServletRequest request) throws Exception {

        try {

            procInit(request);

            response = getResponse(200);

            Integer headerCheck = headerService.checkPagingSet(page_size, page_number, recordbirth);

            if (headerCheck != 200) {
                response = getResponse(headerCheck);
                procClose(request, response.toString());
                return response;
            }

            if (PROTOCOL_IDENTIFIER.PROTOCOL_ADMIN_MAIN_2_2) {
                pagingSet PAGING_SET = new pagingSet(
                    page_size,
                    page_number,
                    recordbirth,
                    categoryTopService.getTotalRecordNumber());

                resp_category result = categoryTopService.getList(pc_id);
                response.put("result", result);
                pagingResult(PAGING_SET);
                result = null;

            } else {
                response = getResponse(219);
            }

            procClose(request, (String) response.get("msg"));

        } catch (Exception e) {
            thExecption(request.getServletPath(), e);
        }

        return response;
    }

}  