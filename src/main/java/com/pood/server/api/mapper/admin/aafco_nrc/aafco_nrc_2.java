package com.pood.server.api.mapper.admin.aafco_nrc;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.meta.aafco_nrc.aafcoNRCService;
import com.pood.server.config.*;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_aafco_nrc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class aafco_nrc_2 extends PROTOCOL{

    @Autowired
    @Qualifier("aafcoNRCService")
    aafcoNRCService aafcoNRCService;
     
    @ResponseBody
    @RequestMapping(value = "/pood/admin/aafco-nrc/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_AAFCO_NRC(HttpServletRequest request,
        @RequestParam(value="page_size", required=false) Integer page_size, 
        @RequestParam(value="recordbirth", required=false) String recordbirth, 
        @RequestParam(value="page_number", required=false) Integer page_number) throws Exception {
    

        try{

            procInit(request);
        
            response = getResponse(200);
            
            Integer headerCheck = headerService.checkPagingSet(page_size, page_number, recordbirth);

            if(headerCheck != 200){
                response = getResponse(headerCheck);
                procClose(request, response.toString());
                return response;
            }
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_ADMIN_AAFCO_NRC_2){
                pagingSet PAGING_SET = new pagingSet(
                    page_size, 
                    page_number, 
                    recordbirth,
                    aafcoNRCService.getTotalRecordNumber());
                    

                List<dto_aafco_nrc> result = aafcoNRCService.getAafcoNRCList(PAGING_SET);
                response.put("result", result);
                pagingResult(PAGING_SET); 

                result = null;

            }else response = getResponse(219);
            
            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        
        
        return response;
    }
 
}  