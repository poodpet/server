package com.pood.server.api.mapper.user.pet.image;


import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.admin.hAM_image_delete;
import com.pood.server.config.*;

import com.pood.server.api.service.user.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_pet_image_3 extends PROTOCOL {

    Logger logger = LoggerFactory.getLogger(user_pet_image_3.class);
  
    @Autowired
    @Qualifier("userPetService")
    userPetService userPetService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/pet/image/3", method = RequestMethod.POST)
    public HashMap<String, Object> DELETE_USER_PET_IMAGE_3(
        @RequestBody hAM_image_delete header, HttpServletRequest request) throws Exception {
        

        try{

            Integer resp_status = procInit(request);
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            logger.info("동물 이미지 삭제 : ");

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_PET_IMAGE_3){        
                // 이미지 파일 삭제
                for(Integer e : header.getImage_idx()){
                    logger.info("[" + e + "]");
                    userPetService.deleteImage(e);
                }
            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

 
        return response;
    }
 
}
