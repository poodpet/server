package com.pood.server.api.mapper.user;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.dto.meta.dto_user_info;
import com.pood.server.api.service.user.userService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_2_4 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_2_4.class);
 
    @Autowired
    @Qualifier("userService")
    userService userService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/2/4", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_USER(
        @RequestParam(value="user_name",    required=false) String encoded_name,
        @RequestParam(value="user_phone",   required=false) String user_phone,
        HttpServletRequest request) throws Exception {
    

        try{

            String user_name = AES256.aesDecode(encoded_name);

            logger.info("DECODED : " + user_name);

            
            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_2_4){        
                dto_user_info USER_INFO = userService.getUserUUID(user_name, user_phone);
                if(USER_INFO != null)
                    response.put("user_uuid", USER_INFO.getUser_uuid());
            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        
        return response;
    }
}  