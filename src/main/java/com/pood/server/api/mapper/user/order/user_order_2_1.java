
package com.pood.server.api.mapper.user.order;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.log.logUserOrderService;
import com.pood.server.config.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_order_2_1 extends PROTOCOL {

    @Autowired
    @Qualifier("logUserOrderService")
    public logUserOrderService logUserOrderService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/order/2/1", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_ORDER(@RequestParam(value = "user_uuid") String user_uuid,
            HttpServletRequest request)
            throws Exception {


        try{

            // 데이터베이스 커넥션 시작
            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                    
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_ORDER_2_1){

                Integer pay_cnt =       logUserOrderService.getPayCount(user_uuid);
                
                Integer purchase_cnt =  logUserOrderService.getPurchasedCnt(user_uuid);
    
                HashMap<String, Object> result = new HashMap<String, Object>();
                result.put("pay_cnt", pay_cnt);
                result.put("purchase_cnt", purchase_cnt);

                response.put("result", result);

            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }



        return response;
    }

}