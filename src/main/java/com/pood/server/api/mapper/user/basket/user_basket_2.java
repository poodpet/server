package com.pood.server.api.mapper.user.basket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.meta.coupon.*;
import com.pood.server.api.service.user.*;
import com.pood.server.config.*;
import com.pood.server.config.meta.user.USER_BASKET_STATUS;
import com.pood.server.dto.user.dto_user_basket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_basket_2 extends PROTOCOL {

    
    @Autowired
    @Qualifier("couponService")
    public couponService couponService;


    @Autowired
    @Qualifier("userBasketService")
    public userBasketService userBasketService;




    
    @ResponseBody
    @RequestMapping(value = "/pood/user/basket/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_USER_BASKET(
        @RequestParam(value="user_uuid", required=false) String user_uuid,
        @RequestParam(value="idx", required=false) Integer basket_idx[],  
        HttpServletRequest request)
            throws Exception {
         

        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_BASKET_2){
                List<dto_user_basket> result = new ArrayList<dto_user_basket>();
    
                // 회원에 대한 장바구니 목록 조회
                List<Integer> USER_BASKET_LIST = userBasketService.getUserBasketList(user_uuid);
            

                
                // 파라미터로 들어온 basket_idx가 null인 경우 회원이 가지고 있는 전체 장바구니 목록을 내려줄 것.
                if(basket_idx == null){
                    basket_idx = new Integer[USER_BASKET_LIST.size()];
                    basket_idx = USER_BASKET_LIST.toArray(basket_idx);
                }


                Integer TOTAL_CNT = 0;
                
                for(Integer e:basket_idx){
                    TOTAL_CNT ++;
                    if(USER_BASKET_LIST.contains(e)){
                        dto_user_basket record = userBasketService.getUserBasket(e);
                        Integer BASKET_STATUS = record.getStatus();
                        if(BASKET_STATUS != USER_BASKET_STATUS.NOT_VISIBLE)
                            result.add(record);
                    }

                    if(TOTAL_CNT == 50)break;
                }
        
                
                response.put("result", result);

                result = null;

            }else response = getResponse(219);
        

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        

        
        return response;
    }
 
}