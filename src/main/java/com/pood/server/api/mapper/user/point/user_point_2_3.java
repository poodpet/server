package com.pood.server.api.mapper.user.point;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.log.*;
import com.pood.server.api.service.user.userPointService;
import com.pood.server.config.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_point_2_3 extends PROTOCOL {
     
    @Autowired
    @Qualifier("logUserUsePointService")
    logUserUsePointService logUserUsePointService;

    @Autowired
    @Qualifier("logUserSavePointService")
    logUserSavePointService logUserSavePointService;

    @Autowired
    @Qualifier("userPointService")
    userPointService userPointService;
    @ResponseBody
    @RequestMapping(value = "/pood/user/point/2/3", method = RequestMethod.GET)
    public HashMap<String, Object> GET_USER_POINT(
        @RequestParam(value="user_uuid") String user_uuid,
        HttpServletRequest request) throws Exception {
    
        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;    
            }
    
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_POINT_2_3){

                HashMap<String, Object> result = new HashMap<String, Object>();
                
                // 내 푸드 포인트
                Integer CURRENT_POINT       = userService.getUserPoint(user_uuid);

                // 총 받은 혜택
                Integer GIVEN_POINT         = logUserSavePointService.getTotalGivenPoint(user_uuid);

                // 적립 예정
                Integer TO_BE_SAVED_POINT   = logUserSavePointService.getToBeSavedPoint(user_uuid);

                // 소멸 예정
                long    TO_BE_REMOVED       = userPointService.getToBeRemovedPoint(user_uuid);

                result.put("curent_point",      CURRENT_POINT);
                result.put("to_be_expired",     TO_BE_REMOVED);
                result.put("ready_to_save",     TO_BE_SAVED_POINT);
                result.put("total_point",       GIVEN_POINT);

                response.put("result", result);
            }else response = getResponse(219);

            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        
        return response;
    }
}  