package com.pood.server.api.mapper.admin.coupon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.meta.coupon.*;
import com.pood.server.config.*;
import com.pood.server.dto.meta.dto_coupon_2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class coupon_2_1 extends PROTOCOL {
 
    @Autowired
    @Qualifier("couponService")
    public couponService couponService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/admin/coupon/2/1", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_POINT(
        @RequestParam(value="goods_idx", required=false) Integer param_goods_idx,
        @RequestParam(value="brand_idx", required=false) Integer param_brand_idx,
    HttpServletRequest request) throws Exception {
        
        try{
                
            procInit(request);

            response = getResponse(200);
                
            if(PROTOCOL_IDENTIFIER.PROTOCOL_ADMIN_COUPON_2_1){
                    
                List<dto_coupon_2> result = new ArrayList<dto_coupon_2>();
                
                if((param_goods_idx == null) && (param_brand_idx == null)){
                    result = couponService.getCouponList3();    
                }else result = couponService.getCouponList5(param_goods_idx, param_brand_idx);
                    
                response.put("coupon_info", result);

                result = null;

            }else response = getResponse(219);

            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        

        return response;
    }
} 