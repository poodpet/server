package com.pood.server.api.mapper.user.basket;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.user.basket.hUser_basket_4_1;
import com.pood.server.api.service.user.userCouponService;
import com.pood.server.config.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_basket_4_1 extends PROTOCOL {

    Logger logger = LoggerFactory.getLogger(user_basket_4_1.class);

    @Autowired
    @Qualifier("userCouponService")
    userCouponService userCouponService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/basket/4/1", method = RequestMethod.POST)
    public HashMap<String, Object> UPDATE_USER_BASKET_COUPON(
        @RequestBody hUser_basket_4_1 header,
         HttpServletRequest request)
            throws Exception {
         

        try{

            logger.info("장바구니 쿠폰 항목 매핑 :"+header.toString());
            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_BASKET_4_1){
                Integer TO_BASKET_IDX = header.getTo_basket_idx();
                Integer USER_COUPON_IDX = header.getUser_coupon_idx();
                
                // 장바구니 항목번호에 매핑되어 있는 기존 쿠폰 반납
                userCouponService.rollbackBasketCoupon(TO_BASKET_IDX);

                // 신규 장바구니와 매핑
                userCouponService.toUserCoupon(TO_BASKET_IDX, USER_COUPON_IDX);
            }else response = getResponse(219);
    
            
            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

            
        return response;
    }
} 