package com.pood.server.api.mapper.user.review_clap;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.user.review_clap.hUser_review_clap_2;
import com.pood.server.config.*;
import com.pood.server.dto.user.review.dto_user_review_clap;
import com.pood.server.api.service.user.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController 
public class user_review_clap_2 extends PROTOCOL{
   

    @Autowired
    @Qualifier("userReviewService")
    userReviewService userReviewService;

    @Autowired
    @Qualifier("userReviewClapService")
    userReviewClapService userReviewClapService;
    

    @ResponseBody
    @RequestMapping(value = "/pood/user/review/clap/2", method = RequestMethod.POST)
    public HashMap<String, Object> ADD_REVIEW_CLAP(@RequestBody hUser_review_clap_2 header, HttpServletRequest request)
            throws Exception {
                 
        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_REVIEW_CLAP_2){        
                String USER_UUID = header.getUser_uuid();

                Integer USER_IDX = userService.getUserIDX(USER_UUID);
                
                List<dto_user_review_clap> list = userReviewClapService.getList(USER_IDX);

                USER_UUID   = null;

                USER_IDX    = null; 

                response.put("result", list);
    
            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
         

        
        return response;
    }
 
}