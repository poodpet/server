package com.pood.server.api.mapper.user;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.user.user_info.hUser_info_3;

import com.pood.server.config.*;
import com.pood.server.config.meta.user.USER_STATUS;
import com.pood.server.api.service.user.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_3 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_3.class);

    @Autowired
    @Qualifier("userService")
    userService userService;
    
    @Autowired
    @Qualifier("userBasketService")
    userBasketService userBasketService;

    @Autowired
    @Qualifier("userCouponService")
    userCouponService userCouponService;

    @Autowired
    @Qualifier("userDeliveryService")
    userDeliveryService userDeliveryService;

    @Autowired
    @Qualifier("userNotiService")
    userNotiService userNotiService;

    @Autowired
    @Qualifier("userCardService")
    userCardService userCardService;

    @Autowired
    @Qualifier("userWishService")
    userWishService userWishService;

    @Autowired
    @Qualifier("userReferralCodeService")
    userReferralCodeService userReferralCodeService;

    @Autowired
    @Qualifier("userPointService")
    userPointService userPointService;

    @Autowired
    @Qualifier("userPetService")
    userPetService userPetService;

    @Autowired
    @Qualifier("userPetAIService")
    userPetAIService userPetAIService;

    @Autowired
    @Qualifier("userPetAllergyService")
    userPetAllergyService userPetAllergyService;

    @Autowired
    @Qualifier("userPetFeedService")
    userPetFeedService userPetFeedService;

    @Autowired
    @Qualifier("userPetGrainSizeService")
    userPetGrainSizeService userPetGrainSizeService;

    @Autowired
    @Qualifier("userPetSickService")
    userPetSickService userPetSickService;

    @Autowired
    @Qualifier("userPetAIFeedService")
    userPetAIFeedService userPetAIFeedService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/3", method = RequestMethod.POST)
    public HashMap<String, Object> DELETE_USER(
        @RequestBody hUser_info_3 header,
            HttpServletRequest request) throws Exception {
         
                
        try{

            logger.info("USER UUID : "+header.getUser_uuid());
            
            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_3){
                String  USER_UUID = header.getUser_uuid();
                userService.updateUserStatus(USER_UUID, USER_STATUS.DEACTIVE);
                userService.userInit(USER_UUID);
                USER_UUID = null;

            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }


        return response;
    }
} 