package com.pood.server.api.mapper.order.exchange;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.config.meta.order.ORDER_EXCHANGE;
import com.pood.server.config.meta.order.ORDER_STATUS;
import com.pood.server.config.meta.order.ORDER_STATUS_MESSAGE;
import com.pood.server.dto.meta.order.dto_order;
import com.pood.server.object.meta.vo_order_exchange;
import com.pood.server.api.service.log.logUserOrderService;
import com.pood.server.api.service.meta.order.*;
import com.pood.server.api.service.time.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
 
@RestController
public class order_exchange_1_2 extends PROTOCOL{

    @Autowired
    @Qualifier("orderExchangeService")
    orderExchangeService orderExchangeService;

    @Autowired
    @Qualifier("logUserOrderService")
    logUserOrderService logUserOrderService;

    @Autowired
    @Qualifier("orderService")
    OrderService orderService;

    @Autowired
    @Qualifier("timeService")
    timeService timeService;

    @ResponseBody 
    @RequestMapping(value = "/pood/order/exchange/1/2", method = RequestMethod.GET)
    public HashMap<String, Object> ADD_ORDER_EXCHANGE_1_2(
        @RequestParam(value="uuid", required=false) String uuid,  HttpServletRequest request)
            throws Exception {
         
                
        try{
        
            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            if(PROTOCOL_IDENTIFIER.PROTOCOL_ORDER_EXCHANGE_1_2){


                /*
                
                Integer TIME_LIMIT = timeService.EXCHANGE_REFUND_EXPIRED_HOURS();

                // 교환/반품 신청 만료시간 
                if(TIME_LIMIT == 1){
                    response = getResponse(245);
                    procClose(request, response.toString());
                    return response;
                }

                */

                vo_order_exchange record   = orderExchangeService.getRecord(uuid);

                String  ORDER_NUMBER    = record.getOrder_number();

                dto_order   ORDER           = orderService.getOrder(ORDER_NUMBER);
                
                Integer USER_IDX        = ORDER.getUser_idx();

                String  USER_UUID       = userService.getUserUUID(USER_IDX);




                // 교환 접수 단계로 이미 상태가 변경되었을 경우 철회 불가
                if( record.getType() == ORDER_EXCHANGE.TYPE_ACCEPTANCE ||   // 이미 교환요청이 접수되었거나
                    record.getType() == ORDER_EXCHANGE.TYPE_REJECT ||       // 이미 교환요청이 거절되었거나 
                    record.getType() == ORDER_EXCHANGE.TYPE_ACCEPT          // 이미 교환요청이 승인되었을경우
                ){
                    response = getResponse(246);
                    procClose(request, response.toString());
                    return response;
                }


                

                // 교환 철회에 대해서 로그를 남김
                logUserOrderService.insertOrderHistory(
                                ORDER.getIdx(), 
                                ORDER_STATUS.EXCHANGE_WITHDRAWAL_ACCEPT, 
                                ORDER_STATUS_MESSAGE.EXCHANGE_WITHDRAWAL_ACCEPTED(uuid),
                                ORDER_NUMBER,    
                                USER_UUID,
                                record.getGoods_idx(),
                                record.getQty()
                            );
                            

                // 해당 교환 건에 대해서 [교환철회]로 업데이트
                orderExchangeService.upddateStatus(ORDER_EXCHANGE.TYPE_WITHDRAWAL_ACCEPT, uuid);


                ORDER           = null;

                record          = null;

                ORDER_NUMBER    = null;
                
                USER_IDX        = null;

                USER_UUID       = null;

            }else response = getResponse(219);
        
            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }


        
        return response;
    }

}