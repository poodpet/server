package com.pood.server.api.mapper.admin.pet;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.meta.pet.PETService;
import com.pood.server.config.*;
import com.pood.server.dto.meta.pet.dto_pet;
import com.pood.server.object.pagingSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class pet_2 extends PROTOCOL {
    
    @Autowired
    @Qualifier("PETService")
    PETService PETService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/admin/pet/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_POINT(
        @RequestParam(value="page_size", required=false) Integer page_size, 
        @RequestParam(value="page_number", required=false) Integer page_number, 
        @RequestParam(value="recordbirth", required=false) String recordbirth,
        @RequestParam(value="pc_id", required=false) Integer pc_id,
        @RequestParam(value="keyword", required=false) String keyword,
        HttpServletRequest request) throws Exception {
    

        try{
                
            procInit(request);

            response = getResponse(200);
            
            Integer headerCheck = headerService.checkPagingSet(page_size, page_number, recordbirth);

            if(headerCheck != 200){
                response = getResponse(headerCheck);
                procClose(request, response.toString());
                return response;
            }
    
            if(PROTOCOL_IDENTIFIER.PROTOCOL_ADMIN_PET_2){
                pagingSet PAGING_SET = new pagingSet(
                        page_size, 
                        page_number, 
                        recordbirth,
                        PETService.getTotalRecordNumber(pc_id, keyword));
                
                List<dto_pet> result = PETService.getPetList(PAGING_SET, pc_id, keyword);
                pagingResult(PAGING_SET); 
                response.put("result", result);
                result = null;

            }else response = getResponse(219);

            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        
        return response;
    }
}  