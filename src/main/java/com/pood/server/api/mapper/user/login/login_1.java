package com.pood.server.api.mapper.user.login;

import com.pood.server.api.service.user.userJoinService;
import com.pood.server.api.service.user.userLoginService;
import com.pood.server.api.service.user.userService;
import com.pood.server.api.service.user.userTokenService;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.meta.user.META_USER_LOGIN_LOG;
import com.pood.server.dto.log.dto_log_user_join;
import com.pood.server.dto.user.dto_user_1;
import com.pood.server.object.user.vo_user_login;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class login_1 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(login_1.class);

    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("userJoinService")
    userJoinService userJoinService;

    @Autowired
    @Qualifier("userLoginService")
    userLoginService userLoginService;

    @Autowired
    @Qualifier("userTokenService")
    userTokenService userTokenService;

    @PostMapping(value = "/pood/user/login/1")
    public HashMap<String, Object> USER_SNS_AUTHENTICATION(@RequestBody vo_user_login socialLoginRequest, HttpServletRequest request)
        throws Exception {

        try {

            procInit(request);
            response = getResponse(200);
            logger.info(socialLoginRequest.toString());

            String snsKey = socialLoginRequest.getSns_key();
            dto_log_user_join logUserJoin = userJoinService.getUserJoinInfo(snsKey);
            if (logUserJoin == null) {
                response = getResponse(220);
                logger.info("SNS 키  : " + snsKey + "에 해당하는 회원이 검색되지 않습니다.");
                return response;
            }

            String userUuid = logUserJoin.getUser_uuid();
            Integer loginType = socialLoginRequest.getLogin_type();
            dto_user_1 userInfo = userService.getUser2("user_uuid", userUuid, loginType);

            Long userBaseImageIdx = userInfo.getUser_base_image_idx();

            String userBaseImg = userBaseImageIdx != null ?
                userService.getUserBaseImg(userBaseImageIdx) :
                userService.getUserBaseImg(1L);

            userInfo.setUserBaseImg(userBaseImg);

            final Integer statusCode = userService.validation(userInfo, 2, userService.getUserPassword(userUuid));

            if (statusCode != 200) {
                response = getResponse(statusCode);
                return response;
            }

            // 회원 UUID 및 SNS 키 로그
            logger.info("USER UUID : " + userUuid + ", SNS_KEY : " + snsKey);

            Integer userIdx = userService.getUserIDX(userUuid);

            // 로그인 로그 추가
            userLoginService.insertRecord(userIdx, userUuid, META_USER_LOGIN_LOG.LOG_IN);

            // 로그인 시 토큰 신규 발급
            String token = userTokenService.issueToken(userUuid);
            userInfo.setToken(token);
            response.put("result", userInfo);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        return response;
    }

}