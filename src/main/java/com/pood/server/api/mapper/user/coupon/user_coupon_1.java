package com.pood.server.api.mapper.user.coupon;

import com.pood.server.api.req.header.user.coupon.hUser_coupon_1;
import com.pood.server.api.service.meta.coupon.couponService;
import com.pood.server.api.service.time.timeService;
import com.pood.server.api.service.user.userCouponService;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import com.pood.server.dto.meta.dto_coupon;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_coupon_1 extends PROTOCOL {

    Logger logger = LoggerFactory.getLogger(user_coupon_1.class);

    @Autowired
    @Qualifier("userCouponService")
    userCouponService userCouponService;

    @Autowired
    @Qualifier("couponService")
    couponService couponService;

    @Autowired
    @Qualifier("timeService")
    timeService timeService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/coupon/1", method = RequestMethod.POST)
    public HashMap<String, Object> ADD_USER_COUPON(@RequestBody hUser_coupon_1 header, HttpServletRequest request)
            throws Exception {
         

        try{

            Integer resp_status = procInit(request);
            
            logger.info(header.toString());

            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_COUPON_1){


                

                /********************* 등록하고자 하는 쿠폰 ******************/
                Integer COUPON_IDX = header.getCoupon_idx();
                if(COUPON_IDX == null){
                    // 삽입시 NULL이 들어갈 경우 231 에러
                    response = getResponse(231);
                    procClose(request, (String)response.get("msg"));
                    return response;
                }


                Integer USER_IDX = userService.getUserIDX(header.getUser_uuid());
                Integer OVER_TYPE = null;

                String CURRENT_TIME = timeService.getCurrentTime();
                String AVAILABLE_TIME = null;

                Boolean isExpired = false;





                /**************** 해당 쿠폰에 대해서 발급타입 등의 제약 조건 조회 ************/
                Integer ISSUED_TYPE = 0;        // 0:중복발행 안됨, 1: 중복발급
                Integer ISSUED_COUNT = 0;       // 쿠폰 수량

                dto_coupon coupon = couponService.getCoupon(COUPON_IDX);

                AVAILABLE_TIME  = coupon.getAvailable_time();
                ISSUED_TYPE     = coupon.getIssued_type();
                ISSUED_COUNT    = coupon.getIssued_count();
                OVER_TYPE       = coupon.getOver_type();

                isExpired = timeService.isExpired(AVAILABLE_TIME);


                if(isExpired){
                    response = getResponse(209);
                    procClose(request, response.toString());
                    return response;
                }

                Boolean isIssued = false;




                // 회원이 해당 쿠폰으로 사전에 발급을 받은 기록이 있는지 조회
                Integer USER_ISSUED_COUNT = userCouponService.getIssuedCount(COUPON_IDX, USER_IDX);

                if(ISSUED_TYPE == 0)
                    if (USER_ISSUED_COUNT >= 1) {
                        isIssued = true;
                    }

                if(isIssued){
                    response = getResponse(208);
                    procClose(request, response.toString());
                    return response;

                }

                if(ISSUED_TYPE != 0){

                    /**************** 중복 쿠폰에 해당되는 경우, 발급 수량을 초과하는지 조회 ************/
                    if((ISSUED_COUNT - USER_ISSUED_COUNT) <= 0)  {
                        response = getResponse(210);
                        procClose(request, response.toString());
                        return response;
                    }
                }

                // 회원 쿠폰 삽입
                Integer CART_IDX = null;
                userCouponService.insertRecord(
                    COUPON_IDX,
                    USER_IDX,
                    CART_IDX,
                    com.pood.server.config.meta.coupon.COUPON_STATUS.AVAILABLE,
                    OVER_TYPE,
                    CURRENT_TIME,
                    AVAILABLE_TIME);

            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        

        
        
        return response;
    }
}