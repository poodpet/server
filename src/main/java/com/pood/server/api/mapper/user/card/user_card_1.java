package com.pood.server.api.mapper.user.card;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.user.userCardService;
import com.pood.server.config.*;
import com.pood.server.object.user.vo_user_card;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_card_1 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_card_1.class);
 
    @Autowired
    @Qualifier("userCardService")
    userCardService userCardService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/simple-card/1", method = RequestMethod.POST)
    public HashMap<String, Object> USER_SIGNUP_CARD(@RequestBody vo_user_card header, HttpServletRequest request)
            throws Exception {
        

        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            logger.info("간편결제 카드 등록 : "+header.toString());

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_CARD_1){

                Integer user_idx = userService.getUserIDX(header.getUser_uuid());

                Integer RESULT_IDX = userCardService.insertRecord(user_idx, 
                        header.getBilling_key(), 
                        header.getCard_name(), 
                        header.getCard_number(), 
                        header.getCard_user(), 
                        header.getMain_card());

                if(RESULT_IDX == -1){
                    response = getResponse(231);
                    procClose(request, (String)response.get("msg"));
                    return response;
                }
            }else response = getResponse(219);
    

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        
        
        
        return response;
    }
}