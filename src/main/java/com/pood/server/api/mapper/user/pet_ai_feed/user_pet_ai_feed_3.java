package com.pood.server.api.mapper.user.pet_ai_feed;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.admin.hAM_delete;
import com.pood.server.config.*;

import com.pood.server.api.service.user.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_pet_ai_feed_3 extends PROTOCOL{

    @Autowired
    @Qualifier("userPetAIFeedService")
    userPetAIFeedService userPetAIFeedService;
 
    @ResponseBody
    @RequestMapping(value = "/pood/user/pet-ai-feed/3", method = RequestMethod.POST)
    public HashMap<String, Object> DELETE_USER_PET_AI_FEED(@RequestBody hAM_delete header, HttpServletRequest request)
            throws Exception {
        
         
        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            if(header.getIdx() == null){
                response = getResponse(300);
                procClose(request, response.toString());
                return response;
            }
    
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_PET_AI_FEED_3){
                for(Integer e:header.getIdx())
                    userPetAIFeedService.deleteRecord(e);
                    
            }else response = getResponse(219);
            
            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        

        return response;
    }
}