package com.pood.server.api.mapper.user.basket;
 
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.user.basket.hUser_basket_3;
import com.pood.server.config.*;
import com.pood.server.api.service.user.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_basket_3 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_basket_3.class);

    @Autowired
    @Qualifier("userCouponService")
    userCouponService userCouponService;

    @Autowired
    @Qualifier("userBasketService")
    userBasketService userBasketService;
 
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/basket/3", method = RequestMethod.POST)
    public HashMap<String, Object> DELETE_USER_BASKET(@RequestBody hUser_basket_3 header, HttpServletRequest request)
            throws Exception {
             

        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(header.getIdx() == null){
                response = getResponse(300);
                procClose(request, response.toString());
                return response;
            }
    
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_BASKET_3){
            
                // 회원 장바구니 삭제
                logger.info("삭제되는 장바구니 항목 번호 : ");

                for(Integer e:header.getIdx()){
                    logger.info("[" + e.toString() + "]");

                    userBasketService.deleteUserBasket(e);
        
                    Integer USER_COUPON_IDX = userCouponService.getUserCouponIndex(e);

                    // 쿠폰 매핑 정보 돌려 놓음
                    if(USER_COUPON_IDX != -1) 
                        userCouponService.rollbackUserCoupon(USER_COUPON_IDX);
                    
                }
            }else response = getResponse(219);
                
            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        return response;
    }
}