package com.pood.server.api.mapper.user;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.user.*;
import com.pood.server.config.*;
import com.pood.server.object.user.vo_user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_4_1 extends PROTOCOL{
  

    @Autowired
    @Qualifier("userService")
    userService userService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/4/1", method = RequestMethod.GET)
    public HashMap<String, Object> USER_DEACTIVATION(
            @RequestParam(value="user_uuid", required=false) String user_uuid,  
            HttpServletRequest request)
            throws Exception {
         

        try{
            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_4_1){     
                
                vo_user USER = userService.getUser("user_uuid", user_uuid);
                
                if(USER.getIdx() == null){
                    // 없는 회원 UUID인 경우 205에러
                    response = getResponse(205);
                    return response;
                }

                // 이미 탈퇴처리된 계정인 경우 205에러
                if(USER.getUser_status() == com.pood.server.config.meta.user.USER_STATUS.DEACTIVE){
                    response = getResponse(229);
                    return response;
                }

                userService.updateUserStatus(user_uuid, com.pood.server.config.meta.user.USER_STATUS.DEACTIVE);

                USER = null;

            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        return response;
    }
}
