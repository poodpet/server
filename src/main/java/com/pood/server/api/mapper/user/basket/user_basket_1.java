package com.pood.server.api.mapper.user.basket;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.config.meta.user.USER_BASKET_STATUS;
import com.pood.server.api.req.header.user.basket.hUser_basket_1;
import com.pood.server.api.service.user.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_basket_1 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_basket_1.class);
    
    @Autowired
    @Qualifier("userCouponService")
    userCouponService userCouponService;

    @Autowired
    @Qualifier("userBasketService")
    userBasketService userBasketService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/basket/1", method = RequestMethod.POST)
    public HashMap<String, Object> ADD_USER_BASKET(@RequestBody hUser_basket_1 header, HttpServletRequest request)
            throws Exception {
      

        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }


            logger.info(header.toString());
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_BASKET_1){

                String  USER_UUID           = header.getUser_uuid();
                Integer USER_IDX            = userService.getUserIDX(header.getUser_uuid());
                Integer USER_COUPON_IDX     = header.getUser_coupon_idx();
                Integer GOODS_IDX           = header.getGoods_idx();
                Integer PR_CODE_IDX         = header.getPr_code_idx();
                Integer QTY                 = header.getQty();
                Integer GOODS_PRICE         = header.getGoods_price();
                String REGULAR_DATE         = header.getRegular_date();
                Integer STATUS              = header.getStatus();
                
                Integer inBasket = userBasketService.checkIsInBasket(USER_UUID, GOODS_IDX, QTY);

                if(STATUS == null)
                    STATUS = USER_BASKET_STATUS.DEFAULT;

                if(STATUS == USER_BASKET_STATUS.BUY_NOW)
                    response.put("basket_idx", inBasket);
                    
                if(inBasket == -1){        
                    Integer BASKET_IDX = userBasketService.insertUserBasket(USER_UUID, USER_IDX, GOODS_IDX, PR_CODE_IDX, QTY, REGULAR_DATE, GOODS_PRICE, STATUS);
                    if(BASKET_IDX == -1){
                        // 삽입시 NULL이 들어갈 경우 231 에러
                        response = getResponse(231);
                        procClose(request, (String)response.get("msg"));
                        return response;
                    }
                    userCouponService.toUserCoupon(BASKET_IDX, USER_COUPON_IDX);
                    response.put("basket_idx", BASKET_IDX);
                }

                USER_UUID           = null;
                USER_IDX            = null;
                USER_COUPON_IDX     = null;
                GOODS_IDX           = null;
                PR_CODE_IDX         = null;
                QTY                 = null;
                GOODS_PRICE         = null;
                REGULAR_DATE        = null;
                STATUS              = null;

            }else response = getResponse(219);
            
            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        
        return response;
    }
}