package com.pood.server.api.mapper.user.point;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.log.*;
import com.pood.server.config.*;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.log.dto_log_user_use_point;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_point_2_2 extends PROTOCOL {
     
    @Autowired
    @Qualifier("logUserUsePointService")
    logUserUsePointService logUserUsePointService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/point/2/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_USER_USE_POINT_LIST(
        @RequestParam(value="page_size", required=false) Integer page_size, 
        @RequestParam(value="page_number", required=false) Integer page_number,
        @RequestParam(value="recordbirth", required=false) String recordbirth,
        @RequestParam(value="user_uuid") String user_uuid,
        HttpServletRequest request) throws Exception {
    

        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;    
            }

            Integer headerCheck = headerService.checkPagingSet(page_size, page_number, recordbirth);

            if(headerCheck != 200){
                response = getResponse(headerCheck);
                procClose(request, response.toString());
                return response;
            }
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_POINT_2_2){

                pagingSet PAGING_SET = new pagingSet(
                    page_size, 
                    page_number, 
                    recordbirth,
                    logUserUsePointService.getTotalRecordNumber(user_uuid));
            
                List<dto_log_user_use_point> result = logUserUsePointService.getList(PAGING_SET, user_uuid);
                pagingResult(PAGING_SET); 
                response.put("result", result);

                result = null;
                
            }else response = getResponse(219);

            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        
        return response;
    }
}  