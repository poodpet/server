package com.pood.server.api.mapper.admin.image;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.object.pagingSet;
import com.pood.server.object.meta.vo_pood_image;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class image_2 extends PROTOCOL {

    
    @ResponseBody
    @RequestMapping(value = "/pood/admin/pood-image/2", method = RequestMethod.GET)
    public HashMap<String, Object> IMAGE_LIST(
        @RequestParam(value="idx", required=false) Integer idx, 
        @RequestParam(value="page_size", required=false) Integer page_size, 
        @RequestParam(value="page_number", required=false) Integer page_number, 
        @RequestParam(value="recordbirth", required=false) String recordbirth,
        HttpServletRequest request) throws Exception {
    

        try{
                
            Integer resp_status = procInit(request);

            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            Integer headerCheck = headerService.checkPagingSet(page_size, page_number, recordbirth);

            if(headerCheck != 200){
                response = getResponse(headerCheck);
                procClose(request, response.toString());
                return response;
            }
    
            if(PROTOCOL_IDENTIFIER.PROTOCOL_ADMIN_POOD_IMAGE_2){
                pagingSet PAGING_SET = new pagingSet(
                    page_size, 
                    page_number, 
                    recordbirth,
                    imageService.getTotalPoodImageRecordNumber(idx));
                
                List<vo_pood_image> result = imageService.getPoodImageList(PAGING_SET, idx);
                response.put("result", result);
                pagingResult(PAGING_SET); 

                result = null;
            }else response = getResponse(219);


            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        
        return response;
    }
 
}  