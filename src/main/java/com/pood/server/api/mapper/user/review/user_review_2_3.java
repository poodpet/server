package com.pood.server.api.mapper.user.review;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.object.resp.resp_product_rating;
import com.pood.server.api.service.user.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController 
public class user_review_2_3 extends PROTOCOL{


    @Autowired
    @Qualifier("userReviewService")
    userReviewService userReviewService;
    


    @ResponseBody
    @RequestMapping(value = "/pood/user/review/2/3", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_USER_REVIEW(
        @RequestParam("product_idx") Integer product_idx,
        HttpServletRequest request)
            throws Exception {
    
        try{

            procInit(request);
            
            response = getResponse(200);
                
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_REVIEW_2_3){        
                resp_product_rating result = userReviewService.getProductRating(product_idx);
                response.put("result", result);
                result = null;
            }else response = getResponse(219);

            procClose(request, null);

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        

        return response;
    }

   
}