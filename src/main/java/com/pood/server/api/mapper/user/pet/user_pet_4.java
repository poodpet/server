package com.pood.server.api.mapper.user.pet;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.user.pet.hUser_pet_1_1;
import com.pood.server.api.req.header.user.pet.hUser_pet_4;
import com.pood.server.api.req.header.user.pet.hUser_pet_image;
import com.pood.server.api.service.user.*;
import com.pood.server.config.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_pet_4 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_pet_4.class);

    @Autowired
    @Qualifier("userPetService")
    userPetService userPetService;

    @Autowired
    @Qualifier("userPetAIService")
    userPetAIService userPetAIService;

    @Autowired
    @Qualifier("userPetAllergyService")
    userPetAllergyService userPetAllergyService;

    @Autowired
    @Qualifier("userPetFeedService")
    userPetFeedService userPetFeedService;

    @Autowired
    @Qualifier("userPetGrainSizeService")
    userPetGrainSizeService userPetGrainSizeService;

    @Autowired
    @Qualifier("userPetSickService")
    userPetSickService userPetSickService;
 
    @ResponseBody
    @RequestMapping(value = "/pood/user/pet/4", method = RequestMethod.POST)
    public HashMap<String, Object> UPDATE_USER_PET(@RequestBody hUser_pet_4 header, HttpServletRequest request)
            throws Exception {


        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_PET_4){

                logger.info(header.toString());
                    
                Integer USER_PET_IDX = header.getIdx();

                userPetService.updateUserPet(
                    USER_PET_IDX, 
                    header.getPet_activity(), 
                    header.getPet_name(), 
                    header.getPet_birth(), 
                    header.getPet_gender(), 
                    header.getPet_weight(), 
                    header.getPet_status(),
                    header.getPc_id(),
                    header.getPsc_id());
            
                if(header.getImage() != null){
                    for(hUser_pet_image e: header.getImage()){
                        Integer IMAGE_IDX       = e.getImage_idx();
                        userPetService.updateUserPetImage(USER_PET_IDX, IMAGE_IDX);
                    }   
                }

                if(header.getAi_diagnosis() != null){
                    userPetAIService.deleteRecordWithUserPetIDX(USER_PET_IDX);

                    for(hUser_pet_1_1 e : header.getAi_diagnosis())
                        userPetAIService.insertRecord(USER_PET_IDX, e.getIdx());
                }

                if(header.getAllergy() != null){
                    userPetAllergyService.deleteRecordWithUserPetIDX(USER_PET_IDX);

                    for(hUser_pet_1_1 e : header.getAllergy()){
                        if(e.getIdx() == -1) continue;
                        userPetAllergyService.insertRecord(USER_PET_IDX, e.getIdx());
                    }

                }

                if(header.getGrain_size() != null){
                    userPetGrainSizeService.deleteRecordWithUserPetIDX(USER_PET_IDX);

                    for(hUser_pet_1_1 e : header.getGrain_size())
                    userPetGrainSizeService.insertRecord(USER_PET_IDX, e.getIdx());
                }


                if(header.getSick_idx() != null){
                    userPetSickService.deleteRecordWithUserPetIDX(USER_PET_IDX);

                    for(hUser_pet_1_1 e : header.getSick_idx()){
                        userPetSickService.insertRecord(USER_PET_IDX, e.getIdx());
                    }
                }

                if(header.getOther_feed_idx() != null){
                    userPetFeedService.deleteRecordWithUserPetIDX(USER_PET_IDX);
                    userPetSickService.insertRecord(USER_PET_IDX, header.getOther_feed_idx());
                }


            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        return response;
    }
}
