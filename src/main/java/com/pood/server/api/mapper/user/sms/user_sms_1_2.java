package com.pood.server.api.mapper.user.sms;

import static com.pood.server.exception.ErrorMessage.SERVER;

import com.pood.server.api.req.header.user.user_info.hUser_info_1;
import com.pood.server.api.service.json.jsonService;
import com.pood.server.api.service.time.timeService;
import com.pood.server.api.service.user.userSMSAuthService;
import com.pood.server.config.AES256;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import com.pood.server.exception.CustomStatusException;
import com.pood.server.object.api.API_RESPONSE;
import com.pood.server.object.user.vo_user;
import java.time.LocalDateTime;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import lombok.Builder;
import lombok.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class user_sms_1_2 extends PROTOCOL {

    @org.springframework.beans.factory.annotation.Value("${legacy.server-ip}")
    private String serverIp;

    Logger logger = LoggerFactory.getLogger(user_sms_1_2.class);



    @Autowired
    @Qualifier("userSMSAuthService")
    userSMSAuthService userSMSAuthService;

    @Autowired
    @Qualifier("timeService")
    timeService timeService;

    @Autowired
    @Qualifier("jsonService")
    jsonService jsonService;

    @Deprecated
    @ResponseBody
    @RequestMapping(value = "/pood/user/sms/1/2", method = RequestMethod.GET)
    public HashMap<String, Object> SEND_MESSAGE(HttpServletRequest request,
            @RequestParam(value = "phone_number", required = false) String PHONE_NUMBER,
            @RequestParam(value = "user_name", required = false)String encoded_name) throws Exception{
         
        try{

            procInit(request);

            response = sendMessageProcess(encoded_name, PHONE_NUMBER, request);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
            throw CustomStatusException.serverError(SERVER);
        }

        return response;
    }


    @ResponseBody
    @RequestMapping(value = "/pood/user/sms/1/2", method = RequestMethod.POST)
    public HashMap<String, Object> SEND_MESSAGE_POST(@Valid @RequestBody hUser_info_1 header, HttpServletRequest request)
            throws Exception {
        try{
            procInit(request);

            String encodedName = header.getUser_name();
            String phoneNumber = header.getPhone_number();

            response = sendMessageProcess(encodedName, phoneNumber, request);

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
            throw CustomStatusException.serverError(SERVER);
        }
        return response;

    }

    public HashMap<String,Object> sendMessageProcess(String encoded_name, String PHONE_NUMBER, HttpServletRequest request) throws Exception {
        response = getResponse(200);


        String USER_NAME = AES256.aesDecode(encoded_name);
        logger.info("DECODED : " + USER_NAME);

        Boolean chkUser = userService.checkUser(USER_NAME, PHONE_NUMBER);

        if(!chkUser){
            response = getResponse(242);
            procClose(request, response.toString());
            return response;
        }

//        PARAMETER : [user_name: q%2Bpfm0p4OBcQ%2F75ry5dVNg%3D%3D][phone_number: 01079139597]
//        PARAMETER : [user_name: q+pfm0p4OBcQ/75ry5dVNg==][phone_number: 01030890122]

        chkUser = false;

        vo_user USER = userService.getUser("user_phone", PHONE_NUMBER);

        // 없는 회원인 경우 241 에러
        if(USER == null){
            response = getResponse(203);
            procClose(request, response.toString());
            return response;
        }

        if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_SMS_1_1){

            Boolean chk = userSMSAuthService.isRecordbirthValid(PHONE_NUMBER);

            if(chk){

                String url = serverIp + "/pood/user/sms/1?phone_number=" + PHONE_NUMBER;
                logger.info("CALL ADMIN SERVER API : "+ url);
                String result = new RestTemplate().getForObject(url, String.class);
                API_RESPONSE resp = (API_RESPONSE)jsonService.getDecodedObject(result, API_RESPONSE.class);
                if(!(resp.getStatus() == 200)){
                    response = getResponse(resp.getStatus());
                }

                response.put("user_uuid", USER.getUser_uuid());

            }else response = getResponse(235);

        }else response = getResponse(219);

        return response;
    }

    @Value
    private static class UserSmsErrorResponse {
        LocalDateTime timestamp;

        String status;

        String error;

        String message;

        String path;

        @Builder
        private UserSmsErrorResponse(final LocalDateTime timestamp, final String status,
            final String error,
            final String message, final String path) {
            this.timestamp = timestamp;
            this.status = status;
            this.error = error;
            this.message = message;
            this.path = path;
        }

        public static UserSmsErrorResponse of(final String status, final String error, final String message, final String path) {
            return UserSmsErrorResponse.builder()
                .timestamp(LocalDateTime.now())
                .status(status)
                .error(error)
                .message(message)
                .path(path)
                .build();
        }

    }

} 
