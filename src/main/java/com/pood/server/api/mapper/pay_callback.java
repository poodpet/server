package com.pood.server.api.mapper;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.pay.hPay_callback;

import com.pood.server.config.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class pay_callback extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(pay_callback.class);
  
    @ResponseBody
    @RequestMapping(value = "/pood/iamport/pay-callback/1", method = RequestMethod.POST)
    public HashMap<String, Object> ADD_USER(@RequestBody hPay_callback header, HttpServletRequest request)
            throws Exception {
        
        try{

            procInit(request);

            response = getResponse(200);

            logger.info(header.toString());

            if(PROTOCOL_IDENTIFIER.PROTOCOL_IAMPORT_PAY_CALLBACK){        


                String MERCHANT_UID     =   header.getMerchant_uid();

                String STATUS           =   header.getStatus();

                if(STATUS.equals("cancelled"))
                    retreieveService.CANCEL_CALLBACK(MERCHANT_UID,     "대시보드 취소 CALLBACK");       

                STATUS          = null;

                MERCHANT_UID    = null;


            }else response = getResponse(219);

            

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        procClose(request, response.toString());
        

        
        return response;
    }
}