package com.pood.server.api.mapper;


import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.log.*;
import com.pood.server.api.service.meta.goods.*;
import com.pood.server.api.service.user.*;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import com.pood.server.config.meta.META_LOG_PRODUCT_HISTORY;
import com.pood.server.dto.log.dto_log_user_point;
import com.pood.server.dto.log.user_order.dto_log_user_order;
import com.pood.server.dto.user.dto_user_coupon_2;
import com.pood.server.dto.user.dto_user_point;
import com.pood.server.api.service.delivery.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

 
@RestController
public class test100 extends PROTOCOL{

  
   
    @Autowired
    @Qualifier("userService")
    userService userService;
    
    @Autowired
    @Qualifier("userBasketService")
    userBasketService userBasketService;

    @Autowired
    @Qualifier("userCouponService")
    userCouponService userCouponService;

    @Autowired
    @Qualifier("userDeliveryService")
    userDeliveryService userDeliveryService;

    @Autowired
    @Qualifier("userNotiService")
    userNotiService userNotiService;

    @Autowired
    @Qualifier("userCardService")
    userCardService userCardService;

    @Autowired
    @Qualifier("userWishService")
    userWishService userWishService;

    @Autowired
    @Qualifier("userReferralCodeService")
    userReferralCodeService userReferralCodeService;

    @Autowired
    @Qualifier("userPointService")
    userPointService userPointService;

    @Autowired
    @Qualifier("userPetService")
    userPetService userPetService;

    @Autowired
    @Qualifier("userPetAIService")
    userPetAIService userPetAIService;

    @Autowired
    @Qualifier("userPetAllergyService")
    userPetAllergyService userPetAllergyService;

    @Autowired
    @Qualifier("userPetFeedService")
    userPetFeedService userPetFeedService;

    @Autowired
    @Qualifier("userPetGrainSizeService")
    userPetGrainSizeService userPetGrainSizeService;

    @Autowired
    @Qualifier("userPetSickService")
    userPetSickService userPetSickService;

    @Autowired
    @Qualifier("userPetAIFeedService")
    userPetAIFeedService userPetAIFeedService;

    @Autowired
    @Qualifier("logUserSavePointService")
    logUserSavePointService logUserSavePointService;

    @Autowired
    @Qualifier("logUserUsePointService")
    logUserUsePointService logUserUsePointService;

    @Autowired
    @Qualifier("logUserPointService")
    logUserPointService logUserPointService;
    
    @Autowired
    @Qualifier("logUserOrderService")
    logUserOrderService logUserOrderService;

    @Autowired
    @Qualifier("remoteDeliveryService")
    remoteDeliveryService remoteDeliveryService;

    @Autowired
    @Qualifier("goodsProductService")
    goodsProductService goodsProductService;

    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;

    @Autowired
    @Qualifier("userJoinService")
    userJoinService userJoinService;

    @Autowired
    @Qualifier("userLoginService")
    userLoginService userLoginService;




    // 적립금 전부 초기화
    @ResponseBody
    @RequestMapping(value = "/test100/", method = RequestMethod.GET)
    public HashMap<String, Object> INIT_USER_POINT(
        @RequestParam(value="user_uuid", required=false) String user_uuid, 
        HttpServletRequest request) throws Exception {
 
        if(PROTOCOL_IDENTIFIER.PROTOCOL_TEST_100){
            userPointService.init(user_uuid);
            logUserSavePointService.init(user_uuid);
            logUserUsePointService.init(user_uuid);
            logUserPointService.init(user_uuid);
            userService.userPointInit(user_uuid);
        }else response = getResponse(219);

        return response;
    } 





    // 적립금 리스트 호출
    @ResponseBody
    @RequestMapping(value = "/test101/", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_USER_POINT(
        @RequestParam(value="user_uuid", required=false) String user_uuid, 
        HttpServletRequest request) throws Exception {

 
        if(PROTOCOL_IDENTIFIER.PROTOCOL_TEST_101){
            Integer user_point = userService.getUserPoint(user_uuid);
            List<dto_user_point> list = userPointService.getListWithUserUUID(user_uuid);
            List<dto_log_user_point> list2 = logUserPointService.getList(user_uuid);
            response.put("user_point", user_point);
            response.put("result", list);
            response.put("history", list2);
            list = null;
        }else response = getResponse(219);

        
        return response;
    } 




    // 주문 내역 호출
    @ResponseBody
    @RequestMapping(value = "/test102/", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_ORDER_HISTORY(
        @RequestParam(value="order_number", required=false) String order_number,
        HttpServletRequest request) throws Exception {
        
        if(PROTOCOL_IDENTIFIER.PROTOCOL_TEST_102){
            List<dto_log_user_order> list = logUserOrderService.getOrderHistoryList2(order_number);
            response.put("result", list);
            list = null;
        }else response = getResponse(219);
        
        return response;
    } 




  
    // 회원 쿠폰 발급 
    @ResponseBody
    @RequestMapping(value = "/test103/", method = RequestMethod.GET)
    public HashMap<String, Object> ISSUE_COUPON(@RequestParam(value="user_idx", required=false) Integer USER_IDX, HttpServletRequest request) throws Exception {
 
        if(PROTOCOL_IDENTIFIER.PROTOCOL_TEST_103)
            userCouponService.issueWelcomeCoupon(USER_IDX);
        else response = getResponse(219);
        
        return response;
    } 




   
    // 회원 쿠폰 소멸
    @ResponseBody
    @RequestMapping(value = "/test104/", method = RequestMethod.GET)
    public HashMap<String, Object> DELETE_COUPON(@RequestParam(value="user_idx", required=false) Integer USER_IDX, HttpServletRequest request) throws Exception {
 
        if(PROTOCOL_IDENTIFIER.PROTOCOL_TEST_104)
            userCouponService.remove(USER_IDX);
        else response = getResponse(219);
        
        return response;
    } 





    // 회원 쿠폰 목록
    @ResponseBody
    @RequestMapping(value = "/test105/", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_USER_COUPON(@RequestParam(value="user_idx", required=false) Integer USER_IDX, HttpServletRequest request) throws Exception {
 
        if(PROTOCOL_IDENTIFIER.PROTOCOL_TEST_105){
            List<dto_user_coupon_2> list = userCouponService.getCouponList(USER_IDX);
            response.put("result", list);
        }else response = getResponse(219);

        return response;
    } 






    // 배송금액 테스트
    @ResponseBody
    @RequestMapping(value = "/test106/", method = RequestMethod.GET)
    public HashMap<String, Object> DELIVERY_FEE(
        @RequestParam(value="user_uuid", required=false) String USER_UUID,
        @RequestParam(value="remote_type", required=false) Integer REMOTE_TYPE,
        @RequestParam(value="delivery_fee", required=false) Integer DELIVERY_FEE,
        @RequestParam(value="order_price", required=false) Integer ORDER_PRICE,
        @RequestParam(value="zipcode", required=false) String ZIPCODE, HttpServletRequest request) throws Exception {
 
        if(PROTOCOL_IDENTIFIER.PROTOCOL_TEST_106)
            remoteDeliveryService.checkDeliveryFee(REMOTE_TYPE, DELIVERY_FEE, ORDER_PRICE, ZIPCODE, USER_UUID);
        else response = getResponse(219);

        return response;
    } 






     // 굿즈 수량 증가 테스트
     @ResponseBody
     @RequestMapping(value = "/test107/", method = RequestMethod.GET)
     public HashMap<String, Object> ADD_GOODS_QTY(
         @RequestParam(value="goods_idx", required=false) Integer GOODS_IDX,
         @RequestParam(value="purchase_qty", required=false) Integer PURCHASE_QUANTITY,
         @RequestParam(value="text", required=false) String TEXT, HttpServletRequest request) throws Exception {
 
        if(PROTOCOL_IDENTIFIER.PROTOCOL_TEST_107)
            goodsProductService.addQtyWithGoodsIDX(GOODS_IDX, META_LOG_PRODUCT_HISTORY.TYPE_PAY_RETRIEVE, PURCHASE_QUANTITY, TEXT);
        else response = getResponse(219);
        
        return response;
     } 






     // 굿즈 수량 차감 테스트
     @ResponseBody
     @RequestMapping(value = "/test108/", method = RequestMethod.GET)
     public HashMap<String, Object> DEDUCT_GOODS_QTY(
        @RequestParam(value="goods_idx", required=false) Integer GOODS_IDX,
        @RequestParam(value="purchase_qty", required=false) Integer PURCHASE_QUANTITY,
        @RequestParam(value="text", required=false) String TEXT, HttpServletRequest request) throws Exception {
 
        if(PROTOCOL_IDENTIFIER.PROTOCOL_TEST_108)
            goodsProductService.deductQtyWithGoodsIDX(GOODS_IDX, PURCHASE_QUANTITY, META_LOG_PRODUCT_HISTORY.TYPE_PAY_SUCCESS, TEXT);
        else response = getResponse(219);
        
        return response;
     } 







    // 회원 데이터 완전히 삭제
    @ResponseBody
    @RequestMapping(value = "/test109/", method = RequestMethod.GET)
    public HashMap<String, Object> INIT_USER(@RequestParam(value="user_uuid", required=false) String USER_UUID, HttpServletRequest request) throws Exception {
 
        if(PROTOCOL_IDENTIFIER.PROTOCOL_TEST_109){
            
            userService.userInit(USER_UUID);
            
            userService.deleteRecord(USER_UUID);


            // 회원 포인트 지급 내역 삭제
            logUserSavePointService.init(USER_UUID);
            

            // 회원 포인트 지급 내역 삭제
            logUserUsePointService.init(USER_UUID);
            

            // 회원 포인트 지급 내역 삭제
            logUserPointService.init(USER_UUID);

        
            // 로그인 로그 삭제
            userLoginService.deleteRecord(USER_UUID);

            
            // 가입 로그 삭제
            userJoinService.deleteRecord(USER_UUID);


        }else response = getResponse(219);
        
        return response;
    } 

}     