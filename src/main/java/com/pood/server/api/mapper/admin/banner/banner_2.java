package com.pood.server.api.mapper.admin.banner;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.meta.banner.*;
import com.pood.server.config.*;
import com.pood.server.dto.meta.dto_banner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class banner_2 extends PROTOCOL {
 

    @Autowired
    @Qualifier("bannerService")
    bannerService bannerService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/admin/banner/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_BANNER(
        @RequestParam(value="pc_id", required=false) Integer pc_id, 
        @RequestParam(value="page_size", required=false) Integer page_size, 
        @RequestParam(value="page_number", required=false) Integer page_number,
        @RequestParam(value="recordbirth", required=false) String recordbirth,
        HttpServletRequest request) throws Exception {
    
        try{

            response = getResponse(200);

            if(PROTOCOL_IDENTIFIER.PROTOCOL_ADMIN_BANNER_2){
 
                List<dto_banner> result = bannerService.getList(pc_id);
                response.put("result", result);
                result = null;
                
            }else response = getResponse(219);

            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
        
        return response;
    }
}  