package com.pood.server.api.mapper.user.review;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.req.header.user.review.hUser_review;
import com.pood.server.api.req.header.user.review.hUser_review_1;
import com.pood.server.api.service.log.*;
import com.pood.server.api.service.meta.order.*;
import com.pood.server.api.service.pood_point.*;
import com.pood.server.api.service.user.userReviewService;
import com.pood.server.api.service.meta.goods.*;
import com.pood.server.config.*;
import com.pood.server.config.meta.point.META_POINT_TYPE;
import com.pood.server.config.meta.point.POINT_SAVE_STATUS;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController 
public class user_review_1 extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(user_review_1.class);

    @Autowired
    @Qualifier("orderService")
    OrderService orderService;

    @Autowired
    @Qualifier("userReviewService")
    userReviewService userReviewService;

    @Autowired
    @Qualifier("goodsService")
    goodsService goodsService;

    @Autowired
    @Qualifier("logUserSavePointService")
    logUserSavePointService logUserSavePointService;

    @Autowired
    @Qualifier("poodPointService")
    poodPointService poodPointService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/review/1", method = RequestMethod.POST)
    public HashMap<String, Object> ADD_USER_REVIEW(@RequestBody hUser_review_1 header, HttpServletRequest request)
            throws Exception {
        

        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }

            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_REVIEW_1){        

                logger.info(header.toString());

                String  USER_UUID       =   header.getUser_uuid();
                Integer USER_IDX        =   userService.getUserIDX(USER_UUID);

                String  ORDER_NUMBER    =   header.getOrder_number();
                Integer PET_IDX         =   header.getPet_idx();
                Integer GOODS_IDX       =   header.getgoods_idx();
                Integer RATING          =   header.getRating();
                String  REVIEW_TEXT     =   header.getReview_text();
                Integer PRODUCT_IDX     =   header.getProduct_idx();
                String  GOODS_NAME      =   goodsService.getgoodsName(GOODS_IDX);
                

                Integer REVIEW_IDX = userReviewService.InsertUserReview(USER_IDX, PET_IDX, GOODS_IDX, RATING, REVIEW_TEXT, ORDER_NUMBER, PRODUCT_IDX, GOODS_NAME);

                Integer IMAGE_SEQUENCE = 1;

                if(REVIEW_IDX == -1){
                    // 삽입시 NULL이 들어갈 경우 231 에러
                    response = getResponse(231);
                    return response;
                }
                
                if(!header.getImage().isEmpty()){
                    for(hUser_review e:header.getImage()){
                        logger.info(e.toString());
                        userReviewService.updateUserReviewImage(REVIEW_IDX, ORDER_NUMBER, IMAGE_SEQUENCE++, e.getImage_idx());
                    }
                }

                Integer REVIEW_CNT = userReviewService.getUserReviewCount(USER_IDX);

                if (header.getImage().isEmpty()) {
                    // 텍스트 리뷰 시 포인트 지급
                    List<String> process_0 = poodPointService.issuePoint(null, REVIEW_IDX, USER_UUID,
                        META_POINT_TYPE.TEXT_REVIEW, POINT_SAVE_STATUS.SAVE_COMPLETE);
                    updateService.commit(process_0);
                    process_0 = null;

                } else {
                    // 이미지 리뷰 시 포인트 지급
                    List<String> process_0 = poodPointService.issuePoint(null, REVIEW_IDX, USER_UUID,
                        META_POINT_TYPE.PHOTO_REVIEW, POINT_SAVE_STATUS.SAVE_COMPLETE);
                    updateService.commit(process_0);
                    process_0 = null;
                }
                
                ORDER_NUMBER    = null;
    
                PET_IDX         = null;

                GOODS_IDX       = null;

                RATING          = null;

                REVIEW_TEXT     = null;

                PRODUCT_IDX     = null;

                GOODS_NAME      = null;

                USER_UUID       = null;

                REVIEW_CNT      = null;
                
            }else response = getResponse(219);


            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }
 
        
        return response;
    }





    
}