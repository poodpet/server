package com.pood.server.api.mapper.user.wish;

import com.pood.server.api.req.header.user.wish.hUser_wish_1;
import com.pood.server.api.service.user.userWishService;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_wish_2 extends PROTOCOL{
   
    @Autowired
    @Qualifier("userWishService")
    userWishService userWishService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/user/wish/2", method = RequestMethod.POST)
    public HashMap<String, Object> WISH_CHECK(@RequestBody hUser_wish_1 header, HttpServletRequest request)
            throws Exception {
         

        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_WISH_2){        
                Integer USER_IDX    = userService.getUserIDX(header.getUser_uuid());
                if(USER_IDX == null){
                    response = getResponse(203);
                    return response;
                }
                Integer GOODS_IDX = header.getGoods_idx();
                Boolean isWished = userWishService.getWishCheck(USER_IDX, GOODS_IDX);
                response.put("isWished", isWished);
            }else response = getResponse(219);

            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }


        
        return response;
    }
}