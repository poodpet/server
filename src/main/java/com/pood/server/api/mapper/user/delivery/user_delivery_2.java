package com.pood.server.api.mapper.user.delivery;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.dto.user.dto_user_delivery_address;
import com.pood.server.api.service.user.*;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class user_delivery_2 extends PROTOCOL{
 

    @Autowired
    @Qualifier("userDeliveryService")
    userDeliveryService userDeliveryService;

    @ResponseBody
    @RequestMapping(value = "/pood/user/delivery/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_USER_DELIVERY(@RequestParam("user_uuid") String user_uuid, HttpServletRequest request)
            throws Exception {
    

        try{

            Integer resp_status = procInit(request);
            
            // 인증토큰에 대해서 유효성 검증에 실패한 경우
            if(resp_status != 200){
                procClose(request, response.toString());
                return response;
            }
                    
            if(PROTOCOL_IDENTIFIER.PROTOCOL_USER_DELIVERY_2){
                Integer user_idx = userService.getUserIDX(user_uuid);
                List<dto_user_delivery_address> result = userDeliveryService.getUserDeliveryList(user_idx);
                response.put("result", result);
                result = null;
            }else response = getResponse(219);


            procClose(request, response.toString());

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        

        return response;
    }
}