package com.pood.server.api.mapper.main;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.meta.maintab.*;
import com.pood.server.config.*;
import com.pood.server.object.pagingSet;
import com.pood.server.object.resp.resp_main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class main_2_1 extends PROTOCOL {
   
    @Autowired
    @Qualifier("maintabService")
    maintabService maintabService;
     
    @ResponseBody
    @RequestMapping(value = "/pood/main/2/1", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_MAIN_LIST(
        @RequestParam(value="pc_id",    required=false)Integer pc_id,
        @RequestParam(value="page_size", required=false) Integer page_size, 
        @RequestParam(value="page_number", required=false) Integer page_number, 
        @RequestParam(value="recordbirth", required=false) String recordbirth,
        HttpServletRequest request) throws Exception {


        try{

            procInit(request);

            response = getResponse(200);
                    
            Integer headerCheck = headerService.checkPagingSet(page_size, page_number, recordbirth);

            if(headerCheck != 200){
                response = getResponse(headerCheck);
                procClose(request, response.toString());
                return response;
            } 
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_ADMIN_MAIN_2_1){
                pagingSet PAGING_SET = new pagingSet(
                        page_size, 
                        page_number, 
                        recordbirth,
                        maintabService.getTotalRecordNumber());
                
                List<resp_main> result = maintabService.getMainList(pc_id);
                response.put("result", result);
                pagingResult(PAGING_SET); 
                result = null;
                
            }else response = getResponse(219);

            procClose(request, (String)response.get("msg"));

        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        
        
        return response;
    }
}  