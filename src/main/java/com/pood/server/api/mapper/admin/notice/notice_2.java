package com.pood.server.api.mapper.admin.notice;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.service.meta.notice.*;
import com.pood.server.config.*;
import com.pood.server.object.pagingSet;
import com.pood.server.dto.meta.dto_notice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class notice_2 extends PROTOCOL {
    
    @Autowired
    @Qualifier("noticeService")
    noticeService noticeService;
    
    @ResponseBody
    @RequestMapping(value = "/pood/admin/notice/2", method = RequestMethod.GET)
    public HashMap<String, Object> LIST_PET_DISEASE(
        @RequestParam(value="page_size", required=false) Integer page_size, 
        @RequestParam(value="page_number", required=false) Integer page_number, 
        @RequestParam(value="recordbirth", required=false) String recordbirth,
        HttpServletRequest request) throws Exception {
    
        try{

            procInit(request);

            response = getResponse(200);
            
            Integer headerCheck = headerService.checkPagingSet(page_size, page_number, recordbirth);

            if(headerCheck != 200){
                response = getResponse(headerCheck);
                procClose(request, response.toString());
                return response;
            }
            
            if(PROTOCOL_IDENTIFIER.PROTOCOL_ADMIN_NOTICE_2){
                pagingSet PAGING_SET = new pagingSet(
                        page_size, 
                        page_number, 
                        recordbirth,
                        noticeService.getTotalRecordNumber());
                    
                List<dto_notice> result = noticeService.getList(PAGING_SET);    
                response.put("result", result);        
                pagingResult(PAGING_SET); 

                result = null; 
                
            }else response = getResponse(219);

            procClose(request, (String)response.get("msg"));


        }catch(Exception e){
            thExecption(request.getServletPath(), e);
        }

        
        return response;
    }
 
}  