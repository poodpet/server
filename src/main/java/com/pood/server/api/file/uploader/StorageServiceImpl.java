package com.pood.server.api.file.uploader;

import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.pood.server.config.AWS;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;
import javax.imageio.ImageIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service("storageService")
public class StorageServiceImpl implements StorageService {

    @Value("${aws.s3.url}")
    private String url;

    @Value("${aws.s3.bucket-name}")
    private String bucketName;

    Logger logger = LoggerFactory.getLogger(StorageServiceImpl.class);

    /********** AWS S3 MultipartFile 파일 저장 *********/
    public String multiPartFileStore(MultipartFile mfile, String FOLDER_NAME) throws IOException {
        String FILE_TYPE[] = mfile.getContentType().split("/");
        BufferedImage image = ImageIO.read(mfile.getInputStream());
        String FILE_NAME = store(image, FILE_TYPE[1], FOLDER_NAME);

        return url + FILE_NAME;
    }


    /********** AWS S3 MultipartFile 파일 저장 *********/
    public String store(BufferedImage ORIGINAL_IMAGE, String FILE_TYPE, String FOLDER_NAME)
        throws IOException {

        String CURRENT_DATETIME = "";

        String FILE_NAME = "";
        String ORIGINAL_FILE_NAME = "";
        String FILE_UUID = UUID.randomUUID().toString();

        // 원본 이미지 저장
        CURRENT_DATETIME = new SimpleDateFormat("yyyyMMddHHmmssmmm").format(
            Calendar.getInstance().getTime());
        FILE_NAME = CURRENT_DATETIME + "-" + FILE_UUID + "." + FILE_TYPE;
        ORIGINAL_FILE_NAME = FILE_NAME;
        logger.info("A NEW FILE HAS BEEN CREATED. NAME : " + FILE_NAME);

        Integer IMAGE_TYPE =
            ORIGINAL_IMAGE.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : ORIGINAL_IMAGE.getType();
        Integer origin_width = ORIGINAL_IMAGE.getWidth();
        Integer origin_height = ORIGINAL_IMAGE.getHeight();

        storeObject(FILE_NAME, getByteFromImage(ORIGINAL_IMAGE, FILE_TYPE).toByteArray(),
            FOLDER_NAME);

        // 360 이미지 저장
        FILE_NAME = CURRENT_DATETIME + "-" + FILE_UUID + "_360" + "." + FILE_TYPE;

        imageResize IMAGE_RESIZE = getNewImageProperty(360, 360, origin_width, origin_height);

        BufferedImage NEW_IMAGE =
            getResizedImage(ORIGINAL_IMAGE, IMAGE_RESIZE.getNew_width(),
                IMAGE_RESIZE.getNew_height(), IMAGE_TYPE);

        storeObject(FILE_NAME, getByteFromImage(NEW_IMAGE, FILE_TYPE).toByteArray(), FOLDER_NAME);

        // 720 이미지 저장
        FILE_NAME = CURRENT_DATETIME + "-" + FILE_UUID + "_720" + "." + FILE_TYPE;

        IMAGE_RESIZE = getNewImageProperty(720, 720, origin_width, origin_height);

        BufferedImage NEW_IMAGE2 =
            getResizedImage(ORIGINAL_IMAGE, IMAGE_RESIZE.getNew_width(),
                IMAGE_RESIZE.getNew_height(), IMAGE_TYPE);

        storeObject(FILE_NAME, getByteFromImage(NEW_IMAGE2, FILE_TYPE).toByteArray(), FOLDER_NAME);

        return FOLDER_NAME + "/" + ORIGINAL_FILE_NAME;
    }


    /********* AWS S3 특정 파일 전부 삭제 *********/
    @Override
    public void deleteObject(String file_url) {
        if (file_url != null) {
            String[] separated = file_url.split("\\/");
            String[] separated2 = separated[4].split("\\.");
            String FILE_NAME = separated2[0];
            String FILE_NAME_360 = FILE_NAME + "_360";
            String FILE_NAME_720 = FILE_NAME + "_720";

            String FILE_1 = separated[3] + "/" + FILE_NAME + "." + separated2[1];
            String FILE_2 = separated[3] + "/" + FILE_NAME_360 + "." + separated2[1];
            String FILE_3 = separated[3] + "/" + FILE_NAME_720 + "." + separated2[1];

            AWSCredentials credentials = new BasicAWSCredentials(AWS.accessKey, AWS.secretKey);

            AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.AP_NORTHEAST_2)
                .build();

            try {
                DeleteObjectsRequest delObjReq = new DeleteObjectsRequest(bucketName).withKeys(
                    FILE_1);
                s3Client.deleteObjects(delObjReq);

                DeleteObjectsRequest delObjReq2 = new DeleteObjectsRequest(bucketName).withKeys(
                    FILE_2);
                s3Client.deleteObjects(delObjReq2);

                DeleteObjectsRequest delObjReq3 = new DeleteObjectsRequest(bucketName).withKeys(
                    FILE_3);
                s3Client.deleteObjects(delObjReq3);
            } catch (SdkClientException s) {
                logger.error(s.getMessage());
            }

        }
    }


    /* ************* 새로운 높이와 넓이에 대해서 리사이즈 비율로 계산된 이미지를 반환함 ********************/
    public BufferedImage getResizedImage(BufferedImage img, Integer new_width, Integer new_height,
        Integer image_type) {
        BufferedImage new_image = new BufferedImage(new_width, new_height, image_type);
        Graphics2D g2 = new_image.createGraphics();
        g2.drawImage(img, 0, 0, new_width, new_height, null);
        g2.dispose();

        return new_image;
    }


    /***************** BufferedImage로부터 바이트를 추출함 *****************/
    public ByteArrayOutputStream getByteFromImage(BufferedImage img, String FILE_TYPE)
        throws IOException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        ImageIO.write(img, FILE_TYPE, baos);

        return baos;

    }

    /****************** S3에 실질적으로 파일을 업로드함 ******************************/
    public void storeObject(String FILE_NAME, byte[] bytes, String FOLDER_NAME) {
        AWSCredentials credentials = new BasicAWSCredentials(AWS.accessKey, AWS.secretKey);

        AmazonS3 s3client = AmazonS3ClientBuilder.standard()
            .withCredentials(new AWSStaticCredentialsProvider(credentials))
            .withRegion(Regions.AP_NORTHEAST_2)
            .build();

        ObjectMetadata metaData = new ObjectMetadata();

        metaData.setContentLength(bytes.length);
        logger.info("S3  bytes length : " + bytes.length);
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);

        s3client.putObject(
            new PutObjectRequest(bucketName, FOLDER_NAME + "/" + FILE_NAME, byteArrayInputStream,
                metaData)
                .withCannedAcl(CannedAccessControlList.PublicRead));

    }


    /*********** 새로운 비율로 계산된 넓이, 높이 값을 반환 *********************/
    public imageResize getNewImageProperty(Integer ratio_x, Integer ratio_y, Integer origin_width,
        Integer origin_height) {

        Integer new_width = origin_width;
        Integer new_height = origin_height;
        if (origin_width >= origin_height) {
            new_width = ratio_x;
            new_height = (new_width * origin_height) / origin_width;
        } else {
            new_height = ratio_y;
            new_width = (new_height * origin_width) / origin_height;
        }

        return new imageResize(new_width, new_height);
    }


}