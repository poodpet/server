package com.pood.server.api.file.uploader;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class imageResize {

    private Integer new_width;

    private Integer new_height;

    public Integer getNew_width() {
        return new_width;
    }

    public void setNew_width(Integer new_width) {
        this.new_width = new_width;
    }

    public Integer getNew_height() {
        return new_height;
    }

    public void setNew_height(Integer new_height) {
        this.new_height = new_height;
    }

    @Override
    public String toString() {
        return "imageResize [new_height=" + new_height + ", new_width=" + new_width + "]";
    }

}
