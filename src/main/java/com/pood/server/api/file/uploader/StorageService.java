package com.pood.server.api.file.uploader;

import org.springframework.web.multipart.MultipartFile;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public interface StorageService {

 
	/********** AWS S3 MultipartFile 파일 저장 *********/
	public String multiPartFileStore(MultipartFile file, String FOLDER_NAME) throws IOException;
 


	/********* AWS S3 특정 파일 전부 삭제 *********/
	public void deleteObject(String file_url);
	

	/********* AWS S3 MultipartFile 파일 저장 *********/
	public String store(BufferedImage ORIGINAL_IMAGE, String FILE_TYPE, String FOLDER_NAME) throws IOException;
 


	/******************** S3에 실질적으로 파일을 업로드함 ******************************/
	public void storeObject(String FILE_NAME, byte[] bytes, String FOLDER_NAME);


	/****************** BufferedImage로부터 바이트를 추출함 *****************/
	public ByteArrayOutputStream getByteFromImage(BufferedImage img, String FILE_TYPE) throws IOException;


	/************* 새로운 높이와 넓이에 대해서 리사이즈 비율로 계산된 이미지를 반환함 ********************/
	public BufferedImage getResizedImage(BufferedImage img, Integer new_width, Integer new_height, Integer image_type);



	/************** 새로운 비율로 계산된 넓이, 높이 값을 반환 *********************/
	public imageResize getNewImageProperty(Integer ratio_x, Integer ratio_y, Integer origin_x, Integer origin_y);
}