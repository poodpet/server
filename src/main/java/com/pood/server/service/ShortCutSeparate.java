package com.pood.server.service;

import com.pood.server.controller.response.home.icon.ShortCutIconResponse;
import com.pood.server.entity.meta.ShortcutIcon;
import com.pood.server.repository.meta.ShortcutIconRepository;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("metaTransactionManager")
public class ShortCutSeparate {

    private static final int COMMON = 0;

    private final ShortcutIconRepository shortcutIconRepository;

    public List<ShortCutIconResponse> findAll(final int pcIdx) {
        final List<ShortcutIcon> iconList = shortcutIconRepository.findAllByPetCategoryIdxInOrderByPriority(List.of(COMMON, pcIdx));

        List<ShortCutIconResponse> list = new ArrayList<>();
        for (ShortcutIcon shortcutIcon : iconList) {
            list.add(ShortCutIconResponse.of(shortcutIcon));
        }
        return list;
    }

}
