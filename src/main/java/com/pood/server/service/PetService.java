package com.pood.server.service;

import com.pood.server.entity.meta.Pet;
import com.pood.server.repository.meta.PetRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional("metaTransactionManager")
@RequiredArgsConstructor
public class PetService {

    private final PetRepository petRepository;

    public List<Pet> allPetInfo() {
        return petRepository.findAllOrderByPcKindDesc();
    }

}
