package com.pood.server.service;

import com.pood.server.entity.meta.EventDonation;
import com.pood.server.entity.meta.mapper.event.EventDonationThumbnailMapper;
import com.pood.server.exception.AlreadyExistException;
import com.pood.server.repository.meta.EventDonationRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional("metaTransactionManager")
@RequiredArgsConstructor
public class EventDonationSeparate {

    private final EventDonationRepository repository;

    public long getTotalParticipation(final Integer donationEventIdx) {
        return repository.countAllByEventInfoIdx(donationEventIdx);
    }

    public void participateDonation(final String userUuid, final int eventIdx, final int pcIdx) {

        if (repository.existsByUserUuidAtToday(userUuid)) {
            throw new AlreadyExistException("오늘은 이미 이벤트에 참여하셨습니다.");
        }

        repository.save(EventDonation.of(eventIdx, userUuid, pcIdx));
    }

    public List<EventDonationThumbnailMapper> getThumbnailAll() {
        return repository.findThumbnailAll();
    }
}
