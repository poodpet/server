package com.pood.server.service;


import static com.pood.server.exception.ErrorMessage.API_TOKEN;
import static com.pood.server.exception.ErrorMessage.USER;

import com.pood.server.dto.meta.goods.GoodsDto;
import com.pood.server.dto.meta.goods.PromotionDto;
import com.pood.server.dto.user.UserBasketDto;
import com.pood.server.entity.user.UserBasket;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.exception.CustomStatusException;
import com.pood.server.repository.meta.ProductRepository;
import com.pood.server.repository.meta.PromotionRepository;
import com.pood.server.repository.user.UserBasketRepository;
import com.pood.server.repository.user.UserInfoRepository;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserBasketServiceV2 {

    @Autowired
    UserInfoRepository userInfoRepository;

    @Autowired
    UserBasketRepository userBasketRepository;

    @Autowired
    PromotionRepository promotionRepository;

    @Autowired
    ProductRepository productRepository;

    public List<UserBasket> getBasketList(String token){
        Optional.ofNullable(token).orElseThrow(() ->
            CustomStatusException.unAuthorized(API_TOKEN));
        UserInfo userInfo = userInfoRepository.getUserByTokenOptional(token)
            .orElseThrow(() -> CustomStatusException.unAuthorized(USER));

        return userBasketRepository.findAllByUserUuidAndStatusIn(userInfo.getUserUuid(),Arrays.asList(1,2));
    }

    public List<UserBasketDto.UserBasketListDto> getMyBasketGoodsDetailList( List<UserBasket> basketList, List<GoodsDto.GoodsDetail> goodsInfoList) {
        List<UserBasketDto.UserBasketListDto> results = new ArrayList<>();

        for (UserBasket userBasket : basketList) {

            UserBasketDto.UserBasketListDto userBasketListDto = new UserBasketDto.UserBasketListDto(userBasket.getIdx(), userBasket.getGoodsIdx(), userBasket.getPrCodeIdx(), userBasket.getQty(), userBasket.getGoodsPrice(), userBasket.getStatus(), null);

            GoodsDto.GoodsDetail sortedGoodsList = goodsInfoList.stream().filter(x -> x.getIdx().equals(userBasket.getGoodsIdx())).findFirst()
                    .orElse(null);
            if(sortedGoodsList == null)
                continue;

            userBasketListDto.setGoodsInfo(sortedGoodsList);

            PromotionDto.PromotionGroupGoods2 promotionGroupGoods2 = promotionRepository.promotionGroupGoodsInfo(userBasket.getGoodsIdx(), userBasket.getPrCodeIdx());
            userBasketListDto = promotionPriceSetting(userBasketListDto, promotionGroupGoods2);

            results.add(userBasketListDto);
        }

        return results;
    }

    private UserBasketDto.UserBasketListDto promotionPriceSetting(UserBasketDto.UserBasketListDto userBasketListDto, PromotionDto.PromotionGroupGoods2 promotionGroupGoods2){
        
        if(promotionGroupGoods2.getGoodsIdx() != null){
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime dateStart = LocalDateTime.parse(promotionGroupGoods2.getStartPeriod(), formatter);
            LocalDateTime dateEnd = LocalDateTime.parse(promotionGroupGoods2.getEndPeriod(), formatter);
            userBasketListDto.setPromotionAvailable(LocalDateTime.now().isAfter(dateStart) && LocalDateTime.now().isBefore(dateEnd));
            userBasketListDto.setPromotion(Boolean.TRUE);
            userBasketListDto.setPromotionGroupGoods(promotionGroupGoods2);
        }else{
            userBasketListDto.setPromotionAvailable(Boolean.FALSE);
            userBasketListDto.setPromotion(Boolean.FALSE);
            userBasketListDto.setPromotionGroupGoods(null);
        }
        return userBasketListDto;
    }
}
