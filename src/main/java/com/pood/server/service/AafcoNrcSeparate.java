package com.pood.server.service;

import static com.pood.server.exception.ErrorMessage.SERVER;

import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.exception.CustomStatusException;
import com.pood.server.repository.meta.AafcoNrcRepository;
import com.pood.server.util.PetCategoryName;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AafcoNrcSeparate {

    private final AafcoNrcRepository aafcoNrcRepository;

    public AafcoNrc getAafcoNrc(final int pcIdx, final PetCategoryName petCategoryName) {
        List<AafcoNrc> aafcoNrcList = aafcoNrcRepository.findAllByPetIdx(pcIdx,
            Sort.by(Sort.Direction.ASC, "idx"));
        if (!petCategoryName.equals(PetCategoryName.PUPPY) && !petCategoryName.equals(
            PetCategoryName.KITTEN)) {
            return aafcoNrcList.stream()
                .skip(aafcoNrcList.size() - 2L)
                .findFirst()
                .orElseThrow(() -> CustomStatusException.serverError(SERVER));
        }
        return aafcoNrcList.stream()
            .findFirst()
            .orElseThrow(() -> CustomStatusException.serverError(SERVER));
    }

}
