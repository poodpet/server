package com.pood.server.service;

import com.pood.server.controller.request.reviewclap.ReviewClapCreateDto;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserReview;
import com.pood.server.entity.user.UserReviewClap;
import com.pood.server.repository.user.UserReviewClapRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("userTransactionManager")
public class UserReviewClapSeparate {

    private final UserReviewClapRepository userReviewClapRepository;

    public boolean isExistReviewLike(final ReviewClapCreateDto reviewClapCreateDto,
        final UserInfo userInfo) {
        return userReviewClapRepository.existsByUserIdxAndUserReviewIdx(
            userInfo.getIdx(),
            reviewClapCreateDto.getReviewIdx());
    }

    public void saveReviewLike(final int userIdx, final UserReview userReview) {
        userReviewClapRepository.save(UserReviewClap.toEntity(userIdx, userReview));
    }

    public void deleteReviewLike(final int userIdx, final int reviewId) {
        userReviewClapRepository.deleteByUserIdxAndUserReviewIdx(userIdx, reviewId);
    }

}
