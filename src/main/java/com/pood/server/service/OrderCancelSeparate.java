package com.pood.server.service;

import com.pood.server.entity.order.OrderCancel;
import com.pood.server.repository.order.OrderCancelRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderCancelSeparate {

    private final OrderCancelRepository orderCancelRepository;
    private static final int SUCCESS_TYPE = 1;
    public int countOrderCancelByOrderNumber(final String orderNumber) {
        return orderCancelRepository.countOrderCancelByOrderNumber(orderNumber);
    }

    public List<OrderCancel> getOrderCancelListByOrderNumber(final String orderNumber) {
        return orderCancelRepository.findByOrderNumberAndType(orderNumber, SUCCESS_TYPE);
    }
}
