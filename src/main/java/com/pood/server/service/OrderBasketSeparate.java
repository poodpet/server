package com.pood.server.service;

import com.pood.server.dto.order.OrderBasketUserGroup;
import com.pood.server.entity.meta.Goods;
import com.pood.server.entity.order.OrderBasket;
import com.pood.server.entity.order.mapper.OrderBasketMapper;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.repository.order.OrderBasketRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderBasketSeparate {

    private final OrderBasketRepository orderBasketRepository;

    public OrderBasket save(final OrderBasket orderBasket) {
        return orderBasketRepository.save(orderBasket);
    }

    public List<OrderBasket> getOrderBasketByOrderNumber(final String orderNumber) {
        return orderBasketRepository.findAllByOrderNumber(
            orderNumber);
    }

    public OrderBasketUserGroup getGoodsQuantity(final int userIdx, final Goods goods) {
        return new OrderBasketUserGroup(
            orderBasketRepository.getBasketJoinOrder(userIdx, goods.getIdx()));
    }

    public boolean isBuyAvailable(final UserInfo userInfo, final Goods goods) {
        return orderBasketRepository.isBuyAvailable(userInfo.getIdx(), goods.getIdx());
    }

    public List<OrderBasketMapper> getUserOrderBasket(final int userIdx) {
        return orderBasketRepository.getUserOrderBasket(userIdx);
    }
}
