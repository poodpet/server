package com.pood.server.service;

import com.pood.server.entity.meta.PoodImage;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.meta.PoodImageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional("metaTransactionManager")
@RequiredArgsConstructor
public class PoodImageSeparate {

    private final PoodImageRepository poodImageRepository;

    public PoodImage findByIdx(final int imageIdx) {
        return poodImageRepository.findById(imageIdx)
            .orElseThrow(() -> new NotFoundException("해당하는 이미지를 찾을 수 없습니다."));
    }

}
