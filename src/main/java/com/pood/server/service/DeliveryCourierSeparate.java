package com.pood.server.service;

import com.amazonaws.services.kms.model.NotFoundException;
import com.pood.server.config.meta.META_DELIVERY_FEE;
import com.pood.server.entity.meta.DeliveryCourier;
import com.pood.server.repository.meta.DeliveryCourierRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional("metaTransactionManager")
@RequiredArgsConstructor
public class DeliveryCourierSeparate {

    private static final int DEFUALT_TYPE = 0;

    private final DeliveryCourierRepository deliveryCourierRepository;

    public int findFitRemoteType(final String zipcode) {
        final List<DeliveryCourier> deliveryCourierList = deliveryCourierRepository.findAllByStartZipcodeLessThanEqualAndEndZipcodeGreaterThanEqual(
            zipcode, zipcode);

        int remoteType = 0;

        for (DeliveryCourier deliveryCourier : deliveryCourierList) {
            remoteType = deliveryCourier.getAreaType();
        }

        return remoteType;
    }

    public int getDeliveryFee(final String zipcode, final int remoteType) {

        if (remoteType == DEFUALT_TYPE) {
            return META_DELIVERY_FEE.DEFUALT_DELIVERY_FEE;
        }
        return META_DELIVERY_FEE.DEFUALT_DELIVERY_FEE
            + deliveryCourierRepository.findDeliveryFeeByZipcodeAndType(zipcode, remoteType)
            .orElseThrow(() -> new NotFoundException("배송지 정보를 찾을수 없습니다."));
    }
}
