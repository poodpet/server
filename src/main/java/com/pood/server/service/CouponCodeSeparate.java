package com.pood.server.service;

import com.pood.server.entity.meta.CouponCode;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.meta.CouponCodeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("metaTransactionManager")
public class CouponCodeSeparate {

    private final CouponCodeRepository couponCodeRepository;

    public CouponCode findByCode(final String code) {
        return couponCodeRepository.findByCode(code)
            .orElseThrow(() -> new NotFoundException("쿠폰이 존재하지 않습니다."));
    }
}
