package com.pood.server.service;


import com.pood.server.dto.meta.event.EventDto.EventDetail;
import com.pood.server.repository.meta.EventPhotoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EventPhotoSeparate implements EventTypeJoinPossible {

    private static final String PHOTO_TYPE = "D";

    private final EventPhotoRepository eventPhotoRepository;

    @Override
    public boolean isParticipationPossible(EventDetail eventDetail,
        final int userIdx) {
        int count = eventPhotoRepository.countByEventInfoIdxAndUserInfoIdx(eventDetail.getIdx(),
            userIdx);
        return eventDetail.getAttendLimit() > count;
    }

    @Override
    public String getType() {
        return PHOTO_TYPE;
    }

}
