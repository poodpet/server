package com.pood.server.service;

import com.pood.server.entity.log.LogUserSavePoint;
import com.pood.server.repository.log.LogUserSavePointRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional("logTransactionManager")
@RequiredArgsConstructor
public class LogUserSavePointSeparate {

    private static final int SAVE_UP_POINT = 11;
    private static final int RECEIVED_BENEFIT = 12;

    private final LogUserSavePointRepository logUserSavePointRepository;

    public List<LogUserSavePoint> findReceivedBenefitAll(final String userUuid) {
        return logUserSavePointRepository.findAllByUserUuidAndType(userUuid, RECEIVED_BENEFIT);
    }

    public List<LogUserSavePoint> findToBeRewardPoints(final String userUuid) {
        return logUserSavePointRepository.findAllByUserUuidAndType(userUuid, SAVE_UP_POINT);
    }

}
