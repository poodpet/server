package com.pood.server.service.pet_solution.calc;


import com.pood.server.controller.response.solution.PetProductWordResponse;
import com.pood.server.dto.meta.pet.PetProductWord;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.ProductType;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.util.FeedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PetProduct implements Suitable, WordExtract {

    private final UserPetBaseData userPetBaseData;
    private final ProductBaseData productBaseData;

    @Override
    public double getScore() {
        final double petSizeScore = fitPetSizeAndIsDog(); //  lifeRecommendDealCalc
        final double petAgeScore = fitAge();  // lifeRecommendDealCalc
        double petGrainSizeScore = 0;
        double petNeuteredScore = 0;
        double petMoistureScore = 0;
        double petAllergyScore = 0;
        if (productBaseData.eqProductType(ProductType.FEED)) {
            petGrainSizeScore = fitGrainSizeAndWetFeed();
            petNeuteredScore = fitNeutered();
            petMoistureScore = fitMoisture();  // 펫 습식
        }
        if (!productBaseData.eqProductType(ProductType.SUPPLIES)) {
            petAllergyScore = fitAllergy(); //  lifeRecommendDealCalc // calcAllergy
        }

        if (productBaseData.eqProductType(ProductType.FEED)) {
            int totalPoint = (int) (petSizeScore + petAgeScore + petGrainSizeScore
                + petNeuteredScore + petMoistureScore + petAllergyScore);
            return Math.min(totalPoint, 100);
        }

        if (productBaseData.eqProductType(ProductType.SUPPLIES) && userPetBaseData.isPetTypeDog()) {
            return getNotFeedScore(petSizeScore, petAgeScore);
        }
        if (productBaseData.eqProductType(ProductType.SUPPLIES) && userPetBaseData.isPetTypeCat()) {
            return getNotFeedScore(petAgeScore);
        }
        if (userPetBaseData.isPetTypeDog()) {
            return getNotFeedScore(petSizeScore, petAgeScore, petAllergyScore);
        }
        return getNotFeedScore(petAgeScore, petAllergyScore);
    }

    private double getNotFeedScore(double... petScores) {
        List<Boolean> scoreChkList = new ArrayList<>();
        for (double score : petScores) {
            if (score > 0) {
                scoreChkList.add(Boolean.TRUE);
            }
        }
        return Math.ceil(((double) scoreChkList.size() / petScores.length) * 100);
    }


    @Override
    public double calcProductTypeScore() {
        return (getScore() / 100) * getProductTypeScorePercent();
    }

    @Override
    public int getProductTypeScorePercent() {
        if (productBaseData.eqProductType(ProductType.FEED)) {
            return 20;
        }
        return 30;
    }

    @Override
    public String makeScoreKey() {
        return "baseScore";
    }

    /**
     * feedTarget/펫 크기 유형 체크 펫 크기는 분류는 강아지만 있다
     */
    private double fitPetSizeAndIsDog() {
        if (userPetBaseData.isPetTypeCat()) {
            return 0;
        }
        //feed_target == 0은 모두에게 잘 맞음 또는 제품의 FeedTarget과 유저 펫의 feedTarget이 일치한다면
        if (productBaseData.getFeedTarget() == 0 ||
            productBaseData.eqFeedTarget(userPetBaseData.getUserPetFeedTargetNum())) {
            return userPetBaseData.getFeedTargetSurgeryScore();
        }
        return 0.0;
    }

    private double fitAge() {
        // 상품연령대, 유저펫의 연령대 맞는경우 중성화 여부와 펫 유형에 따라 점수를 줌
        boolean isFlag = FeedType.isFlag(productBaseData.getFeedType(), userPetBaseData);
        if (isFlag) {
            return userPetBaseData.getPetAgeSurgeryScore();
        }
        return 0.0;
    }

    /**
     * 선호하는 알갱이 크기 분석
     */
    private double fitGrainSizeAndWetFeed() {
        return userPetBaseData.getPetUnitSizeScore(productBaseData.getUnitSize(),
            productBaseData.isWetFeed());
    }

    /**
     * 펫이 가지고 있는 알러지에 도움이 되는 제품인지 분석
     */
    private double fitAllergy() {
        if (productBaseData.getProductType().equals(ProductType.SUPPLIES)) {
            return 0.0d;
        }
        return userPetBaseData.getPetAllergyScore(productBaseData.getAnimalProtein(),
            productBaseData.getMainProperty());
    }

    /**
     * 중성화 수술 여부 상품의 칼로리가 4000가 넘고 중성화 수술을 안했을 경우 점수 계산 X
     */
    private double fitNeutered() {
        //상품의 칼로리가 4000이 넘거나 중성화 수술을 안했을 경우 점수 계산 X
        if (Objects.isNull(productBaseData.getCalorie())
            || (productBaseData.getCalorie() >= 4000
            && userPetBaseData.isNeutered())) {
            return 0;
        }
        return userPetBaseData.getPetSurgeryScore();
    }

    /**
     * 수분공급 분석, 습식(ct_sub_idx == 1) 인 경우에만 점수 추가
     */
    private double fitMoisture() {
        if (!productBaseData.isWetFeed()) {
            return 0;
        }
        return userPetBaseData.getPetMoistureSurgeryScore();
    }

    @Override
    public Object fitWord() {
        if (!productBaseData.getProductType().equals(ProductType.FEED)) {
            return new PetProductWordResponse(
                new PetProductWord(
                    getAgeString(), convertFitAgeString(),
                    getPcSize(), convertFitPetSizeString(),
                    null, convertFitNeutered()
                )
            );
        }
        return new PetProductWordResponse(
            new PetProductWord(
                getAgeString(), convertFitAgeString(),
                getPcSize(), convertFitPetSizeString(),
                getNeutered(), convertFitNeutered()
            )
        );
    }

    // 나이 퍼피, 어덜트, 시니어
    private String getAgeString() {
        return userPetBaseData.petAgeString();
    }

    private String getPcSize() {
        if (userPetBaseData.isPetTypeCat()) {
            return null;
        }
        return userPetBaseData.getPcSize();
    }

    private String getNeutered() {
        if (userPetBaseData.isNeutered()) {
            return "중성화 했어요";
        }
        return null;
    }

    private String convertFitAgeString() {
        final boolean flag = FeedType.isFlag(productBaseData.getFeedType(), userPetBaseData);
        if (flag) {
            return "적합";
        }
        return null;
    }

    private String convertFitPetSizeString() {
        if (fitPetSizeAndIsDog() > 0) {
            return "적합";
        }
        return null;
    }

    private String convertFitNeutered() {
        if (fitNeutered() > 0) {
            return "적합";
        }
        return null;
    }

    public PetProductWord toWord() {
        if (!productBaseData.getProductType().equals(ProductType.FEED)) {
            new PetProductWord(
                getAgeString(), convertFitAgeString(),
                getPcSize(), convertFitPetSizeString(),
                null, convertFitNeutered()
            );
        }
        return new PetProductWord(
            getAgeString(), convertFitAgeString(),
            getPcSize(), convertFitPetSizeString(),
            getNeutered(), convertFitNeutered()
        );
    }

}
