package com.pood.server.service.pet_solution.group;

import com.pood.server.entity.meta.AllergyData;
import com.pood.server.entity.user.UserPetAllergy;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

public class UserPetAllergyAiSolutionGroup {

    private final List<UserPetAllergy> userPetAllergies;

    public UserPetAllergyAiSolutionGroup(final List<UserPetAllergy> userPetAllergyList) {
        this.userPetAllergies = userPetAllergyList;
    }

    public boolean isPetAllergyCompatibility(final String animalProtein, final String mainProperty) {
        for (UserPetAllergy userPetAllergy : userPetAllergies) {
            if (userPetAllergy.getName().contains(animalProtein) || userPetAllergy.getName()
                .contains(mainProperty)) {
                return true;
            }
        }
        return false;
    }

    public List<AllergyStr> isFitPetAllergy(final String animalProtein, final String mainProperty,
        final List<AllergyData> allergyDataList) {

        List<AllergyStr> allergyStrList = new ArrayList<>();
        for (UserPetAllergy userPetAllergy : userPetAllergies) {
            String imgUrl = null;
            for (AllergyData allergyData : allergyDataList) {
                if (userPetAllergy.getAllergyIdx().equals(allergyData.getIdx())) {
                    imgUrl = allergyData.getUrl();
                }
            }

            if (userPetAllergy.getName().contains(animalProtein) || userPetAllergy.getName()
                .contains(mainProperty)) {
                allergyStrList.add(AllergyStr.fit(userPetAllergy.getName(), imgUrl));
                continue;
            }
            allergyStrList.add(AllergyStr.notFit(userPetAllergy.getName(), imgUrl));
        }
        return allergyStrList;
    }

    public List<String> getAllergyNameList() {
        return userPetAllergies.stream()
            .map(UserPetAllergy::getName)
            .collect(Collectors.toList());
    }

    public List<Integer> getAllergyIdxList() {
        return userPetAllergies.stream()
            .map(UserPetAllergy::getAllergyIdx)
            .collect(Collectors.toList());
    }

    @Getter
    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    public static class AllergyStr {

        private final String allergyName;
        private final String fitString;
        private final String imgUrl;

        public static AllergyStr fit(final String allergyName, final String imgUrl) {
            return new AllergyStr(allergyName, null, imgUrl);
        }

        public static AllergyStr notFit(final String allergyName, final String imgUrl) {
            return new AllergyStr(allergyName, "없음", imgUrl);
        }
    }

}
