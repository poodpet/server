package com.pood.server.service.pet_solution;

import lombok.experimental.UtilityClass;

@UtilityClass
public class SuitableCalc {

    public boolean isMinEnough(final float minValue, final float baseValue) {
        return minValue <= baseValue;
    }

    public boolean isBothEnough(final float minValue, final float maxValue, final float baseValue) {
        return minValue <= baseValue && baseValue <= maxValue;
    }

    public String makeMessage(final float minValue, final float baseValue) {
        if (isMinEnough(minValue, baseValue)) {
            return "만족";
        }
        return "미달";
    }

    public String makeMinMaxMessage(final float minValue, final float maxValue, final float baseValue) {
        if (isBothEnough(minValue, maxValue, baseValue)) {
            return "만족";
        }

        if (minValue > baseValue) {
            return "미달";
        }

        return "초과";
    }

    public float calculateIntakeRange(final double value) {
        float rValue = (float) (Math.floor(value * 10) / 10f);
        if (rValue >= 0f && rValue <= 0.7f) {
            return 0.5f;
        }

        if (rValue >= 0.8f && rValue <= 1.2f) {
            return 1f;
        }

        if (rValue >= 1.3f && rValue <= 1.7f) {
            return 1.5f;
        }

        if (rValue >= 1.8f && rValue <= 2.2f) {
            return 2f;
        }

        if (rValue >= 2.3f && rValue <= 2.7f) {
            return 2.5f;
        }

        if (rValue >= 2.8f && rValue <= 3.2f) {
            return 3f;
        }

        if (rValue >= 3.3f && rValue <= 3.7f) {
            return 3.5f;
        }

        if (rValue >= 3.8f && rValue <= 4.2f) {
            return 4f;
        }

        if (rValue >= 4.3f && rValue <= 4.7f) {
            return 4.5f;
        }

        if (rValue >= 4.8f && rValue <= 5.2f) {
            return 5f;
        }

        if (rValue >= 5.3f && rValue <= 5.7f) {
            return 5.5f;
        }

        if (rValue >= 5.8f && rValue <= 6.2f) {
            return 6f;
        }

        if (rValue >= 6.3f && rValue <= 6.7f) {
            return 6.5f;
        }

        if (rValue >= 6.8f && rValue <= 7.2f) {
            return 7f;
        }

        if (rValue >= 7.3f && rValue <= 7.7f) {
            return 7.5f;
        }

        if (rValue >= 7.8f && rValue <= 8.2f) {
            return 8f;
        }

        if (rValue >= 8.3f && rValue <= 8.7f) {
            return 8.5f;
        }

        if (rValue >= 8.8f && rValue <= 9.2f) {
            return 9f;
        }

        if (rValue >= 9.3f && rValue <= 9.7f) {
            return 9.5f;
        }

        if (rValue >= 9.8f && rValue <= 10.2f) {
            return 10f;
        }

        if (rValue >= 10.3f && rValue <= 10.7f) {
            return 10.5f;
        }

        if (rValue >= 10.8f && rValue <= 11.2f) {
            return 11f;
        }

        if (rValue >= 11.3f && rValue <= 11.7f) {
            return 11.5f;
        }

        return 12f;
    }
}
