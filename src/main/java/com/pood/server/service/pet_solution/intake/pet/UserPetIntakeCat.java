package com.pood.server.service.pet_solution.intake.pet;

import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.service.pet_solution.intake.UserPetIntake;

public class UserPetIntakeCat extends UserPetIntake {

    public UserPetIntakeCat(final UserPetBaseData userPetBaseData, final ProductBaseData productBaseData) {
        super(userPetBaseData, productBaseData);
    }

    // 반려동물의 하루치 칼로리
    @Override
    public float getPetCalorie() {
        final float weight = userPetBaseData.catBasicWeight();
        final long month = userPetBaseData.getAgeMonth();

        if (month >= 13L) {
            return (float) (weight * (Math.pow(userPetBaseData.getNowPetWeight(), 0.711f)));
        }

        return weight * (userPetBaseData.getNowPetWeight());
    }

    @Override
    public int getCupCalorie() {
        return (int) ((productBaseData.getCalorie() / KG_1) * productBaseData.getCupWeight());
    }

    //  펫칼로리를 컵칼로리로 나눈 수량
    @Override
    public float divideCupCalorieAmount() {
        return getPetCalorie() / getCupCalorie();
    }

    // 제품에 대한 동물이 먹어야 하는 그램
    @Override
    public float calcEatFoodGrams() {
        return getPetCalorie() / (productBaseData.getCalorie() / KG_1);
    }

}
