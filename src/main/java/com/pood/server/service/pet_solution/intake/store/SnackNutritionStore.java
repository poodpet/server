package com.pood.server.service.pet_solution.intake.store;

import com.pood.server.entity.meta.Snack;
import com.pood.server.entity.meta.Snack.SnackType;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.intake.ProductStore;
import com.pood.server.service.pet_solution.intake.UserPetIntake;
import java.util.Objects;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SnackNutritionStore implements ProductStore {

    private final ProductBaseData productBaseData;
    private final UserPetIntake userPetIntake;

    //하루 간식 섭취량 cupType
    @Override
    public String cupType() {
        return SnackType.messageType(productBaseData.getSnack().getType()).getType();
    }

    @Override
    public String cupSubType() {
        return SnackType.messageType(productBaseData.getSnack().getType()).getSubType();
    }

    //일별 섭취량
    @Override
    public double dailyTakesToEat() {
        //min max 빈값일 경우 = -1
        final Snack snack = productBaseData.getSnack();
        double maxValue = snack.getMax();
        double minValue = snack.getMin();

        if (Objects.nonNull(snack)) {

            // max 없을 경우(0) -> (펫칼로리 *0.1) / per_cal
            if (maxValue == 0) {
                float v = calculatePetCal();
                return getRoundProc(v);
            }

            // min max 같을 경우 min 값으로 세팅
            if (maxValue == minValue) {
                return minValue;
            }

            if (calculatePetCal() < minValue) {
                return minValue;
            }

            // gram으로 나오는 경우 ex) (캣만두 펫칼로리 *0.1 ) /per_grams
            if (snack.isGramType() || snack.isMLType()) {
                final Double perGrams = snack.getPerGrams();
                final int round = Math.round(calculatePetCal());
                return getRoundProc(round * perGrams);
            }

            // max 있을 경우 하루간식섭취량구하기() 계산해서 max 넘으면 max값으로 세팅
            if (maxValue > 0) {
                if (calculatePetCal() > maxValue) {
                    return maxValue;
                }
                if (calculatePetCal() < 0.5) {
                    return 0.5;
                }

                return getRoundProc(Math.round(calculatePetCal()));
            }
            return getRoundProc(calculatePetCal());
        }

        final float calculateValue = calculatePetCal();
        // dailySnackAnalysisResult 값이 0보다 크면서 min보다 작을 경우 min값으로 사용
        if (calculateValue <= minValue && minValue > 0) {
            return snack.getMin();
        }

        //dailySnackAnalysisResult 값이 max보다 크면서 max 값이 0보다 클 경우 max값으로 사용
        if (calculateValue >= snack.getMax() && snack.getMax() > 0) {
            return snack.getMax();
        }

        return (float) getRoundProc(calculateValue);
    }

    private double getRoundProc(final double val) {
        return Math.ceil(val * 2.0) / 2.0;
    }

    // 먹는데 걸리는 기간
    @Override
    public int longItTakesToEat() {
        return productBaseData.intakeSnackPeriod(dailyTakesToEat());
    }

    private float calculatePetCal() {
        return (float) (userPetIntake.getPetCalorie() * 0.1 / productBaseData.getSnack()
            .getPerCal());
    }

}
