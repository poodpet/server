package com.pood.server.service.pet_solution.nutrition;

public interface NutritionDetailInfo {

    NutritionDetailModelLegacy create();

    NutritionDetailModel createModel();
}
