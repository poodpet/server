package com.pood.server.service.pet_solution.nutrition.dog;

import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.SuitableCalc;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModel;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModelLegacy;
import com.pood.server.service.pet_solution.nutrition.NutritionDog;
import com.pood.server.service.pet_solution.nutrition.NutritionUnit;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MiCalciumDog implements NutritionDog {

    private static final int TYPE_TEN = 10;
    private static final String TITLE = "칼슘";
    private static final float AAFCO_MAX_MICALCIUM = 6.25f;

    private final UserPetBaseData userPetBaseData;
    private final ProductBaseData productBaseData;
    private final AafcoNrc aafcoNrc;

    @Override
    public NutritionDetailModelLegacy create() {
        if (aafcoNrc.getMiCalcium() > 0 && productBaseData.getFeedMiCalcium() > 0) {
            return null;
        }

        float petCalorie = userPetBaseData.getPetCalorie();
        float limitMin = (float) (aafcoNrc.getMiCalcium() * (petCalorie / 1000f));
        float limitMax = AAFCO_MAX_MICALCIUM * (petCalorie / 1000f);
        float contain = (float) ((TYPE_TEN * productBaseData.getFeedMiCalcium()) * (1
            / productBaseData.getCalorie().floatValue()) * petCalorie);

        return new NutritionDetailModelLegacy(
            TITLE, limitMin >= contain,
            String.format(NutritionUnit.NEED_G.getUnit(), limitMin),
            String.format(NutritionUnit.NEED_G_MAX.getUnit(), limitMax),
            String.format(NutritionUnit.INCLUDE_G.getUnit(), contain),
            16f, true
        );
    }

    @Override
    public NutritionDetailModel createModel() {
        if (productBaseData.getFeedMiCalcium() < 0) {
            return null;
        }

        float minValue = aafcoNrc.getMiCalcium().floatValue();
        float maxValue = AAFCO_MAX_MICALCIUM;
        final double baseValue = productBaseData.getFeedMiCalcium();
        float calcValue = (float) ((TYPE_TEN * baseValue) *
            (1 / productBaseData.getCalorie().floatValue())) * 1000f;

        if (baseValue <= 0.0) {
            return null;
        }

        return new NutritionDetailModel(TITLE,
            SuitableCalc.makeMinMaxMessage(minValue, maxValue, calcValue),
            NutritionUnit.unitConvert(NutritionUnit.PERCENT, baseValue),
            SuitableCalc.isBothEnough(minValue, maxValue, calcValue), 16f);
    }

}
