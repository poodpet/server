package com.pood.server.service.pet_solution.calc;

import com.pood.server.controller.response.solution.AllergyWordResponse;
import com.pood.server.entity.meta.AllergyData;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.service.pet_solution.group.UserPetAllergyAiSolutionGroup.AllergyStr;
import java.util.List;

public class AllergyExtract implements WordExtract {

    private final UserPetBaseData userPetBaseData;
    private final ProductBaseData productBaseData;
    private final List<AllergyData> allergyDataList;

    public AllergyExtract(final UserPetBaseData userPetBaseData,
        final ProductBaseData productBaseData,
        final List<AllergyData> allergyDataList) {
        this.userPetBaseData = userPetBaseData;
        this.productBaseData = productBaseData;
        this.allergyDataList = allergyDataList;
    }

    @Override
    public Object fitWord() {
        return new AllergyWordResponse(
            userPetBaseData.fitEachAllergy(productBaseData.getAnimalProtein(),
                productBaseData.getMainProperty(),
                allergyDataList));
    }

    public List<AllergyStr> toWordList() {
        return userPetBaseData.fitEachAllergy(productBaseData.getAnimalProtein(),
            productBaseData.getMainProperty(), allergyDataList);
    }
}
