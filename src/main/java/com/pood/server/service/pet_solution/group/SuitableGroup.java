package com.pood.server.service.pet_solution.group;

import com.pood.server.controller.response.product.ProductSolutionPointResponse;
import com.pood.server.service.pet_solution.calc.Suitable;
import java.util.List;
import java.util.stream.Collectors;

public class SuitableGroup {

    private final List<Suitable> suitableList;

    public SuitableGroup(final List<Suitable> suitableList) {
        this.suitableList = suitableList;
    }

    public double getTotalScore() {
        return suitableList.stream()
            .mapToDouble(Suitable::calcProductTypeScore)
            .sum();
    }

    public List<ProductSolutionPointResponse> suitableList() {
        final List<ProductSolutionPointResponse> responseList = suitableList.stream()
            .map(suitable -> ProductSolutionPointResponse.of(suitable.makeScoreKey(), (int) suitable.getScore()))
            .collect(Collectors.toList());
        responseList.add(ProductSolutionPointResponse.of("totalScore", (int) getTotalScore()));
        return responseList;
    }
}
