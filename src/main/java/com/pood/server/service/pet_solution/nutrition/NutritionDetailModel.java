package com.pood.server.service.pet_solution.nutrition;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class NutritionDetailModel {

    private final String title;
    private final String suitable;
    private final String baseValue;
    private final boolean enough;
    private final float score;


    public float isEnoughThenCalcScore() {
        return enough ? score : 0;
    }

}

