package com.pood.server.service.pet_solution.calc;

import com.pood.server.controller.response.solution.GrainSizeWordResponse;
import com.pood.server.entity.meta.GrainSize;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.UserPetBaseData;
import java.util.List;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GrainSizeExtract implements WordExtract {

    private final UserPetBaseData userPetBaseData;
    private final ProductBaseData productBaseData;
    private final List<GrainSize> grainSizeList;

    @Override
    public Object fitWord() {
        return new GrainSizeWordResponse(
            userPetBaseData.fitEachUnitSize(productBaseData.getUnitSize(),
                grainSizeList, productBaseData.isWetFeed()));
    }
}
