package com.pood.server.service.pet_solution;

import com.pood.server.entity.meta.Brand;
import com.pood.server.entity.meta.Feed;
import com.pood.server.entity.meta.FeedUnitSize;
import com.pood.server.entity.meta.PetDoctorFeedDescription;
import com.pood.server.entity.meta.Product.PackageType;
import com.pood.server.entity.meta.Snack;
import com.pood.server.service.pet_solution.group.UserPetAiDiagnosisAiSolutionGroup.UserPetAiDiagnosisStr;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@Setter
@NoArgsConstructor
public class ProductBaseData {

    private static final String WET_FEED = "습식";

    Logger logger = LoggerFactory.getLogger(ProductBaseData.class);

    private Integer idx;
    private String uuid;
    private Brand brand;
    private String productName;
    private String productCode;
    private String barcode;
    private Integer pcIdx;
    private Integer ctIdx;
    private Integer ctSubIdx;
    private String ctSubName;
    private Integer showIndex;
    private String tag;
    private Integer allNutrients;
    private String mainProperty;
    private String tasty;
    private String productVideo;
    private Integer calorie;
    private Integer feedTarget;
    private String feedType;
    private Integer glutenFree;
    private String animalProtein;
    private String vegetableProtein;
    private String aafco;
    private Integer singleProtein;
    private String ingredients;
    private Double unitSize;
    private String ingredientsSearch;
    private String noticeDesc;
    private String noticeTitle;
    private PackageType packageType;
    private Integer cupWeight;
    private Integer isRecommend;
    private Integer grainFree;
    private Integer avaQuantity;
    private Integer quantity;
    private Integer weight;
    private boolean approve;
    private Integer productQty;
    private LocalDateTime updatetime;
    private LocalDateTime recordbirth;

    private Feed feed;
    private Snack snack;
    private PetDoctorFeedDescription petDoctorFeedDescription;

    @Builder
    public ProductBaseData(Integer idx, String uuid, Brand brand, String productName,
        String productCode, String barcode, Integer pcIdx, Integer ctIdx, Integer ctSubIdx,
        String ctSubName, Integer showIndex, String tag, Integer allNutrients,
        String mainProperty, String tasty, String productVideo, Integer calorie,
        Integer feedTarget, String feedType, Integer glutenFree, String animalProtein,
        String vegetableProtein, String aafco, Integer singleProtein, String ingredients,
        Double unitSize, String ingredientsSearch, String noticeDesc, String noticeTitle,
        PackageType packageType, Integer cupWeight, Integer isRecommend, Integer grainFree,
        Integer avaQuantity, Integer quantity, Integer weight, boolean approve,
        Integer productQty, LocalDateTime updatetime, LocalDateTime recordbirth,
        Feed feed, Snack snack,
        PetDoctorFeedDescription petDoctorFeedDescription) {
        this.idx = idx;
        this.uuid = uuid;
        this.brand = brand;
        this.productName = productName;
        this.productCode = productCode;
        this.barcode = barcode;
        this.pcIdx = pcIdx;
        this.ctIdx = ctIdx;
        this.ctSubIdx = ctSubIdx;
        this.ctSubName = ctSubName;
        this.showIndex = showIndex;
        this.tag = tag;
        this.allNutrients = allNutrients;
        this.mainProperty = mainProperty;
        this.tasty = tasty;
        this.productVideo = productVideo;
        this.calorie = calorie;
        this.feedTarget = feedTarget;
        this.feedType = feedType;
        this.glutenFree = glutenFree;
        this.animalProtein = animalProtein;
        this.vegetableProtein = vegetableProtein;
        this.aafco = aafco;
        this.singleProtein = singleProtein;
        this.ingredients = ingredients;
        this.unitSize = unitSize;
        this.ingredientsSearch = ingredientsSearch;
        this.noticeDesc = noticeDesc;
        this.noticeTitle = noticeTitle;
        this.packageType = packageType;
        this.cupWeight = cupWeight;
        this.isRecommend = isRecommend;
        this.grainFree = grainFree;
        this.avaQuantity = avaQuantity;
        this.quantity = quantity;
        this.weight = weight;
        this.approve = approve;
        this.productQty = productQty;
        this.updatetime = updatetime;
        this.recordbirth = recordbirth;
        this.feed = feed;
        this.snack = snack;
        this.petDoctorFeedDescription = petDoctorFeedDescription;
    }

    public float getCupCalorie() {
        return (this.calorie / 1000f) * cupWeight;
    }

    public Double getFeedPrProtein() {
        return feed.getPrProtein();
    }

    public Double getFeedPrFat() {
        return feed.getPrFat();
    }

    public Double getFeedPrAsh() {
        return feed.getPrFat();
    }

    public Double getFeedPrFiber() {
        return feed.getPrFiber();
    }

    public Double getFeedPrMoisture() {
        return feed.getPrMoisture();
    }

    public Double getFeedPrCarbo() {
        return feed.getPrCarbo();
    }

    public Double getFeedAmArginine() {
        return feed.getAmArginine();
    }

    public Double getFeedAmHistidine() {
        return feed.getAmHistidine();
    }

    public Double getFeedAmIsoleucine() {
        return feed.getAmIsoleucine();
    }

    public Double getFeedAmLeucine() {
        return feed.getAmLeucine();
    }

    public Double getFeedAmLysine() {
        return feed.getAmLysine();
    }

    public Double getFeedAmMetCys() {
        return feed.getAmMetCys();
    }

    public Double getFeedAmMethionine() {
        return feed.getAmMethionine();
    }

    public Double getFeedAmPheTyr() {
        return feed.getAmPheTyr();
    }

    public Double getFeedAmPhenylanlanine() {
        return feed.getAmPhenylanlanine();
    }

    public Double getFeedAmThreonine() {
        return feed.getAmThreonine();
    }

    public Double getFeedAmTryptophan() {
        return feed.getAmTryptophan();
    }

    public Double getFeedAmValine() {
        return feed.getAmValine();
    }

    public Double getFeedMiCalcium() {
        return feed.getMiCalcium();
    }

    public Double getFeedMiPhosphours() {
        return feed.getMiPhosphours();
    }

    public Double getFeedMiPotassium() {
        return feed.getMiPotassium();
    }

    public Double getFeedMiChloride() {
        return feed.getMiChloride();
    }

    public Double getFeedMiMagnessium() {
        return feed.getMiMagnessium();
    }

    public Double getFeedMiIron() {
        return feed.getMiIron();
    }

    public Double getFeedMiCopper() {
        return feed.getMiCopper();
    }

    public Double getFeedMiManganese() {
        return feed.getMiManganese();
    }

    public Double getFeedMiIodine() {
        return feed.getMiIodine();
    }

    public Double getFeedMiSelenium() {
        return feed.getMiSelenium();
    }

    public Double getFeedMiCaPh() {
        return feed.getMiCaPh();
    }

    public Double getFeedViVitaminA() {
        return feed.getViVitaminA();
    }

    public Double getFeedViVitaminD() {
        return feed.getViVitaminD();
    }

    public Double getFeedViVitaminE() {
        return feed.getViVitaminE();
    }

    public Double getFeedViVitaminB1() {
        return feed.getViVitaminB1();
    }

    public Double getFeedViVitaminB2() {
        return feed.getViVitaminB2();
    }

    public Double getFeedViVitaminB3() {
        return feed.getViVitaminB3();
    }

    public Double getFeedViVitaminB5() {
        return feed.getViVitaminB5();
    }

    public Double getFeedViVitaminB6() {
        return feed.getViVitaminB6();
    }

    public Double getFeedViVitaminB7() {
        return feed.getViVitaminB7();
    }

    public Double getFeedViVitaminB9() {
        return feed.getViVitaminB9();
    }

    public Double getFeedViVitaminK3() {
        return feed.getViVitaminK3();
    }

    public Double getFeedMiSodium() {
        return feed.getMiSodium();
    }

    public double getFeedMiZinc() {
        return feed.getMiZinc();
    }

    public double getFeedViVitaminB12() {
        return feed.getViVitaminB12();
    }

    public Double getFeedFaArachidonicAcid() {
        return feed.getFaArachidonicAcid();
    }

    public Double getFeedFaALinoleicacid() {
        return feed.getFaALinoleicacid();
    }

    public Double getFeedOtTaurine() {
        return feed.getOtTaurine();
    }

    public boolean isWetFeed() {
        return ctSubIdx.equals(1);
    }

    public boolean eqFeedTarget(final int target) {
        return feedTarget == target;
    }

    private List<Boolean> checkArdGroupCodeList(final List<Integer> petArdGroupCodeList) {
        List<Integer> productArdCodeList = getMeaningfulArdCodeList();
        return getArdGroupCodeMaching(petArdGroupCodeList, productArdCodeList);
    }

    private List<Integer> getMeaningfulArdCodeList() {
        List<Integer> productArdCodeList = new ArrayList<>();
        try {
            Class<? extends PetDoctorFeedDescription> aClass = petDoctorFeedDescription.getClass();
            Field[] fields = aClass.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                boolean annotationPresent = field.isAnnotationPresent(ArdCodeMakerAnnotation.class);
                if (annotationPresent) {
                    if ((int) field.get(petDoctorFeedDescription) > 0) {
                        productArdCodeList.add(Integer.valueOf(field.getName().substring(8, 11)));
                    }
                }
            }
        } catch (IllegalAccessException e) {
            logger.error("상품 고민에 대한 정보 설정에 실패했습니다. " + e.getMessage());
        }
        return productArdCodeList;
    }

    private List<Boolean> getArdGroupCodeMaching(final List<Integer> petArdGroupCodeList,
        final List<Integer> productArdCodeList) {
        return petArdGroupCodeList.stream().map(productArdCodeList::contains)
            .collect(Collectors.toList());
    }

    public double getPetWorryScore(final List<Integer> ardGroupCodeList) {
        List<Boolean> list = checkArdGroupCodeList(ardGroupCodeList);
        return AiDiagnosisScore.getTotalScore(list);
    }

    public ProductType getProductType() {
        return ProductType.getProductTypeByIdx(ctIdx);
    }

    public boolean eqProductType(ProductType productType) {
        return getProductType().equals(productType);
    }

    public boolean isDog() {
        return pcIdx == 1;
    }

    public List<UserPetAiDiagnosisStr> fitAiDiagnosis(final List<UserPetAiDiagnosisStr> worryList) {
        checkDiagnosisStr(worryList);
        return worryList;
    }

    private void checkDiagnosisStr(final List<UserPetAiDiagnosisStr> worryList) {
        List<Integer> productArdCodeList = getMeaningfulArdCodeList();
        for (UserPetAiDiagnosisStr userPetAiDiagnosisStr : worryList) {
            if (productArdCodeList.contains(userPetAiDiagnosisStr.getArdGroupCode())) {
                userPetAiDiagnosisStr.setFitName("도움");
                continue;
            }

            userPetAiDiagnosisStr.setFitName(null);
        }
    }

    public float calculateCupSize() {
        if (cupWeight > 0) {
            return cupWeight;
        }

        return 100f;
    }

    public int intakeFeedPeriod(final double dailyFeed) {
        final float feedWeight = calculateFeedWeight();
        final float ceilFeed = (float) Math.ceil(dailyFeed);

        if (Objects.isNull(snack)) {
            return (int) Math.ceil((feedWeight / calculateCupSize()) / ceilFeed);
        }

        if (snack.isNumOrCountType()) {
            return (int) Math.ceil(feedWeight / (snack.getPerGrams() * ceilFeed));
        }

        return (int) Math.ceil(feedWeight / ceilFeed);
    }

    public int intakeSnackPeriod(final double dailySnack) {
        //사료무게
        final float feedWeight = calcFeedWeight();

        if (Objects.isNull(snack)) {
            return (int) Math.ceil(feedWeight / calculateCupSize() / dailySnack);
        }

        if (snack.isPetOrNum()) {
            return (int) Math.ceil(feedWeight / (snack.getPerGrams() * dailySnack));
        }

        return (int) Math.ceil(feedWeight / dailySnack);
    }

    private float calcFeedWeight() {
        if (Objects.isNull(snack)) {
            return productQty * weight;
        }

        if (weight == 0) {
            return (float) (productQty * snack.getPerGrams());
        }

        return productQty * weight;
    }

    private float calculateFeedWeight() {
        if (Objects.isNull(snack)) {
            return productQty * weight;
        }

        if (weight == 0) {
            return (float) (productQty * snack.getPerGrams());
        }

        return productQty * weight;
    }

    public boolean isSupplies() {
        return ProductType.SUPPLIES.getIdx() == ctIdx;
    }


    private enum AiDiagnosisScore {
        ONE(1, List.of(100d)),
        TWO(2, List.of(50d, 50d)),
        THREE(3, List.of(50d, 30d, 20d));
        private final int size;
        private final List<Double> score;

        AiDiagnosisScore(final int size, final List<Double> score) {
            this.size = size;
            this.score = score;
        }

        public static double getTotalScore(final List<Boolean> list) {
            if (list.size() == 3) {
                return AiDiagnosisScore.THREE.score(list);
            }
            if (list.size() == 2) {
                return AiDiagnosisScore.TWO.score(list);
            }
            if (list.size() == 1) {
                return AiDiagnosisScore.ONE.score(list);
            }
            return 0;
        }

        private double score(final List<Boolean> list) {
            double point = 0.0;
            for (int i = 0; i < list.size(); i++) {
                if (Boolean.TRUE.equals(list.get(i))) {
                    point += score.get(i);
                }
            }
            return point;
        }
    }

    public boolean isIncludeAllergy(final List<String> allergyNameList) {
        return allergyNameList.stream()
            .anyMatch(allergyName -> allergyName.contains(mainProperty)
                || allergyName.contains(
                animalProtein));
    }

    public boolean getBestScoreByArdCode(final Integer ardCode) {
        int temp = 0;
        int bestArdCode = 0;
        try {
            Class<? extends PetDoctorFeedDescription> aClass = petDoctorFeedDescription.getClass();
            Field[] fields = aClass.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                boolean annotationPresent = field.isAnnotationPresent(ArdCodeMakerAnnotation.class);
                if (annotationPresent) {
                    if ((int) field.get(petDoctorFeedDescription) == temp &&
                        Integer.valueOf(field.getName().substring(8, 11)).equals(ardCode)) {
                        temp = (int) field.get(petDoctorFeedDescription);
                        bestArdCode = ardCode;
                    }
                    if ((int) field.get(petDoctorFeedDescription) > temp) {
                        temp = (int) field.get(petDoctorFeedDescription);
                        bestArdCode = Integer.valueOf(field.getName().substring(8, 11));
                    }
                }
            }
        } catch (IllegalAccessException e) {
            logger.error("상품 고민에 대한 정보 설정에 실패했습니다. " + e.getMessage());
        }
        return bestArdCode == ardCode;
    }

    public String getFeedFormulation() {
        if (isWetFeed()) {
            return WET_FEED;
        }
        return FeedUnitSize.getUnitSizeName(getUnitSize());
    }
}
