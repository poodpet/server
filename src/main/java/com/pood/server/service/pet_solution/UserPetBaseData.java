package com.pood.server.service.pet_solution;

import com.pood.server.entity.meta.AllergyData;
import com.pood.server.entity.meta.GrainSize;
import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.meta.PetWorry;
import com.pood.server.entity.user.UserPet;
import com.pood.server.entity.user.UserPetGrainSize;
import com.pood.server.service.pet_solution.group.UserPetAiDiagnosisAiSolutionGroup;
import com.pood.server.service.pet_solution.group.UserPetAiDiagnosisAiSolutionGroup.UserPetAiDiagnosisStr;
import com.pood.server.service.pet_solution.group.UserPetAllergyAiSolutionGroup;
import com.pood.server.service.pet_solution.group.UserPetAllergyAiSolutionGroup.AllergyStr;
import com.pood.server.service.pet_solution.group.UserPetGrainSizeAiSolutionGroup;
import com.pood.server.service.pet_solution.group.UserPetGrainSizeAiSolutionGroup.GrainSizeStr;
import com.pood.server.util.FeedType;
import com.pood.server.util.PetCategoryName;
import java.util.List;
import java.util.function.Predicate;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserPetBaseData {

    private final UserPet userPet;
    private final UserPetGrainSizeAiSolutionGroup userPetGrainSizeAiSolutionGroup;
    private final UserPetAllergyAiSolutionGroup userPetAllergyAiSolutionGroup;
    private final UserPetAiDiagnosisAiSolutionGroup userPetAiDiagnosisAiSolutionGroup;
    private final Pet petInfo;
    private final UserPetCalorieCalc userPetCalc;

    private final List<PetWorry> worryList;

    public String getPetName() {
        return userPet.getPetName();
    }

    public float getPetCalorie() {
        return userPet.getPetCalorie(userPetCalc);
    }

    public boolean isPetTypeDog() {
        return userPet.isDog();
    }

    public boolean isPetTypeCat() {
        return userPet.isCat();
    }

    public int getPcIdx() {
        return userPet.getPcId();
    }

    public Integer getUserPetIdx() {
        return userPet.getIdx();
    }

    public String getPcSize() {
        return petInfo.getPcSize();
    }

    public boolean isPcBigSize() {
        return petInfo.isPcBigSize();
    }

    public boolean isPcMediumSize() {
        return petInfo.isMediumPcSize();
    }

    public boolean isPcSmallSize() {
        return petInfo.isPcSmallSize();
    }

    public boolean isNeutered() {
        return userPet.isNeutering();
    }

    public int getUserPetFeedTargetNum() {
        return petInfo.getPcSizeCode();
    }

    public long getAgeMonth() {
        return userPet.getAgeMonth();
    }

    public String getPetGrowthState() {
        return userPet.getPetGrowthState().getName();
    }

    public PetCategoryName getPetGrowthStateType() {
        return userPet.getPetGrowthState();
    }

    public String petAgeString() {
        return userPet.getPetGrowthState().getAge();
    }

    public boolean isDogAndIsNeutered() {
        return userPet.isPetTypeDogAndIsNeutering();
    }

    public boolean isDogAndNotNeutered() {
        return userPet.isPetTypeDogAndIsNotNeutering();
    }

    public boolean isPetTypeCatAndIsNotNeutering() {
        return userPet.isPetTypeCatAndIsNotNeutering();
    }

    public boolean isCatAndNeutered() {
        return userPet.isPetTypeCatAndIsNeutering();
    }

    public double getPetSurgeryScore() {
        if (isPetTypeDog()) {
            return AnalysisPointsType.getScore(AnalysisPointsType.PET_SURGERY_DOG);
        }

        return AnalysisPointsType.getScore(AnalysisPointsType.PET_SURGERY_CAT);
    }

    public double getPetMoistureSurgeryScore() {
        if (isDogAndIsNeutered()) {
            return AnalysisPointsType.getScore(AnalysisPointsType.PET_MOISTURE_DOG_SURGERY_TRUE);
        }

        if (isDogAndNotNeutered()) {
            return AnalysisPointsType.getScore(AnalysisPointsType.PET_MOISTURE_DOG_SURGERY_FALSE);
        }

        if (isCatAndNeutered()) {
            return AnalysisPointsType.getScore(AnalysisPointsType.PET_MOISTURE_CAT_SURGERY_TRUE);
        }

        return AnalysisPointsType.getScore(AnalysisPointsType.PET_MOISTURE_CAT_SURGERY_FALSE);
    }

    public double getPetAllergySurgeryScore() {
        if (isDogAndIsNeutered()) {
            return AnalysisPointsType.getScore(AnalysisPointsType.PET_ALLERGY_DOG_SURGERY_TRUE);
        }

        if (isDogAndNotNeutered()) {
            return AnalysisPointsType.getScore(AnalysisPointsType.PET_ALLERGY_DOG_SURGERY_FALSE);
        }

        if (isCatAndNeutered()) {
            return AnalysisPointsType.getScore(AnalysisPointsType.PET_ALLERGY_CAT_SURGERY_TRUE);
        }

        return AnalysisPointsType.getScore(AnalysisPointsType.PET_ALLERGY_CAT_SURGERY_FALSE);
    }

    public double getPetUnitSizeSurgeryScore() {
        if (isDogAndIsNeutered()) {
            return AnalysisPointsType.getScore(AnalysisPointsType.PET_UNIT_SIZE_DOG_SURGERY_TRUE);
        }

        if (isDogAndNotNeutered()) {
            return AnalysisPointsType.getScore(AnalysisPointsType.PET_UNIT_SIZE_DOG_SURGERY_FALSE);
        }

        if (isCatAndNeutered()) {
            return AnalysisPointsType.getScore(AnalysisPointsType.PET_UNIT_SIZE_CAT_SURGERY_TRUE);
        }

        return AnalysisPointsType.getScore(AnalysisPointsType.PET_UNIT_SIZE_CAT_SURGERY_FALSE);
    }

    public double getPetUnitSizeWebSurgeryScore() {
        if (isDogAndIsNeutered()) {
            return AnalysisPointsType.getScore(AnalysisPointsType.FEED_TARGET_SURGERY_TRUE);
        }

        if (isDogAndNotNeutered()) {
            return AnalysisPointsType.getScore(
                AnalysisPointsType.PET_UNIT_SIZE_WEB_DOG_SURGERY_FALSE);
        }

        if (isCatAndNeutered()) {
            return AnalysisPointsType.getScore(
                AnalysisPointsType.PET_UNIT_SIZE_WEB_CAT_SURGERY_TRUE);
        }

        return AnalysisPointsType.getScore(AnalysisPointsType.PET_UNIT_SIZE_WEB_CAT_SURGERY_FALSE);
    }

    public double getFeedTargetSurgeryScore() {
        if (isNeutered()) {
            return AnalysisPointsType.getScore(AnalysisPointsType.FEED_TARGET_SURGERY_TRUE);
        }

        return AnalysisPointsType.getScore(AnalysisPointsType.FEED_TARGET_SURGERY_FALSE);
    }

    public double getPetAgeSurgeryScore() {
        if (isDogAndIsNeutered()) {
            return AnalysisPointsType.getScore(AnalysisPointsType.PET_AGE_DOG_SURGERY_TRUE);
        }

        if (isDogAndNotNeutered()) {
            return AnalysisPointsType.getScore(AnalysisPointsType.PET_AGE_DOG_SURGERY_FALSE);
        }

        if (isCatAndNeutered()) {
            return AnalysisPointsType.getScore(AnalysisPointsType.PET_AGE_CAT_SURGERY_TRUE);
        }

        return AnalysisPointsType.getScore(AnalysisPointsType.PET_AGE_CAT_SURGERY_FALSE);
    }

    public double getPetUnitSizeScore(final double unitSize, final boolean isWetFeed) {
        //펫 등록할 때 선택한 선호하는 알갱이 정보
        //선호하는 알갱이는 소중대 3개가 있으며, 유저가 1개를 선택할 수도 있고, 3개를 선택할 수도 있다.
        //선택한 알갱이 크기 중에 한개라도 사료와 적합하면 배점 부가
        if (userPetGrainSizeAiSolutionGroup.isWetFeedCompatibility(isWetFeed)) {
            return getPetUnitSizeWebSurgeryScore();
        }

        if (userPetGrainSizeAiSolutionGroup.isUnitSizeCompatibility(unitSize)) {
            return getPetUnitSizeSurgeryScore();
        }

        return 0.0;
    }

    public double getPetAllergyScore(final String animalProtein, final String mainProperty) {
        if (userPetAllergyAiSolutionGroup.isPetAllergyCompatibility(animalProtein, mainProperty)) {
            return 0.0;
        }
        return getPetAllergySurgeryScore();
    }

    public List<Long> getPetWorryListIdxOrderByPriority() {
        return userPetAiDiagnosisAiSolutionGroup.getWorryIdxOrderByPriority();
    }

    public List<Integer> getArdGroupCodeOrderByPriority() {
        return userPetAiDiagnosisAiSolutionGroup.getArdGroupCodeOrderByPriority();
    }

    public List<GrainSizeStr> fitEachUnitSize(final double unitSize,
        final List<GrainSize> grainSizeList, final boolean isWetFeed) {
        return userPetGrainSizeAiSolutionGroup.isFitEachUnitSize(unitSize, grainSizeList,
            filterFeedType(isWetFeed));
    }

    public List<GrainSizeStr> fitEachALLUnitSize(final double unitSize,
        final List<GrainSize> grainSizeList) {
        return userPetGrainSizeAiSolutionGroup.isFitEachUnitSize(unitSize, grainSizeList,
            userPetGrainSize -> true);
    }

    private Predicate<UserPetGrainSize> filterFeedType(final boolean isWetFeed) {
        if (isWetFeed) {
            return UserPetGrainSize::isWetType;
        }
        return UserPetGrainSize::isNotWetType;
    }

    public List<AllergyStr> fitEachAllergy(final String animalProtein, final String mainProperty,
        final List<AllergyData> allergyDataList) {
        return userPetAllergyAiSolutionGroup.isFitPetAllergy(animalProtein, mainProperty,
            allergyDataList);
    }

    public List<UserPetAiDiagnosisStr> fitEachDiagnosis() {
        return userPetAiDiagnosisAiSolutionGroup.getAiDiagnosisStr(worryList, userPet.getPcId());
    }


    public float getNowPetWeight() {
        return userPet.getNowPetWeight();
    }

    public float catBasicWeight() {
        return userPet.catBasicWeight();
    }

    public float dogBasicWeight() {
        return userPet.dogBasicWeight();
    }

    public List<Long> getAiDiagnosisIdxOrderByPriority() {
        return userPetAiDiagnosisAiSolutionGroup.getWorryIdxOrderByPriority();
    }

    public List<Integer> getGrainSizeIdxList() {
        return userPetGrainSizeAiSolutionGroup.getGrainSizeIdx();
    }

    private enum AnalysisPointsType {
        FEED_TARGET_SURGERY_TRUE(20),             //feed_target 통과하고 중성화 수술 한 경우
        FEED_TARGET_SURGERY_FALSE(25),            //feed_target 통과하고 중성화 수술 안 한 경우
        PET_AGE_DOG_SURGERY_TRUE(20),             //펫 나이 체크 통과하고, 강아지이며, 중성화 한 경우
        PET_AGE_DOG_SURGERY_FALSE(25),            //펫 나이 체크 통과하고, 강아지이며, 중성화 안 한 경우
        PET_AGE_CAT_SURGERY_TRUE(25),             //펫 나이 체크 통과하고, 고양이이며, 중성화 한 경우
        PET_AGE_CAT_SURGERY_FALSE(34),            //펫 나이 체크 통과하고, 고양이이며, 중성화 안 한 경우
        PET_UNIT_SIZE_DOG_SURGERY_TRUE(20),       //강아지가 선호하는 알갱이 크기, 중성화 한 경우
        PET_UNIT_SIZE_DOG_SURGERY_FALSE(25),      //강아지가 선호하는 알갱이 크기, 중성화 안 한 경우
        PET_UNIT_SIZE_CAT_SURGERY_TRUE(25),       //고양이가 선호하는 알갱이 크기, 중성화 한 경우
        PET_UNIT_SIZE_CAT_SURGERY_FALSE(33),      //고양이가 선호하는 알갱이 크기, 중성화 안 한 경우
        PET_UNIT_SIZE_WEB_DOG_SURGERY_TRUE(18),   //강아지, 습식, 중성화 한 경우
        PET_UNIT_SIZE_WEB_DOG_SURGERY_FALSE(18),  //강아지, 습식, 중성화 안 한 경우
        PET_UNIT_SIZE_WEB_CAT_SURGERY_TRUE(22),   //고양이, 습식, 중성화 한 경우
        PET_UNIT_SIZE_WEB_CAT_SURGERY_FALSE(27),  //고양이, 습식, 중성화 안 한 경우
        PET_ALLERGY_DOG_SURGERY_TRUE(20),         //알러지에 도움이 되는 제품이며, 강아지이고, 중성화 수술 한 경우
        PET_ALLERGY_DOG_SURGERY_FALSE(25),        //알러지에 도움이 되는 제품이며, 강아지이고, 중성화 수술 안 한 경우
        PET_ALLERGY_CAT_SURGERY_TRUE(25),         //알러지에 도움이 되는 제품이며, 고양이이고, 중성화 수술 한 경우
        PET_ALLERGY_CAT_SURGERY_FALSE(33),        //알러지에 도움이 되는 제품이며, 고양이이고, 중성화 수술 안 한 경우
        PET_SURGERY_DOG(20),                      //중성화수술을 한 강아지인 경우
        PET_SURGERY_CAT(25),                      //중성화수술을 한 고양이인 경우
        PET_MOISTURE_DOG_SURGERY_TRUE(20),        //수분공급분석, 강아지이며 중성화 수술을 한 경우
        PET_MOISTURE_DOG_SURGERY_FALSE(25),       //수분공급분석, 강아지이며 중성화 수술을 안 한 경우
        PET_MOISTURE_CAT_SURGERY_TRUE(25),        //수분공급분석, 고양이이며 중성화 수술을 한 경우
        PET_MOISTURE_CAT_SURGERY_FALSE(33);       //수분공급분석, 강아지이며 중성화 수술을 안 한 경우

        private final int score;

        static int getScore(AnalysisPointsType analysisPointsType) {
            return analysisPointsType.score;
        }

        AnalysisPointsType(int score) {
            this.score = score;
        }

    }

    public List<String> getAllergyNameList() {
        return userPetAllergyAiSolutionGroup.getAllergyNameList();
    }

    public List<Integer> getAllergyIdxList() {
        return userPetAllergyAiSolutionGroup.getAllergyIdxList();
    }


    public FeedType getPetFeedType() {
        long month = getAgeMonth();

        if (month < 12 && petInfo.isPcBigSize()) {
            return FeedType.PUPPY_ADULT_EXCEPT_SENIOR;
        }
        if (month < 12) {
            return FeedType.PUPPY;
        }
        if (month < 84) {
            return FeedType.ADULT;
        }
        return FeedType.SENIOR;
    }

    public boolean isSeniorCat() {
        return PetCategoryName.OLD_CAT.equals(getPetGrowthStateType());
    }

    public boolean isAdultCat() {
        return PetCategoryName.ADULT_CAT.equals(getPetGrowthStateType());
    }

    public boolean isKitten() {
        return PetCategoryName.KITTEN.equals(getPetGrowthStateType());
    }

    public boolean isPcBigSizeAndPuppy() {
        return PetCategoryName.PUPPY.equals(getPetGrowthStateType()) && isPcBigSize();
    }

    public boolean isPcBigSizeAndAdultDog() {
        return PetCategoryName.ADULT_DOG.equals(getPetGrowthStateType()) && isPcBigSize();
    }

    public boolean isPcBigSizeAndOldDog() {
        return PetCategoryName.OLD_DOG.equals(getPetGrowthStateType()) && isPcBigSize();
    }

    public boolean isPcMediumSizeAndPuppy() {
        return PetCategoryName.PUPPY.equals(getPetGrowthStateType()) && isPcMediumSize();
    }

    public boolean isPcMediumSizeAndAdultDog() {
        return PetCategoryName.ADULT_DOG.equals(getPetGrowthStateType()) && isPcMediumSize();
    }

    public boolean isPcMediumSizeAndOldDog() {
        return PetCategoryName.OLD_DOG.equals(getPetGrowthStateType()) && isPcMediumSize();
    }

    public boolean isPcSmallSizeAndPuppy() {
        return PetCategoryName.PUPPY.equals(getPetGrowthStateType()) && isPcSmallSize();
    }

    public boolean isPcSmallSizeAndAdultDog() {
        return PetCategoryName.ADULT_DOG.equals(getPetGrowthStateType()) && isPcSmallSize();
    }

    public boolean isPcSmallSizeAndOldDog() {
        return PetCategoryName.OLD_DOG.equals(getPetGrowthStateType()) && isPcSmallSize();
    }

    public void petOwnerIsCorrectValid(final Integer userIdx) {
        userPet.petOwnerIsCorrectValid(userIdx);
    }
}
