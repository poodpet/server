package com.pood.server.service.pet_solution.intake;

import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.UserPetBaseData;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class UserPetIntake {

    protected static final float KG_1 = 1000f;

    protected final UserPetBaseData userPetBaseData;
    protected final ProductBaseData productBaseData;

    public float divideCupCalorieAmount() {
        return getPetCalorie() / getCupCalorie();
    }

    public int getCupCalorie() {
        return (int) ((productBaseData.getCalorie() / KG_1) * productBaseData.getCupWeight());
    }

    public float calcEatFoodGrams() {
        return getPetCalorie() / (productBaseData.getCalorie() / KG_1);
    }

    public abstract float getPetCalorie();

}
