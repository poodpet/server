package com.pood.server.service.pet_solution.intake.store;

import com.pood.server.service.pet_solution.intake.ProductStore;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class SuppliesStore implements ProductStore {
    @Override
    public double dailyTakesToEat() {
        return 0;
    }

    @Override
    public int longItTakesToEat() {
        return 0;
    }

    @Override
    public String cupType() {
        return null;
    }

    @Override
    public String cupSubType() {
        return null;
    }
}
