package com.pood.server.service.pet_solution;

import com.pood.server.entity.user.UserPet;

public interface UserPetCalorieCalc {

    boolean support(final int pcIdx);

    float getPetWeight(final UserPet userPet);

    float getPetCalorie(final long month,final float  petWeight ,final float weight);

}
