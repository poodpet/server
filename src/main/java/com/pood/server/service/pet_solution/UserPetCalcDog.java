package com.pood.server.service.pet_solution;

import com.pood.server.entity.user.UserPet;
import java.time.Month;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class UserPetCalcDog implements UserPetCalorieCalc {

    private static final int DOG = 1;
    private static final int MONTH_ZERO = 0;

    @Override
    public boolean support(final int pcIdx) {
        return DOG == pcIdx;
    }

    @Override
    public float getPetWeight(final UserPet userPet) {
        long month = userPet.getAgeMonth();

        if (List.of(MONTH_ZERO, Month.JANUARY.getValue(), Month.FEBRUARY.getValue(),
            Month.MARCH.getValue(), Month.APRIL.getValue()).contains((int) month)) {
            return 210f;
        }
        if (List.of(Month.MAY.getValue(), Month.JUNE.getValue(), Month.JULY.getValue())
            .contains((int) month)) {
            return 170f;
        }
        if (List.of(Month.AUGUST.getValue(), Month.SEPTEMBER.getValue()).contains((int) month)) {
            return 140f;
        }
        if (List.of(Month.OCTOBER.getValue(), Month.NOVEMBER.getValue()).contains((int) month)) {
            return 122.5f;
        }
        if (month >= 12 && month < 96) {
            //1: 남아, 2: 여아, 3 : 남아 중성화, 4: 여아 중성화
            if (List.of(1, 2).contains(userPet.getPetGender())) {
                return 112;
            }
        }
        return 98f;
    }

    @Override
    public float getPetCalorie(final long month, final float petWeight, final float weight) {
        return (float) (weight * (Math.pow(petWeight, 0.75f)));
    }

}

