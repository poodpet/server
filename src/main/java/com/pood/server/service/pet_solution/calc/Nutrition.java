package com.pood.server.service.pet_solution.calc;

import com.pood.server.controller.response.solution.NutritionWordResponse;
import com.pood.server.dto.meta.pet.NutritionWord;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.ProductType;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModel;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Nutrition implements Suitable, WordExtract {

    private final ProductBaseData productBaseData;
    private final List<NutritionDetailModel> list;

    @Override
    public double getScore() {
        return list.stream()
            .filter(Objects::nonNull)
            .mapToDouble(NutritionDetailModel::isEnoughThenCalcScore)
            .sum();
    }

    @Override
    public double calcProductTypeScore() {
        return (getScore() / 100) * getProductTypeScorePercent();
    }

    @Override
    public int getProductTypeScorePercent() {
        if (productBaseData.eqProductType(ProductType.FEED)) {
            return 30;
        }
        return 0;
    }

    @Override
    public String makeScoreKey() {
        return "nutritionScore";
    }

    @Override
    public Object fitWord() {
        return new NutritionWordResponse(list.stream()
            .map(model -> new NutritionWord(model.getTitle(), model.getSuitable(),
                model.getBaseValue()))
            .collect(Collectors.toList()));
    }

    public List<NutritionWord> toWordList() {
        return list.stream()
            .map(model -> new NutritionWord(
                model.getTitle(),
                model.getSuitable(),
                model.getBaseValue())
            )
            .collect(Collectors.toList());
    }
}
