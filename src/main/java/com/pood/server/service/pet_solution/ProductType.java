package com.pood.server.service.pet_solution;

public enum ProductType {
    FEED(0, "feed"),
    SNACK(1, "snack"),
    NUTRIENTS(2, "nutrients"),
    SUPPLIES(3, "supplies"),
    SAMPLE(14, "sample");

    private final int idx;
    private final String name;

    ProductType(int idx, String name) {
        this.idx = idx;
        this.name = name;
    }

    public static ProductType getProductTypeByIdx(final int idx) {
        for (ProductType productType : values()) {
            if (productType.idx == idx) {
                return productType;
            }
        }
        return null;
    }

    public static ProductType getProductTypeByName(final String name) {
        for (ProductType productType : values()) {
            if (productType.name.equals(name)) {
                return productType;
            }
        }
        return null;
    }


    public int getIdx() {
        return idx;
    }

    public String getName() {
        return name;
    }
}
