package com.pood.server.service.pet_solution.nutrition.dog;

import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.SuitableCalc;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModel;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModelLegacy;
import com.pood.server.service.pet_solution.nutrition.NutritionDog;
import com.pood.server.service.pet_solution.nutrition.NutritionUnit;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PrProteinDog implements NutritionDog {

    private static final int TYPE_TEN = 10;
    private static final String TITLE = "조단백";

    private final UserPetBaseData userPetBaseData;
    private final ProductBaseData productBaseData;
    private final AafcoNrc aafcoNrc;

    @Override
    public NutritionDetailModelLegacy create() {

        float petCalorie = userPetBaseData.getPetCalorie();
        float minimumValue = (float) (aafcoNrc.getPrProtein() * (petCalorie / 1000f));
        float productValue = (float) ((TYPE_TEN * productBaseData.getFeedPrProtein()) * (1
            / productBaseData.getCalorie().floatValue()) * petCalorie);

        return new NutritionDetailModelLegacy(
            TITLE, minimumValue >= productValue,
            String.format(NutritionUnit.NEED_G.getUnit(), minimumValue),
            "",
            String.format(NutritionUnit.INCLUDE_G.getUnit(), productValue),
            16f, true
        );
    }

    @Override
    public NutritionDetailModel createModel() {
        float minValue = aafcoNrc.getPrProtein().floatValue();
        final double baseValue = productBaseData.getFeedPrProtein();
        float calcValue =
            (float) ((TYPE_TEN * baseValue) * (1 / productBaseData.getCalorie().floatValue()))
                * 1000f;

        if (baseValue <= 0.0) {
            return null;
        }

        return new NutritionDetailModel(
            TITLE, SuitableCalc.makeMessage(minValue, calcValue),
            NutritionUnit.unitConvert(NutritionUnit.PERCENT, baseValue),
            SuitableCalc.isMinEnough(minValue, calcValue), 16f);
    }

}
