package com.pood.server.service.pet_solution.nutrition;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
@Deprecated
public class NutritionDetailModelLegacy {

    private final String title;
    private final boolean enough;
    private final String minimumNeed;
    private final String maximumNeed;
    private final String include;
    private final float score;
    private final boolean isBasicNutrition;

    public float getScoreByEnoughTrue() {
        return enough ? score : 0;
    }
}

