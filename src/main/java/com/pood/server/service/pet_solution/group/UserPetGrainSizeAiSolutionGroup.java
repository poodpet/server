package com.pood.server.service.pet_solution.group;

import com.pood.server.entity.meta.GrainSize;
import com.pood.server.entity.user.UserPetGrainSize;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
public class UserPetGrainSizeAiSolutionGroup {

    private static final int WET_FEED = 3;

    private final List<UserPetGrainSize> userPetGrainSizeList;

    public UserPetGrainSizeAiSolutionGroup(final List<UserPetGrainSize> userPetGrainSizeList) {
        emptyCheck(userPetGrainSizeList);
        this.userPetGrainSizeList = userPetGrainSizeList;
    }

    private void emptyCheck(final List<UserPetGrainSize> userPetGrainSizeList) {
        if (Objects.requireNonNull(userPetGrainSizeList).isEmpty()) {
            throw new NullPointerException("유저 반려동물의 선호 알갱이 사이즈가 빈값 입니다.");
        }
    }

    public boolean isUnitSizeCompatibility(double unitSize) {
        for (UserPetGrainSize userPetGrainSize : userPetGrainSizeList) {
            if (unitSize >= userPetGrainSize.getSizeMin()
                && unitSize <= userPetGrainSize.getSizeMax()) {
                return true;
            }
        }
        return false;
    }

    public List<GrainSizeStr> isFitEachUnitSize(final double unitSize,
        final List<GrainSize> grainSizeList, final Predicate<UserPetGrainSize> userGrainFilter) {

        final List<GrainSizeStr> fitList = new ArrayList<>();

        for (UserPetGrainSize userPetGrainSize : getUserPetGrainSizeFilter(userGrainFilter)) {

            final String imgUrl = getGrainSizeMatchingUrl(grainSizeList,
                userPetGrainSize.getGrainSizeIdx());

            fitList.add(getIsFitGrainSizeStr(unitSize, userPetGrainSize, imgUrl));
        }
        return fitList;
    }

    private GrainSizeStr getIsFitGrainSizeStr(final double unitSize,
        final UserPetGrainSize userPetGrainSize, final String imgUrl) {

        if(userPetGrainSize.getGrainSizeIdx() == WET_FEED && unitSize <= 0){
            return GrainSizeStr.fit(userPetGrainSize.getName(), imgUrl);
        }

        if (unitSize >= userPetGrainSize.getSizeMin() && unitSize <= userPetGrainSize.getSizeMax()) {
            return GrainSizeStr.fit(userPetGrainSize.getName(), imgUrl);
        }

        return GrainSizeStr.notFit(userPetGrainSize.getName(), imgUrl);
    }

    private static String getGrainSizeMatchingUrl(final List<GrainSize> grainSizeList,
        final Integer userPetGrainSizeIdx) {

        return grainSizeList.stream()
            .filter(grainSize -> grainSize.getIdx().equals(userPetGrainSizeIdx))
            .findFirst()
            .map(GrainSize::getUrl)
            .orElse(null);
    }

    private List<UserPetGrainSize> getUserPetGrainSizeFilter(
        final Predicate<UserPetGrainSize> filter) {
        return getUserPetGrainSizeList().stream().filter(filter).collect(Collectors.toList());
    }

    public boolean isWetFeedCompatibility(boolean isWetFeed) {
        long count = userPetGrainSizeList.stream()
            .filter(userPetGrainSize -> userPetGrainSize.getGrainSizeIdx() == 3).count();
        return count >= 1 && isWetFeed;
    }

    public List<Integer> getGrainSizeIdx() {
        return userPetGrainSizeList.stream()
            .map(UserPetGrainSize::getGrainSizeIdx)
            .collect(Collectors.toList());
    }

    @Getter
    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    public static class GrainSizeStr {

        private final String grainSizeName;
        private final String fitString;
        private final String imgUrl;

        public static GrainSizeStr fit(final String grainSizeName, final String imgUrl) {
            return new GrainSizeStr(grainSizeName, "해당", imgUrl);
        }

        public static GrainSizeStr notFit(final String grainSizeName, final String imgUrl) {
            return new GrainSizeStr(grainSizeName, null, imgUrl);
        }
    }
}
