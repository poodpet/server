package com.pood.server.service.pet_solution.group;

import com.pood.server.entity.meta.PetWorry;
import com.pood.server.entity.meta.mapper.AiRecommendDiagnosisMapper;
import com.pood.server.entity.user.UserPetAiDiagnosis;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;

public class UserPetAiDiagnosisAiSolutionGroup {

    private final List<UserPetAiDiagnosis> userPetAiDiagnosisList;

    public UserPetAiDiagnosisAiSolutionGroup(final List<UserPetAiDiagnosis> userPetAiDiagnosisList) {
        this.userPetAiDiagnosisList = userPetAiDiagnosisList;
    }

    public List<Long> getWorryIdxOrderByPriority() {
        return userPetAiDiagnosisList.stream()
            .sorted(Comparator.comparingInt(UserPetAiDiagnosis::getPriority))
            .map(UserPetAiDiagnosis::getPetWorryIdx)
            .collect(Collectors.toList());
    }
    
    public List<Integer> getArdGroupCodeOrderByPriority() {
        return userPetAiDiagnosisList.stream()
            .sorted(Comparator.comparingInt(UserPetAiDiagnosis::getPriority))
            .map(UserPetAiDiagnosis::getArdGroupCode)
            .collect(Collectors.toList());
    }

    public List<UserPetAiDiagnosisStr> getAiDiagnosisStr(final List<PetWorry> worryList, final int pcIdx) {
        userPetAiDiagnosisList.sort(Comparator.comparingInt(UserPetAiDiagnosis::getPriority));

        List<UserPetAiDiagnosisStr> list = new ArrayList<>();

        for (UserPetAiDiagnosis userPetAiDiagnosis : userPetAiDiagnosisList) {
            for (PetWorry petWorry : worryList) {
                if (userPetAiDiagnosis.getPetWorryIdx().equals(petWorry.getId())) {
                    list.add(new UserPetAiDiagnosisStr(userPetAiDiagnosis.getPetWorryIdx(), petWorry.getName(),
                        imgUrlByPcIdx(petWorry, pcIdx),userPetAiDiagnosis.getArdGroupCode()));
                }
            }
        }

        return list;
    }

    private String imgUrlByPcIdx(final PetWorry petWorry, final int pcIdx) {
        if (pcIdx == 1) {
            return petWorry.getDogUrl();
        }

        return petWorry.getCatUrl();
    }

    public UserPetAiDiagnosisAiSolutionGroup setAiRecommendDiagnosis(final long worryIdx, final AiRecommendDiagnosisMapper aiRecommendDiagnosis) {
        for (UserPetAiDiagnosis userPetAiDiagnosis : getList()) {
            if(userPetAiDiagnosis.eqWorryIdx(worryIdx)){
                userPetAiDiagnosis.setArdGroupCode(aiRecommendDiagnosis.getArdGroupCode());
            }
        }
        return new UserPetAiDiagnosisAiSolutionGroup(getList());
    }

    public List<UserPetAiDiagnosis> getList(){
        return userPetAiDiagnosisList;
    }

    @Getter
    public static class UserPetAiDiagnosisStr {

        private final long worryIdx;
        private String worryName;
        private String imgUrl;
        @Setter
        private String fitName;

        @Setter
        private Integer ardGroupCode;

        public UserPetAiDiagnosisStr(final long worryIdx, final String worryName, final String imgUrl, final Integer ardGroupCode) {
            this.worryIdx = worryIdx;
            this.worryName = worryName;
            this.imgUrl = imgUrl;
            this.ardGroupCode =  ardGroupCode;
        }
    }


}
