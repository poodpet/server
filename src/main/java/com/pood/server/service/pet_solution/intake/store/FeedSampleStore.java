package com.pood.server.service.pet_solution.intake.store;

import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.SuitableCalc;
import com.pood.server.service.pet_solution.intake.ProductStore;
import com.pood.server.service.pet_solution.intake.UserPetIntake;

public class FeedSampleStore implements ProductStore {

    private final ProductBaseData productBaseData;
    private final float feedGram;

    public FeedSampleStore(final ProductBaseData productBaseData, final UserPetIntake userPetIntake) {
        this.productBaseData = productBaseData;
        this.feedGram = userPetIntake.divideCupCalorieAmount();
    }

    @Override
    public String cupType() {
        return productBaseData.getPackageType().getCupType();
    }

    @Override
    public String cupSubType() {
        return productBaseData.getPackageType().getCupSubType();
    }

    //하루 사료 섭취량
    @Override
    public double dailyTakesToEat() {
        return SuitableCalc.calculateIntakeRange((float) Math.round(feedGram * 10) / 10);
    }

    //다 먹는데까지의 걸리는 기간
    @Override
    public int longItTakesToEat() {
        return productBaseData.intakeFeedPeriod(dailyTakesToEat());
    }

}
