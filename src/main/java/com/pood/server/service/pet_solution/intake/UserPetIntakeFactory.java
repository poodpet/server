package com.pood.server.service.pet_solution.intake;

import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.service.pet_solution.intake.pet.UserPetIntakeCat;
import com.pood.server.service.pet_solution.intake.pet.UserPetIntakeDog;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserPetIntakeFactory {

    private final UserPetBaseData userPetBaseData;
    private final ProductBaseData productBaseData;

    public UserPetIntake make() {
        if (userPetBaseData.getPcIdx() == 2) {
            return new UserPetIntakeCat(userPetBaseData, productBaseData);
        }

        return new UserPetIntakeDog(userPetBaseData, productBaseData);
    }

}
