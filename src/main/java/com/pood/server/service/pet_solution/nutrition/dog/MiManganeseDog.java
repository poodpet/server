package com.pood.server.service.pet_solution.nutrition.dog;

import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.SuitableCalc;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModel;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModelLegacy;
import com.pood.server.service.pet_solution.nutrition.NutritionDog;
import com.pood.server.service.pet_solution.nutrition.NutritionUnit;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MiManganeseDog implements NutritionDog {

    private static final int TYPE_ONE = 1;
    private static final String TITLE = "망간";

    private final UserPetBaseData userPetBaseData;
    private final ProductBaseData productBaseData;
    private final AafcoNrc aafcoNrc;

    @Override
    public NutritionDetailModelLegacy create() {
        float petCalorie = userPetBaseData.getPetCalorie();
        float limitMin = (float) (aafcoNrc.getMiManganese() * (petCalorie / 1000f));
        float contain = (float) ((TYPE_ONE * productBaseData.getFeedMiManganese()) * (1
            / productBaseData.getCalorie().floatValue()) * petCalorie);

        return new NutritionDetailModelLegacy(
            TITLE, limitMin >= contain,
            String.format(NutritionUnit.NEED_MG.getUnit(), limitMin),
            "",
            String.format(NutritionUnit.INCLUDE_MG.getUnit(), contain),
            1f, false
        );
    }

    @Override
    public NutritionDetailModel createModel() {
        float minValue = aafcoNrc.getMiManganese().floatValue();
        final double baseValue = productBaseData.getFeedMiManganese();
        float calcValue = (float) ((TYPE_ONE * baseValue) * (1
            / productBaseData.getCalorie().floatValue())) * 1000f;

        if (baseValue <= 0.0) {
            return null;
        }

        return new NutritionDetailModel(TITLE, SuitableCalc.makeMessage(minValue, calcValue),
            NutritionUnit.unitConvert(NutritionUnit.MILLIGRAM, baseValue),
            SuitableCalc.isMinEnough(minValue, calcValue),
            1f);
    }

}
