package com.pood.server.service.pet_solution.nutrition.dog;

import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.SuitableCalc;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModel;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModelLegacy;
import com.pood.server.service.pet_solution.nutrition.NutritionDog;
import com.pood.server.service.pet_solution.nutrition.NutritionUnit;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MiIodineDog implements NutritionDog {

    private static final int TYPE_ONE = 1;
    private static final String TITLE = "요오드";
    private static final float AAFCO_MAX_MIIODINE = 2.75f;

    private final UserPetBaseData userPetBaseData;
    private final ProductBaseData productBaseData;
    private final AafcoNrc aafcoNrc;

    @Override
    public NutritionDetailModelLegacy create() {

        float petCalorie = userPetBaseData.getPetCalorie();
        float limitMin = (float) (aafcoNrc.getMiIodine() * (petCalorie / 1000f));
        float limitMax = AAFCO_MAX_MIIODINE * (petCalorie / 1000f);
        float contain = (float) ((TYPE_ONE * productBaseData.getFeedMiIodine()) * (1
            / productBaseData.getCalorie().floatValue()) * petCalorie);

        return new NutritionDetailModelLegacy(
            TITLE, limitMin >= contain,
            String.format(NutritionUnit.NEED_MG.getUnit(), limitMin),
            String.format(NutritionUnit.NEED_MG_MAX.getUnit(), limitMax),
            String.format(NutritionUnit.INCLUDE_MG.getUnit(), contain),
            1f, false
        );
    }

    @Override
    public NutritionDetailModel createModel() {
        float minValue = aafcoNrc.getMiIodine().floatValue();
        float maxValue = AAFCO_MAX_MIIODINE;
        final double baseValue = productBaseData.getFeedMiIodine();
        float calcValue = (float) ((TYPE_ONE * baseValue) * (1
            / productBaseData.getCalorie().floatValue())) * 1000f;

        if (baseValue <= 0.0) {
            return null;
        }

        return new NutritionDetailModel(TITLE,
            SuitableCalc.makeMinMaxMessage(minValue, maxValue, calcValue),
            NutritionUnit.unitConvert(NutritionUnit.MILLIGRAM, baseValue),
            SuitableCalc.isBothEnough(minValue, maxValue, calcValue),
            1f);
    }

}
