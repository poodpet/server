package com.pood.server.service.pet_solution.nutrition.common;

import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.SuitableCalc;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.service.pet_solution.nutrition.NutritionCat;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModel;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModelLegacy;
import com.pood.server.service.pet_solution.nutrition.NutritionDog;
import com.pood.server.service.pet_solution.nutrition.NutritionUnit;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AmLeucine implements NutritionCat, NutritionDog {

    private static final int TYPE_TEN = 10;
    private static final String TITLE = "류신";

    private final UserPetBaseData userPetBaseData;
    private final ProductBaseData productBaseData;
    private final AafcoNrc aafcoNrc;

    @Override
    public NutritionDetailModelLegacy create() {

        float petCalorie = userPetBaseData.getPetCalorie();
        float limitMin = (float) (aafcoNrc.getAmLeucine() * (petCalorie / 1000f));
        float contain = (float) ((TYPE_TEN * productBaseData.getFeedAmLeucine()) * (1
            / productBaseData.getCalorie().floatValue()) * petCalorie);

        return new NutritionDetailModelLegacy(
            TITLE, limitMin >= contain,
            String.format(NutritionUnit.NEED_MG.getUnit(), limitMin),
            "",
            String.format(NutritionUnit.INCLUDE_MG.getUnit(), contain),
            0.1f, false
        );
    }

    @Override
    public NutritionDetailModel createModel() {
        float minValue = aafcoNrc.getAmLeucine().floatValue();
        final double baseValue = productBaseData.getFeedAmLeucine();
        float calcValue = (float) ((TYPE_TEN * baseValue) *
            (1 / productBaseData.getCalorie().floatValue())) * 1000f;

        if (baseValue <= 0.0) {
            return null;
        }

        return new NutritionDetailModel(TITLE, SuitableCalc.makeMessage(minValue, calcValue),
            NutritionUnit.unitConvert(NutritionUnit.PERCENT, baseValue),
            SuitableCalc.isMinEnough(minValue, calcValue),
            0.1f);
    }

}
