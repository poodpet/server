package com.pood.server.service.pet_solution.nutrition.cat;

import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.SuitableCalc;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.service.pet_solution.nutrition.NutritionCat;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModel;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModelLegacy;
import com.pood.server.service.pet_solution.nutrition.NutritionUnit;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AmMethionineCat implements NutritionCat {

    private static final int TYPE_TEN = 10;
    private static final String TITLE = "메티오닌";
    private static final float AAFCO_MAX_AMMETHIONINE = 3.75f;

    private final UserPetBaseData userPetBaseData;
    private final ProductBaseData productBaseData;
    private final AafcoNrc aafcoNrc;

    @Override
    public NutritionDetailModelLegacy create() {

        if (aafcoNrc.getAmMethionine() > 0 && productBaseData.getFeedAmMethionine() > 0) {
            return null;
        }

        float petCalorie = userPetBaseData.getPetCalorie();
        float limitMin = (float) (aafcoNrc.getAmMethionine() * (petCalorie / 1000f));
        float limitMax = AAFCO_MAX_AMMETHIONINE * (petCalorie / 1000f);
        float contain = (float) ((TYPE_TEN * productBaseData.getFeedAmMethionine()) *
            (1 / productBaseData.getCalorie().floatValue()) * petCalorie);

        return new NutritionDetailModelLegacy(
            TITLE, limitMin >= contain,
            String.format(NutritionUnit.NEED_MG.getUnit(), limitMin),
            String.format(NutritionUnit.NEED_MG_MAX.getUnit(), limitMax),
            String.format(NutritionUnit.INCLUDE_MG.getUnit(), contain),
            0.4f, false
        );
    }

    @Override
    public NutritionDetailModel createModel() {
        if (productBaseData.getFeedAmMethionine() <= 0) {
            return null;
        }

        float minValue = aafcoNrc.getAmMethionine().floatValue();
        float maxValue = AAFCO_MAX_AMMETHIONINE;
        final double baseValue = productBaseData.getFeedAmMethionine();
        float calcValue = (float) ((TYPE_TEN * baseValue) *
            (1 / productBaseData.getCalorie().floatValue())) * 1000f;

        if (baseValue <= 0.0) {
            return null;
        }

        return new NutritionDetailModel(TITLE,
            SuitableCalc.makeMinMaxMessage(minValue, maxValue, calcValue),
            NutritionUnit.unitConvert(NutritionUnit.PERCENT, baseValue),
            SuitableCalc.isBothEnough(minValue, maxValue, calcValue), 0.4f);
    }

}
