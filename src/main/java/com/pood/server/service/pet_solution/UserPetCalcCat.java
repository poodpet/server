package com.pood.server.service.pet_solution;

import com.pood.server.entity.user.UserPet;
import java.time.Month;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class UserPetCalcCat implements UserPetCalorieCalc {

    private static final int CAT = 2;
    private static final int MONTH_ZERO = 0;
    @Override
    public boolean support(final int pcIdx) {
        return CAT == pcIdx;
    }

    @Override
    public float getPetWeight(final UserPet userPet) {
        long month = userPet.getAgeMonth();

        if (List.of(MONTH_ZERO, Month.JANUARY.getValue()).contains((int) month)) {
            return 240f;
        }
        if (Month.FEBRUARY.getValue() == (int) month) {
            return 210f;
        }
        if (Month.MARCH.getValue() == (int) month) {
            return 200f;
        }
        if (Month.APRIL.getValue() == (int) month) {
            return 175f;
        }
        if (Month.MAY.getValue() == (int) month) {
            return 145f;
        }
        if (Month.JUNE.getValue() == (int) month) {
            return 135f;
        }
        if (Month.JULY.getValue() == (int) month) {
            return 120f;
        }
        if (Month.AUGUST.getValue() == (int) month) {
            return 110f;
        }
        if (Month.SEPTEMBER.getValue() == (int) month) {
            return 100f;
        }
        if (Month.OCTOBER.getValue() == (int) month) {
            return 95f;
        }
        if (Month.NOVEMBER.getValue() == (int) month) {
            return 90f;
        }
        if (Month.DECEMBER.getValue() == (int) month) {
            return 85f;
        }
        //1: 남아, 2: 여아, 3 : 남아 중성화, 4: 여아 중성화
        if (List.of(1, 2).contains(userPet.getPetGender())) {
            return 77.6f;
        }

        return 62f;
    }

    @Override
    public float getPetCalorie(final long month, final float petWeight, final float weight) {
        if (month >= 13) {
            return (float) (weight * (Math.pow(petWeight, 0.711f)));
        }
        return (float) (weight * (petWeight));
    }

}
