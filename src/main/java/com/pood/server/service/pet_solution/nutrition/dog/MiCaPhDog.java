package com.pood.server.service.pet_solution.nutrition.dog;

import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.SuitableCalc;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModel;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModelLegacy;
import com.pood.server.service.pet_solution.nutrition.NutritionDog;
import com.pood.server.service.pet_solution.nutrition.NutritionUnit;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MiCaPhDog implements NutritionDog {

    private static final String TITLE = "칼슘:인";
    private static final float AAFCO_MAX_MICAPH = 2f;

    private final ProductBaseData productBaseData;
    private final AafcoNrc aafcoNrc;

    @Override
    public NutritionDetailModelLegacy create() {
        if (aafcoNrc.getMiCaPh() > 0 && productBaseData.getFeedMiCaPh() > 0) {
            return null;
        }

        float limitMin = aafcoNrc.getMiCaPh().floatValue();
        float limitMax = AAFCO_MAX_MICAPH;
        float contain = productBaseData.getFeedMiCaPh().floatValue();

        return new NutritionDetailModelLegacy(
            TITLE, limitMin >= contain,
            String.format(NutritionUnit.PERCENT_N.getUnit(), limitMin),
            String.format(NutritionUnit.PERCENT_N.getUnit(), limitMax),
            String.format(NutritionUnit.PERCENT_N.getUnit(), contain),
            16f, true
        );
    }

    @Override
    public NutritionDetailModel createModel() {
        if (productBaseData.getFeedMiCaPh() <= 0) {
            return null;
        }

        float minValue = aafcoNrc.getMiCaPh().floatValue();
        float maxValue = AAFCO_MAX_MICAPH;
        float baseValue = productBaseData.getFeedMiCaPh().floatValue();

        if (baseValue <= 0.0) {
            return null;
        }

        return new NutritionDetailModel(TITLE,
            SuitableCalc.makeMinMaxMessage(minValue, maxValue, baseValue),
            NutritionUnit.unitConvert(NutritionUnit.PERCENT_N, baseValue),
            SuitableCalc.isBothEnough(minValue, maxValue, baseValue), 16f);
    }

}
