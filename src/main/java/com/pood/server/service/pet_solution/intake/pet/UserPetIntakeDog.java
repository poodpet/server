package com.pood.server.service.pet_solution.intake.pet;

import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.service.pet_solution.intake.UserPetIntake;

public class UserPetIntakeDog extends UserPetIntake {

    public UserPetIntakeDog(final UserPetBaseData userPetBaseData, final ProductBaseData productBaseData) {
        super(userPetBaseData, productBaseData);
    }

    // 반려동물의 하루치 칼로리
    @Override
    public float getPetCalorie() {
        return (float) (userPetBaseData.dogBasicWeight() * (Math.pow(userPetBaseData.getNowPetWeight(), 0.75f)));
    }

}
