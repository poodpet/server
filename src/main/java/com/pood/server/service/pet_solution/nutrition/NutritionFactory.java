package com.pood.server.service.pet_solution.nutrition;

import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.service.pet_solution.nutrition.cat.AmMetCysCat;
import com.pood.server.service.pet_solution.nutrition.cat.AmMethionineCat;
import com.pood.server.service.pet_solution.nutrition.cat.AmPheTyrCat;
import com.pood.server.service.pet_solution.nutrition.cat.AmTryptophanCat;
import com.pood.server.service.pet_solution.nutrition.cat.FaAlinoleicacidCat;
import com.pood.server.service.pet_solution.nutrition.cat.FaArachidonicAcidCat;
import com.pood.server.service.pet_solution.nutrition.cat.MIMagnessiumCat;
import com.pood.server.service.pet_solution.nutrition.cat.MiCalciumCat;
import com.pood.server.service.pet_solution.nutrition.cat.MiCopperCat;
import com.pood.server.service.pet_solution.nutrition.cat.MiIodineCat;
import com.pood.server.service.pet_solution.nutrition.cat.MiManganeseCat;
import com.pood.server.service.pet_solution.nutrition.cat.MiPhosphoursnCat;
import com.pood.server.service.pet_solution.nutrition.cat.MiPotassiumCat;
import com.pood.server.service.pet_solution.nutrition.cat.MiSeleniumCat;
import com.pood.server.service.pet_solution.nutrition.cat.OtTaurineCat;
import com.pood.server.service.pet_solution.nutrition.cat.PrFatCat;
import com.pood.server.service.pet_solution.nutrition.cat.PrProteinCat;
import com.pood.server.service.pet_solution.nutrition.cat.ViVitaminACat;
import com.pood.server.service.pet_solution.nutrition.cat.ViVitaminDCat;
import com.pood.server.service.pet_solution.nutrition.cat.ViVitaminECat;
import com.pood.server.service.pet_solution.nutrition.common.AmHistidine;
import com.pood.server.service.pet_solution.nutrition.common.AmIsoleucine;
import com.pood.server.service.pet_solution.nutrition.common.AmLeucine;
import com.pood.server.service.pet_solution.nutrition.common.AmLysine;
import com.pood.server.service.pet_solution.nutrition.common.AmPhenyLanlanine;
import com.pood.server.service.pet_solution.nutrition.common.AmThreonine;
import com.pood.server.service.pet_solution.nutrition.common.AmValine;
import com.pood.server.service.pet_solution.nutrition.common.FaLinoleicacid;
import com.pood.server.service.pet_solution.nutrition.common.MiChloride;
import com.pood.server.service.pet_solution.nutrition.common.MiIron;
import com.pood.server.service.pet_solution.nutrition.common.MiSodium;
import com.pood.server.service.pet_solution.nutrition.common.MiZinc;
import com.pood.server.service.pet_solution.nutrition.common.VitaminB1;
import com.pood.server.service.pet_solution.nutrition.common.VitaminB12;
import com.pood.server.service.pet_solution.nutrition.common.VitaminB2;
import com.pood.server.service.pet_solution.nutrition.common.VitaminB3;
import com.pood.server.service.pet_solution.nutrition.common.VitaminB5;
import com.pood.server.service.pet_solution.nutrition.common.VitaminB6;
import com.pood.server.service.pet_solution.nutrition.common.VitaminB7;
import com.pood.server.service.pet_solution.nutrition.common.VitaminB9;
import com.pood.server.service.pet_solution.nutrition.common.VitaminK3;
import com.pood.server.service.pet_solution.nutrition.dog.AmMetCysDog;
import com.pood.server.service.pet_solution.nutrition.dog.AmMethionineDog;
import com.pood.server.service.pet_solution.nutrition.dog.AmPheTyrDog;
import com.pood.server.service.pet_solution.nutrition.dog.AmTryptophanDog;
import com.pood.server.service.pet_solution.nutrition.dog.FaAlinoleicacidDog;
import com.pood.server.service.pet_solution.nutrition.dog.FaArachidonicAcidDog;
import com.pood.server.service.pet_solution.nutrition.dog.MiCaPhDog;
import com.pood.server.service.pet_solution.nutrition.dog.MiCalciumDog;
import com.pood.server.service.pet_solution.nutrition.dog.MiCopperDog;
import com.pood.server.service.pet_solution.nutrition.dog.MiIodineDog;
import com.pood.server.service.pet_solution.nutrition.dog.MiManganeseDog;
import com.pood.server.service.pet_solution.nutrition.dog.MiPhosphoursnDog;
import com.pood.server.service.pet_solution.nutrition.dog.MiPotassiumDog;
import com.pood.server.service.pet_solution.nutrition.dog.MiSeleniumDog;
import com.pood.server.service.pet_solution.nutrition.dog.PrFatDog;
import com.pood.server.service.pet_solution.nutrition.dog.PrProteinDog;
import com.pood.server.service.pet_solution.nutrition.dog.ViVitaminADog;
import com.pood.server.service.pet_solution.nutrition.dog.ViVitaminDDog;
import com.pood.server.service.pet_solution.nutrition.dog.ViVitaminEDog;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class NutritionFactory {

    private static final int DOG = 1;

    private final UserPetBaseData userPetBaseData;
    private final ProductBaseData productBaseData;
    private final AafcoNrc aafcoNrc;

    public List<NutritionDetailModel> createNutritionModel() {
        if (DOG == userPetBaseData.getPcIdx()) {
            return extractNullInstance(dog());
        }

        return extractNullInstance(cat());
    }

    private List<NutritionDetailModel> extractNullInstance(
        final List<NutritionDetailModel> nutritionList) {
        return nutritionList.stream()
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    private List<NutritionDetailModel> cat() {
        List<NutritionDetailModel> catNutritionList = new ArrayList<>();
        catNutritionList.addAll(
            Arrays.asList(
                // 필수 영양소
                new PrProteinCat(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new PrFatCat(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new MiCalciumCat(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new MiPhosphoursnCat(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new MiPotassiumCat(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new MiSodium(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new MiChloride(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new MIMagnessiumCat(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new MiIron(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new MiCopperCat(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new MiManganeseCat(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new MiZinc(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new MiIodineCat(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new MiSeleniumCat(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new ViVitaminACat(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new VitaminB1(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new VitaminB2(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new VitaminB3(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new VitaminB5(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new VitaminB6(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new VitaminB7(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new VitaminB9(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new VitaminB12(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new ViVitaminDCat(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new ViVitaminECat(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new VitaminK3(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new AmHistidine(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new AmIsoleucine(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new AmLeucine(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new AmLysine(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new AmPhenyLanlanine(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new AmMetCysCat(userPetBaseData, productBaseData).createModel(),
                new AmMethionineCat(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new AmPheTyrCat(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new AmThreonine(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new AmTryptophanCat(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new AmValine(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new FaLinoleicacid(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new OtTaurineCat(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new FaArachidonicAcidCat(userPetBaseData, productBaseData, aafcoNrc).createModel(),
                new FaAlinoleicacidCat(userPetBaseData, productBaseData, aafcoNrc).createModel()
            )
        );
        return catNutritionList;
    }

    private List<NutritionDetailModel> dog() {
        List<NutritionDetailModel> dogNutritionList = new ArrayList<>();
        dogNutritionList.addAll(Arrays.asList(
            // 필수 영양소
            new PrProteinDog(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new PrFatDog(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new MiCalciumDog(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new MiPhosphoursnDog(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new MiCaPhDog(productBaseData, aafcoNrc).createModel(),
            new MiPotassiumDog(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new MiSodium(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new MiChloride(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new MIMagnessiumCat(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new MiIron(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new MiCopperDog(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new MiManganeseDog(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new MiZinc(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new MiIodineDog(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new MiSeleniumDog(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new ViVitaminADog(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new VitaminB1(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new VitaminB2(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new VitaminB3(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new VitaminB5(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new VitaminB6(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new VitaminB7(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new VitaminB9(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new VitaminB12(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new ViVitaminDDog(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new ViVitaminEDog(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new VitaminK3(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new AmHistidine(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new AmIsoleucine(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new AmLeucine(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new AmLysine(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new AmMetCysDog(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new AmMethionineDog(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new AmPheTyrDog(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new AmPhenyLanlanine(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new AmThreonine(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new AmTryptophanDog(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new AmValine(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new FaLinoleicacid(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new FaArachidonicAcidDog(userPetBaseData, productBaseData, aafcoNrc).createModel(),
            new FaAlinoleicacidDog(userPetBaseData, productBaseData, aafcoNrc).createModel()
        ));

        return dogNutritionList;
    }
}
