package com.pood.server.service.pet_solution;

import com.pood.server.controller.response.product.ProductSolutionPointResponse;
import com.pood.server.controller.response.solution.EatAmountResponse;
import com.pood.server.controller.response.solution.SolutionResponse;
import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.entity.meta.AllergyData;
import com.pood.server.entity.meta.GrainSize;
import com.pood.server.service.pet_solution.calc.WordExtract;
import com.pood.server.service.pet_solution.group.SuitableGroup;
import com.pood.server.service.pet_solution.intake.ProductStore;
import java.util.List;
import java.util.stream.Collectors;

public class SolutionCalculateResponseGroup {

    public static final int FEED = 0;
    private final UserPetBaseData userPetBaseData;
    private final ProductBaseData productBaseData;
    private final AafcoNrc aafcoNrc;

    public SolutionCalculateResponseGroup(final UserPetBaseData userPetBaseData,
        final ProductBaseData productBaseData,
        final AafcoNrc aafcoNrc) {
        this.userPetBaseData = userPetBaseData;
        this.productBaseData = productBaseData;
        this.aafcoNrc = aafcoNrc;
    }

    public SolutionResponse toResponse(final ProductStore productStore,
        List<AllergyData> allergyDataList,
        final List<GrainSize> grainSizeList) {
        SolutionFactory solutionFactory = new SolutionFactory(userPetBaseData, productBaseData,
            aafcoNrc);
        SuitableGroup suitableGroup = getSuitableGroup(solutionFactory);

        final List<ProductSolutionPointResponse> productSolutionPointResponses = suitableGroup.suitableList();

        final List<WordExtract> wordExtracts = getWordExtract(solutionFactory, allergyDataList,
            grainSizeList);

        final List<Object> extractList = wordExtracts.stream()
            .map(WordExtract::fitWord)
            .collect(Collectors.toList());

        return SolutionResponse.of(productSolutionPointResponses, extractList,
            new EatAmountResponse(productStore));
    }

    private SuitableGroup getSuitableGroup(final SolutionFactory solutionFactory) {
        if (productBaseData.getCtIdx() == FEED) {
            return new SuitableGroup(solutionFactory.isFeedProduct());
        }

        return new SuitableGroup(solutionFactory.basicSuitable());
    }

    private List<WordExtract> getWordExtract(final SolutionFactory solutionFactory,
        final List<AllergyData> allergyDataList,
        final List<GrainSize> grainSizeList) {
        if (productBaseData.getCtIdx() == FEED) {
            return solutionFactory.feedExtract(allergyDataList, grainSizeList);
        }

        if (productBaseData.getCtIdx() == 2) {
            return solutionFactory.catAndSnackExtract(allergyDataList);
        }

        return solutionFactory.supplies();
    }

}
