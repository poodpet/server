package com.pood.server.service.pet_solution.nutrition;

public enum NutritionUnit {
    NEED_G("최소필요 %.2fg"), NEED_G_MAX("최대허용 %.2fg"), INCLUDE_G("%.2fg"),
    NEED_MG("최소필요 %.2fmg"), NEED_MG_MAX("최대허용 %.2fmg"), INCLUDE_MG("%.2fmg"),
    NEED_IU("최소필요 %.3fIU"), NEED_IU_MAX("최대허용 %.3fIU"), INCLUDE_IU("%.3fIU"),
    NEED_IU_4("최소필요 %.4fIU"), NEED_IU_4_MAX("최대허용 %.4fIU"), INCLUDE_IU_4("%.4fIU"),
    NEED_MG_KG("최소필요 %.1fmg/kg"), NEED_MG_KG_MAX("최대허용 %.1fmg/kg"), INCLUDE_MG_KG("%.1fmg/kg"),
    NEED_IU_KG("최소필요 %.1fIU/kg"), NEED_IU_KG_MAX("최소필요 %.1fIU/kg"), INCLUDE_IU_KG("%.1fIU/kg"),
    NEED_IU_ROUND_MAX("최대허용 %fIU"), INCLUDE_ROUND("%fIU"),
    // 최소최대 허용문구는 사용하지 않음
    PERCENT_N("%.2f"), PERCENT("%.2f%%"), PERCENT_STRING("%s%"), NEED_IU_ROUND("최소필요 %fIU"),
    MILLIGRAM("%.2fmg"),
    GRAM("%.2fg"),
    IU("%.2fIU"),
    IU_ROUND("%.2fIU"),
    IU_4("%.2fIU");

    private final String unit;

    NutritionUnit(String unit) {
        this.unit = unit;
    }

    public String getUnit() {
        return unit;
    }

    public static String unitConvert(final NutritionUnit nutritionUnit, final double value) {
        return String.format(nutritionUnit.unit, value);
    }
}
