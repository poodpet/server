package com.pood.server.service.pet_solution.calc;

public interface Suitable {

    double getScore();

    double calcProductTypeScore();

    int getProductTypeScorePercent();

    String makeScoreKey();

}
