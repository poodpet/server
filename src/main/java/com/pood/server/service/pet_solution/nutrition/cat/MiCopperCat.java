package com.pood.server.service.pet_solution.nutrition.cat;

import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.SuitableCalc;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.service.pet_solution.nutrition.NutritionCat;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModel;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModelLegacy;
import com.pood.server.service.pet_solution.nutrition.NutritionUnit;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MiCopperCat implements NutritionCat {

    private static final int TYPE_ONE = 1;
    private static final String TITLE = "구리";

    private final UserPetBaseData userPetBaseData;
    private final ProductBaseData productBaseData;
    private final AafcoNrc aafcoNrc;

    @Override
    public NutritionDetailModelLegacy create() {

        float petCalorie = userPetBaseData.getPetCalorie();
        float limitMin = (float) (aafcoNrc.getMiCopper() * (petCalorie / 1000f));
        float contain = (float) ((TYPE_ONE * productBaseData.getFeedMiCopper()) * (1
            / productBaseData.getCalorie().floatValue()) * petCalorie);

        return new NutritionDetailModelLegacy(
            TITLE, limitMin >= contain,
            String.format(NutritionUnit.NEED_MG.getUnit(), limitMin),
            "",
            String.format(NutritionUnit.INCLUDE_MG.getUnit(), contain),
            0.5f, false
        );
    }

    @Override
    public NutritionDetailModel createModel() {
        float minValue = aafcoNrc.getMiCopper().floatValue();
        final double baseValue = productBaseData.getFeedMiCopper();
        float calcValue = (float) ((TYPE_ONE * baseValue) * (1
            / productBaseData.getCalorie().floatValue())) * 1000f;

        if (baseValue <= 0.0) {
            return null;
        }

        return new NutritionDetailModel(TITLE, SuitableCalc.makeMessage(minValue, calcValue),
            NutritionUnit.unitConvert(NutritionUnit.MILLIGRAM, baseValue),
            SuitableCalc.isMinEnough(minValue, calcValue), 0.5f);
    }

}
