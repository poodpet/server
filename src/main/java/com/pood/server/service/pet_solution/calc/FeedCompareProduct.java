package com.pood.server.service.pet_solution.calc;

import com.pood.server.dto.meta.pet.PetProductWord;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.ProductType;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.util.FeedType;
import java.util.Objects;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FeedCompareProduct implements Suitable, WordExtract{

    private final UserPetBaseData userPetBaseData;
    private final ProductBaseData productBaseData;

    @Override
    public double getScore() {
        final double petSizeScore = fitPetSizeAndIsDog(); //  lifeRecommendDealCalc
        final double petAgeScore = fitAge();  // lifeRecommendDealCalc
        final double petGrainSizeScore = fitGrainSizeAndWetFeed();
        final double petNeuteredScore = fitNeutered();
        final double petMoistureScore = fitMoisture();    //습식
        final double petAllergyScore = fitAllergy();

        final double totalPoint = (petSizeScore + petAgeScore + petGrainSizeScore
            + petNeuteredScore + petMoistureScore + petAllergyScore);

        final double removedGrainScore = totalPoint - petGrainSizeScore;

        if (removedGrainScore >= 100) {
            return 100.0;
        }

        final double refactorScore = Math.round(removedGrainScore * 100 / (100 - petGrainSizeScore));

        return Math.min(refactorScore, 100);
    }

    /**
     * feedTarget/펫 크기 유형 체크 펫 크기는 분류는 강아지만 있다
     */
    private double fitPetSizeAndIsDog() {
        if (userPetBaseData.isPetTypeCat()) {
            return 0;
        }
        //feed_target == 0은 모두에게 잘 맞음 또는 제품의 FeedTarget과 유저 펫의 feedTarget이 일치한다면
        if (productBaseData.getFeedTarget() == 0 ||
            productBaseData.eqFeedTarget(userPetBaseData.getUserPetFeedTargetNum())) {
            return userPetBaseData.getFeedTargetSurgeryScore();
        }
        return 0.0;
    }

    private double fitAge() {
        // 상품연령대, 유저펫의 연령대 맞는경우 중성화 여부와 펫 유형에 따라 점수를 줌
        boolean isFlag = FeedType.isFlag(productBaseData.getFeedType(), userPetBaseData);
        if (isFlag) {
            return userPetBaseData.getPetAgeSurgeryScore();
        }
        return 0.0;
    }

    /**
     * 선호하는 알갱이 크기 분석
     */
    private double fitGrainSizeAndWetFeed() {
        return userPetBaseData.getPetUnitSizeScore(productBaseData.getUnitSize(),
            productBaseData.isWetFeed());
    }

    /**
     * 중성화 수술 여부 상품의 칼로리가 4000가 넘고 중성화 수술을 안했을 경우 점수 계산 X
     */
    private double fitNeutered() {
        //상품의 칼로리가 4000이 넘거나 중성화 수술을 안했을 경우 점수 계산 X
        if (Objects.isNull(productBaseData.getCalorie())
            || (productBaseData.getCalorie() >= 4000
            && userPetBaseData.isNeutered())) {
            return 0;
        }
        return userPetBaseData.getPetSurgeryScore();
    }

    /**
     * 수분공급 분석, 습식(ct_sub_idx == 1) 인 경우에만 점수 추가
     */
    private double fitMoisture() {
        if (!productBaseData.isWetFeed()) {
            return 0;
        }
        return userPetBaseData.getPetMoistureSurgeryScore();
    }

    /**
     * 펫이 가지고 있는 알러지에 도움이 되는 제품인지 분석
     */
    private double fitAllergy() {
        if (productBaseData.getProductType().equals(ProductType.SUPPLIES)) {
            return 0.0d;
        }
        return userPetBaseData.getPetAllergyScore(productBaseData.getAnimalProtein(),
            productBaseData.getMainProperty());
    }


    @Override
    public double calcProductTypeScore() {
        return 0;
    }

    @Override
    public int getProductTypeScorePercent() {
        return 0;
    }

    @Override
    public String makeScoreKey() {
        return null;
    }


    @Override
    public PetProductWord fitWord() {
        if (!productBaseData.getProductType().equals(ProductType.FEED)) {
            throw new IllegalArgumentException("비교 상품이 사료가 아닙니다.");
        }

        return new PetProductWord(
            getAgeString(), convertFitAgeString(),
            getPcSize(), convertFitPetSizeString(),
            getNeutered(), convertFitNeutered()
        );
    }

    // 나이 퍼피, 어덜트, 시니어
    private String getAgeString() {
        return userPetBaseData.petAgeString();
    }

    private String getPcSize() {
        if (userPetBaseData.isPetTypeCat()) {
            return null;
        }
        return userPetBaseData.getPcSize();
    }

    private String getNeutered() {
        if (userPetBaseData.isNeutered()) {
            return "중성화 했어요";
        }
        return null;
    }

    private String convertFitAgeString() {
        final boolean flag = FeedType.isFlag(productBaseData.getFeedType(), userPetBaseData);
        if (flag) {
            return "적합";
        }
        return null;
    }

    private String convertFitPetSizeString() {
        if (fitPetSizeAndIsDog() > 0) {
            return "적합";
        }
        return null;
    }

    private String convertFitNeutered() {
        if (fitNeutered() > 0) {
            return "적합";
        }
        return null;
    }

}
