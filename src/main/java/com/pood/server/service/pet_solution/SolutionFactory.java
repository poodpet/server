package com.pood.server.service.pet_solution;

import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.entity.meta.AllergyData;
import com.pood.server.entity.meta.GrainSize;
import com.pood.server.service.pet_solution.calc.AllergyExtract;
import com.pood.server.service.pet_solution.calc.DoctorPersonal;
import com.pood.server.service.pet_solution.calc.GrainSizeExtract;
import com.pood.server.service.pet_solution.calc.Nutrition;
import com.pood.server.service.pet_solution.calc.PetProduct;
import com.pood.server.service.pet_solution.calc.Suitable;
import com.pood.server.service.pet_solution.calc.WordExtract;
import com.pood.server.service.pet_solution.calc.Worry;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModel;
import com.pood.server.service.pet_solution.nutrition.NutritionFactory;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SolutionFactory {

    private final UserPetBaseData userPetBaseData;
    private final ProductBaseData productBaseData;
    private final AafcoNrc aafcoNrc;

    public List<Suitable> basicSuitable() {
        List<Suitable> list = new ArrayList<>();
        list.add(new PetProduct(userPetBaseData, productBaseData));
        list.add(new Worry(userPetBaseData, productBaseData));
        list.add(new DoctorPersonal(productBaseData));
        return list;
    }

    public List<Suitable> isFeedProduct() {
        final List<Suitable> suitableList = basicSuitable();
        if (!productBaseData.eqProductType(ProductType.FEED)) {
            return suitableList;
        }
        final List<NutritionDetailModel> nutritionModel = getNutritionDetailModels();
        suitableList.add(new Nutrition(productBaseData, nutritionModel));
        return suitableList;
    }

    private List<NutritionDetailModel> getNutritionDetailModels() {
        final NutritionFactory nutritionFactory = new NutritionFactory(userPetBaseData, productBaseData, aafcoNrc);
        return nutritionFactory.createNutritionModel();
    }

    public List<WordExtract> supplies() {
        List<WordExtract> list = new ArrayList<>();
        list.add(new PetProduct(userPetBaseData, productBaseData));
        list.add(new Worry(userPetBaseData, productBaseData));
        return list;
    }

    public List<WordExtract> catAndSnackExtract(final List<AllergyData> allergyDataList) {
        final List<WordExtract> basicList = supplies();
        basicList.add(new AllergyExtract(userPetBaseData, productBaseData, allergyDataList));
        basicList.add(new Nutrition(productBaseData, getNutritionDetailModels()));
        return basicList;
    }

    public List<WordExtract> feedExtract(final List<AllergyData> allergyDataList,
        final List<GrainSize> grainSizeList) {
        final List<WordExtract> basicList = supplies();
        basicList.add(new AllergyExtract(userPetBaseData, productBaseData, allergyDataList));
        basicList.add(new Nutrition(productBaseData, getNutritionDetailModels()));
        basicList.add(new GrainSizeExtract(userPetBaseData, productBaseData, grainSizeList));
        return basicList;
    }
}
