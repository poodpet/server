package com.pood.server.service.pet_solution.calc;

import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.ProductType;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DoctorPersonal implements Suitable {

    private final ProductBaseData productBaseData;

    @Override
    public double getScore() {
        return productBaseData.getPetDoctorFeedDescription().getBaseScore();
    }

    @Override
    public double calcProductTypeScore() {
        return (getScore() / 100) * getProductTypeScorePercent();
    }

    @Override
    public int getProductTypeScorePercent() {
        if (productBaseData.getProductType().equals(ProductType.FEED)) {
            return 20;
        }
        return 40;
    }

    @Override
    public String makeScoreKey() {
        return "doctorScore";
    }

}
