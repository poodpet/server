package com.pood.server.service.pet_solution.nutrition.cat;

import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.SuitableCalc;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.service.pet_solution.nutrition.NutritionCat;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModel;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModelLegacy;
import com.pood.server.service.pet_solution.nutrition.NutritionUnit;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AmTryptophanCat implements NutritionCat {

    private static final int TYPE_TEN = 10;
    private static final String TITLE = "트립토판";
    private static final float AAFCO_MAX_AMTRYPTOPHAN = 4.25f;

    private final UserPetBaseData userPetBaseData;
    private final ProductBaseData productBaseData;
    private final AafcoNrc aafcoNrc;

    @Override
    public NutritionDetailModelLegacy create() {

        if (aafcoNrc.getAmTryptophan() > 0 && productBaseData.getFeedAmTryptophan() > 0) {
            return null;
        }

        float petCalorie = userPetBaseData.getPetCalorie();
        float limitMin = (float) (aafcoNrc.getAmTryptophan() * (petCalorie / 1000f));
        float limitMax = AAFCO_MAX_AMTRYPTOPHAN * (petCalorie / 1000f);
        float contain = (float) ((TYPE_TEN * productBaseData.getFeedAmTryptophan()) * (1
            / productBaseData.getCalorie().floatValue()) * petCalorie);

        return new NutritionDetailModelLegacy(
            TITLE, limitMin >= contain,
            String.format(NutritionUnit.NEED_MG.getUnit(), limitMin),
            String.format(NutritionUnit.NEED_MG_MAX.getUnit(), limitMax),
            String.format(NutritionUnit.INCLUDE_MG.getUnit(), contain),
            0.1f, false
        );
    }

    @Override
    public NutritionDetailModel createModel() {
        if (productBaseData.getFeedAmTryptophan() <= 0) {
            return null;
        }

        float minValue = aafcoNrc.getAmTryptophan().floatValue();
        float maxValue = AAFCO_MAX_AMTRYPTOPHAN;
        final double baseValue = productBaseData.getFeedAmTryptophan();
        float calcValue = (float) ((TYPE_TEN * baseValue) *
            (1 / productBaseData.getCalorie().floatValue())) * 1000f;

        if (baseValue <= 0.0) {
            return null;
        }

        return new NutritionDetailModel(TITLE,
            SuitableCalc.makeMinMaxMessage(minValue, maxValue, calcValue),
            NutritionUnit.unitConvert(NutritionUnit.PERCENT, baseValue),
            SuitableCalc.isBothEnough(minValue, maxValue, calcValue), 0.1f);
    }

}
