package com.pood.server.service.pet_solution.intake;

import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.intake.store.FeedSampleStore;
import com.pood.server.service.pet_solution.intake.store.SnackNutritionStore;
import com.pood.server.service.pet_solution.intake.store.SuppliesStore;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ProductStoreFactory {

    private final ProductBaseData productBaseData;
    private final UserPetIntake userPetIntake;

    public ProductStore make() {
        final int productCtIdx = productBaseData.getCtIdx();

        if (productCtIdx == 0 || productCtIdx == 14) {
            return new FeedSampleStore(productBaseData, userPetIntake);
        }else if(productCtIdx == 3 ){
            return new SuppliesStore();
        }

        return new SnackNutritionStore(productBaseData, userPetIntake);
    }
}
