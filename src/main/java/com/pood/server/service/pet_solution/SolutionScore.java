package com.pood.server.service.pet_solution;

import com.pood.server.entity.ScoreSort;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.springframework.data.util.Pair;

public class SolutionScore {

    private final List<Map<ProductType, Pair<Integer, Double>>> list;

    public SolutionScore(final List<Map<ProductType, Pair<Integer, Double>>> list) {
        this.list = list;
    }

    private Pair<Integer, Double> getTopScoreProductByType(final ProductType productType) {
        return list.stream()
            .map(score -> score.get(productType))
            .filter(Objects::nonNull)
            .sorted(new ScoreSort().reversed())
            .findAny().orElse(null);
    }

    public List<Pair<Integer, Double>> getTopPackageProduct() {
        List<Pair<Integer, Double>> results = new ArrayList<>();
        if (existsByType(ProductType.FEED)) {
            results.add(getTopScoreProductByType(ProductType.FEED));
        }
        if (existsByType(ProductType.SNACK)) {
            results.add(getTopScoreProductByType(ProductType.SNACK));
        }
        if (existsByType(ProductType.NUTRIENTS)) {
            results.add(getTopScoreProductByType(ProductType.NUTRIENTS));
        }
        if (existsByType(ProductType.SUPPLIES)) {
            results.add(getTopScoreProductByType(ProductType.SUPPLIES));
        }

        return results;
    }

    private boolean existsByType(final ProductType productType) {
        return list.stream()
            .map(score -> score.get(productType))
            .filter(Objects::nonNull)
            .count() > 0L;
    }

    public List<Pair<Integer, Double>> getListOrderByScoreDesc() {
        List<Pair<Integer, Double>> result = new ArrayList();
        for (Map<ProductType, Pair<Integer, Double>> productTypePairMap : list) {
            for (ProductType next : productTypePairMap.keySet()) {
                result.add(productTypePairMap.get(next));
            }
        }
        result.sort(new ScoreSort().reversed());
        return result;
    }
}
