package com.pood.server.service.pet_solution.nutrition.dog;

import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.SuitableCalc;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModel;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModelLegacy;
import com.pood.server.service.pet_solution.nutrition.NutritionDog;
import com.pood.server.service.pet_solution.nutrition.NutritionUnit;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MiPhosphoursnDog implements NutritionDog {

    private static final int TYPE_TEN = 10;
    private static final String TITLE = "인";
    private static final float AAFCO_MAX_MIPHOSPHOURS = 4f;

    private final UserPetBaseData userPetBaseData;
    private final ProductBaseData productBaseData;
    private final AafcoNrc aafcoNrc;

    @Override
    public NutritionDetailModelLegacy create() {

        float petCalorie = userPetBaseData.getPetCalorie();
        float limitMin = (float) (aafcoNrc.getMiPhosphours() * (petCalorie / 1000f));
        float limitMax = (float) (AAFCO_MAX_MIPHOSPHOURS * (petCalorie / 1000f));
        float contain = (float) ((TYPE_TEN * productBaseData.getFeedMiPhosphours()) * (1
            / productBaseData.getCalorie().floatValue())
            * petCalorie);

        return new NutritionDetailModelLegacy(
            TITLE, limitMin >= contain,
            String.format(NutritionUnit.NEED_G.getUnit(), limitMin),
            String.format(NutritionUnit.NEED_G_MAX.getUnit(), limitMax),
            String.format(NutritionUnit.INCLUDE_G.getUnit(), contain),
            16f, true
        );
    }

    @Override
    public NutritionDetailModel createModel() {
        float minValue = aafcoNrc.getMiPhosphours().floatValue();
        float maxValue = AAFCO_MAX_MIPHOSPHOURS;
        final double baseValue = productBaseData.getFeedMiPhosphours();
        float calcValue = (float) ((TYPE_TEN * baseValue) * (1 / productBaseData.getCalorie()
            .floatValue())) * 1000f;

        if (baseValue <= 0.0) {
            return null;
        }

        return new NutritionDetailModel(TITLE,
            SuitableCalc.makeMinMaxMessage(minValue, maxValue, calcValue),
            NutritionUnit.unitConvert(NutritionUnit.PERCENT, baseValue),
            SuitableCalc.isBothEnough(minValue, maxValue, calcValue), 16f);
    }

}
