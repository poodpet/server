package com.pood.server.service.pet_solution.nutrition.cat;

import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.SuitableCalc;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.service.pet_solution.nutrition.NutritionCat;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModel;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModelLegacy;
import com.pood.server.service.pet_solution.nutrition.NutritionUnit;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ViVitaminDCat implements NutritionCat {

    private static final int TYPE_ONE = 1;
    private static final String TITLE = "비타민D";
    private static final float AAFCO_MAX_VITAMIN_D = 7520f;

    private final UserPetBaseData userPetBaseData;
    private final ProductBaseData productBaseData;
    private final AafcoNrc aafcoNrc;

    @Override
    public NutritionDetailModelLegacy create() {

        if (aafcoNrc.getViVitaminD() > 0 && productBaseData.getFeedViVitaminD() > 0) {
            return null;
        }

        float petCalorie = userPetBaseData.getPetCalorie();
        float limitMin = (float) (aafcoNrc.getViVitaminD() * (petCalorie / 1000f));
        float limitMax = AAFCO_MAX_VITAMIN_D * (petCalorie / 1000f);
        float contain = (float) ((TYPE_ONE * productBaseData.getFeedViVitaminD()) * (1
            / productBaseData.getCalorie().floatValue()) * petCalorie);

        return new NutritionDetailModelLegacy(
            TITLE, limitMin >= contain,
            String.format(NutritionUnit.NEED_IU.getUnit(), limitMin),
            String.format(NutritionUnit.NEED_IU_MAX.getUnit(), limitMax),
            String.format(NutritionUnit.INCLUDE_IU.getUnit(), contain),
            1f, false
        );
    }

    @Override
    public NutritionDetailModel createModel() {
        if (productBaseData.getFeedViVitaminD() <= 0) {
            return null;
        }

        float minValue = aafcoNrc.getViVitaminD().floatValue();
        float maxValue = AAFCO_MAX_VITAMIN_D;
        final double baseValue = productBaseData.getFeedViVitaminD();
        float calcValue = (float) ((TYPE_ONE * baseValue) * (1
            / productBaseData.getCalorie().floatValue())) * 1000f;

        if (baseValue <= 0.0) {
            return null;
        }

        return new NutritionDetailModel(TITLE,
            SuitableCalc.makeMinMaxMessage(minValue, maxValue, calcValue),
            NutritionUnit.unitConvert(NutritionUnit.IU, baseValue),
            SuitableCalc.isBothEnough(minValue, maxValue, calcValue), 1f);
    }

}
