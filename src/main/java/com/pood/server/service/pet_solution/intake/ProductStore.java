package com.pood.server.service.pet_solution.intake;

public interface ProductStore {

    double dailyTakesToEat();

    int longItTakesToEat();

    String cupType();

    String cupSubType();

}
