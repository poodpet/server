package com.pood.server.service.pet_solution.nutrition.cat;

import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.SuitableCalc;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.service.pet_solution.nutrition.NutritionCat;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModel;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModelLegacy;
import com.pood.server.service.pet_solution.nutrition.NutritionUnit;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AmMetCysCat implements NutritionCat {

    private static final int TYPE_TEN = 10;
    private static final String TITLE = "메티오닌+시스틴";
    private static final float AAFCO_MIN_AMMETCYS = 1f;

    private final UserPetBaseData userPetBaseData;
    private final ProductBaseData productBaseData;

    @Override
    public NutritionDetailModelLegacy create() {

        float petCalorie = userPetBaseData.getPetCalorie();

        float limitMin = (float) (AAFCO_MIN_AMMETCYS * (petCalorie / 1000f));
        float contain = (float) ((TYPE_TEN * productBaseData.getFeedAmMetCys()) * (1
            / productBaseData.getCalorie().floatValue()) * petCalorie);

        return new NutritionDetailModelLegacy(
            TITLE, limitMin >= contain,
            String.format(NutritionUnit.NEED_MG.getUnit(), limitMin),
            "",
            String.format(NutritionUnit.INCLUDE_MG.getUnit(), contain),
            0.4f, false
        );
    }

    @Override
    public NutritionDetailModel createModel() {

        float minValue = AAFCO_MIN_AMMETCYS;
        final double baseValue = productBaseData.getFeedAmMetCys();
        float calcValue = (float) ((TYPE_TEN * baseValue) *
            (1 / productBaseData.getCalorie().floatValue())) * 1000f;

        if (baseValue <= 0.0) {
            return null;
        }

        return new NutritionDetailModel(TITLE, SuitableCalc.makeMessage(minValue, calcValue),
            NutritionUnit.unitConvert(NutritionUnit.PERCENT, baseValue),
            SuitableCalc.isMinEnough(minValue, calcValue), 0.4f);
    }

}
