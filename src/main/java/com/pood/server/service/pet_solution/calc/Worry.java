package com.pood.server.service.pet_solution.calc;

import com.pood.server.controller.response.solution.WorryWordResponse;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.service.pet_solution.group.UserPetAiDiagnosisAiSolutionGroup.UserPetAiDiagnosisStr;
import java.util.List;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Worry implements Suitable, WordExtract {

    private final UserPetBaseData userPetBaseData;
    private final ProductBaseData productBaseData;

    @Override
    public double getScore() {
        List<Integer> ardGroupCodeList = userPetBaseData.getArdGroupCodeOrderByPriority();
        if (ardGroupCodeList.isEmpty()) {
            return 0.0d;
        }
        return productBaseData.getPetWorryScore(ardGroupCodeList);
    }

    @Override
    public double calcProductTypeScore() {
        return (getScore() / 100) * getProductTypeScorePercent();
    }

    @Override
    public int getProductTypeScorePercent() {
        return 30;
    }

    @Override
    public String makeScoreKey() {
        return "worryScore";
    }

    @Override
    public Object fitWord() {
        final List<UserPetAiDiagnosisStr> worryList = userPetBaseData.fitEachDiagnosis();
        if (worryList.isEmpty()) {
            return new WorryWordResponse();
        }
        return new WorryWordResponse(productBaseData.fitAiDiagnosis(worryList));
    }

    public List<UserPetAiDiagnosisStr> toWordList() {
        final List<UserPetAiDiagnosisStr> worryList = userPetBaseData.fitEachDiagnosis();
        return productBaseData.fitAiDiagnosis(worryList);
    }
}
