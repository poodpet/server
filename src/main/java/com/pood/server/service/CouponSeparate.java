package com.pood.server.service;

import static com.pood.server.util.PublishCouponType.FIRST_PET_REGISTER;
import static com.pood.server.util.PublishCouponType.WELCOME;

import com.pood.server.entity.meta.Coupon;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.meta.CouponRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("metaTransactionManager")
public class CouponSeparate {

    private static final int WELCOME_COUPON = 14;

    private final CouponRepository couponRepository;

    public List<Coupon> findAll() {
        return couponRepository.findAll();
    }

    public Coupon findByIdx(final int idx) {
        return couponRepository.findById(idx)
            .orElseThrow(() -> new NotFoundException("등록된 쿠폰을 찾을 수 없습니다."));
    }

    public List<Coupon> findWelcomeCouponList() {
        return couponRepository.findAllByPublishTypeIdxOrderByMaxPriceAsc(WELCOME.getType());
    }

    public List<Coupon> findFirstRegisteredPetCouponList() {
        return couponRepository.findAllByPublishTypeIdxOrderByMaxPriceAsc(FIRST_PET_REGISTER.getType());
    }

}
