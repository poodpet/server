package com.pood.server.service;

import com.pood.server.entity.meta.IntroMarketingImage;
import com.pood.server.repository.meta.IntroMarketingImageRepository;
import java.time.LocalDateTime;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class IntroMarketingImageSeparate {

    private final IntroMarketingImageRepository introMarketingImageRepository;

    public List<IntroMarketingImage> getActiveIntroMarketingImageList() {
        return introMarketingImageRepository.findAllByStartTimeBeforeAndEndTimeAfter(
            LocalDateTime.now(), LocalDateTime.now());
    }
}
