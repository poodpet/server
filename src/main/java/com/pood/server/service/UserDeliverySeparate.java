package com.pood.server.service;

import com.pood.server.dto.user.delivery.UserDeliveryCreateRequest;
import com.pood.server.dto.user.delivery.UserDeliveryUpdateRequest;
import com.pood.server.entity.user.UserDeliveryAddress;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.user.UserDeliveryAddressRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional("userTransactionManager")
@RequiredArgsConstructor
public class UserDeliverySeparate {

    private static final boolean IS_DEFAULT_SHIPPING_ADDRESS = true;
    public static final int DEFAULT_TYPE = 1;

    private final UserDeliveryAddressRepository userDeliveryAddressRepository;

    public void save(final UserDeliveryCreateRequest userDeliveryCreateRequest, final UserInfo userInfo, final int remoteType) {
        if (userDeliveryCreateRequest.getDefaultType() == DEFAULT_TYPE) {
            updatePreviousDefaultType(userInfo);
        }

        userDeliveryAddressRepository.save(userDeliveryCreateRequest.toEntity(userInfo, remoteType));
    }

    private void updatePreviousDefaultType(final UserInfo userInfo) {
        final UserDeliveryAddress userDeliveryAddress = userDeliveryAddressRepository.findByUserInfoAndDefaultType(
                userInfo, IS_DEFAULT_SHIPPING_ADDRESS)
            .orElse(UserDeliveryAddress.builder().build());
        userDeliveryAddress.updateNotDefaultType();
    }

    public List<UserDeliveryAddress> findAllAddressOfUser(final UserInfo userInfo) {
        return userDeliveryAddressRepository.findAllByUserInfo(userInfo);
    }

    public void updateDeliveryAddress(final UserDeliveryUpdateRequest userDeliveryUpdateRequest,
        final UserInfo userInfo, final int fitRemoteType) {
        final UserDeliveryAddress changeAddress = findByIdx(userDeliveryUpdateRequest);

        if (userDeliveryUpdateRequest.getDefaultType() == DEFAULT_TYPE) {
            updatePreviousDefaultType(userInfo);
        }

        changeAddress.updateAll(userDeliveryUpdateRequest, userInfo, fitRemoteType);
    }

    public UserDeliveryAddress findByIdx(
        final UserDeliveryUpdateRequest userDeliveryUpdateRequest) {
        return userDeliveryAddressRepository.findById(userDeliveryUpdateRequest.getIdx())
            .orElseThrow(() -> new NotFoundException("해당하는 배송지가 없습니다."));
    }

    public void deleteByIdx(final int deliveryIdx) {
        userDeliveryAddressRepository.deleteById(deliveryIdx);
    }

    public void onlyOneAddressThenChangeDefault(final UserInfo userInfo) {
        final List<UserDeliveryAddress> addressList = userDeliveryAddressRepository.findAllByUserInfo(
            userInfo);

        if (addressList.size() == 1) {
            final UserDeliveryAddress userDeliveryAddress = addressList.get(0);
            userDeliveryAddress.updateDefaultType();
        }
    }
}
