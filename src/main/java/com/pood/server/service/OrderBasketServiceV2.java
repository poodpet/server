package com.pood.server.service;

import com.pood.server.config.meta.user.USER_BASKET_STATUS;
import com.pood.server.entity.meta.GoodsProduct;
import com.pood.server.entity.order.OrderBasket;
import com.pood.server.entity.order.OrderBasketProduct;
import com.pood.server.object.IMP.IMP_SHOP;
import com.pood.server.repository.meta.GoodsProductRepository;
import com.pood.server.repository.meta.PromotionRepository;
import com.pood.server.repository.order.OrderBasketProductRepository;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderBasketServiceV2 {

    private final OrderBasketProductRepository orderBasketProductRepository;
    private final GoodsProductRepository goodsProductRepository;
    private final PromotionRepository promotionRepository;
    private final OrderBasketSeparate orderBasketServiceSeparate;

    public void insertBasketInfo(IMP_SHOP e1, Integer ORDER_IDX, String ORDER_NUMBER,
        Integer DISCOUNT_PRICE, Integer PR_DISCOUNT_RATE) {

        OrderBasket orderBasket = new OrderBasket(e1.getIdx(), ORDER_IDX, ORDER_NUMBER,
            e1.getCoupon_idx(), e1.getPr_code(), e1.getgoods_idx(), e1.getSeller_idx(),
            DISCOUNT_PRICE, e1.getQty(), USER_BASKET_STATUS.DEFAULT, e1.getgoods_price());
        OrderBasket saveOrderBasket = orderBasketServiceSeparate.save(orderBasket);

        List<GoodsProduct> goodsProducts = goodsProductRepository.findAllByGoodsIdx(
            e1.getgoods_idx());

        List<OrderBasketProduct> orderBasketProductList = new ArrayList<>();
        for (GoodsProduct goodsProduct : goodsProducts) {
            OrderBasketProduct orderBasketProduct = new OrderBasketProduct(saveOrderBasket.getIdx(),
                goodsProduct.getProductIdx(), goodsProduct.getProductQty(),
                goodsProduct.getProductPrice());
            orderBasketProductList.add(orderBasketProduct);
        }

        orderBasketProductRepository.saveAll(orderBasketProductList);


    }

}
