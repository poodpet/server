package com.pood.server.service;

import com.pood.server.entity.user.BodyShape;
import com.pood.server.entity.user.UserPet;
import com.pood.server.entity.user.UserPetBodyShapeDiary;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.user.UserPetBodyShapeDiaryRepository;
import com.pood.server.util.date.DateUtil;
import java.time.LocalDate;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("userTransactionManager")
public class UserPetBodyShapeSeparate {

    private final UserPetBodyShapeDiaryRepository userPetBodyShapeDiaryRepository;

    public void recordBodyManagement(final UserPet userPet, final BodyShape bodyShape) {

        final Optional<UserPetBodyShapeDiary> userPetDiary = userPetBodyShapeDiaryRepository.getUserPetBodyShape(
            userPet.getIdx(), DateUtil.of(LocalDate.now()).convertWeeklyFridayDate());

        if (userPetDiary.isPresent()) {
            userPetDiary.get().updateBodyShape(bodyShape);
            return;
        }

        userPetBodyShapeDiaryRepository.save(UserPetBodyShapeDiary.of(userPet, bodyShape));
    }

    public UserPetBodyShapeDiary findRecentInfo(final Integer userPetIdx) {
        return userPetBodyShapeDiaryRepository.findFirstByUserPetIdxAndBodyShapeIsNotNullOrderByBaseDateDesc(
            userPetIdx).orElseThrow(
            () -> new NotFoundException(userPetIdx + " 번호의 유저 펫의 최근 체형 정보가 존재하지 않습니다.")
        );
    }

    public UserPetBodyShapeDiary findRecentInfoOrElseNull(final Integer userPetIdx) {
        return userPetBodyShapeDiaryRepository.findFirstByUserPetIdxAndBodyShapeIsNotNullOrderByBaseDateDesc(
            userPetIdx).orElse(UserPetBodyShapeDiary.empty());
    }

}
