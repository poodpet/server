package com.pood.server.service.order;

import com.pood.server.entity.order.OrderPoint;
import com.pood.server.entity.order.dto.OrderPointRefundDto;
import com.pood.server.entity.order.group.OrderPointMapperGroup;
import com.pood.server.exception.CustomStatusException;
import com.pood.server.exception.ErrorMessage;
import com.pood.server.repository.order.OrderPointRepository;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional("orderTransactionManager")
@RequiredArgsConstructor
public class OrderPointSeparate {

    private final OrderPointRepository orderPointRepository;

    public OrderPoint getByIdx(final Integer idx) {
        return orderPointRepository.findById(idx)
            .orElseThrow(() -> CustomStatusException.badRequest(ErrorMessage.ORDER_POINT));
    }

    public OrderPointMapperGroup getUsePointList(final String orderNumber) {

        return OrderPointMapperGroup.of(
            orderPointRepository.findAllByOrderNumberAndUsed(orderNumber));
    }

    public void refundPoint(final List<OrderPointRefundDto> refundList) {

        final Optional<OrderPointRefundDto> subTractUsedOrderPoint = refundList.stream()
            .filter(Predicate.not(OrderPointRefundDto::isAllRefund))
            .findFirst();

        if (subTractUsedOrderPoint.isPresent()) {
            final OrderPointRefundDto refundDto = subTractUsedOrderPoint.get();
            subTractUsedPoint(refundDto.getIdx(), refundDto.getRefundPoint());
            refundList.remove(refundDto);
        }

        updateAllPointReturnStatus(
            refundList.stream()
                .map(OrderPointRefundDto::getIdx)
                .collect(Collectors.toList())
        );
    }

    private void updateAllPointReturnStatus(final List<Integer> orderPointIdxListToReturnStatus) {
        List<OrderPoint> orderPointList = orderPointRepository.findAllById(
            orderPointIdxListToReturnStatus);

        orderPointList.forEach(OrderPoint::updateTypeToPointReturn);
    }

    private void subTractUsedPoint(final Integer idx, final int returnPoint) {
        OrderPoint orderPoint = getByIdx(idx);
        orderPoint.minusUsedPoint(returnPoint);
    }
}
