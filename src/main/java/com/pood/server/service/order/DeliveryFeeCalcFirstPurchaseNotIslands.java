package com.pood.server.service.order;

import com.pood.server.web.mapper.payment.Money;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeliveryFeeCalcFirstPurchaseNotIslands implements DeliveryFeeCalc {

    Logger logger = LoggerFactory.getLogger(DeliveryFeeCalcFirstPurchaseNotIslands.class);

    private final Money defualtDeliveryFee;

    @Override
    public Money calc(final Money isIslandDeliveryFee) {
        logger.info("해당 구매자는 첫 구매자이며, 원격 배송지가 아니므로 배송비가 0원이 부과됩니다.");
        return new Money(0);
    }

    public DeliveryFeeCalcFirstPurchaseNotIslands(final Money defualtDeliveryFee) {
        this.defualtDeliveryFee = defualtDeliveryFee;
    }
}
