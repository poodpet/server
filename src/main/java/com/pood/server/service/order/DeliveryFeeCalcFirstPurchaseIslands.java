package com.pood.server.service.order;

import com.pood.server.web.mapper.payment.Money;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeliveryFeeCalcFirstPurchaseIslands implements DeliveryFeeCalc {

    Logger logger = LoggerFactory.getLogger(DeliveryFeeCalcFirstPurchaseIslands.class);

    private final Money defualtDeliveryFee;

    @Override
    public Money calc(final Money isIslandDeliveryFee) {
        final Money totalMoney = new Money(0).plus(isIslandDeliveryFee);
        logger.info(
            "해당 구매자는 첫 구매자이며, 원격 배송지가 맞으므로 배송비가 " + isIslandDeliveryFee.getLongValue() + "원이 부과됩니다.");
        return isIslandDeliveryFee;
    }

    public DeliveryFeeCalcFirstPurchaseIslands(final Money defualtDeliveryFee) {
        this.defualtDeliveryFee = defualtDeliveryFee;
    }
}
