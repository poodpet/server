package com.pood.server.service.order;

import com.pood.server.web.mapper.payment.Money;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeliveryFeeCalcNotIslandsAndBasicFree implements DeliveryFeeCalc {

    Logger logger = LoggerFactory.getLogger(DeliveryFeeCalcNotIslandsAndBasicFree.class);

    private final Money defualtDeliveryFee;

    @Override
    public Money calc(final Money isIslandDeliveryFee) {
        logger.info("원격 배송지 X, 기본 배송비 X 이므로 배송비가 0 원이 부과됩니다.");
        return new Money(0);
    }

    public DeliveryFeeCalcNotIslandsAndBasicFree(final Money defualtDeliveryFee) {
        this.defualtDeliveryFee = defualtDeliveryFee;
    }
}
