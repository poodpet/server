package com.pood.server.service.order;

import com.pood.server.web.mapper.payment.Money;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeliveryFeeCalcIslandsAndBasicPaid implements DeliveryFeeCalc {

    Logger logger = LoggerFactory.getLogger(DeliveryFeeCalcIslandsAndBasicPaid.class);

    private final Money defualtDeliveryFee;

    @Override
    public Money calc(final Money isIslandDeliveryFee) {
        final Money totalMoney = defualtDeliveryFee.plus(
            new Money(isIslandDeliveryFee.getLongValue()));
        logger.info("원격 배송지 O, 기본 배송비 O 이므로 배송비가 "
            + totalMoney.getLongValue() + "원이 부과됩니다.");
        return totalMoney;
    }

    public DeliveryFeeCalcIslandsAndBasicPaid(final Money defualtDeliveryFee) {
        this.defualtDeliveryFee = defualtDeliveryFee;
    }

}
