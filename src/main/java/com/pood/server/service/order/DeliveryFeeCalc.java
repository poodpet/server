package com.pood.server.service.order;

import com.pood.server.web.mapper.payment.Money;

public interface DeliveryFeeCalc {

    Money calc(Money money);
}
