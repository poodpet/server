package com.pood.server.service;


import static com.pood.server.exception.ErrorMessage.PROMOTION;

import com.pood.server.dto.meta.goods.GoodsDto;
import com.pood.server.dto.meta.goods.PromotionDto;
import com.pood.server.dto.meta.goods.PromotionDto.PromotionGoodsProductInfo;
import com.pood.server.dto.meta.goods.SortedGoodsGroup;
import com.pood.server.entity.meta.PromotionGroup;
import com.pood.server.exception.CustomStatusException;
import com.pood.server.repository.meta.PromotionGroupRepository;
import com.pood.server.repository.meta.PromotionImageRepository;
import com.pood.server.repository.meta.PromotionRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PromotionServiceV2 {

    private final PromotionRepository promotionRepository;
    private final PromotionGroupRepository promotionGroupRepository;
    private final PromotionImageRepository promotionImageRepository;
    private final GoodsSeparate goodsSeparate;

    public PromotionDto.PromotionGoodsListInfo getPromotionDetail(Integer idx) {

        PromotionDto.PromotionGoodsListInfo promotionGoodsListInfo = promotionRepository.findListInfoById(
            idx);
        Optional.ofNullable(promotionGoodsListInfo)
            .orElseThrow(() -> CustomStatusException.badRequest(
                PROMOTION));

        List<PromotionDto.PromotionImage> promotionImageList = promotionImageRepository.findAllByPrIdxAndType(
                idx, 1).stream().map(promotionImage -> new PromotionDto.PromotionImage(promotionImage.getIdx(), promotionImage.getUrl(), promotionImage.getType()))
            .collect(Collectors.toList());
        promotionGoodsListInfo.setPromotionImageList(promotionImageList);

        List<PromotionGroup> promotionGroupList = promotionGroupRepository.findByPrIdx(
            promotionGoodsListInfo.getIdx());

        List<PromotionDto.PromotionGroup> promotionGroupDtoList = new ArrayList<>();
        for (PromotionGroup promotionGroup : promotionGroupList) {
            List<PromotionDto.PromotionGoods> promotionGroupGoodsList = promotionRepository.findPromotionGoodsDetail(
                promotionGroup.getIdx());
            PromotionDto.PromotionGroup promotionGroupInfo = new PromotionDto.PromotionGroup(
                promotionGroup.getName(), promotionGroupGoodsList);
            promotionGroupDtoList.add(promotionGroupInfo);
        }

        promotionGoodsListInfo.setPromotionGroupGoods(promotionGroupDtoList);
        return promotionGoodsListInfo;
    }

    public List<GoodsDto.SortedGoodsList> getHotTimeGoodsList(Integer petCtIdx) {

        List<PromotionDto.PromotionGoodsProductInfo> promotionList = promotionRepository.findByHotTimeGoodsList(
            petCtIdx);
        List<Integer> goodsIdsList = promotionList.stream().map(
                PromotionGoodsProductInfo::getGoodsIdx)
            .collect(Collectors.toList());
        SortedGoodsGroup sortedGoodsList = new SortedGoodsGroup(goodsSeparate.myOrderGoodsListByInteger(
            goodsIdsList));
        sortedGoodsList.setPromotionData(promotionList);
        return sortedGoodsList.getSortedGoodsListOrderByPrDiscountRateDesc();
    }
}
