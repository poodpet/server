package com.pood.server.service;

import static com.pood.server.exception.ErrorMessage.GOODS;
import static com.pood.server.exception.ErrorMessage.PRODUCT;

import com.pood.server.controller.request.goods.GoodsFilterRequest;
import com.pood.server.dto.meta.goods.GoodsDto.GoodsDetail;
import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.entity.meta.Goods;
import com.pood.server.entity.meta.GoodsSubCt;
import com.pood.server.entity.meta.dto.home.CustomRankInfoDto;
import com.pood.server.entity.meta.group.CustomRankInfoGroup;
import com.pood.server.entity.meta.mapper.OrderGoodsMapper;
import com.pood.server.entity.meta.mapper.SuggestRandomGoodsMapper;
import com.pood.server.entity.order.OrderBasket;
import com.pood.server.exception.CustomStatusException;
import com.pood.server.exception.ErrorMessage;
import com.pood.server.repository.meta.GoodsRepository;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional("metaTransactionManager")
@RequiredArgsConstructor
public class GoodsSeparate {

    private static final int SOLD_STOP = 2;
    private static final int SOLD_WAIT = 0;
    private static final int ALL = 0;
    private static final int IS_SALE = 1;

    private final GoodsRepository goodsRepository;

    public Goods findById(final int goodsIdx) {
        return goodsRepository.findById(goodsIdx)
            .orElseThrow(() -> CustomStatusException.badRequest(PRODUCT));
    }

    public GoodsDetail findByGoodsIdValidation(final int idx) {
        return goodsRepository.findByGoodsId(idx)
            .orElseThrow(() -> CustomStatusException.badRequest(GOODS));
    }

    public GoodsDetail findByGoodsIdGetNull(final int idx) {
        return goodsRepository.findByGoodsId(idx).orElse(null);
    }

    public SortedGoodsList getRecommendGoodsList(final int productIndex, final int pcIdx) {
        return goodsRepository.getrRcommendGoodsList(productIndex, pcIdx).stream().findFirst()
            .orElse(null);
    }

    public SortedGoodsList getRecommendGoodsListByIsSale(final int productIndex, final int pcIdx) {
        return goodsRepository.getRecommendGoodsListByIsSale(productIndex, pcIdx).stream()
            .findFirst()
            .orElse(null);
    }

    public SortedGoodsList getRcommendGoodsListByProductIdxAndGoodsIdxListNotIn(
        final int pcIdx,
        final int productIdx,
        final List<Integer> goodsIdxList) {
        return goodsRepository.getrRcommendGoodsListByProductIdxAndNotGoodsIdxList(pcIdx,
                productIdx,
                goodsIdxList)
            .stream()
            .findFirst()
            .orElse(null);
    }

    public List<SortedGoodsList> myOrderGoodsListByBasket(final List<OrderBasket> orderBasketList) {
        return goodsRepository.getMyOrderGoodsList(
            orderBasketList.stream().map(OrderBasket::getGoodsIdx).collect(
                Collectors.toList()));
    }

    public Page<SortedGoodsList> goodsSearchList(final Pageable pageable, final String searchText,
        final Integer pcIdx, final Long goodsCtIdx) {

        if (Objects.isNull(goodsCtIdx)) {
            return goodsRepository.goodsSearchList(pageable, searchText, pcIdx);
        }

        return goodsRepository.goodsSearchList(pageable, searchText, pcIdx, goodsCtIdx);
    }

    public Page<SortedGoodsList> goodsSearchListFilterByProductCt(final Pageable pageable,
        final String searchText, final Integer pcIdx) {

        return goodsRepository.goodsSearchListByFeedComparison(pageable, searchText, pcIdx);
    }

    public Page<SortedGoodsList> goodsFilteredList(final GoodsFilterRequest request,
        final Pageable pageable) {
        return goodsRepository.goodsFilteredList(pageable, request.getPetWorryList(),
            request.getGoodsCtIdx(), request.getGoodsSubCtIdx(), request.getProteinList(),
            request.getGrainSizeList(), request.getFeedTargetList(), request.getPcIdx(),
            request.getAgeTypeList());
    }

    public List<SortedGoodsList> myOrderGoodsListByInteger(final List<Integer> goodsIdsList) {
        return goodsRepository.getMyOrderGoodsList(goodsIdsList);
    }

    public List<SortedGoodsList> getSortedGoodsInIdxList(final List<Integer> goodsIdxList) {
        return goodsRepository.getSortedGoodsInIdxList(goodsIdxList);
    }

    public List<String> searchCategoryAndKeyword(final int petCategoryIndex, final String keyWord,
        final Long goodsCt) {

        if (!Objects.isNull(goodsCt)) {
            return goodsRepository.findGoodsNameBySearchText(petCategoryIndex, keyWord, goodsCt);
        }
        return goodsRepository.findGoodsNameBySearchText(petCategoryIndex, keyWord);

    }

    public List<String> searchCategoryAndKeywordForFeedCompare(final int petCategoryIndex,
        final String keyWord) {

        return goodsRepository.findGoodsNameBySearchTextByFeedComparison(petCategoryIndex, keyWord);

    }

    public List<String> getGoodsNameList(final List<Integer> goodsIdxList) {
        List<String> list = getGoodsListByIdxIn(goodsIdxList).stream()
            .map(Goods::getGoodsName).collect(
                Collectors.toList());
        if (list.isEmpty()) {
            throw CustomStatusException.badRequest(ErrorMessage.GOODS);
        }
        return list;
    }

    public List<Goods> getGoodsListByIdxIn(final List<Integer> goodsIdxList) {
        return goodsRepository.findAllByIdxIn(goodsIdxList);
    }

    public Page<SortedGoodsList> getPoodHomeGoodsList(final GoodsSubCt goodsSubCt, final int pcIdx,
        final Pageable pageable) {
        return goodsRepository.getPoodHomeDeliveryGoodsList(goodsSubCt, pcIdx, pageable);
    }

    public List<SortedGoodsList> poodHomeSetGoodsList(final int petCategoryIdx) {
        final List<SortedGoodsList> setGoodsList = goodsRepository.poodHomeSetGoodsList(
            petCategoryIdx);
        Collections.shuffle(setGoodsList, ThreadLocalRandom.current());
        return setGoodsList;
    }

    public List<SortedGoodsList> poodHomeNewGoodsList(final int petCategoryIdx) {
        return goodsRepository.getPoodHomeNewGoodsList(petCategoryIdx, IS_SALE);
    }

    public List<SortedGoodsList> poodMarketGoodsList(final int petCategoryIdx) {
        return goodsRepository.poodMarketGoodsList(petCategoryIdx);
    }

    public boolean existsByPcIdxAndGoodsNameContains(final int petCategoryIdx,
        final List<String> searchList) {
        return goodsRepository.existsByPcIdxAndGoodsNameLikeInSearchList(petCategoryIdx,
            searchList);
    }

    public List<SortedGoodsList> poodMatchGoodsList(final int petCategoryIdx,
        final List<String> searchList) {
        List<SortedGoodsList> resultList = null;
        if (existsByPcIdxAndGoodsNameContains(petCategoryIdx, searchList)) {
            resultList = goodsRepository.poodMatchGoodsList(petCategoryIdx, searchList);
        } else {
            resultList = pageConvertList(goodsRepository.getSortedGoodsList(
                PageRequest.of(0, 99999, Sort.unsorted()), null, null, null, null, petCategoryIdx,
                null));
        }

        Collections.shuffle(resultList);
        return resultList.stream().limit(24).collect(Collectors.toList());
    }

    private List<SortedGoodsList> pageConvertList(Page<SortedGoodsList> sortedGoodsList) {
        return sortedGoodsList.stream().collect(Collectors.toList());
    }

    public List<Integer> getRankGoodsIdxList(final GoodsSubCt goodSubCt, final int pcIdx) {
        return goodsRepository.getRankGoodsIdxList(goodSubCt, pcIdx);
    }

    public List<Integer> getGoodsIdxByPcIdx(final int pcIdx) {
        return goodsRepository.getRankGoodsIdxList(pcIdx);
    }

    public List<SuggestRandomGoodsMapper> suggest10Goods() {
        return goodsRepository.suggestRandom10Goods();
    }

    public boolean existsByIdx(final int goodsIdx) {
        return goodsRepository.existsById(goodsIdx);
    }

    public List<OrderGoodsMapper> myOrderGoodsInfoListByBasket(
        final List<OrderBasket> orderBasketList) {
        return goodsRepository.myOrderGoodsInfoListByBasket(
            orderBasketList.stream().map(OrderBasket::getGoodsIdx).collect(
                Collectors.toList()));
    }

    public List<Integer> getStopWaitGoodsIdxList() {
        return goodsRepository.findAllBySaleStatusIn(Arrays.asList(SOLD_STOP, SOLD_WAIT))
            .stream().map(Goods::getIdx).collect(Collectors.toList());
    }

    public List<SortedGoodsList> getSortedGoodsByProductIdxsOrderReviewCount(
        final List<Integer> productIdxList) {
        return goodsRepository.getSortedGoodsByProductIdxsOrderReviewCount(productIdxList);
    }

    public Page<SortedGoodsList> getNewstGoodsList(final int pcIdx, final Pageable pageable) {
        return goodsRepository.getNewstGoodsList(pcIdx, pageable);
    }

    public CustomRankInfoGroup getCustomRankListNotIn(final List<Integer> goodsIdsList,
        final int pcIdx) {

        return new CustomRankInfoGroup(goodsRepository.getCustomRankListNotIn(goodsIdsList, pcIdx)
            .stream().map(CustomRankInfoDto::of)
            .collect(Collectors.toList()));
    }
}
