package com.pood.server.service;

import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserReview;
import com.pood.server.entity.user.UserReviewSue;
import com.pood.server.repository.user.UserReviewSueRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserReviewSueSeparate {
    private final UserReviewSueRepository userReviewSueRepository;

    public void save(final UserReviewSue userReviewSue){
        userReviewSueRepository.save(userReviewSue);
    }

    public boolean existsByUserReviewAndUserInfo(final UserReview userReview, final UserInfo userInfo) {
        return userReviewSueRepository.existsByUserReviewAndUserInfo(userReview, userInfo);
    }

    public List<Integer> getCheckingReviewIdx() {
        return userReviewSueRepository.getCheckingReviewIdx();
    }


}
