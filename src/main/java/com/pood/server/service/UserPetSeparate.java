package com.pood.server.service;

import com.pood.server.controller.request.userpet.UserPetUpdateRequest;
import com.pood.server.entity.user.UserPet;
import com.pood.server.entity.user.mapper.pet.UserPetTotalCountMapper;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.user.UserPetRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("userTransactionManager")
public class UserPetSeparate {

    private final UserPetRepository userPetRepository;

    public UserPet save(final UserPet userPet) {
        return userPetRepository.save(userPet);
    }

    public UserPet findByIdx(final int userPetIdx) {
        return userPetRepository.findById(userPetIdx)
            .orElseThrow(() -> new NotFoundException("해당하는 펫을 찾을 수 없습니다."));
    }

    public UserPet updateUserPet(final UserPetUpdateRequest userPetUpdateRequest) {
        final UserPet userPet = findByIdx(userPetUpdateRequest.getUserPetIdx());
        userPet.updatePetNameByDto(userPetUpdateRequest);
        return userPet;
    }

    public List<UserPet> findAllByUserIdx(final int userIdx) {
        return userPetRepository.findAllByUserIdx(userIdx);
    }

    public void deleteByUserPetIdx(final int idx) {
        userPetRepository.deleteById(idx);
    }

    public void petOwnerValidation(final int userPetIdx, final int userIdx) {
        if (!userPetRepository.existsByIdxAndUserIdx(userPetIdx, userIdx)) {
            throw new NotFoundException("해당하는 유저의 펫이 없습니다.");
        }
    }

    public boolean existsSerialNumber(final String serialNumber) {
        return userPetRepository.existsBySerialNumber(serialNumber);
    }

    public List<UserPetTotalCountMapper> getPetTotalCount(){
        return userPetRepository.getUserPetTotalCount();
    }

}
