package com.pood.server.service;

import com.pood.server.entity.user.UserPetAiDiagnosis;
import com.pood.server.repository.user.UserPetAiDiagnosisRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("userTransactionManager")
public class UserPetAiDiagnosisSeparate {

    private final UserPetAiDiagnosisRepository userPetAiDiagnosisRepository;

    public List<UserPetAiDiagnosis> getUserPetAiDiagnosisList(final int userPetIdx) {
        return userPetAiDiagnosisRepository.findByUserPetIdx(userPetIdx);
    }

}
