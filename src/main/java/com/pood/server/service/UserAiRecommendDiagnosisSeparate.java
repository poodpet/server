package com.pood.server.service;

import com.pood.server.dto.meta.user.pet.UserPetAiDiagnosisGroup;
import com.pood.server.dto.user.pet.PetWorryCreateGroup;
import com.pood.server.entity.meta.PetWorry;
import com.pood.server.repository.user.UserPetAiDiagnosisRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("userTransactionManager")
public class UserAiRecommendDiagnosisSeparate {

    private final UserPetAiDiagnosisRepository userPetAiDiagnosisRepository;

    public void insertPetWorry(final List<PetWorry> petWorryList, final PetWorryCreateGroup group,
        final int userPetIdx) {

        final UserPetAiDiagnosisGroup diagnosisGroup = new UserPetAiDiagnosisGroup();
        userPetAiDiagnosisRepository.saveAll(
            diagnosisGroup.saveAllObjects(petWorryList, group.getPetWorryCreateRequestList(),
                userPetIdx));
    }

    public void deleteAllByUserPetIdx(final int userPetIdx) {
        userPetAiDiagnosisRepository.deleteAllByUserPetIdx(userPetIdx);
    }

    public List<Long> getUserPetWorryIdxList(final int userPetIdx) {
        return UserPetAiDiagnosisGroup.of(userPetAiDiagnosisRepository.findByUserPetIdx(userPetIdx))
            .getWorryIdxList();
    }

    public UserPetAiDiagnosisGroup findUserPetWorryList(final int userPetIdx) {
        return UserPetAiDiagnosisGroup.of(
            userPetAiDiagnosisRepository.findByUserPetIdxOrderByPriority(userPetIdx));
    }

    public void updateUserPetWorry(final List<PetWorry> petWorryList,
        final PetWorryCreateGroup replaceGroup, final Integer userPetIdx) {

        deleteAllByUserPetIdx(userPetIdx);
        insertPetWorry(petWorryList, replaceGroup, userPetIdx);
    }
}
