package com.pood.server.service;

import com.pood.server.dto.meta.product.ProductDto.DiscontinuedProductList;
import com.pood.server.entity.meta.mapper.ProductIngredientMapper;
import com.pood.server.entity.meta.mapper.solution.ProductGoodsBaseDataMapper;
import com.pood.server.repository.meta.ProductRepository;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.ProductType;
import com.pood.server.util.FeedType;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductSeparate {

    private final ProductRepository productRepository;

    public boolean existsByProductId(final int productIdx) {
        return productRepository.existsById(productIdx);
    }

    public ProductBaseData getSolutionProductDetail(final int goodsIdx, final int productIdx) {
        return productRepository.getSolutionProductDetail(
            goodsIdx, productIdx);
    }

    public List<ProductBaseData> getProductInfoListByGoodsIdx(final int goodsIdx) {
        return productRepository.getProductInfoListByGoodsIdx(goodsIdx);
    }

    public List<ProductBaseData> getAllProductsOnStockAndPoodMarketNotIn(final int pcIdx) {
        return productRepository.getAllProductsOnStock(pcIdx, getZeroStockProductListAndPoodMarketNotIn());
    }

    public ProductBaseData getProductInfoByProductIdx(final int productIdx) {
        return productRepository.getProductInfoByProductIdx(
            productIdx);
    }

    public List<ProductBaseData> getProductInfoListByFeedTarget(final int pcIdx,
        final int feedTarget) {
        return productRepository.getProductInfoListByFeedTarget(pcIdx, feedTarget);
    }

    public List<ProductBaseData> getProductInfoListByWorryIdx(final int pcIdx,
        final long worryIdx) {
        return productRepository.getProductInfoListByWorryIdx(pcIdx, String.valueOf(worryIdx));
    }

    public List<ProductBaseData> getProductInfoListByPcIdxAndProductType(
        final int pcIdx, final ProductType productType) {
        return productRepository.getProductInfoListByPcIdxAndProductType(pcIdx, productType);
    }

    public List<ProductBaseData> getProductInfoListByFeedType(final int pcIdx,
        final FeedType petFeedType) {
        return productRepository.getProductInfoListByFeedType(pcIdx, petFeedType);
    }

    public List<ProductBaseData> getProductInfoListByFeedTypeAndProductType(final int pcIdx,
        final FeedType petFeedType, final ProductType productType) {
        return productRepository.getProductInfoListByFeedTypeAndProductType(pcIdx, petFeedType,
            productType);
    }

    public List<ProductBaseData> getProductInfoListByFeedTargetAndProductTypeAndPoodMarketNotIn(final int pcIdx,
        final int feedTarget, final ProductType productType) {
        return productRepository.getProductInfoListByFeedTargetAndProductTypeAndPoodMarketNotIn(pcIdx, feedTarget,
            productType);
    }

    private List<Integer> getZeroStockProductListAndPoodMarketNotIn() {
        return productRepository.getGoodsOfProductCountListAndPoodMarketNotIn()
            .stream()
            .filter(DiscontinuedProductList::isEmptyProduct)
            .map(DiscontinuedProductList::getIdx)
            .collect(Collectors.toList());
    }

    public List<ProductBaseData> getProductInfoListByWorryIdxIn(final int pcIdx,
        final List<Long> worryIdxList) {
        return productRepository.getProductInfoListByWorryIdxIn(pcIdx, worryIdxList);
    }

    public List<ProductBaseData> getProductInfoListByBestWorryScoreFilter(final int pcIdx,
        final long worryIdx, final FeedType feedType) {

        return productRepository.getProductInfoListByBestWorryScoreFilter(pcIdx,
            String.valueOf(worryIdx), feedType);
    }

    public List<ProductGoodsBaseDataMapper> getProductInfoListByBestWorryScoreAndProductTypeFilter(
        final int pcIdx, final Integer ardCode, final FeedType feedType, final Long goodsCtIdx,
        final List<Long> goodsSubCtIdxList, final List<Integer> alreadyIncludeGoodsIdxList) {

        return productRepository.getProductInfoListByBestWorryScoreFilterNotIn(
                pcIdx, ardCode, feedType, goodsCtIdx, goodsSubCtIdxList, alreadyIncludeGoodsIdxList)
            .orElse(List.of());
    }

    public List<ProductBaseData> getProductInfoListByFeedTypeAndFoodMartNotIn(final int pcIdx,
        final FeedType petFeedType) {
        return productRepository.getProductInfoListByFeedTypeAndFoodMartNotIn(pcIdx, petFeedType);
    }

    public List<ProductBaseData> getProductInfoListByPcIdxAndProductTypeAndFoodMartNotIn(
        final int pcIdx, final ProductType productType) {
        return productRepository.getProductInfoListByPcIdxAndProductType(pcIdx, productType);
    }

    public List<ProductBaseData> getProductInfoListByWorryIdxInAndFoodMartNotIn(final int pcIdx,
        final List<Long> worryIdxList) {
        return productRepository.getProductInfoListByWorryIdxInAndFoodMartNotIn(pcIdx, worryIdxList);
    }

    public List<ProductIngredientMapper> findAllIngredientInIdxList(
        final List<Integer> productIdxList) {
        return productRepository.findAllIngredientInIdxList(productIdxList);
    }
}
