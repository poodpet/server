package com.pood.server.service;

import com.pood.server.entity.user.UserEventCustomBlock;
import com.pood.server.exception.AlreadyExistException;
import com.pood.server.repository.user.UserEventCustomBlockRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional("userTransactionManager")
@RequiredArgsConstructor
public class UserPostingBlockSeparate {

    private final UserEventCustomBlockRepository blockRepository;

    public void save(final UserEventCustomBlock userEventCustomBlock) {

        if (blockRepository.existsByMyInfoIdxAndEventInfoIdxAndUserInfo(userEventCustomBlock.getMyInfoIdx(),
            userEventCustomBlock.getEventInfoIdx(), userEventCustomBlock.getUserInfo())) {
            throw new AlreadyExistException("이미 해당 게시물의 차단 기록이 존재합니다.");
        }

        blockRepository.save(userEventCustomBlock);
    }

}
