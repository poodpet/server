package com.pood.server.service;

import com.pood.server.entity.user.UserPetFeed;
import com.pood.server.repository.user.UserPetFeedRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("userTransactionManager")
public class UserPetFeedSeparate {

    private final UserPetFeedRepository userPetFeedRepository;

    public void save(final UserPetFeed userPetFeed) {
        userPetFeedRepository.save(userPetFeed);
    }

    public void deleteByUserPetIdx(final int userPetIdx) {
        userPetFeedRepository.deleteByUserPetIdx(userPetIdx);
    }
}
