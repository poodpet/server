package com.pood.server.service;

import com.pood.server.entity.meta.GoodsCt;
import com.pood.server.repository.meta.GoodsCtRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GoodsCtService {

   private final GoodsCtRepository goodsCtRepository;

   public List<GoodsCt> getGoodsCtList() {
      return goodsCtRepository.findAll();
   }


}
