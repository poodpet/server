package com.pood.server.service;

import static com.pood.server.exception.ErrorMessage.MEMBER;

import com.pood.server.dto.meta.goods.GoodsCtDto;
import com.pood.server.dto.meta.goods.GoodsDto;
import com.pood.server.dto.meta.goods.PromotionDto;
import com.pood.server.dto.meta.product.ProductDto;
import com.pood.server.entity.meta.Feed;
import com.pood.server.entity.meta.GoodsImage;
import com.pood.server.entity.meta.GoodsSubCt;
import com.pood.server.entity.meta.ProductImage;
import com.pood.server.entity.user.UserWish;
import com.pood.server.exception.CustomStatusException;
import com.pood.server.repository.meta.FeedRepository;
import com.pood.server.repository.meta.GoodsCtRepository;
import com.pood.server.repository.meta.GoodsFilterCtRepository;
import com.pood.server.repository.meta.GoodsImageRepository;
import com.pood.server.repository.meta.GoodsSubCtRepository;
import com.pood.server.repository.meta.ProductImageRepository;
import com.pood.server.repository.meta.ProductRepository;
import com.pood.server.repository.meta.PromotionRepository;
import com.pood.server.repository.user.UserInfoRepository;
import com.pood.server.repository.user.UserReviewBlockRepository;
import com.pood.server.repository.user.UserTokenRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoodsServiceV2 {

    @Autowired
    GoodsSeparate goodsSeparate;

    @Autowired
    UserTokenRepository userTokenRepository;

    @Autowired
    UserInfoRepository userInfoRepository;

    @Autowired
    PromotionRepository promotionRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductImageRepository productImageRepository;

    @Autowired
    GoodsImageRepository goodsImageRepository;

    @Autowired
    GoodsCtRepository goodsCtRepository;

    @Autowired
    GoodsSubCtRepository goodsSubCtRepository;

    @Autowired
    FeedRepository feedRepository;

    @Autowired
    GoodsFilterCtRepository goodsFilterCtRepository;

    @Autowired
    UserReviewBlockRepository userReviewBlockRepository;

    public GoodsDto.GoodsDetail getGoodsDetail(Integer idx) {
        GoodsDto.GoodsDetail goods = goodsSeparate.findByGoodsIdValidation(idx);
        List<ProductDto.ProductDetail> productDetails = productRepository.getProductListByGoodsIdx(
            idx);
        goods.setProductList(productDetails);

        PromotionDto.PromotionProductInfo promotion = promotionRepository.findByGoodsIdxAndNowActive(
            idx);
        goods.setPromotion(promotion);

        List<GoodsImage> goodsImageList = goodsImageRepository.findAllByGoodsIdx(idx);
        goods.setGoodsImages(goodsImageList);

        productDetails.forEach(x -> {
            List<ProductImage> productImageList = productImageRepository.findAllByProductIdx(
                x.getIdx());
            Feed feed = feedRepository.findByProductIdx(x.getIdx());
            x.setProductImageList(productImageList);
            x.setFeed(feed);
            if (goods.getMainProduct().equals(x.getIdx())) {
                goods.setMainImages(productImageList);
            }
        });

        if (promotion != null) {
            goods.setDiscountRate(promotion.getPrDiscountRate());
            goods.setGoodsPrice(promotion.getPrPrice());
            goods.setIsPromotion(Boolean.TRUE);

        }

        return goods;
    }


    public GoodsDto.GoodsDetail getGoodsBasketDetail(Integer idx) {
        GoodsDto.GoodsDetail goods = goodsSeparate.findByGoodsIdGetNull(idx);
        if (Objects.isNull(goods)) {
            return null;
        }

        List<ProductDto.ProductDetail> productDetails = productRepository.getProductListByGoodsIdx(
            idx);
        goods.setProductList(productDetails);

        PromotionDto.PromotionProductInfo promotion = promotionRepository.findByGoodsIdxAndNowActive(
            idx);
        goods.setPromotion(promotion);

        List<GoodsImage> goodsImageList = goodsImageRepository.findAllByGoodsIdx(idx);
        goods.setGoodsImages(goodsImageList);

        productDetails.forEach(x -> {
            List<ProductImage> productImageList = productImageRepository.findAllByProductIdx(
                x.getIdx());
            Feed feed = feedRepository.findByProductIdx(x.getIdx());
            x.setProductImageList(productImageList);
            x.setFeed(feed);
            if (goods.getMainProduct().equals(x.getIdx())) {
                goods.setMainImages(productImageList);
            }
        });

        if (promotion != null) {
            goods.setDiscountRate(promotion.getPrDiscountRate());
            goods.setGoodsPrice(promotion.getPrPrice());
            goods.setIsPromotion(Boolean.TRUE);

        }

        return goods;
    }


    public List<GoodsCtDto.GoodsCtListDto> getGoodsCt() {
        return goodsCtRepository.findAll().stream()
            .map(x -> new GoodsCtDto.GoodsCtListDto(x.getIdx(), x.getName(), x.getUrl()))
            .collect(Collectors.toList());
    }

    public List<GoodsCtDto.GoodsSubCtListDto> getGoodsSubCts(Long ctIdx) {
        return goodsSubCtRepository.findAllByGoodsCtIdx(ctIdx).stream().map(
            x -> new GoodsCtDto.GoodsSubCtListDto(x.getIdx(), x.getGoodsCt().getIdx(), x.getName(),
                x.getUrl())).collect(Collectors.toList());
    }

    public List<GoodsCtDto.GoodsCtDetailDto> geGoodsCtDetailDtos(Integer pcIdx) {
        List<GoodsCtDto.GoodsCtDetailDto> goodsCtListDtos = goodsCtRepository.findAll().stream()
            .map(x -> new GoodsCtDto.GoodsCtDetailDto(x.getIdx(), x.getName(), x.getUrl()))
            .collect(Collectors.toList());
        List<GoodsSubCt> allGoodsSubCts = goodsSubCtRepository.findAll();

        for (GoodsCtDto.GoodsCtDetailDto goodsCtListDto : goodsCtListDtos) {
            List<GoodsCtDto.GoodsSubCtListDto> goodsSubCtListDtos = new ArrayList<>();

            for (GoodsSubCt allGoodsSubCt : allGoodsSubCts) {
                if (goodsCtListDto.getIdx().equals(allGoodsSubCt.getGoodsCt().getIdx())
                    && allGoodsSubCt.getPetCategory().getIdx().equals(pcIdx)) {
                    GoodsCtDto.GoodsSubCtListDto goodsSubCtListDto = new GoodsCtDto.GoodsSubCtListDto(
                        allGoodsSubCt.getIdx(), allGoodsSubCt.getGoodsCt().getIdx(),
                        allGoodsSubCt.getName(), allGoodsSubCt.getUrl());
                    goodsSubCtListDtos.add(goodsSubCtListDto);
                }
            }
            goodsCtListDto.setGoodsSubCtListDtos(goodsSubCtListDtos);
        }
        return goodsCtListDtos;
    }

    public List<GoodsDto.GoodsFilterCtDto> getGoodsFilterCtList(Integer pcIdx) {

        return goodsFilterCtRepository.findAllByPetCtIdx(
                pcIdx)
            .stream().map(x -> new GoodsDto.GoodsFilterCtDto(x.getIdx(), x.getGoodsCt().getIdx(),
                x.getPetCt().getIdx(), x.getUrl(), x.getName(), x.getType(), x.getFieldKey(),
                x.getFieldValue())).collect(Collectors.toList());

    }


}
