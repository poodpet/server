package com.pood.server.service;


import static com.pood.server.exception.ErrorMessage.ALREADY_PARTICIPATE_EVENT;
import static com.pood.server.exception.ErrorMessage.API_TOKEN;
import static com.pood.server.exception.ErrorMessage.ATTEND_CHANCE;
import static com.pood.server.exception.ErrorMessage.COMMENT;
import static com.pood.server.exception.ErrorMessage.EVENT;
import static com.pood.server.exception.ErrorMessage.IMAGE;
import static com.pood.server.exception.ErrorMessage.PET;
import static com.pood.server.exception.ErrorMessage.PHOTO;
import static com.pood.server.exception.ErrorMessage.SERVER;
import static com.pood.server.exception.ErrorMessage.USER;

import com.pood.server.api.file.uploader.StorageService;
import com.pood.server.controller.response.event.RunningEvent;
import com.pood.server.dto.meta.event.EventDto;
import com.pood.server.dto.meta.event.EventDto.MyJoinEventDetail;
import com.pood.server.entity.group.EventExceptUserGroup;
import com.pood.server.entity.meta.EventComment;
import com.pood.server.entity.meta.EventImage;
import com.pood.server.entity.meta.EventInfo;
import com.pood.server.entity.meta.EventParticipation;
import com.pood.server.entity.meta.EventPhoto;
import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.user.UserBaseImage;
import com.pood.server.entity.user.UserEventCustomBlock;
import com.pood.server.entity.user.UserEventReport;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserPet;
import com.pood.server.entity.user.UserPetImage;
import com.pood.server.exception.CustomStatusException;
import com.pood.server.exception.InvalidTokenException;
import com.pood.server.repository.meta.EventCommentRepository;
import com.pood.server.repository.meta.EventImageRepository;
import com.pood.server.repository.meta.EventParticipationRepository;
import com.pood.server.repository.meta.EventPhotoRepository;
import com.pood.server.repository.meta.EventRepository;
import com.pood.server.repository.meta.PetRepository;
import com.pood.server.repository.user.UserBaseImageRepository;
import com.pood.server.repository.user.UserEventCustomBlockRepository;
import com.pood.server.repository.user.UserEventReportRepository;
import com.pood.server.repository.user.UserInfoRepository;
import com.pood.server.repository.user.UserPetImageRepository;
import com.pood.server.repository.user.UserPetRepository;
import com.pood.server.util.UserStatus;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;


@Service
@RequiredArgsConstructor
public class EventService {

    Logger logger = LoggerFactory.getLogger(EventService.class);

    private final EventRepository eventRepository;
    private final EventCommentRepository eventCommentRepository;
    private final EventPhotoRepository eventPhotoRepository;
    private final EventParticipationRepository eventParticipationRepository;
    private final UserInfoRepository userInfoRepository;
    private final UserPetRepository userPetRepository;
    private final UserBaseImageRepository userBaseImageRepository;
    private final UserPetImageRepository userPetImageRepository;
    private final EventImageRepository eventImageRepository;
    private final PetRepository petRepository;
    private final UserEventCustomBlockRepository blockRepository;
    private final UserEventReportRepository reportRepository;

    @Qualifier("storageService")
    private final StorageService storageService;


    public List<RunningEvent> getProgressEvents(final int petCategoryIdx, final Pageable pageable) {
        return eventRepository.getProgressEvents(petCategoryIdx,
                pageable).getContent()
            .stream()
            .map(event -> new RunningEvent(event.getIdx(), event.getTitle(),
                event.getIntro(), event.getStartDate(), event.getEndDate(), event.getImgUrl(),
                event.getType(), event.getTypeName(), event.getPcIdx())).collect(Collectors.toList());
    }

    public EventDto.EventDetail getEventDetail(Integer idx, String token) {
        EventDto.EventDetail eventDetail = eventRepository.getEventDetail(idx);
        Optional.ofNullable(eventDetail).orElseThrow(
            () -> CustomStatusException.badRequest(EVENT));

        eventDetail.setParticipationPossible(isParticipationPossible(token, eventDetail));

        List<EventImage> eventImageList = eventImageRepository.findAllByEventIdxAndType(idx,
            1);
        eventDetail.setEventImageList(eventImageList);
        return eventDetail;
    }

    public void insertEventComment(EventDto.EventComment eventCommentDto, String token) {
        Optional.ofNullable(token).orElseThrow(() -> CustomStatusException.unAuthorized(API_TOKEN));
        //등록 제한
        //이벤트 type 체크
        UserInfo userInfo = userInfoRepository.getUserByTokenOptional(token)
            .orElseThrow(() -> CustomStatusException.unAuthorized(USER));

        EventInfo eventInfo = eventRepository.findById(eventCommentDto.getEventInfoIdx())
            .orElseThrow(
                () -> CustomStatusException.badRequest(EVENT));
        EventComment eventComment = new EventComment(eventInfo, userInfo.getIdx(),
            eventCommentDto.getComment());

        List<EventComment> eventCommentList = eventCommentRepository.findByEventInfoIdxAndUserInfoIdx(
            eventInfo.getIdx(), userInfo.getIdx());
        if (eventInfo.getAttendLimit() <= eventCommentList.size()) {
            throw CustomStatusException.badRequest(ATTEND_CHANCE);
        }
        eventCommentRepository.save(eventComment);
    }

    public Page<EventDto.EventUserCommentDto> getEventCommentList(final Integer idx,
        final Pageable pageable, final String token) {

        EventExceptUserGroup exceptUserGroup = new EventExceptUserGroup(getBlockUserInfo(token),
            getReportUserList(), getResignUserList());

        Page<EventDto.EventUserComment> eventCommentList = eventRepository.getEventCommentList(idx,
            pageable, exceptUserGroup.getList());
        List<Integer> userIdxList = eventCommentList.stream()
            .map(EventDto.EventUserComment::getUserInfoIdx).collect(Collectors.toList());
        List<UserInfo> allById = userInfoRepository.findAllById(userIdxList);
        return eventCommentList
            .map(x -> {
                Optional<UserInfo> first = allById.stream()
                    .filter(y -> y.getIdx().equals(x.getUserInfoIdx())).findFirst();
                UserInfo userInfo = first.orElseThrow(
                    () -> CustomStatusException.serverError(SERVER));
                if (userInfo.getUserBaseImage() == null) {
                    UserBaseImage userBaseImage = userBaseImageRepository.findAll().stream()
                        .findFirst().orElseThrow(
                            () -> CustomStatusException.badRequest(IMAGE));
                    userInfo.setUserBaseImage(userBaseImage);
                }
                return new EventDto.EventUserCommentDto(x.getIdx(), x.getUserInfoIdx(),
                    userInfo.getUserNickname(), x.getComment(), x.getRecordbirth(),
                    userInfo.getUserBaseImage().getUrl());
            });

    }

    private List<Integer> getResignUserList() {
        return userInfoRepository.
            findAllByUserStatusIn(List.of(UserStatus.TO_RESIGN, UserStatus.RESIGNED))
            .stream()
            .map(UserInfo::getIdx).collect(
                Collectors.toList());
    }

    public EventPhoto uploadEventPhotoImg(Integer eventIdx, MultipartFile files, String token)
        throws IOException {
        Optional.ofNullable(token).orElseThrow(
            () -> CustomStatusException.unAuthorized(API_TOKEN));
        Optional.ofNullable(files).orElseThrow(
            () -> CustomStatusException.badRequest(IMAGE));
        UserInfo userInfo = userInfoRepository.getUserByTokenOptional(token)
            .orElseThrow(() -> CustomStatusException.unAuthorized(USER));
        logger.info("uploadEventPhotoImg : " + files.getSize());
        EventInfo eventInfo = eventRepository.findById(eventIdx)
            .orElseThrow(() -> CustomStatusException.badRequest(EVENT));

        List<EventPhoto> eventPhotoList = eventPhotoRepository.findByEventInfoIdxAndUserInfoIdx(
            eventInfo.getIdx(), userInfo.getIdx());

        if (eventInfo.getAttendLimit() <= eventPhotoList.size()) {
            throw CustomStatusException.badRequest(ATTEND_CHANCE);
        }
//        eventInfo.getPhotoLimit()

        String imgUrl = storageService.multiPartFileStore(files, "event_photo");
        EventPhoto magazineImage = new EventPhoto(userInfo.getIdx(), eventInfo, imgUrl);
        return eventPhotoRepository.save(magazineImage);

    }

    public Page<EventDto.EventUserPhotoDto> getEventPhotoImgList(Integer idx, Pageable pageable,
        final String token) {
        EventExceptUserGroup exceptUserGroup = new EventExceptUserGroup(getBlockUserInfo(token),
            getReportUserList(), getResignUserList());

        Page<EventDto.EventUserPhoto> eventCommentList = eventRepository.getEventPhotoImgList(idx,
            pageable, exceptUserGroup.getList());

        List<Integer> userIdxList = eventCommentList.stream()
            .map(EventDto.EventUserPhoto::getUserInfoIdx).collect(Collectors.toList());

        List<UserInfo> userInfoList = userInfoRepository.findAllById(userIdxList);

        return eventCommentList
            .map(x -> {
                Optional<UserInfo> first = userInfoList.stream()
                    .filter(y -> y.getIdx().equals(x.getUserInfoIdx())).findFirst();
                UserInfo userInfo = first.orElseThrow(
                    () -> CustomStatusException.serverError(SERVER));
                return new EventDto.EventUserPhotoDto(x.getIdx(), x.getUserInfoIdx(),
                    userInfo.getUserNickname(), x.getUrl(), x.getRecordbirth());
            });
    }

    private List<Integer> getReportUserList() {
        final List<UserEventReport> userEventReports = reportRepository.reportUserList();

        return userEventReports.stream()
            .map(userEventReport -> userEventReport.getUserInfo().getIdx())
            .collect(Collectors.toList());
    }

    public void participationEvent(EventDto.EventIdx eventIdx, String token) {
        Optional.ofNullable(token).orElseThrow(
            () -> CustomStatusException.unAuthorized(API_TOKEN));
        UserInfo userInfo = userInfoRepository.getUserByTokenOptional(token)
            .orElseThrow(() -> CustomStatusException.unAuthorized(USER));
        EventInfo eventInfo = eventRepository.findById(eventIdx.getIdx()).orElseThrow(
            () -> CustomStatusException.badRequest(EVENT));

        eventInfo.checkValidation();

        Integer participationEventCnt = eventParticipationRepository.countByUserInfoIdxAndEventInfoIdx(
            userInfo.getIdx(), eventInfo.getIdx());
        if (participationEventCnt != 0) {
            throw CustomStatusException.badRequest(ALREADY_PARTICIPATE_EVENT);
        }

        EventParticipation eventParticipation = new EventParticipation(eventInfo,
            userInfo.getIdx());
        eventParticipationRepository.save(eventParticipation);
    }

    public void deleteEventComment(Integer eventIdx, Long idx, String token) {
        Optional.ofNullable(token).orElseThrow(
            () -> CustomStatusException.unAuthorized(API_TOKEN));
        UserInfo userInfo = userInfoRepository.getUserByTokenOptional(token)
            .orElseThrow(() -> CustomStatusException.unAuthorized(USER));
        if (!eventRepository.existsById(eventIdx)) {
            throw CustomStatusException.badRequest(EVENT);
        }

        EventComment eventCommentInfo = eventCommentRepository.findByIdxAndEventInfoIdxAndUserInfoIdx(
            idx, eventIdx, userInfo.getIdx());
        Optional.ofNullable(eventCommentInfo).orElseThrow(
            () -> CustomStatusException.badRequest(COMMENT));

        eventCommentRepository.delete(eventCommentInfo);

    }

    public void deleteEventPhotoImg(Integer eventIdx, Long idx, String token) {
        Optional.ofNullable(token).orElseThrow(
            () -> CustomStatusException.unAuthorized(API_TOKEN));

        UserInfo userInfo = userInfoRepository.getUserByTokenOptional(token)
            .orElseThrow(() -> CustomStatusException.unAuthorized(USER));

        if (!eventRepository.existsById(eventIdx)) {
            throw CustomStatusException.badRequest(EVENT);
        }

        EventPhoto eventPhotoInfo = eventPhotoRepository.findByIdxAndEventInfoIdxAndUserInfoIdx(idx,
            eventIdx, userInfo.getIdx());
        Optional.ofNullable(eventPhotoInfo).orElseThrow(
            () -> CustomStatusException.badRequest(PHOTO));
        eventPhotoRepository.delete(eventPhotoInfo);
        storageService.deleteObject(eventPhotoInfo.getUrl());
    }

    public EventDto.EventPhotoUserDetail getEventPhotoImgDetail(Integer eventIdx, Integer userIdx) {

        EventInfo eventInfo = eventRepository.findById(eventIdx).orElseThrow(
            () -> CustomStatusException.badRequest(EVENT));
        UserInfo userInfo = userInfoRepository.findById(userIdx).orElseThrow(
            () -> CustomStatusException.badRequest(USER));
        if (userInfo.getUserBaseImage() == null) {
            UserBaseImage userBaseImage = userBaseImageRepository.findAll().stream().findFirst()
                .orElseThrow(() -> CustomStatusException.badRequest(IMAGE));
            userInfo.setUserBaseImage(userBaseImage);
        }
        EventDto.UserInfoSimpleData userInfoSimpleData = new EventDto.UserInfoSimpleData(
            userInfo.getIdx(), userInfo.getUserNickname(), userInfo.getUserBaseImage());
        // 사용자 정보

        List<EventDto.EventPhotoSimpleData> photoSimpleDataList = eventPhotoRepository.findByEventInfoIdxAndUserInfoIdx(
                eventInfo.getIdx(), userInfo.getIdx()).stream()
            .map(x -> new EventDto.EventPhotoSimpleData(x.getIdx(), x.getUrl()))
            .collect(Collectors.toList());
        // 사용자 포토 리스트

        List<EventDto.UserPetSimpleData> userPetSimpleDataList = new ArrayList<>();
        List<UserPet> userPetList = userPetRepository.findAllByUserIdx(userIdx);
        for (UserPet userPet : userPetList) {
            UserPetImage petImage = userPetImageRepository.findByUserPetIdxOrderByIdxDesc(
                userPet.getIdx()).stream().findFirst().orElse(null);
            Pet pet = petRepository.findById(userPet.getPscId()).orElseThrow(
                () -> CustomStatusException.badRequest(PET));
            EventDto.UserPetSimpleData petDate = new EventDto.UserPetSimpleData(userPet.getIdx(),
                petImage, userPet.getPetBirth(), pet.getPcSize(), userPet.getPetName(),
                pet.getPcKind(), pet.getPcId());
            userPetSimpleDataList.add(petDate);
        }
        return new EventDto.EventPhotoUserDetail(
            userPetSimpleDataList, photoSimpleDataList, userInfoSimpleData);

    }

    public List<EventDto.MyEventList> getMyEventList(String token) {
        Optional.ofNullable(token).orElseThrow(
            () -> CustomStatusException.unAuthorized(API_TOKEN));
        UserInfo userInfo = userInfoRepository.getUserByTokenOptional(token)
            .orElseThrow(() -> CustomStatusException.unAuthorized(USER));
        List<EventDto.MyEventList> myEventLists = new ArrayList<>();
        List<EventDto.MyEventList> myEventCommentList = eventRepository.getMyEventCommentList(
            userInfo.getIdx());
        List<EventDto.MyEventList> myEventPhotoList = eventRepository.getMyEventPhotoList(
            userInfo.getIdx());
        List<EventDto.MyEventList> myEventParticipationList = eventRepository.getMyEventParticipationList(
            userInfo.getIdx());
        myEventLists.addAll(myEventCommentList);
        myEventLists.addAll(myEventPhotoList);
        myEventLists.addAll(myEventParticipationList);
        myEventLists = myEventLists.stream()
            .sorted(Comparator.comparing(EventDto.MyEventList::getEndDate).reversed())
            .collect(Collectors.toList());
        return myEventLists;
    }

    public void deleteEventJoin(String token, Integer eventIdx) {
        Optional.ofNullable(token).orElseThrow(
            () -> CustomStatusException.unAuthorized(API_TOKEN));
        UserInfo userInfo = userInfoRepository.getUserByTokenOptional(token)
            .orElseThrow(() -> CustomStatusException.unAuthorized(USER));
        if (!eventRepository.existsById(eventIdx)) {
            throw CustomStatusException.badRequest(EVENT);
        }
        EventParticipation myEventParticipation = eventParticipationRepository.findByEventInfoIdxAndUserInfoIdx(
            eventIdx, userInfo.getIdx());
        eventParticipationRepository.delete(myEventParticipation);

    }

    public void deleteEventPhoto(String token, Integer eventIdx) {
        Optional.ofNullable(token).orElseThrow(
            () -> CustomStatusException.unAuthorized(API_TOKEN));
        UserInfo userInfo = userInfoRepository.getUserByTokenOptional(token)
            .orElseThrow(() -> CustomStatusException.unAuthorized(USER));
        if (!eventRepository.existsById(eventIdx)) {
            throw CustomStatusException.badRequest(EVENT);
        }
        List<EventPhoto> myEventPhotoList = eventPhotoRepository.findByEventInfoIdxAndUserInfoIdx(
            eventIdx, userInfo.getIdx());
        for (EventPhoto eventPhoto : myEventPhotoList) {
            Optional.ofNullable(eventPhoto).orElseThrow(
                () -> CustomStatusException.badRequest(PHOTO));
            eventPhotoRepository.delete(eventPhoto);
            storageService.deleteObject(eventPhoto.getUrl());
        }
    }

    public void deleteEventComment(String token, Integer eventIdx) {
        Optional.ofNullable(token).orElseThrow(
            () -> CustomStatusException.unAuthorized(API_TOKEN));
        UserInfo userInfo = userInfoRepository.getUserByTokenOptional(token)
            .orElseThrow(() -> CustomStatusException.unAuthorized(USER));
        if (!eventRepository.existsById(eventIdx)) {
            throw CustomStatusException.badRequest(EVENT);
        }
        List<EventComment> myEventCommentList = eventCommentRepository.findByEventInfoIdxAndUserInfoIdx(
            eventIdx, userInfo.getIdx());
        for (EventComment eventComment : myEventCommentList) {
            Optional.ofNullable(eventComment).orElseThrow(
                () -> CustomStatusException.badRequest(COMMENT));
            eventCommentRepository.delete(eventComment);
        }
    }

    public EventDto.MyCommentEventDetail getMyCommentEvent(final String token,
        final Integer eventIdx) {
        Optional.ofNullable(token).orElseThrow(
            () -> CustomStatusException.unAuthorized(API_TOKEN));
        UserInfo userInfo = userInfoRepository.getUserByTokenOptional(token)
            .orElseThrow(() -> CustomStatusException.unAuthorized(USER));
        EventInfo eventInfo = eventRepository.findById(eventIdx).orElseThrow(
            () -> CustomStatusException.badRequest(EVENT));
        EventDto.MyCommentEventDetail myCommentEventDetail = eventRepository.findByCommentEventDetail(
            eventIdx, userInfo.getIdx());

        List<EventComment> myCommentEventList = eventCommentRepository.findByEventInfoIdxAndUserInfoIdx(
            eventIdx, userInfo.getIdx());
        List<EventDto.CommentEventDetail> eventDetailList = myCommentEventList
            .stream().map(
                x -> new EventDto.CommentEventDetail(x.getIdx(), x.getUserInfoIdx(), x.getComment(),
                    x.getRecordbirth())).collect(Collectors.toList());
        if (eventDetailList.isEmpty()) {
            throw CustomStatusException.badRequest(EVENT);
        }

        myCommentEventDetail.setRanking(getCommentRankStr(eventInfo, myCommentEventList));
        myCommentEventDetail.setCommentEventDetails(eventDetailList);

        return myCommentEventDetail;
    }

    private String getCommentRankStr(final EventInfo eventInfo,
        final List<EventComment> myCommentEventList) {
        for (EventComment eventComment : myCommentEventList) {
            if (eventInfo.isRunning(LocalDateTime.now())) {
                return "R";
            }

            if (eventComment.isPrize()) {
                return "O";
            }
        }
        return "N";
    }

    public EventDto.MyPhotoEventDetail getMyPhotoEvent(String token, Integer eventIdx) {
        Optional.ofNullable(token).orElseThrow(
            () -> CustomStatusException.unAuthorized(API_TOKEN));
        UserInfo userInfo = userInfoRepository.getUserByTokenOptional(token)
            .orElseThrow(() -> CustomStatusException.unAuthorized(USER));
        EventInfo eventInfo = eventRepository.findById(eventIdx).orElseThrow(
            () -> CustomStatusException.badRequest(EVENT));
        EventDto.MyPhotoEventDetail byPhotoEventDetail = eventRepository.findByPhotoEventDetail(
            eventIdx, userInfo.getIdx());

        List<EventPhoto> myEventPhotoList = eventPhotoRepository.findByEventInfoIdxAndUserInfoIdx(
            eventIdx, userInfo.getIdx());
        List<EventDto.PhotoEventDetail> eventDetailList = myEventPhotoList
            .stream().map(
                x -> new EventDto.PhotoEventDetail(x.getIdx(), x.getUserInfoIdx(), x.getUrl(),
                    x.getRecordbirth())).collect(Collectors.toList());
        if (eventDetailList.isEmpty()) {
            throw CustomStatusException.badRequest(EVENT);
        }

        byPhotoEventDetail.setRanking(getPhotoRankStr(eventInfo, myEventPhotoList));
        byPhotoEventDetail.setPhotoEventDetails(eventDetailList);

        return byPhotoEventDetail;
    }

    private String getPhotoRankStr(final EventInfo eventInfo,
        final List<EventPhoto> myEventPhotoList) {
        for (EventPhoto eventPhoto : myEventPhotoList) {
            if (eventInfo.isRunning(LocalDateTime.now())) {
                return "R";
            }

            if (eventPhoto.isPrize()) {
                return "O";
            }
        }
        return "N";
    }

    public EventDto.MyJoinEventDetail getMyJoinEvent(String token, Integer eventIdx) {
        Optional.ofNullable(token).orElseThrow(
            () -> CustomStatusException.unAuthorized(API_TOKEN));
        UserInfo userInfo = userInfoRepository.getUserByTokenOptional(token)
            .orElseThrow(() -> CustomStatusException.unAuthorized(USER));
        if (!eventRepository.existsById(eventIdx)) {
            throw CustomStatusException.badRequest(EVENT);
        }
        final Optional<MyJoinEventDetail> byJoinEventDetail = eventRepository.findByJoinEventDetail(
            eventIdx, userInfo.getIdx());
        return byJoinEventDetail.orElseThrow(
            () -> CustomStatusException.badRequest(EVENT));
    }


    private boolean isParticipationPossible(String token, EventDto.EventDetail eventDetail) {
        if (token == null || token.equals("")) {
            return true;
        } else {
            UserInfo userInfo = userInfoRepository.getUserByToken(token);
            if (userInfo == null) {
                return true;
            } else {
                if (eventDetail.getTypeName().equals("C")) {
                    List<EventComment> eventCommentList = eventCommentRepository.findByEventInfoIdxAndUserInfoIdx(
                        eventDetail.getIdx(), userInfo.getIdx());
                    if (eventDetail.getAttendLimit() <= eventCommentList.size()) {
                        return false;
                    }
                } else if (eventDetail.getTypeName().equals("D")) {
                    List<EventPhoto> eventPhotoList = eventPhotoRepository.findByEventInfoIdxAndUserInfoIdx(
                        eventDetail.getIdx(), userInfo.getIdx());
                    if (eventDetail.getAttendLimit() <= eventPhotoList.size()) {
                        return false;
                    }
                } else if (eventDetail.getTypeName().equals("E")) {
                    Integer participationEventCnt = eventParticipationRepository.countByUserInfoIdxAndEventInfoIdx(
                        userInfo.getIdx(), eventDetail.getIdx());
                    if (participationEventCnt != 0) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private List<Integer> getBlockUserInfo(final String token) {
        if (!StringUtils.isEmpty(token)) {
            final UserInfo user = userInfoRepository.getUserByTokenOptional(token)
                .orElseThrow(() -> new InvalidTokenException("토큰이 유효하지 않습니다."));
            final List<UserEventCustomBlock> myBlockList = blockRepository.findAllByMyInfoIdx(
                user.getIdx());
            return myBlockList.stream().map(blockEvent -> blockEvent.getUserInfo().getIdx())
                .collect(Collectors.toList());
        }
        return null;
    }
}