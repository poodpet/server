package com.pood.server.service;

import com.pood.server.controller.response.home.delivery.PoodDeliveryCategoryDto;
import com.pood.server.repository.meta.PoodDeliveryCategoryRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PoodDeliveryCategorySeparate {

    private final PoodDeliveryCategoryRepository poodDeliveryCategoryRepository;

    public List<PoodDeliveryCategoryDto> getCategoryList(final int pcIdx) {
        return poodDeliveryCategoryRepository.getCategoryList(pcIdx);
    }

}
