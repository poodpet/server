package com.pood.server.service;

import com.pood.server.entity.meta.PromotionGroup;
import com.pood.server.repository.meta.PromotionGroupRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PromotionGroupSeparate {

    private final PromotionGroupRepository promotionGroupRepository;

    public List<PromotionGroup> promotionGroupListByPromotionIdx(final int idx) {
        return promotionGroupRepository.findByPrIdx(idx);
    }

}
