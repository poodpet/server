package com.pood.server.service;

import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserReview;
import com.pood.server.entity.user.UserReviewBlock;
import com.pood.server.repository.user.UserReviewBlockRepository;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserReviewBlockSeparate {

    private final UserReviewBlockRepository userReviewBlockRepository;

    public boolean existsByUserReviewAndUserInfo(final UserReview userReview,
        final UserInfo userInfo) {
        return userReviewBlockRepository.existsByUserReviewAndUserInfo(userReview, userInfo);
    }

    public void save(final UserReviewBlock userReviewBlock) {
        userReviewBlockRepository.save(userReviewBlock);
    }

    public List<UserReviewBlock> getUserReviewBlockByUserInfo(final UserInfo userInfo) {
        return userReviewBlockRepository.findAllByUserInfo(userInfo);
    }

    public List<Integer> getReviewIdxList(final UserInfo userInfo) {
        return getUserReviewBlockByUserInfo(userInfo)
            .stream()
            .map(UserReviewBlock::getReviewIdx)
            .collect(Collectors.toList());
    }


}
