package com.pood.server.service;

import com.pood.server.controller.response.pet.UserPetTotalCountResponse;
import com.pood.server.entity.group.UserPetTotalCountGroup;
import com.pood.server.entity.meta.IntroMarketingImage;
import com.pood.server.facade.Facade;
import java.util.List;
import lombok.RequiredArgsConstructor;

@Facade
@RequiredArgsConstructor
public class MainFacade {

    private final IntroMarketingImageSeparate mainSeparate;
    private final UserPetSeparate userPetSeparate;

    public List<IntroMarketingImage> getActiveIntroMarketingImageList() {
        return mainSeparate.getActiveIntroMarketingImageList();
    }

    public UserPetTotalCountResponse getPetTotalCount() {
        return new UserPetTotalCountGroup(userPetSeparate.getPetTotalCount()).getResponse();
    }

}
