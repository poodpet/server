package com.pood.server.service;

import com.pood.server.entity.user.PetAllergyJoinView;
import com.pood.server.repository.user.PetAllergyJoinViewRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("userTransactionManager")
public class PetAllergyJoinViewSeparate {

    private final PetAllergyJoinViewRepository petAllergyJoinViewRepository;

    public List<PetAllergyJoinView> findAllByUserPetIdx(final List<Integer> userIdxList) {
        return petAllergyJoinViewRepository.findAllByUserPetIdxIn(userIdxList);
    }

}
