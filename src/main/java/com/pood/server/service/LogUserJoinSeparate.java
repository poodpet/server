package com.pood.server.service;

import com.pood.server.controller.request.user.UserSignUpRequest;
import com.pood.server.entity.log.LogUserJoin;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.log.LogUserJoinRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service
@RequiredArgsConstructor
@Transactional("logTransactionManager")
public class LogUserJoinSeparate {

    private final LogUserJoinRepository logUserJoinRepository;

    public void save(final UserSignUpRequest request, final UserInfo saveUser) {
        logUserJoinRepository.save(
            new LogUserJoin(saveUser.getIdx(), saveUser.getUserUuid(), request.getLoginType(), request.getSnsKey())
        );
    }

    public String findBySnsKey(final String snsKey) {

        if (StringUtils.isEmpty(snsKey)) {
            return null;
        }

        return logUserJoinRepository.findBySnsKey(snsKey)
            .map(LogUserJoin::getUserUuid)
            .orElseThrow(
                () -> new NotFoundException("해당 snskey [" + snsKey + "] 에 해당하는 유저를 찾을 수 없습니다.")
            );
    }
}
