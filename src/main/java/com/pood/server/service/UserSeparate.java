package com.pood.server.service;

import static com.pood.server.exception.ErrorMessage.USER;

import com.pood.server.controller.request.user.UserInfoUpdateRequest;
import com.pood.server.controller.request.user.UserSignUpRequest;
import com.pood.server.dto.meta.goods.UserReviewImageDto.ReviewPhotoList;
import com.pood.server.entity.user.UserBaseImage;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserWish;
import com.pood.server.exception.CustomStatusException;
import com.pood.server.exception.InvalidTokenException;
import com.pood.server.exception.NotFoundException;
import com.pood.server.facade.user.info.UserPasswordChangeRequest;
import com.pood.server.repository.user.UserInfoRepository;
import com.pood.server.util.RecommendCodeGenerator;
import com.pood.server.util.UserStatus;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional("userTransactionManager")
public class UserSeparate {

    private final UserInfoRepository userInfoRepository;

    public UserInfo getUserByUUID(final String userUUID) {
        return userInfoRepository.findByUserUuid(userUUID)
            .orElseThrow(() -> CustomStatusException.badRequest(USER));
    }

    public UserInfo getUserByToken(final String token) {
        return userInfoRepository.getUserByToken(token);
    }

    public UserInfo getOptionalUserByToken(final String token) {
        return Optional.ofNullable(userInfoRepository.getUserByToken(token))
            .orElseThrow(() -> new InvalidTokenException("토큰이 유효하지 않습니다."));
    }

    public List<UserWish> getUserWishList(final String userUuid) {
        return userInfoRepository.getUserWishList(userUuid);
    }

    public UserInfo findByIdx(final int userIdx) {
        return userInfoRepository.findById(userIdx)
            .orElseThrow(() -> new NotFoundException("유저를 찾을 수 없습니다."));
    }

    public void logout(final UserInfo userInfo) {
        final UserInfo toBeRemovedUser = findByIdx(userInfo.getIdx());
        toBeRemovedUser.softDelete();
    }

    public UserInfo findByUserEmail(final String email) {
        return userInfoRepository.findByUserEmail(email)
            .orElseThrow(() -> new NotFoundException("해당하는 계정 정보가 없습니다."));
    }

    public UserInfo loginValidation(final String email, final String password) {
        return findByUserEmail(email).isMatchedPassword(password);
    }

    public UserInfo findByExactlyUser(final String userEmail, final String userPhone,
        final String userName) {
        return userInfoRepository.findByUserEmailAndUserPhoneAndUserName(userEmail, userPhone,
                userName)
            .orElseThrow(() -> new NotFoundException("해당하는 계정 정보가 없습니다."));
    }

    public Page<ReviewPhotoList> getReviewPhotoImgList(Integer idx, List<Integer> notInReviewIdx,
        Pageable pageable) {
        return userInfoRepository.getReviewPhotoImgList(idx,
            notInReviewIdx.isEmpty() ? null : notInReviewIdx, pageable);
    }

    public void changePassword(final UserPasswordChangeRequest request, final UserInfo updateUser) {
        if (updateUser.currentPasswordValidate(request.getCurrentPassword())) {
            updateUser.changePassword(request.getNewPassword());
            save(updateUser);
        }
    }

    public void updateUserInfo(final UserInfoUpdateRequest request) {
        final UserInfo user = getUserByUUID(request.getUserUuid());
        user.updateInfo(request);
    }

    public boolean existByEmail(final String email) {
        return userInfoRepository.existsByUserEmail(email);
    }

    public UserInfo findByRecommendUser(final String referralCode) {
        return userInfoRepository.findByReferralCode(referralCode)
            .orElseThrow(() -> new NotFoundException("해당 추천 코드의 유저를 찾을 수 없습니다."));
    }

    public boolean existsByUserPhone(final String userPhone) {
        return userInfoRepository.existsByUserPhone(userPhone);
    }

    public UserInfo save(final UserSignUpRequest request, final UserBaseImage userBaseImage) {
        while (true) {
            final String recommendCode = RecommendCodeGenerator.generate();
            boolean isExist = userInfoRepository.existsByReferralCode(recommendCode);
            if (!isExist) {
                return userInfoRepository.save(request.toEntity(recommendCode, userBaseImage));
            }
        }
    }

    public void save(final UserInfo userInfo) {
        userInfoRepository.save(userInfo);
    }

    public void withdrawalUser(final UserInfo userInfo) {
        userInfo.toResignUser();
        userInfoRepository.save(userInfo);
    }

    private List<UserInfo> getUserListByResignUser() {
        return userInfoRepository.findAllByUserStatusIn(
            List.of(UserStatus.TO_RESIGN, UserStatus.RESIGNED));
    }

    public List<Integer> getUserIdxListByResignUser() {
        return getUserListByResignUser()
            .stream()
            .map(UserInfo::getIdx)
            .collect(Collectors.toList());
    }

    public UserInfo findByUserNameAndPhoneNumber(final String userPhone, final String userName) {
        return userInfoRepository.findByUserPhoneAndUserName(userPhone, userName)
            .orElseThrow(() -> new NotFoundException("유저를 찾을 수 없습니다."));
    }

    public void addPoint(final String userUuid, final int retrievePoint) {
        final UserInfo userInfo = getUserByUUID(userUuid);
        userInfo.addUserPoint(retrievePoint);
    }

    public UserInfo changeActive(final String email, final String userUuid) {

        final UserInfo user =
            StringUtils.isEmpty(userUuid) ? findByUserEmail(email) : getUserByUUID(userUuid);

        user.returnActive();

        return user;
    }
}
