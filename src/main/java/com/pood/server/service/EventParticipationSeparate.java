package com.pood.server.service;

import com.pood.server.dto.meta.event.EventDto.EventDetail;
import com.pood.server.repository.meta.EventParticipationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EventParticipationSeparate implements EventTypeJoinPossible {

    private static final String PARTICIPATION_TYPE = "E";

    private final EventParticipationRepository eventParticipationRepository;

    @Override
    public boolean isParticipationPossible(final EventDetail eventDetail,
        final int userIdx) {
        return eventParticipationRepository.countByUserInfoIdxAndEventInfoIdx(userIdx,
            eventDetail.getIdx()) == 0;
    }

    @Override
    public String getType() {
        return PARTICIPATION_TYPE;
    }
}
