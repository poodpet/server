package com.pood.server.service;

import com.pood.server.entity.user.UserPet;
import com.pood.server.entity.user.UserPetWeightDiary;
import com.pood.server.exception.AlreadyExistException;
import com.pood.server.exception.DateMatchingException;
import com.pood.server.exception.NotFoundException;
import com.pood.server.facade.user.pet.request.WeightUpdateRequest;
import com.pood.server.repository.user.UserPetWeightDiaryRepository;
import com.pood.server.util.date.DateUtil;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("userTransactionManager")
public class UserPetWeightDiarySeparate {

    private final UserPetWeightDiaryRepository userPetWeightDiaryRepository;

    public void save(final UserPet userPet, final float petWeight) {
        userPetWeightDiaryRepository.save(
            UserPetWeightDiary.builder()
                .userPet(userPet)
                .weight(petWeight)
                .baseDate(
                    LocalDate.now().plusDays(
                        DateUtil.of(LocalDate.now()).getCompareDayOfWeekNumber(DayOfWeek.FRIDAY)))
                .build()
        );
    }

    public UserPetWeightDiary save(final UserPet userPet, final float petWeight,
        final LocalDate baseDate) {
        return userPetWeightDiaryRepository.save(
            UserPetWeightDiary.builder()
                .userPet(userPet)
                .weight(petWeight)
                .baseDate(baseDate)
                .build()
        );
    }

    public void recordBodyManagement(final UserPet userPet, final Float weight) {

        final Optional<UserPetWeightDiary> userPetDiary = userPetWeightDiaryRepository.getUserPetWeight(
            userPet.getIdx(), DateUtil.of(LocalDate.now()).convertWeeklyFridayDate());

        if (userPetDiary.isPresent()) {
            userPetDiary.get().updateWeight(weight);
            return;
        }

        save(userPet, weight);
    }

    public void updateWeightRecord(final WeightUpdateRequest request) {
        final UserPetWeightDiary userPetWeightDiary = userPetWeightDiaryRepository.findById(
                request.getIdx())
            .orElseThrow(() -> new NotFoundException("해당하는 무게 기록을 찾을 수 없습니다."));
        userPetWeightDiary.updateWeight(request.getWeight());
    }

    public List<UserPetWeightDiary> findAllPetWeightRecord(final UserPet userPet) {
        return userPetWeightDiaryRepository.findAllByUserPetOrderByIdxDesc(userPet);
    }

    public void deleteByUserPetIdx(final int userPetIdx) {
        userPetWeightDiaryRepository.deleteAllByUserPetIdx(userPetIdx);
    }

    public UserPetWeightDiary updateWeightRecord(final long idx, final float weight) {
        final UserPetWeightDiary userPetWeightDiary = userPetWeightDiaryRepository.findById(idx)
            .orElseThrow(() -> new NotFoundException("해당하는 무게 기록을 찾을 수 없습니다."));
        userPetWeightDiary.updateWeight(weight);
        return userPetWeightDiary;
    }

    public UserPetWeightDiary createWeightRecord(final UserPet userPet, final LocalDate date,
        final Float weight) {
        if (!date.getDayOfWeek().equals(DayOfWeek.FRIDAY)) {
            throw new DateMatchingException("해당 요일로 지정 할수 없습니다.");
        }
        if (userPetWeightDiaryRepository.existsByBaseDateAndUserPet(date, userPet)) {
            throw new AlreadyExistException("해당 일자의 값이 이미 존재 합니다.");
        }
        return save(userPet, weight, date);
    }

    public UserPetWeightDiary findRecentInfo(final Integer userPetIdx) {
        return userPetWeightDiaryRepository.findFirstByUserPetIdxAndWeightIsNotNullOrderByBaseDateDesc(
            userPetIdx).orElse(UserPetWeightDiary.empty());
    }

}
