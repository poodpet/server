package com.pood.server.service;

import com.pood.server.entity.meta.Coupon;
import com.pood.server.entity.user.UserCoupon;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.user.UserCouponRepository;
import com.pood.server.util.UserCouponStatus;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("userTransactionManager")
public class UserCouponSeparate {

    private final UserCouponRepository userCouponRepository;

    public UserCoupon findByUuid(final String userUuid) {
        return userCouponRepository.findByUserUuid(userUuid)
            .orElseThrow(() -> new NotFoundException("유저 쿠폰을 찾을 수 없습니다."));
    }

    public List<UserCoupon> findAllByAvailableCouponIdx(final String userUuid) {
        return userCouponRepository.findAllByUserUuidAndStatusAndAvailableTimeGreaterThanEqual(
            userUuid, UserCouponStatus.AVAILABLE, LocalDateTime.now());
    }

    public void save(final UserCoupon userCoupon) {
        userCouponRepository.save(userCoupon);
    }

    public boolean isNotExistHistory(final int userIdx, final List<Integer> couponIdxList) {
        return !userCouponRepository.existsByUserIdxAndCouponIdxIn(userIdx, couponIdxList);
    }

    public void publishWelcomeCoupon(final List<Coupon> welcomeCouponList, final UserInfo saveUser) {
        List<UserCoupon> saveCouponList = new ArrayList<>();
        for (Coupon coupon : welcomeCouponList) {
            saveCouponList.add(
                UserCoupon.builder()
                    .couponIdx(coupon.getIdx())
                    .userIdx(saveUser.getIdx())
                    .userUuid(saveUser.getUserUuid())
                    .applyCartIdx(null)
                    .status(UserCouponStatus.AVAILABLE)
                    .overType(coupon.getOverType())
                    .publishTime(LocalDateTime.now())
                    .availableTime(LocalDateTime.now().plusDays(coupon.getAvailableDay()))
                    .build()
            );
        }

        userCouponRepository.saveAll(saveCouponList);
    }

}