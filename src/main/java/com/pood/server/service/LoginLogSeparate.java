package com.pood.server.service;

import com.pood.server.entity.log.LogUserLogin;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.log.LogUserLoginRepository;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("logTransactionManager")
public class LoginLogSeparate {

    private static final int LOGIN = 0;

    private final LogUserLoginRepository logUserLoginRepository;

    public void saveLog(final UserInfo userInfo, final int osType) {
        logUserLoginRepository.save(
            LogUserLogin.builder()
                .userIdx(userInfo.getIdx())
                .loginOut(LOGIN)
                .osType(osType)
                .build()
        );
    }

    public void saveLogOut(final UserInfo userInfo) {
        final LogUserLogin logUserLogin = logUserLoginRepository.findTopByUserUuidAndLoginOutOrderByIdxDesc(
                userInfo.getUserUuid(), LOGIN)
            .orElseThrow(() -> new NotFoundException("로그인 기록이 없습니다."));

        logUserLogin.updateLogoutStatus();
    }

    public void insert(final Integer userIdx, final String userUuid, final Integer osType) {
        logUserLoginRepository.save(LogUserLogin.create(userIdx, userUuid, osType));
    }

    public void doDormant(final String userUuid) {
        Optional<LogUserLogin> log = logUserLoginRepository.findTopByUserUuidOrderByIdxDesc(
            userUuid);

        log.ifPresent(LogUserLogin::updateDormant);
    }
}
