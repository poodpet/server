package com.pood.server.service;

import com.pood.server.config.AES256;
import com.pood.server.exception.CustomStatusException;
import com.pood.server.exception.ErrorMessage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import reactor.core.publisher.Mono;

@Service
public class HoliyDaySeparate {

    @Value("${holiday.url}")
    private String defaultUrl;

    @Value("${holiday.key}")
    private String apiKey;

    public List<String> getHolidayList() {

        List<String> holidayData = new ArrayList<>();
        try {
            holidayData = getHolidayData();
        } catch (URISyntaxException | ParserConfigurationException | SAXException | IOException e) {
            throw CustomStatusException.serverError(ErrorMessage.SERVER);
        }

        List<LocalDate> weekendsBetween = findWeekendsBetween();
        List<String> holidayAndWeekendDate = weekendsBetween.stream().map(String::valueOf).collect(
            Collectors.toList());

        holidayAndWeekendDate.addAll(holidayData);
        holidayAndWeekendDate = holidayAndWeekendDate.stream()
            .sorted()
            .distinct()
            .collect(Collectors.toList());

        if (holidayAndWeekendDate.isEmpty()) {
            throw CustomStatusException.serverError(ErrorMessage.HOLIDAY);
        }

        return holidayAndWeekendDate;
    }

    private List<String> getHolidayData()
        throws ParserConfigurationException, IOException, SAXException, URISyntaxException {

        int year = LocalDate.now().getYear();
        Mono<String> webClient = setWebClient(year);
        List<String> holidayMap = new ArrayList<>(parseXML(
            Objects.requireNonNull(webClient.block(Duration.ofSeconds(10000L)))));
        if (LocalDate.now().getMonthValue() == 12) {
            year = year + 1;
            Mono<String> nextYearWebClient = setWebClient(year);
            holidayMap.addAll(parseXML(
                Objects.requireNonNull(nextYearWebClient.block(Duration.ofSeconds(10000L)))));
        }

        return holidayMap;
    }

    private LocalDate getHolidayCheckEndDate() {
        LocalDate now = LocalDate.now();
        LocalDate endDate = LocalDate.of(Year.now().getValue(), 12, 31);

        if (now.getMonth().getValue() == Month.DECEMBER.getValue()) {
            endDate = endDate.plusYears(1L);
        }

        endDate = endDate.with(TemporalAdjusters.lastDayOfMonth());
        return endDate;
    }

    private List<LocalDate> findWeekendsBetween() {

        LocalDate startDate = LocalDate.of(Year.now().getValue(), 1, 1);
        LocalDate endDate = getHolidayCheckEndDate();

        List<LocalDate> weekends = new ArrayList<>();
        while (startDate.isBefore(endDate) || startDate.equals(endDate)) {
            if (startDate.getDayOfWeek() == DayOfWeek.SATURDAY) {
                weekends.add(startDate);
                weekends.add(startDate.plusDays(1));
                startDate = startDate.plusDays(2);
            } else {
                startDate = startDate.plusDays(1);
            }
        }
        return weekends;
    }

    private Mono<String> setWebClient(final int year) throws URISyntaxException {
        URI uri = new URI(String.format(defaultUrl, year, AES256.getAesDecodeOrNull(apiKey)));
        return WebClient.create().get().uri(uri)
            .retrieve()
            .bodyToMono(String.class);
    }

    private List<String> parseXML(final String xml)
        throws ParserConfigurationException, IOException, SAXException {
        List<String> holidayMap = new ArrayList<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        factory.setFeature("http://xml.org/sax/features/external-general-entities", false);
        factory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
        DocumentBuilder documentBuilder = factory.newDocumentBuilder();
        ByteArrayInputStream ip = new ByteArrayInputStream(xml.getBytes());

        Document doc = documentBuilder.parse(ip);
        Element element = doc.getDocumentElement();
        NodeList itemList = element.getElementsByTagName("item");

        if (itemList != null && itemList.getLength() > 0) {
            for (int i = 0; i < itemList.getLength(); i++) {
                Node n = itemList.item(i);
                if (n.getNodeType() == Node.ELEMENT_NODE) {
                    Element el = (Element) n;
                    String locdate = el.getElementsByTagName("locdate").item(0).getTextContent();
                    String date = LocalDate.parse(locdate, DateTimeFormatter.ofPattern("yyyyMMdd"))
                        .toString();
                    holidayMap.add(date);
                }
            }
        }
        return holidayMap;
    }

}
