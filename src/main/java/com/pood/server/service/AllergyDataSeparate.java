package com.pood.server.service;

import com.pood.server.entity.meta.AllergyData;
import com.pood.server.repository.meta.AllergyDataRepository;
import java.util.Collection;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("metaTransactionManager")
public class AllergyDataSeparate {

    private static final boolean EXCEPT_NOT_VISIBLE = false;
    private final AllergyDataRepository allergyDataRepository;

    public List<AllergyData> findAllByIdxIn(final Collection<Integer> allergyList) {
        return allergyDataRepository.findAllByIdxIn(allergyList);
    }

    @Transactional(transactionManager = "metaTransactionManager", readOnly = true)
    public List<AllergyData> findAllOrderByPriority() {
        return allergyDataRepository.findAllByVisibleNotOrderByPriorityAsc(EXCEPT_NOT_VISIBLE);
    }
}
