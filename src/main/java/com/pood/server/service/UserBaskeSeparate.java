package com.pood.server.service;

import com.pood.server.repository.user.UserBasketRepository;
import java.util.Arrays;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserBaskeSeparate {

    private final UserBasketRepository userBasketRepository;

    public Long getBasketGoodsCount(final String userUuid, final List<Integer> goodsIdxList) {
        return userBasketRepository.countByUserUuidAndStatusInAndGoodsIdxNotIn(userUuid,
            Arrays.asList(1, 2), goodsIdxList);
    }
}
