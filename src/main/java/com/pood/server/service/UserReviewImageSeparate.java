package com.pood.server.service;

import com.pood.server.entity.user.UserReviewImage;
import com.pood.server.repository.user.UserReviewImageRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserReviewImageSeparate {

    private final UserReviewImageRepository userReviewImageRepository;

    public List<UserReviewImage> getReviewAllByIdxIn(final List<Integer> userReviewIdx) {
        return userReviewImageRepository.findAllByUserReviewIdxIn(userReviewIdx);
    }

    public void saveAll(final List<UserReviewImage> saveList) {
        userReviewImageRepository.saveAll(saveList);
    }
}
