package com.pood.server.service.faq;

import com.pood.server.entity.meta.FaqCategory;
import com.pood.server.repository.meta.FaqCategoryRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FaqCategorySeparate {

    private final FaqCategoryRepository faqCategoryRepository;

    public List<FaqCategory> getFaqCategoryList() {
        return faqCategoryRepository.findAll(Sort.by(Sort.Direction.ASC, "priority"));
    }

}
