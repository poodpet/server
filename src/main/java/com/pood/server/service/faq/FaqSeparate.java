package com.pood.server.service.faq;

import com.pood.server.entity.meta.Faq;
import com.pood.server.repository.meta.FaqRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FaqSeparate {

    private final FaqRepository faqRepository;

    public List<Faq> getFaqListByCategoryIdx(final long categoryIdx) {
        return faqRepository.findAllByFaqCategoryIdxOrderByPriorityAsc(categoryIdx);
    }

}
