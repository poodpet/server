package com.pood.server.service;


import static com.pood.server.exception.ErrorMessage.API_TOKEN;
import static com.pood.server.exception.ErrorMessage.IDX;
import static com.pood.server.exception.ErrorMessage.SERVER;
import static com.pood.server.exception.ErrorMessage.USER;

import com.pood.server.dto.log.user_order.OrderDto;
import com.pood.server.dto.meta.goods.GoodsDto;
import com.pood.server.dto.meta.user.review.UserReviewGroup;
import com.pood.server.entity.meta.PaymentType;
import com.pood.server.entity.meta.Seller;
import com.pood.server.entity.meta.dto.OrderInfoDto;
import com.pood.server.entity.meta.mapper.OrderGoodsMapper;
import com.pood.server.entity.meta.mapper.SellerMapper;
import com.pood.server.entity.order.Order;
import com.pood.server.entity.order.OrderBasket;
import com.pood.server.entity.order.OrderCancel;
import com.pood.server.entity.order.OrderDelivery;
import com.pood.server.entity.order.OrderDeliveryTrackInfo;
import com.pood.server.entity.order.OrderExchange;
import com.pood.server.entity.order.OrderRefund;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserReview;
import com.pood.server.exception.CustomStatusException;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.meta.PaymentTypeRepository;
import com.pood.server.repository.order.OrderBasketRepository;
import com.pood.server.repository.order.OrderCancelRepository;
import com.pood.server.repository.order.OrderDeliveryRepository;
import com.pood.server.repository.order.OrderDeliveryTrackInfoOrderRepository;
import com.pood.server.repository.order.OrderExchangeRepository;
import com.pood.server.repository.order.OrderRefundRepository;
import com.pood.server.repository.order.OrderRepository;
import com.pood.server.repository.user.UserInfoRepository;
import com.pood.server.repository.user.UserReviewRepository;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceV2 {

    @Autowired
    UserInfoRepository userInfoRepository;

    @Autowired
    OrderDeliveryRepository orderDeliveryRepository;

    @Autowired
    OrderBasketRepository orderBasketRepository;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    PaymentTypeRepository paymentTypeRepository;

    @Autowired
    OrderDeliveryTrackInfoOrderRepository orderDeliveryTrackInfoOrderRepository;

    @Autowired
    private GoodsSeparate goodsSeparate;

    @Autowired
    OrderCancelRepository orderCancelRepository;

    @Autowired
    OrderRefundRepository orderRefundRepository;

    @Autowired
    OrderExchangeRepository orderExchangeRepository;

    @Autowired
    UserReviewRepository userReviewRepository;


    public Page<OrderDto.UserOrderDto> getMyOrderList(String token, Pageable pageable) {
        Optional.ofNullable(token).orElseThrow(() -> CustomStatusException.unAuthorized(API_TOKEN));
        UserInfo userInfo = userInfoRepository.getUserByTokenOptional(token)
            .orElseThrow(() -> CustomStatusException.unAuthorized(USER));

        List<OrderDto.UserOrderDto> userOrderList = new ArrayList<>();
        Page<Order> orderList = orderRepository.findAllByUserIdxAndOrderStatusNot(userInfo.getIdx(),
            0, pageable);
        List<Integer> orderIdxList = orderList.stream().map(Order::getIdx)
            .collect(Collectors.toList());
        List<OrderBasket> orderBasketList = orderBasketRepository.findAllByOrderIdxIn(orderIdxList);
        List<PaymentType> paymentTypeList = paymentTypeRepository.findAll();
        List<OrderDelivery> orderDeliveryList = orderDeliveryRepository.findAllByOrderIdxInAndDeliveryType(
            orderIdxList, 10);

        List<OrderDeliveryTrackInfo> orderDeliveryTrackInfos = orderDeliveryTrackInfoOrderRepository.findByOrderNumberIn(
            orderBasketList.stream().map(OrderBasket::getOrderNumber).collect(Collectors.toList()));

        List<OrderGoodsMapper> myOrderGoodsList = goodsSeparate.myOrderGoodsInfoListByBasket(
            orderBasketList);
        List<UserReview> userReviewList = userReviewRepository.findAllByOrderNumberIn(
            orderBasketList.stream().map(OrderBasket::getOrderNumber).collect(Collectors.toList()));

        for (Order order : orderList) {
            PaymentType paymentType = paymentTypeList.stream()
                .filter(x -> x.getIdx().equals(order.getOrderType())).findFirst()
                .orElseThrow(() -> CustomStatusException.unAuthorized(API_TOKEN));
            OrderDto.OrderAddress orderAddress = new OrderDto.OrderAddress();
            OrderDto.Delivery delivery = new OrderDto.Delivery();
            List<OrderDto.UserOrderInfo> userOrderInfoList = new ArrayList<>();
            OrderDto.UserOrderDto userOrderDto = new OrderDto.UserOrderDto(
                order.getIdx(),
                order.getOrderName(),
                order.getOrderNumber(),
                order.getRecordbirth(),
                order.getTotalPrice(), // orderPrice
                order.getOrderPrice(), //  payPrice
                order.getDeliveryFee(),
                order.getSavedPoint(),
                order.getUsedPoint(),
                order.getDiscountCouponPrice(),
                order.getMemo(),
                paymentType.getName(),
                orderAddress,
                delivery,
                userOrderInfoList,
                order.getOrderStatus()
            );

            for (OrderBasket orderBasket : orderBasketList) {
                if (order.getIdx().equals(orderBasket.getOrderIdx())) {
                    for (OrderGoodsMapper sortedGoodsList : myOrderGoodsList) {
                        SellerMapper sellerMapper = sortedGoodsList.getSeller();
                        Seller seller = new Seller(sellerMapper.getIdx(), sellerMapper.getName());
                        if (sortedGoodsList.getIdx().equals(orderBasket.getGoodsIdx())) {
                            OrderDto.UserOrderInfo userOrderInfo = new OrderDto.UserOrderInfo();
                            userOrderInfo.setIdx(orderBasket.getIdx());
                            userOrderInfo.setGoodsIdx(orderBasket.getGoodsIdx());
                            userOrderInfo.setGoodsName(sortedGoodsList.getGoodsName());
                            userOrderInfo.setDisplayType(sortedGoodsList.getDisplayType());
                            userOrderInfo.setGoodsPrice(orderBasket.getGoodsPrice());
                            userOrderInfo.setImage(sortedGoodsList.getMainImage());
                            userOrderInfo.setQuantity(orderBasket.getQuantity());
                            userOrderInfo.setDirectDelivery(seller.isDirectDelivery());
                            userOrderInfoList.add(userOrderInfo);

                            userOrderInfo.setPossibleWriteReview(
                                new UserReviewGroup(userReviewList).isPossibleWriteReview(
                                    orderBasket));

                            List<OrderCancel> cancelList = orderCancelRepository.findAllByOrderNumberAndGoodsIdxOrderByRecordbirthDesc(
                                orderBasket.getOrderNumber(), orderBasket.getGoodsIdx());
                            List<OrderRefund> refundList = orderRefundRepository.findAllByOrderNumberAndGoodsIdxOrderByRecordbirthDesc(
                                orderBasket.getOrderNumber(), orderBasket.getGoodsIdx());
                            List<OrderExchange> exchangeList = orderExchangeRepository.findAllByOrderNumberAndGoodsIdxOrderByRecordbirthDesc(
                                orderBasket.getOrderNumber(), orderBasket.getGoodsIdx());
                            String recentType = orderRecordBirthDataList(cancelList, refundList,
                                exchangeList);
                            if (recentType != null) {
                                if (recentType.equals("C")) {
                                    OrderCancel orderCancel = cancelList.stream().findFirst()
                                        .orElseThrow(
                                            () -> CustomStatusException.serverError(SERVER));
                                    userOrderInfo.setOrderCancel(orderCancel);
                                } else if (recentType.equals("R")) {
                                    OrderRefund orderRefund = refundList.stream().findFirst()
                                        .orElseThrow(
                                            () -> CustomStatusException.serverError(SERVER));
                                    userOrderInfo.setOrderRefund(orderRefund);
                                }
                                if (recentType.equals("E")) {
                                    OrderExchange orderExchange = exchangeList.stream().findFirst()
                                        .orElseThrow(
                                            () -> CustomStatusException.serverError(SERVER));
                                    List<OrderDeliveryTrackInfo> orderDeliveryTrackInfoList = orderDeliveryTrackInfos.stream()
                                        .filter(
                                            x -> x.getOrderNumber().equals(order.getOrderNumber()))
                                        .collect(Collectors.toList());
                                    if (orderDeliveryTrackInfoList.size() > 1) {
                                        OrderDeliveryTrackInfo exchangeTracking = orderDeliveryTrackInfoList.stream()
                                            .sorted(Comparator.comparing(
                                                OrderDeliveryTrackInfo::getRecordbirth).reversed())
                                            .findFirst().orElseThrow(
                                                () -> CustomStatusException.serverError(SERVER));
                                        OrderDto.Delivery exchangeDelivery = new OrderDto.Delivery();
                                        exchangeDelivery.setTrackingId(
                                            exchangeTracking.getTrackingId());
                                        exchangeDelivery.setCourier(exchangeTracking.getCourier());
                                        orderExchange.setDelivery(exchangeDelivery);
                                    }

                                    userOrderInfo.setOrderExchange(orderExchange);
                                }
                            }
                        }
                    }

                }
            }
            for (OrderDelivery orderDelivery : orderDeliveryList) {
                if (order.getIdx().equals(orderDelivery.getOrderIdx())) {
                    orderAddress.setAddress(orderDelivery.getAddress());
                    orderAddress.setDetailAddress(orderDelivery.getAddressDetail());
                    orderAddress.setZipcode(orderDelivery.getZipcode());
                    orderAddress.setPhoneNumber(orderDelivery.getPhoneNumber());
                    orderAddress.setNickName(orderDelivery.getNickname());
                    orderAddress.setName(orderDelivery.getReceiver());
                    break;
                }
            }

            for (OrderDeliveryTrackInfo orderDeliveryTrackInfo : orderDeliveryTrackInfos) {
                if (order.getOrderNumber().equals(orderDeliveryTrackInfo.getOrderNumber())) {
                    delivery.setCourier(orderDeliveryTrackInfo.getCourier());
                    delivery.setTrackingId(orderDeliveryTrackInfo.getTrackingId());
                    break;
                }
            }
            userOrderList.add(userOrderDto);
        }
        return new PageImpl<>(userOrderList, orderList.getPageable(), orderList.getTotalElements());
    }


    public OrderDto.UserOrderDto getMyOrderDetail(String token, Integer orderIdx) {
        Optional.ofNullable(token).orElseThrow(() -> CustomStatusException.unAuthorized(API_TOKEN));
        UserInfo userInfo = userInfoRepository.getUserByTokenOptional(token)
            .orElseThrow(() -> CustomStatusException.unAuthorized(USER));

        List<OrderDto.UserOrderDto> userOrderList = new ArrayList<>();
        Order orderInfo = orderRepository.findById(orderIdx)
            .orElseThrow(() -> CustomStatusException.badRequest(IDX));
        Integer orderIdxList = orderInfo.getIdx();
        List<OrderBasket> orderBasketList = orderBasketRepository.findAllByOrderIdxIn(
            Arrays.asList(orderIdxList));
        List<OrderDelivery> orderDeliveryList = orderDeliveryRepository.findAllByOrderIdxInAndDeliveryType(
            Arrays.asList(orderIdxList), 10);
        List<PaymentType> paymentTypeList = paymentTypeRepository.findAll();
        List<OrderDeliveryTrackInfo> orderDeliveryTrackInfos = orderDeliveryTrackInfoOrderRepository.findByOrderNumberIn(
            Arrays.asList(orderInfo.getOrderNumber()));
        List<GoodsDto.SortedGoodsList> myOrderGoodsList = goodsSeparate.myOrderGoodsListByBasket(
            orderBasketList);

        PaymentType paymentType = paymentTypeList.stream()
            .filter(x -> x.getIdx().equals(orderInfo.getOrderType())).findFirst()
            .orElseThrow(() -> CustomStatusException.unAuthorized(API_TOKEN));
        OrderDto.OrderAddress orderAddress = new OrderDto.OrderAddress();
        OrderDto.Delivery delivery = new OrderDto.Delivery();

        List<OrderDto.UserOrderInfo> userOrderInfoList = new ArrayList<>();
        OrderDto.UserOrderDto userOrderDto = new OrderDto.UserOrderDto(
            orderInfo.getIdx(),
            orderInfo.getOrderName(),
            orderInfo.getOrderNumber(),
            orderInfo.getRecordbirth(),
            orderInfo.getTotalPrice(), // orderPrice
            orderInfo.getOrderPrice(), //  payPrice
            orderInfo.getDeliveryFee(),
            orderInfo.getSavedPoint(),
            orderInfo.getUsedPoint(),
            orderInfo.getDiscountCouponPrice(),
            orderInfo.getMemo(),
            paymentType.getName(),
            orderAddress,
            delivery, userOrderInfoList,
            orderInfo.getOrderStatus()
        );

        for (OrderBasket orderBasket : orderBasketList) {
            if (orderInfo.getIdx().equals(orderBasket.getOrderIdx())) {
                for (GoodsDto.SortedGoodsList sortedGoodsList : myOrderGoodsList) {
                    if (sortedGoodsList.getIdx().equals(orderBasket.getGoodsIdx())) {
                        OrderDto.UserOrderInfo userOrderInfo = new OrderDto.UserOrderInfo();
                        userOrderInfo.setIdx(orderBasket.getIdx());
                        userOrderInfo.setGoodsIdx(orderBasket.getGoodsIdx());
                        userOrderInfo.setGoodsName(sortedGoodsList.getGoodsName());
                        userOrderInfo.setDisplayType(sortedGoodsList.getDisplayType());
                        userOrderInfo.setGoodsPrice(orderBasket.getGoodsPrice());
                        userOrderInfo.setImage(sortedGoodsList.getMainImage());
                        userOrderInfo.setQuantity(orderBasket.getQuantity());
                        userOrderInfoList.add(userOrderInfo);

                        List<OrderCancel> cancelList = orderCancelRepository.findAllByOrderNumberAndGoodsIdxOrderByRecordbirthDesc(
                            orderBasket.getOrderNumber(), orderBasket.getGoodsIdx());
                        List<OrderRefund> refundList = orderRefundRepository.findAllByOrderNumberAndGoodsIdxOrderByRecordbirthDesc(
                            orderBasket.getOrderNumber(), orderBasket.getGoodsIdx());
                        List<OrderExchange> exchangeList = orderExchangeRepository.findAllByOrderNumberAndGoodsIdxOrderByRecordbirthDesc(
                            orderBasket.getOrderNumber(), orderBasket.getGoodsIdx());
                        String recentType = orderRecordBirthDataList(cancelList, refundList,
                            exchangeList);
                        if (recentType != null) {
                            if (recentType.equals("C")) {
                                OrderCancel orderCancel = cancelList.stream().findFirst()
                                    .orElseThrow(() -> CustomStatusException.serverError(SERVER));
                                userOrderInfo.setOrderCancel(orderCancel);
                            } else if (recentType.equals("R")) {
                                OrderRefund orderRefund = refundList.stream().findFirst()
                                    .orElseThrow(() -> CustomStatusException.serverError(SERVER));
                                userOrderInfo.setOrderRefund(orderRefund);
                            }
                            if (recentType.equals("E")) {
                                OrderExchange orderExchange = exchangeList.stream().findFirst()
                                    .orElseThrow(() -> CustomStatusException.serverError(SERVER));

                                List<OrderDeliveryTrackInfo> orderDeliveryTrackInfoList = orderDeliveryTrackInfos.stream()
                                    .filter(
                                        x -> x.getOrderNumber().equals(orderInfo.getOrderNumber()))
                                    .collect(Collectors.toList());
                                if (orderDeliveryTrackInfoList.size() > 1) {
                                    OrderDeliveryTrackInfo exchangeTracking = orderDeliveryTrackInfoList.stream()
                                        .sorted(Comparator.comparing(
                                            OrderDeliveryTrackInfo::getRecordbirth).reversed())
                                        .findFirst().orElseThrow(
                                            () -> CustomStatusException.serverError(SERVER));
                                    OrderDto.Delivery exchangeDelivery = new OrderDto.Delivery();
                                    exchangeDelivery.setTrackingId(
                                        exchangeTracking.getTrackingId());
                                    exchangeDelivery.setCourier(exchangeTracking.getCourier());
                                    orderExchange.setDelivery(exchangeDelivery);
                                }

                                userOrderInfo.setOrderExchange(orderExchange);
                            }
                        }
                    }
                }
            }
        }

        for (OrderDelivery orderDelivery : orderDeliveryList) {
            if (orderInfo.getIdx().equals(orderDelivery.getOrderIdx())) {
                orderAddress.setAddress(orderDelivery.getAddress());
                orderAddress.setDetailAddress(orderDelivery.getAddressDetail());
                orderAddress.setZipcode(orderDelivery.getZipcode());
                orderAddress.setPhoneNumber(orderDelivery.getPhoneNumber());
                orderAddress.setNickName(orderDelivery.getNickname());
                orderAddress.setName(orderDelivery.getReceiver());
                break;
            }
        }

        for (OrderDeliveryTrackInfo orderDeliveryTrackInfo : orderDeliveryTrackInfos) {
            if (orderInfo.getOrderNumber().equals(orderDeliveryTrackInfo.getOrderNumber())) {
                delivery.setCourier(orderDeliveryTrackInfo.getCourier());
                delivery.setTrackingId(orderDeliveryTrackInfo.getTrackingId());
                break;
            }
        }
//        userOrderList.add(userOrderDto);

        return userOrderDto;

    }

    private String orderRecordBirthDataList(List<OrderCancel> cancelList,
        List<OrderRefund> refundList, List<OrderExchange> exchangeList) {
        List<OrderDto.OrderRecordBirthData> results = new ArrayList<>();
        results.addAll(cancelList.stream()
            .map(x -> new OrderDto.OrderRecordBirthData(x.getIdx(), "C", x.getRecordbirth()))
            .collect(Collectors.toList()));
        results.addAll(refundList.stream()
            .map(x -> new OrderDto.OrderRecordBirthData(x.getIdx(), "R", x.getRecordbirth()))
            .collect(Collectors.toList()));
        results.addAll(exchangeList.stream()
            .map(x -> new OrderDto.OrderRecordBirthData(x.getIdx(), "E", x.getRecordbirth()))
            .collect(Collectors.toList()));
        results = results.stream()
            .sorted(Comparator.comparing(OrderDto.OrderRecordBirthData::getRecordBirth).reversed())
            .collect(Collectors.toList());
        return results.stream().map(x -> x.getType()).findFirst().orElse(null);

    }

    public Integer getOrderPriceByOrderNumber(final String orderNumber) {
        return orderRepository.getOrderPriceByOrderNumber(orderNumber);
    }


    private boolean isPossibleWriteReview(final List<UserReview> userReviewList,
        final OrderBasket orderBasket) {
        return userReviewList.stream()
            .filter(userReview -> userReview.eqOrderNumber(orderBasket.getOrderNumber())
                && userReview.eqGoodsIdx(orderBasket.getGoodsIdx()))
            .findFirst()
            .isEmpty();
    }

    public OrderInfoDto getOrderInfo(final String orderNumber) {
        final Order orderInfo = orderRepository.findByOrderNumber(orderNumber)
            .orElseThrow(() -> new NotFoundException("주문 정보를 찾을수 없습니다."));
        final UserInfo userInfo = userInfoRepository.findByUserUuid(orderInfo.getUserUuid())
            .orElseThrow(() -> CustomStatusException.badRequest(USER));

        return new OrderInfoDto(userInfo.getUserName(),
            userInfo.getUserPhone(),
            orderInfo.getRecordbirth().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),
            orderInfo.getOrderNumber(), orderInfo.getOrderName(),
            orderInfo.getOrderPrice2().toString());

    }
}



