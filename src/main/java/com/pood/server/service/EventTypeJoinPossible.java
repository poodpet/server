package com.pood.server.service;

import com.pood.server.dto.meta.event.EventDto;

public interface EventTypeJoinPossible {

    boolean isParticipationPossible(final EventDto.EventDetail eventDetail,
        final int userIdx);

    String getType();
}
