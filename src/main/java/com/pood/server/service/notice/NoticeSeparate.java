package com.pood.server.service.notice;

import com.pood.server.entity.meta.Notice;
import com.pood.server.entity.meta.NoticeType;
import com.pood.server.entity.meta.mapper.notice.NoticeDetailMapper;
import com.pood.server.entity.meta.mapper.notice.NoticePageMapper;
import com.pood.server.repository.meta.NoticeRepository;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NoticeSeparate {

    private final NoticeRepository noticeRepository;

    public Page<NoticePageMapper> getNoticePage(final Pageable pageable) {
        return noticeRepository.getNoticePage(pageable);
    }

    public Optional<NoticeDetailMapper> getNoticeDetail(final int idx) {
        return noticeRepository.getNoticeDetail(idx);
    }

    public Optional<Notice> getNoticePopupInfoByTypeOrNull(final NoticeType noticeType) {
        return noticeRepository.findTopByIsVisibleTrueAndNoticeTypeAndIsDeletedFalseOrderByIdxDesc(noticeType);
    }
}
