package com.pood.server.service.notice;

import com.pood.server.entity.meta.NoticeType;
import com.pood.server.repository.meta.NoticeTypeRepository;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NoticeTypeSeparate {

    private static final String SYSTEM_CODE = "SYSTEM";

    private final NoticeTypeRepository noticeTypeRepository;

    public NoticeType getSystemType() {
        Optional<NoticeType> byCode = noticeTypeRepository.findByCode(SYSTEM_CODE);
        return byCode
            .orElseThrow(() -> new NullPointerException("해당 공지사항 타입을 찾을수 없습니다."));
    }
}
