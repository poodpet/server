package com.pood.server.service;

import com.pood.server.entity.user.PetGrainSizeJoinView;
import com.pood.server.repository.user.PetGrainSizeJoinViewRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("userTransactionManager")
public class PetGrainSizeJoinViewSeparate {

    private final PetGrainSizeJoinViewRepository petGrainSizeJoinViewRepository;

    public List<PetGrainSizeJoinView> findAllByUserPetIdx(final List<Integer> userPetIdxList) {
        return petGrainSizeJoinViewRepository.findAllByUserPetIdxIn(userPetIdxList);
    }
}
