package com.pood.server.service;

import com.pood.server.controller.response.home.rank.PoodRankCategoryDto;
import com.pood.server.repository.meta.PoodRankCategoryRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PoodRankCategorySeparate {

    private final PoodRankCategoryRepository poodRankCategoryRepository;

    public List<PoodRankCategoryDto> getCategoryList(final int pcIdx) {
        return poodRankCategoryRepository.getCategoryList(pcIdx);
    }

}
