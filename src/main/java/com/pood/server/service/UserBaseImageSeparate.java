package com.pood.server.service;

import com.pood.server.entity.user.UserBaseImage;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.user.UserBaseImageRepository;
import java.util.Collections;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional("userTransactionManager")
@RequiredArgsConstructor
public class UserBaseImageSeparate {

    private final UserBaseImageRepository imageRepository;

    public UserBaseImage findDefaultRandomImage() {
        final List<UserBaseImage> defaultImageList = findDefaultImageList();
        Collections.shuffle(defaultImageList);
        return defaultImageList.get(0);
    }

    public List<UserBaseImage> findDefaultImageList() {
        final List<UserBaseImage> defaultImageList = imageRepository.findAllByBasicIsTrue();
        if (defaultImageList.isEmpty()) {
            throw new NotFoundException("사용자 기본 이미지 리스트가 비어 있습니다.");
        }
        return defaultImageList;
    }

    public UserBaseImage findByIdAndBaseIsTrue(final long idx) {
       return imageRepository.findByIdxAndBasicIsTrue(idx)
            .orElseThrow(() -> new NotFoundException("해당 기본 이미지를 찾을 수 없습니다."));
    }

    public void delete(final UserBaseImage userBaseImage) {
        imageRepository.delete(userBaseImage);
    }

    public UserBaseImage create(final String url) {
        return imageRepository.save(UserBaseImage.create(url));
    }

}
