package com.pood.server.service.doctor.magazine;

import com.pood.server.entity.meta.PetDoctorMagazineCategory;
import com.pood.server.repository.meta.PetDoctorMagazineCategoryRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PetDoctorMagazineCategorySeparate {

    private final PetDoctorMagazineCategoryRepository petDoctorMagazineCategoryRepository;

    public List<PetDoctorMagazineCategory> getAllList() {
        return petDoctorMagazineCategoryRepository.findAllByOrderByPriorityAsc();
    }

}
