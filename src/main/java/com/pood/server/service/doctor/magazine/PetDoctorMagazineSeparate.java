package com.pood.server.service.doctor.magazine;

import com.pood.server.entity.meta.mapper.doctor.PetDoctorMagazineDetailMapper;
import com.pood.server.entity.meta.mapper.doctor.PetDoctorMagazineMapper;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.meta.PetDoctorMagazineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PetDoctorMagazineSeparate {

    private final PetDoctorMagazineRepository petDoctorMagazineRepository;

    public Page<PetDoctorMagazineMapper> getMagazineListByCtIdx(final Long ctIdx, final int pcidx,
        final Pageable pageable) {
        return petDoctorMagazineRepository.getListByCtidx(ctIdx, pcidx, pageable);
    }

    public PetDoctorMagazineDetailMapper getMagazineDetail(final long idx) {
        return petDoctorMagazineRepository.getMagazineDetail(idx)
            .orElseThrow(() -> new NotFoundException("수의사의 발견 정보를 찾을수 없습니다."));
    }
}
