package com.pood.server.service.factory;

import com.pood.server.entity.meta.dto.ProductAndWorryFilterDto;
import com.pood.server.entity.user.BodyShape;
import java.util.List;
import lombok.Value;

@Value
public class UserPetBodyWorryFilterFactory {

    private static final long GOODS_CT_IDX_FEED = 2;
    private static final long GOODS_CT_IDX_NUTRIENTS = 4;
    private static final long IMMUNITY_WORRY_IDX = 17;
    private static final long DIET_WORRY_IDX = 6;
    private static final long DOG_WET_FEED_CATEGORY_GOODS_IDX = 7;
    private static final long CAT_WET_FEED_CATEGORY_GOODS_IDX = 53;
    private static final long DOG_PRESCRIPTION_FEED_GOODS_CATEGORY_IDX = 101;
    private static final long CAT_PRESCRIPTION_FEED_GOODS_CATEGORY_IDX = 102;

    BodyShape bodyShape;

    public ProductAndWorryFilterDto create() {

        if (BodyShape.SKINNY.equals(bodyShape)) {
            return ProductAndWorryFilterDto.of(
                List.of(DOG_WET_FEED_CATEGORY_GOODS_IDX, CAT_WET_FEED_CATEGORY_GOODS_IDX),
                GOODS_CT_IDX_FEED,
                null);
        }

        if (BodyShape.NORMAL.equals(bodyShape)) {
            return ProductAndWorryFilterDto.of(
                null,
                GOODS_CT_IDX_NUTRIENTS,
                IMMUNITY_WORRY_IDX
            );
        }

        if (BodyShape.OBESITY.equals(bodyShape)) {
            return ProductAndWorryFilterDto.of(null, GOODS_CT_IDX_FEED, DIET_WORRY_IDX);
        }

        if (BodyShape.HIGHLY_OBESE.equals(bodyShape)) {
            return ProductAndWorryFilterDto.of(
                List.of(DOG_PRESCRIPTION_FEED_GOODS_CATEGORY_IDX,
                    CAT_PRESCRIPTION_FEED_GOODS_CATEGORY_IDX),
                GOODS_CT_IDX_FEED,
                DIET_WORRY_IDX
            );
        }

        return ProductAndWorryFilterDto.empty();
    }
}
