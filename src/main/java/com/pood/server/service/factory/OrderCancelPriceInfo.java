package com.pood.server.service.factory;

import com.pood.server.web.mapper.payment.Money;
import lombok.Value;

@Value
public class OrderCancelPriceInfo {

    int goodsPrice;
    int totalPrice;
    int deliveryFee;
    Money retreievePrice;
    long retreievePoint;
    double retreieveRatio;
    boolean isDeliveryFeeCalculation;

    public int getOrgGoodsPrice() {
        return goodsPrice;
    }

    public Money getGoodsPrice() {
        if (isDeliveryFeeCalculation) {
            return new Money(goodsPrice - deliveryFee);
        }
        return new Money(goodsPrice);
    }

    public double getRetreieveRatio() {
        return retreieveRatio;
    }

    public Money getRetreievePrice() {
        if (isDeliveryFeeCalculation) {
            return retreievePrice.minus(new Money(deliveryFee));
        }
        return retreievePrice;
    }

    public long getRetreievePoint() {
        return retreievePoint;
    }

    public Integer totalPrice() {
        return totalPrice;
    }

    public int getDeliveryFee() {
        if (isDeliveryFeeCalculation) {
            return deliveryFee;
        }
        return 0;
    }
}
