package com.pood.server.service.factory;

import com.pood.server.entity.meta.dto.user_pet_worry_manaer.ArthritisWorryCat;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.ArthritisWorryDog;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.BadBreathWorryCat;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.BadBreathWorryDog;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.ConstipationWorryCat;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.ConstipationWorryDog;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.DiarrheaWorryCat;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.DiarrheaWorryDog;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.EarwaxWorryCat;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.EarwaxWorryDog;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.EyeHealthWorryCat;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.EyeHealthWorryDog;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.FatWorryCat;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.FatWorryDog;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.ImmuneWorryCat;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.ImmuneWorryDog;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.LickFeetWorryCat;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.LickFeetWorryDog;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.SkinWorryCat;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.SkinWorryDog;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.TartarWorryCat;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.TartarWorryDog;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.TearStainsWorryCat;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.TearStainsWorryDog;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.ThrowUpWorryCat;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.ThrowUpWorryDog;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.UnderWeightWorryCat;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.UnderWeightWorryDog;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.UrinationWorryCat;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.UrinationWorryDog;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.UserPetWorryManager;
import com.pood.server.service.pet_solution.UserPetBaseData;

public class UserPetWorryManagerFactory {

    private final UserPetBaseData userPetBaseData;
    private final int ardGroupCode;

    public UserPetWorryManagerFactory(final UserPetBaseData userPetBaseData,
        final int ardGroupCode) {
        this.userPetBaseData = userPetBaseData;
        this.ardGroupCode = ardGroupCode;
    }

    public UserPetWorryManager create() {
        if (userPetBaseData.isPetTypeDog() && ardGroupCode == 501) {
            return new SkinWorryDog();
        } else if (userPetBaseData.isPetTypeCat() && ardGroupCode == 501) {
            return new SkinWorryCat();
        } else if (userPetBaseData.isPetTypeDog() && ardGroupCode == 422) {
            return new TearStainsWorryDog();
        } else if (userPetBaseData.isPetTypeCat() && ardGroupCode == 422) {
            return new TearStainsWorryCat();
        } else if (userPetBaseData.isPetTypeDog() && ardGroupCode == 423) {
            return new EarwaxWorryDog();
        } else if (userPetBaseData.isPetTypeCat() && ardGroupCode == 423) {
            return new EarwaxWorryCat();
        } else if (userPetBaseData.isPetTypeDog() && ardGroupCode == 502) {
            return new LickFeetWorryDog();
        } else if (userPetBaseData.isPetTypeCat() && ardGroupCode == 502) {
            return new LickFeetWorryCat();
        } else if (userPetBaseData.isPetTypeDog() && ardGroupCode == 301) {
            return new ArthritisWorryDog();
        } else if (userPetBaseData.isPetTypeCat() && ardGroupCode == 301) {
            return new ArthritisWorryCat();
        } else if (userPetBaseData.isPetTypeDog() && ardGroupCode == 941) {
            return new FatWorryDog();
        } else if (userPetBaseData.isPetTypeCat() && ardGroupCode == 941) {
            return new FatWorryCat();
        } else if (userPetBaseData.isPetTypeDog() && ardGroupCode == 942) {
            return new UnderWeightWorryDog();
        } else if (userPetBaseData.isPetTypeCat() && ardGroupCode == 942) {
            return new UnderWeightWorryCat();
        } else if (userPetBaseData.isPetTypeDog() && ardGroupCode == 125) {
            return new ConstipationWorryDog();
        } else if (userPetBaseData.isPetTypeCat() && ardGroupCode == 125) {
            return new ConstipationWorryCat();
        } else if (userPetBaseData.isPetTypeDog() && ardGroupCode == 601) {
            return new ThrowUpWorryDog();
        } else if (userPetBaseData.isPetTypeCat() && ardGroupCode == 601) {
            return new ThrowUpWorryCat();
        } else if (userPetBaseData.isPetTypeDog() && ardGroupCode == 121) {
            return new UrinationWorryDog();
        } else if (userPetBaseData.isPetTypeCat() && ardGroupCode == 121) {
            return new UrinationWorryCat();
        } else if (userPetBaseData.isPetTypeDog() && ardGroupCode == 201) {
            return new EyeHealthWorryDog();
        } else if (userPetBaseData.isPetTypeCat() && ardGroupCode == 201) {
            return new EyeHealthWorryCat();
        } else if (userPetBaseData.isPetTypeDog() && ardGroupCode == 241) {
            return new TartarWorryDog();
        } else if (userPetBaseData.isPetTypeCat() && ardGroupCode == 241) {
            return new TartarWorryCat();
        } else if (userPetBaseData.isPetTypeDog() && ardGroupCode == 145) {
            return new BadBreathWorryDog();
        } else if (userPetBaseData.isPetTypeCat() && ardGroupCode == 145) {
            return new BadBreathWorryCat();
        } else if (userPetBaseData.isPetTypeDog() && ardGroupCode == 961) {
            return new ImmuneWorryDog();
        } else if (userPetBaseData.isPetTypeCat() && ardGroupCode == 961) {
            return new ImmuneWorryCat();
        } else if (userPetBaseData.isPetTypeDog() && ardGroupCode == 126) {
            return new DiarrheaWorryDog();
        }
        return new DiarrheaWorryCat();
    }
}
