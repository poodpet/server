package com.pood.server.service.factory;

import com.pood.server.entity.meta.dto.user_pet_life_cycle.UserPetLifeCycle;
import com.pood.server.entity.meta.dto.user_pet_life_cycle.AdultCat;
import com.pood.server.entity.meta.dto.user_pet_life_cycle.BigAdultDog;
import com.pood.server.entity.meta.dto.user_pet_life_cycle.BigPuppy;
import com.pood.server.entity.meta.dto.user_pet_life_cycle.BigSeniorDog;
import com.pood.server.entity.meta.dto.user_pet_life_cycle.Kitten;
import com.pood.server.entity.meta.dto.user_pet_life_cycle.MediumAdultDog;
import com.pood.server.entity.meta.dto.user_pet_life_cycle.MediumPuppy;
import com.pood.server.entity.meta.dto.user_pet_life_cycle.MediumSeniorDog;
import com.pood.server.entity.meta.dto.user_pet_life_cycle.SeniorCat;
import com.pood.server.entity.meta.dto.user_pet_life_cycle.SmallAdultDog;
import com.pood.server.entity.meta.dto.user_pet_life_cycle.SmallPuppy;
import com.pood.server.entity.meta.dto.user_pet_life_cycle.SmallSeniorDog;
import com.pood.server.service.pet_solution.UserPetBaseData;

public class UserPetLifeCycleFactory {

    private UserPetBaseData userPetBaseData;

    public UserPetLifeCycleFactory(final UserPetBaseData userPetBaseData) {
        this.userPetBaseData = userPetBaseData;
    }

    public UserPetLifeCycle create() {
        if (userPetBaseData.isPcSmallSizeAndPuppy()) {
            return new SmallPuppy();
        } else if (userPetBaseData.isPcSmallSizeAndAdultDog()) {
            return new SmallAdultDog();
        } else if (userPetBaseData.isPcSmallSizeAndOldDog()) {
            return new SmallSeniorDog();
        } else if (userPetBaseData.isPcMediumSizeAndPuppy()) {
            return new MediumPuppy();
        } else if (userPetBaseData.isPcMediumSizeAndAdultDog()) {
            return new MediumAdultDog();
        } else if (userPetBaseData.isPcMediumSizeAndOldDog()) {
            return new MediumSeniorDog();
        } else if (userPetBaseData.isPcBigSizeAndPuppy()) {
            return new BigPuppy();
        } else if (userPetBaseData.isPcBigSizeAndAdultDog()) {
            return new BigAdultDog();
        } else if (userPetBaseData.isPcBigSizeAndOldDog()) {
            return new BigSeniorDog();
        } else if (userPetBaseData.isKitten()) {
            return new Kitten();
        } else if (userPetBaseData.isAdultCat()) {
            return new AdultCat();
        }

        return new SeniorCat();
    }

}
