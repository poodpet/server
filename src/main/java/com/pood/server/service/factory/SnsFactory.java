package com.pood.server.service.factory;

import com.pood.server.api.service.sns.snsService;
import com.pood.server.entity.DeviceType;
import java.util.EnumMap;
import java.util.List;

public class SnsFactory {
    EnumMap<DeviceType, snsService> snsFactoryMap = new EnumMap<>(DeviceType.class);

    public SnsFactory(List<snsService> snsServiceList) {
        snsServiceList.forEach(x -> snsFactoryMap.put(x.getType(), x));
    }

    public snsService getSnsFactoryMap(DeviceType type) {
        return snsFactoryMap.get(type);
    }
}
