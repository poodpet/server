package com.pood.server.service.factory;

import com.pood.server.service.EventTypeJoinPossible;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventTypeFactory {

    Map<String, EventTypeJoinPossible> eventTypeJoinPossibleMap = new HashMap<>();

    public EventTypeFactory(List<EventTypeJoinPossible> eventTypeJoinPossiblesList) {
        eventTypeJoinPossiblesList.forEach(x -> eventTypeJoinPossibleMap.put(x.getType(), x));
    }

    public EventTypeJoinPossible getEventTypeJoinPossibleMap(String type) {
        return eventTypeJoinPossibleMap.get(type);
    }

}
