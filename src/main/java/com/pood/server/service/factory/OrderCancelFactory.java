package com.pood.server.service.factory;

import com.pood.server.entity.order.dto.OrderBaseDataDto;
import java.util.List;

public class OrderCancelFactory {

    private static final int DELIVERY_FEE_FREE = 0;

    public static OrderCancelPriceInfo create(final OrderBaseDataDto orderbaseData,
        final List<Integer> cancelGoodsIdxList, final Integer goodsIdx) {

        final int goodsPrice = orderbaseData.getGoodsPriceByGoodsIdx(goodsIdx);

        // 3. 배송비가 존재하지 않을 경우
        // 3-1. 취소 total 금액이 기본 배송비 보다 높지 않을 경우 반환 불가 반환
        if (orderbaseData.isDeliveryFeeFree()
            && !orderbaseData.isTotalMoneyGreaterThanDeliveryFee(cancelGoodsIdxList)
            && !orderbaseData.isOrderPriceThenDefualtPrice(cancelGoodsIdxList)
            && !orderbaseData.isAllCancel(cancelGoodsIdxList)
        ) {
            throw new IllegalArgumentException(
                "배송비가 존재하지 않을 경우 취소 total 금액이 기본 배송비 보다 높지 않을 경우 반환 불가 반환");
        }

        // 1. 전부 취소일 경우 그대로 반환 || 2. 배송비가 있을 경우 배송비에 대한 계산 없이 그대로 반환
        // || 3. 상품을 취소하더라고 주문 가격이 기본 주문 가격보다 높은 경우 그대로 반환
        if (orderbaseData.isAllCancel(cancelGoodsIdxList) || orderbaseData.isDeliveryFee()
            || orderbaseData.isOrderPriceThenDefualtPrice(cancelGoodsIdxList)
        ) {
            return new OrderCancelPriceInfo(goodsPrice,
                orderbaseData.getTotalPrice().getIntValue(),
                DELIVERY_FEE_FREE,
                orderbaseData.getRetreievePrice(goodsPrice),
                orderbaseData.getRetreievePoint(goodsPrice),
                orderbaseData.getRetreieveRatio(goodsPrice),
                false
            );
        }

        // 배송비가 있고 부분 취소 일 경우
        if (orderbaseData.isDeliveryFee()) {
            return new OrderCancelPriceInfo(goodsPrice,
                orderbaseData.getTotalPrice().getIntValue(),
                DELIVERY_FEE_FREE,
                orderbaseData.getRetreievePrice(goodsPrice),
                orderbaseData.getRetreievePoint(goodsPrice),
                orderbaseData.getRetreieveRatio(goodsPrice),
                orderbaseData.isGoodsPriceGreaterThanDeliveryFee(cancelGoodsIdxList, goodsIdx)
            );
        }

        // 3-3. 취소 total 금액이 기본 배송비 보다 높고 1개의 사품으로 배송비보다 클 경우 그냥 반환
        if (orderbaseData.isDeliveryFeeFree()
            && orderbaseData.isTotalMoneyGreaterThanDeliveryFee(cancelGoodsIdxList)
            && orderbaseData.isGoodsPriceGreaterThanDeliveryFee(cancelGoodsIdxList)) {
            return new OrderCancelPriceInfo(goodsPrice,
                orderbaseData.getTotalPrice().getIntValue(),
                orderbaseData.getReOrgDeliveryFee(),
                orderbaseData.getRetreievePrice(goodsPrice),
                orderbaseData.getRetreievePoint(goodsPrice),
                orderbaseData.getRetreieveRatio(goodsPrice),
                orderbaseData.isGoodsPriceGreaterThanDeliveryFee(cancelGoodsIdxList, goodsIdx)
            );
        }

        // 3-2. 취소 total 금액이 기본 배송비 보다 높고 1개의 상품으론 배송비보다 작을 경우 나눠 반환
        return new OrderCancelPriceInfo(goodsPrice,
            orderbaseData.getTotalPrice().getIntValue(),
            orderbaseData.getDeliveryFee(orderbaseData.getReOrgDeliveryFee(), goodsPrice),
            orderbaseData.getRetreievePrice(goodsPrice),
            orderbaseData.getRetreievePoint(goodsPrice),
            orderbaseData.getRetreieveRatio(goodsPrice),
            true
        );
    }

}
