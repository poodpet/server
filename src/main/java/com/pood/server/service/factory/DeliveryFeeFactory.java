package com.pood.server.service.factory;

import com.pood.server.config.meta.META_DELIVERY_FEE;
import com.pood.server.service.order.DeliveryFeeCalc;
import com.pood.server.service.order.DeliveryFeeCalcFirstPurchaseIslands;
import com.pood.server.service.order.DeliveryFeeCalcFirstPurchaseNotIslands;
import com.pood.server.service.order.DeliveryFeeCalcIslandsAndBasicFree;
import com.pood.server.service.order.DeliveryFeeCalcIslandsAndBasicPaid;
import com.pood.server.service.order.DeliveryFeeCalcNotIslandsAndBasicFree;
import com.pood.server.service.order.DeliveryFeeCalcNotIslandsAndBasicPaid;
import com.pood.server.web.mapper.payment.Money;
import lombok.Value;

@Value
public class DeliveryFeeFactory {

    public static DeliveryFeeCalc create(final boolean isPaySuccess,
        final boolean isNotIslandsDelivery, final Integer orderPrice) {

        if (!isPaySuccess &&
            isNotIslandsDelivery) {
            return new DeliveryFeeCalcFirstPurchaseNotIslands(new Money(0));
        }
        if (!isPaySuccess) {
            return new DeliveryFeeCalcFirstPurchaseIslands(new Money(0));
        }

        if (isNotIslandsDelivery &&
            orderPrice >= META_DELIVERY_FEE.DEFUALT_PRICE) {
            return new DeliveryFeeCalcNotIslandsAndBasicFree(
                new Money(0));
        }
        if (isNotIslandsDelivery) {
            return new DeliveryFeeCalcNotIslandsAndBasicPaid(
                new Money(META_DELIVERY_FEE.DEFUALT_DELIVERY_FEE));
        }
        if (orderPrice >= META_DELIVERY_FEE.DEFUALT_PRICE) {
            return new DeliveryFeeCalcIslandsAndBasicFree(
                new Money(0));
        }

        return new DeliveryFeeCalcIslandsAndBasicPaid(
            new Money(META_DELIVERY_FEE.DEFUALT_DELIVERY_FEE));
    }
}
