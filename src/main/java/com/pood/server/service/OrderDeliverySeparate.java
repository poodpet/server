package com.pood.server.service;

import com.amazonaws.services.kms.model.NotFoundException;
import com.pood.server.entity.order.OrderDelivery;
import com.pood.server.repository.order.OrderDeliveryRepository;
import com.pood.server.util.OrderDeliveryType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderDeliverySeparate {
    final OrderDeliveryRepository orderDeliveryRepository;

    public OrderDelivery getOrderDeliveryByOrderNumberAndDefault(final String orderNumber){

        return orderDeliveryRepository.findByOrderNumberAndDeliveryType(orderNumber,
                OrderDeliveryType.DEFAULT.getCode())
            .orElseThrow(()-> new NotFoundException("해당 주문의 배송지를 찾을수 없습니다."));
    }

}
