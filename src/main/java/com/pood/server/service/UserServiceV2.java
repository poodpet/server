package com.pood.server.service;

import static com.pood.server.exception.ErrorMessage.IMAGE;
import static com.pood.server.exception.ErrorMessage.USER;

import com.pood.server.dto.meta.goods.UserBaseImageDto;
import com.pood.server.dto.user.UserDto;
import com.pood.server.dto.user.UserDto.PointDto;
import com.pood.server.entity.log.LogUserSavePoint;
import com.pood.server.entity.log.LogUserUsePoint;
import com.pood.server.entity.user.UserBaseImage;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.exception.CustomStatusException;
import com.pood.server.repository.log.LogUserSavePointRepository;
import com.pood.server.repository.log.LogUserUsePointRepository;
import com.pood.server.repository.user.UserBaseImageRepository;
import com.pood.server.repository.user.UserInfoRepository;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceV2 {

    private static final int COMPLETE = 12;

    @Autowired
    UserInfoRepository userInfoRepository;

    @Autowired
    UserBaseImageRepository userBaseImageRepository;


    @Autowired
    LogUserSavePointRepository logUserSavePointRepository;

    @Autowired
    LogUserUsePointRepository logUserUsePointRepository;


    public List<UserBaseImageDto.UserBaseImageList> getUserBaseImgList(final String token) {
        UserInfo userByToken = userInfoRepository.getUserByToken(token);
        Optional.ofNullable(userByToken)
            .orElseThrow(() -> CustomStatusException.unAuthorized(USER));
        return userBaseImageRepository.findAllByBasicIsTrue()
            .stream().map(x -> new UserBaseImageDto.UserBaseImageList(x.getIdx(), x.getUrl()))
            .collect(Collectors.toList());
    }

    public void saveUserBaseImg(UserBaseImageDto.IdxDto idxDto, String token) {
        UserInfo userInfo = userInfoRepository.getUserByToken(token);
        Optional.ofNullable(userInfo).orElseThrow(() -> CustomStatusException.unAuthorized(USER));
        UserBaseImage userBaseImage = userBaseImageRepository.findById(idxDto.getIdx())
            .orElseThrow(() -> CustomStatusException.badRequest(IMAGE));
        userInfo.setUserBaseImage(userBaseImage);

        userInfoRepository.save(userInfo);
    }

    public List<UserDto.PointDto> getMyPointList(String token) {
        UserInfo userInfo = userInfoRepository.getUserByTokenOptional(token)
            .orElseThrow(() -> CustomStatusException.unAuthorized(USER));

        List<LogUserUsePoint> userUsePointList = logUserUsePointRepository.findAllByUserUuid(
            userInfo.getUserUuid());
        List<LogUserSavePoint> userSavePointList = logUserSavePointRepository.findAllByUserUuidAndType(
            userInfo.getUserUuid(), COMPLETE);

        List<UserDto.PointDto> pointDtoList = new ArrayList<>();
        pointDtoList.addAll(
            userUsePointList.stream()
                .map(LogUserUsePoint::createPointDto)
                .collect(Collectors.toList())
        );

        pointDtoList.addAll(
            userSavePointList.stream()
                .map(LogUserSavePoint::createPointDto)
                .collect(Collectors.toList())
        );

        pointDtoList.sort(Comparator.comparing(PointDto::getRecordBirth).reversed());
        return pointDtoList;
    }

    public HashMap<Object, Object> checkMyPhoneNumber(String token, UserDto.Phone phone) {
        HashMap<Object, Object> result = new HashMap<>();
        String phoneNumber = phone.getUserPhone().replaceAll("-", "");
        UserInfo userInfo = userInfoRepository.getUserByTokenOptional(token)
            .orElseThrow(() -> CustomStatusException.unAuthorized(USER));
        if (userInfo.getUserPhone().replaceAll("-", "").equals(phoneNumber)) {
            result.put("msg", "현재 번호와 같습니다.");
            result.put("valid", false);
            return result;
        }
        Integer cnt = userInfoRepository.countByUserUuidNotAndUserPhone(userInfo.getUserUuid(),
            phoneNumber);
        if (cnt > 0) {
            result.put("msg", "이미 등록된 번호 입니다.");
            result.put("valid", false);
            return result;
        }
        result.put("msg", "");
        result.put("valid", true);
        return result;
    }
}
