package com.pood.server.service;

import com.pood.server.entity.log.LogUserToken;
import com.pood.server.entity.user.UserToken;
import com.pood.server.repository.log.LogUserTokenRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("logTransactionManager")
public class LogUserTokenSeparate {

    private final LogUserTokenRepository userTokenRepository;

    public void saveUserTokenLog(final UserToken userToken) {
        userTokenRepository.save(new LogUserToken(userToken.getToken(), userToken.getUserUuid()));
    }
}
