package com.pood.server.service;

import com.pood.server.dto.meta.goods.PromotionDto.PromotionGoods;
import com.pood.server.dto.meta.goods.PromotionDto.PromotionGoodsListInfo;
import com.pood.server.dto.meta.goods.PromotionDto.PromotionGoodsProductInfo;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.meta.PromotionRepository;
import java.util.List;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PromotionSeparate {

    private final PromotionRepository promotionRepository;

    public PromotionGoodsListInfo getPromotionGooodsInfo(final int idx) {
        final PromotionGoodsListInfo listInfoById = promotionRepository.findListInfoById(idx);

        if (Objects.isNull(listInfoById)) {
            throw new NotFoundException("프로모션 상품 정보를 찾을 수 없습니다.");
        }

        return listInfoById;
    }

    public List<PromotionGoods> getPromotionGoodsDetail(final int groupIdx) {
        return promotionRepository.findPromotionGoodsDetail(
            groupIdx);
    }

    public List<PromotionGoods> getPromotionGoodsDetailInGroupIdx(
        final List<Integer> groupIdxList) {
        return promotionRepository.findPromotionGoodsDetail(
            groupIdxList);
    }

    public List<PromotionGoodsProductInfo> findByGoodsIdxAndNowActiveList(
        final List<Integer> goodsIdxList) {
        return promotionRepository.findByGoodsIdxAndNowActiveList(goodsIdxList);
    }

}
