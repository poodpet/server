package com.pood.server.service;

import com.pood.server.api.file.uploader.StorageService;
import com.pood.server.entity.user.UserPetImage;
import com.pood.server.exception.LimitQuantityException;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.user.UserPetImageRepository;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
@Transactional("userTransactionManager")
public class UserPetImageSeparate {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserPetImageSeparate.class);
    private static final String USER_PET_FOLDER = "user_pet";

    private final UserPetImageRepository userPetImageRepository;
    private final StorageService storageService;

    public UserPetImageSeparate(
        final UserPetImageRepository userPetImageRepository,
        @Qualifier("storageService") final StorageService storageService) {
        this.userPetImageRepository = userPetImageRepository;
        this.storageService = storageService;
    }

    public UserPetImage findByIdx(final int imageIdx) {
        return userPetImageRepository.findById(imageIdx)
            .orElseThrow(() -> new NotFoundException("해당하는 이미지를 찾을 수 없습니다."));
    }

    public void saveUserPetImage(final int userPetIdx, List<MultipartFile> multipartFile) {
        if (multipartFile.isEmpty()) {
            multipartFile = Collections.emptyList();
        }

        for (MultipartFile file : multipartFile) {
            try {
                final String url = storageService.multiPartFileStore(file, USER_PET_FOLDER);
                userPetImageRepository.save(UserPetImage.builder()
                    .url(url)
                    .visible(0)
                    .userPetIdx(userPetIdx)
                    .build());
            } catch (IOException e) {
                LOGGER.error("유저 펫 이미지를 등록하는데 실패했습니다. " + e.getMessage());
            }
        }
    }

    public List<UserPetImage> findAllByIdxInRecordBirthDesc(final List<Integer> userPetIdxList) {
        return userPetImageRepository.findAllByIdxInRecordBirthDesc(userPetIdxList);
    }

    public List<UserPetImage> userPetImageList(final List<Integer> petIdxList) {
        return userPetImageRepository.findByUserPetIdxInOrderByIdxDesc(
            petIdxList);
    }

    public void deleteByUserPetIdx(final int userPetIdx) {
        final List<UserPetImage> byUserPetIdxOrderByIdxDesc = userPetImageRepository.findByUserPetIdxOrderByIdxDesc(
            userPetIdx);

        for (UserPetImage userPetImage : byUserPetIdxOrderByIdxDesc) {
            storageService.deleteObject(userPetImage.getUrl());
        }

        userPetImageRepository.deleteByUserPetIdx(userPetIdx);
    }

    public void findByIdxAndThenUpdateImage(final int userPetIdx,
        final List<MultipartFile> multipartFileList) {
        sizeValidation(multipartFileList);

        // 이미지가 있다가 없어지는 경우
        final Optional<UserPetImage> optionalUserPetImage = userPetImageRepository.findByUserPetIdx(userPetIdx);
        
        MultipartFile file = null;
        if (!multipartFileList.isEmpty()) {
            file = multipartFileList.get(0);
        }

        //이미지 있고 펫이미지도 있는경우
        if (optionalUserPetImage.isPresent()) {
            final UserPetImage userPetImage = optionalUserPetImage.get();
            storageService.deleteObject(userPetImage.getUrl());
            saveImage(file, userPetImage);
            return;
        }

        //펫이미지 없고 request이미지 있는경우
        try {
            if (Objects.nonNull(file)) {
                final String url = storageService.multiPartFileStore(file, USER_PET_FOLDER);
                userPetImageRepository.save(UserPetImage.builder()
                    .url(url)
                    .visible(0)
                    .userPetIdx(userPetIdx)
                    .build());
            }
        } catch (IOException e) {
            LOGGER.error("유저 펫 이미지를 등록하는데 실패했습니다. " + e.getMessage());
        }
    }

    private void sizeValidation(final List<MultipartFile> multipartFileList) {
        if (multipartFileList.size() >= 2) {
            throw new LimitQuantityException("이미지는 2개 이상일 수 없습니다.");
        }
    }

    private boolean fileIsNullAndImageIsNotNull(final List<MultipartFile> multipartFileList,
        final Optional<UserPetImage> optionalUserPetImage) {
        return multipartFileList.isEmpty() && optionalUserPetImage.isPresent();
    }

    private void saveImage(final MultipartFile multipartFile, final UserPetImage userPetImage) {
        try {
            if (Objects.nonNull(multipartFile)) {
                final String url = storageService.multiPartFileStore(multipartFile, USER_PET_FOLDER);
                userPetImage.updateImageUrl(url);
            }
        } catch (IOException e) {
            LOGGER.error("유저 펫 이미지를 수정하는데 실패했습니다. " + e.getMessage());
        }
    }
}
