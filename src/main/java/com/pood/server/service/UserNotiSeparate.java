package com.pood.server.service;

import com.pood.server.entity.user.UserNoti;
import com.pood.server.repository.user.UserNotiRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("userTransactionManager")
public class UserNotiSeparate {

    private final UserNotiRepository userNotiRepository;

    public List<UserNoti> findAllUserNoti(final String userUuid) {
        return userNotiRepository.findAllByUserUuidOrderByIdxDesc(userUuid);
    }

}
