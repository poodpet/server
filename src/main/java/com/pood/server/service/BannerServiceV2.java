package com.pood.server.service;

import com.pood.server.entity.meta.Banner;
import com.pood.server.entity.meta.BannerImage;
import com.pood.server.repository.meta.BannerImageRepository;
import com.pood.server.repository.meta.BannerRepository;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BannerServiceV2 {

    @Autowired
    BannerRepository bannerRepository;

    @Autowired
    BannerImageRepository bannerImageRepository;

    public List<Banner> getBannerList(Integer pcIdx) {
        LocalDateTime nowDate = LocalDateTime.now();
        List<Banner> bannerList = bannerRepository.findAllByPcIdAndStatusAndStartPeriodBeforeAndEndPeriodAfter(
            pcIdx, false, nowDate, nowDate);
        List<BannerImage> bannerImageList = bannerImageRepository.findAllByBannerIdxIn(
            bannerList.stream().map(Banner::getIdx).collect(Collectors.toList()));
        for (Banner banner : bannerList) {
            for (BannerImage bannerImage : bannerImageList) {
                if (banner.getIdx().equals(bannerImage.getBannerIdx())) {
                    banner.setBannerImage(bannerImage);
                    break;
                }
            }
        }
        return bannerList;
    }
}
