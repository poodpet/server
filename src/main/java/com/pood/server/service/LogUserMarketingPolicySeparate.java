package com.pood.server.service;

import com.pood.server.controller.request.user.UserSignUpRequest;
import com.pood.server.entity.log.LogUserMarketingPolicy;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.repository.log.LogUserMarketingPolicyRepository;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("logTransactionManager")
public class LogUserMarketingPolicySeparate {

    private static final int EMAIL_NOTICE = 2;
    private static final int PRIVATE_POLICY = 3;

    private final LogUserMarketingPolicyRepository policyRepository;

    public void savePolicyAll(final UserSignUpRequest request, final UserInfo saveUser) {
        List<LogUserMarketingPolicy> list = new ArrayList<>();

        list.add(new LogUserMarketingPolicy(saveUser.getIdx(), saveUser.getUserUuid(),
            PRIVATE_POLICY, request.isPrivatePolicy()));

        if (request.isAdAgreement()) {
            list.add(new LogUserMarketingPolicy(saveUser.getIdx(), saveUser.getUserUuid(),
                EMAIL_NOTICE, request.isAdAgreement()));
        }

        policyRepository.saveAll(list);
    }

}
