package com.pood.server.service;

import com.pood.server.entity.user.UserSmsAuth;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.user.UserSmsAuthRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional("userTransactionManager")
@RequiredArgsConstructor
public class UserSmsAuthSeparate {

    private static final String MASTER_AUTHENTICATION_NUMBER = "2020";

    private final UserSmsAuthRepository authRepository;

    public void findAuthenticationRecord(final String phoneNumber, final String authNumber) {
        final UserSmsAuth authRecord = authRepository.findTop1ByPhoneNumberOrderByIdxDesc(phoneNumber)
            .orElseThrow(() -> new NotFoundException("해당하는 휴대폰 번호의 인증 기록을 찾을 수 없습니다."));
        //박람회 대비 마스터키로 업데이트 치는 로직
        if (!authNumber.equals(MASTER_AUTHENTICATION_NUMBER)) {
            authRecord.isNotExpired();
            authRecord.authenticationCheck(authNumber);
        }

        authRecord.updateAuthenticationStatus();
    }
}
