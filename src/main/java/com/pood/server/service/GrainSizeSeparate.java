package com.pood.server.service;

import com.pood.server.entity.meta.GrainSize;
import com.pood.server.repository.meta.GrainSizeRepository;
import java.util.Collection;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("metaTransactionManager")
public class GrainSizeSeparate {

    private final GrainSizeRepository grainSizeRepository;

    public List<GrainSize> findAllIdxIn(final Collection<Integer> grainSizeList) {
        return grainSizeRepository.findAllByIdxIn(grainSizeList);
    }

    public List<GrainSize> findAll() {
        return grainSizeRepository.findAll(Sort.by(Direction.ASC, "priority"));
    }
}
