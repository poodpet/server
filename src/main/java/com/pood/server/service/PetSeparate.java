package com.pood.server.service;

import com.pood.server.entity.meta.Pet;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.meta.PetRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PetSeparate {

    private final PetRepository petRepository;

    public List<Pet> getPetAllByIdxIn(List<Integer> petIdxList) {
        return petRepository.findAllByIdxIn(petIdxList);
    }

    public Pet getPetInfo(final int petIdx) {
        return petRepository.findById(petIdx)
            .orElseThrow(() -> new NotFoundException("해당하는 메타 펫 정보가 없습니다."));
    }

}
