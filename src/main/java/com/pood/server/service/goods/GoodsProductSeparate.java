package com.pood.server.service.goods;

import com.pood.server.entity.meta.dto.goods.GoodsProductGroupByGoodsDto;
import com.pood.server.entity.meta.group.GoodsProductMapperGroup;
import com.pood.server.repository.meta.GoodsProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("metaTransactionManager")
public class GoodsProductSeparate {

    private final GoodsProductRepository goodsProductRepository;

    public GoodsProductGroupByGoodsDto getGoodsProductIdxDto(final Integer goodsIdx) {
        return GoodsProductMapperGroup.of(
            goodsProductRepository.findAllByGoodsIdxToMapper(goodsIdx)
        ).toGoodsProductGroupByDto();
    }

    public int getProductQuantity(final Integer goodsIdx, final Integer productIdx) {
        return goodsProductRepository.findByGoodsIdxAndProductIdx(goodsIdx, productIdx).getProductQty();
    }
}

