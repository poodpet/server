package com.pood.server.service;

import com.pood.server.entity.meta.AiRecommendDiagnosis;
import com.pood.server.repository.meta.AiRecommendDiagnosisRepository;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("metaTransactionManager")
public class AiRecommendDiagnosisSeparate {

    private final AiRecommendDiagnosisRepository aiRecommendDiagnosisRepository;

    public List<AiRecommendDiagnosis> findAllByIdxIn(final Collection<Integer> aiRecommendIdxList) {
        return aiRecommendDiagnosisRepository.findAllByIdxIn(aiRecommendIdxList);
    }

    public List<Integer> findArdCodeAllByIdxIn(final Collection<Integer> aiRecommendIdxList) {
        return aiRecommendDiagnosisRepository
            .findAllByIdxIn(aiRecommendIdxList)
            .stream()
            .map(AiRecommendDiagnosis::getArdGroupCode)
            .collect(Collectors.toList());
    }

    public List<Integer> findAllByArdGroupCode(final Integer ardGroupCode) {
        return aiRecommendDiagnosisRepository.findAllByArdGroupCode(ardGroupCode)
            .stream().map(AiRecommendDiagnosis::getIdx)
            .collect(Collectors.toList());
    }
}
