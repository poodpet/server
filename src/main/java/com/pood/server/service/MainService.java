package com.pood.server.service;

import static com.pood.server.exception.ErrorMessage.PUSH;

import com.pood.server.dto.meta.main.HomeDto;
import com.pood.server.entity.meta.AppInitConfig;
import com.pood.server.entity.meta.PushAlarm;
import com.pood.server.exception.CustomStatusException;
import com.pood.server.repository.meta.EventRepository;
import com.pood.server.repository.meta.PromotionRepository;
import com.pood.server.repository.meta.PushAlarmRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MainService {

    private static final int COMMON = 0;

    private final AppInitConfigService appInitConfigService;
    private final PromotionRepository promotionRepository;
    private final EventRepository eventRepository;
    private final PushAlarmRepository pushAlarmRepository;

    public List<HomeDto.HomeEventAndPromotionDto> getHomeEventAndPromotionDataList(final Integer pcIdx) {
        List<HomeDto.HomeEventAndPromotionDto> promotionListInfo = promotionRepository.findPromotionListInfo()
            .stream().filter(promotion -> promotion.getPcIdx().equals(pcIdx) || promotion.getPcIdx().equals(COMMON))
            .map(promotion ->
                new HomeDto.HomeEventAndPromotionDto(promotion.getIdx(), promotion.getMainImages(), "P",
                    promotion.getType(), promotion.getStartPeriod(), promotion.getEndPeriod())).collect(Collectors.toList());

        Pageable pageable = PageRequest.of(0, 100, Sort.Direction.ASC, "priority");
        List<HomeDto.HomeEventAndPromotionDto> eventListInfo = eventRepository.getProgressEvents(pcIdx,
                pageable).getContent()
            .stream()
            .map(event -> new HomeDto.HomeEventAndPromotionDto(event.getIdx(), event.getImgUrl(), "E",
                event.getTypeName(), event.getStartDate(), event.getEndDate())).collect(Collectors.toList());

        List<HomeDto.HomeEventAndPromotionDto> homeEventAndPromotionList = new ArrayList<>();
        homeEventAndPromotionList.addAll(promotionListInfo);
        homeEventAndPromotionList.addAll(eventListInfo);
        return homeEventAndPromotionList;
    }


    public void plusPushAlarmViewCount(Long idx) {
        PushAlarm pushAlarm = pushAlarmRepository.findById(idx)
            .orElseThrow(() -> CustomStatusException.badRequest(
                PUSH));
        pushAlarm.pluseViewCount();
        pushAlarmRepository.save(pushAlarm);
    }

    public List<AppInitConfig> findAll() {
        return appInitConfigService.findAll();
    }
}
