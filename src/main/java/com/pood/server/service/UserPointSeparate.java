package com.pood.server.service;

import com.pood.server.entity.user.UserPoint;
import com.pood.server.entity.user.group.UserPointGroup;
import com.pood.server.repository.user.UserPointRepository;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("userTransactionManager")
public class UserPointSeparate {

    private static final int WILL_BE_EXPIRED_POINT = 1;
    private static final int SAVED_POINT = 1;
    private final UserPointRepository userPointRepository;

    public List<UserPoint> findAllByUserUuidAndPointIdx(final String userUuid, final int pointIdx) {
        return userPointRepository.findAllByUserUuidAndPointIdx(userUuid, pointIdx);
    }

    public List<UserPoint> willBeExpiredPoint(final String userUuid) {
        return userPointRepository.findAllByUserUuidAndStatus(userUuid, WILL_BE_EXPIRED_POINT);
    }

    public int userExpiredPointInfoBy30Day(final String userUuid) {

        LocalDateTime nowDate = LocalDateTime.now();
        List<UserPoint> userPointList = userPointRepository.findAllByUserUuidAndStatusAndExpiredDateBetween(
            userUuid, SAVED_POINT, nowDate, nowDate.plusDays(30));
        return userPointList
            .stream()
            .mapToInt(UserPoint::getAvailablePoint)
            .sum();
    }

    public UserPointGroup findAllByPointUuidIn(final List<String> pointUuidList) {
        return UserPointGroup.of(userPointRepository.findAllByPointUuidIn(pointUuidList));
    }

    public void refundAllPoint(final Map<UserPoint, Integer> refundUserPointMap) {
        refundUserPointMap.forEach(UserPoint::validateRefundPoint);
        refundUserPointMap.forEach(UserPoint::refundPoint);

        userPointRepository.saveAll(refundUserPointMap.keySet());
    }

}
