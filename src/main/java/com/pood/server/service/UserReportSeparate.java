package com.pood.server.service;

import com.pood.server.controller.request.report.UserReportSaveRequest;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.exception.AlreadyExistException;
import com.pood.server.repository.user.UserEventReportRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional(value = "userTransactionManager")
public class UserReportSeparate {

    private final UserEventReportRepository reportRepository;

    public void reportUser(final UserReportSaveRequest request, final UserInfo userInfo, final int myInfoIdx) {
        if (reportRepository.existsByMyInfoIdx(myInfoIdx)) {
            throw new AlreadyExistException("이미 신고해주신 이력이 있습니다.");
        }

        reportRepository.save(request.toEntity(userInfo, myInfoIdx));
    }
}
