package com.pood.server.service;

import com.pood.server.controller.response.home.delivery.PoodDeliveryCategoryDto;
import com.pood.server.dto.meta.goods.GoodsDto;
import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.dto.meta.goods.PromotionDto;
import com.pood.server.dto.meta.goods.PromotionDto.PromotionGoodsProductInfo;
import com.pood.server.entity.meta.GoodsSubCt;
import com.pood.server.facade.Facade;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Facade
@RequiredArgsConstructor
public class PoodHomeDeliveryFacade {

    private final PoodDeliveryCategorySeparate poodDeliveryCategorySeparate;
    private final GoodsSeparate goodsSeparate;
    private final PromotionSeparate promotionSeparate;
    private final GoodsSubCtSeparate goodsSubCtSeparate;

    public List<PoodDeliveryCategoryDto> getPoodHomeDeliveryCategory(final int pcIdx) {
        return poodDeliveryCategorySeparate.getCategoryList(pcIdx);
    }

    public Page<GoodsDto.SortedGoodsList> getPoodHomeGoodsList(final Long subCtIdx,
        final int pcIdx,
        Pageable pageable) {

        GoodsSubCt goodSubCt = goodsSubCtSeparate.getGoodsSubCtInfoByIdxOrNull(subCtIdx, pcIdx);
        Page<SortedGoodsList> goodsList = goodsSeparate.getPoodHomeGoodsList(goodSubCt, pcIdx,
            pageable);
        List<Integer> goodsIdxList = goodsList
            .stream()
            .map(SortedGoodsList::getIdx)
            .collect(Collectors.toList());

        List<PromotionDto.PromotionGoodsProductInfo> promotionList = promotionSeparate.findByGoodsIdxAndNowActiveList(
            goodsIdxList);

        for (PromotionGoodsProductInfo promotionGoodsProductInfo : promotionList) {
            for (SortedGoodsList sortedGoodsWishList : goodsList) {
                if (sortedGoodsWishList.getIdx().equals(promotionGoodsProductInfo.getGoodsIdx())) {
                    sortedGoodsWishList.promotionIfExistUpdatePrice(promotionGoodsProductInfo);
                }
            }
        }

        return goodsList;
    }

}
