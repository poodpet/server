package com.pood.server.service;

import com.pood.server.entity.log.LogUserCouponCode;
import com.pood.server.exception.AlreadyExistException;
import com.pood.server.repository.log.LogUserCouponCodeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("logTransactionManager")
public class LogUserCouponCodeSeparate {

    private final LogUserCouponCodeRepository logUserCouponCodeRepository;

    public void isExistCouponCode(final String userUuid, final String code) {
        if (logUserCouponCodeRepository.existsByUserUuidAndCode(userUuid, code)) {
            throw new AlreadyExistException("이미 쿠폰 발급한 이력이 있습니다.");
        }
    }

    public void save(final LogUserCouponCode couponCode) {
        logUserCouponCodeRepository.save(couponCode);
    }

}
