package com.pood.server.service;

import com.pood.server.dto.user.basket.UserBasketGroup;
import com.pood.server.entity.user.UserBasket;
import com.pood.server.repository.user.UserBasketRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserBasketSeparate {

    private final UserBasketRepository userBasketRepository;

    public List<UserBasket> findAllByGoodsIdxAndUserUUid(final int goodsIdx, final String userUuid) {
        return userBasketRepository.findAllByGoodsIdxAndUserUuid(goodsIdx, userUuid);
    }

    public int totalQuantity(final int goodsIdx, final String userUuid) {
        final UserBasketGroup userBasketGroup = new UserBasketGroup(findAllByGoodsIdxAndUserUUid(goodsIdx, userUuid));
        return userBasketGroup.totalBoughtQuantity();
    }

}
