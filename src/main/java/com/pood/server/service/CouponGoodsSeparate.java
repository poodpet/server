package com.pood.server.service;

import com.pood.server.entity.meta.CouponGoods;
import com.pood.server.repository.meta.CouponGoodsRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("metaTransactionManager")
public class CouponGoodsSeparate {

    private final CouponGoodsRepository couponGoodsRepository;

    public List<CouponGoods> findAll() {
        return couponGoodsRepository.findAll();
    }

}
