package com.pood.server.service;

import com.pood.server.entity.user.UserWish;
import com.pood.server.repository.user.UserWishRepository;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("userTransactionManager")
public class UserWishSeparate {

    private final UserWishRepository userWishRepository;

    public void saveUserWish(final UserWish userWish) {
        userWishRepository.save(userWish);
    }

    public boolean wishValidation(final int userIdx, final int goodsIdx) {
        return userWishRepository.existsByUserIdxAndGoodsIdx(userIdx, goodsIdx);
    }

    public int deleteUserWish(final int userIdx, final int goodsIdx) {
        return userWishRepository.deleteByUserIdxAndGoodsIdx(userIdx, goodsIdx);
    }

    public List<Integer> findUserWishList(final Integer userIdx, final List<Integer> goodsIdxList) {
        return userWishRepository.findAllByUserIdxAndGoodsIdxIn(userIdx, goodsIdxList).stream()
            .map(UserWish::getGoodsIdx)
            .collect(Collectors.toList());
    }

}
