package com.pood.server.service;

import com.pood.server.entity.user.UserToken;
import com.pood.server.repository.user.UserTokenRepository;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional("userTransactionManager")
@RequiredArgsConstructor
public class UserTokenSeparate {

    private final UserTokenRepository userTokenRepository;

    public UserToken saveToken(final String userUuid) {
        final Optional<UserToken> existUserToken = userTokenRepository.findByUserUuid(userUuid);

        if (existUserToken.isPresent()) {
            return existUserToken.get().updateToken();
        }

        return userTokenRepository.save(UserToken.builder()
            .token(UUID.randomUUID().toString())
            .userUuid(userUuid)
            .build());
    }
}
