package com.pood.server.service;

import com.pood.server.dto.meta.event.EventDto.EventDetail;
import com.pood.server.repository.meta.EventCommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EventCommentSeparate implements EventTypeJoinPossible {

    private static final String COMMENT_TYPE = "C";

    private final EventCommentRepository eventCommentRepository;

    @Override
    public boolean isParticipationPossible(EventDetail eventDetail, int userIdx) {
        int count = eventCommentRepository.countAllByEventInfoIdxAndUserInfoIdx(
            eventDetail.getIdx(),
            userIdx);
        return eventDetail.getAttendLimit() > count;
    }

    @Override
    public String getType() {
        return COMMENT_TYPE;
    }
}
