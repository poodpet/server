package com.pood.server.service.log;

import com.pood.server.entity.log.LogUserPoint;
import com.pood.server.repository.log.LogUserPointRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("logTransactionManager")
public class LogUserPointSeparate {

    private final LogUserPointRepository logUserPointRepository;

    public void insertAll(final List<LogUserPoint> insertLogUserPointList) {
        logUserPointRepository.saveAll(insertLogUserPointList);
    }
}
