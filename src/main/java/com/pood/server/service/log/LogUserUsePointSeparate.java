package com.pood.server.service.log;

import com.pood.server.entity.log.LogUserUsePoint;
import com.pood.server.repository.log.LogUserUsePointRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("logTransactionManager")
public class LogUserUsePointSeparate {

    private final LogUserUsePointRepository logUserUsePointRepository;

    public void insertCancelLog(final String userUuid, final String text, final Integer point) {
        logUserUsePointRepository.save(LogUserUsePoint.ofRefundPoint(userUuid, text, point));
    }
}
