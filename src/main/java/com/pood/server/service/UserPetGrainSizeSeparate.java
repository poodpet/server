package com.pood.server.service;

import com.pood.server.dto.meta.user.pet.UserPetGrainSizeGroup;
import com.pood.server.entity.meta.GrainSize;
import com.pood.server.entity.user.UserPetGrainSize;
import com.pood.server.repository.user.UserPetGrainSizeRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("userTransactionManager")
public class UserPetGrainSizeSeparate {

    private final UserPetGrainSizeRepository userPetGrainSizeRepository;

    public void insertGrainSizeAll(final List<GrainSize> grainSizeList, final int userPetIdx) {
        final UserPetGrainSizeGroup userPetGrainSizeGroup = new UserPetGrainSizeGroup();
        userPetGrainSizeRepository.saveAll(
            userPetGrainSizeGroup.saveAllObjects(grainSizeList,
                userPetIdx));
    }

    public void deleteAllByUserPetIdx(final int userPetIdx) {
        userPetGrainSizeRepository.deleteAllByUserPetIdx(userPetIdx);
    }

    public List<UserPetGrainSize> getUserPetGrainSizeList(final int userPetIdx) {
        return userPetGrainSizeRepository.findAllByUserPetIdx(userPetIdx);
    }

}
