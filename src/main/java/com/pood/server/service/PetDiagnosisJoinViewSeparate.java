package com.pood.server.service;

import com.pood.server.entity.user.PetDiagnosisJoinView;
import com.pood.server.repository.user.PetDiagnosisJoinViewRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("userTransactionManager")
public class PetDiagnosisJoinViewSeparate {

    private final PetDiagnosisJoinViewRepository petDiagnosisJoinViewRepository;

    public List<PetDiagnosisJoinView> findAllByIdxIn(final List<Integer> userPetIdx) {
        return petDiagnosisJoinViewRepository.findAllByUserPetIdxIn(userPetIdx);
    }

}
