package com.pood.server.service;

import com.pood.server.entity.user.UserRequestRestock;
import com.pood.server.repository.user.UserRequestRestockRepository;
import com.pood.server.util.RestockState;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("userTransactionManager")
public class UserRequestRestockSeparate {

    private final UserRequestRestockRepository repository;

    public String insertRequestRestock(final Integer goodsIdx, final String userUuid) {

        if (!duplicateRequestReStock(goodsIdx, userUuid)) {
            final UserRequestRestock entity = UserRequestRestock.create(goodsIdx, userUuid);
            repository.save(entity);
            return "해당 상품의 재입고 요청이 성공적으로 완료되었습니다.";
        }

        return "이미 재입고 요청한 상품입니다.";
    }

    private boolean duplicateRequestReStock(final Integer goodsIdx, final String userUuid) {

        return repository.existsByGoodsIdxAndUserUuidAndState(goodsIdx, userUuid,
            RestockState.WAIT);
    }
}
