package com.pood.server.service;

import com.pood.server.entity.meta.BrandImage;
import com.pood.server.repository.meta.BrandImageRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BrandImagesSeparate {

    private final BrandImageRepository brandImageRepository;

    public List<BrandImage> getBrandImages(final int brandIdx) {
        return brandImageRepository.findAllByBrandIdx(brandIdx);
    }

}
