package com.pood.server.service;

import com.pood.server.dto.order.OrderRankGroup;
import com.pood.server.entity.meta.dto.home.OrderRankDto;
import com.pood.server.entity.meta.mapper.order.RefundDeliveryMapper;
import com.pood.server.entity.order.Order;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.order.OrderRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional("orderTransactionManager")
@RequiredArgsConstructor
public class OrderSeparate {

    private static final int DELIVERY_SUCCESS = 12;

    private final OrderRepository orderRepository;

    public boolean isExistOrderHistory(final int userIdx) {
        return orderRepository.existsByUserIdxAndOrderStatus(userIdx, DELIVERY_SUCCESS);
    }

    public Page<OrderRankDto> findRankGoodsByInGoodsIdx(final List<Integer> goodsIdxList,
        final String searchType, final Pageable pageable) {
        return orderRepository.findRankGoodsByInGoodsIdx(goodsIdxList, searchType, pageable);
    }

    public OrderRankGroup getRankGoodsGroup(final List<Integer> goodsIdxList,
        final String searchType, final Pageable pageable) {
        return new OrderRankGroup(findRankGoodsByInGoodsIdx(goodsIdxList, searchType, pageable));
    }

    public OrderRankGroup getUserBuyGoodsListByBuyCount(final int buyCount, final int userIdx,
        final List<Integer> goodsIdxList, final Pageable pageable) {
        return new OrderRankGroup(
            findUserBuyGoodsListByBuyCount(buyCount, userIdx, goodsIdxList, pageable));
    }

    private Page<OrderRankDto> findUserBuyGoodsListByBuyCount(final int buyCount, final int userIdx,
        final List<Integer> goodsIdxList, final Pageable pageable) {
        return orderRepository.findUserBuyGoodsListByBuyCount(buyCount, userIdx, goodsIdxList,
            pageable);
    }

    public RefundDeliveryMapper getRefundInfo(final int refundIdx, final int userIdx) {
        return orderRepository.getRefundDeliveryInfo(refundIdx, userIdx);
    }

    public Order getOrderInfo(final String orderNumber) {
        return orderRepository.findByOrderNumber(orderNumber)
            .orElseThrow(() -> new NotFoundException("주문 정보를 찾을 수 없습니다."));
    }

    public Order findByOrderNumberAndUserUuid(final String orderNumber, final String userUUID) {
        return orderRepository.findByOrderNumberAndUserUuid(orderNumber, userUUID)
            .orElseThrow(() -> new NotFoundException("주문 정보를 찾을 수 없습니다."));
    }

    public void updateDeliveryFee(final String orderNumber, final int deliveryFee) {
        final Order orderInfo = getOrderInfo(orderNumber);
        orderInfo.addDeliveryFee(deliveryFee);
    }
}
