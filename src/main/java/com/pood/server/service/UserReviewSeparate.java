package com.pood.server.service;

import com.pood.server.api.mapper.user.review.ReviewRatingAndCountDto;
import com.pood.server.dto.user.review.GoodsDetailReviewDto;
import com.pood.server.dto.user.review.GoodsImagesReviewDto;
import com.pood.server.entity.user.UserReview;
import com.pood.server.exception.CustomStatusException;
import com.pood.server.exception.ErrorMessage;
import com.pood.server.repository.user.UserReviewRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("userTransactionManager")
public class UserReviewSeparate {

    private final UserReviewRepository userReviewRepository;

    public UserReview findByIdx(final int idx) {
        return userReviewRepository.findById(idx)
            .orElseThrow(() -> CustomStatusException.badRequest(ErrorMessage.REVIEW));
    }

    public List<UserReview> findAllByIdxIn(final List<Integer> idx) {
        return userReviewRepository.findAllByIdxIn(idx);
    }

    public Page<GoodsDetailReviewDto> getGoodsReviewList(final Pageable pageable,
        final Integer goodsIdx, final Integer userIdx, final List<Integer> notInReviewIdxList) {
        return userReviewRepository.getGoodsReviewList(pageable, goodsIdx, userIdx,
            notInReviewIdxList);
    }

    public ReviewRatingAndCountDto getReviewRating(final int goodsIdx) {
        return userReviewRepository.getRatingAndCountByGoodsIdx(goodsIdx);
    }

    public Page<GoodsImagesReviewDto> getReviewPhotoImgListV2(final Pageable pageable,
        final Integer goodsIdx, final List<Integer> notInReviewIdxList) {
        return userReviewRepository.getReviewPhotoImgListV2(pageable, goodsIdx,
            notInReviewIdxList);
    }

    public List<UserReview> getUserReviewList(final int userIdx) {
        return userReviewRepository.findAllByUserIdx(userIdx);
    }

    public void modifyReviewText(final int userIdx, final int reviewIdx, final String text) {
        final UserReview userReview = userReviewRepository.findByUserIdxAndIdx(userIdx, reviewIdx)
            .orElseThrow(() -> CustomStatusException.badRequest(ErrorMessage.REVIEW));
        userReview.modifyReviewText(text);
    }

}
