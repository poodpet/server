package com.pood.server.service;

import com.pood.server.entity.user.UserReferralCode;
import com.pood.server.repository.user.UserReferralCodeRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("userTransactionManager")
public class UserRecommendCodeSeparate {

    private final UserReferralCodeRepository userReferralCodeRepository;

    public List<UserReferralCode> findAllJoinUserInfoId(final int userInfoIdx) {
        return userReferralCodeRepository.findAllJoinUserInfoId(userInfoIdx);
    }

    public void save(final int receivedUserIdx, final String recommendCode, final int myUserIdx) {
        userReferralCodeRepository.save(
            new UserReferralCode(receivedUserIdx, recommendCode, myUserIdx)
        );
    }

}
