package com.pood.server.service;

import com.pood.server.entity.meta.CouponBrand;
import com.pood.server.repository.meta.CouponBrandRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("metaTransactionManager")
public class CouponBrandSeparate {

    private final CouponBrandRepository couponBrandRepository;

    public List<CouponBrand> findAll() {
        return couponBrandRepository.findAll();
    }

}
