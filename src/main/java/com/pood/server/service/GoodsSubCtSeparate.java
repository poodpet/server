package com.pood.server.service;

import com.pood.server.entity.meta.GoodsSubCt;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.meta.GoodsSubCtRepository;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GoodsSubCtSeparate {

    private final GoodsSubCtRepository goodsSubCtRepository;

    public GoodsSubCt getGoodsSubCtInfoByIdxAndPcIdx(final long idx, final int pcIdx) {
        return goodsSubCtRepository.findByIdxAndPetCategoryIdx(idx, pcIdx)
            .orElseThrow(() -> new NotFoundException("하위 상품 카테고리가 존재하지 않습니다."));
    }


    public GoodsSubCt getGoodsSubCtInfoByIdxOrNull(final Long idx, final int pcIdx) {
        if (Objects.isNull(idx)) {
            return null;
        }
        return getGoodsSubCtInfoByIdxAndPcIdx(idx, pcIdx);
    }

}
