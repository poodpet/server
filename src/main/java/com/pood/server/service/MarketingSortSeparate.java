package com.pood.server.service;

import com.pood.server.dto.meta.marketing.MarketingDataGroup;
import com.pood.server.dto.meta.promotion.RunningPromotions;
import com.pood.server.repository.meta.MarketingSortRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MarketingSortSeparate {

    private final MarketingSortRepository marketingSortRepository;

    public MarketingDataGroup findAllMarketingData(final int petCategoryIdx) {
        MarketingDataGroup marketingDataGroup = new MarketingDataGroup(marketingSortRepository.findAllEvent(petCategoryIdx));
        marketingDataGroup.addAll(marketingSortRepository.findAllPromotion(petCategoryIdx));
        return marketingDataGroup;
    }

    public RunningPromotions runningPromotions(final int petCategoryId) {
        return new RunningPromotions(marketingSortRepository.runningPromotionAll(petCategoryId));
    }
}
