package com.pood.server.service;

import com.pood.server.entity.meta.AppInitConfig;
import com.pood.server.repository.meta.AppInitConfigRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("metaTransactionManager")
public class AppInitConfigService {

    private final AppInitConfigRepository appInitConfigRepository;

    public List<AppInitConfig> findAll() {
        return appInitConfigRepository.findAll();
    }

}
