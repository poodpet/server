package com.pood.server.service;

import com.pood.server.dto.meta.user.pet.UserPetAllergyGroup;
import com.pood.server.entity.meta.AllergyData;
import com.pood.server.entity.user.UserPetAllergy;
import com.pood.server.repository.user.UserPetAllergyRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("userTransactionManager")
public class UserPetAllergySeparate {

    private final UserPetAllergyRepository userPetAllergyRepository;

    public void insertAllAllergy(final List<AllergyData> allergyList, final int userPetIdx) {
        final UserPetAllergyGroup userPetAllergyGroup = new UserPetAllergyGroup();
        userPetAllergyRepository.saveAll(
            userPetAllergyGroup.saveAllObjects(allergyList, userPetIdx));
    }

    public void deleteAllByUserPetIdx(final int userPetIdx) {
        userPetAllergyRepository.deleteAllByUserPetIdx(userPetIdx);
    }

    public List<UserPetAllergy> getUserPetAllergyList(final int userPetIdx){
        return userPetAllergyRepository.findByUserPetIdx(userPetIdx);
    }

}
