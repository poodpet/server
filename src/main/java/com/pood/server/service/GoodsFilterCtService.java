package com.pood.server.service;

import com.pood.server.dto.meta.goods.GoodsFilterCtForm;
import com.pood.server.entity.meta.GoodsFilterCt;
import com.pood.server.repository.meta.GoodsFilterCtRepository;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GoodsFilterCtService {

    private final GoodsFilterCtRepository goodsFilterCtRepository;

    public List<GoodsFilterCtForm> getGoodsCtList(final int pcIdx) {
        final List<GoodsFilterCt> filterCtList = goodsFilterCtRepository.findAllByPetCtIdxFetchJoin(pcIdx);
        return filterCtList.stream()
            .map(goodsFilterCt -> GoodsFilterCtForm.builder()
                .idx(goodsFilterCt.getIdx())
                .url(goodsFilterCt.getUrl())
                .name(goodsFilterCt.getName())
                .type(goodsFilterCt.getType())
                .fieldKey(goodsFilterCt.getFieldKey())
                .fieldValue(goodsFilterCt.getFieldValue())
                .priority(goodsFilterCt.getPriority())
                .goodsCt(goodsFilterCt.getGoodsCt())
                .build())
            .collect(Collectors.toList());
    }

}
