package com.pood.server.service;

import com.pood.server.entity.meta.Brand;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.meta.BrandRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BrandSeparate {

    private final BrandRepository brandRepository;

    public Brand getBrandInfo(final int idx) {
        return brandRepository.findById(idx)
            .orElseThrow(() -> new NotFoundException("브랜드 정보를 찾을수 없습니다."));
    }

}
