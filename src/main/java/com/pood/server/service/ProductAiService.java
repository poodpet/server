package com.pood.server.service;

import static com.pood.server.exception.ErrorMessage.API_TOKEN;
import static com.pood.server.exception.ErrorMessage.PET;
import static com.pood.server.exception.ErrorMessage.SERVER;
import static com.pood.server.exception.ErrorMessage.USER;

import com.pood.server.dto.meta.goods.GoodsDto;
import com.pood.server.dto.meta.pet.PetDto;
import com.pood.server.dto.meta.product.ProductDto;
import com.pood.server.dto.meta.product.ProductDto.DiscontinuedProductList;
import com.pood.server.entity.PetProduct;
import com.pood.server.entity.ResultCompatibility;
import com.pood.server.entity.ScoreSort;
import com.pood.server.entity.Triple;
import com.pood.server.entity.TripleAndType;
import com.pood.server.entity.UserPetCupInfo;
import com.pood.server.entity.WorryCompatibility;
import com.pood.server.entity.aiSolutions.AafcoNrcEntity;
import com.pood.server.entity.aiSolutions.AafcoNrcEntityCat;
import com.pood.server.entity.aiSolutions.AafcoNrcEntityDog;
import com.pood.server.entity.aiSolutions.ProductUserPetAnalysis;
import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.entity.meta.Feed;
import com.pood.server.entity.meta.GoodsProduct;
import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.meta.PetDoctorFeedDescription;
import com.pood.server.entity.meta.Product;
import com.pood.server.entity.meta.Snack;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserPet;
import com.pood.server.entity.user.UserPetAiDiagnosis;
import com.pood.server.entity.user.UserPetAllergy;
import com.pood.server.entity.user.UserPetGrainSize;
import com.pood.server.exception.CustomStatusException;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.meta.AafcoNrcRepository;
import com.pood.server.repository.meta.AllergyDataRepository;
import com.pood.server.repository.meta.FeedRepository;
import com.pood.server.repository.meta.GoodsProductRepository;
import com.pood.server.repository.meta.PetDoctorFeedDescriptionRepository;
import com.pood.server.repository.meta.PetRepository;
import com.pood.server.repository.meta.ProductRepository;
import com.pood.server.repository.meta.PromotionRepository;
import com.pood.server.repository.meta.SnackRepository;
import com.pood.server.repository.user.UserInfoRepository;
import com.pood.server.repository.user.UserPetAiDiagnosisRepository;
import com.pood.server.repository.user.UserPetAllergyRepository;
import com.pood.server.repository.user.UserPetGrainSizeRepository;
import com.pood.server.repository.user.UserPetRepository;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModelLegacy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ProductAiService {


    private static final int FEED = 0;
    private static final int SNACK = 1;
    private static final int NUTRIENTS = 2;
    private static final int SUPPLIES = 3;
    private static final int DRY_FEED = 0;
    private static final int WET_FEED = 1;

    private static final int COMMON = 0;
    private static final int DOG = 1;
    private static final int CAT = 2;

    private static final int BRAND_ANF = 15;
    private static final int CLOTHING = 30;
    private static final int RECOMMEND_TRUE = 1;


    @Autowired
    AafcoNrcRepository aafcoNrcRepository;

    @Autowired
    UserPetRepository userPetRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    FeedRepository feedRepository;

    @Autowired
    AllergyDataRepository allergyDataRepository;

    @Autowired
    UserPetAllergyRepository userPetAllergyRepository;

    @Autowired
    PetRepository petRepository;

    @Autowired
    UserPetGrainSizeRepository userPetGrainSizeRepository;

    @Autowired
    UserPetAiDiagnosisRepository userPetAiDiagnosisRepository;

    @Autowired
    PetDoctorFeedDescriptionRepository petDoctorFeedDescriptionRepository;

    @Autowired
    UserInfoRepository userInfoRepository;

    @Autowired
    GoodsSeparate goodsSeparate;

    @Autowired
    PromotionRepository promotionRepository;

    @Autowired
    SnackRepository snackRepository;

    @Autowired
    GoodsProductRepository goodsProductRepository;

    //리펙토링 대상
    public List<ProductDto.AiSolutionProduct> getAIGoodsSolution(final UserInfo userInfo,
        final Integer userPetIdx, Integer goodsIdx) {
        if (!userPetRepository.existsByIdxAndUserIdx(userPetIdx, userInfo.getIdx())) {
            throw new NotFoundException("해당하는 유저의 펫이 없습니다.");
        }

        log.info("pet Idx = " + userPetIdx + ", good idx = " + goodsIdx);
        UserPet userPet = userPetRepository.findById(userPetIdx)
            .orElseThrow(() -> CustomStatusException.badRequest(PET));

        List<UserPetGrainSize> userPetGrainSizeList = userPetGrainSizeRepository.findAllByUserPetIdx(
            userPet.getIdx());
        List<UserPetAllergy> userPetAllergyList = userPetAllergyRepository.findByUserPetIdx(
            userPet.getIdx());
        List<ProductDto.ProductDetail> productInfoList = productRepository.getProductListByGoodsIdx(
            goodsIdx);
        List<UserPetAiDiagnosis> userPetAiDiagnosisList = userPetAiDiagnosisRepository.findByUserPetIdx(
            userPet.getIdx());
        HashMap<String, Boolean> petAiDiagnosis = new HashMap<>();
        List<AafcoNrc> aafcoNrcList = aafcoNrcRepository.findAllByPetIdx(userPet.getPcId(),
            Sort.by(Sort.Direction.DESC, "idx"));
        AafcoNrc aafcoNrc = aafcoNrcList.stream()
            .skip(aafcoNrcList.size() - 1L)
            .findFirst()
            .orElseThrow(() -> CustomStatusException.serverError(SERVER));

        List<ProductDto.AiSolutionProduct> aiSolutionProductList = new ArrayList<>();
        for (ProductDto.ProductDetail productInfo : productInfoList) {
            Product product = productRepository.findAllByIdxAndCtIdxIn(productInfo.getIdx(),
                Arrays.asList(FEED, SNACK, NUTRIENTS));
            if (product == null) {
                continue;
            }
            Feed feed = feedRepository.findByProductIdx(productInfo.getIdx());
            PetDoctorFeedDescription petDoctorFeedDescription = petDoctorFeedDescriptionRepository.findByProductIdx(
                productInfo.getIdx());
            Snack snackInfo = snackRepository.findByProductIdx(productInfo.getIdx());

            AafcoNrcEntity aafcoNrcEntity =
                userPet.getPcId().equals(CAT) ? new AafcoNrcEntityCat(
                    product, aafcoNrc, feed, userPet)
                    : new AafcoNrcEntityDog(product, aafcoNrc, feed, userPet);

            List<NutritionDetailModelLegacy> appData = aafcoNrcEntity.getAppData().stream()
                .filter(Objects::nonNull).collect(Collectors.toList());

            product.setPetDoctorFeedDescription(petDoctorFeedDescription);
            product.setFeed(feed);
            userPet.setAafcoNrc(aafcoNrc);

            Pet pet = petRepository.findById(userPet.getPscId())
                .orElseThrow(() -> CustomStatusException.badRequest(PET));

            ResultCompatibility resultCompatibility = new ResultCompatibility();
            List<Pair<Integer, Double>> recommendProductList = resultCompatibility.calc2(userPet,
                List.of(product),
                aafcoNrc,
                userPetAllergyList,
                userPetAiDiagnosisList,
                pet,
                userPetGrainSizeList
            );
            float score = recommendProductList.get(0).getSecond().floatValue();

            GoodsProduct goodsProduct = goodsProductRepository.findByGoodsIdxAndProductIdx(goodsIdx,
                product.getIdx());
            ProductUserPetAnalysis productUserPetAnalysis = new ProductUserPetAnalysis(
                userPet, snackInfo, product, goodsProduct, pet);
            UserPetCupInfo userPetCupInfo = productUserPetAnalysis.getUserPetCupInfo();
            ProductDto.AiSolutionProduct tempDate = new ProductDto.AiSolutionProduct(
                product.getIdx(), score, appData);
            tempDate.setDailyCupAnalysisResult(productUserPetAnalysis.getDailyCupAnalysisResult());
            tempDate.setDailySnackAnalysisResult(
                productUserPetAnalysis.getDailySnackAnalysisResult());
            tempDate.setAllDayAnalysisResult(productUserPetAnalysis.getAllDayAnalysisResult());
            tempDate.setUserPetCupInfo(userPetCupInfo);
            productUserPetAnalysis.setFeedText();
            aiSolutionProductList.add(tempDate);

            WorryCompatibility worryCompatibility = new WorryCompatibility();
            for (UserPetAiDiagnosis index : userPetAiDiagnosisList) {
                String stringPosition = worryCompatibility.changePositionString(
                    index.getAiDiagnosisIdx());
                boolean temp = worryCompatibility.checkWorry(product, stringPosition,
                    product.getPetDoctorFeedDescription());

                petAiDiagnosis.put(stringPosition, temp);
            }
            tempDate.setPetUserWorry(petAiDiagnosis);

            PetProduct petProduct = new PetProduct();
            HashMap<String, Boolean> petAnalysis = petProduct.productAiSolution(userPet,
                product, userPetAllergyList, pet, userPetGrainSizeList);
            tempDate.setPetAnalysis(petAnalysis);

        }

        return aiSolutionProductList;

    }


    public List<GoodsDto.SortedGoodsList> getAiFeedSolution(final UserInfo userInfo,
        final Integer userPetIdx) {
        if (!userPetRepository.existsByIdxAndUserIdx(userPetIdx, userInfo.getIdx())) {
            throw new NotFoundException("해당하는 유저의 펫이 없습니다.");
        }
        UserPet userPet = userPetRepository.findById(userPetIdx)
            .orElseThrow(() -> CustomStatusException.badRequest(PET));
        List<AafcoNrc> aafcoNrcList = aafcoNrcRepository.findAllByPetIdx(userPet.getPcId(),
            Sort.by(Sort.Direction.DESC, "idx"));
        AafcoNrc aafcoNrc = aafcoNrcList.stream()
            .skip(aafcoNrcList.size() - 1L)
            .findFirst()
            .orElseThrow(() -> CustomStatusException.serverError(SERVER));
        List<Integer> discontinuedProductIdxList = exceptZeroStockProductList();

        List<Product> productList = productRepository.getAnalysisProductList(FEED, Boolean.TRUE,
            RECOMMEND_TRUE,
            Arrays.asList(COMMON, userPet.getPcId()), BRAND_ANF, discontinuedProductIdxList);

        List<UserPetAllergy> userPetAllergyList = userPetAllergyRepository.findByUserPetIdx(
            userPet.getIdx());
        Pet pet = petRepository.findById(userPet.getPscId())
            .orElseThrow(() -> CustomStatusException.badRequest(PET));
        List<UserPetGrainSize> userPetGrainSizeList = userPetGrainSizeRepository.findAllByUserPetIdx(
            userPet.getIdx());
        List<UserPetAiDiagnosis> userPetAiDiagnosisList = userPetAiDiagnosisRepository.findByUserPetIdx(
            userPet.getIdx());
        ResultCompatibility resultCompatibility = new ResultCompatibility();
        List<Pair<Integer, Double>> recommendProductList = resultCompatibility.calc(userPet,
            productList,
            aafcoNrc,
            userPetAllergyList,
            userPetAiDiagnosisList,
            pet,
            userPetGrainSizeList
        );

        List<Integer> productIdxList = recommendProductList.stream()
            .sorted(new ScoreSort().reversed())
            .limit(3)
            .map(Pair::getFirst).
            collect(Collectors.toList());
        List<GoodsDto.SortedGoodsList> result = new ArrayList<>();
        for (Integer productIdx : productIdxList) {
            GoodsDto.SortedGoodsList goodsLists = goodsSeparate.getRecommendGoodsList(
                productIdx, userPet.getPcId());
            if (goodsLists != null) {
                result.add(goodsLists);
            }
        }

        return result;
    }

    public PetDto.userPetSolutionDto getMyPetAIHereditySolution(final UserInfo userInfo,
        final Integer userPetIdx) {
        if (!userPetRepository.existsByIdxAndUserIdx(userPetIdx, userInfo.getIdx())) {
            throw new NotFoundException("해당하는 유저의 펫이 없습니다.");
        }
        UserPet userPet = userPetRepository.findById(userPetIdx)
            .orElseThrow(() -> CustomStatusException.badRequest(PET));
        Pet pet = petRepository.findById(userPet.getPscId())
            .orElseThrow(() -> CustomStatusException.badRequest(PET));
        List<UserPetAiDiagnosis> userPetAiDiagnosesr = userPetAiDiagnosisRepository.findByUserPetIdx(
            userPet.getIdx());

        List<TripleAndType> petDiseaseList = new ArrayList<>();
        for (UserPetAiDiagnosis userPetAiDiagnosis : userPetAiDiagnosesr) {
            com.pood.server.entity.aiSolutions2.PetDiseaseSolution petDiseaseSolution = new com.pood.server.entity.aiSolutions2.PetDiseaseSolution();
            TripleAndType petDisease = petDiseaseSolution.create(userPet.getPetName(),
                userPetAiDiagnosis.getArdGroupCode());

            petDiseaseList.add(petDisease);
        }

        com.pood.server.entity.aiSolutions2.PetLifeCycleSolution petLifeCycleSolution = new com.pood.server.entity.aiSolutions2.PetLifeCycleSolution();

        Triple petLifeCycle = petLifeCycleSolution.create(userPet, pet);
        return new PetDto.userPetSolutionDto(petDiseaseList, petLifeCycle);
    }

    public List<GoodsDto.SortedGoodsList> getLifeRecommendDeal(final UserInfo userInfo,
        final Integer userPetIdx, final int size) {
        if (!userPetRepository.existsByIdxAndUserIdx(userPetIdx, userInfo.getIdx())) {
            throw new NotFoundException("해당하는 유저의 펫이 없습니다.");
        }
        UserPet userPet = userPetRepository.findById(userPetIdx)
            .orElseThrow(() -> CustomStatusException.badRequest(PET));
        List<UserPetAllergy> userPetAllergyList = userPetAllergyRepository.findByUserPetIdx(
            userPet.getIdx());
        Pet pet = petRepository.findById(userPet.getPscId())
            .orElseThrow(() -> CustomStatusException.badRequest(PET));
        List<UserPetGrainSize> userPetGrainSizeList = userPetGrainSizeRepository.findAllByUserPetIdx(
            userPet.getIdx());
        List<Integer> discontinuedProductIdxList = exceptZeroStockProductList();

        List<Product> productList = productRepository.getAnalysisProductList(FEED, Boolean.TRUE,
            RECOMMEND_TRUE,
            Arrays.asList(COMMON, userPet.getPcId()), BRAND_ANF, discontinuedProductIdxList);

        ArrayList<Pair<Integer, Double>> scoreDataList = new ArrayList<>();
        for (Product product : productList) {
            double totalPoint = new PetProduct().lifeRecommendDealCalc(userPet, product,
                userPetAllergyList, pet, userPetGrainSizeList);
            scoreDataList.add(Pair.of(product.getIdx(), totalPoint));
        }
        List<Integer> productIdxList = scoreDataList.stream()
            .sorted(new ScoreSort().reversed())
            .limit(size).map(Pair::getFirst)
            .collect(Collectors.toList());
        Collections.shuffle(productIdxList);

        productIdxList = productIdxList.stream().limit(size).collect(Collectors.toList());

        List<GoodsDto.SortedGoodsList> result = new ArrayList<>();
        for (Integer productIdx : productIdxList) {
            GoodsDto.SortedGoodsList goodsLists = goodsSeparate.getRecommendGoodsList(
                productIdx, userPet.getPcId());
            if (goodsLists != null) {
                result.add(goodsLists);
                break;
            }
        }
        return result;
    }

    private UserPet userValiedCheck(final String token, final Integer userPetIdx) {
        Optional.ofNullable(token).orElseThrow(() -> CustomStatusException.unAuthorized(API_TOKEN));
        UserInfo userInfo = userInfoRepository.getUserByToken(token);
        Optional.ofNullable(userInfo).orElseThrow(() -> CustomStatusException.badRequest(USER));

        if (!userPetRepository.existsByIdxAndUserIdx(userPetIdx, userInfo.getIdx())) {
            throw new NotFoundException("해당하는 유저의 펫이 없습니다.");
        }
        return userPetRepository.findById(userPetIdx)
            .orElseThrow(() -> CustomStatusException.badRequest(PET));
    }

    public List<GoodsDto.SortedGoodsList> getWorryRecommendDeal(final UserInfo userInfo,
        final Integer userPetIdx) {
        if (!userPetRepository.existsByIdxAndUserIdx(userPetIdx, userInfo.getIdx())) {
            throw new NotFoundException("해당하는 유저의 펫이 없습니다.");
        }
        UserPet userPet = userPetRepository.findById(userPetIdx)
            .orElseThrow(() -> CustomStatusException.badRequest(PET));
        List<UserPetAllergy> userPetAllergyList = userPetAllergyRepository.findByUserPetIdx(
            userPet.getIdx());
        List<Integer> discontinuedProductIdxList = exceptZeroStockProductList();

        List<Product> productAllList = productRepository.findAllByCtIdxAndApproveAndIsRecommendAndPcIdxInAndBrandIdxNotAndIdxNotIn(
            FEED, Boolean.TRUE, RECOMMEND_TRUE, Arrays.asList(COMMON, userPet.getPcId()), BRAND_ANF,
            discontinuedProductIdxList);
        List<Feed> feedList = feedRepository.findAllByProductIdxIn(
            productAllList.stream().map(Product::getIdx).collect(Collectors.toList()));
        List<PetDoctorFeedDescription> petDoctorFeedDescriptionList = petDoctorFeedDescriptionRepository.findAllByProductIdxIn(
            productAllList.stream().map(Product::getIdx).collect(Collectors.toList()));

        List<UserPetAiDiagnosis> userPetAiDiagnosesr = userPetAiDiagnosisRepository.findByUserPetIdx(
            userPet.getIdx());
        productAllList = productAllList.stream()
            .map(getProductFeedInfoSettingFunction(feedList, petDoctorFeedDescriptionList))
            .collect(Collectors.toList());
        List<Product> productList = new ArrayList<>();
        for (UserPetAiDiagnosis userPetAiDiagnosis : userPetAiDiagnosesr) {
            productList.addAll(diagnosisFilter(userPet, productAllList, userPetAiDiagnosis));
        }

        List<Integer> productIdxList = productList.stream().limit(10).map(Product::getIdx)
            .collect(Collectors.toList());
        List<GoodsDto.SortedGoodsList> result = new ArrayList<>();
        for (Integer productIdx : productIdxList) {
            GoodsDto.SortedGoodsList goodsLists = goodsSeparate.getRecommendGoodsList(
                productIdx, userPet.getPcId());
            if (goodsLists != null) {
                result.add(goodsLists);
            }
        }
        return result;
    }

    private List<Product> diagnosisFilter(final UserPet userPet, final List<Product> productList,
        final UserPetAiDiagnosis userPetAiDiagnosis) {
        return productList.stream().filter(product -> {
            if (product.getPetDoctorFeedDescription() == null) {
                return Boolean.FALSE;
            }

            if (userPetAiDiagnosis.getArdName().equals("심장")) {
                if (product.getPetDoctorFeedDescription().getPosition1().equals("활력증진")
                    || product.getPetDoctorFeedDescription().getPosition2().equals("활력증진")
                    || product.getPetDoctorFeedDescription().getPosition3().equals("활력증진")) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
            if (userPetAiDiagnosis.getArdName().equals("눈")) {
                if (product.getPetDoctorFeedDescription().getPosition1().equals("눈건강")
                    || product.getPetDoctorFeedDescription().getPosition2().equals("눈건강")
                    || product.getPetDoctorFeedDescription().getPosition3().equals("눈건강")) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
            if (userPetAiDiagnosis.getArdName().equals("치아")) {
                if (product.getPetDoctorFeedDescription().getPosition1().equals("치아건강")
                    || product.getPetDoctorFeedDescription().getPosition2().equals("치아건강")
                    || product.getPetDoctorFeedDescription().getPosition3().equals("치아건강")) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }
            if (userPetAiDiagnosis.getArdName().equals("신장")) {
                if (product.getCtSubIdx().equals(DRY_FEED)) {  // 건사료
                    if (userPet.getPcId().equals(DOG)) {
                        if (product.getFeed().getPrProtein() < 38) {
                            return Boolean.TRUE;
                        } else {
                            return Boolean.FALSE;
                        }
                    } else {
                        if (product.getFeed().getPrProtein() < 30) {
                            return Boolean.TRUE;
                        } else {
                            return Boolean.FALSE;
                        }
                    }
                } else if (product.getCtSubIdx().equals(WET_FEED)) {  // 습식
                    if (userPet.getPcId().equals(DOG)) {
                        if (product.getFeed().getPrFat() < 7) {
                            return Boolean.TRUE;
                        } else {
                            return Boolean.FALSE;
                        }
                    } else {
                        if (product.getFeed().getPrProtein() < 11) {
                            return Boolean.TRUE;
                        } else {
                            return Boolean.FALSE;
                        }
                    }
                }
            }
            if (userPetAiDiagnosis.getArdName().equals("췌장")) {
                if (product.getCtSubIdx().equals(DRY_FEED)) {  // 건사료
                    if (product.getFeed().getPrFat() < 13) {
                        return Boolean.TRUE;
                    } else {
                        return Boolean.FALSE;
                    }
                }
            } else {
                if (product.getPetDoctorFeedDescription().getPosition1()
                    .equals(userPetAiDiagnosis.getArdName())
                    || product.getPetDoctorFeedDescription()
                    .getPosition2().equals(userPetAiDiagnosis.getArdName())
                    || product.getPetDoctorFeedDescription().getPosition3()
                    .equals(userPetAiDiagnosis.getArdName())) {
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            }

            return Boolean.FALSE;
        }).collect(Collectors.toList());
    }

    public List<GoodsDto.SortedGoodsList> getAiSnack(final UserInfo userInfo,
        final Integer userPetIdx, final int size) {
        if (!userPetRepository.existsByIdxAndUserIdx(userPetIdx, userInfo.getIdx())) {
            throw new NotFoundException("해당하는 유저의 펫이 없습니다.");
        }
        UserPet userPet = userPetRepository.findById(userPetIdx)
            .orElseThrow(() -> CustomStatusException.badRequest(PET));
        List<UserPetAllergy> userPetAllergyList = userPetAllergyRepository.findByUserPetIdx(
            userPet.getIdx());
        List<Integer> discontinuedProductIdxList = exceptZeroStockProductList();

        List<Product> productList = productRepository.getAnalysisProductList(SNACK, Boolean.TRUE,
            RECOMMEND_TRUE,
            Arrays.asList(COMMON, userPet.getPcId()), BRAND_ANF, discontinuedProductIdxList);

        productList = productList
            .stream()
            .filter(
                product -> filterAllergy(userPet, userPetAllergyList, new PetProduct(),
                    product))
            .collect(Collectors.toList());

        Collections.shuffle(productList);
        List<Integer> productIdxList = productList.stream()
            .map(Product::getIdx)
            .limit(size)
            .collect(Collectors.toList());
        List<GoodsDto.SortedGoodsList> result = new ArrayList<>();
        for (Integer productIdx : productIdxList) {
            GoodsDto.SortedGoodsList goodsLists = goodsSeparate.getRecommendGoodsList(
                productIdx, userPet.getPcId());
            if (goodsLists != null) {
                result.add(goodsLists);
            }
        }
        return result;

    }

    public List<GoodsDto.SortedGoodsList> getAiNutrients(final UserInfo userInfo,
        final Integer userPetIdx,
        final int size) {
        if (!userPetRepository.existsByIdxAndUserIdx(userPetIdx, userInfo.getIdx())) {
            throw new NotFoundException("해당하는 유저의 펫이 없습니다.");
        }
        UserPet userPet = userPetRepository.findById(userPetIdx)
            .orElseThrow(() -> CustomStatusException.badRequest(PET));
        List<Integer> discontinuedProductIdxList = exceptZeroStockProductList();

        List<Product> productAllList = productRepository.getAnalysisProductList(NUTRIENTS,
            Boolean.TRUE, RECOMMEND_TRUE,
            Arrays.asList(COMMON, userPet.getPcId()), BRAND_ANF, discontinuedProductIdxList);
        List<UserPetAiDiagnosis> userPetAiDiagnosesr = userPetAiDiagnosisRepository.findByUserPetIdx(
            userPet.getIdx());
        List<Product> productList = new ArrayList<>();
        for (UserPetAiDiagnosis userPetAiDiagnosis : userPetAiDiagnosesr) {
            productList.addAll(diagnosisFilter(userPet, productAllList, userPetAiDiagnosis));
        }

        Collections.shuffle(productList);

        List<Integer> productIdxList = productList.stream()
            .map(Product::getIdx)
            .limit(size)
            .collect(Collectors.toList());

        List<GoodsDto.SortedGoodsList> result = new ArrayList<>();
        for (Integer productIdx : productIdxList) {
            GoodsDto.SortedGoodsList goodsLists = goodsSeparate.getRecommendGoodsList(
                productIdx, userPet.getPcId());
            if (goodsLists != null) {
                result.add(goodsLists);
            }
        }
        return result;
    }

    public List<GoodsDto.SortedGoodsList> getAiSupplies(final UserInfo userInfo,
        final Integer userPetIdx) {
        if (!userPetRepository.existsByIdxAndUserIdx(userPetIdx, userInfo.getIdx())) {
            throw new NotFoundException("해당하는 유저의 펫이 없습니다.");
        }
        UserPet userPet = userPetRepository.findById(userPetIdx)
            .orElseThrow(() -> CustomStatusException.badRequest(PET));

        List<Integer> discontinuedProductIdxList = exceptZeroStockProductList();
        List<Product> productList = productRepository.findAllByCtIdxAndCtSubIdxNotAndApproveAndIsRecommendAndPcIdxInAndBrandIdxNotAndIdxNotIn(
            SUPPLIES, CLOTHING, Boolean.TRUE, RECOMMEND_TRUE,
            Arrays.asList(COMMON, userPet.getPcId()),
            BRAND_ANF,
            discontinuedProductIdxList);

        Collections.shuffle(productList);
        List<Integer> productIdxList = productList.stream().map(Product::getIdx).limit(5)
            .collect(Collectors.toList());
        List<GoodsDto.SortedGoodsList> result = new ArrayList<>();
        for (Integer productIdx : productIdxList) {
            GoodsDto.SortedGoodsList goodsLists = goodsSeparate.getRecommendGoodsList(
                productIdx, userPet.getPcId());
            if (goodsLists != null) {
                result.add(goodsLists);
            }
        }
        return result;
    }

    public List<GoodsDto.SortedGoodsList> getAiRandomGoods(final Integer ctIdx, final Integer pcIdx,
        final int size) {
        List<Integer> discontinuedProductIdxList = exceptZeroStockProductList();
        List<Product> productList = productRepository.getAnalysisProductList(ctIdx, Boolean.TRUE,
            RECOMMEND_TRUE,
            Arrays.asList(COMMON, pcIdx), BRAND_ANF, discontinuedProductIdxList);
        Collections.shuffle(productList);
        List<Integer> productIdxList = productList.stream().map(Product::getIdx).limit(size)
            .collect(Collectors.toList());
        List<GoodsDto.SortedGoodsList> result = new ArrayList<>();
        for (Integer productIdx : productIdxList) {
            GoodsDto.SortedGoodsList goodsLists = goodsSeparate.getRecommendGoodsList(
                productIdx, pcIdx);
            if (goodsLists != null) {
                result.add(goodsLists);
            }
        }
        return result;
    }

    public List<GoodsDto.SortedGoodsList> getAiRandomSuppliesGoods(final Integer ctIdx,
        final Integer pcIdx, final Integer size) {
        List<Integer> discontinuedProductIdxList = exceptZeroStockProductList();
        List<Product> productList = productRepository.getAnalysisSuppliesProductList(ctIdx,
            CLOTHING,
            Boolean.TRUE, RECOMMEND_TRUE, Arrays.asList(COMMON, pcIdx), BRAND_ANF,
            discontinuedProductIdxList);
        Collections.shuffle(productList);
        List<Integer> productIdxList = productList.stream().map(Product::getIdx).limit(size)
            .collect(Collectors.toList());
        List<GoodsDto.SortedGoodsList> result = new ArrayList<>();
        for (Integer productIdx : productIdxList) {
            GoodsDto.SortedGoodsList goodsLists = goodsSeparate.getRecommendGoodsList(
                productIdx, pcIdx);
            if (goodsLists != null) {
                result.add(goodsLists);
            }
        }
        return result;
    }

    public List<GoodsDto.SortedGoodsList> getWorryRecommendNutrientsDeal(final UserInfo userInfo,
        final Integer userPetIdx) {

        if (!userPetRepository.existsByIdxAndUserIdx(userPetIdx, userInfo.getIdx())) {
            throw new NotFoundException("해당하는 유저의 펫이 없습니다.");
        }
        UserPet userPet = userPetRepository.findById(userPetIdx)
            .orElseThrow(() -> CustomStatusException.badRequest(PET));

        List<UserPetAllergy> userPetAllergyList = userPetAllergyRepository.findByUserPetIdx(
            userPet.getIdx());

        List<Integer> discontinuedProductIdxList = exceptZeroStockProductList();

        List<Product> productAllList = productRepository.findAllByCtIdxAndApproveAndIsRecommendAndPcIdxInAndBrandIdxNotAndIdxNotIn(
            NUTRIENTS, Boolean.TRUE, RECOMMEND_TRUE, Arrays.asList(COMMON, userPet.getPcId()),
            BRAND_ANF,
            discontinuedProductIdxList);

        List<Feed> feedList = feedRepository.findAllByProductIdxIn(productAllList.stream()
            .map(Product::getIdx)
            .collect(Collectors.toList()));
        PetProduct petProduct = new PetProduct();

        List<PetDoctorFeedDescription> petDoctorFeedDescriptionList = petDoctorFeedDescriptionRepository.findAllByProductIdxIn(
            productAllList.stream().map(Product::getIdx).collect(Collectors.toList()));

        List<UserPetAiDiagnosis> userPetAiDiagnosesr = userPetAiDiagnosisRepository.findByUserPetIdx(
            userPet.getIdx());
        productAllList = productAllList.stream()
            .filter(product -> filterAllergy(userPet, userPetAllergyList, petProduct,
                product))
            .map(getProductFeedInfoSettingFunction(feedList, petDoctorFeedDescriptionList))
            .collect(Collectors.toList());

        List<Product> productList = new ArrayList<>();
        for (UserPetAiDiagnosis userPetAiDiagnosis : userPetAiDiagnosesr) {
            productList.addAll(diagnosisFilter(userPet, productAllList, userPetAiDiagnosis));
        }
        Collections.shuffle(productList);
        List<Integer> productIdxList = productList.stream().limit(16).map(Product::getIdx)
            .collect(Collectors.toList());
        List<GoodsDto.SortedGoodsList> result = new ArrayList<>();
        for (Integer productIdx : productIdxList) {
            GoodsDto.SortedGoodsList goodsLists = goodsSeparate.getRecommendGoodsList(
                productIdx, userPet.getPcId());
            if (goodsLists != null) {
                result.add(goodsLists);
            }
        }
        return result;
    }

    public List<GoodsDto.SortedGoodsList> getPoodHomeRecommendNutrientsDeal(final UserInfo userInfo,
        final Integer userPetIdx) {

        if (!userPetRepository.existsByIdxAndUserIdx(userPetIdx, userInfo.getIdx())) {
            throw new NotFoundException("해당하는 유저의 펫이 없습니다.");
        }
        UserPet userPet = userPetRepository.findById(userPetIdx)
            .orElseThrow(() -> CustomStatusException.badRequest(PET));

        Pet pet = petRepository.findById(userPet.getPscId())
            .orElseThrow(() -> CustomStatusException.badRequest(PET));

        if (pet.getPcId().equals(CAT)) {
            return getLifeRecommendDeal(userInfo, userPetIdx, 16);
        }

        List<Integer> discontinuedProductIdxList = exceptZeroStockProductList();
        List<Product> productList = productRepository.findAllByCtIdxAndApproveAndIsRecommendAndPcIdxInAndBrandIdxNotAndIdxNotInAndFeedTargetIn(
            NUTRIENTS, Boolean.TRUE, RECOMMEND_TRUE, Arrays.asList(COMMON, userPet.getPcId()),
            BRAND_ANF,
            discontinuedProductIdxList,
            List.of(COMMON, pet.getPcSizeCode()));
        Collections.shuffle(productList);
        List<Integer> productIdxList = productList.stream()
            .limit(16)
            .map(Product::getIdx)
            .collect(Collectors.toList());
        List<GoodsDto.SortedGoodsList> result = new ArrayList<>();
        for (Integer productIdx : productIdxList) {
            GoodsDto.SortedGoodsList goodsLists = goodsSeparate.getRecommendGoodsList(
                productIdx, userPet.getPcId());
            if (goodsLists != null) {
                result.add(goodsLists);
            }
        }
        return result;
    }

    private boolean filterAllergy(final UserPet userPet,
        final List<UserPetAllergy> userPetAllergyList,
        final PetProduct petProduct, final Product product) {
        double point = petProduct.allergyRecommendDealCalc(userPet, product,
            userPetAllergyList);
        return point > 0;
    }

    private List<Integer> exceptZeroStockProductList() {
        return productRepository.getGoodsOfProductCountListAndPoodMarketNotIn()
            .stream()
            .filter(DiscontinuedProductList::isEmptyProduct)
            .map(DiscontinuedProductList::getIdx)
            .collect(Collectors.toList());
    }

    private Function<Product, Product> getProductFeedInfoSettingFunction(List<Feed> feedList,
        List<PetDoctorFeedDescription> petDoctorFeedDescriptionList) {
        return product -> {
            for (Feed feed : feedList) {
                if (feed.getProductIdx().equals(product.getIdx())) {
                    product.setFeed(feed);
                    break;
                }
            }
            for (PetDoctorFeedDescription petDoctorFeedDescription : petDoctorFeedDescriptionList) {
                if (petDoctorFeedDescription.getProductIdx().equals(product.getIdx())) {
                    product.setPetDoctorFeedDescription(petDoctorFeedDescription);
                    break;
                }
            }
            return product;
        };
    }

}
