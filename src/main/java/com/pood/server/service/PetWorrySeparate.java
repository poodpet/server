package com.pood.server.service;

import com.pood.server.dto.meta.pet.PetWorryGroup;
import com.pood.server.entity.meta.PetWorry;
import com.pood.server.entity.meta.mapper.AiRecommendDiagnosisMapper;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.meta.PetWorryRepository;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional("metaTransactionManager")
public class PetWorrySeparate {

    static final int COMMON = 0;

    private final PetWorryRepository petWorryRepository;

    public List<PetWorry> findWorryByPetCategoryIdx(final int petCategoryIdx) {
        return petWorryRepository.findAllByPetCategoryIdxInOrderByPriorityAsc(
            List.of(COMMON, petCategoryIdx));
    }

    public PetWorryGroup getFitSolutionWorryList(final int petCategoryIdx) {
        return new PetWorryGroup(
            petWorryRepository.getAllByPetCategoryOrderByPriority(petCategoryIdx));
    }

    public List<PetWorry> findAllByIdxIn(final List<Long> petWorryList) {
        return petWorryRepository.findAllByIdIn(petWorryList);
    }

    public AiRecommendDiagnosisMapper findDiagnosisByWorryIdx(final Long worryIdx) {
        if (Objects.isNull(worryIdx)) {
            return AiRecommendDiagnosisMapper.ofEmpty();
        }
        return petWorryRepository.getDiagnosisInfoByWorryIdx(worryIdx);
    }

    public List<Integer> getAiRecommendDiagnosisIdxList(final List<Long> petWorryIdxList) {
        return petWorryRepository.findAllByIdIn(petWorryIdxList).stream()
            .map(petWorry -> petWorry.getAiRecommendDiagnosis().getIdx())
            .distinct()
            .collect(Collectors.toList());
    }

    public List<Long> findAllIdxInDiagnosisIdx(final List<Integer> aiRecommendDiagnosisIdx) {
        return petWorryRepository.findAllByAiRecommendDiagnosisIdxIn(aiRecommendDiagnosisIdx)
            .orElseThrow(
                () -> new NotFoundException("ai 진단 idx에 해당하는 고민이 존재하지 않습니다.")
            )
            .stream().map(PetWorry::getId)
            .collect(Collectors.toList());
    }

}
