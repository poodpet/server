package com.pood.server.service;

import com.pood.server.entity.user.UserGoodsRequst;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.exception.AlreadyExistException;
import com.pood.server.repository.user.UserGoodsRequstRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserGoodsRequstSeparate {

    private final UserGoodsRequstRepository userGoodsRequstRepository;

    public void save(final UserInfo userInfo, final Integer goodsIdx) {
        final boolean isExists = userGoodsRequstRepository.existsByUserInfoIdxAndGoodsIdx(
            userInfo.getIdx(), goodsIdx);
        if (isExists) {
            throw new AlreadyExistException("이미 요청하신 상품 입니다.");
        }
        userGoodsRequstRepository.save(UserGoodsRequst.create(userInfo, goodsIdx));
    }
}
