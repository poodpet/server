package com.pood.server.service;

import com.pood.server.dto.meta.goods.PromotionDto;
import com.pood.server.dto.meta.goods.PromotionDto.PromotionImage;
import com.pood.server.repository.meta.PromotionImageRepository;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PromotionImageSeparate {

    private final PromotionImageRepository promotionImageRepository;

    public List<PromotionImage> getPromoitionImagesByPromotionIdx(final Integer idx) {
        return promotionImageRepository.findAllByPrIdx(idx)
            .stream()
            .map(
                promotionImage -> new PromotionDto.PromotionImage(promotionImage.getIdx(),
                    promotionImage.getUrl(),promotionImage.getType()))
            .collect(Collectors.toList());
    }
}