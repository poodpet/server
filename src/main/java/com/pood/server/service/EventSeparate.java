package com.pood.server.service;

import com.pood.server.dto.meta.event.EndEvent;
import com.pood.server.dto.meta.event.EndPhotoAward;
import com.pood.server.dto.meta.event.EndPhotoImgUrl;
import com.pood.server.dto.meta.event.EventDto.EventDetail;
import com.pood.server.dto.meta.event.RunningPhotoAward;
import com.pood.server.entity.meta.EventInfo;
import com.pood.server.entity.meta.mapper.event.EventDonationMapper;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.meta.EventRepository;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EventSeparate {

    private static final int PHOTO_AWARDS = 31;
    public static final int EVENT_PAUSED = 3;
    public static final int DELETED = 0;

    private final EventRepository eventRepository;

    public EventDetail getEventDetail(final int idx) {
        final EventDetail eventDetail = eventRepository.getEventDetail(idx);

        if (Objects.isNull(eventDetail)) {
            throw new NotFoundException("해당하는 이벤트가 없습니다.");
        }

        return eventDetail;
    }

    public Page<EndEvent> findEndEvent(final Pageable pageable, final int pcIdx) {
        return eventRepository.findEndEventOneYearAgoToNow(pageable, pcIdx, LocalDateTime.now());
    }

    public List<EndPhotoAward> findEndPhotoAwardWinners(final int year, final List<Integer> exceptUserIdxList) {
        final LocalDateTime startDate = LocalDateTime.of(year, 1, 1, 0, 0, 0);
        final LocalDateTime yearLast = LocalDateTime.of(year, 12, 31, 23, 59, 59);

        return eventRepository.findPhotoAwardWinners(startDate, yearLast, year, exceptUserIdxList);
    }

    public List<EndPhotoAward> findEndPhotoAwards(final int year) {
        final LocalDateTime firstDayOfThisYear = LocalDateTime.of(year, 1, 1, 0, 0, 0);
        final LocalDateTime lastDayThisYear = LocalDateTime.of(year, 12, 31, 23, 59, 59);
        return eventRepository.findEndPhotoAwards(PHOTO_AWARDS, firstDayOfThisYear,
            lastDayThisYear, year);
    }

    public int countAwards(final int eventIdx) {
        return eventRepository.countAllByEventTypeIdxAndStatusNotAndStatusNotAndIdxLessThanEqual(
            PHOTO_AWARDS, EVENT_PAUSED,
            DELETED, eventIdx);
    }

    public EventInfo findEndPhotoAward(final int eventIdx) {
        return eventRepository.findById(eventIdx)
            .orElseThrow(() -> new NotFoundException("해당하는 이벤트 포토 어워즈가 존재하지 않습니다."));
    }

    public EndPhotoImgUrl findPhotoAwardWinner(final int eventIdx,
        final List<Integer> notInUserIdxList) {
        return eventRepository.findPhotoAwardsWinnerImage(eventIdx, notInUserIdxList);
    }

    public Page<EndPhotoImgUrl> findPhotoAwardAllImage(final int eventIdx,
        final List<Integer> notInUserIdxList, final Pageable pageable) {
        return eventRepository.findPhotoAwardAllImage(eventIdx, notInUserIdxList, pageable);
    }

    public RunningPhotoAward runningPhotoAward() {
        return eventRepository.runningPhotoAward();
    }

    public EventDonationMapper findDonationDetail(final Integer idx) {

        return eventRepository.findDonationEventByIdx(idx).orElseThrow(
            () -> new NotFoundException("해당하는 이벤트가 존재하지 않거나, 기부 이벤트 타입이 아닙니다.")
        );
    }

    public EventInfo findByIdx(final Integer idx){

        Optional<EventInfo> eventInfo = eventRepository.findById(idx);

        if (eventInfo.isEmpty()) {
            throw new NotFoundException("해당하는 이벤트가 없습니다.");
        }

        return eventInfo.get();
    }

}
