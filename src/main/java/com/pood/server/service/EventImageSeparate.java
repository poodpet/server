package com.pood.server.service;

import com.pood.server.entity.meta.EventImage;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.meta.EventImageRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class EventImageSeparate {

    private static final int MAIN_IMAGE_TYPE = 0;
    private final EventImageRepository eventImageRepository;

    public List<EventImage> getEventImagesByEventIdx(final int eventIdx) {
        return eventImageRepository.findAllByEventIdx(eventIdx);
    }

    public EventImage findMainImage(final int eventIdx) {
        return eventImageRepository.findByEventIdxAndType(eventIdx, MAIN_IMAGE_TYPE)
            .orElseThrow(() -> new NotFoundException("해당하는 이벤트의 메인 이미지가 없습니다."));
    }
}
