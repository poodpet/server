package com.pood.server.service.aligo;

import com.pood.server.config.AES256;
import com.pood.server.entity.meta.dto.OrderInfoDto;
import com.pood.server.facade.aligo.AligoDefalutResponse;
import com.pood.server.facade.aligo.CreateTokenResponse;
import com.pood.server.facade.aligo.DefalutResponse;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

@Component
public class KakaoApi {

    @Value("${aligo.apikey}")
    private String apiKey;

    @Value("${aligo.senderkey}")
    private String senderKey;

    private static final String USER_ID = "pood";
    private static final String URL = "https://kakaoapi.aligo.in";

    private static final WebClient WEB_CLIENT = WebClient.builder()
        .baseUrl(URL)
        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
        .build();


    private String createToken() {
        final CreateTokenResponse tokenResponse = WEB_CLIENT.post()
            .uri(uriBuilder -> uriBuilder
                .path("/akv10/token/create/30/s/")
                .queryParam("apikey", AES256.getAesDecodeOrNull(apiKey))
                .queryParam("userid", USER_ID)
                .build())
            .retrieve()
            .bodyToMono(CreateTokenResponse.class)
            .timeout(Duration.ofSeconds(10000))
            .block();

        aligoResponseExceptionCheck(tokenResponse);

        return tokenResponse.getUrlencode();
    }

    public void orderCompletedSend(final OrderInfoDto orderInfo) {

        MultiValueMap<String, String> formData = getKakaoTokSendformData(createToken(),
            orderInfo.getUserName(), orderInfo.getDate(),
            orderInfo.getOrderNumber(), orderInfo.getOrderName(), orderInfo.getPrice(),
            orderInfo.getPhoneNumber());

        DefalutResponse response = WEB_CLIENT.post()
            .uri("/akv10/alimtalk/send/")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .body(BodyInserters.fromFormData(formData))
            .retrieve()
            .bodyToMono(DefalutResponse.class)
            .timeout(Duration.ofSeconds(10000))
            .block();

        aligoResponseExceptionCheck(response);

    }

    private void aligoResponseExceptionCheck(final AligoDefalutResponse response) {
        if (Objects.isNull(response)) {
            throw new NullPointerException("aligo token Response Is null");
        }
        if (response.isFail()) {
            throw new IllegalArgumentException(response.getMessage());
        }
    }

    private MultiValueMap<String, String> getKakaoTokSendformData(
        final String token, final String name, final String date,
        final String orderNumber, final String goodsName, final String price,
        final String phoneNumber) {
        final JSONObject jsonObj = new JSONObject();
        jsonObj.put("button", List.of(Map.of(
            "name", "주문 내역",
            "linkType", "AL",
            "linkTypeName", "앱링크",
            "linkIos", "https://pood.page.link/WFVp",
            "linkAnd", "https://pood.page.link/WFVp")));
        final MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
        final String fMessage = toText(name, date, orderNumber, goodsName, price);

        formData.add("apikey", AES256.getAesDecodeOrNull(apiKey));
        formData.add("userid", USER_ID);
        formData.add("token", token);
        formData.add("senderkey", AES256.getAesDecodeOrNull(senderKey));
        formData.add("tpl_code", "TK_4593");
        formData.add("sender", "15333299");
        formData.add("receiver_1", phoneNumber);
        formData.add("subject_1", "주문완료_2");
        formData.add("message_1", fMessage);
        formData.add("button_1", jsonObj.toJSONString());
        formData.add("failover", "Y");
        formData.add("fsubject_1", "주문완료");
        formData.add("fmessage_1", fMessage);

        return formData;
    }

    private String toText(final String... text) {
        if (text.length != 5) {
            throw new IllegalArgumentException();
        }
        StringBuilder stringBuilder = new StringBuilder("[푸드]")
            .append("\n").append("안녕하세요. ").append(text[0]).append("님 :)")
            .append("\n\n").append("결제완료가 되었어요. 빠르게 배송해드릴게요!")
            .append("\n\n").append("- 날짜 : ").append(text[1])
            .append("\n").append("- 주문번호 : ").append(text[2])
            .append("\n").append("- 상품명 : ").append(text[3])
            .append("\n").append("- 결제금액 : ").append(text[4]).append("원")
            .append("\n\n").append("*위탁 상품의 경우는 해당 출고지에서 개별 출고")
            .append("\n\n").append("푸드 고객센터")
            .append("\n").append("(1533-3299)");
        return stringBuilder.toString();
    }

}
