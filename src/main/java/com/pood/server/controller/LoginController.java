package com.pood.server.controller;

import com.pood.server.config.AES256;
import com.pood.server.controller.request.user.UserSignUpRequest;
import com.pood.server.controller.request.userinfo.UserLoginRequest;
import com.pood.server.controller.request.userinfo.UserLoginResponseDto;
import com.pood.server.facade.UserFacade;
import com.pood.server.facade.user.info.UserInfoSignUpResponse;
import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pood/v1-0/login")
public class LoginController {

    private final UserFacade userFacade;

    @PostMapping
    @ApiOperation(value = "유저 로그인 갱신 또는 신규")
    public ResponseEntity<UserLoginResponseDto> userLogin(
        @RequestBody @Valid UserLoginRequest userLoginRequest) {
        return ResponseEntity.ok(userFacade.login(userLoginRequest));
    }

    @GetMapping("/sms/auth-check")
    public ResponseEntity<Object> authNumberCheck(
        @RequestParam("phoneNumber") final String phoneNumber,
        @RequestParam("authNumber") final String authNumber) {

        userFacade.checkAuthentication(phoneNumber, authNumber);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/password")
    @ApiOperation(value = "비밀번호 찾기 알리고 문자전송 실행")
    public ResponseEntity<Object> findPassword(@RequestParam("userEmail") final String userEmail,
        @RequestParam("phoneNumber") final String phoneNumber,
        @RequestParam("userName") final String userName) throws Exception {

        final String decodedUserName = AES256.aesDecode(userName);
        userFacade.findPassword(userEmail, phoneNumber, decodedUserName);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/sign-up")
    @ApiOperation(value = "유저의 회원가입")
    public ResponseEntity<UserInfoSignUpResponse> emailSignUp(@RequestBody @Valid final UserSignUpRequest request)
        throws Exception {
        return ResponseEntity.ok().body(userFacade.emailSignUp(request));
    }

}
