package com.pood.server.controller.faq;

import com.pood.server.controller.response.faq.FaqCategoryResponse;
import com.pood.server.controller.response.faq.FaqResponse;
import com.pood.server.facade.faq.FaqFacade;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pood/v1-0/faq")
public class FaqController {

    private final FaqFacade faqFacade;

    @GetMapping("/category")
    @ApiOperation(value = "FAQ 카테고리 리스트 출력", notes = "FAQ 카테고리 리스트 조회합니다.")
    public ResponseEntity<List<FaqCategoryResponse>> getFaqCategoryList() {
        return ResponseEntity.ok(faqFacade.getFeqCatagoryList());
    }

    @GetMapping("/{categoryIdx}")
    @ApiOperation(value = "FAQ 질문 리스트 출력", notes = "FAQ 질문 리스트 조회합니다.")
    public ResponseEntity<List<FaqResponse>> getFaqList(@PathVariable final long categoryIdx) {
        return ResponseEntity.ok(faqFacade.getFaqListByCategoryIdx(categoryIdx));
    }

}
