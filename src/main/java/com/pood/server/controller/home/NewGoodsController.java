package com.pood.server.controller.home;

import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.facade.goods.GoodsFacade;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pood/v1-0/home")
public class NewGoodsController {

    private final GoodsFacade goodsFacade;

    @GetMapping("/new/goods/{pcIdx}")
    @ApiOperation(value = "푸드홈 기획전/이벤트 캐러셀 조회")
    public ResponseEntity<Page<SortedGoodsList>> findNewGoodsList(
        @PathVariable final Integer pcIdx,
        @PageableDefault(sort = "idx", direction = Sort.Direction.DESC) final Pageable pageable) {
        return ResponseEntity.ok(goodsFacade.findNewGoodsList(pcIdx, pageable));
    }

}
