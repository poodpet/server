package com.pood.server.controller.home.rank;

import com.pood.server.controller.response.home.rank.PoodRankCategoryDto;
import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.facade.home.PoodHomeRankFacade;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/pood/v1-0/home")
@RequiredArgsConstructor
public class HomeRankController {

    private final PoodHomeRankFacade poodHomeRankFacade;

    @GetMapping("/rank/{pcIdx}")
    @ApiOperation(value = "푸드 홈 카테고리 리스트 조회", notes = "푸드 홈 카테고리 리스트 조회")
    public ResponseEntity<List<PoodRankCategoryDto>> getPoodHomeRankCategory(@PathVariable final int pcIdx) {
        return ResponseEntity.ok(poodHomeRankFacade.getPoodHomeRankCategory(pcIdx));
    }

    @GetMapping("/rank/goods")
    @ApiOperation(value = "푸드 홈 상품 리스트 조회", notes = "푸드 홈 상품 랭킹 리스트 조회")
    public ResponseEntity<Page<SortedGoodsList>> getPoodHomeRankGoodsList(
        @RequestParam(value = "pcIdx") final int pcIdx,
        @RequestParam(value = "subCtIdx") final Long subCtIdx,
        @RequestParam(value = "searchType") final String searchType,
        @PageableDefault(size = 100, sort = "idx", direction = Sort.Direction.DESC) final Pageable pageable) {
        return ResponseEntity.ok(
            poodHomeRankFacade.getPoodHomeRankGoodsList(subCtIdx, pcIdx, searchType, pageable));
    }

}
