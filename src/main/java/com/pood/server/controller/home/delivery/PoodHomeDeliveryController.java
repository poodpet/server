package com.pood.server.controller.home.delivery;

import com.pood.server.controller.response.home.delivery.PoodDeliveryCategoryDto;
import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.service.PoodHomeDeliveryFacade;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
public class PoodHomeDeliveryController {

    private final PoodHomeDeliveryFacade poodHomeDeliveryFacade;

    @GetMapping("/api/pood/v1-0/home/delivery/{pcIdx}")
    @ApiOperation(value = "푸드 홈 배송 카테고리 리스트 조회", notes = "푸드 홈 배송 카테고리 리스트 조회")
    public ResponseEntity<List<PoodDeliveryCategoryDto>> getPoodHomeDeliveryCategory( @PathVariable final int pcIdx) {
        return ResponseEntity.ok(poodHomeDeliveryFacade.getPoodHomeDeliveryCategory(pcIdx));
    }

    @GetMapping("/api/pood/v1-0/home/delivery/goods")
    @ApiOperation(value = "푸드 홈 상품 리스트 조회", notes = "푸드 홈 상품 리스트 조회")
    public ResponseEntity<Page<SortedGoodsList>> getPoodHomeDeliveryGoodsList(
        @RequestParam(value = "subCtIdx", required = false) Long subCtIdx,
        @RequestParam(value = "pcIdx", required = true) int pcIdx,
        @PageableDefault(sort = "idx", direction = Sort.Direction.DESC) Pageable pageable) {

        Page<SortedGoodsList> poodHomeGoodsList = poodHomeDeliveryFacade.getPoodHomeGoodsList(
            subCtIdx, pcIdx, pageable);

        return ResponseEntity.ok(poodHomeGoodsList);
    }

}

