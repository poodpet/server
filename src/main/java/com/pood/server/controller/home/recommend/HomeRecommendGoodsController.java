package com.pood.server.controller.home.recommend;

import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.facade.goods.GoodsFacade;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pood/v1-0")
public class HomeRecommendGoodsController {

    private final GoodsFacade goodsFacade;

    @ApiOperation(value = "푸드 홈 신상품 조회")
    @GetMapping("/home/new-goods/{petCategoryIdx}")
    public ResponseEntity<List<SortedGoodsList>> poodHomeNewGoods(
        @PathVariable
        @Max(value = 2, message = "idx값이 3 이상일 수 없습니다.") final Integer petCategoryIdx) {
        return ResponseEntity.ok(goodsFacade.poodHomeNewGoodsList(petCategoryIdx));
    }

    @ApiOperation(value = "푸드 홈 펫 카테고리별 세트 상품 조회")
    @GetMapping("/home/set-goods/{petCategoryIdx}")
    public ResponseEntity<List<SortedGoodsList>> poodHomeSetGoodsList(
        @PathVariable
        @Max(value = 2, message = "idx값이 3 이상일 수 없습니다.") final Integer petCategoryIdx) {
        return ResponseEntity.ok(goodsFacade.poodHomeSetGoodsList(petCategoryIdx));
    }

    @ApiOperation(value = "푸드 홈 푸드 마켓 상품 조회")
    @GetMapping("/home/pood-market/{petCategoryIdx}")
    public ResponseEntity<List<SortedGoodsList>> poodMarket(
        @PathVariable
        @Max(value = 2, message = "idx값이 3 이상일 수 없습니다.") final Integer petCategoryIdx) {
        return ResponseEntity.ok(goodsFacade.poodMarketGoodsList(petCategoryIdx));
    }

    @ApiOperation(value = "푸드 홈 푸드 맞춤 상품 조회")
    @PostMapping("/home/pood-match/{pcIdx}")
    public ResponseEntity<List<SortedGoodsList>> poodMatch(
        @PathVariable
        @Max(value = 2, message = "idx값이 3 이상일 수 없습니다.") final Integer pcIdx,
        @RequestBody @Size(max = 5, message = "검색 리스트의 길이가 5 이상일 수 없습니다.") final List<String> searchList) {
        return ResponseEntity.ok(goodsFacade.poodMatchGoodsList(pcIdx, searchList));
    }

}
