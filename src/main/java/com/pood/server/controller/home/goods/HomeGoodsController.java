package com.pood.server.controller.home.goods;

import com.pood.server.config.UserTokenValid;
import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.goods.GoodsFacade;
import com.pood.server.facade.solution.SolutionFacade;
import com.pood.server.service.pet_solution.ProductType;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class HomeGoodsController {

    private final GoodsFacade goodsFacade;
    private final SolutionFacade solutionFacade;

    @GetMapping("/api/pood/v1-0/home/recommend/random/goods/{productType}/{pcIdx}")
    @ApiOperation(value = "내 펫의 추천 타입 상품 리스트 조회", notes = "내 펫의 추천 용품 상품 리스트를 출력 합니다.")
    public ResponseEntity<List<SortedGoodsList>> getPoodHomeRecommendGoodsByProductType(
        @PathVariable final int pcIdx,
        @PathVariable final String productType,
        @RequestParam(defaultValue = "5", value = "size") final int size) {
        return ResponseEntity.ok(
            goodsFacade.getRandomFeedGoodsList(pcIdx, ProductType.getProductTypeByName(productType),
                size)
        );
    }

    @GetMapping("/api/pood/v1-0/home/recommend/goods/feed/{userPetIdx}")
    @UserTokenValid
    @ApiOperation(value = "내 펫의 추천 사료 상품 리스트 조회", notes = "내 펫의 추천 사료 상품 리스트를 출력 합니다.")
    public ResponseEntity<List<SortedGoodsList>> getPoodHomeRecommendGoodsByFeed(
        @PathVariable final int userPetIdx,
        @RequestParam(defaultValue = "5", value = "size") final int size,
        final UserInfo userInfo) {
        return ResponseEntity.ok(solutionFacade.getBestFeedGoodsListByLifeCycle(userPetIdx,
            userInfo, size)
        );
    }

    @GetMapping("/api/pood/v1-0/home/recommend/goods/snack/{userPetIdx}")
    @UserTokenValid
    @ApiOperation(value = "내 펫의 추천 간식 상품 리스트 조회", notes = "내 펫의 추천 간식 상품 리스트를 출력 합니다.")
    public ResponseEntity<List<SortedGoodsList>> getPoodHomeRecommendSnackGoods(
        @PathVariable final int userPetIdx,
        @RequestParam(defaultValue = "5", value = "size") final int size,
        final UserInfo userInfo) {
        return ResponseEntity.ok(solutionFacade.getTodaySnackGoodsList(userPetIdx,
            userInfo, size)
        );
    }

    @GetMapping("/api/pood/v1-0/home/recommend/goods/supplies/{userPetIdx}")
    @UserTokenValid
    @ApiOperation(value = "내 펫의 추천 용품 상품 리스트 조회", notes = "내 펫의 추천 영양제 용품 리스트를 출력 합니다.")
    public ResponseEntity<List<SortedGoodsList>> getPoodHomeRecommendSuppliesGoods(
        @PathVariable final int userPetIdx,
        @RequestParam(defaultValue = "5", value = "size") final int size,
        final UserInfo userInfo) {
        return ResponseEntity.ok(
            solutionFacade.getBestSuppliesGoodsList(userPetIdx, userInfo, size)
        );

    }

    @GetMapping("/api/pood/v1-0/home/recommend/goods/nutrients/{userPetIdx}")
    @UserTokenValid
    @ApiOperation(value = "내 펫의 추천 영양제 상품 리스트 조회", notes = "내 펫의 추천 영양제 상품 리스트를 출력 합니다.")
    public ResponseEntity<List<SortedGoodsList>> getPoodHomeRecommendNutrientsGoods(
        @PathVariable final int userPetIdx,
        @RequestParam(defaultValue = "5", value = "size") final int size,
        final UserInfo userInfo) {
        return ResponseEntity.ok(
            solutionFacade.getBestNutrientsGoodsList(userPetIdx, userInfo, size)
        );
    }


}
