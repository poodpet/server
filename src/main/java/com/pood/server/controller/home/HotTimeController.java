package com.pood.server.controller.home;

import com.pood.server.dto.meta.goods.GoodsDto;
import com.pood.server.service.GoodsServiceV2;
import com.pood.server.service.PromotionServiceV2;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
public class HotTimeController {

    private final GoodsServiceV2 goodsService;

    private final PromotionServiceV2 promotionService;

    @GetMapping("/api/pood/hot-time/{pcIdx}")
    @ApiOperation(value = "핫 타임 상품 리스트 조회", notes = "핫 타임 상품 리스트 조회")
    public ResponseEntity<List<GoodsDto.SortedGoodsList>> getHotTimeGoodsList(
        @PathVariable Integer pcIdx) {
        List<GoodsDto.SortedGoodsList> hotTimeGoodsList = promotionService.getHotTimeGoodsList(
            pcIdx);
        return new ResponseEntity<>(hotTimeGoodsList, HttpStatus.OK);
    }

}

