package com.pood.server.controller.home.today;

import com.pood.server.controller.response.home.icon.ShortCutIconResponse;
import com.pood.server.facade.ShortCutFacade;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pood/v1-0/home")
@Validated
public class PoodHomeTodayController {

    private final ShortCutFacade shortCutFacade;

    @GetMapping("/icon/{pcIdx}")
    @ApiOperation(value = "푸드 홈 short cut icon 조회")
    public ResponseEntity<List<ShortCutIconResponse>> findIconList(@PathVariable
    @Min(value = 0, message = "최소값은 0입니다.")
    @Max(message = "최대값은 2 입니다.", value = 2)
    @NotNull(message = "펫 카테고리가 null일 수 없습니다.") final Integer pcIdx) {
        return ResponseEntity.ok(shortCutFacade.findIconList(pcIdx));
    }

}
