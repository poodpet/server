package com.pood.server.controller.home;

import com.pood.server.controller.response.home.carousel.CarouselResponse;
import com.pood.server.facade.PoodHomeFacade;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pood/v1-0/home")
public class HomeController {

    private final PoodHomeFacade poodHomeFacade;

    @GetMapping("/image/slide/{petCategoryIdx}")
    @ApiOperation(value = "푸드홈 기획전/이벤트 캐러셀 조회")
    public ResponseEntity<CarouselResponse> findAllMarketingData(@PathVariable("petCategoryIdx") final int petCategoryIdx) {
        return ResponseEntity.ok(
            CarouselResponse.of(poodHomeFacade.findAllMarketingData(petCategoryIdx)));
    }
}
