package com.pood.server.controller;


import com.pood.server.config.UserTokenValid;
import com.pood.server.controller.response.order.OrderAvailableResponse;
import com.pood.server.controller.response.order.OrderQuantityResponse;
import com.pood.server.dto.log.user_order.OrderDto;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.OrderFacade;
import com.pood.server.facade.order.OrderCancelInfoRequest;
import com.pood.server.facade.order.OrderCancelInfoResponse;
import com.pood.server.facade.order.RefundAddressResponse;
import com.pood.server.service.OrderServiceV2;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class OrderController {

    private final OrderServiceV2 orderService;
    private final OrderFacade orderFacade;

    @GetMapping("/api/pood/order")
    @ApiOperation(value = "내 주문 목록 전체 조회", notes = "내 주문 목록 전체를 조회 합니다.")
    public ResponseEntity<Page<OrderDto.UserOrderDto>> getMyOrderList(
        @PageableDefault(size = 10, sort = "idx", direction = Sort.Direction.DESC) Pageable pageable,
        HttpServletRequest request) {
        String token = request.getHeader("token");
        Page<OrderDto.UserOrderDto> myOrderList = orderService.getMyOrderList(token, pageable);
        return new ResponseEntity<>(myOrderList, HttpStatus.OK);
    }

    @GetMapping("/api/pood/order/{orderIdx}")
    @ApiOperation(value = "내 주문 상세 조회", notes = "내 주문 상세 조회 합니다.")
    public ResponseEntity<OrderDto.UserOrderDto> getMyOrderDetail(@PathVariable Integer orderIdx,
        HttpServletRequest request) {
        String token = request.getHeader("token");
        OrderDto.UserOrderDto myOrderDetail = orderService.getMyOrderDetail(token, orderIdx);
        return new ResponseEntity<>(myOrderDetail, HttpStatus.OK);
    }

    @GetMapping("/api/pood/order/quantity/{goodsIdx}")
    @UserTokenValid
    @ApiOperation("주문 가능한 수량 조회")
    public ResponseEntity<Object> getMyOrderCount(@PathVariable int goodsIdx,
        final UserInfo userInfo) {
        return ResponseEntity.ok(
            OrderQuantityResponse.of(orderFacade.availableQuantity(goodsIdx, userInfo)));
    }

    @GetMapping("/api/pood/order/available/{goodsIdx}")
    @UserTokenValid
    @ApiOperation(value = "구매가능 체크 api", notes = "purchase_type이 1인경우에만 이 api를 수행")
    public ResponseEntity<OrderAvailableResponse> buyAvailable(
        @PathVariable("goodsIdx") final int goodsIdx, final UserInfo userInfo) {
        return ResponseEntity.ok(
            OrderAvailableResponse.of(orderFacade.buyAvailable(goodsIdx, userInfo)));
    }

    @GetMapping("/api/pood/order/address/refund/{refundIdx}")
    @UserTokenValid
    @ApiOperation(value = "반품에 대한 주소지 반환", notes = "반품에 대한 주소지를 반환 한다.")
    public ResponseEntity<RefundAddressResponse> getRefundAddress(
        @PathVariable("refundIdx") final int refundIdx, final UserInfo userInfo) {
        return ResponseEntity.ok(orderFacade.getRefundAddress(refundIdx, userInfo));
    }

    @PostMapping("/api/pood/order/cancel-info")
    @UserTokenValid
    @ApiOperation(value = "상품에 취소에 대한 정보 반환", notes = "상품에 취소에 대한 정보 반환 한다.")
    public ResponseEntity<OrderCancelInfoResponse> getOrderCancelInfo(
        @Valid @RequestBody final OrderCancelInfoRequest orderCancelInfoRequest,
        final UserInfo userInfo) {
        return ResponseEntity.ok(orderFacade.getOrderCancelInfo(userInfo, orderCancelInfoRequest));
    }

}

