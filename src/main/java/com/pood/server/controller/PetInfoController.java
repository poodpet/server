package com.pood.server.controller;

import com.pood.server.dto.meta.pet.PetInfoDto;
import com.pood.server.dto.meta.pet.PetInfoDtoGroup;
import com.pood.server.service.PetService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "펫 정보")
@RestController
@RequestMapping("/api/pood/pet")
@RequiredArgsConstructor
public class PetInfoController {

    private final PetService petService;

    @GetMapping
    @ApiOperation(value = "펫 정보 전체 조회 (믹스 우선 정렬)", notes = "믹스 우선 정렬로 펫을 조회합니다.")
    public ResponseEntity<List<PetInfoDto>> allPetInfo() {
        return ResponseEntity.ok(new PetInfoDtoGroup().addPetInfo(petService.allPetInfo()));
    }

}
