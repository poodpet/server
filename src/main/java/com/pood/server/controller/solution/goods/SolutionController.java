package com.pood.server.controller.solution.goods;

import com.pood.server.config.UserTokenValid;
import com.pood.server.controller.response.solution.SolutionResponse;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.solution.SolutionFacade;
import com.pood.server.facade.solution.request.FeedMonthlyBudgetRequest;
import com.pood.server.facade.solution.response.FeedAllergyResponse;
import com.pood.server.facade.solution.response.FeedBasicInformationResponse;
import com.pood.server.facade.solution.response.FeedEatAmountResponse;
import com.pood.server.facade.solution.response.FeedHealthCareResponse;
import com.pood.server.facade.solution.response.FeedMonthlyBudgetResponse;
import com.pood.server.facade.solution.response.FeedPreferenceResponse;
import com.pood.server.facade.solution.response.FeedNutrientResponse;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pood/v1-0/product")
public class SolutionController {

    private final SolutionFacade solutionFacade;

    @GetMapping("/detail/solution")
    @UserTokenValid
    @ApiOperation(value = "상품 상세 정보를 보여줍니다.")
    public ResponseEntity<SolutionResponse> goodsSolution(final UserInfo userInfo, @RequestParam final int userPetIdx,
        @RequestParam final int goodsIdx, @RequestParam final int productIdx) {
        return ResponseEntity.ok(solutionFacade.getGoodsSolution(userInfo, userPetIdx, goodsIdx, productIdx));
    }

    @GetMapping("/feed/amountOfEat")
    @UserTokenValid
    @ApiOperation(value = "급여량 정보 조회", notes = "선택된 사료들에 대한 급여량 정보를 조회합니다.")
    public ResponseEntity<List<FeedEatAmountResponse>> amountOfEatSolution(final UserInfo userInfo,
        @RequestParam(name = "goodsIdx") final List<Integer> goodsIdxList,
        @RequestParam @NotNull final Integer petIdx) {
        return ResponseEntity.ok(solutionFacade.getAmountOfEat(userInfo, goodsIdxList, petIdx));
    }

    @PostMapping("/feed/monthly-budget")
    @UserTokenValid
    @ApiOperation(value = "한달 예산 조회", notes = "선택된 사료들에 대해 급여량 정보에 따른 한달 예산")
    public ResponseEntity<List<FeedMonthlyBudgetResponse>> monthlyBudget(final UserInfo userInfo,
        @RequestBody final FeedMonthlyBudgetRequest request) {
        return ResponseEntity.ok(solutionFacade.getMonthlyBudget(userInfo, request));
    }

    @GetMapping("/feed/basic-information")
    @UserTokenValid
    @ApiOperation(value = "기본 정보 조회", notes = "선택된 사료들에 대한 기본 정보 분석 결과를 조회합니다.")
    public ResponseEntity<List<FeedBasicInformationResponse>> getFeedBasicInformation(
        final UserInfo userInfo, @RequestParam(name = "goodsIdx") final List<Integer> goodsIdxList,
        @RequestParam @NotNull final Integer petIdx) {

        return ResponseEntity.ok(solutionFacade.getBasicInformation(userInfo, goodsIdxList, petIdx));
    }

    @GetMapping("/feed/allergy")
    @UserTokenValid
    @ApiOperation(value = "알러지 정보 조회", notes = "선택된 사료들에 대한 알러지 분석 결과를 조회합니다.")
    public ResponseEntity<List<FeedAllergyResponse>> getFeedAllergyInformation(
        final UserInfo userInfo, @RequestParam(name = "goodsIdx") final List<Integer> goodsIdxList,
        @RequestParam @NotNull final Integer petIdx) {

        return ResponseEntity.ok(solutionFacade.getAllergyInfo(userInfo, goodsIdxList, petIdx));
    }

    @GetMapping("/feed/nutrient")
    @UserTokenValid
    @ApiOperation(value = "영양소 정보 조회", notes = "선택된 사료들에 대한 영양소 분석 결과를 조회합니다.")
    public ResponseEntity<List<FeedNutrientResponse>> getFeedNutrientInformation(
        final UserInfo userInfo, @RequestParam(name = "goodsIdx") final List<Integer> goodsIdxList,
        @RequestParam @NotNull final Integer petIdx) {

        return ResponseEntity.ok(solutionFacade.getNutrientsInfo(userInfo, goodsIdxList, petIdx));
    }

    @GetMapping("/feed/preference")
    @UserTokenValid
    @ApiOperation(value = "기호성 정보 조회", notes = "선택된 사료들에 대한 기호성 분석 결과를 조회합니다.")
    public ResponseEntity<List<FeedPreferenceResponse>> getFeedGrainSizeInformation(
        final UserInfo userInfo, @RequestParam(name = "goodsIdx") final List<Integer> goodsIdxList,
        @RequestParam @NotNull final Integer petIdx) {

        return ResponseEntity.ok(solutionFacade.getFeedGrainSizeInfo(userInfo, goodsIdxList, petIdx));
    }

    @GetMapping("/feed/health-care")
    @UserTokenValid
    @ApiOperation(value = "건강케어 정보 조회", notes = "선택된 사료들에 대한 건강케어 분석 결과를 조회합니다.")
    public ResponseEntity<List<FeedHealthCareResponse>> getHealthCareInformation(
        final UserInfo userInfo, @RequestParam(name = "goodsIdx") final List<Integer> goodsIdxList,
        @RequestParam @NotNull final Integer petIdx) {

        return ResponseEntity.ok(solutionFacade.getHealthCareInfo(userInfo, goodsIdxList, petIdx));
    }
}
