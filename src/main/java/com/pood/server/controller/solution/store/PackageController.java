package com.pood.server.controller.solution.store;

import com.pood.server.config.UserTokenValid;
import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.solution.SolutionFacade;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pood/v1-0/solution-store/package")
public class PackageController {

    private final SolutionFacade goodsSolutionInfoFacade;

    @GetMapping("/best/{userPetIdx}")
    @ApiOperation(value = "유저 펫 Best 패키지 상품 묶음", notes = "유저 펫 Best 패키지 상품 묶음을 반환 합니다.")
    @UserTokenValid
    public ResponseEntity<List<SortedGoodsList>> getBestGoodsList(
        @PathVariable("userPetIdx") final int userPetIdx, final UserInfo userInfo) {
        return ResponseEntity.ok(goodsSolutionInfoFacade.getBestGoodsList(userInfo, userPetIdx));
    }

    @GetMapping("/worry/{userPetIdx}/{worryIdx}")
    @ApiOperation(value = "유저 펫 고민 별 패키지 상품 묶음", notes = "유저 펫 고민 별 패키지 상품 묶음을 반환 합니다.")
    @UserTokenValid
    public ResponseEntity<List<SortedGoodsList>> getWorryGoodsList(
        @PathVariable("userPetIdx") final int userPetIdx,
        @PathVariable("worryIdx") final int worryIdx, final UserInfo userInfo) {
        return ResponseEntity.ok(
            goodsSolutionInfoFacade.getWorryGoodsList(userInfo, worryIdx, userPetIdx));
    }


}
