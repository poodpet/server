package com.pood.server.controller.solution.store;

import com.pood.server.config.UserTokenValid;
import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.solution.SolutionFacade;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pood/v1-0/solution-store/frequently")
public class FrequentlyBuyController {

    private final SolutionFacade goodsSolutionInfoFacade;

    @GetMapping("/{userPetIdx}/{buyCount}")
    @ApiOperation(value = "자주 산 상품 리스트 출력", notes = "자주 산 상품 리스트 조회합니다.")
    @UserTokenValid
    public ResponseEntity<Page<SortedGoodsList>> getUserFrequentlyBuyGoodsList(
        @PathVariable int buyCount,
        @PathVariable("userPetIdx") final int userPetIdx,
        @PageableDefault(sort = "goods_idx", direction = Sort.Direction.ASC) final Pageable pageable,
        final UserInfo userInfo) {
        return ResponseEntity.ok(
            goodsSolutionInfoFacade.getUserFrequentlyBuyGoodsList(buyCount, userPetIdx, userInfo,
                pageable));
    }

}
