package com.pood.server.controller.solution.store;

import com.pood.server.config.UserTokenValid;
import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.solution.SolutionFacade;
import com.pood.server.service.pet_solution.ProductType;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pood/v1-0/solution-store/recommend")
public class RecommendController {

    private final SolutionFacade goodsSolutionInfoFacade;

    @GetMapping("/best/feed/{userPetIdx}")
    @ApiOperation(value = "유저 펫 추천 사료 묶음", notes = "유저 펫 추천 사료 묶음을 반환 합니다.")
    @UserTokenValid
    public ResponseEntity<List<SortedGoodsList>> getBestFeedGoodsList(
        @PathVariable("userPetIdx") final int userPetIdx, final UserInfo userInfo) {
        return ResponseEntity.ok(
            goodsSolutionInfoFacade.getBestGoodsListByFeed(userInfo, userPetIdx));
    }

    @GetMapping("/best/snack/{userPetIdx}")
    @ApiOperation(value = "유저 펫 추천 간식 묶음", notes = "유저 펫 추천 간식 묶음을 반환 합니다.")
    @UserTokenValid
    public ResponseEntity<List<SortedGoodsList>> getBestSnackGoodsList(
        @PathVariable("userPetIdx") final int userPetIdx, final UserInfo userInfo) {
        return ResponseEntity.ok(
            goodsSolutionInfoFacade.getBestGoodsListBySnack(userInfo, userPetIdx));
    }

    @GetMapping("/{productType}/{worryIdx}/{userPetIdx}")
    @ApiOperation(value = "유저 펫 걱정별 + 상품 타입 별 상품 리스트 조회", notes = "유저 펫 걱정별 + 상품 타입 별 상품 리스트 조회")
    @UserTokenValid
    public ResponseEntity<List<SortedGoodsList>> getBestGoodsListByProductTypeAndWorry(
        @PathVariable("userPetIdx") final int userPetIdx,
        @PathVariable("worryIdx") final long worryIdx,
        @PathVariable("productType") final int productType,
        final UserInfo userInfo) {
        return ResponseEntity.ok(
            goodsSolutionInfoFacade.getBestGoodsListByProductTypeAndWorry(userInfo, userPetIdx,
                productType, worryIdx));
    }

    @GetMapping("/feed/{worryIdx}/{userPetIdx}")
    @ApiOperation(value = "유저 펫 걱정별 사료 타입 별 상품 리스트 조회", notes = "유저 펫 걱정별 사료 상품 리스트 조회를 조회 한다")
    @UserTokenValid
    public ResponseEntity<List<SortedGoodsList>> getBestGoodsListByFeedAndWorry(
        @PathVariable("userPetIdx") final int userPetIdx,
        @PathVariable("worryIdx") final long worryIdx, final UserInfo userInfo) {
        return ResponseEntity.ok(
            goodsSolutionInfoFacade.getBestGoodsListByBestWorryScoreFilter(userInfo, userPetIdx,
                worryIdx, ProductType.FEED));
    }

    @GetMapping("/snack/{worryIdx}/{userPetIdx}")
    @ApiOperation(value = "유저 펫 걱정별 간식 상품 리스트 조회", notes = "유저 펫 걱정별 간식 상품 리스트 조회를 조회 한다")
    @UserTokenValid
    public ResponseEntity<List<SortedGoodsList>> getBestGoodsListBySanckAndWorry(
        @PathVariable("userPetIdx") final int userPetIdx,
        @PathVariable("worryIdx") final long worryIdx, final UserInfo userInfo) {
        return ResponseEntity.ok(
            goodsSolutionInfoFacade.getBestGoodsListByBestWorryScoreFilter(userInfo, userPetIdx,
                worryIdx, ProductType.SNACK));
    }

    @GetMapping("/supplies/physical/{userPetIdx}")
    @ApiOperation(value = "유저 펫 체형 용품 리스트 조회", notes = "유저 펫의 체형에 맞는 용품 리스트 조회 한다.")
    @UserTokenValid
    public ResponseEntity<List<SortedGoodsList>> getBestSuppliesGoodsListByPhysical(
        @PathVariable("userPetIdx") final int userPetIdx, final UserInfo userInfo) {
        return ResponseEntity.ok(
            goodsSolutionInfoFacade.getBestSuppliesGoodsListByPhysical(userInfo, userPetIdx));
    }

    @GetMapping("/supplies/age/{userPetIdx}")
    @ApiOperation(value = "유저 펫 나이 용품 리스트 조회", notes = "유저 펫의 나이에 맞는 용품 리스트 조회 한다.")
    @UserTokenValid
    public ResponseEntity<List<SortedGoodsList>> getBestSuppliesGoodsListByAge(
        @PathVariable("userPetIdx") final int userPetIdx, final UserInfo userInfo) {
        return ResponseEntity.ok(
            goodsSolutionInfoFacade.getBestSuppliesGoodsListByAge(userInfo, userPetIdx));
    }

    @GetMapping("/non-login/{pcIdx}/{worryIdx}")
    @ApiOperation(value = "비 로그인 고민거리 상품 리스트 조회", notes = "비 로그인 고민거리 상품 리스트를 조회 합니다.")
    public ResponseEntity<List<SortedGoodsList>> getNonLoginBestGoodsList(
        @PathVariable("pcIdx") final int pcIdx,
        @PathVariable("worryIdx") final long worryIdx) {
        return ResponseEntity.ok(goodsSolutionInfoFacade.getNonLoginBestGoodsList(pcIdx, worryIdx));
    }

}
