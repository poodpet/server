package com.pood.server.controller.solution.manager;

import com.pood.server.config.UserTokenValid;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.solution.SolutionFacade;
import com.pood.server.facade.user.pet.response.UserPetLifeCycleResponse;
import com.pood.server.facade.user.pet.response.WorryManagerResponse;
import com.pood.server.facade.user.solution.response.BodyShapeRecommendGoodsResponse;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pood/v1-0/solution-store/manager")
public class ManagerController {

    private final SolutionFacade goodsSolutionInfoFacade;

    @GetMapping("/life/cycle/{userPetIdx}")
    @ApiOperation(value = "유저 펫에 대한 생애주기 정보 조회", notes = "유저 펫에 대한 생애주기 정보를 조회 합니다.")
    @UserTokenValid
    public ResponseEntity<UserPetLifeCycleResponse> getUserPetLifeCycleInfo(
        @PathVariable("userPetIdx") final int userPetIdx, final UserInfo userInfo) {
        return ResponseEntity.ok(
            goodsSolutionInfoFacade.getUserPetLifeCycleInfo(userInfo, userPetIdx));
    }

    @GetMapping("/worry/{worryIdx}/{userPetIdx}")
    @ApiOperation(value = "유저 펫에 대한 고민 정보 조회", notes = "유저 펫에 대한 고민 정보를 조회 합니다.")
    @UserTokenValid
    public ResponseEntity<WorryManagerResponse> getUserPetWorryManagerInfo(
        @PathVariable("userPetIdx") final int userPetIdx,
        @PathVariable("worryIdx") final long worryIdx,
        final UserInfo userInfo) {
        return ResponseEntity.ok(
            goodsSolutionInfoFacade.getUserPetWorryManagerInfo(userInfo, worryIdx, userPetIdx));
    }

    @GetMapping("/body-shape/recommend-goods/{userPetIdx}")
    @ApiOperation(value = "유저 펫 체형 맞춤 상품 조회", notes = "해당하는 유저의 펫의 최근 체형 선택에 대한 상품 리스트를 조회합니다.")
    @UserTokenValid
    public ResponseEntity<List<BodyShapeRecommendGoodsResponse>> getBodyRecommendedProducts(
        final UserInfo userInfo, @PathVariable final int userPetIdx) {

        return ResponseEntity.ok(
            goodsSolutionInfoFacade.findSolutionGoodsByBodyShape(userInfo, userPetIdx));
    }
}
