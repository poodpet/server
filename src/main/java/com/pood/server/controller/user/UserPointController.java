package com.pood.server.controller.user;

import com.pood.server.config.UserTokenValid;
import com.pood.server.dto.user.point.PointInfoResponse;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.UserPointFacade;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pood/v1-0/point")
public class UserPointController {

    private final UserPointFacade userPointFacade;

    @GetMapping
    @UserTokenValid
    @ApiOperation(value = "유저 포인트를 조회합니다.", notes = "header에 token값이 필요합니다. (AOP)")
    public ResponseEntity<PointInfoResponse> userPointInfo(final UserInfo userInfo) {
        return ResponseEntity.ok(userPointFacade.userPointInfoAll(userInfo));
    }

    @GetMapping("/expired")
    @UserTokenValid
    @ApiOperation(value = "기간 만료 30일 남은 포인트 정보 조회", notes = "기간 만료 30일 남은 포인트 정보 조회 합니다.")
    public ResponseEntity<Integer> userExpiredPointInfoBy30Day(final UserInfo userInfo) {
        return ResponseEntity.ok(userPointFacade.userExpiredPointInfoBy30Day(userInfo));
    }

}