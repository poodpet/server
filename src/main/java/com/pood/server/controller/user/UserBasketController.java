package com.pood.server.controller.user;

import com.pood.server.config.UserTokenValid;
import com.pood.server.controller.response.goods.SuggestRandomGoodsResponse;
import com.pood.server.dto.meta.goods.GoodsDto;
import com.pood.server.dto.user.UserBasketDto;
import com.pood.server.entity.user.UserBasket;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.goods.GoodsFacade;
import com.pood.server.facade.UserBaskeFacade;
import com.pood.server.service.GoodsServiceV2;
import com.pood.server.service.UserBasketServiceV2;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class UserBasketController {

    private final UserBasketServiceV2 userBasketService;
    private final GoodsServiceV2 goodsService;
    private final GoodsFacade goodsFacade;
    private final UserBaskeFacade userBaskeFacade;

    @GetMapping("/api/pood/user/basket")
    @ApiOperation(value = "내 장바구니 전체 조회", notes = "내 장바구니 정보 리스트를 출력 합니다.")
    public ResponseEntity<List<UserBasketDto.UserBasketListDto>> getMybasketList(HttpServletRequest request) {
        String token = (String) request.getHeader("token");

        List<UserBasket> basketList = userBasketService.getBasketList(token);
        List<GoodsDto.GoodsDetail> goodsInfoList = new ArrayList<>();
        for (UserBasket userBasket : basketList) {
            GoodsDto.GoodsDetail goodsDetail = goodsService.getGoodsBasketDetail(userBasket.getGoodsIdx());
            if (goodsDetail == null) {
                continue;
            }
            goodsInfoList.add(goodsDetail);
        }
        List<UserBasketDto.UserBasketListDto> myBasketGoodsDetailList = userBasketService.getMyBasketGoodsDetailList(basketList,
            goodsInfoList);

        return new ResponseEntity<>(myBasketGoodsDetailList, HttpStatus.OK);
    }

    @GetMapping("/api/pood/v1-0/user/basket/suggest")
    @ApiOperation(value = "장바구니 하단 랜덤 굿즈 추천", notes = "10개의 상품을 랜덤으로 보여줍니다.")
    public ResponseEntity<List<SuggestRandomGoodsResponse>> suggestRandomGoods() {
        return ResponseEntity.ok(goodsFacade.suggest10Goods());
    }

    @UserTokenValid
    @GetMapping("/api/pood/v1-0/user/basket/goods/count")
    @ApiOperation(value = "장바구니 담김 상품 갯수", notes = "장바구니 담김 상품 갯수를 보여줍니다.")
    public ResponseEntity<Long> getBasketGoodsCount(final UserInfo userInfo) {
        return ResponseEntity.ok(userBaskeFacade.getBasketGoodsCount(userInfo));
    }
}

