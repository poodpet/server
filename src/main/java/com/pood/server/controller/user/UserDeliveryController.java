package com.pood.server.controller.user;

import com.pood.server.config.UserTokenValid;
import com.pood.server.dto.user.delivery.UserDeliveryCreateRequest;
import com.pood.server.dto.user.delivery.UserDeliveryUpdateRequest;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.UserDeliveryFacade;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pood/v1-0/delivery")
public class UserDeliveryController {

    private final UserDeliveryFacade userDeliveryFacade;

    @UserTokenValid
    @PostMapping
    @ApiOperation(value = "유저의 배송지를 추가합니다.")
    public ResponseEntity<Object> saveDeliveryAddress(final UserInfo userInfo,
        @RequestBody final UserDeliveryCreateRequest userDeliveryCreateRequest) {
        userDeliveryFacade.saveAddress(userDeliveryCreateRequest, userInfo);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @UserTokenValid
    @GetMapping
    @ApiOperation(value = "유저의 배송지를 조회합니다.")
    public ResponseEntity<Object> findAllAddressOfUser(final UserInfo userInfo) {
        return ResponseEntity.ok(userDeliveryFacade.findAllAddressOfUser(userInfo));
    }

    @PatchMapping
    @ApiOperation(value = "유저의 배송지를 업데이트합니다.")
    @UserTokenValid
    public ResponseEntity<Object> updateDeliveryAddress(
        @RequestBody final UserDeliveryUpdateRequest userDeliveryUpdateRequest,
        final UserInfo userInfo) {
        userDeliveryFacade.updateAddress(userDeliveryUpdateRequest, userInfo);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{deliveryIdx}")
    @UserTokenValid
    public ResponseEntity<Object> deleteDeliveryAddress(
        @PathVariable("deliveryIdx") final int deliveryIdx,
        final UserInfo userInfo) {
        userDeliveryFacade.deleteAddress(deliveryIdx, userInfo);
        return ResponseEntity.ok().build();
    }
}
