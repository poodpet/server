package com.pood.server.controller.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pood.server.config.UserTokenValid;
import com.pood.server.controller.request.userpet.UserPetCreateDto;
import com.pood.server.controller.request.userpet.UserPetUpdateRequest;
import com.pood.server.controller.response.pet.AllergyDataResponse;
import com.pood.server.controller.response.pet.GrainSizeResponse;
import com.pood.server.controller.response.pet.PetWorryResponse;
import com.pood.server.controller.response.pet.UserPetInfoResponse;
import com.pood.server.controller.response.pet.UserPetWeightDiaryResponse;
import com.pood.server.dto.meta.pet.PetWorryGroup;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.UserPetFacade;
import com.pood.server.facade.user.pet.request.BodyShapeManageCreateRequest;
import com.pood.server.facade.user.pet.request.BodyShapeModifyRequest;
import com.pood.server.facade.user.pet.request.WeightDeleteRequest;
import com.pood.server.facade.user.pet.request.WeightUpdateRequest;
import com.pood.server.facade.user.pet.response.BodyShapeManageCreateResponse;
import com.pood.server.facade.user.pet.response.BodyShapeManageResponse;
import com.pood.server.facade.user.pet.response.UserPetWeightResponse;
import com.pood.server.facade.user.pet.response.UserPetWeightUpdateResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Api(tags = "유저 펫 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pood/v1-0/pet")
@Validated
public class UserPetController {

    private final UserPetFacade userPetFacade;

    private final ObjectMapper objectMapper;

    @PostMapping("/create")
    @ApiOperation(value = "유저 펫 등록", notes = "유저 펫 정보를 등록합니다.")
    @UserTokenValid
    public ResponseEntity<Object> create(
        @RequestPart("file") final List<MultipartFile> multipartFile,
        @RequestPart("request") final String createRequest,
        final UserInfo userInfo) throws Exception {
        final UserPetCreateDto userPetCreateDto = objectMapper.readValue(createRequest,
            UserPetCreateDto.class);

        return ResponseEntity.status(HttpStatus.CREATED)
            .body(userPetFacade.createUserPet(userPetCreateDto, userInfo, multipartFile));
    }

    @GetMapping
    @ApiOperation(value = "유저 펫 조회", notes = "토큰으로 찾은 회원의 펫 정보를 조회합니다.")
    @UserTokenValid
    public ResponseEntity<List<UserPetInfoResponse>> info(
        final UserInfo userInfo) {
        return ResponseEntity.ok(userPetFacade.userPetInfoList(userInfo));
    }

    @DeleteMapping("/{userPetIdx}")
    @ApiOperation(value = "유저 펫 삭제", notes = "해당하는 유저의 펫 정보를 삭제합니다.")
    @UserTokenValid
    public ResponseEntity<Object> delete(@PathVariable("userPetIdx") final int userPetIdx,
        final UserInfo userInfo) {
        userPetFacade.deleteUserPet(userInfo, userPetIdx);
        return ResponseEntity.ok().build();
    }

    @PatchMapping
    @ApiOperation(value = "유저 펫 수정", notes = "해당하는 유저의 펫 정보를 수정합니다.")
    @UserTokenValid
    public ResponseEntity<Object> update(
        @RequestPart("file") final List<MultipartFile> multipartFiles,
        @RequestPart("request") final String request,
        final UserInfo userInfo) throws Exception {
        final UserPetUpdateRequest userPetUpdateRequest = objectMapper.readValue(request,
            UserPetUpdateRequest.class);
        userPetFacade.updateUserPet(userInfo, userPetUpdateRequest, multipartFiles);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/allergy")
    @ApiOperation("알러지 목록 조회")
    public ResponseEntity<List<AllergyDataResponse>> findAllergy() {
        return ResponseEntity.ok(userPetFacade.findAllergyDataList());
    }

    @GetMapping("/grainsize")
    @ApiOperation("알갱이 사이즈 리스트 조회")
    public ResponseEntity<List<GrainSizeResponse>> findGrainSize() {
        return ResponseEntity.ok(userPetFacade.findGrainSizeDataList());
    }

    @GetMapping("/worry/{petCategoryIdx}")
    @ApiOperation("고민거리 리스트 조회")
    public ResponseEntity<List<PetWorryResponse>> findWorry(
        @PathVariable("petCategoryIdx") @Min(value = 1, message = "1 이하의 숫자는 넣을 수 없습니다.")
        @Max(value = 2, message = "2 이상의 숫자는 넣을 수 없습니다.") final int petCategoryIdx) {
        final PetWorryGroup petWorryGroup = userPetFacade.findWorry(petCategoryIdx);
        return ResponseEntity.ok(petWorryGroup.toResponse());
    }

    @GetMapping("/weight/{userPetIdx}")
    @ApiOperation(value = "유저 펫의 무게 기록 조회")
    @UserTokenValid
    public ResponseEntity<List<UserPetWeightDiaryResponse>> findWeightDiary(
        @PathVariable final int userPetIdx, final UserInfo userInfo) {
        return ResponseEntity.ok(userPetFacade.findWeightDiary(userPetIdx, userInfo)
            .toResponse());
    }

    @PutMapping("/weight")
    @ApiOperation(value = "유저 펫 지난 날의 무게를 갱신")
    @UserTokenValid
    public ResponseEntity<UserPetWeightUpdateResponse> weightUpdateData(final UserInfo userInfo,
        @Valid @RequestBody final WeightUpdateRequest request) {
        return ResponseEntity.ok(userPetFacade.weightChange(request, userInfo));
    }

    @GetMapping("/body-shape-management/{userPetIdx}")
    @ApiOperation(value = "유저 펫 체형관리 정보 조회")
    @UserTokenValid
    public ResponseEntity<BodyShapeManageResponse> getBodyShapeInfo(final UserInfo userInfo,
        @PathVariable final int userPetIdx) {

        return ResponseEntity.ok(userPetFacade.getBodyManagementInfo(userInfo, userPetIdx));
    }

    @PostMapping("/body-shape-management")
    @ApiOperation(value = "유저 펫 체형관리 신규 정보 등록")
    @UserTokenValid
    public ResponseEntity<BodyShapeManageCreateResponse> createBodyShapeInfo(
        final UserInfo userInfo, @Valid @RequestBody final BodyShapeManageCreateRequest request) {

        return ResponseEntity.status(HttpStatus.CREATED)
            .body(userPetFacade.recordBodyShapeManagement(request, userInfo));
    }

    @PatchMapping("/body-shape")
    @ApiOperation(value = "유저 펫 체형정보 업데이트")
    @UserTokenValid
    public ResponseEntity<BodyShapeManageCreateResponse> updateBodyShapeInfo(
        final UserInfo userInfo, @Valid @RequestBody final BodyShapeModifyRequest request) {

        return ResponseEntity.ok(userPetFacade.modifyBodyShape(userInfo, request));
    }

    @GetMapping("/weight/{year}/{userPetIdx}")
    @ApiOperation(value = "유저 펫의 무게 기록 조회(주차별)")
    @UserTokenValid
    public ResponseEntity<List<UserPetWeightResponse>> findWeightDiaryByYear(
        @PathVariable final int userPetIdx, final UserInfo userInfo, @PathVariable final int year) {
        return ResponseEntity.ok(userPetFacade.findWeightDiaryByYear(userPetIdx, userInfo, year));
    }

    @DeleteMapping("/weight")
    @ApiOperation(value = "유저 펫 삭제", notes = "해당하는 유저의 펫 정보를 삭제합니다.")
    @UserTokenValid
    public ResponseEntity<Object> deleteWeight(
        @Valid @RequestBody final WeightDeleteRequest request, final UserInfo userInfo) {
        userPetFacade.deleteUserPetWeight(userInfo, request);
        return ResponseEntity.ok().build();
    }

    @PatchMapping("/worry/body-shape/{userPetIdx}")
    @UserTokenValid
    @ApiOperation(value = "유저 펫 체형, 상충 고민 즉시 수정", notes = "최근 등록한 체형과 선택한 고민이 상충될 시 고민 즉시수정합니다.")
    public ResponseEntity<Object> updateConflictWorryWithBodyShape(
        final UserInfo userInfo, @PathVariable final int userPetIdx) {
        userPetFacade.updatePetWorryConflictByBodyShape(userPetIdx, userInfo.getIdx());
        return ResponseEntity.ok().build();
    }
}
