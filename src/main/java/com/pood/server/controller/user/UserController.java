package com.pood.server.controller.user;

import com.pood.server.config.UserTokenValid;
import com.pood.server.controller.request.event.EventBlockRequest;
import com.pood.server.controller.request.user.UserInfoUpdateRequest;
import com.pood.server.controller.response.user.UserEmailResponse;
import com.pood.server.dto.meta.goods.GoodsDto;
import com.pood.server.dto.meta.goods.UserBaseImageDto;
import com.pood.server.dto.user.UserDto;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserNoti;
import com.pood.server.exception.CustomStatusException;
import com.pood.server.exception.ErrorMessage;
import com.pood.server.facade.UserFacade;
import com.pood.server.facade.goods.GoodsFacade;
import com.pood.server.facade.user.image.request.UserBaseImageUpdateRequest;
import com.pood.server.facade.user.image.response.UserBaseImagesResponse;
import com.pood.server.facade.user.info.FindUserEmailRequest;
import com.pood.server.facade.user.info.FindUserEmailResponse;
import com.pood.server.facade.user.info.UserPasswordChangeRequest;
import com.pood.server.facade.user.info.request.DormantCancelRequest;
import com.pood.server.facade.user.info.request.RequstGoodsRequest;
import com.pood.server.service.UserNotiSeparate;
import com.pood.server.service.UserServiceV2;
import io.swagger.annotations.ApiOperation;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pood/user")
public class UserController {

    private final UserServiceV2 userService;
    private final UserNotiSeparate userNotiSeparate;
    private final UserFacade userFacade;
    private final GoodsFacade goodsFacade;

    @GetMapping("/wish")
    @UserTokenValid
    @ApiOperation(value = "내 찜상품 전체 조회", notes = "내 찜상품 정보 리스트를 출력 합니다.")
    public ResponseEntity<List<GoodsDto.SortedGoodsList>> getProductList(final UserInfo userInfo) {
        return new ResponseEntity<>(goodsFacade.getMyGoodsWishList(userInfo), HttpStatus.OK);
    }

    @GetMapping("/images")
    @ApiOperation(value = "내 정보 사진 리스트 조회", notes = "내 정보에서 선택할 수 있는 사진 리스트를 출력 합니다.")
    public ResponseEntity<List<UserBaseImageDto.UserBaseImageList>> getUserBaseImgList(
        @RequestHeader("token") String token) {
        tokenValidation(token);
        List<UserBaseImageDto.UserBaseImageList> userBaseImgList = userService.getUserBaseImgList(
            token);
        return new ResponseEntity<>(userBaseImgList, HttpStatus.OK);
    }

    @GetMapping("/v1-0/images")
    @ApiOperation(value = "내 정보 기본 사진 리스트 조회", notes = "내 정보에서 선택할 수 있는 기본 사진 리스트를 출력 합니다.")
    public ResponseEntity<List<UserBaseImagesResponse>> getUserBaseImgListV1() {
        return ResponseEntity.ok(userFacade.getUserBaseImgList());
    }

    @PatchMapping("/images")
    @ApiOperation(value = "내 정보 사진 변경", notes = "내 정보에서 사진을 변경합니다.")
    public ResponseEntity<String> modifyUserBaseImages(@RequestBody UserBaseImageDto.IdxDto idxDto,
        @RequestHeader("token") String token) {
        tokenValidation(token);
        userService.saveUserBaseImg(idxDto, token);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @UserTokenValid
    @PatchMapping("/v1-0/images")
    @ApiOperation(value = "내 정보 사진 변경", notes = "내 정보에서 사진을 변경합니다.")
    public ResponseEntity<String> modifyUserBaseImagesV1(
        @Valid @RequestBody UserBaseImageUpdateRequest request,
        final UserInfo userInfo) {
        userFacade.saveUserBaseImg(request, userInfo);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/point")
    @ApiOperation(value = "내 포인트 정보 리스트 조회", notes = "내 포인트 정보 리스트를 적립완료만 조회합니다.")
    public ResponseEntity<List<UserDto.PointDto>> getMyPointList(
        @RequestHeader("token") String token) {
        tokenValidation(token);
        return ResponseEntity.ok(userService.getMyPointList(token));
    }

    @GetMapping("/check/my-phone")
    @ApiOperation(value = "내 핸드폰 번호 체크", notes = "내 핸드폰 번호 체크 조회합니다.")
    public ResponseEntity<HashMap<Object, Object>> getMyPointList(UserDto.Phone phone,
        @RequestHeader("token") String token) {
        tokenValidation(token);
        return ResponseEntity.ok(userService.checkMyPhoneNumber(token, phone));
    }

    @UserTokenValid
    @GetMapping("/alarm")
    @ApiOperation(value = "알람 리스트 받아오기")
    public ResponseEntity<List<UserNoti>> getMyPointList(final UserInfo userInfo) {
        return ResponseEntity.ok(userNotiSeparate.findAllUserNoti(userInfo.getUserUuid()));
    }

    @DeleteMapping("/logout")
    @UserTokenValid
    @ApiOperation(value = "로그아웃, 토큰 삭제")
    public ResponseEntity<Object> deleteUserToken(final UserInfo userInfo) {
        userFacade.logout(userInfo);
        return ResponseEntity.ok().build();
    }

    private void tokenValidation(final String token) {
        if (Objects.isNull(token)) {
            throw CustomStatusException.unAuthorized(ErrorMessage.API_TOKEN);
        }
    }

    @PostMapping("/v1-0/block")
    @UserTokenValid
    @ApiOperation(value = "개인적으로 이벤트 게시물 차단")
    public ResponseEntity<Object> postingBlock(
        @Valid @RequestBody final EventBlockRequest eventBlockRequest, final UserInfo userInfo) {
        userFacade.userPostingBlock(eventBlockRequest, userInfo);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/v1-0/email")
    @ApiOperation(value = "유저 이메일 조회")
    public ResponseEntity<Object> findEmail(@RequestParam("userUuid") final String userUuid) {
        final UserInfo userInfo = userFacade.getUserByUUID(userUuid);
        return ResponseEntity.ok(UserEmailResponse.of(userInfo.getUserEmail()));
    }

    @PatchMapping("/v1-0/password")
    @ApiOperation(value = "유저 비밀번호 변경")
    @UserTokenValid
    public ResponseEntity<Object> updatePassword(
        @Valid @RequestBody final UserPasswordChangeRequest request, final UserInfo userInfo) {
        userFacade.changePassword(request, userInfo);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/v1-0/change")
    @ApiOperation(value = "유저 정보 수정")
    public ResponseEntity<Object> updateUserInfo(@RequestBody final UserInfoUpdateRequest request) {
        userFacade.updateUserInfo(request);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/v1-0/erase")
    @ApiOperation(value = "회원 탈퇴")
    @UserTokenValid
    public ResponseEntity<Object> withdrawalUser(final UserInfo userInfo) {
        userFacade.userWithdrawal(userInfo);
        return ResponseEntity.ok().build();
    }

    @UserTokenValid
    @PostMapping("/v1-0/custom/images")
    @ApiOperation(value = "내 정보 사진 변경", notes = "내 정보에서 사진을 변경합니다.")
    public ResponseEntity<UserBaseImagesResponse> modifyUserImagesV1(
        @Valid @RequestPart("file") final MultipartFile file, final UserInfo userInfo)
        throws IOException {

        return ResponseEntity.ok().body(userFacade.modifyUserImages(file, userInfo));
    }

    @PostMapping("/find/email")
    @ApiOperation(value = "휴대폰 인증 후 해당 유저 이메일 찾기")
    public ResponseEntity<FindUserEmailResponse> findEmailByAuth(
        @Valid @RequestBody final FindUserEmailRequest request) {
        return ResponseEntity.ok(userFacade.findUserEmail(request));
    }

    @PostMapping("/requst/goods")
    @UserTokenValid
    @ApiOperation(value = "미취급 상품 요청", notes = "미취급 상품을 요청 합니다.")
    public ResponseEntity<Object> addRequstGoods(
        @Valid @RequestBody final RequstGoodsRequest request,
        final UserInfo userInfo) {
        userFacade.addRequstGoods(request, userInfo);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/dormant/cancel")
    @ApiOperation(value = "휴면계정 해지 요청")
    public ResponseEntity<Objects> cancelDormant(
        @RequestBody DormantCancelRequest request) {

        userFacade.cancelDormantAccount(request);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/do/dormant")
    @UserTokenValid
    @ApiOperation(value = "휴면계정 되기")
    public ResponseEntity<Object> changeDormantHuman(final UserInfo userInfo) {
        userFacade.doDormant(userInfo);
        return ResponseEntity.ok().build();
    }
}

