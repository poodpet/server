package com.pood.server.controller.user;

import com.pood.server.config.UserTokenValid;
import com.pood.server.controller.request.userwish.UserWishCreateDto;
import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.UserFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "유저 찜 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pood/v1-0/wish")
public class UserWishController {

    private final UserFacade userFacade;

    @PostMapping
    @ApiOperation(value = "유저의 찜 목록을 추가")
    public ResponseEntity<Object> create(@RequestHeader("token") final String token,
        @RequestBody final UserWishCreateDto userWishCreateDto) {
        userFacade.saveUserWish(token, userWishCreateDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @UserTokenValid
    @GetMapping("/{goodsIdx}")
    @ApiOperation(value = "유저의 찜 여부를 검증", notes = "header에 token이 필요합니다. (AOP로 token 검증)"
        + "userInfo는 aop로 부터 받아옵니다.")
    public ResponseEntity<Map<String, Boolean>> wishValidation(final UserInfo userInfo,
        @PathVariable("goodsIdx") final int goodsIdx) {
        return ResponseEntity.ok(
            Map.of(
                "isWished", userFacade.userWishValidation(userInfo, goodsIdx)
            )
        );
    }

    @DeleteMapping("/{goodsIdx}")
    @ApiOperation(value = "내 찜 삭제")
    public ResponseEntity<Object> deleteUserWish(@RequestHeader("token") final String token,
        @PathVariable("goodsIdx") final int goodsIdx) {
        userFacade.deleteUserWish(token, goodsIdx);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    @ApiOperation(value = "내 찜상품 전체 조회")
    public ResponseEntity<List<SortedGoodsList>> getProductList(
        @RequestHeader("token") final String token) {
        return ResponseEntity.ok(userFacade.getMyGoodsWishList(token));
    }
}
