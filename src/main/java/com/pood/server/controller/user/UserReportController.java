package com.pood.server.controller.user;

import com.pood.server.config.UserTokenValid;
import com.pood.server.controller.request.report.UserReportSaveRequest;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.ReportFacade;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/pood/v1-0/report")
@RequiredArgsConstructor
public class UserReportController {

    private final ReportFacade reportFacade;

    @PostMapping("/save")
    @UserTokenValid
    public ResponseEntity<Object> reportUser(@Valid @RequestBody final UserReportSaveRequest request,
        final UserInfo myInfo) {
        reportFacade.reportUser(request, myInfo);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
