package com.pood.server.controller;

import com.pood.server.controller.response.brand.BrandInfoResponse;
import com.pood.server.facade.BrandFacade;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
public class BrandController {

    private final BrandFacade brandFacade;

    @GetMapping("/api/pood/v1-0/brand/{brandIdx}")
    @ApiOperation(value = "상품의 브랜드 정보 출력", notes = "상품의 브랜드 정보를 조회합니다.")
    public ResponseEntity<BrandInfoResponse> getBrandInfo(
        @PathVariable final int brandIdx) {
        return ResponseEntity.ok(brandFacade.getProductBrandInfo(brandIdx));
    }

}
