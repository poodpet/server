package com.pood.server.controller;

import com.pood.server.controller.response.pet.UserPetTotalCountResponse;
import com.pood.server.dto.meta.main.HomeDto;
import com.pood.server.entity.meta.AppInitConfig;
import com.pood.server.entity.meta.IntroMarketingImage;
import com.pood.server.service.HoliyDaySeparate;
import com.pood.server.service.MainFacade;
import com.pood.server.service.MainService;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
public class MainController {

    private final MainService mainService;
    private final MainFacade mainFacade;
    private final HoliyDaySeparate holiyDaySeparate;

    @Deprecated
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/api/pood/main/home/{pcIdx}")
    @ApiOperation(value = "진행중인 이벤트 프로모션 조회", notes = "진행중인 이벤트 이벤트 프로모션 조회 정보를 출력 합니다.")
    public ResponseEntity<List<HomeDto.HomeEventAndPromotionDto>> getHomeDataList(
        @NotNull @PathVariable Integer pcIdx) {
        return ResponseEntity.ok(mainService.getHomeEventAndPromotionDataList(pcIdx));
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/api/pood/main/home/pet-total-count")
    @ApiOperation(value = "현재 등록되어 있는 펫의 수 표시", notes = "현재 등록되어 있는 펫의 수 표시를 출력 합니다.")
    public ResponseEntity<UserPetTotalCountResponse> getPetTotalCount() {
        return ResponseEntity.ok(mainFacade.getPetTotalCount());
    }


    @Deprecated
    @GetMapping("/api/pood/main/pood-policy")
    @ApiOperation(value = "포인트 안내 문구 + fcm key ", notes = "포인트 안내 문구를 출력 합니다.")
    public ResponseEntity<HashMap<String, String>> getTempPointText() {
        HashMap<String, String> temp = new HashMap<>();
        temp.put("savePointPercent", "0.02");
        temp.put("saveReviewPoint", "300");
        temp.put("saveFirstReviewPoint", "700");
        temp.put("savePhotoReviewPoint", "500");
        temp.put("deliveryFee", "0");
        temp.put("deliveryDayClosingTime", "14");
        temp.put("card_image",
            "https://pood-bucket.s3.ap-northeast-2.amazonaws.com/card_policy/cardpolicy.jpg");

        temp.put("apiKey", "AIzaSyBnNOZobyLNW9RZu8a_glcXrSA90w2QAdA");
        temp.put("appIdAndroid", "1:843874168533:android:ecd6766009407dbda07c5c");
        temp.put("appIdIOS", "1:843874168533:ios:fe277ffa58ce35dea07c5c");
        temp.put("messagingSenderId", "843874168533");
        temp.put("projectId", "pood-301802");

        temp.put("pathDeal", "/TunM/9b3c80c8");
        temp.put("pathEvent", "/TunM/3ffea52f");
        temp.put("pathPromotion", "/TunM/cd009205");
        temp.put("inviteTempId", "65097");

        return ResponseEntity.ok(temp);
    }

    @GetMapping("/api/pood/main/policy")
    @ApiOperation(value = "포인트 안내 문구 + fcm key ", notes = "포인트 안내 문구를 출력 합니다.")
    public ResponseEntity<Map<String, String>> getTempPointTextAll() {
        final List<AppInitConfig> appInitConfigList = mainService.findAll();

        return ResponseEntity.ok(appInitConfigList.stream()
            .collect(Collectors.toMap(AppInitConfig::getGuideKey, AppInitConfig::getValue)));
    }

    @Deprecated
    @GetMapping("/api/pood/main/temp/magagin")
    @ApiOperation(value = "임시 매거진 리스트", notes = "임시 매거진 리스트를 출력 합니다.")
    public ResponseEntity<List<HashMap<Object, Object>>> getTempMagazineList() {
        List<HashMap<Object, Object>> tempList = new ArrayList<>();
        HashMap<Object, Object> temp1 = new HashMap<>();
        temp1.put("idx", 1);
        temp1.put("url",
            "https://pood-bucket.s3.ap-northeast-2.amazonaws.com/pood_image/20211115182723027-46ade09d-feba-450a-8a24-bb0c23620802.png");
        HashMap<Object, Object> temp2 = new HashMap<>();
        temp2.put("idx", 2);
        temp2.put("url",
            "https://pood-bucket.s3.ap-northeast-2.amazonaws.com/pood_image/20211115182732027-b5c0916b-619b-4439-a4cb-f16efc780fbc.png");
        tempList.add(temp1);
        tempList.add(temp2);
        return ResponseEntity.ok(tempList);
    }

    @Deprecated
    @GetMapping("/api/pood/main/temp/magagin/{idx}")
    @ApiOperation(value = "임시 매거진 상세 보기", notes = "임시 매거진 상세 보기 정보를 출력 합니다.")
    public ResponseEntity<HashMap<Object, Object>> getTempMagazineDetail(
        @NotNull @PathVariable Integer idx) {
        HashMap<Object, Object> temp = new HashMap<>();
        temp.put("idx", idx);
        if (idx.equals(1)) {
            temp.put("url",
                "https://pood-bucket.s3.ap-northeast-2.amazonaws.com/pood_image/20211115182746027-752acb78-6d5c-4c01-8675-4a5212d18013.png");
        } else {
            temp.put("url",
                "https://pood-bucket.s3.ap-northeast-2.amazonaws.com/pood_image/20211115182757027-d35a2c8e-33a6-4ac1-ae75-a9b35216cdd3.png");
        }
        return ResponseEntity.ok(temp);
    }

    @GetMapping("/api/pood/main/push-alarm/count/{idx}")
    @ApiOperation(value = "푸시를 통한 이동 카운트 + 1", notes = "푸시를 통한 이동 카운트 + 1 합니다.")
    public ResponseEntity<Object> plusPushAlarmViewCount(@NotNull @PathVariable Long idx) {
        mainService.plusPushAlarmViewCount(idx);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/api/pood/v1-0/main/marketing/image")
    @ApiOperation(value = "인트로 이미지 리스트 출력", notes = "인트로 이미지 리스트 출력 합니다.")
    public ResponseEntity<List<IntroMarketingImage>> getInitMarketingImageList() {
        return ResponseEntity.ok(mainFacade.getActiveIntroMarketingImageList());
    }

    @Cacheable(value = "getHoliyday")
    @ApiOperation(value = "휴일 정보 전체 조회", notes = "휴일 정보 리스트를 출력 합니다.")
    @GetMapping("/api/pood/main/holiyday-list")
    public ResponseEntity<List<String>> getHoliyDayList() {
        return ResponseEntity.ok(holiyDaySeparate.getHolidayList());
    }


}
