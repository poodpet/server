package com.pood.server.controller.notice;

import com.pood.server.facade.notice.NoticeFacade;
import com.pood.server.facade.notice.response.NoticeDetailResponse;
import com.pood.server.facade.notice.response.NoticePageResponse;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pood/v1-0/notice")
public class NoticeController {

    private final NoticeFacade noticeFacade;

    @GetMapping
    @ApiOperation(value = "공지사항 리스트 출력", notes = "공지사항 리스트 출력를 조회합니다.")
    public ResponseEntity<Page<NoticePageResponse>> getNoticeList(
        @PageableDefault(sort = "idx", direction = Sort.Direction.DESC) final Pageable pageable) {
        return ResponseEntity.ok(noticeFacade.getNoticePage(pageable));
    }

    @GetMapping("/{idx}")
    @ApiOperation(value = "공지사항 상세 정보 조회", notes = "공지사항 상세 정보를 조회합니다.")
    public ResponseEntity<NoticeDetailResponse> getNoticeInfo(
        @PathVariable final int idx) {
        return ResponseEntity.ok(noticeFacade.getNoticeDetail(idx));
    }

    @GetMapping("/popup")
    @ApiOperation(value = "공지사항 팝업 조회", notes = "공지사항  팝업 정보를 조회합니다.")
    public ResponseEntity<NoticeDetailResponse> getNoticePopupInfo() {
        return ResponseEntity.ok(noticeFacade.getNoticePopupInfo());
    }

}
