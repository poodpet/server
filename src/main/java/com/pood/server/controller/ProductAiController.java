package com.pood.server.controller;

import com.pood.server.config.UserTokenValid;
import com.pood.server.dto.meta.goods.GoodsDto;
import com.pood.server.dto.meta.pet.PetDto;
import com.pood.server.dto.meta.product.ProductDto;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.service.ProductAiService;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
public class ProductAiController {

    private final ProductAiService productAiService;

    @UserTokenValid
    @GetMapping("/api/pood/ai/product/{userPetIdx}/{goodsIdx}")
    @ApiOperation(value = "내 펫의 해당 상품의 맞춤 정보 조회", notes = "내 펫의 해당 상품의 맞춤 솔루션 정보를 출력 합니다.")
    public ResponseEntity<List<ProductDto.AiSolutionProduct>> getAIGoodsSolution(
        @PathVariable final Integer userPetIdx, @PathVariable final Integer goodsIdx,
        final UserInfo userInfo) {
        return ResponseEntity.ok(
            productAiService.getAIGoodsSolution(userInfo, userPetIdx, goodsIdx));
    }

    @UserTokenValid
    @GetMapping("/api/pood/ai/my-pet/{userPetIdx}")
    @ApiOperation(value = "내 펫의 유전 질병 조회", notes = "내 펫의 유전 정보 질병 정볼르 출력 합니다.")
    public ResponseEntity<PetDto.userPetSolutionDto> getMyPetAIHereditySolution(
        final @PathVariable Integer userPetIdx,
        final UserInfo userInfo
    ) {
        return ResponseEntity.ok(productAiService.getMyPetAIHereditySolution(userInfo, userPetIdx));
    }

    @UserTokenValid
    @GetMapping("/api/pood/ai/my-pet/life/recommend/{userPetIdx}")
    @ApiOperation(value = "내 펫의 생애 주기 상품 리스트 조회", notes = "내 펫의 추천 생애 주기 상품 리스트를 출력 합니다.")
    public ResponseEntity<List<GoodsDto.SortedGoodsList>> getLifeRecommendDeal(
        @PathVariable final Integer userPetIdx,
        @RequestParam(defaultValue = "3", value = "size") final int size,
        final UserInfo userInfo) {
        return ResponseEntity.ok(productAiService.getLifeRecommendDeal(userInfo, userPetIdx, size));
    }

    @UserTokenValid
    @GetMapping("/api/pood/ai/my-pet/worry/recommend/{userPetIdx}")
    @ApiOperation(value = "내 펫의 걱정 거리 상품 리스트 조회", notes = "내 펫의 추천 딜 상품 리스트를 출력 합니다.")
    public ResponseEntity getWorryRecommendDeal(@PathVariable final Integer userPetIdx,
        final UserInfo userInfo) {
        return ResponseEntity.ok(productAiService.getWorryRecommendDeal(userInfo, userPetIdx));
    }

    @UserTokenValid
    @GetMapping("/api/pood/ai/my-pet/snack/{userPetIdx}")
    @ApiOperation(value = "내 펫의 추천 딜 스낵 상품 리스트 조회", notes = "내 펫의 추천 딜 스낵 상품 리스트를 출력 합니다.")
    public ResponseEntity<List<GoodsDto.SortedGoodsList>> getAiSnack(
        @PathVariable final Integer userPetIdx,
        @RequestParam(defaultValue = "5", value = "size") final int size,
        final UserInfo userInfo) {
        return ResponseEntity.ok(productAiService.getAiSnack(userInfo, userPetIdx, size));
    }

    @UserTokenValid
    @GetMapping("/api/pood/ai/my-pet/supplies/{userPetIdx}")
    @ApiOperation(value = "내 펫의 추천 용품 상품 리스트 조회", notes = "내 펫의 추천 용품 상품 리스트를 출력 합니다.")
    public ResponseEntity<List<GoodsDto.SortedGoodsList>> getAiSupplies(
        @PathVariable final Integer userPetIdx, final UserInfo userInfo) {
        return ResponseEntity.ok(productAiService.getAiSupplies(userInfo, userPetIdx));
    }

    @UserTokenValid
    @GetMapping("/api/pood/ai/my-pet/nutrients/{userPetIdx}")
    @ApiOperation(value = "내 펫의 추천 영양제 리스트 조회", notes = "내 펫의 추천 영양제 리스트 를 출력 합니다.")
    public ResponseEntity<List<GoodsDto.SortedGoodsList>> getAiNutrients(
        @PathVariable final Integer userPetIdx,
        @RequestParam(defaultValue = "5", value = "size") final int size,
        final UserInfo userInfo) {
        return ResponseEntity.ok(productAiService.getAiNutrients(userInfo, userPetIdx, size));
    }

    @UserTokenValid
    @GetMapping("/api/pood/ai/my-pet/feed/{userPetIdx}")
    @ApiOperation(value = "내 펫의 추천 딜 사료 상품 리스트 조회", notes = "내 펫의 추천 딜 사료 상품 리스트를 출력 합니다.")
    public ResponseEntity<List<GoodsDto.SortedGoodsList>> getAiFeed(
        @PathVariable final Integer userPetIdx, final UserInfo userInfo) {
        return ResponseEntity.ok(productAiService.getAiFeedSolution(userInfo, userPetIdx));
    }

    @GetMapping("/api/pood/ai/random/feed/{pcIdx}")
    @ApiOperation(value = "내 펫의 추천 딜 사료 상품 리스트 조회", notes = "내 펫의 추천 딜 사료 상품 리스트를 출력 합니다.")
    public ResponseEntity<List<GoodsDto.SortedGoodsList>> getAiRandomFeed(
        @PathVariable final Integer pcIdx,
        @RequestParam(defaultValue = "5", value = "size") final int size) {
        return ResponseEntity.ok(productAiService.getAiRandomGoods(0, pcIdx, size));
    }

    @GetMapping("/api/pood/ai/random/nutrients/{pcIdx}")
    @ApiOperation(value = "내 펫의 추천 딜 영양제 상품 리스트 조회", notes = "내 펫의 추천 딜 사료 영양제 리스트를 출력 합니다.")
    public ResponseEntity<List<GoodsDto.SortedGoodsList>> getAiRandomNutrients(
        @PathVariable final Integer pcIdx,
        @RequestParam(defaultValue = "5", value = "size") final int size) {
        return ResponseEntity.ok(productAiService.getAiRandomGoods(2, pcIdx, size));
    }

    @GetMapping("/api/pood/ai/random/supplies/{pcIdx}")
    @ApiOperation(value = "내 펫의 추천 딜 용품 리스트 조회", notes = "내 펫의 추천 딜 사료 용품 리스트를 출력 합니다.")
    public ResponseEntity<List<GoodsDto.SortedGoodsList>> getAiRandomSupplies(
        @PathVariable Integer pcIdx,
        @RequestParam(defaultValue = "5", value = "size") final int size) {
        return ResponseEntity.ok(productAiService.getAiRandomSuppliesGoods(3, pcIdx, size));
    }

    @GetMapping("/api/pood/ai/random/snack/{pcIdx}")
    @ApiOperation(value = "내 펫의 추천 딜 간식 상품 리스트 조회", notes = "내 펫의 추천 딜 간식 상품 리스트를 출력 합니다.")
    public ResponseEntity<List<GoodsDto.SortedGoodsList>> getAiRandomSnack(
        @PathVariable Integer pcIdx,
        @RequestParam(defaultValue = "5", value = "size") final int size) {
        return ResponseEntity.ok(productAiService.getAiRandomGoods(1, pcIdx, size));
    }

    @UserTokenValid
    @GetMapping("/api/pood/v1-0/ai/my-pet/worry/recommend/nutrients/{userPetIdx}")
    @ApiOperation(value = "내 펫의 고민거리 영양제 상품 리스트 조회", notes = "내 펫의 고민거리 영양제 상품 리스트를 출력 합니다.")
    public ResponseEntity getWorryRecommendNutrientsDeal(@PathVariable final Integer userPetIdx,
        final UserInfo userInfo) {
        return ResponseEntity.ok(
            productAiService.getWorryRecommendNutrientsDeal(userInfo, userPetIdx));
    }

    @UserTokenValid
    @GetMapping("/api/pood/v1-0/ai/my-pet/nutrients/{userPetIdx}")
    @ApiOperation(value = "내 펫의 추천 용품 상품 리스트 조회", notes = "내 펫의 추천 용품 상품 리스트를 출력 합니다.")
    public ResponseEntity<List<GoodsDto.SortedGoodsList>> getPoodHomeRecommendNutrientsDeal(
        @PathVariable final Integer userPetIdx,
        final UserInfo userInfo) {
        return ResponseEntity.ok(
            productAiService.getPoodHomeRecommendNutrientsDeal(userInfo, userPetIdx));
    }


}

