package com.pood.server.controller.request.user;

import com.pood.server.config.AES256;
import com.pood.server.entity.user.UserBaseImage;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.util.UserStatus;
import java.time.LocalDateTime;
import java.util.UUID;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserSignUpRequest {

    private static final String USER_GRADE = "WELCOME";
    private static final int EMAIL_TYPE = 5;

    @NotNull(message = "로그인 타입은 비어있거나 null일 수 없습니다.")
    private Integer loginType;

    @Email(regexp = "^[0-9a-zA-Z._-]*@[a-zA-Z-_.]*.[a-zA-Z]{2,3}$"
        , message = "이메일 주소 형식이 잘못되었습니다.")
    @NotNull(message = "이메일 주소가 null일 수 없습니다.")
    private String email;

    private String password;

    @NotEmpty(message = "sns key는 비어있거나 null일 수 없습니다.")
    private String snsKey;

    private String recommendCode;

    @NotNull(message = "서비스 이용약관은 비어있거나 null일 수 없습니다.")
    private boolean serviceAgree;

    @NotNull(message = "개인정보처리방침은 비어있거나 null일 수 없습니다.")
    private boolean privatePolicy;

    private boolean dogPoodPush;

    @NotNull(message = "마케팅 동의는 비어있거나 null일 수 없습니다.")
    private boolean adAgreement;

    private boolean catPoodPush;

    @NotEmpty(message = "휴대폰 번호는 비어있거나 null일 수 없습니다.")
    private String phoneNumber;

    @NotEmpty(message = "유저 이름은 비어있거나 null일 수 없습니다.")
    private String userName;

    public UserInfo toEntity(final String recommendCode, final UserBaseImage userBaseImage) {
        final LocalDateTime now = LocalDateTime.now();

        return UserInfo.builder()
            .userEmail(email)
            .userPassword(password)
            .userServiceAgree(serviceAgree ? 0 : 1)
            .userUuid(UUID.randomUUID().toString())
            .mlName(USER_GRADE)
            .mlRate(0.02d)
            .mlPrice(0)
            .mlMonth(0)
            .userPhone(phoneNumber)
            .referralCode(recommendCode)
            .userPoint(0)
            .userName(userName)
            .userNickname(userName)
            .userStatus(UserStatus.ACTIVE)
            .poodPush(isPoodPush())
            .poodPushCgTime(now)
            .orderPush(true)
            .orderPushCgTime(now)
            .servicePush(true)
            .servicePushCgTime(now)
            .dogPoodPush(dogPoodPush)
            .dogPoodPushCgTime(now)
            .catPoodPush(catPoodPush)
            .catPoodPushCgTime(now)
            .adAgreement(adAgreement)
            .adAgreementCgTime(now)
            .userBaseImage(userBaseImage)
            .statusUpdateDate(now)
            .build();
    }

    private boolean isPoodPush() {
        return dogPoodPush || catPoodPush;
    }

    public void insertPassword(final String password) {
        this.password = password;
    }

    public void insertPassword() throws Exception {

        this.password = AES256.aesEncode(getAesPasswordOrg());
    }

    private String getAesPasswordOrg() {
        if (isEmailType()) {
            return getPassword();
        }
        return getPhoneNumber();
    }

    private boolean isEmailType() {
        return loginType == EMAIL_TYPE;
    }

}
