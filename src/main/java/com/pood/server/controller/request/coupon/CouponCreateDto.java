package com.pood.server.controller.request.coupon;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CouponCreateDto {

    @NotNull(message = "code는 null일 수 없습니다.")
    @ApiModelProperty(value = "쿠폰 코드")
    private String code;
}
