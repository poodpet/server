package com.pood.server.controller.request.userpet;

import java.time.LocalDate;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class UserPetCreateDto {

    private int pcId;

    private int pscId;

    private int petActivity;

    private String petName;

    private LocalDate petBirth;

    private int petGender;

    private float petWeight;

    private int petStatus;

    @Size(max = 3, message = "최소 0 에서 최대 3개 까지만 고를 수 있습니다.")
    @NotNull(message = "고민 선택 리스트가 null 일 수 없습니다.")
    private List<PetWorryRequest> petWorryList;

    @Size(max = 3, message = "최소 0 에서 최대 3개 까지만 고를 수 있습니다.")
    @NotNull(message = "알갱이 사이즈 리스트가 null 일 수 없습니다.")
    private List<Integer> grainSizeList;

    @Size(max = 3, message = "최소 0 에서 최대 3개 까지만 고를 수 있습니다.")
    @NotNull(message = "알러지 리스트가 null 일 수 없습니다.")
    private List<Integer> allergyList;

    private int otherFeedIdx;

}
