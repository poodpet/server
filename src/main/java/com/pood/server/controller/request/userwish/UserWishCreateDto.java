package com.pood.server.controller.request.userwish;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserWishCreateDto {

    private int goodsIdx;

}
