package com.pood.server.controller.request.userpet;

import java.util.List;
import java.util.Set;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class UserPetUpdateRequest {

    private int userPetIdx;

    @NotNull(message = "펫이름이 없습니다.")
    private String petName;

    @NotNull(message = "펫 성별은 null일 수 없습니다.")
    private Integer petGender;

    @Size(min = 1, max = 3, message = "고민은 1가지에서 3가지가 존재해야 합니다.")
    @NotNull(message = "고민은 최소 1개 있어야 합니다.")
    private List<PetWorryRequest> petWorryList;

    @Size(max = 3, message = "알러지는 3가지가 최대입니다.")
    private Set<Integer> allergyList;

    @Size(max = 3, message = "알갱이 사이즈는 3가지가 최대입니다.")
    private Set<Integer> grainSizeList;

}
