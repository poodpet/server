package com.pood.server.controller.request.goods;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Value;

@Value(staticConstructor = "of")
public class GoodsFilterRequest {

    @ApiModelProperty(value = "petWorryIdx를 문자열로 ex) '1' ")
    @Size(min = 1, message = "고민거리는 최소 1가지 선택해야 합니다.")
    List<String> petWorryList;

    @ApiModelProperty(value = "딜 대분류 (사료, 간식, 영양제 등)")
    @NotNull(message = "대분류 카테고리가 없습니다.")
    Long goodsCtIdx;

    @ApiModelProperty(value = "딜 소분류 (딜 대분류의 세부 카테고리 ex) 건식사료, 습식사료 등등) null은 전체")
    Long goodsSubCtIdx;

    @ApiModelProperty(value = "main_property 주 단백질")
    @Size(min = 1, message = "단백질은 최소 1가지 선택해야 합니다.")
    List<String> proteinList;

    @ApiModelProperty(value = "알갱이 사이즈")
    @Size(min = 1, message = "알갱이는 최소 1가지 선택해야 합니다.")
    List<Integer> grainSizeList;

    @ApiModelProperty(value = "반려견 체구 0:(크기 상관없이 모두 먹기가능) 1(소형견) 2(중형견) 3(대형견)"
        + "강아지만 적용되는 필터")
    @Size(min = 1, message = "체구 리스트는 1가지 선택해야 합니다.")
    List<Integer> feedTargetList;

    @ApiModelProperty(value = "펫 카테고리 idx")
    @NotNull(message = "펫 카테고리는 필수 값 입니다.")
    Integer pcIdx;

    @ApiModelProperty(value = "반려동물의 연령대")
    @Size(min = 1, message = "연령 선택은 최소 1가지 선택해야 합니다.")
    List<Integer> ageTypeList;

    public void addFreeSize() {
        if (Objects.nonNull(feedTargetList)) {
            feedTargetList.add(0);
        }
    }

}
