package com.pood.server.controller.request.review;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class ReviewBlockAndSueDto {

    private int reviewIdx;
    private String text;

}
