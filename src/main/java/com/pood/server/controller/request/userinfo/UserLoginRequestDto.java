package com.pood.server.controller.request.userinfo;

import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class UserLoginRequestDto {

    private String email;

    private String password;

    /**
     * 앱 쪽에서 로그인 요청을 보낼 때 SNS, 기본 로그인 여부에 따라 값을 주지만, <br> 실제로 데이터를 검증하는 로직은 존재하지 않음.<br> 앱에서 데이터를
     * 한번에 처리해주기 위해 값을 내려주기만 하는 값
     *
     * @since 2022-02-23
     */
    @NotNull(message = "로그인 유형은 필수 값 입니다.")
    private Integer loginType;

    @NotNull(message = "접속기기 유형은 필수 값 입니다.")
    private Integer osType;
}
