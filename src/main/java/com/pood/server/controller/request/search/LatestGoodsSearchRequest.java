package com.pood.server.controller.request.search;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.constraints.Size;
import lombok.Getter;

@Getter
public class LatestGoodsSearchRequest {

    @Size(max = 10, message = "검색 상품 개수는 10개 이상일 수 없습니다.")
    @ApiModelProperty(value = "상품 idx 리스트")
    private List<LatestGoodsSearch> latestGoodsSearch;

    public List<Integer> getGoodsIdxList() {
        return latestGoodsSearch.stream()
            .map(LatestGoodsSearch::getIdx)
            .collect(Collectors.toList());
    }

    public int getPriorityByIdx(final int idx) {
        return latestGoodsSearch.stream()
            .filter(search -> search.getIdx().equals(idx))
            .map(LatestGoodsSearch::getPriority)
            .findAny()
            .orElse(0);
    }

    @Getter
    static class LatestGoodsSearch {

        private Integer idx;
        private Integer priority;

    }

}
