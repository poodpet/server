package com.pood.server.controller.request.userinfo;

import com.pood.server.entity.user.UserBaseImage;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserToken;
import java.time.LocalDateTime;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UserLoginResponseDto {

    int idx;
    String referralCode;
    String userEmail;
    String userNickname;
    String userUuid;

    String userName;
    String userPhone;
    int userLoginType;
    LocalDateTime recordbirth;
    String mlName;
    double mlRate;
    Integer userServiceAgree;
    boolean servicePush;
    boolean poodPush;
    boolean orderPush;
    boolean catPoodPush;
    boolean dogPoodPush;
    String token;
    UserBaseImage userBaseImage;

    @Builder(access = AccessLevel.PRIVATE)
    private UserLoginResponseDto(final int idx, final String referralCode, final String userEmail,
        final String userNickname, final String userUuid, final String userName,
        final String userPhone, final int userLoginType,
        final LocalDateTime recordbirth, final String mlName, final double mlRate,
        final Integer userServiceAgree,
        final boolean servicePush, final boolean poodPush, final boolean orderPush,
        final boolean catPoodPush,
        final boolean dogPoodPush, final String token, final UserBaseImage userBaseImage) {
        this.idx = idx;
        this.referralCode = referralCode;
        this.userEmail = userEmail;
        this.userNickname = userNickname;
        this.userUuid = userUuid;
        this.userName = userName;
        this.userPhone = userPhone;
        this.userLoginType = userLoginType;
        this.recordbirth = recordbirth;
        this.mlName = mlName;
        this.mlRate = mlRate;
        this.userServiceAgree = userServiceAgree;
        this.servicePush = servicePush;
        this.poodPush = poodPush;
        this.orderPush = orderPush;
        this.catPoodPush = catPoodPush;
        this.dogPoodPush = dogPoodPush;
        this.token = token;
        this.userBaseImage = userBaseImage;
    }

    public static UserLoginResponseDto toResponse(final UserInfo userInfo,
        final UserToken userToken, final int loginType) {
        return UserLoginResponseDto.builder()
            .idx(userInfo.getIdx())
            .referralCode(userInfo.getReferralCode())
            .userEmail(userInfo.getUserEmail())
            .userNickname(userInfo.getUserNickname())
            .userUuid(userInfo.getUserUuid())
            .userName(userInfo.getUserName())
            .userPhone(userInfo.getUserPhone())
            .userLoginType(loginType)
            .recordbirth(userInfo.getRecordbirth())
            .mlName(userInfo.getMlName())
            .mlRate(userInfo.getMlRate())
            .userServiceAgree(userInfo.getUserServiceAgree())
            .servicePush(userInfo.getServicePush())
            .poodPush(userInfo.getPoodPush())
            .orderPush(userInfo.getOrderPush())
            .catPoodPush(userInfo.isCatPoodPush())
            .dogPoodPush(userInfo.isDogPoodPush())
            .token(userToken.getToken())
            .userBaseImage(userInfo.getUserBaseImage())
            .build();
    }
}