package com.pood.server.controller.request.event;

import com.pood.server.entity.user.UserEventCustomBlock;
import com.pood.server.entity.user.UserInfo;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class EventBlockRequest {

    @NotNull(message = "이벤트 정보는 null일 수 없습니다.")
    private Integer eventInfoIdx;

    @NotEmpty(message = "차단 메세지는 비어있거나 null일 수 없습니다.")
    private String blockMessage;

    @NotNull(message = "유저 idx는 null일 수 없습니다.")
    private Integer userInfoIdx;

    public UserEventCustomBlock toEntity(final UserInfo blockUser, final UserInfo selfInfo) {
        return UserEventCustomBlock.of(eventInfoIdx, blockUser, blockMessage, selfInfo.getIdx());
    }
}
