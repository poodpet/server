package com.pood.server.controller.request.userinfo;

import com.pood.server.entity.DeviceType;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserLoginRequest {

    private String email;

    private String password;

    @NotNull(message = "로그인 유형은 필수 값 입니다.")
    private Integer loginType;

    @NotNull(message = "접속기기 유형은 필수 값 입니다.")
    private DeviceType osType;
}
