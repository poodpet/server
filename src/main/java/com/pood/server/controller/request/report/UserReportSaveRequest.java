package com.pood.server.controller.request.report;

import com.pood.server.entity.user.UserEventReport;
import com.pood.server.entity.user.UserInfo;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class UserReportSaveRequest {

    @NotNull(message = "유저의 idx는 null일 수 없습니다.")
    private Integer userInfoIdx;

    @NotNull(message = "이벤트 idx는 null일 수 없습니다.")
    private Integer eventInfoIdx;

    @NotEmpty(message = "신고 메세지는 null 이거나 빈 문자열일 수 없습니다.")
    private String reportMessage;

    public UserEventReport toEntity(final UserInfo userInfo, final int myInfoIdx) {
        return UserEventReport.of(eventInfoIdx, userInfo, reportMessage, myInfoIdx);
    }
}
