package com.pood.server.controller.request.reviewclap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class ReviewClapCreateDto {

    private int reviewIdx;

}
