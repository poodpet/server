package com.pood.server.controller.request.user;

import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class UserInfoUpdateRequest {

    @NotEmpty(message = "uuid는 비어있거나 null일 수 없습니다.")
    private String userUuid;
    private String userNickName;
    private String userPhone;

}
