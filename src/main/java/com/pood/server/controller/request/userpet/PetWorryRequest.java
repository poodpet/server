package com.pood.server.controller.request.userpet;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PetWorryRequest {

    private Long idx;
    private Integer priority;

}
