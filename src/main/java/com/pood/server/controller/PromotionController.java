package com.pood.server.controller;

import com.pood.server.controller.response.promotion.RunningPromotionResponse;
import com.pood.server.dto.meta.goods.PromotionDto;
import com.pood.server.facade.PromotionFacade;
import com.pood.server.service.PromotionServiceV2;
import io.swagger.annotations.ApiOperation;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequiredArgsConstructor
public class PromotionController {

    private final PromotionServiceV2 promotionService;
    private final PromotionFacade promotionFacade;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/api/pood/promotion/{idx}")
    @ApiOperation(value = "프로모션 상세 조회", notes = "프로모션 조회 합니다.")
    public ResponseEntity<PromotionDto.PromotionGoodsListInfo> getPromotionDetail(
        @NotNull @PathVariable Integer idx) {
        PromotionDto.PromotionGoodsListInfo promotionGoodsListInfo = promotionService.getPromotionDetail(
            idx);
        return ResponseEntity.ok(promotionGoodsListInfo);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/api/pood/v1-0/promotion/{idx}")
    @ApiOperation(value = "프로모션 상세 조회(v1-0)", notes = "프로모션 조회 합니다.")
    public ResponseEntity<PromotionDto.PromotionGoodsListInfo> getPromotionDetailV10(
        @PathVariable final int idx) {
        return ResponseEntity.ok(promotionFacade.getPromotionDetail(idx));
    }

    @GetMapping("/api/pood/v1-0/promotion/all/{pcIdx}")
    @ApiOperation(value = "진행중인 프로모션 전체 조회")
    public ResponseEntity<Object> proceedingPromotion(@PathVariable
    @Min(value = 1, message = "펫 카테고리는 0 이하일 수 없습니다.")
    @Max(value = 2, message = "펫 카테고리는 3 이상일 수 없습니다.") final int pcIdx) {
        return ResponseEntity.ok(
            RunningPromotionResponse.of(promotionFacade.proceedingPromotion(pcIdx)));
    }

}

