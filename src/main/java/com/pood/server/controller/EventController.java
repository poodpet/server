package com.pood.server.controller;

import com.pood.server.config.UserTokenValid;
import com.pood.server.controller.response.event.DonationEventListResponse;
import com.pood.server.controller.response.event.RunningEvent;
import com.pood.server.controller.response.event.photoaward.AwardParticipateResponse;
import com.pood.server.controller.response.event.photoaward.EndPhotoAwardDetailResponse;
import com.pood.server.controller.response.event.photoaward.EndPhotoAwardResponse;
import com.pood.server.controller.response.event.photoaward.EndPhotoAwardWinner;
import com.pood.server.dto.meta.event.EndEvent;
import com.pood.server.dto.meta.event.EventDto;
import com.pood.server.dto.meta.event.EventDto.MyEventList;
import com.pood.server.dto.meta.event.RunningPhotoAward;
import com.pood.server.entity.meta.EventPhoto;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.event.EventFacade;
import com.pood.server.facade.event.response.EventDonationResponse;
import com.pood.server.service.EventService;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Validated
@RestController
@RequiredArgsConstructor
public class EventController {

    private final EventService eventService;
    private final EventFacade eventFacade;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/api/pood/event/{petCategoryIdx}/all")
    @ApiOperation(value = "진행중인 이벤트 조회", notes = "진행중인 이벤트 조회 정보를 펫 별로 전체 출력 합니다.")
    public ResponseEntity<List<RunningEvent>> getProgressEvents(
        @PageableDefault(sort = "idx", direction = Sort.Direction.ASC) final Pageable pageable,
        @PathVariable @Min(value = 0, message = "펫 카테고리는 음수가 될 수 없습니다.")
        @Max(value = 2, message = "펫 카테고리는 3이상일 수 없습니다.") final Integer petCategoryIdx) {
        return ResponseEntity.ok(eventService.getProgressEvents(petCategoryIdx, pageable));
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/api/pood/event/{idx}")
    @ApiOperation(value = "이벤트 상세 조회", notes = "이벤트 상제 정보를 출력 합니다.")
    public ResponseEntity<EventDto.EventDetail> getEventDetail(
        @NotNull @PathVariable final Integer idx,
        final HttpServletRequest request) {
        String token = request.getHeader("token");
        EventDto.EventDetail detail = eventService.getEventDetail(idx, token);
        return ResponseEntity.ok(detail);
    }

    @GetMapping("/api/pood/event/donation/{idx}")
    @ApiOperation(value = "기부 이벤트 상세 조회", notes = "B타입(기부) 이벤트 상세 조회")
    public ResponseEntity<EventDonationResponse> getDonationEventDetail(
        @PathVariable final Integer idx) {

        return ResponseEntity.ok(eventFacade.getDonationEventDetail(idx));
    }


    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/api/pood/event/photo/{idx}")
    @ApiOperation(value = "이벤트 포토 전체 조회", notes = "이벤트 포토 정보를 출력 합니다.")
    public ResponseEntity<Page<EventDto.EventUserPhotoDto>> getEventPhotoImgList(
        @NotNull @PathVariable final Integer idx,
        @PageableDefault(sort = "recordbirth", direction = Sort.Direction.DESC) final Pageable pageable,
        final HttpServletRequest request) {
        final String token = request.getHeader("token");
        Page<EventDto.EventUserPhotoDto> all = eventService.getEventPhotoImgList(idx, pageable,
            token);
        return ResponseEntity.ok(all);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/api/pood/event/photo/img/upload")
    @ApiOperation(value = "이벤트 포토 등록", notes = "이벤트 포토 정보를 등록 합니다.")
    public ResponseEntity<EventPhoto> uploadEventPhotoImg(
        @RequestParam(value = "image", required = false) final MultipartFile files,
        @RequestParam(value = "event_idx", required = false) final Integer eventIdx
        , final HttpServletRequest request
    ) throws Exception {
        String token = request.getHeader("token");
        EventPhoto magazineImage = eventService.uploadEventPhotoImg(eventIdx, files, token);
        return ResponseEntity.ok(magazineImage);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @DeleteMapping("/api/pood/event/photo/img/{eventIdx}/{idx}")
    @ApiOperation(value = "이벤트 포토 삭제", notes = "이벤트 포토 정보를 삭제 합니다.")
    public ResponseEntity<Object> deleteEventPhotoImg(@NotNull @PathVariable final Integer eventIdx,
        @NotNull @PathVariable final Long idx, final HttpServletRequest request) {
        String token = request.getHeader("token");
        eventService.deleteEventPhotoImg(eventIdx, idx, token);
        return ResponseEntity.ok().build();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/api/pood/event/participation")
    @ApiOperation(value = "이벤트 참여", notes = "이벤트를 참여 합니다.")
    public ResponseEntity<Object> participationEvent(@RequestBody final EventDto.EventIdx EventIdx,
        final HttpServletRequest request) {
        String token = request.getHeader("token");
        eventService.participationEvent(EventIdx, token);
        return ResponseEntity.ok().build();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/api/pood/event/comment")
    @ApiOperation(value = "이벤트 댓글 등록", notes = "이벤트 댓글을 등록 합니다.")
    public ResponseEntity<Object> insertEventComment(
        @RequestBody final EventDto.EventComment eventComment,
        final HttpServletRequest request) {
        String token = request.getHeader("token");
        eventService.insertEventComment(eventComment, token);
        return ResponseEntity.ok().build();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/api/pood/event/comment/{idx}")
    @ApiOperation(value = "이벤트 댓글 조회", notes = "이벤트 상제 정보를 출력 합니다.")
    public ResponseEntity<Page<EventDto.EventUserCommentDto>> getEventCommentList(
        @NotNull @PathVariable final Integer idx,
        @PageableDefault(sort = "recordbirth", direction = Sort.Direction.DESC) final Pageable pageable,
        final HttpServletRequest request) {
        final String token = request.getHeader("token");
        Page<EventDto.EventUserCommentDto> all = eventService.getEventCommentList(idx, pageable,
            token);
        return ResponseEntity.ok(all);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @DeleteMapping("/api/pood/event/comment/{eventIdx}/{idx}")
    @ApiOperation(value = "이벤트 댓글 삭제", notes = "이벤트 댓글 정보를 삭제 합니다.")
    public ResponseEntity<Object> deleteEventComment(@NotNull @PathVariable final Integer eventIdx,
        @NotNull @PathVariable final Long idx, final HttpServletRequest request) {
        String token = request.getHeader("token");
        eventService.deleteEventComment(eventIdx, idx, token);
        return ResponseEntity.ok().build();
    }


    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/api/pood/event/photo/{eventIdx}/{userIdx}")
    @ApiOperation(value = "이벤트 포토 조회", notes = "이벤트 포토 정보를 출력 합니다.")
    public ResponseEntity<EventDto.EventPhotoUserDetail> getEventPhotoImgDetail(
        @NotNull @PathVariable final Integer eventIdx,
        @NotNull @PathVariable final Integer userIdx) {
        EventDto.EventPhotoUserDetail eventPhotoImgDetail = eventService.getEventPhotoImgDetail(
            eventIdx, userIdx);
        return ResponseEntity.ok(eventPhotoImgDetail);
    }


    @GetMapping("/api/pood/user/event-list")
    @ApiOperation(value = "내 참여 이벤트 (포토, 댓글, 버튼참여형) 리스트 조회", notes = "내 참여 이벤트 리스트를 조회합니다.")
    public ResponseEntity<List<MyEventList>> getMyEventList(final HttpServletRequest request) {
        String token = request.getHeader("token");
        return ResponseEntity.ok(eventService.getMyEventList(token));
    }

    @GetMapping("/api/pood/user/event/comment/{eventIdx}")
    @ApiOperation(value = "내 참여 댓글 이벤트 상세 조회", notes = "당첨내역 조회")
    public ResponseEntity<EventDto.MyCommentEventDetail> getMyCommentEvent(
        @NotNull @PathVariable final Integer eventIdx, final HttpServletRequest request) {
        String token = request.getHeader("token");
        EventDto.MyCommentEventDetail myCommentEvent = eventService.getMyCommentEvent(token,
            eventIdx);
        return ResponseEntity.ok(myCommentEvent);
    }

    @GetMapping("/api/pood/user/event/photo/{eventIdx}")
    @ApiOperation(value = "내 참여 포토 이벤트 상세 조회", notes = "포토 이벤트 당첨 내역 조회")
    public ResponseEntity<EventDto.MyPhotoEventDetail> getMyPhotoEvent(
        @NotNull @PathVariable final Integer eventIdx, final HttpServletRequest request) {
        String token = request.getHeader("token");
        EventDto.MyPhotoEventDetail myPhotoEvent = eventService.getMyPhotoEvent(token, eventIdx);
        return ResponseEntity.ok(myPhotoEvent);
    }

    @GetMapping("/api/pood/user/event/join/{eventIdx}")
    @ApiOperation(value = "내 참여 이벤트 상세 조회", notes = "이벤트 당첨 내역")
    public ResponseEntity<EventDto.MyJoinEventDetail> getMyJoinEvent(
        @NotNull @PathVariable final Integer eventIdx, final HttpServletRequest request) {
        String token = request.getHeader("token");
        EventDto.MyJoinEventDetail myJoinEvent = eventService.getMyJoinEvent(token, eventIdx);
        return ResponseEntity.ok(myJoinEvent);
    }

    @DeleteMapping("/api/pood/user/event/comment/{eventIdx}")
    @ApiOperation(value = "내 댓글 이벤트 삭제", notes = "내 댓글 이벤트 삭제 합니다.")
    public ResponseEntity<Object> deleteMyCommentEvent(
        @NotNull @PathVariable final Integer eventIdx,
        final HttpServletRequest request) {
        String token = request.getHeader("token");
        eventService.deleteEventComment(token, eventIdx);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/api/pood/user/event/photo/{eventIdx}")
    @ApiOperation(value = "내 사진 이벤트 삭제", notes = "내 사진 이벤트 삭제 합니다.")
    public ResponseEntity<Object> deleteMyPhotoEvent(@NotNull @PathVariable final Integer eventIdx,
        final HttpServletRequest request) {
        String token = request.getHeader("token");
        eventService.deleteEventPhoto(token, eventIdx);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/api/pood/user/event/join/{eventIdx}")
    @ApiOperation(value = "내 참여 이벤트 삭제", notes = "내 참여 이벤트 삭제 합니다.")
    public ResponseEntity<Object> deleteMyJoinEvent(@NotNull @PathVariable final Integer eventIdx,
        final HttpServletRequest request) {
        String token = request.getHeader("token");
        eventService.deleteEventJoin(token, eventIdx);
        return ResponseEntity.ok().build();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/api/pood/v1-1/event/{idx}")
    @ApiOperation(value = "이벤트 상세 조회(v1.1)", notes = "이벤트 상제 정보를 출력 합니다.")
    public ResponseEntity<EventDto.EventDetail> getEventDetailV10(
        @PathVariable final int idx, final HttpServletRequest request) {
        String token = request.getHeader("token");
        EventDto.EventDetail eventDetail = eventFacade.getEventDetail(idx, token);
        return ResponseEntity.ok(eventDetail);
    }

    @GetMapping("/api/pood/v1-0/event/end/{pcIdx}")
    @ApiOperation(value = "종료된 이벤트 조회")
    public ResponseEntity<Page<EndEvent>> findEndEvent(@PathVariable final int pcIdx,
        @PageableDefault final Pageable pageable) {
        return ResponseEntity.ok(eventFacade.findEndEvent(pageable, pcIdx));
    }

    @GetMapping("/api/pood/v1-0/event/photo-awards/winner")
    @ApiOperation(value = "포토 어워즈 명예의 전당", notes = "해당 년도의 1~12월까지의 포토 어워즈 우승작을 보여줍니다.")
    public ResponseEntity<List<EndPhotoAwardWinner>> findPhotoAwardsWinner(
        @RequestParam final int year) {
        return ResponseEntity.ok(eventFacade.findEndPhotoAwardWinners(year));
    }

    @GetMapping("/api/pood/v1-0/event/photo-awards")
    @ApiOperation(value = "종료된 포토 어워즈 목록 조회", notes = "해당 년도의 종료된 포토 어워즈 목록을 보여줍니다.")
    public ResponseEntity<List<EndPhotoAwardResponse>> findEndPhotoAwards(
        @RequestParam final int year) {
        return ResponseEntity.ok(eventFacade.findEndPhotoAwards(year));
    }

    @GetMapping("/api/pood/v1-0/event/photo-awards/{eventIdx}")
    @ApiOperation(value = "종료된 포토 어워즈 상세 조회")
    public ResponseEntity<EndPhotoAwardDetailResponse> findEndPhotoAwardDetail(
        @PathVariable final int eventIdx) {
        return ResponseEntity.ok(eventFacade.endPhotoAwardsDetail(eventIdx));
    }

    @GetMapping("/api/pood/v1-0/event/photo-awards/join/{eventIdx}")
    @ApiOperation(value = "종료된 포토 어워즈 상세 조회 참여작")
    public ResponseEntity<AwardParticipateResponse> awardParticipateList(
        @PathVariable final int eventIdx,
        @PageableDefault final Pageable pageable) {
        return ResponseEntity.ok(eventFacade.awardParticipateList(eventIdx, pageable));
    }

    @GetMapping("/api/pood/v1-0/event/photo-awards/running")
    @ApiOperation(value = "진행중인 포토 어워즈 idx")
    public ResponseEntity<RunningPhotoAward> runningAward() {
        return ResponseEntity.ok(eventFacade.runningPhotoAward());
    }

    @UserTokenValid
    @PostMapping("/api/pood/v1-0/event/participation/donation/{eventIdx}")
    @ApiOperation(value = "기부이벤트 참여")
    public ResponseEntity<Object> donationEvent(@PathVariable Integer eventIdx, UserInfo userInfo) {

        eventFacade.participateDonationEvent(eventIdx, userInfo);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/api/pood/v1-0/event/donation")
    @ApiOperation(value = "기부이벤트 목록 리스트 조회")
    public ResponseEntity<List<DonationEventListResponse>> getDonationList() {

        return ResponseEntity.ok(eventFacade.getDonationList());
    }
}
