package com.pood.server.controller.search;

import com.pood.server.controller.request.search.LatestGoodsSearchRequest;
import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.facade.goods.GoodsFacade;
import com.pood.server.facade.SearchFacade;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pood/v1-0-0/search")
public class SearchController {

    private final SearchFacade searchService;
    private final GoodsFacade goodsFacade;

    @GetMapping
    @ApiOperation(value = "검색어 필터 조회", notes = "펫 번호, 키워드로 상품을 검색합니다.")
    public ResponseEntity<List<String>> searchFilter(
        @RequestParam("pcIdx") final int petCategoryIndex,
        @RequestParam("keyWord") final String keyWord,
        @RequestParam(value = "goodsCt", required = false) final Long goodsCt) {

        return ResponseEntity.ok(
            searchService.searchCategoryAndKeyword(petCategoryIndex, keyWord, goodsCt));
    }

    @GetMapping("/feed-comparison")
    @ApiOperation(value = "사료비교 검색어 필터 조회", notes = "펫 번호, 키워드로 상품을 검색합니다.")
    public ResponseEntity<List<String>> searchFilterByFeedCompare(
        @RequestParam("pcIdx") final int petCategoryIndex,
        @RequestParam("keyWord") final String keyWord) {

        return ResponseEntity.ok(
            searchService.searchCategoryAndKeywordForFeedCompare(petCategoryIndex, keyWord));
    }

    @PostMapping("/latest-goods")
    @ApiOperation(value = "최근 검색된 상품 리스트 ", notes = "최근 검색된 상품 리스트 검색합니다.")
    public ResponseEntity<List<SortedGoodsList>> latestGoodsSearch(
        @Valid @RequestBody final LatestGoodsSearchRequest request) {
        return ResponseEntity.ok(goodsFacade.getSortedGoodsInIdxList(request));
    }

}
