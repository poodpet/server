package com.pood.server.controller.review;

import com.pood.server.api.mapper.user.review.ReviewRatingAndCountDto;
import com.pood.server.config.UserTokenValid;
import com.pood.server.controller.request.review.ReviewBlockAndSueDto;
import com.pood.server.dto.user.review.GoodsDetailReviewDto;
import com.pood.server.dto.user.review.GoodsImagesReviewDto;
import com.pood.server.dto.user.review.UserReviewSaveDto;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.ReviewFacade;
import com.pood.server.facade.review.request.ReviewTextUpdateRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Api(tags = "리뷰 API")
@RestController
@RequestMapping("/api/pood/v1-0/review")
@RequiredArgsConstructor
public class ReviewController {

    private final ReviewFacade reviewFacade;


    @GetMapping("/{goodsIdx}")
    @ApiOperation(value = "상품의 리뷰 리스트 출력", notes = "상품의 리뷰 리스트틀 출력합니다.")
    public ResponseEntity<Page<GoodsDetailReviewDto>> getGoodsReviewList(
        @PageableDefault(sort = "idx", direction = Sort.Direction.DESC) final Pageable pageable,
        @PathVariable final int goodsIdx, final HttpServletRequest request) {
        final String token = request.getHeader("token");

        return ResponseEntity.ok(reviewFacade.getGoodsReviewList(pageable, goodsIdx, token));
    }

    @GetMapping("/rating/{goodsIdx}")
    @ApiOperation(value = "리뷰의 총 개수와 평점 출력", notes = "리뷰의 총 개수와 평점 출력합니다.")
    public ResponseEntity<ReviewRatingAndCountDto> getReviewRating(
        @NotNull @PathVariable int goodsIdx) {
        return ResponseEntity.ok(reviewFacade.getReviewRating(goodsIdx));
    }

    @PostMapping
    @ApiOperation(value = "리뷰 작성", notes = "사용자 리뷰를 작성합니다.")
    public ResponseEntity<?> saveReview(@RequestHeader("token") final String token,
        @RequestPart("files") final List<MultipartFile> fileList,
        @RequestPart @Valid final UserReviewSaveDto userReviewSaveDto) {
        reviewFacade.saveUserReview(userReviewSaveDto, token, fileList);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/sue")
    @ApiOperation(value = "리뷰를 신고", notes = "리뷰를 신고합니다.")
    public ResponseEntity<Object> sue(@RequestBody final ReviewBlockAndSueDto reviewBlockAndSueDto,
        @RequestHeader("token") final String token) {
        reviewFacade.insertSue(reviewBlockAndSueDto, token);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PostMapping("/block")
    @ApiOperation(value = "리뷰 차단", notes = "리뷰를 차단합니다.")
    public ResponseEntity<Object> block(
        @RequestBody final ReviewBlockAndSueDto reviewBlockAndSueDto,
        @RequestHeader("token") final String token) {
        reviewFacade.insertBlock(reviewBlockAndSueDto, token);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("/images/{goodsIdx}")
    @ApiOperation(value = "상품 리뷰 사진 리스트 조회", notes = "상품 리뷰 사진 리스트틀 출력합니다.")
    public ResponseEntity<Page<GoodsImagesReviewDto>> getReviewPhotoImgList(
        @PageableDefault(sort = "idx", direction = Sort.Direction.DESC) final Pageable pageable,
        @PathVariable final int goodsIdx, final HttpServletRequest request) {
        final String token = request.getHeader("token");

        return ResponseEntity.ok(reviewFacade.getReviewPhotoImgListV2(goodsIdx, token, pageable));
    }

    @GetMapping("/is-possible")
    @UserTokenValid
    @ApiOperation(value = "리뷰 작성이 가능 여부", notes = "리뷰 작성이 가능 여부를 알려준다.")
    public ResponseEntity<Boolean> isReviewPossible(final UserInfo userInfo) {
        return ResponseEntity.ok(reviewFacade.isReviewPossible(userInfo));
    }

    @PatchMapping
    @UserTokenValid
    @ApiOperation(value = "사용자 리뷰 글 수정", notes = "사용자 리뷰의 글을 수정합니다.")
    public ResponseEntity<Object> modifyReviewText(final UserInfo userInfo,
        @Valid @RequestBody final ReviewTextUpdateRequest reviewTextUpdateRequest) {
        reviewFacade.modifyReviewText(userInfo, reviewTextUpdateRequest);
        return ResponseEntity.ok().build();
    }
}
