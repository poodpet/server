package com.pood.server.controller.review;

import com.pood.server.controller.request.reviewclap.ReviewClapCreateDto;
import com.pood.server.facade.ReviewClapFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "리뷰 추천하기 API")
@RestController
@RequestMapping("/api/pood/v1-0/review/clap")
@RequiredArgsConstructor
public class ReviewClapController {

    private final ReviewClapFacade reviewClapFacade;

    @PostMapping
    @ApiOperation(value = "리뷰를 추천하여 좋아요를 표시합니다.", notes = "좋아요를 1 증가합니다. 1 이상은 증가할 수 없습니다.")
    public ResponseEntity<Object> add(@RequestBody ReviewClapCreateDto reviewClapCreateDto,
        @RequestHeader("token") String token) {
        reviewClapFacade.insertReviewLike(reviewClapCreateDto, token);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping("/{reviewIdx}")
    @ApiOperation(value = "리뷰를 추천 취소하여 좋아요를 삭제합니다.", notes = "좋아요를 삭제합니다.")
    public ResponseEntity<Object> deleteLike(@PathVariable int reviewIdx,
        @RequestHeader("token") String token) {
        reviewClapFacade.deleteReviewLike(reviewIdx, token);
        return ResponseEntity.ok().build();
    }
}
