package com.pood.server.controller;

import com.pood.server.dto.meta.goods.GoodsCtDto;
import com.pood.server.entity.meta.Banner;
import com.pood.server.service.BannerServiceV2;
import com.sun.istack.NotNull;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class BannerController {

    @Autowired
    BannerServiceV2 bannerService;

    @GetMapping("/api/pood/banner/{pcIdx}")
    @ApiOperation(value = "배너 정보 리스트 출력", notes = "배너 정보 리스트 조회합니다.")
    public ResponseEntity<List<Banner>> getGoodsSubCt(@NotNull @PathVariable Integer pcIdx){
        List<Banner> bannerList = bannerService.getBannerList(pcIdx);
//        Page<UserReviewImageDto.ReviewPhotoList> all =  goodsService.getReviewPhotoImgList(idx,pageable);
        return new ResponseEntity<>(bannerList, HttpStatus.OK);
    }

}
