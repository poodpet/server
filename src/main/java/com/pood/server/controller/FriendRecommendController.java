package com.pood.server.controller;

import com.pood.server.config.UserTokenValid;
import com.pood.server.dto.user.point.MyReceivedBenefitResponse;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.UserFacade;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pood/v1-0/friend")
public class FriendRecommendController {

    private final UserFacade userFacade;

    @UserTokenValid
    @GetMapping
    @ApiOperation(value = "친구초대 내가 받은 혜택 데이터를 조회합니다.", notes = "header에 token이 필요합니다. (AOP 토큰검증)"
        + "UserInfo는 aop를 통해 받아옵니다.")
    public ResponseEntity<MyReceivedBenefitResponse> myReceivedBenefit(final UserInfo userInfo,
        @RequestParam("imageIdx") final int imageIdx) {
        return ResponseEntity.ok(userFacade.myReceivedBenefit(userInfo, imageIdx));
    }

}
