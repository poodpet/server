package com.pood.server.controller.response.home.rank;

import java.time.LocalDateTime;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PoodRankCategoryDto {

    private Long idx;

    private Long goodsSubCtIdx;

    private String name;

    private Integer priority;

    private LocalDateTime recordbirth;

    public PoodRankCategoryDto(Long idx, Long goodsSubCtIdx, String name, Integer priority,
        LocalDateTime recordbirth) {
        this.idx = idx;
        this.goodsSubCtIdx = goodsSubCtIdx;
        this.name = name;
        this.priority = priority;
        this.recordbirth = recordbirth;
    }
}
