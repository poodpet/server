package com.pood.server.controller.response.solution;

import com.pood.server.dto.meta.pet.PetProductWord;
import lombok.Value;

@Value
public class PetProductWordResponse {

    PetProductWord petProductWord;
}
