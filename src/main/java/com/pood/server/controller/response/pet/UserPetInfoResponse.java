package com.pood.server.controller.response.pet;

import com.pood.server.dto.user.pet.UserPetAiDiagnosisDto;
import com.pood.server.dto.user.pet.UserPetAllergyDto;
import com.pood.server.dto.user.pet.UserPetGrainSizeDto;
import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.user.UserPet;
import com.pood.server.entity.user.UserPetImage;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserPetInfoResponse {

    private int idx;
    private int userIdx;
    private LocalDateTime recordbirth;
    private int pcId;
    private String petName;
    private LocalDate petBirth;
    private int petGender;
    private UserPetImage userPetImage;
    private String pcKind;
    private String pcSize;
    private String serialNumber;
    private List<UserPetAiDiagnosisDto> aiDiagnosisList = new ArrayList<>();
    private List<UserPetAllergyDto> allergyDtoList = new ArrayList<>();
    private List<UserPetGrainSizeDto> grainSizeList = new ArrayList<>();

    @Builder
    private UserPetInfoResponse(final int idx, final int userIdx, final LocalDateTime recordbirth,
        final int pcId,
        final String petName, final LocalDate petBirth, final int petGender,
        final String pcKind, final String pcSize, final String serialNumber) {
        this.idx = idx;
        this.userIdx = userIdx;
        this.recordbirth = recordbirth;
        this.pcId = pcId;
        this.petName = petName;
        this.petBirth = petBirth;
        this.petGender = petGender;
        this.pcKind = pcKind;
        this.pcSize = pcSize;
        this.serialNumber = serialNumber;
    }

    public static UserPetInfoResponse of(final UserPet userPet, final Pet petInfo) {
        return UserPetInfoResponse.builder()
            .idx(userPet.getIdx())
            .userIdx(userPet.getUserIdx())
            .recordbirth(userPet.getRecordbirth())
            .pcId(userPet.getPcId())
            .petName(userPet.getPetName())
            .petBirth(userPet.getPetBirth())
            .petGender(userPet.getPetGender())
            .pcKind(petInfo.getPcKind())
            .pcSize(petInfo.getPcSize())
            .serialNumber(userPet.getSerialNumber())
            .build();
    }
}
