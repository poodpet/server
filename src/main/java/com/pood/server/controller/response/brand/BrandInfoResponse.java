package com.pood.server.controller.response.brand;

import com.pood.server.entity.meta.Brand;
import com.pood.server.entity.meta.BrandImage;
import java.time.LocalDateTime;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class BrandInfoResponse {

    private Integer idx;

    private String brandName;

    private String brandNameEn;

    private String brandOrigin;

    private String brandDescription;

    private String brandIntro;

    private String brandLogo;

    private String brandTag;

    private String brandMenufac;

    private String brandSeller;

    private String brandPhone;

    private Integer brandVisible;

    private Integer petIdx;

    private Integer poodGrade;

    private LocalDateTime updatetime;

    private LocalDateTime recordbirth;

    private List<BrandImage> brandImages;

    public BrandInfoResponse(final Brand brand, final List<BrandImage> brandImages) {

        for (BrandImage brandImage : brandImages) {
            brandImage.setBrandNull();
        }

        idx = brand.getIdx();
        petIdx = brand.getPetIdx();
        brandVisible = brand.getBrandVisible();
        poodGrade = brand.getPoodGrade();
        brandIntro = brand.getBrandIntro();
        brandDescription = brand.getBrandDescription();
        brandPhone = brand.getBrandPhone();
        brandLogo = brand.getBrandLogo();
        updatetime = brand.getUpdatetime();
        recordbirth = brand.getRecordbirth();
        brandSeller = brand.getBrandSeller();
        brandMenufac = brand.getBrandMenufac();
        brandTag = brand.getBrandTag();
        brandOrigin = brand.getBrandOrigin();
        brandNameEn = brand.getBrandNameEn();
        brandName = brand.getBrandName();
        this.brandImages = brandImages;

    }

}
