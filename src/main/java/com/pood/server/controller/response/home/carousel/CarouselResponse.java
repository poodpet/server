package com.pood.server.controller.response.home.carousel;

import com.pood.server.dto.meta.marketing.MarketingDataSet;
import java.util.List;
import lombok.Value;

@Value(staticConstructor = "of")
public class CarouselResponse {

    List<MarketingDataSet> marketingDataList;
}
