package com.pood.server.controller.response.solution;

import com.pood.server.dto.meta.pet.NutritionWord;
import java.util.List;
import lombok.Value;

@Value
public class NutritionWordResponse {

    List<NutritionWord> nutritionWordList;
}
