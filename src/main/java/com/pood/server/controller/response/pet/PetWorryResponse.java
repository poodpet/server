package com.pood.server.controller.response.pet;

import lombok.Value;

@Value(staticConstructor = "of")
public class PetWorryResponse {

    long idx;
    String name;
    String dogUrl;
    String catUrl;
}
