package com.pood.server.controller.response.event;

import com.pood.server.util.MarketingType;
import lombok.Value;

@Value
public class RunningEvent {

    Integer idx;
    String title;
    String intro;
    String startDate;
    String endDate;
    String url;
    MarketingType type;
    String typeName;
    Integer pcIdx;

    public RunningEvent(final Integer idx, final String title, final String intro,
        final String startDate, final String endDate, final String url, final MarketingType type,
        final String typeName, final Integer pcIdx) {

        this.idx = idx;
        this.title = title;
        this.intro = intro;
        this.startDate = startDate;
        this.endDate = endDate;
        this.url = url;
        this.type = type;
        this.typeName = typeName;
        this.pcIdx = pcIdx;
    }
}
