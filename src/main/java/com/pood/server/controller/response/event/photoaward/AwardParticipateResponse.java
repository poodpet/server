package com.pood.server.controller.response.event.photoaward;

import com.pood.server.dto.meta.event.EndPhotoImgUrl;
import lombok.Value;
import org.springframework.data.domain.Page;

@Value
public class AwardParticipateResponse {

    Page<EndPhotoImgUrl> normalImageList;
}
