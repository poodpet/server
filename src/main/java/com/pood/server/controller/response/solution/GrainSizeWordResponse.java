package com.pood.server.controller.response.solution;

import com.pood.server.service.pet_solution.group.UserPetGrainSizeAiSolutionGroup.GrainSizeStr;
import java.util.Collections;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class GrainSizeWordResponse {

    private List<GrainSizeStr> grainSizeStrList;

    public GrainSizeWordResponse(final List<GrainSizeStr> grainSizeStrList) {
        this.grainSizeStrList = Collections.unmodifiableList(grainSizeStrList);
    }
}
