package com.pood.server.controller.response.pet;

import com.pood.server.entity.user.UserPetWeightDiary;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class UserPetWeightDiaryResponse {

    private final Long idx;

    private final String recordBirth;

    private UserPetWeightDiaryResponse(final Long idx, final LocalDateTime recordBirth) {
        this.idx = idx;
        this.recordBirth = recordBirth.format(DateTimeFormatter.ofPattern("yyyy.MM.dd"));
    }

    public static UserPetWeightDiaryResponse of(final UserPetWeightDiary userPetWeightDiary) {
        return new UserPetWeightDiaryResponse(userPetWeightDiary.getIdx(), userPetWeightDiary.getRecordBirth());
    }
}
