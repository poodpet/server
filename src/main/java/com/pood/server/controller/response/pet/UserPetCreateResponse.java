package com.pood.server.controller.response.pet;

import com.pood.server.facade.coupon.response.CouponCreateResponse;
import lombok.Value;

@Value(staticConstructor = "of")
public class UserPetCreateResponse {

    String petName;
    String serialNumber;
    CouponCreateResponse couponInfo;

    public static UserPetCreateResponse isNotFirst(final String petName, final String serialNumber) {
        return new UserPetCreateResponse(petName, serialNumber, null);
    }

}
