package com.pood.server.controller.response.goods;

import lombok.Value;

@Value
public class SuggestRandomGoodsResponse {

    int idx;
    String goodsName;
    int goodsOriginPrice;
    int goodsPrice;
    int discountRate;
    int discountPrice;
    Long reviewCnt;
    double averageRating;
    String mainImage;
    int saleStatus;
    String brandName;
    boolean newest;
}
