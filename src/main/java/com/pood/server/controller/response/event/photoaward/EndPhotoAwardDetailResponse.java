package com.pood.server.controller.response.event.photoaward;

import com.pood.server.dto.meta.event.EndPhotoImgUrl;
import lombok.Value;

@Value
public class EndPhotoAwardDetailResponse {

    int countAwards;
    String intro;
    String startDate;
    String endDate;
    String imageUrl;
    EndPhotoImgUrl winnerImage;
}
