package com.pood.server.controller.response.order;

import lombok.Value;

@Value(staticConstructor = "of")
public class OrderQuantityResponse {

    int quantity;
}
