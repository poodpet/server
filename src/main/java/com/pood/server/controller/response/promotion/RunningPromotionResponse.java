package com.pood.server.controller.response.promotion;

import com.pood.server.dto.meta.promotion.RunningPromotion;
import java.util.List;
import lombok.Value;

@Value(staticConstructor = "of")
public class RunningPromotionResponse {

    List<RunningPromotion> promotionList;
}
