package com.pood.server.controller.response.home.icon;

import com.pood.server.entity.meta.ShortcutIcon;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;

@Getter
public class ShortCutIconResponse {

    private final Long idx;
    private final String imageUrl;
    private final String iconName;
    private final String redirectUrl;

    @Builder(access = AccessLevel.PRIVATE)
    private ShortCutIconResponse(final Long idx, final String imageUrl, final String iconName, final String redirectUrl) {
        this.idx = idx;
        this.imageUrl = imageUrl;
        this.iconName = iconName;
        this.redirectUrl = redirectUrl;
    }

    public static ShortCutIconResponse of(final ShortcutIcon shortcutIcon) {
        return ShortCutIconResponse.builder()
            .idx(shortcutIcon.getId())
            .imageUrl(shortcutIcon.getImageUrl())
            .iconName(shortcutIcon.getIconName())
            .redirectUrl(shortcutIcon.getRedirectUrl())
            .build();
    }
}
