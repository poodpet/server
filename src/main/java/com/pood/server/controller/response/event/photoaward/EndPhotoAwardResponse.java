package com.pood.server.controller.response.event.photoaward;

import lombok.Value;

@Value
public class EndPhotoAwardResponse {

    int idx;
    String imgUrl;

}
