package com.pood.server.controller.response.event;

import com.pood.server.entity.meta.mapper.event.EventDonationThumbnailMapper;
import lombok.Value;

@Value
public class DonationEventListResponse {

    int idx;
    String imgUrl;

    public static DonationEventListResponse of(final EventDonationThumbnailMapper mapper) {
        return new DonationEventListResponse(mapper.getIdx(), mapper.getImgUrl());
    }
}
