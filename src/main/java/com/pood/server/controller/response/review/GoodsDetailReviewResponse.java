package com.pood.server.controller.response.review;

import com.pood.server.dto.user.review.GoodsDetailReviewDto;
import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.user.UserPetImage;
import com.pood.server.entity.user.UserReviewImage;
import java.util.List;
import org.springframework.data.domain.Page;

public class GoodsDetailReviewResponse {

    private final Page<GoodsDetailReviewDto> goodsReviewList;

    public GoodsDetailReviewResponse(final Page<GoodsDetailReviewDto> goodsReviewList,
        final List<Pet> userPetList, final List<UserPetImage> userPetImageList,
        final List<UserReviewImage> userReviewImages) {
        this.goodsReviewList = goodsReviewList;
        setPetInfo(userPetList);
        setPetImages(userPetImageList);
        addUserReviewImagesEqidx(userReviewImages);
    }

    private void setPetInfo(final List<Pet> userPetList) {
        goodsReviewList.forEach(review -> {
            review.setPetInfo(userPetList);
        });
    }

    private void setPetImages(final List<UserPetImage> userPetImageListV2) {
        goodsReviewList.forEach(review -> {
            review.setPetImages(userPetImageListV2);
        });
    }

    public void addUserReviewImagesEqidx(final List<UserReviewImage> userReviewImages) {
        goodsReviewList.forEach(review -> {
            review.addUserReviewImagesEqidx(userReviewImages);
        });
    }

    public Page<GoodsDetailReviewDto> getJsonData() {
        return goodsReviewList;
    }
}
