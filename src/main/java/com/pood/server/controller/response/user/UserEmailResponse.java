package com.pood.server.controller.response.user;

import lombok.Value;

@Value(staticConstructor = "of")
public class UserEmailResponse {
    String email;
}
