package com.pood.server.controller.response.faq;

import lombok.Value;

@Value
public class FaqResponse {

    Long idx;
    String title;
    String contents;
}
