package com.pood.server.controller.response.solution;

import com.pood.server.service.pet_solution.group.UserPetAllergyAiSolutionGroup.AllergyStr;
import java.util.Collections;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class AllergyWordResponse {

    private List<AllergyStr> allergyStrList;

    public AllergyWordResponse(final List<AllergyStr> allergyStrList) {
        this.allergyStrList = Collections.unmodifiableList(allergyStrList);
    }
}
