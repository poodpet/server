package com.pood.server.controller.response.product;

import lombok.Value;

@Value(staticConstructor = "of")
public class ProductSolutionPointResponse {

    String scoreName;
    int score;
}
