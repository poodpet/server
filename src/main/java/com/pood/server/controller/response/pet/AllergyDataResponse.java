package com.pood.server.controller.response.pet;

import lombok.Value;

@Value
public class AllergyDataResponse {

    int idx;
    String name;
    String url;
}
