package com.pood.server.controller.response.pet;

import lombok.Value;

@Value
public class UserPetTotalCountResponse {

    Long dogCount;
    Long catCount;
}
