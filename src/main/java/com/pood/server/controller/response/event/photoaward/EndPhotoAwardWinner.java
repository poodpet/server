package com.pood.server.controller.response.event.photoaward;

import lombok.Value;

@Value
public class EndPhotoAwardWinner {

    int idx;
    String imgUrl;
}
