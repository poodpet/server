package com.pood.server.controller.response.pet;

import lombok.Value;

@Value(staticConstructor = "of")
public class GrainSizeResponse {

    int idx;

    // grain_size 테이블의 title값
    // 앱쪽 요청사항으로 title -> name으로 변경
    String name;

    String url;
}
