package com.pood.server.controller.response.solution;

import com.pood.server.service.pet_solution.group.UserPetAiDiagnosisAiSolutionGroup.UserPetAiDiagnosisStr;
import java.util.Collections;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class WorryWordResponse {

    private List<UserPetAiDiagnosisStr> worryList;

    public WorryWordResponse(final List<UserPetAiDiagnosisStr> worryList) {
        this.worryList = Collections.unmodifiableList(worryList);
    }
}
