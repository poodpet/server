package com.pood.server.controller.response.faq;

import lombok.Value;

@Value
public class FaqCategoryResponse {

    Long idx;
    String name;
}
