package com.pood.server.controller.response.review;

import com.pood.server.dto.user.review.GoodsImagesReviewDto;
import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.user.UserPetImage;
import java.util.List;
import org.springframework.data.domain.Page;

public class GoodsImagesReviewResponse {

    private final Page<GoodsImagesReviewDto> goodsReviewList;

    public GoodsImagesReviewResponse(final Page<GoodsImagesReviewDto> goodsReviewList,
        final List<Pet> userPetList, final List<UserPetImage> userPetImageList) {
        this.goodsReviewList = goodsReviewList;
        setPetInfo(userPetList);
        setPetImages(userPetImageList);
    }


    private void setPetInfo(final List<Pet> userPetList) {
        goodsReviewList.forEach(review -> {
            review.setPetInfo(userPetList);
        });
    }

    private void setPetImages(final List<UserPetImage> userPetImageList) {
        goodsReviewList.forEach(review -> {
            review.setPetImages(userPetImageList);
        });
    }

    public Page<GoodsImagesReviewDto> getList() {
        return goodsReviewList;
    }
}
