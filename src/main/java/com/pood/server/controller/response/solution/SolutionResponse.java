package com.pood.server.controller.response.solution;

import com.pood.server.controller.response.product.ProductSolutionPointResponse;
import java.util.List;
import lombok.Value;

@Value(staticConstructor = "of")
public class SolutionResponse {

    List<ProductSolutionPointResponse> productSolutionPointList;
    List<Object> wordExtractList;
    EatAmountResponse eatAmountResponse;

}
