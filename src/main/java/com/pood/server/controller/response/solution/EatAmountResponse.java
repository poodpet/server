package com.pood.server.controller.response.solution;

import com.pood.server.service.pet_solution.intake.ProductStore;
import lombok.Getter;

@Getter
public class EatAmountResponse {

    private final double daily;
    private final int allDay;
    private final String cupType;
    private final String cupSubType;

    public EatAmountResponse(final ProductStore productStore) {
        this.daily = productStore.dailyTakesToEat();
        this.allDay = productStore.longItTakesToEat();
        this.cupType = productStore.cupType();
        this.cupSubType = productStore.cupSubType();
    }
}
