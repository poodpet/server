package com.pood.server.controller;

import com.pood.server.controller.request.coupon.CouponCreateDto;
import com.pood.server.dto.user.coupon.CouponResponseDtoGroup;
import com.pood.server.facade.CouponFacade;
import com.pood.server.facade.coupon.response.FirstRegisterPetCouponResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "쿠폰 API")
@RestController
@RequestMapping("/api/pood/v1-0/coupon")
@RequiredArgsConstructor
public class CouponController {

    private final CouponFacade couponFacade;

    @PostMapping("/create")
    @ApiOperation(value = "쿠폰 등록하기 API", notes = "쿠폰 코드와 유저 uuid 값을 통해 쿠폰을 등록합니다.")
    public ResponseEntity<Object> create(@RequestBody @Valid final CouponCreateDto couponCreateDto,
        @RequestHeader("token") String token) {
        couponFacade.createCoupon(couponCreateDto, token);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping
    @ApiOperation(value = "내 쿠폰 조회 API", notes = "토큰을 통해 쿠폰을 조회합니다.")
    public ResponseEntity<CouponResponseDtoGroup> find(@RequestHeader("token") final String token) {
        return ResponseEntity.ok(couponFacade.findCoupon(token));
    }

    @GetMapping("/first-register-pet")
    @ApiOperation(value = "펫 최초 등록 쿠폰 리스트 조회 API", notes = "펫 최초 등록시 발급될수 있는 쿠폰 리스트 조회합니다.")
    public ResponseEntity<List<FirstRegisterPetCouponResponse>> firstRegisterPetCouponList() {
        return ResponseEntity.ok(couponFacade.firstRegisterPetCouponList());
    }
}
