package com.pood.server.controller;

import com.pood.server.api.service.meta.promotion.promotionGroupGoodsService;
import com.pood.server.dto.meta.goods.GoodsDto;
import com.pood.server.object.meta.vo_promotion_group_goods;
import com.pood.server.service.GoodsServiceV2;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@RestController
public class TestController {

    @Autowired
    promotionGroupGoodsService promotionGroupGoodsService;


    @GetMapping("/api/pood/test/getPromotionGoodsInfo")
    public ResponseEntity getProductList(HttpServletRequest request) throws Exception {
        vo_promotion_group_goods promotionGoodsInfo = promotionGroupGoodsService.getPromotionGoodsInfo(45,938);

        return new ResponseEntity<>(null, HttpStatus.OK);
    }


}

