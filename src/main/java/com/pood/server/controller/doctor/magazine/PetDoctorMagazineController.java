package com.pood.server.controller.doctor.magazine;

import com.pood.server.facade.doctor.magazine.PetDoctorMagazineFacade;
import com.pood.server.facade.doctor.magazine.response.PetDoctorMagazineCtResponse;
import com.pood.server.facade.doctor.magazine.response.PetDoctorMagazineDetailResponse;
import com.pood.server.facade.doctor.magazine.response.PetDoctorMagazineResponse;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.constraints.Max;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Validated
@RequestMapping("/api/pood/v1-0/pet-doctor/magazine")
public class PetDoctorMagazineController {

    private final PetDoctorMagazineFacade petDoctorMagazineFacade;

    @GetMapping("/category")
    @ApiOperation(value = "수의사의 발견 카테고리 리스트 조회", notes = "수의사의 발견 카테고리 리스트 조회")
    public ResponseEntity<List<PetDoctorMagazineCtResponse>> getCategory() {
        return ResponseEntity.ok(petDoctorMagazineFacade.getCategory());
    }

    @GetMapping
    @ApiOperation(value = "수의사의 발견 리스트 조회", notes = "수의사의 발견 리스트 조회 조회")
    public ResponseEntity<Page<PetDoctorMagazineResponse>> getPetDoctorMagazineList(
        @PageableDefault(sort = "idx", direction = Sort.Direction.DESC) final Pageable pageable,
        @RequestParam(value = "ctIdx", required = false) Long ctIdx,
        @RequestParam(value = "pcIdx") @Max(value = 2, message = "존재하지 않는 펫 카테고리 idx 입니다.") Integer pcIdx) {
        return ResponseEntity.ok(petDoctorMagazineFacade.getMagazineList(ctIdx, pcIdx, pageable));
    }

    @GetMapping("/{idx}")
    @ApiOperation(value = "수의사의 발견 상세 조회", notes = "수의사의 발견 상세 조회")
    public ResponseEntity<PetDoctorMagazineDetailResponse> getPetDoctorMagazineDetail(
        @PathVariable Long idx) {
        return ResponseEntity.ok(petDoctorMagazineFacade.getPetDoctorMagazineDetail(idx));
    }

}
