package com.pood.server.controller;


import com.pood.server.config.UserTokenValid;
import com.pood.server.controller.request.goods.GoodsFilterRequest;
import com.pood.server.dto.meta.goods.GoodsCtDto;
import com.pood.server.dto.meta.goods.GoodsDto;
import com.pood.server.dto.meta.goods.GoodsDto.GoodsFilterCtDto;
import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.dto.meta.goods.UserReviewImageDto;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.GoodsCtFacade;
import com.pood.server.facade.ReviewFacade;
import com.pood.server.facade.UserFacade;
import com.pood.server.facade.goods.GoodsFacade;
import com.pood.server.facade.goods.response.GoodsIngredientsResponse;
import com.pood.server.service.GoodsServiceV2;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/pood/goods")
public class GoodsController {

    private final GoodsServiceV2 goodsService;
    private final GoodsCtFacade goodsFilterFacade;
    private final ReviewFacade reviewFacade;
    private final GoodsFacade goodsFacade;
    private final UserFacade userFacade;

    @GetMapping
    @ApiOperation(value = "딜상품 검색", notes = "검색 정보 리스트를 출력 합니다.")
    public ResponseEntity<Page<SortedGoodsList>> getGoodsFilteredList(
        @RequestParam(value = "pcIdx") int pcIdx,
        @RequestParam(value = "searchText", required = false) String searchText,
        @RequestParam(value = "goodsCtIdx", required = false) Long goodsCtIdx,
        @PageableDefault(size = 10, sort = "idx", direction = Sort.Direction.ASC) Pageable pageable) {

        return ResponseEntity.ok(
            goodsFacade.goodsSearchList(pageable, searchText, pcIdx, goodsCtIdx));
    }

    @GetMapping("/feed-comparison")
    @ApiOperation(value = "사료비교 딜상품 검색", notes = "검색 정보 리스트를 출력 합니다.")
    public ResponseEntity<Page<SortedGoodsList>> getGoodsFilteredListForFeedComparison(
        @RequestParam(value = "pcIdx") int pcIdx,
        @RequestParam(value = "searchText", required = false) String searchText,
        @PageableDefault(size = 10, sort = "idx", direction = Sort.Direction.ASC) Pageable pageable) {

        return ResponseEntity.ok(
            goodsFacade.goodsSearchListSoringProductCt(pageable, searchText, pcIdx));
    }

    @PostMapping("/filter")
    @ApiOperation(value = "딜상품 필터 조회 v3")
    public ResponseEntity<Page<GoodsDto.SortedGoodsList>> goodsFilterListV3(
        @PageableDefault(sort = "idx", direction = Sort.Direction.ASC) final Pageable pageable,
        @RequestBody final GoodsFilterRequest request) {
        request.addFreeSize();
        return ResponseEntity.ok(goodsFacade.getSortedGoodsList(pageable, request));
    }

    @GetMapping("/review/{idx}")
    @ApiOperation(value = "상품 리뷰 사진 리스트 조회", notes = "상품 리뷰 사진 리스트 조회 합니다.")
    public ResponseEntity<Page<UserReviewImageDto.ReviewPhotoList>> getReviewPhotoImgList(
        @PathVariable Integer idx,
        @PageableDefault(size = 10, sort = "recordbirth", direction = Sort.Direction.DESC) Pageable pageable,
        HttpServletRequest request) {
        String token = request.getHeader("token");
        return ResponseEntity.ok(reviewFacade.getReviewPhotoImgList(idx, token,
            pageable));
    }

    @GetMapping("/{idx}")
    @ApiOperation(value = "상품 상세 조회", notes = "상품 정보를 상세 조회합니다.")
    public ResponseEntity<GoodsDto.GoodsDetail> getGoods(@PathVariable Integer idx) {
        return ResponseEntity.ok(goodsService.getGoodsDetail(idx));
    }

    @GetMapping("/ct")
    @ApiOperation(value = "딜상품 카테고리 조회", notes = "딜상품 카테고리 조회 조회합니다.")
    public ResponseEntity<List<GoodsCtDto.GoodsCtListDto>> getGoodsCt() {
        return ResponseEntity.ok(goodsService.getGoodsCt());
    }

    @GetMapping("/sub/ct/{ctIdx}")
    @ApiOperation(value = "딜상품 서브 카테고리 조회", notes = "딜상품 서브 카테고리 조회합니다.")
    public ResponseEntity<List<GoodsCtDto.GoodsSubCtListDto>> getGoodsSubCt(
        @PathVariable Long ctIdx) {
        return ResponseEntity.ok(goodsService.getGoodsSubCts(ctIdx));
    }

    @GetMapping("/group/ct/{pcIdx}")
    @ApiOperation(value = "딜상품 그룹 조회", notes = "딜상품 그룹 조회 조회합니다.")
    public ResponseEntity<List<GoodsCtDto.GoodsCtDetailDto>> geGoodsCtDetail(
        @PathVariable Integer pcIdx) {
        return ResponseEntity.ok(goodsService.geGoodsCtDetailDtos(
            pcIdx));
    }

    @GetMapping("/group/filter/ct/{pcIdx}")
    @ApiOperation(value = "딜상품 필터 조회", notes = "딜상품 필터 조회 조회합니다.")
    public ResponseEntity<List<GoodsFilterCtDto>> getGoodsFilterCtList(
        @PathVariable Integer pcIdx) {
        return ResponseEntity.ok(goodsService.getGoodsFilterCtList(
            pcIdx));
    }

    @GetMapping("/v1-1/group/filter/ct/{pcIdx}")
    @ApiOperation(value = "딜상품 필터 조회(v1.1)", notes = "딜상품 필터 조회 조회합니다.")
    public ResponseEntity<List<GoodsDto.GoodsCtDto>> getGoodsFilterCtListV11(
        @PathVariable Integer pcIdx) {
        return ResponseEntity.ok(goodsFilterFacade.getGoodsFilterCtList(pcIdx));
    }

    @GetMapping("/ingredients/all")
    @ApiOperation(value = "딜상품 원료 조회", notes = "딜의 구성된 상품들의 원료를 조회합니다.")
    public ResponseEntity<List<GoodsIngredientsResponse>> getGoodsFilterCtListV11(
        @RequestParam(name = "goodsIdx") final List<Integer> goodsIdxList) {
        return ResponseEntity.ok(goodsFacade.getGoodsIngredientsList(goodsIdxList));
    }

    @UserTokenValid
    @PostMapping("/request/re-stock/{goods-idx}")
    @ApiOperation(value = "재입고 요청", notes = "상품의 재입고를 요청합니다.")
    public ResponseEntity<String> requestReStock(@PathVariable("goods-idx") final Integer goodsIdx,
        final UserInfo userInfo) {
        return ResponseEntity.ok(userFacade.requestReStock(goodsIdx, userInfo));
    }

}

