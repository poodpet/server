package com.pood.server.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class WebViewController {
  @RequestMapping(value = "/api/pood/view/promotion", method = RequestMethod.GET)
  public String viewPromotion(){
    return "promotion_event/promotion.html";
  }

  @RequestMapping(value = "/api/pood/view/event", method = RequestMethod.GET)
  public String viewEvent(){
    return "promotion_event/event.html";
  }

  @RequestMapping(value = "/api/pood/view/privacy", method = RequestMethod.GET)
  public String getPrivacy(){
    return "layout/privacy.html";
  }

  @RequestMapping(value = "/api/pood/view/terms", method = RequestMethod.GET)
  public String getTerms(){
    return "layout/terms.html";
  }


  @RequestMapping(value = "/api/pood/view/temp", method = RequestMethod.GET)
  public String getTempTest(){
    return "promotion_event/webviewTest.html";
  }

}

