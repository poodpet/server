package com.pood.server.exception;

public class SmsSendException extends RuntimeException {

    public SmsSendException() {
        super("문자 전송에 실패했습니다.");
    }
}
