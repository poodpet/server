package com.pood.server.exception;

import org.springframework.dao.DuplicateKeyException;

public final class AlreadyExistException extends DuplicateKeyException {

    public AlreadyExistException(final String message) {
        super(message);
    }
}
