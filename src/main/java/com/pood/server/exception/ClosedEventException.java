package com.pood.server.exception;


public class ClosedEventException extends IllegalStateException {

    public ClosedEventException(final String message) {
        super(message);
    }

}
