package com.pood.server.exception;

public final class UsageTimeExpiredException extends IllegalStateException {

    public UsageTimeExpiredException(final String message) {
        super(message);
    }
}
