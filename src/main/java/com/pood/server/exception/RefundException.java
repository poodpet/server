package com.pood.server.exception;

public class RefundException extends RuntimeException{

    public RefundException(String message) {
        super(message);
    }


}
