package com.pood.server.exception;

public enum ErrorMessage {
    //400
    VERSION("version 값이 입력되지 않았습니다."),
    USERNAME("사용자 이름은 필수 값 입니다."),
    PHONE_NUMBER("핸드폰 번호는 필수 값 입니다."),
    EVENT("이벤트가 존재하지 않습니다."),
    ORDER_POINT("해당 주문 포인트 건이 존재하지 않습니다."),
    ALREADY_PARTICIPATE_EVENT("이미 참여한 이벤트 입니다."),
    COMMENT("댓글이 존재하지 않습니다."),
    IDX("idx가 존재하지 않습니다."),
    PET("펫 정보가 존재하지 않습니다."),
    PRODUCT("상품이 존재하지 않습니다."),
    GOODS("딜 상품이 존재하지 않습니다."),
    USER("사용자가 존재하지 않습니다."),
    IMAGE("이미지 파일이 존재하지 않습니다."),
    PHOTO("사진이 존재하지 않습니다."),
    ATTEND_CHANCE("참여 기회가 남아있지 않습니다."),
    PUSH("푸시 정보가 존재하지 않습니다."),
    PROMOTION("기획전이 존재하지 않습니다."),
    MEMBER("회원 정보가 유효하지 않습니다."),
    REVIEW("리뷰가 존재하지 않습니다."),
    HOLIDAY("공휴일 정보가 NULL일수 없습니다."),
    EVENT_HAVE_STARTED("시작되지 않은 이벤트 입니다."),
    EVENT_HAVE_ENDED("이미 종료된 이벤트 입니다."),
    EVENT_PAUSE("일시 중지 된 이벤트 입니다."),

    //401
    API_TOKEN("API 토큰이 존재하지 않습니다."),

    //500
    SERVER("서버 내부 오류가 발생했습니다.");

    private final String message;

    ErrorMessage(final String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
