package com.pood.server.exception;

public class LimitQuantityException extends RuntimeException {

    public LimitQuantityException(final String message) {
        super(message);
    }
}
