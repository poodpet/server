package com.pood.server.exception;

public class IndexNotMatchException extends RuntimeException {

    public IndexNotMatchException(final String message) {
        super(message);
    }
}
