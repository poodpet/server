package com.pood.server.exception;


public class PauseEventException extends IllegalStateException {

    public PauseEventException(final String message) {
        super(message);
    }

}
