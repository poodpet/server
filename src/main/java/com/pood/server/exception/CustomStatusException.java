package com.pood.server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class CustomStatusException extends ResponseStatusException {

    private CustomStatusException(final HttpStatus status, final String message) {
        super(status, message);
    }

    private CustomStatusException(final HttpStatus status, final ErrorMessage error) {
        super(status, error.getMessage());
    }

    public static CustomStatusException badRequest(final ErrorMessage error) {
        return new CustomStatusException(HttpStatus.BAD_REQUEST, error);
    }

    public static CustomStatusException conflict(final String message) {
        return new CustomStatusException(HttpStatus.CONFLICT, message);
    }

    public static CustomStatusException unAuthorized(final ErrorMessage error) {
        return new CustomStatusException(HttpStatus.UNAUTHORIZED, error);
    }

    public static CustomStatusException serverError(final ErrorMessage error) {
        return new CustomStatusException(HttpStatus.INTERNAL_SERVER_ERROR, error);
    }

}
