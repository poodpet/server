package com.pood.server.exception;

public final class DataNotMatchingException extends RuntimeException {

    public DataNotMatchingException(String message) {
        super(message);
    }

}
