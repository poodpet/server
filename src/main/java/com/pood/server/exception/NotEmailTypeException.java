package com.pood.server.exception;

public class NotEmailTypeException extends RuntimeException {

    public NotEmailTypeException() {
        super("이메일 형식의 회원가입이 아닙니다.");
    }

}
