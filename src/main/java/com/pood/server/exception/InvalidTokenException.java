package com.pood.server.exception;

public class InvalidTokenException extends IllegalArgumentException {

    public InvalidTokenException(final String message) {
        super(message);
    }
}
