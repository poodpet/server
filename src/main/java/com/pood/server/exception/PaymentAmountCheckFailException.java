package com.pood.server.exception;

public class PaymentAmountCheckFailException extends RuntimeException {

    public PaymentAmountCheckFailException(String message) {
        super(message);
    }
}
