package com.pood.server.exception;

public class PasswordNotMatchException extends RuntimeException {

    private static final String ERROR_MESSAGE = "비밀번호가 일치하지 않습니다.";

    public PasswordNotMatchException() {
        super(ERROR_MESSAGE);
    }

    public PasswordNotMatchException(final String message) {
        super(message);
    }
}
