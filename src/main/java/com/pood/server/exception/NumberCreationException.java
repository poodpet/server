package com.pood.server.exception;

public class NumberCreationException extends RuntimeException {

    public NumberCreationException(final String message) {
        super(message);
    }
}
