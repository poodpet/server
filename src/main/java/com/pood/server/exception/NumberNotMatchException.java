package com.pood.server.exception;

public class NumberNotMatchException extends IllegalArgumentException {

    public NumberNotMatchException() {
        super("인증번호가 일치하지 않습니다.");
    }
}
