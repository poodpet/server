package com.pood.server.exception;

public class AlreadyChangedException extends IllegalStateException {

    public AlreadyChangedException() {
        super("이미 인증이 성공한 상태입니다.");
    }
}
