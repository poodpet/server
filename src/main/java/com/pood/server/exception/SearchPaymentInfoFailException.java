package com.pood.server.exception;

import java.util.NoSuchElementException;

public class SearchPaymentInfoFailException extends NoSuchElementException {

    public SearchPaymentInfoFailException(String message) {
        super(message);
    }
}
