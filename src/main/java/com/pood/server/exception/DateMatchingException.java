package com.pood.server.exception;

public class DateMatchingException  extends RuntimeException {

    public DateMatchingException(final String message) {
        super(message);
    }
}
