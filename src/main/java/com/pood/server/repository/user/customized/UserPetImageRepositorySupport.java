package com.pood.server.repository.user.customized;

import com.pood.server.entity.user.UserPetImage;
import java.util.List;

public interface UserPetImageRepositorySupport {

    List<UserPetImage> findAllByIdxInRecordBirthDesc(List<Integer> userPetIdxList);
}
