package com.pood.server.repository.user;

import com.pood.server.entity.user.PetDiagnosisJoinView;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetDiagnosisJoinViewRepository extends
    JpaRepository<PetDiagnosisJoinView, Integer> {

    List<PetDiagnosisJoinView> findAllByUserPetIdxIn(final List<Integer> userPetIdxList);
}
