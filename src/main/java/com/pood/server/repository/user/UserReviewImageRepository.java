package com.pood.server.repository.user;

import com.pood.server.entity.user.UserReviewImage;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserReviewImageRepository extends JpaRepository<UserReviewImage, Integer>{

    List<UserReviewImage> findAllByUserReviewIdxIn(final List<Integer> userReviewList);
}
