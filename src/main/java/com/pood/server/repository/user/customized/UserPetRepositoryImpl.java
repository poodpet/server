package com.pood.server.repository.user.customized;


import com.pood.server.config.querydsl.UserQuerydslRepositorySupport;
import com.pood.server.entity.user.QUserPet;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.mapper.pet.QUserPetTotalCountMapper;
import com.pood.server.entity.user.mapper.pet.UserPetTotalCountMapper;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;


public class UserPetRepositoryImpl extends UserQuerydslRepositorySupport implements
    UserPetCustomRepository {

    private final JPAQueryFactory queryFactory;
    private final QUserPet qUserPet = QUserPet.userPet;

    public UserPetRepositoryImpl(@Qualifier("userQueryFactory") JPAQueryFactory queryFactory) {
        super(UserInfo.class);
        this.queryFactory = queryFactory;
    }


    @Override
    public List<UserPetTotalCountMapper> getUserPetTotalCount() {
        return queryFactory.select(new QUserPetTotalCountMapper(
                qUserPet.pcId,
                qUserPet.pcId.count()
            ))
            .from(qUserPet)
            .groupBy(qUserPet.pcId)
            .fetch();
    }
}

