package com.pood.server.repository.user;

import com.pood.server.entity.user.UserSmsAuth;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserSmsAuthRepository extends JpaRepository<UserSmsAuth, Integer> {

    Optional<UserSmsAuth> findTop1ByPhoneNumberOrderByIdxDesc(final String phoneNumber);

}