package com.pood.server.repository.user;

import com.pood.server.entity.user.UserPetFeed;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserPetFeedRepository extends JpaRepository<UserPetFeed, Integer> {
    void deleteByUserPetIdx(final int userPetIdx);
}
