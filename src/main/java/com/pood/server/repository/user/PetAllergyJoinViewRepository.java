package com.pood.server.repository.user;

import com.pood.server.entity.user.PetAllergyJoinView;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetAllergyJoinViewRepository extends JpaRepository<PetAllergyJoinView, Integer> {

    List<PetAllergyJoinView> findAllByUserPetIdxIn(final List<Integer> userPetIdx);
}