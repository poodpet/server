package com.pood.server.repository.user;

import com.pood.server.entity.user.UserReviewClap;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserReviewClapRepository extends JpaRepository<UserReviewClap, Integer> {

    boolean existsByUserIdxAndUserReviewIdx(final int userIdx, final int reviewIdx);

    void deleteByUserIdxAndUserReviewIdx(final int userIdx, final int reviewIdx);
}