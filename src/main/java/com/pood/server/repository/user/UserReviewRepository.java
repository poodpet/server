package com.pood.server.repository.user;

import com.pood.server.entity.user.UserReview;
import com.pood.server.repository.user.customized.UserReviewCustomRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface UserReviewRepository extends JpaRepository<UserReview, Integer>,
    UserReviewCustomRepository, QuerydslPredicateExecutor<UserReview> {

    List<UserReview> findAllByIdxIn(final List<Integer> idx);

    List<UserReview> findAllByOrderNumberIn(final List<String> idxList);

    List<UserReview> findAllByUserIdx(final int userIdx);

    Optional<UserReview> findByUserIdxAndIdx(final int userIdx, final int idx);

}
