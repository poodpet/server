package com.pood.server.repository.user;

import com.pood.server.entity.user.UserBaseImage;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserBaseImageRepository extends JpaRepository<UserBaseImage, Long>{

    List<UserBaseImage> findAllByBasicIsTrue();

    Optional<UserBaseImage> findByIdxAndBasicIsTrue(final long idx);
}
