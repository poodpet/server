package com.pood.server.repository.user;

import com.pood.server.entity.user.UserBasket;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserBasketRepository extends JpaRepository<UserBasket, Integer> {

    List<UserBasket> findAllByUserUuidAndStatusIn(final String uuid, final List<Integer> status);

    List<UserBasket> findAllByGoodsIdxAndUserUuid(final Integer goodsIdx, final String userUuid);

    Long countByUserUuidAndStatusInAndGoodsIdxNotIn(final String uuid, final List<Integer> status,
        final List<Integer> goodsIdxList);

}
