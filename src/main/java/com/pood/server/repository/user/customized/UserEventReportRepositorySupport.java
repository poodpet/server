package com.pood.server.repository.user.customized;

import com.pood.server.entity.user.UserEventReport;
import java.util.List;

public interface UserEventReportRepositorySupport {

    List<UserEventReport> reportUserList();
}
