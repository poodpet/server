package com.pood.server.repository.user.customized;

import static com.pood.server.entity.user.QUserInfo.userInfo;
import static com.pood.server.entity.user.QUserReferralCode.userReferralCode;

import com.pood.server.entity.user.UserReferralCode;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;

public class UserReferralCodeRepositorySupportImpl implements
    UserReferralCodeRepositorySupport {

    private final JPAQueryFactory queryFactory;

    public UserReferralCodeRepositorySupportImpl(
        @Qualifier("userQueryFactory") final JPAQueryFactory queryFactory) {
        this.queryFactory = queryFactory;
    }

    @Override
    public List<UserReferralCode> findAllJoinUserInfoId(final int userInfoIdx) {
        return queryFactory.selectFrom(userReferralCode)
            .innerJoin(userInfo)
            .on(userReferralCode.receivedUserIdx.eq(userInfo.idx))
            .where(userReferralCode.receivedUserIdx.eq(userInfoIdx))
            .fetch();
    }

}
