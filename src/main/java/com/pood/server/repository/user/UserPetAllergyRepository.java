package com.pood.server.repository.user;

import com.pood.server.entity.user.UserPetAllergy;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserPetAllergyRepository extends JpaRepository<UserPetAllergy, Integer> {

    List<UserPetAllergy> findByUserPetIdx(Integer userPetIdx);

    void deleteAllByUserPetIdx(final int userPetIdx);
}
