package com.pood.server.repository.user.customized;


import com.pood.server.api.mapper.user.review.ReviewRatingAndCountDto;
import com.pood.server.dto.user.review.GoodsDetailReviewDto;
import com.pood.server.dto.user.review.GoodsImagesReviewDto;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserReviewCustomRepository {

    Page<GoodsDetailReviewDto> getGoodsReviewList(final Pageable pageable, final Integer goodsIdx,
        final Integer userIdx, final List<Integer> notInReviewIdxList);

    ReviewRatingAndCountDto getRatingAndCountByGoodsIdx(final int productIdx);

    Page<GoodsImagesReviewDto> getReviewPhotoImgListV2(final Pageable pageable,
        final Integer goodsIdx, final List<Integer> notInReviewIdxList);
}
