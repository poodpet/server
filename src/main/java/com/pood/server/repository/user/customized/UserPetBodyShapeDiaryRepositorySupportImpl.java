package com.pood.server.repository.user.customized;

import com.pood.server.entity.user.QUserPetBodyShapeDiary;
import com.pood.server.entity.user.UserPetBodyShapeDiary;
import com.querydsl.core.types.ConstantImpl;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.time.LocalDate;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Qualifier;

public class UserPetBodyShapeDiaryRepositorySupportImpl implements
    UserPetBodyShapeDiaryRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;
    private static final QUserPetBodyShapeDiary userPetBodyShapeDiary = QUserPetBodyShapeDiary.userPetBodyShapeDiary;

    public UserPetBodyShapeDiaryRepositorySupportImpl(
        @Qualifier("userQueryFactory") final JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public Optional<UserPetBodyShapeDiary> getUserPetBodyShape(final Integer userPetIdx,
        final LocalDate now) {
        return Optional.ofNullable(jpaQueryFactory.selectFrom(userPetBodyShapeDiary)
            .where(userPetBodyShapeDiary.userPet.idx.eq(userPetIdx))
            .where(Expressions.stringTemplate(
                    "DATE_FORMAT({0}, {1})"
                    , userPetBodyShapeDiary.baseDate, ConstantImpl.create("%Y-%m-%d"))
                .eq(String.valueOf(now)))
            .fetchOne());
    }

}
