package com.pood.server.repository.user;

import com.pood.server.entity.user.UserPet;
import com.pood.server.entity.user.UserPetWeightDiary;
import com.pood.server.repository.user.customized.UserPetWeightDiaryRepositorySupport;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserPetWeightDiaryRepository extends JpaRepository<UserPetWeightDiary, Long>,
    UserPetWeightDiaryRepositorySupport {

    List<UserPetWeightDiary> findAllByUserPetOrderByIdxDesc(final UserPet userPet);

    void deleteAllByUserPetIdx(final int userPetIdx);

    boolean existsByBaseDateAndUserPet(final LocalDate baseDate, final UserPet userPet);


    void deleteByIdxAndUserPetIdx(final long idx, final int userPetidx);

    void deleteByIdxAndUserPet(final long idx, final UserPet userPet);

    Optional<UserPetWeightDiary> findFirstByUserPetIdxAndWeightIsNotNullOrderByBaseDateDesc(
        final Integer userPetIdx);
}