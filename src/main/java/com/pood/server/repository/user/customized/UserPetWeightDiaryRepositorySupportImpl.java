package com.pood.server.repository.user.customized;

import static com.pood.server.entity.user.QUserPetWeightDiary.userPetWeightDiary;

import com.pood.server.entity.user.UserPetWeightDiary;
import com.querydsl.core.types.ConstantImpl;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.time.LocalDate;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Qualifier;

public class UserPetWeightDiaryRepositorySupportImpl implements
    UserPetWeightDiaryRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public UserPetWeightDiaryRepositorySupportImpl(@Qualifier("userQueryFactory") final JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public Optional<UserPetWeightDiary> getUserPetWeight(final int userPetIdx, final LocalDate now) {
        return Optional.ofNullable(jpaQueryFactory.selectFrom(userPetWeightDiary)
            .where(userPetWeightDiary.userPet.idx.eq(userPetIdx))
            .where(Expressions.stringTemplate(
                    "DATE_FORMAT({0}, {1})"
                    , userPetWeightDiary.baseDate, ConstantImpl.create("%Y-%m-%d"))
                .eq(String.valueOf(now)))
            .fetchOne());
    }
}
