package com.pood.server.repository.user;

import com.pood.server.entity.user.UserPetBodyShapeDiary;
import com.pood.server.repository.user.customized.UserPetBodyShapeDiaryRepositorySupport;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserPetBodyShapeDiaryRepository extends JpaRepository<UserPetBodyShapeDiary, Long>,
    UserPetBodyShapeDiaryRepositorySupport {

    Optional<UserPetBodyShapeDiary> findFirstByUserPetIdxAndBodyShapeIsNotNullOrderByBaseDateDesc(final Integer userPetIdx);
}
