package com.pood.server.repository.user.customized;


import com.pood.server.config.querydsl.UserQuerydslRepositorySupport;
import com.pood.server.dto.meta.goods.UserReviewImageDto;
import com.pood.server.entity.meta.EventInfo;
import com.pood.server.entity.user.QUserInfo;
import com.pood.server.entity.user.QUserReview;
import com.pood.server.entity.user.QUserReviewImage;
import com.pood.server.entity.user.QUserToken;
import com.pood.server.entity.user.QUserWish;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserReviewImage;
import com.pood.server.entity.user.UserWish;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;


public class UserInfoRepositoryImpl extends UserQuerydslRepositorySupport implements
    UserInfoCustomRepository {

    private final JPAQueryFactory queryFactory;

    public UserInfoRepositoryImpl(@Qualifier("userQueryFactory") JPAQueryFactory queryFactory) {
        super(UserInfo.class);
        this.queryFactory = queryFactory;
    }

    QUserWish qUserWish = QUserWish.userWish;
    QUserInfo qUserInfo = QUserInfo.userInfo;
    QUserToken qUserToken = QUserToken.userToken;

    QUserReview qUserReview = QUserReview.userReview;
    QUserReviewImage qUserReviewImage = QUserReviewImage.userReviewImage;


    @Override
    public List<UserWish> getUserWishList(final String userUuid) {
        return queryFactory.selectFrom(qUserWish)
            .innerJoin(qUserInfo)
            .on(qUserInfo.idx.eq(qUserWish.userIdx))
            .where(qUserInfo.userUuid.eq(userUuid))
            .fetch();
    }

    @Override
    public UserInfo getUserByToken(String token) {
        return queryFactory.selectFrom(qUserInfo)
            .innerJoin(qUserToken)
            .on(qUserInfo.userUuid.eq(qUserToken.userUuid))
            .where(qUserToken.token.eq(token))
            .fetchOne();
    }

    @Override
    public Optional<UserInfo> getUserByTokenOptional(final String token) {
        return Optional.ofNullable(queryFactory.selectFrom(qUserInfo)
            .innerJoin(qUserToken)
            .on(qUserInfo.userUuid.eq(qUserToken.userUuid))
            .where(qUserToken.token.eq(token))
            .fetchOne());

    }

    @Override
    public Page<UserReviewImageDto.ReviewPhotoList> getReviewPhotoImgList(Integer idx,
        List<Integer> reviewIdxList, Pageable pageable) {

        QueryResults<UserReviewImageDto.ReviewPhotoList> results =
            queryFactory.select(Projections.bean(UserReviewImageDto.ReviewPhotoList.class,
                    qUserReviewImage.idx,
                    qUserReviewImage.url
                )).from(qUserReview)
                .innerJoin(qUserReviewImage)
                .on(qUserReview.idx.eq(qUserReviewImage.userReview.idx))
                .where(qUserReview.goodsIdx.eq(idx))
                .where(notInReviewIdxList(reviewIdxList))
                .orderBy(orderCondition(pageable, UserReviewImage.class, "userReviewImage")) // (1)
                .offset(pageable.getOffset())
                .limit(pageable.getPageSize())
                .fetchResults();

        pageable.getSort();

        long count = results.getTotal();
        return new PageImpl<>(results.getResults(), pageable, count);
    }

    private BooleanExpression notInReviewIdxList(List<Integer> reviewIdxList) {
        if (Objects.isNull(reviewIdxList) || reviewIdxList.isEmpty()) {
            return null;
        }
        return qUserReview.idx.notIn(reviewIdxList);
    }

    private OrderSpecifier[] orderCondition(Pageable pageable, Class clazz, String variable) {
//        PathBuilder<EventInfo> entityPath = new PathBuilder<>(EventInfo.class, "eventInfo");
        PathBuilder<EventInfo> entityPath = new PathBuilder<>(clazz, variable);
        return pageable.getSort() // (2)
            .stream() // (3)
            .map(order -> new OrderSpecifier(Order.valueOf(order.getDirection().name()),
                entityPath.get(order.getProperty()))) // (4)
            .toArray(OrderSpecifier[]::new); // (5)
    }

}

