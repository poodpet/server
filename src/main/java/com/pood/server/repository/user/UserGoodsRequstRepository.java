package com.pood.server.repository.user;

import com.pood.server.entity.user.UserGoodsRequst;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserGoodsRequstRepository extends JpaRepository<UserGoodsRequst, Long> {

    boolean existsByUserInfoIdxAndGoodsIdx(final Integer userIdx, final Integer goodsIdx);
}
