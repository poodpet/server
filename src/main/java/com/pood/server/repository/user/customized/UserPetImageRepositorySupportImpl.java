package com.pood.server.repository.user.customized;

import static com.pood.server.entity.user.QUserPetImage.userPetImage;

import com.pood.server.entity.user.UserPetImage;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;

public class UserPetImageRepositorySupportImpl implements UserPetImageRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public UserPetImageRepositorySupportImpl(
        @Qualifier("userQueryFactory") final JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<UserPetImage> findAllByIdxInRecordBirthDesc(final List<Integer> userPetIdxList) {
        return jpaQueryFactory.selectFrom(userPetImage)
            .where(
                Expressions.list(userPetImage.userPetIdx, userPetImage.recordbirth).in(
                    JPAExpressions.select(
                            Expressions.list(userPetImage.userPetIdx, userPetImage.recordbirth.max()))
                        .from(userPetImage)
                        .groupBy(userPetImage.userPetIdx)
                )
            )
            .where(userPetImage.userPetIdx.in(userPetIdxList))
            .fetch();
    }
}
