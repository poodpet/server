package com.pood.server.repository.user;

import com.pood.server.entity.user.PetGrainSizeJoinView;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetGrainSizeJoinViewRepository extends
    JpaRepository<PetGrainSizeJoinView, Integer> {

    List<PetGrainSizeJoinView> findAllByUserPetIdxIn(final List<Integer> userPetIdx);
}