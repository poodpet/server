package com.pood.server.repository.user.customized;

import com.pood.server.entity.user.UserPetBodyShapeDiary;
import java.time.LocalDate;
import java.util.Optional;

public interface UserPetBodyShapeDiaryRepositorySupport {

    Optional<UserPetBodyShapeDiary> getUserPetBodyShape(final Integer idx, final LocalDate convertWeeklyFridayDate);

}
