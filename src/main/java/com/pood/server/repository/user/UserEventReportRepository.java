package com.pood.server.repository.user;

import com.pood.server.entity.user.UserEventReport;
import com.pood.server.repository.user.customized.UserEventReportRepositorySupport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserEventReportRepository extends JpaRepository<UserEventReport, Long>, UserEventReportRepositorySupport {

    boolean existsByMyInfoIdx(final int myInfoIdx);
}