package com.pood.server.repository.user;

import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserReview;
import com.pood.server.entity.user.UserReviewSue;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserReviewSueRepository extends JpaRepository<UserReviewSue, Long> {

    boolean existsByUserReviewAndUserInfo(final UserReview userReview, final UserInfo userInfo);

    @Query("SELECT m.userReview.idx FROM UserReviewSue m GROUP BY m.userReview.idx HAVING COUNT(m.userReview.idx) > 2")
    List<Integer> getCheckingReviewIdx();

}
