package com.pood.server.repository.user;

import com.pood.server.entity.user.UserPet;
import com.pood.server.repository.user.customized.UserPetCustomRepository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserPetRepository extends JpaRepository<UserPet, Integer>,
    UserPetCustomRepository {

    List<UserPet> findAllByUserIdx(Integer userIdx);

    boolean existsByIdxAndUserIdx(final Integer idx, final Integer userIdx);

    boolean existsBySerialNumber(final String serialNumber);
}
