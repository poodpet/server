package com.pood.server.repository.user.customized;

import com.pood.server.entity.user.UserPetWeightDiary;
import java.time.LocalDate;
import java.util.Optional;

public interface UserPetWeightDiaryRepositorySupport {

    Optional<UserPetWeightDiary> getUserPetWeight(final int userPetIdx, final LocalDate date);
}
