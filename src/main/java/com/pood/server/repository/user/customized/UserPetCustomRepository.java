package com.pood.server.repository.user.customized;


import com.pood.server.entity.user.mapper.pet.UserPetTotalCountMapper;
import java.util.List;

public interface UserPetCustomRepository {

    List<UserPetTotalCountMapper> getUserPetTotalCount();

}
