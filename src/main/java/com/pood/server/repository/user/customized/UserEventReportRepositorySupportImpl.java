package com.pood.server.repository.user.customized;

import static com.pood.server.entity.user.QUserEventReport.userEventReport;

import com.pood.server.entity.user.UserEventReport;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;

public class UserEventReportRepositorySupportImpl implements UserEventReportRepositorySupport {

    private static final int MAX_REPORT_NUMBER = 3;

    private final JPAQueryFactory jpaQueryFactory;

    public UserEventReportRepositorySupportImpl(@Qualifier("userQueryFactory") final JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<UserEventReport> reportUserList() {
        return jpaQueryFactory.selectFrom(userEventReport)
            .groupBy(userEventReport.userInfo.idx)
            .having(userEventReport.userInfo.idx.count().goe(MAX_REPORT_NUMBER))
            .fetch();
    }
}
