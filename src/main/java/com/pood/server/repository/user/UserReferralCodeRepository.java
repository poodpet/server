package com.pood.server.repository.user;

import com.pood.server.entity.user.UserReferralCode;
import com.pood.server.repository.user.customized.UserReferralCodeRepositorySupport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserReferralCodeRepository extends JpaRepository<UserReferralCode, Integer>,
    UserReferralCodeRepositorySupport {

}