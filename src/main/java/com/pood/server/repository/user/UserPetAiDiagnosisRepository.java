package com.pood.server.repository.user;

import com.pood.server.entity.user.UserPetAiDiagnosis;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserPetAiDiagnosisRepository extends JpaRepository<UserPetAiDiagnosis, Integer> {
    List<UserPetAiDiagnosis> findByUserPetIdx(Integer userPetIdx);

    void deleteAllByUserPetIdx(final int userPetIdx);

    List<UserPetAiDiagnosis> findByUserPetIdxOrderByPriority(final int userPetIdx);
}
