package com.pood.server.repository.user;


import com.pood.server.entity.user.UserInfo;
import com.pood.server.repository.user.customized.UserInfoCustomRepository;
import com.pood.server.util.UserStatus;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;


public interface UserInfoRepository extends JpaRepository<UserInfo, Integer>,
    UserInfoCustomRepository, QuerydslPredicateExecutor<UserInfo> {

    Optional<UserInfo> findByUserUuid(String userUuid);

    boolean existsByUserUuid(final String userUuid);

    Integer countByUserUuidNotAndUserPhone(String uuid, String phoneNumber);

    Optional<UserInfo> findByUserEmail(final String userEmail);

    Optional<UserInfo> findByUserEmailAndUserPhoneAndUserName(final String userEmail, final String userPhone, final String userName);

    boolean existsByUserEmail(final String userEmail);

    Optional<UserInfo> findByReferralCode(final String referralCode);

    boolean existsByReferralCode(final String referralCode);

    boolean existsByUserPhone(final String userPhone);

    Optional<UserInfo> findByUserPhoneAndUserName(final String userPhone, final String userName);

    List<UserInfo> findAllByUserStatusIn(List<UserStatus> userStatusList);
}
