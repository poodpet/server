package com.pood.server.repository.user.customized;


import com.pood.server.dto.meta.goods.UserReviewImageDto;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserWish;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserInfoCustomRepository {

    List<UserWish> getUserWishList(String userUuid);

    UserInfo getUserByToken(String token);

    Optional<UserInfo> getUserByTokenOptional(String token);

    Page<UserReviewImageDto.ReviewPhotoList> getReviewPhotoImgList(Integer idx,
        List<Integer> reviewIdxs, Pageable pageable);

}
