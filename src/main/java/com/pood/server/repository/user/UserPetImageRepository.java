package com.pood.server.repository.user;

import com.pood.server.entity.user.UserPetImage;
import com.pood.server.repository.user.customized.UserPetImageRepositorySupport;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserPetImageRepository extends JpaRepository<UserPetImage, Integer>,
    UserPetImageRepositorySupport {

    List<UserPetImage> findByUserPetIdxOrderByIdxDesc(Integer userPetIdx);

    List<UserPetImage> findByUserPetIdxInOrderByIdxDesc(List<Integer> petIdxList);

    void deleteByUserPetIdx(final int userPetIdx);

    Optional<UserPetImage> findByUserPetIdx(final int userPetIdx);

}
