package com.pood.server.repository.user.customized;

import com.pood.server.entity.user.UserReferralCode;
import java.util.List;

public interface UserReferralCodeRepositorySupport {

    List<UserReferralCode> findAllJoinUserInfoId(final int userInfoIdx);
}
