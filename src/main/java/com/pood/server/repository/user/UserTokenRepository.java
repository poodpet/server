package com.pood.server.repository.user;


import com.pood.server.entity.user.UserToken;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserTokenRepository extends JpaRepository<UserToken, Integer> {

    Long countByUserUuidAndToken(String userUuid, String token);

    Optional<UserToken> findByToken(final String token);

    Optional<UserToken> findByUserUuid(final String userUuid);

    boolean existsByToken(String token);
}
