package com.pood.server.repository.user.customized;


import com.pood.server.api.mapper.user.review.ReviewRatingAndCountDto;
import com.pood.server.config.querydsl.UserQuerydslRepositorySupport;
import com.pood.server.dto.user.review.GoodsDetailReviewDto;
import com.pood.server.dto.user.review.GoodsImagesReviewDto;
import com.pood.server.dto.user.review.ReviewPetInfoDto;
import com.pood.server.dto.user.review.ReviewUserInfoDto;
import com.pood.server.entity.meta.EventInfo;
import com.pood.server.entity.user.QUserInfo;
import com.pood.server.entity.user.QUserPet;
import com.pood.server.entity.user.QUserPetImage;
import com.pood.server.entity.user.QUserReview;
import com.pood.server.entity.user.QUserReviewClap;
import com.pood.server.entity.user.QUserReviewImage;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserReview;
import com.pood.server.util.UserStatus;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.CaseBuilder;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;


public class UserReviewRepositoryImpl extends UserQuerydslRepositorySupport implements
    UserReviewCustomRepository {

    private final JPAQueryFactory queryFactory;

    public UserReviewRepositoryImpl(@Qualifier("userQueryFactory") JPAQueryFactory queryFactory) {
        super(UserInfo.class);
        this.queryFactory = queryFactory;
    }

    QUserReview qUserReview = QUserReview.userReview;
    QUserReviewImage qUserReviewImage = QUserReviewImage.userReviewImage;
    QUserInfo qUserInfo = QUserInfo.userInfo;
    QUserPet qUserPet = QUserPet.userPet;
    QUserReviewClap qUserReviewClap = QUserReviewClap.userReviewClap;
    QUserReviewClap qMyReviewClap = new QUserReviewClap("myReviewClap");
    QUserPetImage qUserPetImage = QUserPetImage.userPetImage;

    @Override
    public Page<GoodsDetailReviewDto> getGoodsReviewList(final Pageable pageable,
        final Integer goodsIdx, final Integer userIdx, final List<Integer> notInReviewIdxList) {

        QueryResults<GoodsDetailReviewDto> results =
            queryFactory.select(Projections.bean(GoodsDetailReviewDto.class,
                    qUserReview.idx,
                    qUserReview.reviewText,
                    Projections.bean(ReviewUserInfoDto.class,
                        qUserInfo.idx.as("idx"),
                        qUserInfo.userNickname.as("userNickname"),
                        qUserInfo.userBaseImage.as("userBaseImage")).as("userInfo"),
                    Projections.bean(ReviewPetInfoDto.class,
                        qUserPet.idx.as("idx"),
                        qUserPet.petName.as("petName"),
                        qUserPet.petBirth.as("petBirth"),
                        qUserPet.pscId.as("pscId")).as("userPetInfo"),
                    qUserReviewClap.count().as("likeCount"),
                    new CaseBuilder()
                        .when(qMyReviewClap.idx.eq(0)).then(false)
                        .when(qMyReviewClap.idx.ne(0)).then(true)
                        .otherwise(false).as("clap")
                    , qUserReview.recordbirth.as("recordbirth")
                )).from(qUserReview)
                .innerJoin(qUserInfo)
                .on(qUserReview.userIdx.eq(qUserInfo.idx))
                .leftJoin(qUserPet)
                .on(qUserPet.idx.eq(qUserReview.petIdx))
                .leftJoin(qUserReviewClap)
                .on(qUserReview.idx.eq(qUserReviewClap.userReview.idx))
                .leftJoin(qMyReviewClap)
                .on(qUserReviewClapJoinOn(userIdx))
                .where(qUserReview.goodsIdx.eq(goodsIdx)
                    .and(qUserInfo.userStatus.notIn(UserStatus.TO_RESIGN, UserStatus.RESIGNED)))
                .where(notInReviewIdxs(notInReviewIdxList))
                .groupBy(qUserReview.idx)
                .orderBy(orderCondition(pageable, UserReview.class, "userReview"))
                .offset(pageable.getOffset())
                .limit(pageable.getPageSize())
                .fetchResults();

        pageable.getSort();

        long count = results.getTotal();
        return new PageImpl<>(results.getResults(), pageable, count);

    }

    private OrderSpecifier<Long> getOrder(final Pageable pageable) {

        while (pageable.getSort().stream().iterator().hasNext()) {
            Sort.Order order = pageable.getSort().stream().iterator().next();
            String property = order.getProperty();
            Direction direction = order.getDirection();

        }

        OrderSpecifier<Integer> desc = qUserReview.idx.desc();
        return qUserReviewClap.count().desc();
    }

    private BooleanExpression notInReviewIdxs(List<Integer> reviewIdxList) {
        if (Objects.isNull(reviewIdxList) || reviewIdxList.isEmpty()) {
            return null;
        }
        return qUserReview.idx.notIn(reviewIdxList);
    }

    @Override
    public ReviewRatingAndCountDto getRatingAndCountByGoodsIdx(final int goodsIdx) {
        ReviewRatingAndCountDto reviewRatingAndCountDto = queryFactory.select(Projections.bean(
                ReviewRatingAndCountDto.class,
                qUserReview.rating.avg().as("avgRating"),
                qUserReview.count().as("totalCnt")
            )).from(qUserReview)
            .innerJoin(qUserInfo).on(qUserReview.userIdx.eq(qUserInfo.idx))
            .where(qUserReview.goodsIdx.eq(goodsIdx)
                .and(qUserInfo.userStatus.notIn(UserStatus.TO_RESIGN, UserStatus.RESIGNED)))
            .groupBy(qUserReview.goodsIdx)
            .fetchOne();

        if (Objects.isNull(reviewRatingAndCountDto)) {
            return new ReviewRatingAndCountDto(0d, 0L);
        }

        return reviewRatingAndCountDto.convertAvgRating();
    }

    private String url;

    private ReviewUserInfoDto userInfo;

    private ReviewPetInfoDto userPetInfo;

    private String reviewText;


    @Override
    public Page<GoodsImagesReviewDto> getReviewPhotoImgListV2(Pageable pageable,
        Integer goodsIdx, List<Integer> notInReviewIdxList) {
        QueryResults<GoodsImagesReviewDto> results =
            queryFactory.select(Projections.bean(GoodsImagesReviewDto.class,
                    qUserReviewImage.url,
                    qUserReview.reviewText,
                    Projections.bean(ReviewUserInfoDto.class,
                        qUserInfo.idx.as("idx"),
                        qUserInfo.userNickname.as("userNickname"),
                        qUserInfo.userBaseImage.as("userBaseImage")).as("userInfo"),
                    Projections.bean(ReviewPetInfoDto.class,
                        qUserPet.idx.as("idx"),
                        qUserPet.petName.as("petName"),
                        qUserPet.petBirth.as("petBirth"),
                        qUserPet.pscId.as("pscId")
                    ).as("userPetInfo")
                )).from(qUserReviewImage)
                .innerJoin(qUserReview)
                .on(qUserReview.idx.eq(qUserReviewImage.userReview.idx))
                .innerJoin(qUserInfo)
                .on(qUserReview.userIdx.eq(qUserInfo.idx))
                .leftJoin(qUserPet)
                .on(qUserPet.idx.eq(qUserReview.petIdx))
                .where(qUserReview.goodsIdx.eq(goodsIdx)
                    .and(qUserInfo.userStatus.notIn(UserStatus.TO_RESIGN, UserStatus.RESIGNED)))
                .where(notInReviewIdxs(notInReviewIdxList))
                .orderBy(orderCondition(pageable, UserReview.class, "userReview"))
                .offset(pageable.getOffset())
                .limit(pageable.getPageSize())
                .fetchResults();
        pageable.getSort();
        long count = results.getTotal();
        return new PageImpl<>(results.getResults(), pageable, count);
    }

    private BooleanExpression qUserReviewClapJoinOn(Integer userIdx) {
        BooleanExpression whereOn = qUserReview.idx.eq(qMyReviewClap.userReview.idx);
        if (Objects.nonNull(userIdx)) {
            return whereOn.and(qMyReviewClap.userIdx.eq(userIdx));
        }
        return whereOn.and(qMyReviewClap.userIdx.isNull());
    }


    private OrderSpecifier[] orderCondition(final Pageable pageable, final Class clazz,
        final String variable) {
        PathBuilder<EventInfo> entityPath = new PathBuilder<>(clazz, variable);
        return pageable.getSort()
            .stream()
            .map(order -> {
                if (isLikeDesc(order)) {
                    return qUserReviewClap.count().desc();
                }
                if (isLikeAsc(order)) {
                    return qUserReviewClap.count().asc();
                }
                return new OrderSpecifier(Order.valueOf(order.getDirection().name()),
                    entityPath.get(order.getProperty()));
            })
            .toArray(OrderSpecifier[]::new);
    }

    private boolean isLikeDesc(Sort.Order order) {
        return order.getProperty().equals("like") && Direction.DESC.equals(
            order.getDirection());
    }

    private boolean isLikeAsc(Sort.Order order) {
        return order.getProperty().equals("like") && Direction.ASC.equals(
            order.getDirection());
    }

}

