package com.pood.server.repository.user;

import com.pood.server.entity.user.UserDeliveryAddress;
import com.pood.server.entity.user.UserInfo;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDeliveryAddressRepository extends JpaRepository<UserDeliveryAddress, Integer> {

    Optional<UserDeliveryAddress> findByUserInfoAndDefaultType(final UserInfo userInfo,
        final boolean defaultType);

    List<UserDeliveryAddress> findAllByUserInfo(final UserInfo userInfo);
}