package com.pood.server.repository.user;

import com.pood.server.entity.user.UserEventCustomBlock;
import com.pood.server.entity.user.UserInfo;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserEventCustomBlockRepository extends JpaRepository<UserEventCustomBlock, Long> {

    boolean existsByMyInfoIdxAndEventInfoIdxAndUserInfo(final int myInfoIdx, final Integer eventInfoIdx, final UserInfo userInfo);

    List<UserEventCustomBlock> findAllByMyInfoIdx(final int myInfoIdx);
}