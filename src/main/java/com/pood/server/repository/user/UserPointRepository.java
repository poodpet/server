package com.pood.server.repository.user;

import com.pood.server.entity.user.UserPoint;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserPointRepository extends JpaRepository<UserPoint, Integer> {

    List<UserPoint> findAllByUserUuidAndPointIdx(final String userUuid, final int pointIdx);

    List<UserPoint> findAllByUserUuidAndStatus(final String userUuid, final Integer status);

    List<UserPoint> findAllByUserUuidAndStatusAndExpiredDateBetween(final String userUuid,
        final int status,
        final LocalDateTime nowDate,
        final LocalDateTime expiredDate);

    List<UserPoint> findAllByPointUuidIn(final List<String> pointUuidList);
}