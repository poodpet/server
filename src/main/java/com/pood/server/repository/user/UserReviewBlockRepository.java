package com.pood.server.repository.user;

import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserReview;
import com.pood.server.entity.user.UserReviewBlock;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserReviewBlockRepository extends JpaRepository<UserReviewBlock, Long> {

    boolean existsByUserReviewAndUserInfo(final UserReview userReview, final UserInfo userInfo);

    List<UserReviewBlock> findAllByUserInfo(UserInfo userInfo);

}
