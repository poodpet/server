package com.pood.server.repository.user;

import com.pood.server.entity.user.UserWish;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserWishRepository extends JpaRepository<UserWish, Integer> {

    boolean existsByUserIdxAndGoodsIdx(final int userIdx, final int goodsIdx);

    int deleteByUserIdxAndGoodsIdx(int userIdx, int goodsIdx);

    List<UserWish> findAllByUserIdxAndGoodsIdxIn(final Integer userIdx, final List<Integer> goodsIdx);
}
