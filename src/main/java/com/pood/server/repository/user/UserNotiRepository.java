package com.pood.server.repository.user;

import com.pood.server.entity.user.UserNoti;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserNotiRepository extends JpaRepository<UserNoti, Integer> {

    List<UserNoti> findAllByUserUuidOrderByIdxDesc(final String userUuid);
}