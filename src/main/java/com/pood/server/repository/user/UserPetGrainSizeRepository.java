package com.pood.server.repository.user;

import com.pood.server.entity.user.UserPetGrainSize;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserPetGrainSizeRepository extends JpaRepository<UserPetGrainSize, Integer> {

    List<UserPetGrainSize> findAllByUserPetIdx(Integer userPetIdx);

    void deleteAllByUserPetIdx(final int userPetIdx);
}
