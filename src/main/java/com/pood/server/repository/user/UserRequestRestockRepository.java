package com.pood.server.repository.user;

import com.pood.server.entity.user.UserRequestRestock;
import com.pood.server.util.RestockState;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRequestRestockRepository extends JpaRepository<UserRequestRestock, Long> {

    boolean existsByGoodsIdxAndUserUuidAndState(final Integer goodsIdx, final String userUuid, final
        RestockState state);
}
