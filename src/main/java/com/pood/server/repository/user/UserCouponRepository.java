package com.pood.server.repository.user;

import com.pood.server.entity.user.UserCoupon;
import com.pood.server.util.UserCouponStatus;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserCouponRepository extends JpaRepository<UserCoupon, Integer> {

    Optional<UserCoupon> findByUserUuid(final String userUuid);

    boolean existsByUserIdxAndCouponIdxIn(final Integer userIdx, final List<Integer> couponIdx);

    List<UserCoupon> findAllByUserUuidAndStatusAndAvailableTimeGreaterThanEqual(
        final String userUuid,
        final UserCouponStatus status, final LocalDateTime availableTime);
}