package com.pood.server.repository.meta;

import com.pood.server.entity.meta.AiRecommendDiagnosis;
import java.util.Collection;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AiRecommendDiagnosisRepository extends
    JpaRepository<AiRecommendDiagnosis, Integer> {

    List<AiRecommendDiagnosis> findAllByIdxIn(Collection<Integer> idxList);

    List<AiRecommendDiagnosis> findAllByArdGroupCode(final Integer ardGroupCode);
}
