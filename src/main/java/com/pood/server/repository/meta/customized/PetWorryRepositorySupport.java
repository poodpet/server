package com.pood.server.repository.meta.customized;

import com.pood.server.dto.meta.worry.PetWorryReadDto;
import com.pood.server.entity.meta.mapper.AiRecommendDiagnosisMapper;
import java.util.List;

public interface PetWorryRepositorySupport {

    List<PetWorryReadDto> getAllByPetCategoryOrderByPriority(final int petCategoryIdx);

    AiRecommendDiagnosisMapper getDiagnosisInfoByWorryIdx(final Long worryIdx);

}
