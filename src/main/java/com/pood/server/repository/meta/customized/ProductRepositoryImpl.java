package com.pood.server.repository.meta.customized;


import com.pood.server.config.OrderByNull;
import com.pood.server.config.querydsl.MetaQuerydslRepositorySupport;
import com.pood.server.dto.meta.product.ProductDto;
import com.pood.server.entity.meta.Product;
import com.pood.server.entity.meta.QAiRecommendDiagnosis;
import com.pood.server.entity.meta.QBrand;
import com.pood.server.entity.meta.QFeed;
import com.pood.server.entity.meta.QGoods;
import com.pood.server.entity.meta.QGoodsImage;
import com.pood.server.entity.meta.QGoodsProduct;
import com.pood.server.entity.meta.QGoodsSubCt;
import com.pood.server.entity.meta.QGoodsSubCtGroup;
import com.pood.server.entity.meta.QPetDoctorFeedDescription;
import com.pood.server.entity.meta.QPetWorry;
import com.pood.server.entity.meta.QProduct;
import com.pood.server.entity.meta.QProductImage;
import com.pood.server.entity.meta.QSeller;
import com.pood.server.entity.meta.QSnack;
import com.pood.server.entity.meta.mapper.ProductIngredientMapper;
import com.pood.server.entity.meta.mapper.QProductIngredientMapper;
import com.pood.server.entity.meta.mapper.solution.ProductGoodsBaseDataMapper;
import com.pood.server.entity.meta.mapper.solution.QProductGoodsBaseDataMapper;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.ProductType;
import com.pood.server.util.FeedType;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.CaseBuilder;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.ObjectUtils;


public class ProductRepositoryImpl extends MetaQuerydslRepositorySupport implements
    ProductCustomRepository {

    private static final int STOP = 0;
    private static final int IS_SALE = 1;
    private static final int COMMON = 0;
    private static final int PAUSE = 2;
    private static final long POOD_MARKET = 5L;
    private static final int NEWST_DAY = 120;

    private final JPAQueryFactory queryFactory;

    public ProductRepositoryImpl(@Qualifier("metaQueryFactory") JPAQueryFactory queryFactory) {
        super(Product.class);
        this.queryFactory = queryFactory;
    }

    QProduct qProduct = QProduct.product;
    QProductImage qProductImage = QProductImage.productImage;
    QGoodsImage qGoodsImage = QGoodsImage.goodsImage;

    QGoodsProduct qGoodsProduct = QGoodsProduct.goodsProduct;
    QBrand qBrand = QBrand.brand;
    QSeller qSeller = QSeller.seller;
    QFeed qFeed = QFeed.feed;
    QGoods qGoods = QGoods.goods;
    QPetDoctorFeedDescription qPetDoctorFeedDescription = QPetDoctorFeedDescription.petDoctorFeedDescription;
    QSnack qSnack = QSnack.snack;
    QGoodsSubCtGroup qGoodsSubCtGroup = QGoodsSubCtGroup.goodsSubCtGroup;
    QGoodsSubCt qGoodsSubCt = QGoodsSubCt.goodsSubCt;

    QPetWorry qPetWorry = QPetWorry.petWorry;
    QAiRecommendDiagnosis qAiRecommendDiagnosis = QAiRecommendDiagnosis.aiRecommendDiagnosis;

    @Override
    public List<ProductDto.ProductDetail> getProductListByGoodsIdx(Integer goodsIdx) {
        JPAQuery<ProductDto.ProductDetail> results = queryFactory.select(
                Projections.bean(ProductDto.ProductDetail.class,
                    qProduct.idx,
                    qBrand.idx.as("brandIdx"),
                    qProduct.productName,
                    qProduct.quantity,
                    qGoodsProduct.productQty,
                    qProduct.ingredients,
                    qSeller.idx.as("sellerIdx"),
                    qProduct.ctSubIdx,
                    qProduct.ctIdx
                )).from(qProduct)
            .innerJoin(qGoodsProduct)
            .on(qProduct.idx.eq(qGoodsProduct.productIdx))
            .innerJoin(qBrand)
            .on(qBrand.idx.eq(qProduct.brand.idx))
            .innerJoin(qSeller)
            .on(qSeller.idx.eq(qProduct.seller.idx))
            .where(qGoodsProduct.goodsIdx.eq(goodsIdx));
        return results.fetchResults().getResults();
    }

    @Override
    public List<Product> getAnalysisSuppliesProductList(Integer ctIdx, Integer ctSubIdx,
        Boolean approve, Integer isRecommend, List<Integer> pcIdx, Integer brandIdx,
        List<Integer> discontinuedProductIdxList) {
        JPAQuery<Product> results = queryFactory.select(Projections.bean(Product.class,
                qProduct.idx,
                qProduct.uuid,
                qProduct.productName,
                qProduct.productCode,
                qProduct.barcode,
                qProduct.pcIdx,
                qProduct.ctIdx,
                qProduct.ctSubIdx,
                qProduct.ctSubName,
                qProduct.showIndex,
                qProduct.tag,
                qProduct.allNutrients,
                qProduct.mainProperty,
                qProduct.tasty,
                qProduct.productVideo,
                qProduct.calorie,
                qProduct.feedTarget,
                qProduct.feedType,
                qProduct.glutenFree,
                qProduct.animalProtein,
                qProduct.vegetableProtein,
                qProduct.aafco,
                qProduct.singleProtein,
                qProduct.ingredients,
                qProduct.unitSize,
                qProduct.ingredientsSearch,
                qProduct.noticeDesc,
                qProduct.noticeTitle,
                qProduct.packageType,
                qProduct.cupWeight,
                qProduct.isRecommend,
                qProduct.grainFree,
                qProduct.avaQuantity,
                qProduct.quantity,
                qProduct.weight,
                qProduct.updatetime,
                qProduct.recordbirth,
                qProduct.approve,
                qSeller.as("seller"),
                qBrand.as("brand")
            )).from(qProduct)
            .innerJoin(qBrand)
            .on(qBrand.idx.eq(qProduct.brand.idx))
            .innerJoin(qSeller)
            .on(qSeller.idx.eq(qProduct.seller.idx))
            .where(qProduct.ctIdx.eq(ctIdx)
                .and(qProduct.ctSubIdx.ne(ctSubIdx))
                .and(qProduct.approve.eq(approve))
                .and(qProduct.isRecommend.eq(isRecommend))
                .and(qProduct.pcIdx.in(pcIdx))
                .and(qProduct.brand.idx.ne(brandIdx)
                    .and(qProduct.idx.notIn(discontinuedProductIdxList)))
            );
        return results.fetchResults().getResults();
    }

    @Override
    public List<Product> getAnalysisProductList(Integer ctIdx, Boolean approve, Integer isRecommend,
        List<Integer> pcIdx, Integer brandIdx, List<Integer> discontinuedProductIdxList) {
        JPAQuery<Product> results = queryFactory.select(Projections.bean(Product.class,
                qProduct.idx,
                qProduct.uuid,
                qProduct.productName,
                qProduct.productCode,
                qProduct.barcode,
                qProduct.pcIdx,
                qProduct.ctIdx,
                qProduct.ctSubIdx,
                qProduct.ctSubName,
                qProduct.showIndex,
                qProduct.tag,
                qProduct.allNutrients,
                qProduct.mainProperty,
                qProduct.tasty,
                qProduct.productVideo,
                qProduct.calorie,
                qProduct.feedTarget,
                qProduct.feedType,
                qProduct.glutenFree,
                qProduct.animalProtein,
                qProduct.vegetableProtein,
                qProduct.aafco,
                qProduct.singleProtein,
                qProduct.ingredients,
                qProduct.unitSize,
                qProduct.ingredientsSearch,
                qProduct.noticeDesc,
                qProduct.noticeTitle,
                qProduct.packageType,
                qProduct.cupWeight,
                qProduct.isRecommend,
                qProduct.grainFree,
                qProduct.avaQuantity,
                qProduct.quantity,
                qProduct.weight,
                qProduct.updatetime,
                qProduct.recordbirth,
                qProduct.approve,
                qSeller.as("seller"),
                qBrand.as("brand"),
                qFeed,
                qPetDoctorFeedDescription
            )).from(qProduct)
            .innerJoin(qBrand)
            .on(qBrand.idx.eq(qProduct.brand.idx))
            .innerJoin(qSeller)
            .on(qSeller.idx.eq(qProduct.seller.idx))
            .innerJoin(qFeed)
            .on(qFeed.productIdx.eq(qProduct.idx))
            .innerJoin(qPetDoctorFeedDescription)
            .on(qPetDoctorFeedDescription.productIdx.eq(qProduct.idx))
            .where(qProduct.ctIdx.eq(ctIdx)
                .and(qProduct.approve.eq(approve))
                .and(qProduct.isRecommend.eq(isRecommend))
                .and(qProduct.pcIdx.in(pcIdx))
                .and(qProduct.brand.idx.ne(brandIdx)
                    .and(qProduct.idx.notIn(discontinuedProductIdxList)))
            );
        return results.fetchResults().getResults();
    }

    public List<ProductDto.DiscontinuedProductList> getGoodsOfProductCountListAndPoodMarketNotIn() {
        JPAQuery<ProductDto.DiscontinuedProductList> results = queryFactory.select(
                Projections.bean(ProductDto.DiscontinuedProductList.class,
                    qProduct.idx,
                    qGoods.idx.count().as("count")
                )).from(qProduct)
            .leftJoin(qGoodsProduct)
            .on(qProduct.idx.eq(qGoodsProduct.productIdx))
            .leftJoin(qGoods)
            .on(qGoods.idx.eq(qGoodsProduct.goodsIdx).and(
                (qGoods.saleStatus.eq(1)
                    .and(qGoods.isRecommend.eq(Boolean.TRUE))))
            ).leftJoin(qGoodsSubCtGroup)
            .on(qGoodsSubCtGroup.goods.idx.eq(qGoods.idx))
            .leftJoin(qGoodsSubCt)
            .on(qGoodsSubCt.idx.eq(qGoodsSubCtGroup.goodsSubCt.idx))
            .where(qGoodsSubCt.idx.ne(POOD_MARKET))
            .groupBy(qProduct.idx);
        return results.fetchResults().getResults();
    }

    @Override
    public ProductBaseData getSolutionProductDetail(final int goodsIdx, final int productIdx) {
        return queryFactory.select(
                getProductBaseDataColumn()).from(qGoodsProduct)
            .innerJoin(qProduct)
            .on(qGoodsProduct.productIdx.eq(qProduct.idx))

            .innerJoin(qGoods)
            .on(qGoods.idx.eq(qGoodsProduct.goodsIdx))

            .leftJoin(qFeed)
            .on(qFeed.productIdx.eq(qProduct.idx))

            .leftJoin(qPetDoctorFeedDescription)
            .on(qPetDoctorFeedDescription.productIdx.eq(qProduct.idx))

            .leftJoin(qSnack).on(qSnack.productIdx.eq(qProduct.idx))

            .where(qGoods.idx.eq(goodsIdx)
                .and(qProduct.idx.eq(productIdx)))
            .fetchOne();
    }

    @Override
    public List<ProductBaseData> getProductInfoListByGoodsIdx(final int goodsIdx) {
        return queryFactory.select(
                getProductBaseDataColumn()).from(qGoodsProduct)
            .innerJoin(qProduct)
            .on(qGoodsProduct.productIdx.eq(qProduct.idx))
            .innerJoin(qGoods)
            .on(qGoods.idx.eq(qGoodsProduct.goodsIdx)
                .and((qGoods.saleStatus.ne(PAUSE)
                    .and(qGoods.saleStatus.ne(STOP))
                    .and(qGoods.isRecommend.eq(true)))
                    .and(qGoods.mainProduct.eq(qProduct.idx)))
            )
            .leftJoin(qFeed).on(qFeed.productIdx.eq(qProduct.idx))
            .leftJoin(qPetDoctorFeedDescription)
            .on(qPetDoctorFeedDescription.productIdx.eq(qProduct.idx))
            .leftJoin(qSnack).on(qSnack.productIdx.eq(qProduct.idx))
            .where(qGoods.idx.eq(goodsIdx))
            .fetch();
    }

    @Override
    public List<ProductBaseData> getAllProductsOnStock(final int pcIdx,
        final List<Integer> productIdxNotIn) {
        JPAQuery<ProductBaseData> results = queryFactory.select(
                getProductBaseDataColumn()).from(qGoodsProduct)
            .innerJoin(qProduct)
            .on(qGoodsProduct.productIdx.eq(qProduct.idx))
            .innerJoin(qGoods)
            .on(qGoods.idx.eq(qGoodsProduct.goodsIdx)
                .and((qGoods.saleStatus.eq(IS_SALE)
                    .and(qGoods.isRecommend.eq(true)))
                    .and(qGoods.mainProduct.eq(qProduct.idx)))
            )
            .leftJoin(qFeed).on(qFeed.productIdx.eq(qProduct.idx))
            .leftJoin(qPetDoctorFeedDescription)
            .on(qPetDoctorFeedDescription.productIdx.eq(qProduct.idx))
            .leftJoin(qSnack).on(qSnack.productIdx.eq(qProduct.idx))
            .where(qProduct.approve.eq(true)
                .and(qGoods.pcIdx.eq(pcIdx)))
            .where(productNotIn(productIdxNotIn));
        return results.fetchResults().getResults();
    }

    private BooleanExpression productNotIn(final List<Integer> productIdxNotIn) {
        if (ObjectUtils.isEmpty(productIdxNotIn)) {
            return null;
        }
        return qProduct.idx.notIn(productIdxNotIn);
    }

    @Override
    public List<ProductBaseData> getProductInfoListByWorryIdx(final int pcIdx,
        final String worryIdx) {
        JPAQuery<ProductBaseData> results = queryFactory.select(
                getProductBaseDataColumn()).from(qGoodsProduct)
            .innerJoin(qProduct)
            .on(qGoodsProduct.productIdx.eq(qProduct.idx))
            .innerJoin(qGoods)
            .on(qGoods.idx.eq(qGoodsProduct.goodsIdx)
                .and((qGoods.saleStatus.ne(PAUSE)
                    .and(qGoods.saleStatus.ne(STOP))
                    .and(qGoods.isRecommend.eq(true)))
                    .and(qGoods.mainProduct.eq(qProduct.idx)))
            )
            .leftJoin(qFeed)
            .on(qFeed.productIdx.eq(qProduct.idx))
            .leftJoin(qPetDoctorFeedDescription)
            .on(qPetDoctorFeedDescription.productIdx.eq(qProduct.idx))
            .leftJoin(qSnack)
            .on(qSnack.productIdx.eq(qProduct.idx))
            .where(qProduct.approve.eq(true)
                .and(qGoods.pcIdx.eq(pcIdx)))
            .where(
                isMatchingArdGroupCode(getAiRecommendDiagnosisByWorryIdx(Long.valueOf(worryIdx))));
        return results.fetchResults().getResults();
    }

    @Override
    public List<ProductBaseData> getProductInfoListByBestWorryScoreFilter(final int pcIdx,
        final String worryIdx, final FeedType feedType) {
        JPAQuery<ProductBaseData> results = queryFactory.select(
                getProductBaseDataColumn()).from(qGoodsProduct)
            .innerJoin(qProduct)
            .on(qGoodsProduct.productIdx.eq(qProduct.idx))
            .innerJoin(qGoods)
            .on(qGoods.idx.eq(qGoodsProduct.goodsIdx)
                .and((qGoods.saleStatus.ne(PAUSE)
                    .and(qGoods.saleStatus.ne(STOP))
                    .and(qGoods.isRecommend.eq(true)))
                    .and(qGoods.mainProduct.eq(qProduct.idx)))
            )
            .leftJoin(qFeed)
            .on(qFeed.productIdx.eq(qProduct.idx))
            .leftJoin(qPetDoctorFeedDescription)
            .on(qPetDoctorFeedDescription.productIdx.eq(qProduct.idx))
            .leftJoin(qSnack)
            .on(qSnack.productIdx.eq(qProduct.idx))
            .where(qProduct.approve.eq(true)
                .and(qGoods.pcIdx.eq(pcIdx)))
            .where(feedTypeIn(feedType))
            .where(
                isMatchingArdGroupCode(getAiRecommendDiagnosisByWorryIdx(Long.valueOf(worryIdx))));
        return results.fetchResults().getResults();
    }

    @Override
    public Optional<List<ProductGoodsBaseDataMapper>> getProductInfoListByBestWorryScoreFilterNotIn(final int pcIdx,
        final Integer ardCode, final FeedType feedType, final Long goodsCtIdx,
        final List<Long> goodsSubCtIdxList, final List<Integer> alreadyIncludeGoodsIdxList) {

        return Optional.ofNullable(queryFactory
            .select(new QProductGoodsBaseDataMapper(
                qGoods.idx,
                qGoods.pcIdx,
                qGoods.displayType,
                qGoods.goodsName,
                qGoods.goodsOriginPrice,
                qGoods.goodsPrice,
                qGoods.discountRate,
                qGoods.discountPrice,
                qGoods.saleStatus,
                qGoods.mainProduct,
                qGoods.averageRating,
                getMainImage(),
                qGoods.isRecommend,
                qBrand.brandName,
                qGoods.recordbirth.after(LocalDateTime.now().minusDays(NEWST_DAY)).as("isNewest"),
                qGoodsSubCt.goodsCt.idx,
                qGoodsSubCt.idx,
                qProduct.mainProperty,
                qProduct.tasty,
                qProduct.calorie,
                qProduct.feedTarget,
                qProduct.feedType,
                qProduct.glutenFree,
                qProduct.animalProtein,
                qProduct.vegetableProtein,
                qProduct.aafco,
                qProduct.singleProtein,
                qProduct.unitSize,
                qProduct.ingredientsSearch,
                qProduct.cupWeight,
                qProduct.grainFree
            ))
            .from(qGoods)
            .innerJoin(qProduct)
            .on(qGoods.mainProduct.eq(qProduct.idx)
                .and(qGoods.saleStatus.ne(PAUSE))
                .and(qGoods.saleStatus.ne(STOP))
            )
            .innerJoin(qBrand)
            .on(qProduct.brand.idx.eq(qBrand.idx))
            .leftJoin(qGoodsImage)
            .on(qGoods.idx.eq(qGoodsImage.goodsIdx))
            .leftJoin(qProductImage)
            .on(qProduct.idx.eq(qProductImage.productIdx)
                .and(qProductImage.type.eq(0))
                .and(qProductImage.priority.eq(0))
            )
            .innerJoin(qGoodsSubCtGroup)
            .on(qGoods.idx.eq(qGoodsSubCtGroup.goods.idx))
            .innerJoin(qGoodsSubCt)
            .on(qGoodsSubCtGroup.goodsSubCt.idx.eq(qGoodsSubCt.idx))
            .leftJoin(qFeed)
            .on(qFeed.productIdx.eq(qProduct.idx))
            .leftJoin(qPetDoctorFeedDescription)
            .on(qPetDoctorFeedDescription.productIdx.eq(qProduct.idx))
            .leftJoin(qSnack)
            .on(qSnack.productIdx.eq(qProduct.idx))
            .where(
                qProduct.approve.eq(true),
                qGoods.pcIdx.eq(pcIdx),
                qGoods.idx.notIn(alreadyIncludeGoodsIdxList),
                inGoodsSubCt(goodsSubCtIdxList),
                feedTypeIn(feedType),
                eqGoodsCtIdx(goodsCtIdx),
                isMatchingArdGroupCode(ardCode)
            )
            .groupBy(qGoods.idx)
            .orderBy(sortByArdCodeField(ardCode))
            .fetch());
    }

    private StringExpression getMainImage() {
        return new CaseBuilder()
            .when(qGoodsImage.count().gt(0L)).then(qGoodsImage.url)
            .when(qProductImage.count().gt(0L)).then(qProductImage.url)
            .otherwise("").as("mainImage");
    }

    private BooleanExpression eqGoodsCtIdx(final Long goodsCtIdx) {
        if (Objects.isNull(goodsCtIdx)) {
            return null;
        }
        return qGoodsSubCt.goodsCt.idx.eq(goodsCtIdx);
    }

    private BooleanExpression inGoodsSubCt(final List<Long> goodsSubCtIdxList) {
        if (ObjectUtils.isEmpty(goodsSubCtIdxList)) {
            return null;
        }
        return qGoodsSubCtGroup.goodsSubCt.idx.in(goodsSubCtIdxList);
    }

    private OrderSpecifier<?> sortByArdCodeField(final Integer ardGroupCode) {

        Order order = Order.DESC;

        if (Objects.isNull(ardGroupCode)) {
            return OrderByNull.getDefault();
        }
        if (ardGroupCode == 122) {
            return new OrderSpecifier<>(order, qPetDoctorFeedDescription.ardGroup122);
        }
        if (ardGroupCode == 421) {
            return new OrderSpecifier<>(order, qPetDoctorFeedDescription.ardGroup421);
        }
        if (ardGroupCode == 501) {
            return new OrderSpecifier<>(order, qPetDoctorFeedDescription.ardGroup501);
        }
        if (ardGroupCode == 201) {
            return new OrderSpecifier<>(order, qPetDoctorFeedDescription.ardGroup201);
        }
        if (ardGroupCode == 301) {
            return new OrderSpecifier<>(order, qPetDoctorFeedDescription.ardGroup301);
        }
        if (ardGroupCode == 961) {
            return new OrderSpecifier<>(order, qPetDoctorFeedDescription.ardGroup961);
        }
        if (ardGroupCode == 241) {
            return new OrderSpecifier<>(order, qPetDoctorFeedDescription.ardGroup241);
        }
        if (ardGroupCode == 121) {
            return new OrderSpecifier<>(order, qPetDoctorFeedDescription.ardGroup121);
        }
        if (ardGroupCode == 941) {
            return new OrderSpecifier<>(order, qPetDoctorFeedDescription.ardGroup941);
        }
        if (ardGroupCode == 125) {
            return new OrderSpecifier<>(order, qPetDoctorFeedDescription.ardGroup125);
        }
        if (ardGroupCode == 942) {
            return new OrderSpecifier<>(order, qPetDoctorFeedDescription.ardGroup942);
        }
        if (ardGroupCode == 126) {
            return new OrderSpecifier<>(order, qPetDoctorFeedDescription.ardGroup126);
        }
        if (ardGroupCode == 601) {
            return new OrderSpecifier<>(order, qPetDoctorFeedDescription.ardGroup601);
        }
        if (ardGroupCode == 422) {
            return new OrderSpecifier<>(order, qPetDoctorFeedDescription.ardGroup422);
        }
        if (ardGroupCode == 423) {
            return new OrderSpecifier<>(order, qPetDoctorFeedDescription.ardGroup423);
        }
        if (ardGroupCode == 502) {
            return new OrderSpecifier<>(order, qPetDoctorFeedDescription.ardGroup502);
        }
        if (ardGroupCode == 145) {
            return new OrderSpecifier<>(order, qPetDoctorFeedDescription.ardGroup145);
        }
        return null;
    }

    private Integer getAiRecommendDiagnosisByWorryIdx(final Long worryIdx) {
        return queryFactory.select(
                qAiRecommendDiagnosis.ardGroupCode
            ).from(qAiRecommendDiagnosis)
            .innerJoin(qPetWorry)
            .on(qAiRecommendDiagnosis.idx.eq(qPetWorry.aiRecommendDiagnosis.idx))
            .where(qPetWorry.id.eq(worryIdx))
            .fetchOne();
    }

    private BooleanExpression isMatchingArdGroupCode(final Integer ardGroupCode) {

        if (Objects.isNull(ardGroupCode)) {
            return null;
        }
        if (ardGroupCode == 122) {
            return qPetDoctorFeedDescription.ardGroup122.gt(0);
        }
        if (ardGroupCode == 421) {
            return qPetDoctorFeedDescription.ardGroup421.gt(0);
        }
        if (ardGroupCode == 501) {
            return qPetDoctorFeedDescription.ardGroup501.gt(0);
        }
        if (ardGroupCode == 201) {
            return qPetDoctorFeedDescription.ardGroup201.gt(0);
        }
        if (ardGroupCode == 301) {
            return qPetDoctorFeedDescription.ardGroup301.gt(0);
        }
        if (ardGroupCode == 961) {
            return qPetDoctorFeedDescription.ardGroup961.gt(0);
        }
        if (ardGroupCode == 241) {
            return qPetDoctorFeedDescription.ardGroup241.gt(0);
        }
        if (ardGroupCode == 121) {
            return qPetDoctorFeedDescription.ardGroup121.gt(0);
        }
        if (ardGroupCode == 941) {
            return qPetDoctorFeedDescription.ardGroup941.gt(0);
        }
        if (ardGroupCode == 125) {
            return qPetDoctorFeedDescription.ardGroup125.gt(0);
        }
        if (ardGroupCode == 942) {
            return qPetDoctorFeedDescription.ardGroup942.gt(0);
        }
        if (ardGroupCode == 126) {
            return qPetDoctorFeedDescription.ardGroup126.gt(0);
        }
        if (ardGroupCode == 601) {
            return qPetDoctorFeedDescription.ardGroup601.gt(0);
        }
        if (ardGroupCode == 422) {
            return qPetDoctorFeedDescription.ardGroup422.gt(0);
        }
        if (ardGroupCode == 423) {
            return qPetDoctorFeedDescription.ardGroup423.gt(0);
        }
        if (ardGroupCode == 502) {
            return qPetDoctorFeedDescription.ardGroup502.gt(0);
        }
        if (ardGroupCode == 145) {
            return qPetDoctorFeedDescription.ardGroup145.gt(0);
        }
        return null;
    }

    public List<ProductBaseData> getProductInfoListByPcIdxAndProductType(
        final int pcIdx, final ProductType productType) {
        JPAQuery<ProductBaseData> results = queryFactory.select(
                getProductBaseDataColumn()).from(qGoodsProduct)
            .innerJoin(qProduct)
            .on(qGoodsProduct.productIdx.eq(qProduct.idx))
            .innerJoin(qGoods)
            .on(qGoods.idx.eq(qGoodsProduct.goodsIdx)
                .and((qGoods.saleStatus.ne(PAUSE)
                    .and(qGoods.saleStatus.ne(STOP))
                    .and(qGoods.isRecommend.eq(true)))
                    .and(qGoods.mainProduct.eq(qProduct.idx)))
            )
            .leftJoin(qFeed).on(qFeed.productIdx.eq(qProduct.idx))
            .leftJoin(qPetDoctorFeedDescription)
            .on(qPetDoctorFeedDescription.productIdx.eq(qProduct.idx))
            .leftJoin(qSnack).on(qSnack.productIdx.eq(qProduct.idx))
            .where(qProduct.approve.eq(true)
                .and(qGoods.pcIdx.eq(pcIdx))
                .and(qProduct.ctIdx.eq(productType.getIdx()))
            );
        return results.fetchResults().getResults();
    }

    @Override
    public List<ProductBaseData> getProductInfoListByFeedTarget(final int pcIdx,
        final int feedTarget) {
        return queryFactory.select(
                getProductBaseDataColumn()).from(qGoodsProduct)
            .innerJoin(qProduct)
            .on(qGoodsProduct.productIdx.eq(qProduct.idx))
            .innerJoin(qGoods)
            .on(qGoods.idx.eq(qGoodsProduct.goodsIdx)
                .and((qGoods.saleStatus.ne(PAUSE)
                    .and(qGoods.saleStatus.ne(STOP))
                    .and(qGoods.isRecommend.eq(true)))
                    .and(qGoods.mainProduct.eq(qProduct.idx)))
            )
            .leftJoin(qFeed).on(qFeed.productIdx.eq(qProduct.idx))
            .leftJoin(qPetDoctorFeedDescription)
            .on(qPetDoctorFeedDescription.productIdx.eq(qProduct.idx))
            .leftJoin(qSnack).on(qSnack.productIdx.eq(qProduct.idx))
            .where(qProduct.approve.eq(true)
                .and(qGoods.pcIdx.eq(pcIdx))
                .and(qProduct.feedTarget.in(feedTarget, COMMON))
            ).fetch();
    }

    @Override
    public List<ProductBaseData> getProductInfoListByFeedType(final int pcIdx,
        final FeedType petFeedType) {
        return queryFactory.select(
                getProductBaseDataColumn()).from(qGoodsProduct)
            .innerJoin(qProduct)
            .on(qGoodsProduct.productIdx.eq(qProduct.idx))
            .innerJoin(qGoods)
            .on(qGoods.idx.eq(qGoodsProduct.goodsIdx)
                .and((qGoods.saleStatus.ne(PAUSE)
                    .and(qGoods.saleStatus.ne(STOP))
                    .and(qGoods.isRecommend.eq(true)))
                    .and(qGoods.mainProduct.eq(qProduct.idx)))
            )
            .leftJoin(qFeed).on(qFeed.productIdx.eq(qProduct.idx))
            .leftJoin(qPetDoctorFeedDescription)
            .on(qPetDoctorFeedDescription.productIdx.eq(qProduct.idx))
            .leftJoin(qSnack).on(qSnack.productIdx.eq(qProduct.idx))
            .where(qProduct.approve.eq(true)
                .and(qGoods.pcIdx.eq(pcIdx))
                .and(feedTypeIn(petFeedType))
            ).fetch();
    }

    @Override
    public List<ProductBaseData> getProductInfoListByWorryIdxIn(final int pcIdx,
        final List<Long> worryIdxList) {
        return queryFactory.select(
                getProductBaseDataColumn()).from(qGoodsProduct)
            .innerJoin(qProduct)
            .on(qGoodsProduct.productIdx.eq(qProduct.idx))
            .innerJoin(qGoods)
            .on(qGoods.idx.eq(qGoodsProduct.goodsIdx)
                .and((qGoods.saleStatus.ne(PAUSE)
                    .and(qGoods.saleStatus.ne(STOP))
                    .and(qGoods.isRecommend.eq(true)))
                    .and(qGoods.mainProduct.eq(qProduct.idx)))
            )
            .leftJoin(qFeed)
            .on(qFeed.productIdx.eq(qProduct.idx))
            .leftJoin(qPetDoctorFeedDescription)
            .on(qPetDoctorFeedDescription.productIdx.eq(qProduct.idx))
            .leftJoin(qSnack)
            .on(qSnack.productIdx.eq(qProduct.idx))
            .where(qProduct.approve.eq(true)
                .and(qGoods.pcIdx.eq(pcIdx)))
            .where(productWorryIdxIn(worryIdxList))
            .fetch();
    }

    @Override
    public List<ProductBaseData> getProductInfoListByFeedTypeAndProductType(final int pcIdx,
        final FeedType petFeedType, final ProductType productType) {
        return queryFactory.select(
                getProductBaseDataColumn()).from(qGoodsProduct)
            .innerJoin(qProduct)
            .on(qGoodsProduct.productIdx.eq(qProduct.idx))
            .innerJoin(qGoods)
            .on(qGoods.idx.eq(qGoodsProduct.goodsIdx)
                .and((qGoods.saleStatus.ne(PAUSE)
                    .and(qGoods.saleStatus.ne(STOP))
                    .and(qGoods.isRecommend.eq(true)))
                    .and(qGoods.mainProduct.eq(qProduct.idx)))
            )
            .leftJoin(qFeed).on(qFeed.productIdx.eq(qProduct.idx))
            .leftJoin(qPetDoctorFeedDescription)
            .on(qPetDoctorFeedDescription.productIdx.eq(qProduct.idx))
            .leftJoin(qSnack).on(qSnack.productIdx.eq(qProduct.idx))
            .where(qProduct.approve.eq(true)
                .and(qGoods.pcIdx.eq(pcIdx))
                .and(feedTypeIn(petFeedType))
                .and(qProduct.ctIdx.eq(productType.getIdx()))
            ).fetch();
    }

    @Override
    public List<ProductBaseData> getProductInfoListByFeedTargetAndProductTypeAndPoodMarketNotIn(
        final int pcIdx,
        final int feedTarget, final ProductType productType) {
        return queryFactory.select(
                getProductBaseDataColumn()).from(qGoodsProduct)
            .innerJoin(qProduct)
            .on(qGoodsProduct.productIdx.eq(qProduct.idx))
            .innerJoin(qGoods)
            .on(qGoods.idx.eq(qGoodsProduct.goodsIdx)
                .and((qGoods.saleStatus.ne(PAUSE)
                    .and(qGoods.saleStatus.ne(STOP))
                    .and(qGoods.isRecommend.eq(true)))
                    .and(qGoods.mainProduct.eq(qProduct.idx)))
            )
            .leftJoin(qFeed).on(qFeed.productIdx.eq(qProduct.idx))
            .leftJoin(qPetDoctorFeedDescription)
            .on(qPetDoctorFeedDescription.productIdx.eq(qProduct.idx))
            .leftJoin(qSnack).on(qSnack.productIdx.eq(qProduct.idx))
            .leftJoin(qGoodsSubCtGroup)
            .on(qGoodsSubCtGroup.goods.idx.eq(qGoods.idx))
            .leftJoin(qGoodsSubCt)
            .on(qGoodsSubCt.idx.eq(qGoodsSubCtGroup.goodsSubCt.idx))
            .where(qProduct.approve.eq(true)
                .and(qGoods.pcIdx.eq(pcIdx))
                .and(qProduct.feedTarget.in(feedTarget, COMMON))
                .and(qProduct.ctIdx.eq(productType.getIdx()))
                .and(qGoodsSubCt.idx.ne(POOD_MARKET))
            ).fetch();
    }

    @Override
    public List<ProductBaseData> getProductInfoListByFeedTypeAndFoodMartNotIn(final int pcIdx,
        final FeedType petFeedType) {
        return queryFactory.select(
                getProductBaseDataColumn()).from(qGoodsProduct)
            .innerJoin(qProduct)
            .on(qGoodsProduct.productIdx.eq(qProduct.idx))
            .innerJoin(qGoods)
            .on(qGoods.idx.eq(qGoodsProduct.goodsIdx)
                .and((qGoods.saleStatus.ne(PAUSE)
                    .and(qGoods.saleStatus.ne(STOP))
                    .and(qGoods.isRecommend.eq(true)))
                    .and(qGoods.mainProduct.eq(qProduct.idx)))
            )
            .leftJoin(qFeed).on(qFeed.productIdx.eq(qProduct.idx))
            .leftJoin(qPetDoctorFeedDescription)
            .on(qPetDoctorFeedDescription.productIdx.eq(qProduct.idx))
            .leftJoin(qSnack).on(qSnack.productIdx.eq(qProduct.idx))
            .leftJoin(qGoodsSubCtGroup)
            .on(qGoodsSubCtGroup.goods.idx.eq(qGoods.idx))
            .leftJoin(qGoodsSubCt)
            .on(qGoodsSubCt.idx.eq(qGoodsSubCtGroup.goodsSubCt.idx))
            .where(qProduct.approve.eq(true)
                .and(qGoods.pcIdx.eq(pcIdx))
                .and(feedTypeIn(petFeedType))
                .and(qGoodsSubCt.idx.ne(POOD_MARKET))
            ).fetch();
    }

    public List<ProductBaseData> getProductInfoListByPcIdxAndProductTypeAndFoodMartNotIn(
        final int pcIdx, final ProductType productType) {
        JPAQuery<ProductBaseData> results = queryFactory.select(
                getProductBaseDataColumn()).from(qGoodsProduct)
            .innerJoin(qProduct)
            .on(qGoodsProduct.productIdx.eq(qProduct.idx))
            .innerJoin(qGoods)
            .on(qGoods.idx.eq(qGoodsProduct.goodsIdx)
                .and((qGoods.saleStatus.ne(PAUSE)
                    .and(qGoods.saleStatus.ne(STOP))
                    .and(qGoods.isRecommend.eq(true)))
                    .and(qGoods.mainProduct.eq(qProduct.idx)))
            )
            .leftJoin(qFeed).on(qFeed.productIdx.eq(qProduct.idx))
            .leftJoin(qPetDoctorFeedDescription)
            .on(qPetDoctorFeedDescription.productIdx.eq(qProduct.idx))
            .leftJoin(qSnack).on(qSnack.productIdx.eq(qProduct.idx))
            .leftJoin(qGoodsSubCtGroup)
            .on(qGoodsSubCtGroup.goods.idx.eq(qGoods.idx))
            .leftJoin(qGoodsSubCt)
            .on(qGoodsSubCt.idx.eq(qGoodsSubCtGroup.goodsSubCt.idx))
            .where(qProduct.approve.eq(true)
                .and(qGoods.pcIdx.eq(pcIdx))
                .and(qProduct.ctIdx.eq(productType.getIdx())
                    .and(qGoodsSubCt.idx.ne(POOD_MARKET)))
            );
        return results.fetchResults().getResults();
    }

    @Override
    public List<ProductBaseData> getProductInfoListByWorryIdxInAndFoodMartNotIn(final int pcIdx,
        final List<Long> worryIdxList) {
        return queryFactory.select(
                getProductBaseDataColumn()).from(qGoodsProduct)
            .innerJoin(qProduct)
            .on(qGoodsProduct.productIdx.eq(qProduct.idx))
            .innerJoin(qGoods)
            .on(qGoods.idx.eq(qGoodsProduct.goodsIdx)
                .and((qGoods.saleStatus.ne(PAUSE)
                    .and(qGoods.saleStatus.ne(STOP))
                    .and(qGoods.isRecommend.eq(true)))
                    .and(qGoods.mainProduct.eq(qProduct.idx)))
            )
            .leftJoin(qFeed)
            .on(qFeed.productIdx.eq(qProduct.idx))
            .leftJoin(qPetDoctorFeedDescription)
            .on(qPetDoctorFeedDescription.productIdx.eq(qProduct.idx))
            .leftJoin(qSnack)
            .on(qSnack.productIdx.eq(qProduct.idx))
            .leftJoin(qGoodsSubCtGroup)
            .on(qGoodsSubCtGroup.goods.idx.eq(qGoods.idx))
            .leftJoin(qGoodsSubCt)
            .on(qGoodsSubCt.idx.eq(qGoodsSubCtGroup.goodsSubCt.idx))
            .where(qProduct.approve.eq(true)
                .and(qGoods.pcIdx.eq(pcIdx))
                .and(qGoodsSubCt.idx.ne(POOD_MARKET)))
            .where(productWorryIdxIn(worryIdxList))
            .fetch();
    }

    @Override
    public List<ProductIngredientMapper> findAllIngredientInIdxList(
        final List<Integer> productIdxList) {
        return queryFactory.select(new QProductIngredientMapper(
                qProduct.idx,
                qProduct.ingredients
            ))
            .from(qProduct)
            .where(qProduct.idx.in(productIdxList))
            .fetch();
    }

    private BooleanExpression productWorryIdxIn(final List<Long> worryIdxList) {
        if (worryIdxList.isEmpty()) {
            return null;
        }
        BooleanExpression contains = isMatchingArdGroupCode(
            getAiRecommendDiagnosisByWorryIdx(worryIdxList.get(0)));
        for (Long petWorry : worryIdxList) {
            contains = contains.or(
                isMatchingArdGroupCode(getAiRecommendDiagnosisByWorryIdx(petWorry)));
        }
        return contains;
    }

    private BooleanExpression feedTypeIn(final FeedType petFeedType) {
        if (FeedType.PUPPY_ADULT_EXCEPT_SENIOR.equals(petFeedType)) {
            return qProduct.feedType.in(FeedType.ALL.getValue(),
                FeedType.PUPPY_ADULT_EXCEPT_SENIOR.getValue());
        }
        if (FeedType.PUPPY.equals(petFeedType)) {
            return qProduct.feedType.in(FeedType.ALL.getValue(), FeedType.PUPPY.getValue(),
                FeedType.PUPPY_ADULT.getValue());
        }
        if (FeedType.ADULT.equals(petFeedType)) {
            return qProduct.feedType.in(FeedType.ALL.getValue(), FeedType.ADULT.getValue(),
                FeedType.PUPPY_ADULT.getValue(), FeedType.ADULT_SENIOR.getValue());
        }
        return qProduct.feedType.in(FeedType.ALL.getValue(), FeedType.SENIOR.getValue(),
            FeedType.ADULT_SENIOR.getValue());

    }

    @Override
    public ProductBaseData getProductInfoByProductIdx(final int productIdx) {
        return queryFactory.select(
                getProductBaseDataColumn()).from(qGoodsProduct)
            .innerJoin(qProduct)
            .on(qGoodsProduct.productIdx.eq(qProduct.idx))
            .innerJoin(qGoods)
            .on(qGoods.idx.eq(qGoodsProduct.goodsIdx)
                .and((qGoods.saleStatus.ne(PAUSE)
                    .and(qGoods.saleStatus.ne(STOP))))
            )
            .leftJoin(qFeed).on(qFeed.productIdx.eq(qProduct.idx))
            .leftJoin(qPetDoctorFeedDescription)
            .on(qPetDoctorFeedDescription.productIdx.eq(qProduct.idx))
            .leftJoin(qSnack).on(qSnack.productIdx.eq(qProduct.idx))
            .where(qProduct.idx.eq(productIdx))
            .fetchOne();
    }

    private QBean<ProductBaseData> getProductBaseDataColumn() {
        return Projections.bean(ProductBaseData.class,
            qProduct.idx,
            qProduct.uuid,
            qProduct.brand,
            qProduct.productName,
            qProduct.productCode,
            qProduct.barcode,
            qProduct.pcIdx,
            qProduct.ctIdx,
            qProduct.ctSubIdx,
            qProduct.ctSubName,
            qProduct.showIndex,
            qProduct.tag,
            qProduct.allNutrients,
            qProduct.mainProperty,
            qProduct.tasty,
            qProduct.productVideo,
            qProduct.calorie,
            qProduct.feedTarget,
            qProduct.feedType,
            qProduct.glutenFree,
            qProduct.animalProtein,
            qProduct.vegetableProtein,
            qProduct.aafco,
            qProduct.singleProtein,
            qProduct.ingredients,
            qProduct.unitSize,
            qProduct.ingredientsSearch,
            qProduct.noticeDesc,
            qProduct.noticeTitle,
            qProduct.packageType,
            qProduct.cupWeight,
            qProduct.isRecommend,
            qProduct.grainFree,
            qProduct.avaQuantity,
            qProduct.quantity,
            qProduct.weight,
            qGoodsProduct.productQty.as("productQty"),
            qSnack.as("snack"),
            qFeed.as("feed"),
            qPetDoctorFeedDescription.as("petDoctorFeedDescription")
        );
    }
}

