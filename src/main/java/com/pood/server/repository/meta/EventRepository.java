package com.pood.server.repository.meta;

import com.pood.server.entity.meta.EventInfo;
import com.pood.server.repository.meta.customized.EventCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface EventRepository extends JpaRepository<EventInfo, Integer>, EventCustomRepository, QuerydslPredicateExecutor<EventInfo> {

    int countAllByEventTypeIdxAndStatusNotAndStatusNotAndIdxLessThanEqual(final int eventTypeIdx, final int paused,
        final int deleted, final int eventIdx);
}
