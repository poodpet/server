package com.pood.server.repository.meta;

import com.pood.server.entity.meta.Banner;
import com.pood.server.entity.meta.Feed;
import jdk.jshell.execution.LoaderDelegate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface BannerRepository extends JpaRepository<Banner, Integer> {
        List<Banner> findAllByPcIdAndStatusAndStartPeriodBeforeAndEndPeriodAfter(Integer pcIdx, Boolean status, LocalDateTime start, LocalDateTime end);

}
