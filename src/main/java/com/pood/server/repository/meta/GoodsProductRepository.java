package com.pood.server.repository.meta;

import com.pood.server.entity.meta.GoodsProduct;
import com.pood.server.repository.meta.customized.GoodsProductCustomRepository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoodsProductRepository extends JpaRepository<GoodsProduct, Integer>,
    GoodsProductCustomRepository {

    GoodsProduct findByGoodsIdxAndProductIdx(Integer goodsIdx, Integer productIdx);

    List<GoodsProduct> findAllByGoodsIdx(Integer goodsIdx);
}
