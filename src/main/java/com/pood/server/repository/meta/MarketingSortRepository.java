package com.pood.server.repository.meta;

import com.pood.server.entity.meta.MarketingSort;
import com.pood.server.repository.meta.customized.MarketingSortRepositorySupport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MarketingSortRepository extends JpaRepository<MarketingSort, Long>, MarketingSortRepositorySupport {

}
