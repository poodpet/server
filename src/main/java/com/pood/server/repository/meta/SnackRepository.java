package com.pood.server.repository.meta;

import com.pood.server.entity.meta.Snack;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SnackRepository extends JpaRepository<Snack, Integer> {
    Snack findByProductIdx(Integer productIdx);

}
