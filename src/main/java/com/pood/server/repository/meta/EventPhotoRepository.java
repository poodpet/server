package com.pood.server.repository.meta;


import com.pood.server.entity.meta.EventPhoto;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventPhotoRepository extends JpaRepository<EventPhoto, Long> {

    EventPhoto findByIdxAndEventInfoIdxAndUserInfoIdx(Long idx, Integer eventInfoIdx,
        Integer userInfoIdx);

    List<EventPhoto> findByEventInfoIdxAndUserInfoIdx(Integer eventInfoIdx, Integer userInfoIdx);

    int countByEventInfoIdxAndUserInfoIdx(Integer eventInfoIdx, Integer userInfoIdx);
}
