package com.pood.server.repository.meta.customized;


import static com.pood.server.entity.meta.QGoodsProduct.goodsProduct;
import static com.pood.server.entity.meta.QGoodsSubCt.goodsSubCt;
import static com.pood.server.entity.meta.QPetCategory.petCategory;

import com.pood.server.config.querydsl.MetaQuerydslRepositorySupport;
import com.pood.server.dto.meta.goods.GoodsDto;
import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.entity.meta.Goods;
import com.pood.server.entity.meta.GoodsSubCt;
import com.pood.server.entity.meta.GoodsUserReview;
import com.pood.server.entity.meta.QAiRecommendDiagnosis;
import com.pood.server.entity.meta.QBrand;
import com.pood.server.entity.meta.QCustomRankInfo;
import com.pood.server.entity.meta.QGoodsCt;
import com.pood.server.entity.meta.QGoodsImage;
import com.pood.server.entity.meta.QGoodsProduct;
import com.pood.server.entity.meta.QGoodsSubCt;
import com.pood.server.entity.meta.QGoodsSubCtGroup;
import com.pood.server.entity.meta.QGoodsUserReview;
import com.pood.server.entity.meta.QPetDoctorFeedDescription;
import com.pood.server.entity.meta.QPetWorry;
import com.pood.server.entity.meta.QPoodDeliveryCategory;
import com.pood.server.entity.meta.QPoodRankCategory;
import com.pood.server.entity.meta.QProduct;
import com.pood.server.entity.meta.QProductImage;
import com.pood.server.entity.meta.QSeller;
import com.pood.server.entity.meta.mapper.OrderGoodsMapper;
import com.pood.server.entity.meta.mapper.QOrderGoodsMapper;
import com.pood.server.entity.meta.mapper.QSellerMapper;
import com.pood.server.entity.meta.mapper.QSuggestRandomGoodsMapper;
import com.pood.server.entity.meta.mapper.SuggestRandomGoodsMapper;
import com.pood.server.entity.meta.mapper.home.CustomRankInfoMapper;
import com.pood.server.entity.meta.mapper.home.QCustomRankInfoMapper;
import com.pood.server.util.AnimalSize;
import com.pood.server.util.FeedType;
import com.pood.server.util.SaleStatus;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.CaseBuilder;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.NumberExpression;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

public class GoodsRepositoryImpl extends MetaQuerydslRepositorySupport implements
    GoodsCustomRepository {

    private static final int WET_FEED = 1;
    private static final int SMALL_SIZE = 1;
    private static final int MEDIUM_SIZE = 2;

    private static final int LARGE_SIZE = 3;
    private static final int WET_FEED_SIZE = 4;
    private static final double SMALL_MEDIUM_BOUNDARY = 1.0d;
    private static final double MEDIUM_LARGE_BOUNDARY = 1.4d;
    private static final long POOD_MARKET = 1L;
    private static final String SET_PRODUCT = "세트 상품";
    private static final int SALE_STATUS_ON = 1;
    private static final int MAIN = 0;
    private static final long FLASH_MARKET = 2L;
    private static final int PUPPY = 1;
    private static final int ADULT = 2;
    private static final int SENIOR = 3;
    private static final int NEWST_DAY = 120;
    private static final int COMMON_PC_IDX = 0;
    private static final int FEED_PRODUCT_CT = 0;
    private static final long FEED_GOODS_CT = 2L;

    private final JPAQueryFactory queryFactory;

    public GoodsRepositoryImpl(@Qualifier("metaQueryFactory") JPAQueryFactory queryFactory) {
        super(Goods.class);
        this.queryFactory = queryFactory;
    }

    QGoodsUserReview goodsUserReview = QGoodsUserReview.goodsUserReview;
    QProduct qProduct = QProduct.product;
    QGoodsImage qGoodsImage = QGoodsImage.goodsImage;
    QProductImage qProductImage = QProductImage.productImage;
    QGoodsSubCtGroup qGoodsSubCtGroup = QGoodsSubCtGroup.goodsSubCtGroup;
    QGoodsCt qGoodsCt = QGoodsCt.goodsCt;
    QPetDoctorFeedDescription qPetDoctorFeedDescription = QPetDoctorFeedDescription.petDoctorFeedDescription;
    QGoodsSubCt qGoodsSubCt = QGoodsSubCt.goodsSubCt;
    QPoodDeliveryCategory qPoodDeliveryCategory = QPoodDeliveryCategory.poodDeliveryCategory;
    QPoodRankCategory qPoodRankCategory = QPoodRankCategory.poodRankCategory;
    QSeller qSeller = QSeller.seller;
    QPetWorry qPetWorry = QPetWorry.petWorry;
    QAiRecommendDiagnosis qAiRecommendDiagnosis = QAiRecommendDiagnosis.aiRecommendDiagnosis;
    QBrand qBrand = QBrand.brand;
    QCustomRankInfo qCustomRankInfo = QCustomRankInfo.customRankInfo;
    QGoodsProduct qGoodsProduct = goodsProduct;

    @Override
    public Page<GoodsDto.SortedGoodsList> getSortedGoodsList(Pageable pageable, Long goodSubCtIdx,
        String fieldKey, String fieldValue, String searchText, Integer pcIdx, Long goodsCtIdx) {

        QueryResults<GoodsDto.SortedGoodsList> results = queryFactory.select(
                Projections.bean(GoodsDto.SortedGoodsList.class,
                    goodsUserReview.idx,
                    goodsUserReview.pcIdx,
                    goodsUserReview.goodsTypeIdx,
                    goodsUserReview.displayType,
                    goodsUserReview.goodsName,
                    goodsUserReview.goodsOriginPrice,
                    goodsUserReview.goodsPrice,
                    goodsUserReview.discountRate,
                    goodsUserReview.discountPrice,
                    goodsUserReview.saleStatus,
                    goodsUserReview.visible,
                    goodsUserReview.mainProduct,
                    goodsUserReview.averageRating,
                    goodsUserReview.reviewCnt,
                    goodsUserReview.updatetime,
                    goodsUserReview.recordbirth,
                    getMainImage(),
                    qBrand.brandName,
                    goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(NEWST_DAY))
                        .as("newest")
                )).from(goodsUserReview)
            .innerJoin(qGoodsSubCtGroup)
            .on(goodsUserReview.idx.eq(qGoodsSubCtGroup.goods.idx))
            .innerJoin(qGoodsCt)
            .on(qGoodsCt.idx.eq(qGoodsSubCtGroup.goodsSubCt.goodsCt.idx))
            .innerJoin(qProduct)
            .on(goodsUserReview.mainProduct.eq(qProduct.idx))
            .innerJoin(qBrand)
            .on(qProduct.brand.idx.eq(qBrand.idx))
            .leftJoin(qPetDoctorFeedDescription)
            .on(qPetDoctorFeedDescription.productIdx.eq(qProduct.idx))
            .leftJoin(qGoodsImage)
            .on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))
            .leftJoin(qProductImage)
            .on(qProduct.idx.eq(qProductImage.productIdx)
                .and(qProductImage.type.eq(0).and(qProductImage.priority.eq(0))))
            .where(inNormalGoodsStatus())
            .where(eqSearchFiled(fieldKey, fieldValue))
            .where(likeGoodsName(searchText))
            .where(eqGoodSubCtIdx(goodSubCtIdx))
            .where(eqGoodsCtIdx(goodsCtIdx))
            .where(eqPcIdx(pcIdx))
            .groupBy(goodsUserReview.idx)
            .orderBy(orderCondition(pageable)) // (1)
            .offset(pageable.getOffset())
            .limit(pageable.getPageSize())
            .fetchResults();

        pageable.getSort();

        long count = results.getTotal();
        return new PageImpl<>(results.getResults(), pageable, count);
    }

    @Override
    public Page<GoodsDto.SortedGoodsList> goodsSearchList(final Pageable pageable,
        final String searchText, final Integer pcIdx) {

        QueryResults<GoodsDto.SortedGoodsList> results = queryFactory.select(
                Projections.bean(GoodsDto.SortedGoodsList.class,
                    goodsUserReview.idx,
                    goodsUserReview.pcIdx,
                    goodsUserReview.goodsTypeIdx,
                    goodsUserReview.displayType,
                    goodsUserReview.goodsName,
                    goodsUserReview.goodsOriginPrice,
                    goodsUserReview.goodsPrice,
                    goodsUserReview.discountRate,
                    goodsUserReview.discountPrice,
                    goodsUserReview.saleStatus,
                    goodsUserReview.visible,
                    goodsUserReview.mainProduct,
                    goodsUserReview.averageRating,
                    goodsUserReview.reviewCnt,
                    goodsUserReview.updatetime,
                    goodsUserReview.recordbirth,
                    getMainImage(),
                    qBrand.brandName,
                    goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(NEWST_DAY))
                        .as("newest")
                )).from(goodsUserReview)
            .leftJoin(qGoodsImage)
            .on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))
            .innerJoin(qProduct)
            .on(goodsUserReview.mainProduct.eq(qProduct.idx))
            .innerJoin(qBrand)
            .on(qProduct.brand.idx.eq(qBrand.idx))
            .leftJoin(qProductImage)
            .on(qProduct.idx.eq(qProductImage.productIdx)
                .and(qProductImage.type.eq(0).and(qProductImage.priority.eq(0))))
            .where(inNormalGoodsStatus())
            .where(likeGoodsName(searchText))
            .where(eqPcIdx(pcIdx))
            .groupBy(goodsUserReview.idx)
            .orderBy(orderCondition(pageable)) // (1)
            .offset(pageable.getOffset())
            .limit(pageable.getPageSize())
            .fetchResults();

        pageable.getSort();

        long count = results.getTotal();
        return new PageImpl<>(results.getResults(), pageable, count);
    }

    @Override
    public Page<GoodsDto.SortedGoodsList> goodsSearchList(final Pageable pageable,
        final String searchText, final Integer pcIdx, final Long goodsCtIdx) {

        QueryResults<GoodsDto.SortedGoodsList> results = queryFactory.select(
                Projections.bean(GoodsDto.SortedGoodsList.class,
                    goodsUserReview.idx,
                    goodsUserReview.pcIdx,
                    goodsUserReview.goodsTypeIdx,
                    goodsUserReview.displayType,
                    goodsUserReview.goodsName,
                    goodsUserReview.goodsOriginPrice,
                    goodsUserReview.goodsPrice,
                    goodsUserReview.discountRate,
                    goodsUserReview.discountPrice,
                    goodsUserReview.saleStatus,
                    goodsUserReview.visible,
                    goodsUserReview.mainProduct,
                    goodsUserReview.averageRating,
                    goodsUserReview.reviewCnt,
                    goodsUserReview.updatetime,
                    goodsUserReview.recordbirth,
                    getMainImage(),
                    qBrand.brandName,
                    goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(NEWST_DAY))
                        .as("newest"),
                    getWeight()
                )).from(goodsUserReview)
            .innerJoin(qGoodsProduct)
            .on(goodsUserReview.idx.eq(qGoodsProduct.goodsIdx)
                .and(goodsUserReview.mainProduct.eq(qGoodsProduct.productIdx)))
            .innerJoin(qGoodsSubCtGroup)
            .on(goodsUserReview.idx.eq(qGoodsSubCtGroup.goods.idx))
            .innerJoin(qGoodsCt)
            .on(qGoodsCt.idx.eq(qGoodsSubCtGroup.goodsSubCt.goodsCt.idx))
            .leftJoin(qGoodsImage)
            .on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))
            .innerJoin(qProduct)
            .on(goodsUserReview.mainProduct.eq(qProduct.idx))
            .innerJoin(qBrand)
            .on(qProduct.brand.idx.eq(qBrand.idx))
            .leftJoin(qProductImage)
            .on(qProduct.idx.eq(qProductImage.productIdx)
                .and(qProductImage.type.eq(0).and(qProductImage.priority.eq(0))))
            .where(inNormalGoodsStatus(),
                likeGoodsName(searchText),
                eqPcIdx(pcIdx),
                eqGoodsCtIdx(goodsCtIdx)
            )
            .groupBy(goodsUserReview.idx)
            .orderBy(orderCondition(pageable)) // (1)
            .offset(pageable.getOffset())
            .limit(pageable.getPageSize())
            .fetchResults();

        pageable.getSort();

        long count = results.getTotal();
        return new PageImpl<>(results.getResults(), pageable, count);
    }

    private NumberExpression<Integer> getWeight() {
        if (Objects.isNull(qProduct.weight)) {
            return null;
        }
        return qProduct.weight.multiply(qGoodsProduct.productQty).as("weight");
    }

    private BooleanExpression inNormalGoodsStatus() {
        return goodsUserReview.saleStatus.in(
            SaleStatus.SALE.getStatus(),
            SaleStatus.INTERRUPT.getStatus());
    }

    private BooleanExpression eqSaleStatus(final Integer status) {
        if (Objects.isNull(status)) {
            return null;
        }
        return goodsUserReview.saleStatus.eq(status);
    }

    @Override
    public Page<SortedGoodsList> goodsSearchListByFeedComparison(final Pageable pageable,
        final String searchText, final Integer pcIdx) {

        QueryResults<GoodsDto.SortedGoodsList> results = queryFactory.select(
                Projections.bean(GoodsDto.SortedGoodsList.class,
                    goodsUserReview.idx,
                    goodsUserReview.pcIdx,
                    goodsUserReview.goodsTypeIdx,
                    goodsUserReview.displayType,
                    goodsUserReview.goodsName,
                    goodsUserReview.goodsOriginPrice,
                    goodsUserReview.goodsPrice,
                    goodsUserReview.discountRate,
                    goodsUserReview.discountPrice,
                    goodsUserReview.saleStatus,
                    goodsUserReview.visible,
                    goodsUserReview.mainProduct,
                    goodsUserReview.averageRating,
                    goodsUserReview.reviewCnt,
                    goodsUserReview.updatetime,
                    goodsUserReview.recordbirth,
                    getMainImage(),
                    qBrand.brandName,
                    goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(NEWST_DAY))
                        .as("newest"),
                    getWeight()
                )).from(goodsUserReview)
            .innerJoin(qGoodsProduct)
            .on(goodsUserReview.idx.eq(qGoodsProduct.goodsIdx)
                .and(goodsUserReview.mainProduct.eq(qGoodsProduct.productIdx)))
            .innerJoin(qGoodsSubCtGroup)
            .on(goodsUserReview.idx.eq(qGoodsSubCtGroup.goods.idx))
            .innerJoin(qGoodsCt)
            .on(qGoodsCt.idx.eq(qGoodsSubCtGroup.goodsSubCt.goodsCt.idx))
            .leftJoin(qGoodsImage)
            .on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))
            .innerJoin(qProduct)
            .on(goodsUserReview.mainProduct.eq(qProduct.idx))
            .innerJoin(qBrand)
            .on(qProduct.brand.idx.eq(qBrand.idx))
            .leftJoin(qProductImage)
            .on(qProduct.idx.eq(qProductImage.productIdx)
                .and(qProductImage.type.eq(0).and(qProductImage.priority.eq(0))))
            .where(inNormalGoodsStatus().or(eqSaleStatus(SaleStatus.NOT_TREATED.getStatus())),
                likeGoodsName(searchText),
                eqPcIdx(pcIdx),
                qProduct.ctIdx.eq(FEED_PRODUCT_CT)
            )
            .groupBy(goodsUserReview.idx)
            .orderBy(orderCondition(pageable)) // (1)
            .offset(pageable.getOffset())
            .limit(pageable.getPageSize())
            .fetchResults();

        pageable.getSort();

        long count = results.getTotal();
        return new PageImpl<>(results.getResults(), pageable, count);
    }

    @Override
    public Page<SortedGoodsList> goodsFilteredList(final Pageable pageable,
        final List<String> petWorryList, final Long goodsCtIdx, final Long goodsSubCtIdx
        , final List<String> proteinList, final List<Integer> grainSizeList,
        final List<Integer> feedTargetList,
        final int pcIdx, final List<Integer> ageTypeList) {
        final QueryResults<SortedGoodsList> results = queryFactory.select(
                Projections.bean(SortedGoodsList.class,
                    goodsUserReview.idx,
                    goodsUserReview.pcIdx,
                    goodsUserReview.goodsTypeIdx,
                    goodsUserReview.displayType,
                    goodsUserReview.goodsName,
                    goodsUserReview.goodsOriginPrice,
                    goodsUserReview.goodsPrice,
                    goodsUserReview.discountRate,
                    goodsUserReview.discountPrice,
                    goodsUserReview.saleStatus,
                    goodsUserReview.visible,
                    goodsUserReview.mainProduct,
                    goodsUserReview.averageRating,
                    goodsUserReview.reviewCnt,
                    goodsUserReview.updatetime,
                    goodsUserReview.recordbirth,
                    getMainImage(),
                    qBrand.brandName,
                    goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(NEWST_DAY))
                        .as("newest")
                )).from(goodsUserReview)
            .innerJoin(qGoodsSubCtGroup)
            .on(goodsUserReview.idx.eq(qGoodsSubCtGroup.goods.idx))

            .innerJoin(qGoodsSubCt)
            .on(qGoodsSubCt.idx.eq(qGoodsSubCtGroup.goodsSubCt.idx))

            .innerJoin(qGoodsCt)
            .on(qGoodsCt.idx.eq(qGoodsSubCt.goodsCt.idx))

            .innerJoin(goodsProduct)
            .on(goodsProduct.goodsIdx.eq(goodsUserReview.idx))

            .innerJoin(qProduct)
            .on(goodsProduct.productIdx.eq(qProduct.idx))

            .innerJoin(qBrand)
            .on(qProduct.brand.idx.eq(qBrand.idx))

            .leftJoin(qPetDoctorFeedDescription)
            .on(qPetDoctorFeedDescription.productIdx.eq(qProduct.idx))

            .leftJoin(qGoodsImage)
            .on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))

            .leftJoin(qProductImage)
            .on(goodsUserReview.mainProduct.eq(qProductImage.productIdx)
                .and(qProductImage.type.eq(0).and(qProductImage.priority.eq(0))))

            .where(inNormalGoodsStatus())

            .where(goodsUserReview.pcIdx.eq(pcIdx))
            // 카테고리 대분류
            .where(qGoodsCt.idx.eq(goodsCtIdx))
            // 카테고리 중분류
            .where(subCategoryFilter(goodsSubCtIdx))
            // 건강특징 in절
            .where(worryFilter(petWorryList))
            // 단백질 리스트
            .where(proteinFilter(proteinList))
            // 알갱이 사이즈
            .where(grainSizeFilter(grainSizeList))
            //반려견 체구 (개일때만)
            .where(dogSizeFilter(feedTargetList))
            //반려동물 나이
            .where(ageFilter(ageTypeList))
            .groupBy(goodsUserReview.idx)
            //카테고리 상품 조회시 일시품절 상품 후 순위 노출
            .orderBy(new OrderSpecifier<>(Order.ASC, goodsUserReview.saleStatus))
            .orderBy(orderCondition(pageable)) // (1)
            .offset(pageable.getOffset())
            .limit(pageable.getPageSize())
            .fetchResults();

        pageable.getSort();
        long count = results.getTotal();
        return new PageImpl<>(results.getResults(), pageable, count);
    }

    private BooleanExpression ageFilter(final List<Integer> ageTypeList) {
        if (Objects.isNull(ageTypeList)) {
            return null;
        }

        if (ageTypeList.size() == 1) {

            switch (ageTypeList.get(0)) {
                case PUPPY:
                    return qProduct.feedType.eq(FeedType.PUPPY.getValue())
                        .or(qProduct.feedType.eq(FeedType.PUPPY_ADULT.getValue()))
                        .or(qProduct.feedType.eq(FeedType.PUPPY_ADULT_EXCEPT_SENIOR.getValue()))
                        .or(qProduct.feedType.eq(FeedType.ALL.getValue()));
                case ADULT:
                    return qProduct.feedType.eq(FeedType.ADULT.getValue())
                        .or(qProduct.feedType.eq(FeedType.PUPPY_ADULT.getValue()))
                        .or(qProduct.feedType.eq(FeedType.ADULT_SENIOR.getValue()))
                        .or(qProduct.feedType.eq(FeedType.ALL.getValue()));
                case SENIOR:
                    return qProduct.feedType.eq(FeedType.SENIOR.getValue())
                        .or(qProduct.feedType.eq(FeedType.ADULT_SENIOR.getValue()))
                        .or(qProduct.feedType.eq(FeedType.ALL.getValue()));
            }
        }

        if (ageTypeList.size() == 2) {
            if (ageTypeList.containsAll(List.of(PUPPY, ADULT))) {
                return qProduct.feedType.eq(FeedType.PUPPY.getValue())
                    .or(qProduct.feedType.eq(FeedType.PUPPY_ADULT.getValue()))
                    .or(qProduct.feedType.eq(FeedType.PUPPY_ADULT_EXCEPT_SENIOR.getValue()))
                    .or(qProduct.feedType.eq(FeedType.ADULT.getValue()))
                    .or(qProduct.feedType.eq(FeedType.ADULT_SENIOR.getValue()))
                    .or(qProduct.feedType.eq(FeedType.ALL.getValue()));
            }
            if (ageTypeList.containsAll(List.of(PUPPY, SENIOR))) {
                return qProduct.feedType.eq(FeedType.PUPPY.getValue())
                    .or(qProduct.feedType.eq(FeedType.PUPPY_ADULT.getValue()))
                    .or(qProduct.feedType.eq(FeedType.PUPPY_ADULT_EXCEPT_SENIOR.getValue()))
                    .or(qProduct.feedType.eq(FeedType.SENIOR.getValue()))
                    .or(qProduct.feedType.eq(FeedType.ADULT_SENIOR.getValue()))
                    .or(qProduct.feedType.eq(FeedType.ALL.getValue()));
            }
            if (ageTypeList.containsAll(List.of(ADULT, SENIOR))) {
                return qProduct.feedType.eq(FeedType.ADULT.getValue())
                    .or(qProduct.feedType.eq(FeedType.PUPPY_ADULT.getValue()))
                    .or(qProduct.feedType.eq(FeedType.ADULT_SENIOR.getValue()))
                    .or(qProduct.feedType.eq(FeedType.SENIOR.getValue()))
                    .or(qProduct.feedType.eq(FeedType.ALL.getValue()));
            }
        }

        //전체
        return null;
    }

    private BooleanExpression subCategoryFilter(final Long goodsSubCtIdx) {
        if (Objects.isNull(goodsSubCtIdx)) {
            return null;
        }
        return qGoodsSubCt.idx.eq(goodsSubCtIdx);
    }

    private BooleanExpression worryFilter(final List<String> petWorryList) {
        if (Objects.isNull(petWorryList)) {
            return null;
        }
        BooleanExpression contains = isMatchingArdGroupCode(
            getAiRecommendDiagnosisByWorryIdx(Long.valueOf(petWorryList.get(0))));
        for (String petWorry : petWorryList) {
            contains = contains.or(
                isMatchingArdGroupCode(getAiRecommendDiagnosisByWorryIdx(Long.valueOf(petWorry))));
        }
        return contains;
    }

    private BooleanExpression proteinFilter(final List<String> proteinList) {
        if (Objects.isNull(proteinList)) {
            return null;
        }
        return qProduct.mainProperty.in(proteinList);
    }

    private BooleanExpression grainSizeFilter(final List<Integer> grainSizeList) {
        if (Objects.isNull(grainSizeList)) {
            return null;
        }

        if (grainSizeList.size() == 3) {
            if (grainSizeList.contains(WET_FEED_SIZE)) {
                // 작은 + 중간 + 습식 알갱이인 경우 (1.4 미만)
                if (grainSizeList.containsAll(List.of(SMALL_SIZE, MEDIUM_SIZE))) {
                    return qProduct.unitSize.lt(MEDIUM_LARGE_BOUNDARY);
                }

                // 작은 + 큰 + 습식 알갱이인 경우 (1.0 미만 + 1.4 이상)
                if (grainSizeList.containsAll(List.of(SMALL_SIZE, LARGE_SIZE))) {
                    return qProduct.unitSize.lt(SMALL_MEDIUM_BOUNDARY)
                        .and(qProduct.unitSize.goe(MEDIUM_LARGE_BOUNDARY));
                }

                // 중간 + 큰 + 습식 알갱이인 경우 (1.0 이상)
                if (grainSizeList.containsAll(List.of(MEDIUM_SIZE, LARGE_SIZE))) {
                    return qProduct.unitSize.goe(SMALL_MEDIUM_BOUNDARY);
                }
            }

            return qProduct.ctSubIdx.ne(WET_FEED);
        }

        if (grainSizeList.size() == 2) {
            //습식이 존재하지 않는경우
            // 작은 + 중간 알갱이인 경우
            if (grainSizeList.containsAll(List.of(SMALL_SIZE, MEDIUM_SIZE))) {
                return qProduct.unitSize.lt(MEDIUM_LARGE_BOUNDARY)
                    .and(qProduct.ctSubIdx.ne(WET_FEED));
            }

            // 작은 + 큰 알갱이인 경우
            if (grainSizeList.containsAll(List.of(SMALL_SIZE, LARGE_SIZE))) {
                return qProduct.unitSize.lt(SMALL_MEDIUM_BOUNDARY)
                    .and(qProduct.unitSize.goe(MEDIUM_LARGE_BOUNDARY))
                    .and(qProduct.ctSubIdx.ne(WET_FEED));
            }

            // 중간 + 큰 알갱이인 경우
            if (grainSizeList.containsAll(List.of(MEDIUM_SIZE, LARGE_SIZE))) {
                return qProduct.unitSize.goe(SMALL_MEDIUM_BOUNDARY)
                    .and(qProduct.ctSubIdx.ne(WET_FEED));
            }

            //습식인 경우
            if (grainSizeList.contains(WET_FEED_SIZE)) {

                //습식 + 작은 알갱이인 경우
                if (grainSizeList.contains(SMALL_SIZE)) {
                    return qProduct.unitSize.goe(SMALL_MEDIUM_BOUNDARY);
                }

                //습식 + 중간 알갱이인 경우
                if (grainSizeList.contains(MEDIUM_SIZE)) {
                    return qProduct.unitSize.goe(SMALL_MEDIUM_BOUNDARY)
                        .and(qProduct.unitSize.lt(MEDIUM_LARGE_BOUNDARY));
                }

                //습식 + 큰 알갱이인 경우
                if (grainSizeList.contains(LARGE_SIZE)) {
                    return qProduct.unitSize.goe(MEDIUM_LARGE_BOUNDARY);
                }
            }

        }

        if (grainSizeList.size() == 1) {
            switch (grainSizeList.get(0)) {
                case SMALL_SIZE:
                    return qProduct.unitSize.lt(SMALL_MEDIUM_BOUNDARY)
                        .and(qProduct.ctSubIdx.ne(WET_FEED));
                case MEDIUM_SIZE:
                    return qProduct.unitSize.goe(SMALL_MEDIUM_BOUNDARY)
                        .and(qProduct.unitSize.lt(MEDIUM_LARGE_BOUNDARY)
                            .and(qProduct.ctSubIdx.ne(WET_FEED)));
                case LARGE_SIZE:
                    return qProduct.unitSize.goe(MEDIUM_LARGE_BOUNDARY)
                        .and(qProduct.ctSubIdx.ne(WET_FEED));
                case WET_FEED_SIZE:
                    return qProduct.ctSubIdx.eq(WET_FEED);
            }
        }

        return null;
    }


    private BooleanExpression dogSizeFilter(final List<Integer> feedTargetList) {
        if (Objects.isNull(feedTargetList)) {
            return null;
        }
        return qProduct.feedTarget.in(feedTargetList);
    }

    @Override
    public List<GoodsDto.SortedGoodsList> getSortedGoodsInIdxList(final List<Integer> goodsIds) {

        QueryResults<GoodsDto.SortedGoodsList> results =
            queryFactory.select(Projections.bean(GoodsDto.SortedGoodsList.class,
                    goodsUserReview.idx,
                    goodsUserReview.pcIdx,
                    goodsUserReview.goodsTypeIdx,
                    goodsUserReview.displayType,
                    goodsUserReview.goodsName,
                    goodsUserReview.goodsOriginPrice,
                    goodsUserReview.goodsPrice,
                    goodsUserReview.discountRate,
                    goodsUserReview.discountPrice,
                    goodsUserReview.saleStatus,
                    goodsUserReview.visible,
                    goodsUserReview.mainProduct,
                    goodsUserReview.averageRating,
                    goodsUserReview.reviewCnt,
                    goodsUserReview.updatetime,
                    goodsUserReview.recordbirth,
                    getMainImage(),
                    qBrand.brandName,
                    goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(NEWST_DAY))
                        .as("newest")
                )).from(goodsUserReview)
                .innerJoin(qProduct)
                .on(goodsUserReview.mainProduct.eq(qProduct.idx))
                .innerJoin(qBrand)
                .on(qProduct.brand.idx.eq(qBrand.idx))
                .leftJoin(qGoodsImage)
                .on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))
                .leftJoin(qProductImage)
                .on(qProduct.idx.eq(qProductImage.productIdx)
                    .and(qProductImage.type.eq(0).and(qProductImage.priority.eq(0))))
                .where(goodsUserReview.idx.in(goodsIds)
                    .and(inNormalGoodsStatus()))
                .groupBy(goodsUserReview.idx)
                .fetchResults();

        return results.getResults();
    }

    @Override
    public List<GoodsDto.SortedGoodsList> getrRcommendGoodsList(final int productIdx,
        final int pcIdx) {
        QueryResults<GoodsDto.SortedGoodsList> results =
            queryFactory.select(Projections.bean(GoodsDto.SortedGoodsList.class,
                    goodsUserReview.idx,
                    goodsUserReview.pcIdx,
                    goodsUserReview.goodsTypeIdx,
                    goodsUserReview.displayType,
                    goodsUserReview.goodsName,
                    goodsUserReview.goodsOriginPrice,
                    goodsUserReview.goodsPrice,
                    goodsUserReview.discountRate,
                    goodsUserReview.discountPrice,
                    goodsUserReview.saleStatus,
                    goodsUserReview.visible,
                    goodsUserReview.mainProduct,
                    goodsUserReview.averageRating,
                    goodsUserReview.reviewCnt,
                    goodsUserReview.updatetime,
                    goodsUserReview.recordbirth,
                    getMainImage(),
                    qBrand.brandName,
                    goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(NEWST_DAY))
                        .as("newest")
                )).from(goodsUserReview)
                .innerJoin(qProduct)
                .on(goodsUserReview.mainProduct.eq(qProduct.idx))
                .innerJoin(qBrand)
                .on(qProduct.brand.idx.eq(qBrand.idx))
                .leftJoin(qGoodsImage)
                .on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))
                .leftJoin(qProductImage)
                .on(qProduct.idx.eq(qProductImage.productIdx)
                    .and(qProductImage.type.eq(0).and(qProductImage.priority.eq(0))))
                .where(
                    qProduct.idx.eq(productIdx)
                        .and(inNormalGoodsStatus())
                        .and(goodsUserReview.isRecommend.eq(Boolean.TRUE))
                        .and(goodsUserReview.pcIdx.eq(pcIdx)))
                .groupBy(goodsUserReview.idx)
                .fetchResults();

        return results.getResults();
    }

    @Override
    public List<GoodsDto.SortedGoodsList> getrRcommendGoodsListByProductIdxAndNotGoodsIdxList(
        final int pcIdx, final Integer productIdx, final List<Integer> goodsIdxList) {

        return queryFactory.select(Projections.bean(GoodsDto.SortedGoodsList.class,
                goodsUserReview.idx,
                goodsUserReview.pcIdx,
                goodsUserReview.goodsTypeIdx,
                goodsUserReview.displayType,
                goodsUserReview.goodsName,
                goodsUserReview.goodsOriginPrice,
                goodsUserReview.goodsPrice,
                goodsUserReview.discountRate,
                goodsUserReview.discountPrice,
                goodsUserReview.saleStatus,
                goodsUserReview.visible,
                goodsUserReview.mainProduct,
                goodsUserReview.averageRating,
                goodsUserReview.reviewCnt,
                goodsUserReview.updatetime,
                goodsUserReview.recordbirth,
                getMainImage(),
                qBrand.brandName,
                goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(NEWST_DAY)).as("newest"),
                getWeight()
            )).from(goodsUserReview)
            .innerJoin(qProduct)
            .on(goodsUserReview.mainProduct.eq(qProduct.idx))
            .innerJoin(qBrand)
            .on(qProduct.brand.idx.eq(qBrand.idx))
            .innerJoin(qGoodsProduct)
            .on(goodsUserReview.idx.eq(qGoodsProduct.goodsIdx)
                .and(goodsUserReview.mainProduct.eq(qGoodsProduct.productIdx)))
            .leftJoin(qGoodsImage)
            .on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))
            .leftJoin(qProductImage)
            .on(qProduct.idx.eq(qProductImage.productIdx)
                .and(qProductImage.type.eq(0).and(qProductImage.priority.eq(0))))
            .where(qProduct.idx.eq(productIdx).and(inNormalGoodsStatus())
                .and(goodsUserReview.isRecommend.eq(Boolean.TRUE))
                .and(goodsUserReview.mainProduct.eq(productIdx))
                .and(goodsUserReview.pcIdx.eq(pcIdx)))
            .where(notInGoodsIdx(goodsIdxList))
            .groupBy(goodsUserReview.idx)
            .fetchResults().getResults();
    }

    private BooleanExpression notInGoodsIdx(final List<Integer> goodsIdxList) {
        if (goodsIdxList.isEmpty()) {
            return null;
        }
        return goodsUserReview.idx.notIn(goodsIdxList);
    }

    @Override
    public Optional<GoodsDto.GoodsDetail> findByGoodsId(final Integer idx) {

        GoodsDto.GoodsDetail results =
            queryFactory.select(Projections.bean(GoodsDto.GoodsDetail.class,
                    goodsUserReview.idx,
                    goodsUserReview.pcIdx,
                    goodsUserReview.goodsTypeIdx,
                    goodsUserReview.displayType,
                    goodsUserReview.goodsName,
                    goodsUserReview.goodsDescv,
                    goodsUserReview.goodsOriginPrice,
                    goodsUserReview.goodsPrice,
                    goodsUserReview.discountRate,
                    goodsUserReview.quantity,
                    goodsUserReview.saleStatus,
                    goodsUserReview.limitQuantity,
                    goodsUserReview.purchaseType,
                    goodsUserReview.mainProduct,
                    goodsUserReview.averageRating,
                    goodsUserReview.reviewCnt,
                    Expressions.asBoolean(Boolean.FALSE).as("isPromotion"),
                    getWeight()
                )).from(goodsUserReview)
                .innerJoin(qProduct).on(qProduct.idx.eq(goodsUserReview.mainProduct))
                .innerJoin(qGoodsProduct)
                .on(goodsUserReview.idx.eq(qGoodsProduct.goodsIdx)
                    .and(goodsUserReview.mainProduct.eq(qGoodsProduct.productIdx)))
                .where(goodsUserReview.idx.eq(idx)
                    .and(inNormalGoodsStatus()))
                .fetchOne();
        return Optional.ofNullable(results);
    }


    @Override
    public List<GoodsDto.SortedGoodsList> getMyOrderGoodsList(final List<Integer> goodsIds) {

        QueryResults<GoodsDto.SortedGoodsList> results =
            queryFactory.select(Projections.bean(GoodsDto.SortedGoodsList.class,
                    goodsUserReview.idx,
                    goodsUserReview.pcIdx,
                    goodsUserReview.goodsTypeIdx,
                    goodsUserReview.displayType,
                    goodsUserReview.goodsName,
                    goodsUserReview.goodsOriginPrice,
                    goodsUserReview.goodsPrice,
                    goodsUserReview.discountRate,
                    goodsUserReview.discountPrice,
                    goodsUserReview.saleStatus,
                    goodsUserReview.visible,
                    goodsUserReview.mainProduct,
                    goodsUserReview.averageRating,
                    goodsUserReview.reviewCnt,
                    goodsUserReview.updatetime,
                    goodsUserReview.recordbirth,
                    getMainImage(),
                    qBrand.brandName,
                    goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(NEWST_DAY))
                        .as("newest")
                )).from(goodsUserReview)
                .innerJoin(qProduct)
                .on(goodsUserReview.mainProduct.eq(qProduct.idx))
                .innerJoin(qBrand)
                .on(qProduct.brand.idx.eq(qBrand.idx))
                .leftJoin(qGoodsImage)
                .on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))
                .leftJoin(qProductImage)
                .on(qProduct.idx.eq(qProductImage.productIdx)
                    .and(qProductImage.type.eq(0).and(qProductImage.priority.eq(0))))
                .where(goodsUserReview.idx.in(goodsIds))
                .groupBy(goodsUserReview.idx)
                .fetchResults();
        return results.getResults();
    }

    public Page<SortedGoodsList> getPoodHomeDeliveryGoodsList(final GoodsSubCt goodsSubCt,
        final Integer pcIdx, final Pageable pageable) {

        QueryResults<GoodsDto.SortedGoodsList> results = queryFactory.select(
                Projections.bean(SortedGoodsList.class,
                    goodsUserReview.idx,
                    goodsUserReview.pcIdx,
                    goodsUserReview.goodsTypeIdx,
                    goodsUserReview.displayType,
                    goodsUserReview.goodsName,
                    goodsUserReview.goodsOriginPrice,
                    goodsUserReview.goodsPrice,
                    goodsUserReview.discountRate,
                    goodsUserReview.discountPrice,
                    goodsUserReview.saleStatus,
                    goodsUserReview.visible,
                    goodsUserReview.mainProduct,
                    goodsUserReview.averageRating,
                    goodsUserReview.reviewCnt,
                    goodsUserReview.updatetime,
                    goodsUserReview.recordbirth,
                    getMainImage(),
                    qBrand.brandName,
                    goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(NEWST_DAY))
                        .as("newest")
                )).from(goodsUserReview)
            .innerJoin(qGoodsSubCtGroup)
            .on(goodsUserReview.idx.eq(qGoodsSubCtGroup.goods.idx))
            .innerJoin(qGoodsCt)
            .on(qGoodsCt.idx.eq(qGoodsSubCtGroup.goodsSubCt.goodsCt.idx))
            .innerJoin(qProduct)
            .on(goodsUserReview.mainProduct.eq(qProduct.idx))
            .innerJoin(qBrand)
            .on(qProduct.brand.idx.eq(qBrand.idx))
            .leftJoin(qGoodsImage)
            .on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))
            .leftJoin(qProductImage)
            .on(qProduct.idx.eq(qProductImage.productIdx)
                .and(qProductImage.type.eq(0).and(qProductImage.priority.eq(0))))
            .where(inNormalGoodsStatus()
                .and(goodsUserReview.isRecommend.eq(Boolean.TRUE))
                .and(qProduct.seller.name.eq("지앤원")))
            .where(eqPcIdx(pcIdx))
            .where(eqDeliveryGoodsSubCt(goodsSubCt))
            .groupBy(goodsUserReview.idx)
            .orderBy(goodsUserReview.reviewCnt.desc(), goodsUserReview.idx.desc()) // (1)
            .offset(pageable.getOffset())
            .limit(pageable.getPageSize())
            .fetchResults();

        pageable.getSort();

        long count = results.getTotal() > 30 ? 30 : results.getTotal();
        return new PageImpl<>(results.getResults(), pageable, count);
    }

    @Override
    public List<SortedGoodsList> poodHomeSetGoodsList(final int petCategoryIdx) {
        return queryFactory.select(
                Projections.bean(SortedGoodsList.class,
                    goodsUserReview.idx,
                    goodsUserReview.pcIdx,
                    goodsUserReview.goodsTypeIdx,
                    goodsUserReview.displayType,
                    goodsUserReview.goodsName,
                    goodsUserReview.goodsOriginPrice,
                    goodsUserReview.goodsPrice,
                    goodsUserReview.discountRate,
                    goodsUserReview.discountPrice,
                    goodsUserReview.saleStatus,
                    goodsUserReview.visible,
                    goodsUserReview.mainProduct,
                    goodsUserReview.averageRating,
                    goodsUserReview.reviewCnt,
                    goodsUserReview.updatetime,
                    goodsUserReview.recordbirth,
                    getMainImage(),
                    qBrand.brandName,
                    goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(NEWST_DAY))
                        .as("newest")
                ))
            .from(goodsUserReview)
            .innerJoin(qGoodsSubCtGroup).on(goodsUserReview.idx.eq(qGoodsSubCtGroup.goods.idx))
            .innerJoin(goodsSubCt).on(qGoodsSubCtGroup.goodsSubCt.idx.eq(goodsSubCt.idx))
            .innerJoin(qGoodsCt).on(goodsSubCt.goodsCt.idx.eq(qGoodsCt.idx))
            .innerJoin(petCategory).on(goodsUserReview.pcIdx.eq(petCategory.idx))
            .innerJoin(qProduct)
            .on(goodsUserReview.mainProduct.eq(qProduct.idx))
            .innerJoin(qBrand)
            .on(qProduct.brand.idx.eq(qBrand.idx))
            .leftJoin(qGoodsImage)
            .on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))
            .leftJoin(qProductImage)
            .on(qProduct.idx.eq(qProductImage.productIdx)
                .and(qProductImage.type.eq(0).and(qProductImage.priority.eq(0))))
            .where(petCategory.idx.eq(petCategoryIdx)
                .and(qGoodsCt.idx.eq(1L)) //번쩍마트
                .and(goodsSubCt.name.eq(SET_PRODUCT))
                .and(inNormalGoodsStatus()))
            .groupBy(goodsUserReview.idx)
            .orderBy(goodsUserReview.idx.desc())
            .limit(24)
            .fetch();
    }

    @Override
    public List<SortedGoodsList> getPoodHomeNewGoodsList(final int petCategoryIdx,
        final int saleStatus) {
        return queryFactory.select(
                Projections.bean(SortedGoodsList.class,
                    goodsUserReview.idx,
                    goodsUserReview.pcIdx,
                    goodsUserReview.goodsTypeIdx,
                    goodsUserReview.displayType,
                    goodsUserReview.goodsName,
                    goodsUserReview.goodsOriginPrice,
                    goodsUserReview.goodsPrice,
                    goodsUserReview.discountRate,
                    goodsUserReview.discountPrice,
                    goodsUserReview.saleStatus,
                    goodsUserReview.visible,
                    goodsUserReview.mainProduct,
                    goodsUserReview.averageRating,
                    goodsUserReview.reviewCnt,
                    goodsUserReview.updatetime,
                    goodsUserReview.recordbirth,
                    getMainImage(),
                    qBrand.brandName,
                    goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(NEWST_DAY))
                        .as("newest")
                ))
            .from(goodsUserReview)
            .innerJoin(qProduct)
            .on(goodsUserReview.mainProduct.eq(qProduct.idx))
            .innerJoin(qBrand)
            .on(qProduct.brand.idx.eq(qBrand.idx))
            .leftJoin(qGoodsImage)
            .on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))
            .leftJoin(qProductImage)
            .on(qProduct.idx.eq(qProductImage.productIdx)
                .and(qProductImage.type.eq(0).and(qProductImage.priority.eq(0))))
            .where(inNormalGoodsStatus()
                .and(goodsUserReview.pcIdx.eq(petCategoryIdx))) //판매중
            .groupBy(goodsUserReview.idx)
            .orderBy(goodsUserReview.idx.desc())
            .limit(12)
            .fetch();

    }

    @Override
    public List<SortedGoodsList> poodMarketGoodsList(final int petCategoryIdx) {
        return queryFactory.select(
                Projections.bean(SortedGoodsList.class,
                    goodsUserReview.idx,
                    goodsUserReview.pcIdx,
                    goodsUserReview.goodsTypeIdx,
                    goodsUserReview.displayType,
                    goodsUserReview.goodsName,
                    goodsUserReview.goodsOriginPrice,
                    goodsUserReview.goodsPrice,
                    goodsUserReview.discountRate,
                    goodsUserReview.discountPrice,
                    goodsUserReview.saleStatus,
                    goodsUserReview.visible,
                    goodsUserReview.mainProduct,
                    goodsUserReview.averageRating,
                    goodsUserReview.reviewCnt,
                    goodsUserReview.updatetime,
                    goodsUserReview.recordbirth,
                    getMainImage(),
                    qBrand.brandName,
                    goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(NEWST_DAY))
                        .as("newest")
                )).from(goodsUserReview)
            .innerJoin(qGoodsSubCtGroup).on(goodsUserReview.idx.eq(qGoodsSubCtGroup.goods.idx))
            .innerJoin(goodsSubCt).on(qGoodsSubCtGroup.goodsSubCt.idx.eq(goodsSubCt.idx))
            .innerJoin(qGoodsCt).on(goodsSubCt.goodsCt.idx.eq(qGoodsCt.idx))
            .innerJoin(petCategory).on(goodsUserReview.pcIdx.eq(petCategory.idx))
            .innerJoin(qProduct)
            .on(goodsUserReview.mainProduct.eq(qProduct.idx))
            .innerJoin(qBrand)
            .on(qProduct.brand.idx.eq(qBrand.idx))
            .leftJoin(qGoodsImage).on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))
            .leftJoin(qProductImage)
            .on(qProduct.idx.eq(qProductImage.productIdx)
                .and(qProductImage.type.eq(0).and(qProductImage.priority.eq(0))))

            .where(qGoodsCt.idx.eq(POOD_MARKET)
                .and(goodsSubCt.name.ne(SET_PRODUCT))
                .and(petCategory.idx.eq(petCategoryIdx))
                .and(inNormalGoodsStatus()))
            .limit(24L)
            .groupBy(goodsUserReview.idx)
            .orderBy(goodsUserReview.idx.desc())
            .fetch();
    }

    @Override
    public List<SortedGoodsList> poodMatchGoodsList(final int petCategoryIdx,
        final List<String> searchList) {

        return queryFactory.select(
                Projections.bean(SortedGoodsList.class,
                    goodsUserReview.idx,
                    goodsUserReview.pcIdx,
                    goodsUserReview.goodsTypeIdx,
                    goodsUserReview.displayType,
                    goodsUserReview.goodsName,
                    goodsUserReview.goodsOriginPrice,
                    goodsUserReview.goodsPrice,
                    goodsUserReview.discountRate,
                    goodsUserReview.discountPrice,
                    goodsUserReview.saleStatus,
                    goodsUserReview.visible,
                    goodsUserReview.mainProduct,
                    goodsUserReview.averageRating,
                    goodsUserReview.reviewCnt,
                    goodsUserReview.updatetime,
                    goodsUserReview.recordbirth,
                    getMainImage(),
                    qBrand.brandName,
                    goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(NEWST_DAY))
                        .as("newest")
                )).from(goodsUserReview)
            .innerJoin(qGoodsSubCtGroup).on(goodsUserReview.idx.eq(qGoodsSubCtGroup.goods.idx))
            .innerJoin(goodsSubCt).on(qGoodsSubCtGroup.goodsSubCt.idx.eq(goodsSubCt.idx))
            .innerJoin(qGoodsCt).on(goodsSubCt.goodsCt.idx.eq(qGoodsCt.idx))
            .innerJoin(qProduct)
            .on(goodsUserReview.mainProduct.eq(qProduct.idx))
            .innerJoin(qBrand)
            .on(qProduct.brand.idx.eq(qBrand.idx))
            .leftJoin(qGoodsImage).on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))
            .leftJoin(qProductImage)
            .on(qProduct.idx.eq(qProductImage.productIdx)
                .and(qProductImage.type.eq(MAIN).and(qProductImage.priority.eq(MAIN))))
            .where(goodsUserReview.pcIdx.eq(petCategoryIdx)
                .and(goodsUserReview.saleStatus.eq(SALE_STATUS_ON)))
            .where(likeInGoodsName(searchList))
            .groupBy(goodsUserReview.idx)
            .fetch();
    }

    @Override
    public boolean existsByPcIdxAndGoodsNameLikeInSearchList(final int pctIdx,
        final List<String> searchList) {
        JPAQuery<Long> from = queryFactory.select(goodsUserReview.idx.count())
            .from(goodsUserReview)
            .where(inNormalGoodsStatus())
            .where(likeInGoodsName(searchList));
        return Objects.nonNull(from.fetchOne()) && from.fetchOne() > 0;
    }

    @Override
    public List<SuggestRandomGoodsMapper> suggestRandom10Goods() {
        return queryFactory.select(
                new QSuggestRandomGoodsMapper(
                    goodsUserReview.idx,
                    goodsUserReview.goodsName,
                    goodsUserReview.goodsOriginPrice,
                    goodsUserReview.goodsPrice,
                    goodsUserReview.discountRate,
                    goodsUserReview.discountPrice,
                    goodsUserReview.reviewCnt,
                    goodsUserReview.averageRating,
                    getMainImage(),
                    goodsUserReview.saleStatus,
                    qBrand.brandName,
                    goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(NEWST_DAY))
                        .as("newest")
                )
            ).from(goodsUserReview)
            .innerJoin(qProduct)
            .on(goodsUserReview.mainProduct.eq(qProduct.idx))
            .innerJoin(qBrand)
            .on(qProduct.brand.idx.eq(qBrand.idx))
            .leftJoin(qGoodsImage)
            .on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))
            .leftJoin(qProductImage)
            .on(qProduct.idx.eq(qProductImage.productIdx)
                .and(qProductImage.type.eq(MAIN).and(qProductImage.priority.eq(MAIN))))
            .where(goodsUserReview.saleStatus.eq(SALE_STATUS_ON))
            .limit(10)
            .groupBy(goodsUserReview.idx)
            .orderBy(Expressions.numberTemplate(Double.class, "function('rand')").asc())
            .fetch();
    }

    @Override
    public List<OrderGoodsMapper> myOrderGoodsInfoListByBasket(final List<Integer> goodsIds) {
        return queryFactory.select(new QOrderGoodsMapper(
                goodsUserReview.idx,
                goodsUserReview.pcIdx,
                goodsUserReview.goodsTypeIdx,
                goodsUserReview.displayType,
                goodsUserReview.goodsName,
                goodsUserReview.goodsOriginPrice,
                goodsUserReview.goodsPrice,
                goodsUserReview.discountRate,
                goodsUserReview.discountPrice,
                goodsUserReview.saleStatus,
                goodsUserReview.visible,
                goodsUserReview.mainProduct,
                goodsUserReview.averageRating,
                goodsUserReview.reviewCnt,
                getMainImage(),
                goodsUserReview.isRecommend,
                new QSellerMapper(qSeller.idx, qSeller.name)
            )).from(goodsUserReview)
            .innerJoin(qProduct)
            .on(goodsUserReview.mainProduct.eq(qProduct.idx))
            .innerJoin(qSeller)
            .on(qSeller.idx.eq(qProduct.seller.idx))
            .leftJoin(qGoodsImage)
            .on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))
            .leftJoin(qProductImage)
            .on(qProduct.idx.eq(qProductImage.productIdx)
                .and(qProductImage.type.eq(0).and(qProductImage.priority.eq(0))))
            .where(goodsUserReview.idx.in(goodsIds))
            .groupBy(goodsUserReview.idx)
            .fetch();
    }

    @Override
    public List<SortedGoodsList> getSortedGoodsByProductIdxsOrderReviewCount(
        List<Integer> productIdxList) {

        return queryFactory.select(
                Projections.bean(SortedGoodsList.class,
                    goodsUserReview.idx,
                    goodsUserReview.pcIdx,
                    goodsUserReview.goodsTypeIdx,
                    goodsUserReview.displayType,
                    goodsUserReview.goodsName,
                    goodsUserReview.goodsOriginPrice,
                    goodsUserReview.goodsPrice,
                    goodsUserReview.discountRate,
                    goodsUserReview.discountPrice,
                    goodsUserReview.saleStatus,
                    goodsUserReview.visible,
                    goodsUserReview.mainProduct,
                    goodsUserReview.averageRating,
                    goodsUserReview.reviewCnt,
                    goodsUserReview.updatetime,
                    goodsUserReview.recordbirth,
                    getMainImage(),
                    qBrand.brandName,
                    goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(NEWST_DAY))
                        .as("newest")
                )).from(goodsUserReview)
            .innerJoin(qGoodsSubCtGroup)
            .on(goodsUserReview.idx.eq(qGoodsSubCtGroup.goods.idx))
            .innerJoin(qGoodsCt)
            .on(qGoodsCt.idx.eq(qGoodsSubCtGroup.goodsSubCt.goodsCt.idx))
            .innerJoin(qProduct)
            .on(goodsUserReview.mainProduct.eq(qProduct.idx))
            .innerJoin(qBrand)
            .on(qProduct.brand.idx.eq(qBrand.idx))
            .leftJoin(qGoodsImage)
            .on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))
            .leftJoin(qProductImage)
            .on(qProduct.idx.eq(qProductImage.productIdx)
                .and(qProductImage.type.eq(MAIN).and(qProductImage.priority.eq(MAIN))))
            .where(inNormalGoodsStatus()
                .and(goodsUserReview.mainProduct.in(productIdxList)))
            .groupBy(goodsUserReview.idx)
            .orderBy(goodsUserReview.reviewCnt.desc(), goodsUserReview.idx.desc()) // (1)
            .fetch();

    }

    @Override
    public Page<SortedGoodsList> getNewstGoodsList(final int pcIdx, final Pageable pageable) {
        QueryResults<SortedGoodsList> results = queryFactory.select(
                Projections.bean(SortedGoodsList.class,
                    goodsUserReview.idx,
                    goodsUserReview.pcIdx,
                    goodsUserReview.goodsTypeIdx,
                    goodsUserReview.displayType,
                    goodsUserReview.goodsName,
                    goodsUserReview.goodsOriginPrice,
                    goodsUserReview.goodsPrice,
                    goodsUserReview.discountRate,
                    goodsUserReview.discountPrice,
                    goodsUserReview.saleStatus,
                    goodsUserReview.visible,
                    goodsUserReview.mainProduct,
                    goodsUserReview.averageRating,
                    goodsUserReview.reviewCnt,
                    goodsUserReview.updatetime,
                    goodsUserReview.recordbirth,
                    getMainImage(),
                    qBrand.brandName,
                    goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(NEWST_DAY))
                        .as("newest")
                )).from(goodsUserReview)
            .innerJoin(qGoodsSubCtGroup)
            .on(goodsUserReview.idx.eq(qGoodsSubCtGroup.goods.idx))
            .innerJoin(qGoodsCt)
            .on(qGoodsCt.idx.eq(qGoodsSubCtGroup.goodsSubCt.goodsCt.idx))
            .innerJoin(qProduct)
            .on(goodsUserReview.mainProduct.eq(qProduct.idx))
            .innerJoin(qBrand)
            .on(qProduct.brand.idx.eq(qBrand.idx))
            .leftJoin(qGoodsImage)
            .on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))
            .leftJoin(qProductImage)
            .on(qProduct.idx.eq(qProductImage.productIdx)
                .and(qProductImage.type.eq(MAIN).and(qProductImage.priority.eq(MAIN))))
            .where(inNormalGoodsStatus()
                .and(goodsUserReview.pcIdx.eq(pcIdx))
                .and(goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(NEWST_DAY))))
            .groupBy(goodsUserReview.idx)
            .orderBy(goodsUserReview.idx.desc())
            .offset(pageable.getOffset())
            .limit(pageable.getPageSize())
            .fetchResults();

        pageable.getSort();

        long count = results.getTotal();
        return new PageImpl<>(results.getResults(), pageable, count);
    }

    @Override
    public List<Integer> getRankGoodsIdxList(final int pcIdx) {
        return queryFactory.select(qGoodsSubCtGroup.goods.idx)
            .from(qGoodsSubCtGroup)
            .innerJoin(goodsUserReview).on(goodsUserReview.idx.eq(qGoodsSubCtGroup.goods.idx))
            .innerJoin(qGoodsSubCt).on(qGoodsSubCtGroup.goodsSubCt.idx.eq(qGoodsSubCt.idx))
            .innerJoin(qGoodsCt).on(qGoodsCt.idx.eq(qGoodsSubCt.goodsCt.idx))
            .where(qGoodsSubCt.petCategory.idx.eq(pcIdx)
                .and(inNormalGoodsStatus()))
            .fetch();
    }

    private BooleanExpression likeInGoodsName(final List<String> searchList) {
        List<String> textList = Collections.unmodifiableList(searchList);
        if (textList.isEmpty()) {
            return null;
        }

        BooleanExpression contains = goodsUserReview.goodsName.contains(textList.get(0));
        for (String search : textList) {
            contains = contains.or(goodsUserReview.goodsName.contains(search));
        }
        return contains;
    }

    private StringExpression getMainImage() {
        return new CaseBuilder()
            .when(qGoodsImage.count().gt(0L)).then(qGoodsImage.url)
            .when(qProductImage.count().gt(0L)).then(qProductImage.url)
            .otherwise("").as("mainImage");
    }

    private BooleanExpression likeGoodsName(final String searchText) {
        if (searchText == null) {
            return null;
        }
        if (StringUtils.pathEquals(searchText, "")) {
            return null;
        }
        return goodsUserReview.goodsName.contains(searchText);
    }

    private BooleanExpression eqGoodSubCtIdx(final Long goodSubCtIdx) {
        if (Objects.isNull(goodSubCtIdx)) {
            return null;
        }
        return qGoodsSubCtGroup.goodsSubCt.idx.eq(goodSubCtIdx);
    }


    public BooleanExpression eqDeliveryGoodsSubCt(final GoodsSubCt goodsSubCt) {
        if (Objects.isNull(goodsSubCt)) {
            return qGoodsCt.idx.in(getDeliveryGoodsCtList())
                .or(qGoodsSubCtGroup.goodsSubCt.idx.eq(FLASH_MARKET));
        }
        Long subCtIdx = setSubCtIdx(goodsSubCt);
        BooleanExpression booleanExpression = eqGoodsCtIdx(goodsSubCt.getGoodsCt().getIdx());

        if (Objects.nonNull(subCtIdx) && Objects.nonNull(booleanExpression)) { //  goodsSubCt 값 세팅
            return booleanExpression.and(eqGoodSubCtIdx(subCtIdx));
        }

        return booleanExpression;
    }

    public BooleanExpression eqRankGoodsSubCt(final GoodsSubCt goodsSubCt) {
        if (Objects.isNull(goodsSubCt)) {
            return qGoodsCt.idx.in(getRankGoodsCtList())
                .or(qGoodsSubCtGroup.goodsSubCt.idx.eq(FLASH_MARKET));
        }
        Long subCtIdx = setSubCtIdx(goodsSubCt);
        BooleanExpression booleanExpression = eqGoodsCtIdx(goodsSubCt.getGoodsCt().getIdx());

        if (Objects.nonNull(subCtIdx) && Objects.nonNull(booleanExpression)) { //  goodsSubCt 값 세팅
            return booleanExpression.and(eqGoodSubCtIdx(subCtIdx));
        }
        return booleanExpression;
    }

    public List<Integer> getRankGoodsIdxList(final GoodsSubCt goodsSubCt, final int pcIdx) {

        return queryFactory.select(qGoodsSubCtGroup.goods.idx)
            .from(qGoodsSubCtGroup)
            .innerJoin(qGoodsSubCt).on(qGoodsSubCtGroup.goodsSubCt.idx.eq(qGoodsSubCt.idx))
            .innerJoin(qGoodsCt).on(qGoodsCt.idx.eq(qGoodsSubCt.goodsCt.idx))
            .innerJoin(goodsUserReview).on(goodsUserReview.idx.eq(qGoodsSubCtGroup.goods.idx))
            .where(qGoodsSubCt.petCategory.idx.eq(pcIdx)
                .and(inNormalGoodsStatus()))
            .where(eqRankGoodsSubCt(goodsSubCt))
            .fetch();
    }

    private List<Long> getDeliveryGoodsCtList() {

        return queryFactory.select(qGoodsSubCt.goodsCt.idx)
            .from(qPoodDeliveryCategory)
            .innerJoin(qGoodsSubCt).on(qPoodDeliveryCategory.goodsSubCt.idx.eq(qGoodsSubCt.idx))
            .where(qGoodsSubCt.goodsCt.idx.ne(POOD_MARKET))
            .groupBy(qGoodsSubCt.goodsCt.idx)
            .fetch();
    }

    private List<Long> getRankGoodsCtList() {

        return queryFactory.select(qGoodsSubCt.goodsCt.idx)
            .from(qPoodRankCategory)
            .innerJoin(qGoodsSubCt).on(qPoodRankCategory.goodsSubCt.idx.eq(qGoodsSubCt.idx))
            .where(qGoodsSubCt.goodsCt.idx.ne(POOD_MARKET))
            .groupBy(qGoodsSubCt.goodsCt.idx)
            .fetch();
    }

    @Override
    public List<GoodsDto.SortedGoodsList> getRecommendGoodsListByIsSale(final int productIdx,
        final int pcIdx) {
        QueryResults<GoodsDto.SortedGoodsList> results =
            queryFactory.select(Projections.bean(GoodsDto.SortedGoodsList.class,
                    goodsUserReview.idx,
                    goodsUserReview.pcIdx,
                    goodsUserReview.goodsTypeIdx,
                    goodsUserReview.displayType,
                    goodsUserReview.goodsName,
                    goodsUserReview.goodsOriginPrice,
                    goodsUserReview.goodsPrice,
                    goodsUserReview.discountRate,
                    goodsUserReview.discountPrice,
                    goodsUserReview.saleStatus,
                    goodsUserReview.visible,
                    goodsUserReview.mainProduct,
                    goodsUserReview.averageRating,
                    goodsUserReview.reviewCnt,
                    goodsUserReview.updatetime,
                    goodsUserReview.recordbirth,
                    getMainImage(),
                    qBrand.brandName,
                    goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(NEWST_DAY))
                        .as("newest")
                )).from(goodsUserReview)
                .innerJoin(qProduct)
                .on(goodsUserReview.mainProduct.eq(qProduct.idx))
                .innerJoin(qBrand)
                .on(qProduct.brand.idx.eq(qBrand.idx))
                .leftJoin(qGoodsImage)
                .on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))
                .leftJoin(qProductImage)
                .on(qProduct.idx.eq(qProductImage.productIdx)
                    .and(qProductImage.type.eq(0).and(qProductImage.priority.eq(0))))
                .where(
                    qProduct.idx.eq(productIdx)
                        .and(goodsUserReview.saleStatus.eq(SALE_STATUS_ON))
                        .and(goodsUserReview.isRecommend.eq(Boolean.TRUE))
                        .and(goodsUserReview.pcIdx.eq(pcIdx)))
                .groupBy(goodsUserReview.idx)
                .fetchResults();

        return results.getResults();
    }

    private BooleanExpression eqPcIdx(final Integer pcIdx) {
        if (Objects.isNull(pcIdx)) {
            return null;
        }
        if (!StringUtils.hasText(pcIdx.toString())) {
            return null;
        }
        return goodsUserReview.pcIdx.eq(pcIdx);
    }

    private BooleanExpression eqPcIdxAndEqCommonPcIdx(final Integer pcIdx) {
        if (Objects.isNull(pcIdx)) {
            return null;
        }
        return goodsUserReview.pcIdx.eq(pcIdx).or(goodsUserReview.pcIdx.eq(COMMON_PC_IDX));
    }

    private BooleanExpression eqGoodsCtIdx(final Long goodsCtIdx) {
        if (Objects.isNull(goodsCtIdx)) {
            return null;
        }
        return qGoodsCt.idx.eq(goodsCtIdx);
    }

    private BooleanExpression eqSearchFiled(final String fieldKey, final String fieldValue) {

        if (!StringUtils.hasText(fieldKey)) {
            return null;
        }
        if (!StringUtils.hasText(fieldValue)) {
            return null;
        }
        if (fieldKey.equals("main_property")) {
            return qProduct.mainProperty.eq(fieldValue);
        }
        if (fieldKey.equals("position")) {
            return isMatchingArdGroupCode(
                getAiRecommendDiagnosisByWorryIdx(Long.valueOf(fieldValue)));
        }
        if (fieldKey.equals("unit_size")) {
            return qProduct.unitSize.goe(Integer.valueOf(fieldValue))
                .and(qProduct.unitSize.loe(Integer.valueOf(fieldValue)));
        }
        if (fieldKey.equals("feed_target")) {  // 대형경 중형견 소형견 puppy adult senior m??????
            if (Integer.valueOf(fieldValue).equals(AnimalSize.ALL.getValue())) {
                return qProduct.feedType.eq(FeedType.ALL.getValue());
            }
            if (Integer.valueOf(fieldValue).equals(AnimalSize.PUPPY.getValue())) {
                return qProduct.feedType.eq(FeedType.PUPPY.getValue())
                    .or(qProduct.feedType.eq(FeedType.PUPPY_ADULT.getValue())
                        .or(qProduct.feedType.eq(FeedType.PUPPY_ADULT_EXCEPT_SENIOR.getValue())));
            }
            if (Integer.valueOf(fieldValue).equals(AnimalSize.ADULT.getValue())) {
                return qProduct.feedType.eq(FeedType.ADULT.getValue())
                    .or(qProduct.feedType.eq(FeedType.PUPPY_ADULT.getValue())
                        .or(qProduct.feedType.eq(FeedType.ADULT_SENIOR.getValue())));
            }
            if (Integer.valueOf(fieldValue).equals(AnimalSize.SENIOR.getValue())) {
                return qProduct.feedType.eq(FeedType.SENIOR.getValue())
                    .or(qProduct.feedType.eq(FeedType.ADULT_SENIOR.getValue()));
            }
            return qProduct.feedTarget.eq(Integer.valueOf(fieldValue));
        }
        if (fieldKey.equals("life_stage")) {
            return qProduct.feedType.eq(fieldValue);
        }
        return null;
    }

    private OrderSpecifier[] orderCondition(final Pageable pageable) {
        PathBuilder<GoodsUserReview> entityPath = new PathBuilder<>(GoodsUserReview.class,
            "goodsUserReview");
        return pageable.getSort() // (2)
            .stream() // (3)
            .map(order -> new OrderSpecifier(Order.valueOf(order.getDirection().name()),
                entityPath.get(order.getProperty()))) // (4)
            .toArray(OrderSpecifier[]::new); // (5)
    }

    private Long setSubCtIdx(final GoodsSubCt goodsSubCt) {
        if (goodsSubCt.eqNameAll()) {
            return null;
        }
        return goodsSubCt.getIdx();
    }

    private Integer getAiRecommendDiagnosisByWorryIdx(final Long worryIdx) {
        return queryFactory.select(
                qAiRecommendDiagnosis.ardGroupCode
            ).from(qAiRecommendDiagnosis)
            .innerJoin(qPetWorry)
            .on(qAiRecommendDiagnosis.idx.eq(qPetWorry.aiRecommendDiagnosis.idx))
            .where(qPetWorry.id.eq(worryIdx))
            .fetchOne();
    }

    private BooleanExpression isMatchingArdGroupCode(final Integer ardGroupCode) {
        if (ardGroupCode == 122) {
            return qPetDoctorFeedDescription.ardGroup122.gt(0);
        } else if (ardGroupCode == 421) {
            return qPetDoctorFeedDescription.ardGroup421.gt(0);
        } else if (ardGroupCode == 501) {
            return qPetDoctorFeedDescription.ardGroup501.gt(0);
        } else if (ardGroupCode == 201) {
            return qPetDoctorFeedDescription.ardGroup201.gt(0);
        } else if (ardGroupCode == 301) {
            return qPetDoctorFeedDescription.ardGroup301.gt(0);
        } else if (ardGroupCode == 961) {
            return qPetDoctorFeedDescription.ardGroup961.gt(0);
        } else if (ardGroupCode == 241) {
            return qPetDoctorFeedDescription.ardGroup241.gt(0);
        } else if (ardGroupCode == 121) {
            return qPetDoctorFeedDescription.ardGroup121.gt(0);
        } else if (ardGroupCode == 941) {
            return qPetDoctorFeedDescription.ardGroup941.gt(0);
        } else if (ardGroupCode == 125) {
            return qPetDoctorFeedDescription.ardGroup125.gt(0);
        } else if (ardGroupCode == 942) {
            return qPetDoctorFeedDescription.ardGroup942.gt(0);
        } else if (ardGroupCode == 126) {
            return qPetDoctorFeedDescription.ardGroup126.gt(0);
        } else if (ardGroupCode == 601) {
            return qPetDoctorFeedDescription.ardGroup601.gt(0);
        } else if (ardGroupCode == 422) {
            return qPetDoctorFeedDescription.ardGroup422.gt(0);
        } else if (ardGroupCode == 423) {
            return qPetDoctorFeedDescription.ardGroup423.gt(0);
        } else if (ardGroupCode == 502) {
            return qPetDoctorFeedDescription.ardGroup502.gt(0);
        } else if (ardGroupCode == 145) {
            return qPetDoctorFeedDescription.ardGroup145.gt(0);
        }
        return null;
    }

    @Override
    public List<CustomRankInfoMapper> getCustomRankListNotIn(final List<Integer> goodsIdsList, final int pcIdx) {
        return queryFactory.select(
                new QCustomRankInfoMapper(
                    qCustomRankInfo.goodsIdx,
                    qCustomRankInfo.reviewCnt,
                    qCustomRankInfo.wishCnt,
                    qCustomRankInfo.totalPurchaseUserCnt,
                    qCustomRankInfo.rePurchaseUserCnt
                )
            ).from(qCustomRankInfo)
            .innerJoin(goodsUserReview).on(goodsUserReview.idx.eq(qCustomRankInfo.goodsIdx))
            .innerJoin(qGoodsSubCtGroup).on(qGoodsSubCtGroup.goods.idx.eq(qCustomRankInfo.goodsIdx))
            .innerJoin(qGoodsSubCt).on(qGoodsSubCt.idx.eq(qGoodsSubCtGroup.goodsSubCt.idx))
            .innerJoin(qGoodsCt).on(qGoodsCt.idx.eq(qGoodsSubCt.goodsCt.idx))
            .where(
                eqPcIdx(pcIdx),
                inNormalGoodsStatus(),
                qGoodsSubCt.goodsCt.idx.in(getRankGoodsCtList()),
                qCustomRankInfo.goodsIdx.notIn(goodsIdsList)
            )
            .groupBy(qCustomRankInfo.goodsIdx)
            .fetch();
    }

    @Override
    public List<String> findGoodsNameBySearchText(final Integer pcIdx, final String goodsName) {
        return queryFactory.select(
                goodsUserReview.goodsName
            ).from(goodsUserReview)
            .where(eqPcIdxAndEqCommonPcIdx(pcIdx),
                inNormalGoodsStatus(),
                likeGoodsName(goodsName)
            )
            .limit(100L)
            .fetch();
    }

    @Override
    public List<String> findGoodsNameBySearchText(final Integer pcIdx, final String goodsName,
        final Long goodsCt) {
        return queryFactory.select(
                goodsUserReview.goodsName
            ).from(goodsUserReview)
            .innerJoin(qGoodsSubCtGroup).on(qGoodsSubCtGroup.goods.idx.eq(goodsUserReview.idx))
            .innerJoin(qGoodsSubCt).on(qGoodsSubCt.idx.eq(qGoodsSubCtGroup.goodsSubCt.idx))
            .innerJoin(qGoodsCt).on(qGoodsCt.idx.eq(qGoodsSubCt.goodsCt.idx))
            .where(eqPcIdxAndEqCommonPcIdx(pcIdx),
                likeGoodsName(goodsName),
                inNormalGoodsStatus(),
                eqGoodsCtIdx(goodsCt)
            )
            .limit(100L)
            .fetch();
    }

    @Override
    public List<String> findGoodsNameBySearchTextByFeedComparison(final Integer pcIdx,
        final String goodsName) {

        return queryFactory.select(
                goodsUserReview.goodsName
            ).from(goodsUserReview)
            .innerJoin(qGoodsSubCtGroup).on(qGoodsSubCtGroup.goods.idx.eq(goodsUserReview.idx))
            .innerJoin(qGoodsSubCt).on(qGoodsSubCt.idx.eq(qGoodsSubCtGroup.goodsSubCt.idx))
            .innerJoin(qGoodsCt).on(qGoodsCt.idx.eq(qGoodsSubCt.goodsCt.idx))
            .innerJoin(qProduct).on(qProduct.idx.eq(goodsUserReview.mainProduct))
            .where(eqPcIdxAndEqCommonPcIdx(pcIdx),
                likeGoodsName(goodsName),
                inNormalGoodsStatus(),
                eqGoodsCtIdx(FEED_GOODS_CT),
                qProduct.ctIdx.eq(FEED_PRODUCT_CT)
            )
            .limit(100L)
            .fetch();
    }
}

