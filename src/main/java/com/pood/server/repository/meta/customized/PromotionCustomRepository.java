package com.pood.server.repository.meta.customized;

import com.pood.server.dto.meta.goods.PromotionDto;
import java.util.List;

public interface PromotionCustomRepository {

    PromotionDto.PromotionProductInfo findByGoodsIdxAndNowActive(final Integer goodsIdx);

    List<PromotionDto.PromotionGoodsProductInfo> findByGoodsIdxAndNowActiveList(
        final List<Integer> goodsIdx);

    PromotionDto.PromotionGoodsListInfo findListInfoById(final Integer idx);

    List<PromotionDto.PromotionGroupGoods> findPromotionGoodsGroupById(final Integer promotionIdx);

    PromotionDto.PromotionGroupGoods2 promotionGroupGoodsInfo(final Integer goodsIdx, final
    Integer promotionIdx);

    List<PromotionDto.PromotionGoodsListInfo> findPromotionListInfo();

    List<PromotionDto.PromotionGoods> findPromotionGoodsDetail(final Integer idx);

    List<PromotionDto.PromotionGoods> findPromotionGoodsDetail(final List<Integer> idx);

    List<PromotionDto.PromotionGoodsProductInfo> findByHotTimeGoodsList(final Integer petCtIdx);

}
