package com.pood.server.repository.meta;

import com.pood.server.entity.meta.GoodsSubCt;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoodsSubCtRepository extends JpaRepository<GoodsSubCt, Long>  {

    List<GoodsSubCt> findAllByGoodsCtIdx(Long goodsCtIdx);


    Optional<GoodsSubCt> findByIdxAndPetCategoryIdx(final long idx, final int pcIdx);

    boolean existsByIdxAndName(final long idx, final String name);
}
