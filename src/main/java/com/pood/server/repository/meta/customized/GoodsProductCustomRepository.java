package com.pood.server.repository.meta.customized;

import com.pood.server.entity.meta.mapper.goods.GoodsProductMapper;
import java.util.List;

public interface GoodsProductCustomRepository {

    List<GoodsProductMapper> findAllByGoodsIdxToMapper(final Integer goodsIdx);
}
