package com.pood.server.repository.meta;

import com.pood.server.entity.meta.Promotion;
import com.pood.server.repository.meta.customized.PromotionCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface PromotionRepository extends JpaRepository<Promotion, Integer>, PromotionCustomRepository,
    QuerydslPredicateExecutor<Promotion> {

}
