package com.pood.server.repository.meta.customized;


import com.pood.server.config.querydsl.MetaQuerydslRepositorySupport;
import com.pood.server.dto.meta.goods.PromotionDto;
import com.pood.server.entity.meta.Promotion;
import com.pood.server.entity.meta.PromotionGroupGoods;
import com.pood.server.entity.meta.QBrand;
import com.pood.server.entity.meta.QGoodsImage;
import com.pood.server.entity.meta.QGoodsUserReview;
import com.pood.server.entity.meta.QProduct;
import com.pood.server.entity.meta.QProductImage;
import com.pood.server.entity.meta.QPromotion;
import com.pood.server.entity.meta.QPromotionGroup;
import com.pood.server.entity.meta.QPromotionGroupGoods;
import com.pood.server.entity.meta.QPromotionImage;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.CaseBuilder;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;


public class PromotionRepositoryImpl extends MetaQuerydslRepositorySupport implements
    PromotionCustomRepository {

    private final JPAQueryFactory queryFactory;

    public PromotionRepositoryImpl(@Qualifier("metaQueryFactory") JPAQueryFactory queryFactory) {
        super(Promotion.class);
        this.queryFactory = queryFactory;
    }

    QPromotion qPromotion = QPromotion.promotion;
    QPromotionGroupGoods qPromotionGroupGoods = QPromotionGroupGoods.promotionGroupGoods;
    QPromotionGroup qPromotionGroup = QPromotionGroup.promotionGroup;
    QGoodsUserReview goodsUserReview = QGoodsUserReview.goodsUserReview;
    QPromotionImage qPromotionImage = QPromotionImage.promotionImage;
    QProduct qProduct = QProduct.product;
    QGoodsImage qGoodsImage = QGoodsImage.goodsImage;
    QProductImage qProductImage = QProductImage.productImage;
    QBrand qBrand = QBrand.brand;


    private final static int MAIN = 0;
    private final static int STOP = 0;
    private final static int RUN = 1;
    private final static int PAUSE = 2;
    private final static int VIEW_NOT_PAUSE = 3;


    public List<PromotionGroupGoods> getSortedGoodsList(final List<Integer> goodsIdxList) {

        queryFactory.selectFrom(qPromotion)
            .innerJoin(qPromotionGroup)
            .on(qPromotion.idx.eq(qPromotionGroup.prIdx))
            .innerJoin(qPromotionGroupGoods)
            .on(qPromotionGroupGoods.prGroupIdx.eq(qPromotionGroup.idx))
            .where(qPromotionGroupGoods.goodsIdx.in(goodsIdxList));
        return null;
    }


    @Override
    public PromotionDto.PromotionProductInfo findByGoodsIdxAndNowActive(final Integer goodsIdx) {
        PromotionDto.PromotionProductInfo results = queryFactory.select(
                Projections.bean(PromotionDto.PromotionProductInfo.class,
                    qPromotion.idx,
                    qPromotion.name,
                    qPromotionGroupGoods.prPrice.as("prPrice"),
                    qPromotionGroupGoods.prDiscountPrice.as("prDiscountPrice"),
                    qPromotionGroupGoods.prDiscountRate.as("prDiscountRate"),
                    qPromotionImage.url.as("promotionImageUrl"),
                    qPromotion.type,
                    Expressions.stringTemplate("DATE_FORMAT({0}, {1})", qPromotion.startPeriod,
                        "%Y-%m-%d %T").as("startPeriod"),
                    Expressions.stringTemplate("DATE_FORMAT({0}, {1})", qPromotion.endPeriod,
                        "%Y-%m-%d %T").as("endPeriod")
                )).from(qPromotion)
            .innerJoin(qPromotionGroup)
            .on(qPromotion.idx.eq(qPromotionGroup.prIdx))
            .innerJoin(qPromotionGroupGoods)
            .on(qPromotionGroupGoods.prGroupIdx.eq(qPromotionGroup.idx))
            .leftJoin(qPromotionImage)
            .on(qPromotionImage.prIdx.eq(qPromotion.idx).and(qPromotionImage.type.eq(MAIN)))
            .where(qPromotionGroupGoods.goodsIdx.eq(goodsIdx).and(
                    qPromotion.startPeriod.before(LocalDateTime.now())
                        .and(qPromotion.endPeriod.after(LocalDateTime.now())))
                .and(qPromotion.status.ne(PAUSE)))
            .fetchOne();

        return results;
    }

    @Override
    public List<PromotionDto.PromotionGoodsProductInfo> findByGoodsIdxAndNowActiveList(
        final List<Integer> goodsIdx) {
        QueryResults<PromotionDto.PromotionGoodsProductInfo> results = queryFactory.select(
                Projections.bean(PromotionDto.PromotionGoodsProductInfo.class,
                    qPromotion.idx,
                    qPromotion.name,
                    qPromotionGroupGoods.prPrice.as("prPrice"),
                    qPromotionGroupGoods.prDiscountPrice.as("prDiscountPrice"),
                    qPromotionGroupGoods.prDiscountRate.as("prDiscountRate"),
                    qPromotionGroupGoods.goodsIdx.as("goodsIdx"),
                    Expressions.stringTemplate("DATE_FORMAT({0}, {1})", qPromotion.startPeriod,
                        "%Y-%m-%d %T").as("startPeriod"),
                    Expressions.stringTemplate("DATE_FORMAT({0}, {1})", qPromotion.endPeriod,
                        "%Y-%m-%d %T").as("endPeriod")
                )).from(qPromotion)
            .innerJoin(qPromotionGroup)
            .on(qPromotion.idx.eq(qPromotionGroup.prIdx))
            .innerJoin(qPromotionGroupGoods)
            .on(qPromotionGroupGoods.prGroupIdx.eq(qPromotionGroup.idx))
            .where(qPromotionGroupGoods.goodsIdx.in(goodsIdx).and(
                qPromotion.startPeriod.before(LocalDateTime.now())
                    .and(qPromotion.endPeriod.after(LocalDateTime.now()))))
            .fetchResults();
        return results.getResults();
    }

    @Override
    public List<PromotionDto.PromotionGoodsProductInfo> findByHotTimeGoodsList(
        final Integer petCtIdx) {
        QueryResults<PromotionDto.PromotionGoodsProductInfo> results = queryFactory.select(
                Projections.bean(PromotionDto.PromotionGoodsProductInfo.class,
                    qPromotion.idx,
                    qPromotion.name,
                    qPromotionGroupGoods.prPrice.as("prPrice"),
                    qPromotionGroupGoods.prDiscountPrice.as("prDiscountPrice"),
                    qPromotionGroupGoods.prDiscountRate.as("prDiscountRate"),
                    qPromotionGroupGoods.goodsIdx.as("goodsIdx"),
                    Expressions.stringTemplate("DATE_FORMAT({0}, {1})", qPromotion.startPeriod,
                        "%Y-%m-%d %T").as("startPeriod"),
                    Expressions.stringTemplate("DATE_FORMAT({0}, {1})", qPromotion.endPeriod,
                        "%Y-%m-%d %T").as("endPeriod")
                )).from(qPromotion)
            .innerJoin(qPromotionGroup)
            .on(qPromotion.idx.eq(qPromotionGroup.prIdx))
            .innerJoin(qPromotionGroupGoods)
            .on(qPromotionGroupGoods.prGroupIdx.eq(qPromotionGroup.idx))
            .where(qPromotion.type.eq("H")
                .and(qPromotion.startPeriod.before(LocalDateTime.now())
                    .and(qPromotion.status.eq(RUN))
                    .and(qPromotion.endPeriod.after(LocalDateTime.now()))
                    .and(qPromotion.pcId.eq(petCtIdx))))
            .fetchResults();
        return results.getResults();
    }


    @Override
    public PromotionDto.PromotionGoodsListInfo findListInfoById(final Integer idx) {
        return queryFactory.select(
                Projections.bean(PromotionDto.PromotionGoodsListInfo.class,
                    qPromotion.idx,
                    qPromotion.name,
                    qPromotion.subName,
                    qPromotion.type,
                    qPromotion.details,
                    Expressions.stringTemplate("DATE_FORMAT({0}, {1})", qPromotion.startPeriod,
                        "%Y-%m-%d %T").as("startPeriod"),
                    Expressions.stringTemplate("DATE_FORMAT({0}, {1})", qPromotion.endPeriod,
                        "%Y-%m-%d %T").as("endPeriod")
                )).from(qPromotion)
            .where(qPromotion.idx.eq(idx).and(qPromotion.startPeriod.before(LocalDateTime.now())
                .and(qPromotion.endPeriod.after(LocalDateTime.now()))))
            .fetchOne();
    }

    @Override
    public List<PromotionDto.PromotionGroupGoods> findPromotionGoodsGroupById(
        final Integer promotionIdx) {
        QueryResults<PromotionDto.PromotionGroupGoods> results = queryFactory.select(
                Projections.bean(PromotionDto.PromotionGroupGoods.class,
                    qPromotionGroup.idx.as("promotionGroupGoodsIdx"),
                    qPromotionGroup.name,
                    goodsUserReview.idx.as("goodsIdx"),
                    goodsUserReview.goodsName.as("goodsName"),
                    qPromotionGroupGoods.prPrice.as("prPrice"),
                    qPromotionGroupGoods.prDiscountPrice.as("prDiscountPrice"),
                    qPromotionGroupGoods.prDiscountRate.as("prDiscountRate"),
                    new CaseBuilder()
                        .when(qGoodsImage.count().gt(0L)).then(qGoodsImage.url)
                        .when(qProductImage.count().gt(0L)).then(qProductImage.url)
                        .otherwise("").as("mainImage"))
            ).from(qPromotionGroup)
            .innerJoin(qPromotionGroupGoods)
            .on(qPromotionGroupGoods.prGroupIdx.eq(qPromotionGroup.idx))
            .innerJoin(goodsUserReview)
            .on(qPromotionGroupGoods.goodsIdx.eq(goodsUserReview.idx)
                .and(goodsUserReview.saleStatus.ne(PAUSE).and(goodsUserReview.saleStatus.ne(STOP))))
            .leftJoin(qProduct)
            .on(goodsUserReview.mainProduct.eq(qProduct.idx))
            .leftJoin(qGoodsImage)
            .on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))
            .leftJoin(qProductImage)
            .on(qProduct.idx.eq(qProductImage.productIdx)
                .and(qProductImage.type.eq(MAIN).and(qProductImage.priority.eq(MAIN))))
            .where(qPromotionGroup.prIdx.in(promotionIdx))
            .fetchResults();
        return results.getResults();
    }

    @Override
    public PromotionDto.PromotionGroupGoods2 promotionGroupGoodsInfo(final Integer goodsIdx,
        final Integer promotionIdx) {
        PromotionDto.PromotionGroupGoods2 results = queryFactory.select(
                Projections.bean(PromotionDto.PromotionGroupGoods2.class,
                    qPromotionGroup.idx.as("promotionGroupGoodsIdx"),
                    qPromotionGroup.name,
                    goodsUserReview.idx.as("goodsIdx"),
                    goodsUserReview.goodsName.as("goodsName"),
                    qPromotionGroupGoods.prPrice.as("prPrice"),
                    qPromotionGroupGoods.prDiscountPrice.as("prDiscountPrice"),
                    qPromotionGroupGoods.prDiscountRate.as("prDiscountRate"),
                    new CaseBuilder()
                        .when(qGoodsImage.count().gt(0L)).then(qGoodsImage.url)
                        .when(qProductImage.count().gt(0L)).then(qProductImage.url)
                        .otherwise("").as("mainImage"),
                    Expressions.stringTemplate("DATE_FORMAT({0}, {1})", qPromotion.startPeriod,
                        "%Y-%m-%d %T").as("startPeriod"),
                    Expressions.stringTemplate("DATE_FORMAT({0}, {1})", qPromotion.endPeriod,
                        "%Y-%m-%d %T").as("endPeriod"))
            ).from(goodsUserReview)
            .leftJoin(qPromotionGroupGoods)
            .on(qPromotionGroupGoods.goodsIdx.eq(goodsUserReview.idx))
            .leftJoin(qPromotionGroup)
            .on(qPromotionGroupGoods.prGroupIdx.eq(qPromotionGroup.idx))
            .leftJoin(qPromotion)
            .on(qPromotion.idx.eq(qPromotionGroup.prIdx))
            .leftJoin(qProduct)
            .on(goodsUserReview.mainProduct.eq(qProduct.idx))
            .leftJoin(qGoodsImage)
            .on(goodsUserReview.idx.eq(qGoodsImage.goodsIdx))
            .leftJoin(qProductImage)
            .on(qProduct.idx.eq(qProductImage.productIdx)
                .and(qProductImage.type.eq(MAIN).and(qProductImage.priority.eq(MAIN))))
            .where(qPromotionGroup.prIdx.in(promotionIdx)
                .and(goodsUserReview.saleStatus.notIn(PAUSE, STOP)
                    .and(goodsUserReview.idx.eq(goodsIdx))))
            .fetchOne();
        return results;
    }

    @Override
    public List<PromotionDto.PromotionGoodsListInfo> findPromotionListInfo() {
        List<PromotionDto.PromotionGoodsListInfo> results = queryFactory.select(
                Projections.bean(PromotionDto.PromotionGoodsListInfo.class,
                    qPromotion.idx,
                    qPromotion.name,
                    qPromotion.subName,
                    qPromotion.type,
                    qPromotion.pcId.as("pcIdx"),
                    qPromotionImage.url.as("mainImages"),
                    Expressions.stringTemplate("DATE_FORMAT({0}, {1})", qPromotion.startPeriod,
                        "%Y-%m-%d %T").as("startPeriod"),
                    Expressions.stringTemplate("DATE_FORMAT({0}, {1})", qPromotion.endPeriod,
                        "%Y-%m-%d %T").as("endPeriod")
                )).from(qPromotion)
            .leftJoin(qPromotionImage)
            .on(qPromotionImage.prIdx.eq(qPromotion.idx).and(qPromotionImage.type.eq(MAIN)))
            .where(qPromotion.startPeriod.before(LocalDateTime.now())
                .and(qPromotion.endPeriod.after(LocalDateTime.now()))
                .and(qPromotion.status.ne(PAUSE))
                .and(qPromotion.type.ne("H")))
            .orderBy(qPromotion.priority.asc())
            .fetchResults().getResults();
        return results;
    }

    @Override
    public List<PromotionDto.PromotionGoods> findPromotionGoodsDetail(final Integer idx) {

        List<PromotionDto.PromotionGoods> results = queryFactory.select(
                Projections.bean(PromotionDto.PromotionGoods.class,
                    goodsUserReview.idx.as("goodsIdx"),
                    new CaseBuilder()
                        .when(qGoodsImage.count().gt(0L)).then(qGoodsImage.url)
                        .when(qProductImage.count().gt(0L)).then(qProductImage.url)
                        .otherwise("").as("url"),
                    qPromotionGroupGoods.prPrice,
                    qPromotionGroupGoods.prDiscountPrice,
                    qPromotionGroupGoods.prDiscountRate,
                    goodsUserReview.goodsOriginPrice,
                    goodsUserReview.goodsPrice,
                    goodsUserReview.discountRate,
                    goodsUserReview.discountPrice,
                    goodsUserReview.reviewCnt,
                    goodsUserReview.averageRating,
                    goodsUserReview.saleStatus,
                    goodsUserReview.goodsName.as("name"),
                    qBrand.brandName,
                    goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(90)).as("newest")
                )).from(qPromotionGroupGoods)
            .innerJoin(goodsUserReview)
            .on(qPromotionGroupGoods.goodsIdx.eq(goodsUserReview.idx)
                .and(goodsUserReview.saleStatus.notIn(PAUSE, STOP)))
            .innerJoin(qProduct)
            .on(goodsUserReview.mainProduct.eq(qProduct.idx))
            .innerJoin(qBrand)
            .on(qProduct.brand.idx.eq(qBrand.idx))
            .leftJoin(qProductImage)
            .on(qProductImage.productIdx.eq(qProduct.idx).and(qProductImage.type.eq(MAIN))
                .and(qProductImage.priority.eq(MAIN)))
            .leftJoin(qGoodsImage)
            .on(qGoodsImage.goodsIdx.eq(goodsUserReview.idx).and(qGoodsImage.type.eq(MAIN)))
            .where(qPromotionGroupGoods.prGroupIdx.eq(idx))
            .groupBy(goodsUserReview.idx)
            .fetchResults().getResults();
        return results;
    }

    @Override
    public List<PromotionDto.PromotionGoods> findPromotionGoodsDetail(final List<Integer> idxList) {

        List<PromotionDto.PromotionGoods> results = queryFactory.select(
                Projections.bean(PromotionDto.PromotionGoods.class,
                    goodsUserReview.idx.as("goodsIdx"),
                    new CaseBuilder()
                        .when(qGoodsImage.count().gt(0L)).then(qGoodsImage.url)
                        .when(qProductImage.count().gt(0L)).then(qProductImage.url)
                        .otherwise("").as("url"),
                    qPromotionGroupGoods.prPrice,
                    qPromotionGroupGoods.prDiscountPrice,
                    qPromotionGroupGoods.prDiscountRate,
                    goodsUserReview.goodsOriginPrice,
                    goodsUserReview.goodsPrice,
                    goodsUserReview.discountRate,
                    goodsUserReview.discountPrice,
                    goodsUserReview.reviewCnt,
                    goodsUserReview.averageRating,
                    goodsUserReview.saleStatus,
                    goodsUserReview.goodsName.as("name"),
                    qPromotionGroupGoods.prGroupIdx.as("prGroupIdx"),
                    qBrand.brandName,
                    goodsUserReview.recordbirth.after(LocalDateTime.now().minusDays(90)).as("newest")
                )).from(qPromotionGroupGoods)
            .innerJoin(goodsUserReview)
            .on(qPromotionGroupGoods.goodsIdx.eq(goodsUserReview.idx)
                .and(goodsUserReview.saleStatus.notIn(PAUSE, STOP)))
            .innerJoin(qProduct)
            .on(goodsUserReview.mainProduct.eq(qProduct.idx))
            .innerJoin(qBrand)
            .on(qProduct.brand.idx.eq(qBrand.idx))
            .leftJoin(qProductImage)
            .on(qProductImage.productIdx.eq(qProduct.idx).and(qProductImage.type.eq(MAIN))
                .and(qProductImage.priority.eq(MAIN)))
            .leftJoin(qGoodsImage)
            .on(qGoodsImage.goodsIdx.eq(goodsUserReview.idx).and(qGoodsImage.type.eq(MAIN)))
            .where(qPromotionGroupGoods.prGroupIdx.in(idxList))
            .groupBy(goodsUserReview.idx)
            .fetchResults().getResults();
        return results;
    }
}

