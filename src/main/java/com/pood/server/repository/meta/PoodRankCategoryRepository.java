package com.pood.server.repository.meta;

import com.pood.server.entity.meta.PoodRankCategory;
import com.pood.server.repository.meta.customized.PoodRankCategoryCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PoodRankCategoryRepository extends JpaRepository<PoodRankCategory, Long>,
    PoodRankCategoryCustomRepository {

}
