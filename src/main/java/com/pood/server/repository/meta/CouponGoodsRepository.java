package com.pood.server.repository.meta;

import com.pood.server.entity.meta.CouponGoods;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CouponGoodsRepository extends JpaRepository<CouponGoods, Integer> {

    List<CouponGoods> findAllByCouponIdx(final int couponIdx);
}