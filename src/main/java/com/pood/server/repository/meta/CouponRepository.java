package com.pood.server.repository.meta;

import com.pood.server.entity.meta.Coupon;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CouponRepository extends JpaRepository<Coupon, Integer> {

    List<Coupon> findAllByPublishTypeIdxOrderByMaxPriceAsc(final int publishTypeIdx);
}