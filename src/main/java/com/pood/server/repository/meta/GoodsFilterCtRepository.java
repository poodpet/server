package com.pood.server.repository.meta;

import com.pood.server.entity.meta.GoodsFilterCt;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface GoodsFilterCtRepository extends JpaRepository<GoodsFilterCt, Long> {
    List<GoodsFilterCt> findAllByPetCtIdx(Integer idx);

    @Query("SELECT distinct gfc FROM GoodsFilterCt gfc join fetch gfc.petCt join fetch gfc.goodsCt where gfc.petCt.idx=?1")
    List<GoodsFilterCt> findAllByPetCtIdxFetchJoin(Integer idx);
}
