package com.pood.server.repository.meta.customized;


import com.pood.server.dto.meta.event.EndEvent;
import com.pood.server.dto.meta.event.EndPhotoAward;
import com.pood.server.dto.meta.event.EndPhotoImgUrl;
import com.pood.server.dto.meta.event.EventDto;
import com.pood.server.dto.meta.event.RunningPhotoAward;
import com.pood.server.entity.meta.mapper.event.EventListMapper;
import com.pood.server.entity.meta.mapper.event.EventDonationMapper;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface EventCustomRepository {

    Page<EventListMapper> getProgressEvents(final int petCategoryIdx, final Pageable pageable);

    Page<EventDto.EventUserComment> getEventCommentList(final Integer idx, final Pageable pageable,
        final Set<Integer> exceptUserIdxList);

    Page<EventDto.EventUserPhoto> getEventPhotoImgList(final Integer idx, final Pageable pageable,
        final Set<Integer> exceptUserIdxList);

    EventDto.EventDetail getEventDetail(final Integer idx);

    List<EventDto.MyEventList> getMyEventCommentList(final Integer userIdx);

    List<EventDto.MyEventList> getMyEventPhotoList(final Integer userIdx);

    List<EventDto.MyEventList> getMyEventParticipationList(final Integer userIdx);

    EventDto.MyCommentEventDetail findByCommentEventDetail(final Integer eventIdx,
        final Integer idx);

    EventDto.MyPhotoEventDetail findByPhotoEventDetail(final Integer eventIdx, final Integer idx);

    Optional<EventDto.MyJoinEventDetail> findByJoinEventDetail(final Integer eventIdx,
        final Integer idx);

    Page<EndEvent> findEndEventOneYearAgoToNow(final Pageable pageable, final int pcIdx,
        final LocalDateTime now);

    List<EndPhotoAward> findPhotoAwardWinners(final LocalDateTime startDate,
        final LocalDateTime yearLast, final int year, final List<Integer> exceptUserIdxList);

    List<EndPhotoAward> findEndPhotoAwards(final Integer eventTypeIdx,
        final LocalDateTime startDate,
        final LocalDateTime yearEnd, final int year);

    EndPhotoImgUrl findPhotoAwardsWinnerImage(final int eventIdx,
        final List<Integer> notInUserIdxList);

    Page<EndPhotoImgUrl> findPhotoAwardAllImage(final int eventIdx,
        final List<Integer> notInUserIdxList, final Pageable pageable);

    RunningPhotoAward runningPhotoAward();

    Optional<EventDonationMapper> findDonationEventByIdx(final Integer idx);
}
