package com.pood.server.repository.meta.customized;

import static com.pood.server.entity.meta.QAiRecommendDiagnosis.aiRecommendDiagnosis;
import static com.pood.server.entity.meta.QPetCategory.petCategory;
import static com.pood.server.entity.meta.QPetWorry.petWorry;

import com.pood.server.dto.meta.worry.PetWorryReadDto;
import com.pood.server.dto.meta.worry.QPetWorryReadDto;
import com.pood.server.entity.meta.mapper.AiRecommendDiagnosisMapper;
import com.pood.server.entity.meta.mapper.QAiRecommendDiagnosisMapper;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;

public class PetWorryRepositorySupportImpl implements PetWorryRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public PetWorryRepositorySupportImpl(@Qualifier("metaQueryFactory") final JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<PetWorryReadDto> getAllByPetCategoryOrderByPriority(final int petCategoryIdx) {
        return jpaQueryFactory.select(
                new QPetWorryReadDto(
                    petWorry.id,
                    petWorry.name,
                    petWorry.dogUrl,
                    petWorry.catUrl
                )
            )
            .from(petWorry)
            .innerJoin(petWorry.petCategory, petCategory)
            .where(petWorry.petCategory.idx.eq(0).or(petWorry.petCategory.idx.eq(petCategoryIdx)))
            .orderBy(petWorry.priority.asc())
            .fetch();
    }

    public AiRecommendDiagnosisMapper getDiagnosisInfoByWorryIdx(final Long worryIdx) {
        return jpaQueryFactory.select(
                new QAiRecommendDiagnosisMapper(
                    aiRecommendDiagnosis.idx,
                    aiRecommendDiagnosis.ardName,
                    aiRecommendDiagnosis.ardGroup,
                    aiRecommendDiagnosis.ardGroupCode,
                    aiRecommendDiagnosis.updatetime,
                    aiRecommendDiagnosis.recordbirth
                )
            )
            .from(petWorry)
            .innerJoin(aiRecommendDiagnosis)
            .on(aiRecommendDiagnosis.idx.eq(petWorry.aiRecommendDiagnosis.idx))
            .where(petWorry.id.eq(worryIdx))
            .fetchOne();
    }
}
