package com.pood.server.repository.meta;

import com.pood.server.entity.meta.PromotionGroupGoods;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PromotionGroupGoodsRepository extends JpaRepository<PromotionGroupGoods, Integer> {

    List<PromotionGroupGoods> findAllByPrGroupIdx(Integer idx);
}
