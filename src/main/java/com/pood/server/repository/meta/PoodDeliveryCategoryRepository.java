package com.pood.server.repository.meta;

import com.pood.server.controller.response.home.delivery.PoodDeliveryCategoryDto;
import com.pood.server.entity.meta.PoodDeliveryCategory;
import com.pood.server.repository.meta.customized.PoodDeliveryCategoryCustomRepository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PoodDeliveryCategoryRepository extends JpaRepository<PoodDeliveryCategory, Long>,
    PoodDeliveryCategoryCustomRepository {

    List<PoodDeliveryCategoryDto> getCategoryList(final int petIdx);

}
