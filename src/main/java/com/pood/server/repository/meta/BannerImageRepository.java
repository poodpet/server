package com.pood.server.repository.meta;

import com.pood.server.entity.meta.Banner;
import com.pood.server.entity.meta.BannerImage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BannerImageRepository extends JpaRepository<BannerImage, Integer> {

    List<BannerImage> findAllByBannerIdxIn(List<Integer> idxList);


}
