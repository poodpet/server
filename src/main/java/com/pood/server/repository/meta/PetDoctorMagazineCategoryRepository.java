package com.pood.server.repository.meta;

import com.pood.server.entity.meta.PetDoctorMagazineCategory;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetDoctorMagazineCategoryRepository extends
    JpaRepository<PetDoctorMagazineCategory, Long> {

    List<PetDoctorMagazineCategory> findAllByOrderByPriorityAsc();

}
