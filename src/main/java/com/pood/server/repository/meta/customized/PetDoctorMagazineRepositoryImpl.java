package com.pood.server.repository.meta.customized;

import com.pood.server.entity.meta.PetDoctorMagazine;
import com.pood.server.entity.meta.QPetDoctorMagazine;
import com.pood.server.entity.meta.QPetDoctorMagazineCategory;
import com.pood.server.entity.meta.mapper.doctor.PetDoctorMagazineDetailMapper;
import com.pood.server.entity.meta.mapper.doctor.PetDoctorMagazineMapper;
import com.pood.server.entity.meta.mapper.doctor.QPetDoctorMagazineDetailMapper;
import com.pood.server.entity.meta.mapper.doctor.QPetDoctorMagazineMapper;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public class PetDoctorMagazineRepositoryImpl implements PetDoctorMagazineCustomRepository {

    private final JPAQueryFactory queryFactory;
    private final static int COMMON = 0;

    public PetDoctorMagazineRepositoryImpl(
        @Qualifier("metaQueryFactory") final JPAQueryFactory jpaQueryFactory) {
        this.queryFactory = jpaQueryFactory;
    }

    QPetDoctorMagazine qPetDoctorMagazine = QPetDoctorMagazine.petDoctorMagazine;
    QPetDoctorMagazineCategory qPetDoctorMagazineCt = QPetDoctorMagazineCategory.petDoctorMagazineCategory;

    @Override
    public Page<PetDoctorMagazineMapper> getListByCtidx(final Long ctIdx, final int pcIdx,
        final Pageable pageable) {
        QueryResults<PetDoctorMagazineMapper> results =
            queryFactory.select(new QPetDoctorMagazineMapper(
                    qPetDoctorMagazine.idx,
                    qPetDoctorMagazineCt.imgUrl,
                    qPetDoctorMagazineCt.name,
                    qPetDoctorMagazine.title,
                    qPetDoctorMagazine.recordBirth
                )).from(qPetDoctorMagazine)
                .innerJoin(qPetDoctorMagazineCt)
                .on(qPetDoctorMagazineCt.idx.eq(qPetDoctorMagazine.petDoctorMagazineCategory.idx))
                .where(qPetDoctorMagazine.isDeleted.eq(false)
                    .and(qPetDoctorMagazine.isVisible.eq(true))
                    .and(qPetDoctorMagazine.petCategory.idx.in(COMMON, pcIdx))
                    .and(eqCtIdx(ctIdx)))
                .orderBy(orderCondition(pageable, PetDoctorMagazine.class, "petDoctorMagazine"))
                .offset(pageable.getOffset())
                .limit(pageable.getPageSize())
                .fetchResults();
        pageable.getSort();
        long count = results.getTotal();
        return new PageImpl<>(results.getResults(), pageable, count);
    }

    @Override
    public Optional<PetDoctorMagazineDetailMapper> getMagazineDetail(final long idx) {
        final PetDoctorMagazineDetailMapper petDoctorMagazineDetailMapper = queryFactory.select(
                new QPetDoctorMagazineDetailMapper(
                    qPetDoctorMagazine.idx,
                    qPetDoctorMagazine.contents,
                    qPetDoctorMagazineCt.name,
                    qPetDoctorMagazine.title
                )).from(qPetDoctorMagazine)
            .innerJoin(qPetDoctorMagazineCt)
            .on(qPetDoctorMagazineCt.idx.eq(qPetDoctorMagazine.petDoctorMagazineCategory.idx))
            .where(qPetDoctorMagazine.idx.eq(idx))
            .fetchOne();
        return Optional.ofNullable(petDoctorMagazineDetailMapper);

    }

    private BooleanExpression eqCtIdx(final Long ctIdx) {
        if (Objects.isNull(ctIdx)) {
            return null;
        }
        return qPetDoctorMagazineCt.idx.eq(ctIdx);
    }

    private OrderSpecifier[] orderCondition(final Pageable pageable, final Class clazz,
        final String variable) {
        PathBuilder<PetDoctorMagazine> entityPath = new PathBuilder<>(clazz, variable);
        return pageable.getSort()
            .stream()
            .map(order -> new OrderSpecifier(Order.valueOf(order.getDirection().name()),
                entityPath.get(order.getProperty())))
            .toArray(OrderSpecifier[]::new);
    }
}
