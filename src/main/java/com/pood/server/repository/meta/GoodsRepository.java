package com.pood.server.repository.meta;

import com.pood.server.entity.meta.Goods;
import com.pood.server.entity.meta.GoodsSubCt;
import com.pood.server.repository.meta.customized.GoodsCustomRepository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface GoodsRepository extends JpaRepository<Goods, Integer>, GoodsCustomRepository,
    QuerydslPredicateExecutor<Goods> {

    List<Goods> findAllByIdxIn(List<Integer> idxList);

    List<Goods> findAllBySaleStatusIn(final List<Integer> saleStatusList);

    @Query("SELECT m.idx FROM Goods m WHERE m.saleStatus <> 0 AND m.saleStatus <> 2")
    List<Integer> findAllByJpqlIdxColumn();

    List<Integer> getRankGoodsIdxList(final GoodsSubCt goodsSubCt, final int pcIdx);

    List<Integer> getRankGoodsIdxList(final int pcIdx);

}
