package com.pood.server.repository.meta;

import com.pood.server.entity.meta.EventComment;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventCommentRepository extends JpaRepository<EventComment, Long> {

    EventComment findByIdxAndEventInfoIdxAndUserInfoIdx(Long idx, Integer eventIdx, Integer idx1);

    List<EventComment> findByEventInfoIdxAndUserInfoIdx(Integer eventInfoIdx, Integer userInfoIdx);

    int countAllByEventInfoIdxAndUserInfoIdx(Integer eventInfoIdx, Integer userInfoIdx);
}
