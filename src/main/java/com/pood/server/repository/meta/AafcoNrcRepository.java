package com.pood.server.repository.meta;

import com.pood.server.entity.meta.AafcoNrc;
import com.pood.server.entity.meta.Feed;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AafcoNrcRepository extends JpaRepository<AafcoNrc, Integer> {

    List<AafcoNrc> findAllByPetIdx(Integer petType, Sort sort);

}
