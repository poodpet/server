package com.pood.server.repository.meta.customized;


import static com.pood.server.entity.meta.QMarketingSort.marketingSort;
import static com.pood.server.entity.meta.QPetCategory.petCategory;

import com.pood.server.config.querydsl.MetaQuerydslRepositorySupport;
import com.pood.server.dto.meta.event.EndEvent;
import com.pood.server.dto.meta.event.EndPhotoAward;
import com.pood.server.dto.meta.event.EndPhotoImgUrl;
import com.pood.server.dto.meta.event.EventDto;
import com.pood.server.dto.meta.event.QEndPhotoAward;
import com.pood.server.dto.meta.event.QEndPhotoImgUrl;
import com.pood.server.dto.meta.event.QRunningPhotoAward;
import com.pood.server.dto.meta.event.RunningPhotoAward;
import com.pood.server.entity.meta.EventComment;
import com.pood.server.entity.meta.EventInfo;
import com.pood.server.entity.meta.EventPhoto;
import com.pood.server.entity.meta.QEventComment;
import com.pood.server.entity.meta.QEventImage;
import com.pood.server.entity.meta.QEventInfo;
import com.pood.server.entity.meta.QEventParticipation;
import com.pood.server.entity.meta.QEventPhoto;
import com.pood.server.entity.meta.QEventType;
import com.pood.server.entity.meta.mapper.event.EventListMapper;
import com.pood.server.entity.meta.mapper.event.EventDonationMapper;
import com.pood.server.entity.meta.mapper.event.QEventDonationMapper;
import com.pood.server.entity.meta.mapper.event.QEventListMapper;
import com.pood.server.util.MarketingType;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.CaseBuilder;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;


public class EventRepositoryImpl extends MetaQuerydslRepositorySupport implements
    EventCustomRepository {

    private static final int MAIN_IMAGE = 0;
    private static final int COMMON_PET_CATEGORY = 0;
    public static final int EVENT_PAUSED = 3;
    public static final int EVENT_READY = 0;
    public static final String WINNER = "O";
    public static final int PHOTO_AWARDS = 31;
    public static final int DONATION = 26;

    private final JPAQueryFactory queryFactory;

    public EventRepositoryImpl(@Qualifier("metaQueryFactory") final JPAQueryFactory queryFactory) {
        super(EventInfo.class);
        this.queryFactory = queryFactory;
    }

    QEventImage qEventImage = QEventImage.eventImage;
    QEventInfo qEventInfo = QEventInfo.eventInfo;
    QEventType qEventType = QEventType.eventType;


    QEventComment qEventComment = QEventComment.eventComment;
    QEventPhoto qEventPhoto = QEventPhoto.eventPhoto;
    QEventParticipation qEventParticipation = QEventParticipation.eventParticipation;

    @Override
    public Page<EventListMapper> getProgressEvents(final int petCategoryIdx,
        final Pageable pageable) {

        QueryResults<EventListMapper> results =
            queryFactory.select(
                new QEventListMapper(
                    qEventInfo.idx,
                    qEventInfo.title,
                    qEventInfo.intro,
                    Expressions.stringTemplate("DATE_FORMAT({0}, {1})", qEventInfo.startDate,
                        "%Y-%m-%d %T").as("startDate"),
                    Expressions.stringTemplate("DATE_FORMAT({0}, {1})", qEventInfo.endDate,
                        "%Y-%m-%d %T").as("endDate"),
                    qEventImage.url.as("imgUrl"),
                    marketingSort.type,
                    qEventType.typeName,
                    petCategory.idx.as("pcIdx")
                )).from(qEventInfo)
                .innerJoin(qEventType)
                .on(qEventInfo.eventTypeIdx.eq(qEventType.idx))

                .leftJoin(qEventImage)
                .on(qEventInfo.idx.eq(qEventImage.eventIdx).and(qEventImage.type.eq(0)))

                .innerJoin(marketingSort).on(qEventInfo.idx.eq(marketingSort.marketingInfoIdx))

                .innerJoin(petCategory)
                .on(qEventInfo.pcId.eq(petCategory.idx))

                .where(qEventInfo.startDate.loe(LocalDateTime.now())
                    .and(qEventInfo.endDate.goe(LocalDateTime.now()))
                    .and(isNotPaused())
                    .and(isNotReady())
                    .and(isNotDelete())
                    .and(petCategory.idx.eq(petCategoryIdx)
                        .or(petCategory.idx.eq(COMMON_PET_CATEGORY))))
                .where(marketingSort.type.in(MarketingType.EVENT, MarketingType.DONATION))
                .groupBy(qEventInfo.idx)
                .orderBy(marketingSort.priority.asc())
                .offset(pageable.getOffset())
                .limit(pageable.getPageSize())
                .fetchResults();
        pageable.getSort();
        long count = results.getTotal();
        return new PageImpl<>(results.getResults(), pageable, count);
    }

    @Override
    public Page<EventDto.EventUserComment> getEventCommentList(final Integer idx,
        final Pageable pageable, final Set<Integer> exceptUserIdxList) {
        QueryResults<EventDto.EventUserComment> results =
            queryFactory.select(Projections.bean(EventDto.EventUserComment.class,
                    qEventComment.idx,
                    qEventComment.userInfoIdx,
                    qEventComment.comment,
                    qEventComment.recordbirth
                )).from(qEventComment)
                .where(qEventComment.eventInfo.idx.eq(idx))
                .where(notInUserIdx(exceptUserIdxList, qEventComment.userInfoIdx))
                .orderBy(orderCondition(pageable, EventComment.class, "eventComment")) // (1)
                .offset(pageable.getOffset())
                .limit(pageable.getPageSize())
                .fetchResults();
        pageable.getSort();

        long count = results.getTotal();
        return new PageImpl<>(results.getResults(), pageable, count);
    }

    @Override
    public Page<EventDto.EventUserPhoto> getEventPhotoImgList(final Integer eventInfoIdx,
        final Pageable pageable, final Set<Integer> exceptUserIdxList) {
        QueryResults<EventDto.EventUserPhoto> results =
            queryFactory.select(Projections.bean(EventDto.EventUserPhoto.class,
                    qEventPhoto.idx,
                    qEventPhoto.userInfoIdx,
                    qEventPhoto.url,
                    qEventPhoto.recordbirth
                )).from(qEventPhoto)
                .where(qEventPhoto.eventInfo.idx.eq(eventInfoIdx))
                .where(notInUserIdx(exceptUserIdxList, qEventPhoto.userInfoIdx))
                .orderBy(orderCondition(pageable, EventPhoto.class, "eventPhoto")) // (1)
                .offset(pageable.getOffset())
                .groupBy(qEventPhoto.userInfoIdx)
                .limit(pageable.getPageSize())
                .fetchResults();
        pageable.getSort();

        long count = results.getTotal();
        return new PageImpl<>(results.getResults(), pageable, count);
    }

    private BooleanExpression notInUserIdx(final Set<Integer> userIdxList,
        final NumberPath<Integer> userInfoIdx) {
        if (ObjectUtils.isEmpty(userIdxList)) {
            return null;
        }
        return userInfoIdx.notIn(userIdxList);
    }


    @Override
    public EventDto.EventDetail getEventDetail(final Integer idx) {
        return queryFactory.select(Projections.bean(EventDto.EventDetail.class,
                qEventInfo.idx,
                qEventInfo.title,
                qEventInfo.intro,
                qEventInfo.status,
                qEventInfo.eventBtn,
                qEventInfo.schemeUri,
                qEventInfo.attendLimit,
                Expressions.stringTemplate("DATE_FORMAT({0}, {1})", qEventInfo.startDate,
                    "%Y-%m-%d %T").as("startDate"),
                Expressions.stringTemplate("DATE_FORMAT({0}, {1})", qEventInfo.endDate,
                    "%Y-%m-%d %T").as("endDate"),
                qEventType.typeName,
                qEventInfo.details,
                Expressions.asBoolean(true).as("participationPossible")
            )).from(qEventInfo)
            .innerJoin(qEventType)
            .on(qEventInfo.eventTypeIdx.eq(qEventType.idx))
            .where(qEventInfo.idx.eq(idx))
            .fetchOne();
    }

    @Override
    public List<EventDto.MyEventList> getMyEventCommentList(final Integer userIdx) {
        return queryFactory.select(Projections.bean(EventDto.MyEventList.class,
                qEventInfo.idx,
                qEventInfo.title,
                qEventInfo.intro,
                qEventImage.url.as("imgUrl"),
                qEventInfo.innerTitle,
                Expressions.asSimple("C").as("type"),
                qEventInfo.startDate,
                qEventInfo.endDate
            )).from(qEventInfo)
            .innerJoin(qEventComment)
            .on(qEventComment.eventInfo.idx.eq(qEventInfo.idx))
            .innerJoin(qEventImage).on(qEventInfo.idx.eq(qEventImage.eventIdx))
            .where(qEventComment.userInfoIdx.eq(userIdx)
                .and(qEventImage.type.eq(MAIN_IMAGE)))
            .groupBy(qEventInfo.idx)
            .fetch();
    }

    @Override
    public List<EventDto.MyEventList> getMyEventPhotoList(final Integer userIdx) {
        return queryFactory.select(Projections.bean(EventDto.MyEventList.class,
                qEventInfo.idx,
                qEventInfo.title,
                qEventInfo.intro,
                qEventImage.url.as("imgUrl"),
                qEventInfo.innerTitle,
                Expressions.asSimple("P").as("type"),
                qEventInfo.startDate,
                qEventInfo.endDate
            )).from(qEventInfo)
            .innerJoin(qEventPhoto)
            .on(qEventPhoto.eventInfo.idx.eq(qEventInfo.idx))
            .innerJoin(qEventImage).on(qEventInfo.idx.eq(qEventImage.eventIdx))
            .where(qEventPhoto.userInfoIdx.eq(userIdx).and(qEventImage.type.eq(MAIN_IMAGE)))
            .groupBy(qEventInfo.idx)
            .fetch();
    }

    @Override
    public List<EventDto.MyEventList> getMyEventParticipationList(final Integer userIdx) {
        return queryFactory.select(Projections.bean(EventDto.MyEventList.class,
                qEventInfo.idx,
                qEventInfo.title,
                qEventInfo.intro,
                qEventImage.url.as("imgUrl"),
                qEventInfo.innerTitle,
                Expressions.asSimple("J").as("type"),
                qEventInfo.startDate,
                qEventInfo.endDate
            )).from(qEventInfo)
            .innerJoin(qEventParticipation)
            .on(qEventParticipation.eventInfo.idx.eq(qEventInfo.idx))
            .innerJoin(qEventImage).on(qEventInfo.idx.eq(qEventImage.eventIdx))
            .where(qEventParticipation.userInfoIdx.eq(userIdx)
                .and(qEventImage.type.eq(MAIN_IMAGE)))
            .groupBy(qEventInfo.idx)
            .fetch();
    }

    @Override
    public EventDto.MyCommentEventDetail findByCommentEventDetail(final Integer eventIdx,
        final Integer userIdx) {
        return queryFactory.select(Projections.bean(EventDto.MyCommentEventDetail.class,
                qEventInfo.idx,
                qEventInfo.title,
                qEventInfo.intro,
                qEventInfo.innerTitle,
                qEventInfo.startDate,
                qEventInfo.endDate
            )).from(qEventInfo)
            .innerJoin(qEventComment)
            .on(qEventComment.eventInfo.idx.eq(qEventInfo.idx))
            .where(qEventInfo.idx.eq(eventIdx).and(qEventComment.userInfoIdx.eq(userIdx)))
            .groupBy(qEventInfo.idx)
            .fetchOne();
    }

    @Override
    public EventDto.MyPhotoEventDetail findByPhotoEventDetail(final Integer eventIdx,
        final Integer userIdx) {
        return queryFactory.select(Projections.bean(EventDto.MyPhotoEventDetail.class,
                qEventInfo.idx,
                qEventInfo.title,
                qEventInfo.intro,
                qEventInfo.innerTitle,
                qEventInfo.startDate,
                qEventInfo.endDate
            )).from(qEventInfo)
            .innerJoin(qEventPhoto)
            .on(qEventPhoto.eventInfo.idx.eq(qEventInfo.idx))
            .where(qEventInfo.idx.eq(eventIdx).and(qEventPhoto.userInfoIdx.eq(userIdx)))
            .groupBy(qEventInfo.idx).fetchOne();
    }

    @Override
    public Optional<EventDto.MyJoinEventDetail> findByJoinEventDetail(final Integer eventIdx,
        final Integer userIdx) {
        return Optional.ofNullable(
            queryFactory.select(Projections.bean(EventDto.MyJoinEventDetail.class,
                    qEventInfo.idx,
                    qEventInfo.title,
                    qEventInfo.intro,
                    qEventInfo.innerTitle,
                    qEventInfo.startDate,
                    qEventInfo.endDate,
                    new CaseBuilder()
                        .when(qEventInfo.startDate.before(LocalDateTime.now())
                            .and(qEventInfo.endDate.after(LocalDateTime.now()))).then("R")
                        .when(qEventParticipation.ranking.isNull()).then("N")
                        .otherwise(qEventParticipation.ranking).as("ranking"),
                    Projections.bean(EventDto.EventParticipationDetail.class,
                        qEventParticipation.idx,
                        qEventParticipation.userInfoIdx,
                        qEventParticipation.recordbirth
                    ).as("eventParticipation")
                )).from(qEventInfo)
                .innerJoin(qEventParticipation)
                .on(qEventParticipation.eventInfo.idx.eq(qEventInfo.idx))
                .where(qEventInfo.idx.eq(eventIdx).and(qEventParticipation.userInfoIdx.eq(userIdx)))
                .fetchOne());
    }

    @Override
    public Page<EndEvent> findEndEventOneYearAgoToNow(final Pageable pageable, final int pcIdx,
        final LocalDateTime now) {
        final QueryResults<EndEvent> queryResults = queryFactory.select(
                Projections.constructor(EndEvent.class,
                    qEventInfo.idx,
                    qEventInfo.title,
                    qEventInfo.intro,
                    qEventInfo.startDate,
                    qEventInfo.endDate,
                    qEventImage.url,
                    marketingSort.type,
                    qEventType.typeName,
                    petCategory.idx.as("pcIdx")
                )
            )
            .from(qEventInfo)
            .innerJoin(qEventType).on(qEventInfo.eventTypeIdx.eq(qEventType.idx))
            .leftJoin(qEventImage).on(qEventInfo.idx.eq(qEventImage.eventIdx).and(qEventImage.type.eq(MAIN_IMAGE)))
            .innerJoin(marketingSort).on(qEventInfo.idx.eq(marketingSort.marketingInfoIdx))
            .innerJoin(petCategory).on(qEventInfo.pcId.eq(petCategory.idx))
            .where(isNotDelete()
                .and(isNotPaused())
                .and(qEventInfo.pcId.eq(COMMON_PET_CATEGORY).or(qEventInfo.pcId.eq(pcIdx)))
                .and(qEventImage.type.eq(MAIN_IMAGE))
                .and(qEventInfo.endDate.loe(now))
                .and(qEventInfo.startDate.goe(now.minusYears(1L)))
                .and(petCategory.idx.eq(pcIdx)
                    .or(petCategory.idx.eq(COMMON_PET_CATEGORY)))
            )
            .where(marketingSort.type.in(MarketingType.EVENT, MarketingType.DONATION))
            .groupBy(qEventInfo.idx)
            .orderBy(qEventInfo.idx.desc())
            .offset(pageable.getOffset())
            .limit(pageable.getPageSize())
            .fetchResults();

        return new PageImpl<>(queryResults.getResults(), pageable, queryResults.getTotal());
    }

    @Override
    public List<EndPhotoAward> findPhotoAwardWinners(final LocalDateTime startDate,
        final LocalDateTime yearLast, final int year, final List<Integer> exceptUserIdxList) {
        return queryFactory.select(
                new QEndPhotoAward(
                    qEventInfo.idx,
                    qEventPhoto.url
                )
            ).from(qEventPhoto)
            .innerJoin(qEventInfo).on(qEventPhoto.eventInfo.idx.eq(qEventInfo.idx))
            .where(qEventInfo.startDate.goe(startDate)
                .and(qEventInfo.endDate.loe(yearLast))
                .and(qEventInfo.eventTypeIdx.eq(PHOTO_AWARDS))
                .and(qEventPhoto.ranking.eq(WINNER))
            )
            .where(userIdxListNotin(exceptUserIdxList))
            .where(isThisYearThenApply(year))
            .orderBy(qEventInfo.idx.desc())
            .fetch();
    }

    private BooleanExpression isThisYearThenApply(final int year) {
        final LocalDateTime expiredTime = LocalDateTime.now();
        if (year == expiredTime.getYear()) {
            return qEventInfo.endDate.loe(expiredTime);
        }
        return null;
    }

    @Override
    public List<EndPhotoAward> findEndPhotoAwards(final Integer eventTypeIdx,
        final LocalDateTime startDate, final LocalDateTime yearEnd,
        final int year) {

        return queryFactory.select(
                new QEndPhotoAward(
                    qEventInfo.idx,
                    qEventImage.url
                ))
            .from(qEventInfo)
            .innerJoin(qEventImage).on(qEventImage.eventIdx.eq(qEventInfo.idx)
                .and(qEventImage.type.eq(0)))
            .where(qEventInfo.eventTypeIdx.eq(eventTypeIdx)
                .and(qEventInfo.startDate.goe(startDate))
                .and(qEventInfo.endDate.loe(yearEnd))
                .and(isNotPaused())
                .and(isNotReady())
                .and(isNotDelete())
            )
            .where(isThisYearThenApply(year))
            .orderBy(qEventInfo.idx.desc())
            .fetch();
    }

    @Override
    public EndPhotoImgUrl findPhotoAwardsWinnerImage(final int eventIdx,
        final List<Integer> notInUserIdxList) {
        return queryFactory.select(
                new QEndPhotoImgUrl(qEventPhoto.url)
            ).from(qEventPhoto)
            .innerJoin(qEventInfo).on(qEventPhoto.eventInfo.idx.eq(qEventInfo.idx))
            .where(qEventInfo.idx.eq(eventIdx)
                .and(qEventPhoto.ranking.eq(WINNER))
            ).where(userIdxListNotin(notInUserIdxList))
            .fetchOne();
    }

    private BooleanExpression userIdxListNotin(final List<Integer> notInUserIdxList) {
        if (notInUserIdxList.isEmpty()) {
            return null;
        }
        return qEventPhoto.userInfoIdx.notIn(notInUserIdxList);
    }

    private BooleanExpression isNotPaused() {
        return qEventInfo.status.ne(EVENT_PAUSED);
    }

    private BooleanExpression isNotReady() {
        return qEventInfo.status.ne(EVENT_READY);
    }

    private BooleanExpression isNotDelete() {
        return qEventInfo.isDeleted.eq(false);
    }

    @Override
    public Page<EndPhotoImgUrl> findPhotoAwardAllImage(final int eventIdx,
        final List<Integer> notInUserIdxList, final Pageable pageable) {
        final QueryResults<EndPhotoImgUrl> results = queryFactory.select(
                new QEndPhotoImgUrl(qEventPhoto.url)
            ).from(qEventPhoto)
            .innerJoin(qEventInfo).on(qEventPhoto.eventInfo.idx.eq(qEventInfo.idx))
            .where(qEventInfo.idx.eq(eventIdx)
                .and(qEventPhoto.ranking.isNull().or(qEventPhoto.ranking.ne(WINNER)))
            )
            .where(userIdxListNotin(notInUserIdxList))
            .orderBy(qEventPhoto.idx.desc())
            .orderBy(orderCondition(pageable, EventPhoto.class, "eventPhoto"))
            .offset(pageable.getOffset())
            .limit(pageable.getPageSize())
            .fetchResults();
        pageable.getSort();
        long count = results.getTotal();

        return new PageImpl<>(results.getResults(), pageable, count);
    }

    @Override
    public RunningPhotoAward runningPhotoAward() {
        return queryFactory.select(
                new QRunningPhotoAward(qEventInfo.idx)
            ).from(qEventInfo)
            .where(qEventInfo.eventTypeIdx.eq(PHOTO_AWARDS)
                .and(qEventInfo.startDate.loe(LocalDateTime.now()))
                .and(qEventInfo.endDate.goe(LocalDateTime.now()))
                .and(isNotPaused())
                .and(isNotReady())
                .and(isNotDelete())
            )
            .fetchOne();
    }

    @Override
    public Optional<EventDonationMapper> findDonationEventByIdx(final Integer idx) {

        return Optional.ofNullable(queryFactory.select(
                new QEventDonationMapper(
                    qEventInfo.idx,
                    qEventInfo.title,
                    qEventInfo.intro,
                    Expressions.stringTemplate("DATE_FORMAT({0}, {1})", qEventInfo.startDate,
                        "%Y-%m-%d %T").as("startDate"),
                    Expressions.stringTemplate("DATE_FORMAT({0}, {1})", qEventInfo.endDate,
                        "%Y-%m-%d %T").as("endDate"),
                    qEventInfo.status,
                    qEventInfo.pcId
                )
            ).from(qEventInfo)
            .where(qEventInfo.idx.eq(idx)
                .and(qEventInfo.eventTypeIdx.eq(DONATION)))
            .fetchFirst());
    }

    private OrderSpecifier[] orderCondition(final Pageable pageable, final Class clazz,
        final String variable) {
        PathBuilder<EventInfo> entityPath = new PathBuilder<>(clazz, variable);
        return pageable.getSort() // (2)
            .stream() // (3)
            .map(order -> new OrderSpecifier(Order.valueOf(order.getDirection().name()),
                entityPath.get(order.getProperty()))) // (4)
            .toArray(OrderSpecifier[]::new); // (5)
    }
}

