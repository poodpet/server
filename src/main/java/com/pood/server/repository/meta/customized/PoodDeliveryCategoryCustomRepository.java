package com.pood.server.repository.meta.customized;

import com.pood.server.controller.response.home.delivery.PoodDeliveryCategoryDto;
import java.util.List;

public interface PoodDeliveryCategoryCustomRepository {

    List<PoodDeliveryCategoryDto> getCategoryList(final int petIdx);

}
