package com.pood.server.repository.meta;

import com.pood.server.entity.meta.NoticeType;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NoticeTypeRepository extends JpaRepository<NoticeType, Integer> {

    Optional<NoticeType> findByCode(final String code);

}
