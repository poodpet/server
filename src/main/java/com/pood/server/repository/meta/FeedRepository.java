package com.pood.server.repository.meta;

import com.pood.server.entity.meta.Feed;
import com.pood.server.entity.meta.Product;
import io.swagger.models.auth.In;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FeedRepository extends JpaRepository<Feed, Integer> {

    Feed findByProductIdx(Integer productIdx);

    List<Feed> findAllByProductIdxIn(List<Integer> productIdx);


}
