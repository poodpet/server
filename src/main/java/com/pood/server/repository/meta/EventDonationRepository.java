package com.pood.server.repository.meta;

import com.pood.server.entity.meta.EventDonation;
import com.pood.server.repository.meta.customized.EventDonationCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventDonationRepository extends JpaRepository<EventDonation, Long>,
    EventDonationCustomRepository {

    long countAllByEventInfoIdx(final Integer idx);
}
