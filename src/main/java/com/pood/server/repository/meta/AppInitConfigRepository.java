package com.pood.server.repository.meta;

import com.pood.server.entity.meta.AppInitConfig;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppInitConfigRepository extends JpaRepository<AppInitConfig, String> {

}