package com.pood.server.repository.meta.customized;

import com.pood.server.entity.meta.mapper.event.EventDonationThumbnailMapper;
import java.util.List;

public interface EventDonationCustomRepository {

    boolean existsByUserUuidAtToday(final String UserUuid);

    List<EventDonationThumbnailMapper> findThumbnailAll();
}
