package com.pood.server.repository.meta;

import com.pood.server.entity.meta.Faq;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FaqRepository extends JpaRepository<Faq, Long> {

    List<Faq> findAllByFaqCategoryIdxOrderByPriorityAsc(final long faqCategoryIdx);

}
