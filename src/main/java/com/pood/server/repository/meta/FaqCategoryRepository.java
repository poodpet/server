package com.pood.server.repository.meta;

import com.pood.server.entity.meta.FaqCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FaqCategoryRepository extends JpaRepository<FaqCategory, Long> {


}
