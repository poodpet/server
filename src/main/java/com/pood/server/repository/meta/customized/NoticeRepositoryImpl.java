package com.pood.server.repository.meta.customized;

import com.pood.server.entity.meta.Notice;
import com.pood.server.entity.meta.PetDoctorMagazine;
import com.pood.server.entity.meta.QNotice;
import com.pood.server.entity.meta.QNoticeType;
import com.pood.server.entity.meta.mapper.notice.NoticeDetailMapper;
import com.pood.server.entity.meta.mapper.notice.NoticePageMapper;
import com.pood.server.entity.meta.mapper.notice.QNoticeDetailMapper;
import com.pood.server.entity.meta.mapper.notice.QNoticePageMapper;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public class NoticeRepositoryImpl implements NoticeCustomRepository {

    private final JPAQueryFactory queryFactory;

    public NoticeRepositoryImpl(
        @Qualifier("metaQueryFactory") final JPAQueryFactory jpaQueryFactory) {
        this.queryFactory = jpaQueryFactory;
    }

    QNotice qNotice = QNotice.notice;
    QNoticeType qNoticeType = QNoticeType.noticeType;

    @Override
    public Page<NoticePageMapper> getNoticePage(Pageable pageable) {
        QueryResults<NoticePageMapper> results =
            queryFactory.select(new QNoticePageMapper(
                    qNotice.idx,
                    qNoticeType.typeName,
                    qNotice.title,
                    qNotice.recordbirth
                )).from(qNotice)
                .innerJoin(qNoticeType)
                .on(qNoticeType.idx.eq(qNotice.noticeType.idx))
                .where(qNotice.isDeleted.eq(false))
                .orderBy(orderCondition(pageable, PetDoctorMagazine.class, "notice"))
                .offset(pageable.getOffset())
                .limit(pageable.getPageSize())
                .fetchResults();
        pageable.getSort();
        long count = results.getTotal();
        return new PageImpl<>(results.getResults(), pageable, count);
    }

    @Override
    public Optional<NoticeDetailMapper> getNoticeDetail(int idx) {
        final NoticeDetailMapper noticeDetailMapper = queryFactory.select(new QNoticeDetailMapper(
                qNotice.idx,
                qNotice.title,
                qNotice.text,
                qNotice.recordbirth
            )).from(qNotice)
            .innerJoin(qNoticeType)
            .on(qNoticeType.idx.eq(qNotice.noticeType.idx))
            .where(qNotice.isDeleted.eq(false)
                .and(qNotice.idx.eq(idx)))
            .fetchOne();
        return Optional.ofNullable(noticeDetailMapper);
    }

    private OrderSpecifier[] orderCondition(final Pageable pageable, final Class clazz,
        final String variable) {
        PathBuilder<Notice> entityPath = new PathBuilder<>(clazz, variable);
        return pageable.getSort()
            .stream()
            .map(order -> new OrderSpecifier(Order.valueOf(order.getDirection().name()),
                entityPath.get(order.getProperty())))
            .toArray(OrderSpecifier[]::new);
    }
}
