package com.pood.server.repository.meta.customized;


import com.pood.server.entity.meta.mapper.doctor.PetDoctorMagazineDetailMapper;
import com.pood.server.entity.meta.mapper.doctor.PetDoctorMagazineMapper;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PetDoctorMagazineCustomRepository {

    Page<PetDoctorMagazineMapper> getListByCtidx(final Long ctIdx, final int pcIdx,
        final Pageable page);

    Optional<PetDoctorMagazineDetailMapper> getMagazineDetail(final long idx);
}
