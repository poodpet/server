package com.pood.server.repository.meta.customized;

import static com.pood.server.entity.meta.QPet.pet;

import com.pood.server.config.querydsl.MetaQuerydslRepositorySupport;
import com.pood.server.entity.meta.Pet;
import com.querydsl.core.types.dsl.CaseBuilder;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;

public class PetRepositoryImpl extends MetaQuerydslRepositorySupport implements
    PetCustomRepository {

    private final JPAQueryFactory queryFactory;

    public PetRepositoryImpl(@Qualifier("metaQueryFactory") JPAQueryFactory queryFactory) {
        super(Pet.class);
        this.queryFactory = queryFactory;
    }

    @Override
    public List<Pet> findAllOrderByPcKindDesc() {
        return queryFactory.selectFrom(pet)
            .orderBy(mixPrioritySort().asc())
            .fetch();
    }

    private StringExpression mixPrioritySort() {
        return new CaseBuilder()
            .when(pet.pcKind.eq("믹스")).then("0")
            .when(pet.pcKind.eq("믹스 소형")).then("1")
            .when(pet.pcKind.eq("믹스 중형")).then("2")
            .when(pet.pcKind.eq("믹스 대형")).then("3")
            .otherwise(pet.pcKind);
    }
}
