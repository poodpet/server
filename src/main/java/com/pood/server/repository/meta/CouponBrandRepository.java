package com.pood.server.repository.meta;

import com.pood.server.entity.meta.CouponBrand;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CouponBrandRepository extends JpaRepository<CouponBrand, Integer> {

    List<CouponBrand> findAllByCouponIdx(final int couponIdx);
}