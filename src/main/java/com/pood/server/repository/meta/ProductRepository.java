package com.pood.server.repository.meta;

import com.pood.server.entity.meta.Product;
import com.pood.server.repository.meta.customized.ProductCustomRepository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface ProductRepository extends JpaRepository<Product, Integer> , ProductCustomRepository, QuerydslPredicateExecutor<Product> {
    List<Product> findAllByCtIdxAndApprove(Integer ctIdx, Boolean approve);
    List<Product> findAllByCtIdxAndApproveAndIsRecommendAndPcIdxInAndBrandIdxNotAndIdxNotIn(Integer ctIdx, Boolean approve,Integer isRecommend, List<Integer> pcIdx, Integer brandIdx, List<Integer> IdxList);
    List<Product> findAllByCtIdxAndApproveAndIsRecommendAndPcIdxInAndBrandIdxNotAndIdxNotInAndFeedTargetIn(Integer ctIdx, Boolean approve,Integer isRecommend, List<Integer> pcIdx, Integer brandIdx, List<Integer> IdxList, List<Integer> feedTargetList);
    List<Product> findAllByCtIdxAndCtSubIdxNotAndApproveAndIsRecommendAndPcIdxInAndBrandIdxNotAndIdxNotIn(Integer ctIdx, Integer ctSubIdx,Boolean approve,Integer isRecommend, List<Integer> pcIdx, Integer brandIdx, List<Integer> IdxList);
    Product findAllByIdxAndCtIdxIn(Integer idx, List<Integer> ctIdx);


}
