package com.pood.server.repository.meta;

import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.meta.PetDoctorFeedDescription;
import com.pood.server.entity.meta.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PetDoctorFeedDescriptionRepository extends JpaRepository<PetDoctorFeedDescription, Integer> {
    PetDoctorFeedDescription findByProductIdx(Integer productIdx);
    List<PetDoctorFeedDescription> findAllByProductIdxIn(List<Integer> productIdx);
}
