package com.pood.server.repository.meta;

import com.pood.server.entity.meta.PromotionImage;
import com.pood.server.entity.meta.PushAlarm;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PushAlarmRepository extends JpaRepository<PushAlarm, Long>{
}
