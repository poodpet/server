package com.pood.server.repository.meta;

import com.pood.server.entity.meta.Notice;
import com.pood.server.entity.meta.NoticeType;
import com.pood.server.repository.meta.customized.NoticeCustomRepository;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NoticeRepository extends JpaRepository<Notice, Integer>, NoticeCustomRepository {

    Optional<Notice> findTopByIsVisibleTrueAndNoticeTypeAndIsDeletedFalseOrderByIdxDesc(final
        NoticeType noticeType);

}
