package com.pood.server.repository.meta;

import com.pood.server.entity.meta.EventParticipation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventParticipationRepository extends JpaRepository<EventParticipation, Long> {

    int countByUserInfoIdxAndEventInfoIdx(int userInfoIdx, int eventInfoIdx);

    EventParticipation findByEventInfoIdxAndUserInfoIdx(Integer userInfoIdx, Integer eventInfoIdx);

}
