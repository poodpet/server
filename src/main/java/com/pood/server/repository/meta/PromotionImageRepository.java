package com.pood.server.repository.meta;

import com.pood.server.entity.meta.PromotionImage;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PromotionImageRepository extends JpaRepository<PromotionImage, Integer> {

    List<PromotionImage> findAllByPrIdxAndType(final Integer prIdx, final Integer type);

    List<PromotionImage> findAllByPrIdx(final int prIdx);

}
