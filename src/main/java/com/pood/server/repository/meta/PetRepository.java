package com.pood.server.repository.meta;

import com.pood.server.entity.meta.Pet;
import com.pood.server.repository.meta.customized.PetCustomRepository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetRepository extends JpaRepository<Pet, Integer>, PetCustomRepository {

    List<Pet> findAllByIdxIn(List<Integer> idxList);
}
