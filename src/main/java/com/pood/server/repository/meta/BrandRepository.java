package com.pood.server.repository.meta;

import com.pood.server.entity.meta.Brand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BrandRepository extends JpaRepository<Brand, Integer> {

}
