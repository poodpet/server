package com.pood.server.repository.meta;

import com.pood.server.entity.meta.DeliveryCourier;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DeliveryCourierRepository extends JpaRepository<DeliveryCourier, Integer> {

    List<DeliveryCourier> findAllByStartZipcodeLessThanEqualAndEndZipcodeGreaterThanEqual(
        final String startZipcode,
        final String endZipcode);

    @Query("SELECT dc.deliveryFee FROM DeliveryCourier dc where dc.startZipcode<=?1 AND dc.endZipcode>=?1 AND dc.areaType=?2")
    Optional<Integer> findDeliveryFeeByZipcodeAndType(final String zipcode, final int type);

}