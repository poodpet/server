package com.pood.server.repository.meta.customized;

import com.pood.server.entity.meta.QGoodsProduct;
import com.pood.server.entity.meta.mapper.goods.GoodsProductMapper;
import com.pood.server.entity.meta.mapper.goods.QGoodsProductMapper;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;

public class GoodsProductCustomRepositoryImpl implements GoodsProductCustomRepository{

    private final JPAQueryFactory queryFactory;

    public GoodsProductCustomRepositoryImpl(
        @Qualifier("metaQueryFactory") final JPAQueryFactory jpaQueryFactory) {
        this.queryFactory = jpaQueryFactory;
    }

    QGoodsProduct goodsProduct = QGoodsProduct.goodsProduct;

    @Override
    public List<GoodsProductMapper> findAllByGoodsIdxToMapper(final Integer goodsIdx) {
        return queryFactory.select(
                new QGoodsProductMapper(
                    goodsProduct.goodsIdx,
                    goodsProduct.productIdx
                )
            ).from(goodsProduct)
            .where(goodsProduct.goodsIdx.eq(goodsIdx))
            .fetch();
    }

}
