package com.pood.server.repository.meta;

import com.pood.server.entity.meta.BrandImage;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BrandImageRepository extends JpaRepository<BrandImage, Integer> {

    List<BrandImage> findAllByBrandIdx(final int brandIdx);

}
