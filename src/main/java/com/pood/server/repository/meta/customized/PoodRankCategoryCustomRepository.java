package com.pood.server.repository.meta.customized;

import com.pood.server.controller.response.home.rank.PoodRankCategoryDto;
import java.util.List;

public interface PoodRankCategoryCustomRepository {

    List<PoodRankCategoryDto> getCategoryList(final int petIdx);
}
