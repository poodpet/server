package com.pood.server.repository.meta;

import com.pood.server.entity.meta.GrainSize;
import java.util.Collection;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GrainSizeRepository extends JpaRepository<GrainSize, Integer> {

    List<GrainSize> findAllByIdxIn(final Collection<Integer> idxList);
}