package com.pood.server.repository.meta.customized;


import com.pood.server.controller.response.home.rank.PoodRankCategoryDto;
import com.pood.server.entity.meta.QGoodsCt;
import com.pood.server.entity.meta.QGoodsSubCt;
import com.pood.server.entity.meta.QPoodRankCategory;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.CaseBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;


public class PoodRankCategoryRepositoryImpl implements
    PoodRankCategoryCustomRepository {

    private final JPAQueryFactory queryFactory;

    public PoodRankCategoryRepositoryImpl(
        @Qualifier("metaQueryFactory") JPAQueryFactory queryFactory) {
        this.queryFactory = queryFactory;
    }

    QPoodRankCategory qPoodRankCategory = QPoodRankCategory.poodRankCategory;
    QGoodsCt qGoodsCt = QGoodsCt.goodsCt;
    QGoodsSubCt qGoodsSubCt = QGoodsSubCt.goodsSubCt;

    @Override
    public List<PoodRankCategoryDto> getCategoryList(final int petIdx) {
        return queryFactory.select(
                Projections.bean(PoodRankCategoryDto.class,
                    qPoodRankCategory.idx,
                    qGoodsSubCt.idx.as("goodsSubCtIdx"),
                    qPoodRankCategory.priority,
                    new CaseBuilder()
                        .when(qGoodsSubCt.name.eq("전체")).then(qGoodsCt.name)
                        .otherwise(qGoodsSubCt.name).as("name"),
                    qPoodRankCategory.recordbirth
                )).from(qPoodRankCategory)
            .innerJoin(qGoodsSubCt)
            .on(qPoodRankCategory.goodsSubCt.eq(qGoodsSubCt))
            .innerJoin(qGoodsCt)
            .on(qGoodsSubCt.goodsCt.eq(qGoodsCt))
            .where(qGoodsSubCt.petCategory.idx.eq(petIdx))
            .orderBy(qPoodRankCategory.priority.asc())
            .fetchResults().getResults();
    }
}

