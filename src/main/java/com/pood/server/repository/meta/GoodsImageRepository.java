package com.pood.server.repository.meta;

import com.pood.server.entity.meta.GoodsImage;
import com.pood.server.entity.meta.ProductImage;
import io.swagger.models.auth.In;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GoodsImageRepository extends JpaRepository<GoodsImage, Integer>  {
    List<GoodsImage> findAllByGoodsIdx(Integer idx);
}
