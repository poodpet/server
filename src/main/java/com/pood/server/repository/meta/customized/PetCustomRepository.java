package com.pood.server.repository.meta.customized;

import com.pood.server.entity.meta.Pet;
import java.util.List;

public interface PetCustomRepository {

    List<Pet> findAllOrderByPcKindDesc();

}
