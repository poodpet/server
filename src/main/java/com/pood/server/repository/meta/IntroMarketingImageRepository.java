package com.pood.server.repository.meta;

import com.pood.server.entity.meta.IntroMarketingImage;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IntroMarketingImageRepository extends JpaRepository<IntroMarketingImage, Long> {

    List<IntroMarketingImage> findAllByStartTimeBeforeAndEndTimeAfter(LocalDateTime startTime,
        LocalDateTime endTime);
}
