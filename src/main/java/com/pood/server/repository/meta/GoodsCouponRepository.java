package com.pood.server.repository.meta;

import com.pood.server.entity.meta.GoodsCoupon;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoodsCouponRepository extends JpaRepository<GoodsCoupon, Integer> {

    Optional<GoodsCoupon> findByCouponIdx(final Integer couponIdx);
}