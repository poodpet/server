package com.pood.server.repository.meta.customized;


import com.pood.server.dto.meta.product.ProductDto;
import com.pood.server.entity.meta.Product;
import com.pood.server.entity.meta.mapper.ProductIngredientMapper;
import com.pood.server.entity.meta.mapper.solution.ProductGoodsBaseDataMapper;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.ProductType;
import com.pood.server.util.FeedType;
import java.util.List;
import java.util.Optional;

public interface ProductCustomRepository {

    List<ProductDto.ProductDetail> getProductListByGoodsIdx(Integer goodsIdx);

    List<Product> getAnalysisSuppliesProductList(Integer ctIdx, Integer ctSubIdx, Boolean approve,
        Integer isRecommend, List<Integer> pcIdx, Integer brandIdx,
        List<Integer> discontinuedProductIdxList);

    List<Product> getAnalysisProductList(Integer ctIdx, Boolean approve, Integer isRecommend,
        List<Integer> pcIdx, Integer brandIdx, List<Integer> discontinuedProductIdxList);

    List<ProductDto.DiscontinuedProductList> getGoodsOfProductCountListAndPoodMarketNotIn();

    ProductBaseData getSolutionProductDetail(final int goodsIdx, final int productIdx);

    ProductBaseData getProductInfoByProductIdx(final int productIdx);

    List<ProductBaseData> getProductInfoListByGoodsIdx(final int goodsIdx);

    List<ProductBaseData> getAllProductsOnStock(final int pcIdx,
        final List<Integer> productIdxNotIn);

    List<ProductBaseData> getProductInfoListByWorryIdx(final int pcIdx, final String worryIdx);

    List<ProductBaseData> getProductInfoListByPcIdxAndProductType(
        final int pcIdx, final ProductType productType);

    List<ProductBaseData> getProductInfoListByFeedTarget(final int pcIdx, final int feedTarget);

    List<ProductBaseData> getProductInfoListByFeedType(final int pcIdx, final FeedType petFeedType);

    List<ProductBaseData> getProductInfoListByWorryIdxIn(final int pcIdx,
        final List<Long> worryIdxList);

    List<ProductBaseData> getProductInfoListByFeedTypeAndProductType(final int pcIdx,
        final FeedType petFeedType, final ProductType productType);

    List<ProductBaseData> getProductInfoListByFeedTargetAndProductTypeAndPoodMarketNotIn(
        final int pcIdx, final int feedTarget, final ProductType productType);

    List<ProductBaseData> getProductInfoListByBestWorryScoreFilter(final int pcIdx,
        final String worryIdx, final FeedType feedType);

    Optional<List<ProductGoodsBaseDataMapper>> getProductInfoListByBestWorryScoreFilterNotIn(final int pcIdx,
        final Integer ardCode, final FeedType feedType, final Long goodsCtIdx,
        final List<Long> goodsSubCtIdxList, final List<Integer> notInGoodsIdxList);

    List<ProductBaseData> getProductInfoListByFeedTypeAndFoodMartNotIn(final int pcIdx,
        final FeedType petFeedType);

    List<ProductBaseData> getProductInfoListByPcIdxAndProductTypeAndFoodMartNotIn(
        final int pcIdx, final ProductType productType);

    List<ProductBaseData> getProductInfoListByWorryIdxInAndFoodMartNotIn(final int pcIdx,
        final List<Long> worryIdxList);

    List<ProductIngredientMapper> findAllIngredientInIdxList(final List<Integer> productIdxList);

}
