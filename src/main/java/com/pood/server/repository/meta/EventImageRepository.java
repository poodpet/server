package com.pood.server.repository.meta;

import com.pood.server.entity.meta.EventImage;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventImageRepository extends JpaRepository<EventImage, Integer> {

    List<EventImage> findAllByEventIdxAndType(Integer eventIdx, Integer type);

    List<EventImage> findAllByEventIdx(Integer eventIdx);

    Optional<EventImage> findByEventIdxAndType(final int eventIdx, final int type);

}
