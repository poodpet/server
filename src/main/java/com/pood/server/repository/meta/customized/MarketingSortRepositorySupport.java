package com.pood.server.repository.meta.customized;

import com.pood.server.dto.meta.marketing.MarketingDataSet;
import com.pood.server.dto.meta.promotion.RunningPromotion;
import java.util.List;

public interface MarketingSortRepositorySupport {

    List<MarketingDataSet> findAllEvent(final int petCategoryIdx);

    List<MarketingDataSet> findAllPromotion(final int petCategoryIdx);

    List<RunningPromotion> runningPromotionAll(final int petCategoryIdx);
}
