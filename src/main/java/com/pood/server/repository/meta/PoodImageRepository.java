package com.pood.server.repository.meta;

import com.pood.server.entity.meta.PoodImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PoodImageRepository extends JpaRepository<PoodImage, Integer> {

}