package com.pood.server.repository.meta.customized;

import static com.pood.server.entity.meta.QEventImage.eventImage;
import static com.pood.server.entity.meta.QEventInfo.eventInfo;
import static com.pood.server.entity.meta.QMarketingSort.marketingSort;
import static com.pood.server.entity.meta.QPromotion.promotion;
import static com.pood.server.entity.meta.QPromotionImage.promotionImage;

import com.pood.server.dto.meta.marketing.MarketingDataSet;
import com.pood.server.dto.meta.promotion.RunningPromotion;
import com.pood.server.util.MarketingType;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;

public class MarketingSortRepositorySupportImpl implements MarketingSortRepositorySupport {

    private static final int MAIN_IMAGE = 0;
    private static final int COMMON = 0;
    private static final int RUNNING = 1;
    private final JPAQueryFactory jpaQueryFactory;

    public MarketingSortRepositorySupportImpl(@Qualifier("metaQueryFactory") final JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<MarketingDataSet> findAllEvent(final int petCategoryIdx) {
        LocalDateTime now = now();
        return jpaQueryFactory.select(
                Projections.constructor(MarketingDataSet.class,
                    marketingSort.marketingInfoIdx.as("marketingIdx"),
                    eventImage.url,
                    marketingSort.priority,
                    marketingSort.type
                )
            )
            .from(marketingSort)
            .innerJoin(eventInfo).on(marketingSort.marketingInfoIdx.eq(eventInfo.idx))
            .innerJoin(eventImage).on(eventImage.eventIdx.eq(eventInfo.idx))
            .where(marketingSort.type.in(MarketingType.EVENT, MarketingType.DONATION)
                .and(eventInfo.startDate.loe(now))
                .and(eventInfo.endDate.goe(now))
                .and(eventInfo.status.eq(RUNNING)))
            .where(eventImage.type.eq(MAIN_IMAGE))
            .where(marketingSort.petCategoryIdx.eq(COMMON)
                .or(marketingSort.petCategoryIdx.eq(petCategoryIdx)))
            .where(marketingSort.visible.eq(true))
            .where(eventInfo.isDeleted.eq(false))
            .fetch();
    }

    @Override
    public List<MarketingDataSet> findAllPromotion(final int petCategoryIdx) {
        LocalDateTime now = now();
        return jpaQueryFactory.select(
                Projections.constructor(MarketingDataSet.class,
                    marketingSort.marketingInfoIdx.as("marketingIdx"),
                    promotionImage.url,
                    marketingSort.priority,
                    marketingSort.type
                )
            )
            .from(marketingSort)
            .innerJoin(promotion).on(marketingSort.marketingInfoIdx.eq(promotion.idx))
            .innerJoin(promotionImage).on(promotionImage.prIdx.eq(promotion.idx))
            .where(marketingSort.type.eq(MarketingType.PROMOTION)
                .and(promotion.startPeriod.loe(now))
                .and(promotion.endPeriod.goe(now))
                .and(promotion.status.eq(RUNNING)))
            .where(promotionImage.type.eq(MAIN_IMAGE))
            .where(marketingSort.petCategoryIdx.eq(petCategoryIdx))
            .where(marketingSort.visible.eq(true))
            .fetch();
    }

    @Override
    public List<RunningPromotion> runningPromotionAll(final int petCategoryIdx) {
        LocalDateTime now = now();
        return jpaQueryFactory.select(
                Projections.constructor(RunningPromotion.class,
                    marketingSort.marketingInfoIdx.as("promotionIdx"),
                    promotionImage.url.as("imageUrl"),
                    marketingSort.priority,
                    promotion.innerTitle,
                    promotion.type,
                    promotion.startPeriod,
                    promotion.endPeriod,
                    promotion.details
                )
            )
            .from(marketingSort)
            .innerJoin(promotion).on(marketingSort.marketingInfoIdx.eq(promotion.idx))
            .innerJoin(promotionImage).on(promotionImage.prIdx.eq(promotion.idx))
            .where(marketingSort.type.eq(MarketingType.PROMOTION)
                .and(promotion.startPeriod.loe(now))
                .and(promotion.endPeriod.goe(now))
                .and(promotion.status.eq(RUNNING)))
            .where(promotionImage.type.eq(MAIN_IMAGE))
            .where(marketingSort.petCategoryIdx.eq(petCategoryIdx))
            .fetch();
    }

    private LocalDateTime now() {
        return LocalDateTime.now();
    }
}
