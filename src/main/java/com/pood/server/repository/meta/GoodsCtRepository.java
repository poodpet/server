package com.pood.server.repository.meta;

import com.pood.server.entity.meta.Goods;
import com.pood.server.entity.meta.GoodsCt;
import com.pood.server.entity.meta.GoodsImage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GoodsCtRepository extends JpaRepository<GoodsCt, Long>  {

}
