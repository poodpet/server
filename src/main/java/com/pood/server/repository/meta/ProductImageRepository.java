package com.pood.server.repository.meta;


import com.pood.server.entity.meta.ProductImage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductImageRepository extends JpaRepository<ProductImage, Integer>  {
    List<ProductImage> findAllByProductIdx(Integer idx);
}
