package com.pood.server.repository.meta.customized;


import com.pood.server.controller.response.home.delivery.PoodDeliveryCategoryDto;
import com.pood.server.entity.meta.QGoodsCt;
import com.pood.server.entity.meta.QGoodsSubCt;
import com.pood.server.entity.meta.QPoodDeliveryCategory;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.CaseBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;


public class PoodDeliveryCategoryRepositoryImpl implements
    PoodDeliveryCategoryCustomRepository {

    private final JPAQueryFactory queryFactory;

    public PoodDeliveryCategoryRepositoryImpl(
        @Qualifier("metaQueryFactory") final JPAQueryFactory queryFactory) {
        this.queryFactory = queryFactory;
    }
    
    QPoodDeliveryCategory qPoodDeliveryCategory = QPoodDeliveryCategory.poodDeliveryCategory;
    QGoodsCt qGoodsCt = QGoodsCt.goodsCt;
    QGoodsSubCt qGoodsSubCt = QGoodsSubCt.goodsSubCt;

    @Override
    public List<PoodDeliveryCategoryDto> getCategoryList(final int petIdx) {
        return queryFactory.select(
                Projections.bean(PoodDeliveryCategoryDto.class,
                    qPoodDeliveryCategory.idx,
                    qGoodsSubCt.idx.as("goodsSubCtIdx"),
                    qPoodDeliveryCategory.priority,
                    new CaseBuilder()
                        .when(qGoodsSubCt.name.eq("전체")).then(qGoodsCt.name)
                        .otherwise(qGoodsSubCt.name).as("name"),
                    qPoodDeliveryCategory.recordbirth
                )).from(qPoodDeliveryCategory)
            .innerJoin(qGoodsSubCt)
            .on(qPoodDeliveryCategory.goodsSubCt.eq(qGoodsSubCt))
            .innerJoin(qGoodsCt)
            .on(qGoodsSubCt.goodsCt.eq(qGoodsCt))
            .where(qGoodsSubCt.petCategory.idx.eq(petIdx))
            .orderBy(qPoodDeliveryCategory.priority.asc())
            .fetchResults().getResults();
    }

}

