package com.pood.server.repository.meta;

import com.pood.server.entity.meta.ShortcutIcon;
import java.util.Collection;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface ShortcutIconRepository extends JpaRepository<ShortcutIcon, Long> {

    List<ShortcutIcon> findAllByPetCategoryIdxInOrderByPriority(@Param("petCategoryIdx") final Collection<Integer> pcIdxList);
}