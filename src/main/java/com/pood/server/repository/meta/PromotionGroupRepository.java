package com.pood.server.repository.meta;

import com.pood.server.entity.meta.PromotionGroup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PromotionGroupRepository extends JpaRepository<PromotionGroup, Integer> {

    List<PromotionGroup> findByPrIdx(final Integer prIdx);

}
