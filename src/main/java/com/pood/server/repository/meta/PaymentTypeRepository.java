package com.pood.server.repository.meta;

import com.pood.server.entity.meta.PaymentType;
import com.pood.server.entity.meta.PromotionImage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PaymentTypeRepository extends JpaRepository<PaymentType, Integer> {



}
