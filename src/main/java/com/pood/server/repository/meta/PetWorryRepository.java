package com.pood.server.repository.meta;

import com.pood.server.entity.meta.PetWorry;
import com.pood.server.repository.meta.customized.PetWorryRepositorySupport;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetWorryRepository extends JpaRepository<PetWorry, Long>, PetWorryRepositorySupport {

    List<PetWorry> findAllByPetCategoryIdxInOrderByPriorityAsc(final List<Integer> petCategoryIdxList);

    List<PetWorry> findAllByIdIn(final List<Long> idList);

    Optional<List<PetWorry>> findAllByAiRecommendDiagnosisIdxIn(final List<Integer> diagnosisIdx);
}