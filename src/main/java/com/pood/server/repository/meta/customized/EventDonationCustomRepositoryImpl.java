package com.pood.server.repository.meta.customized;

import com.pood.server.entity.meta.QEventDonation;
import com.pood.server.entity.meta.QEventImage;
import com.pood.server.entity.meta.QEventInfo;
import com.pood.server.entity.meta.QMarketingSort;
import com.pood.server.entity.meta.mapper.event.EventDonationThumbnailMapper;
import com.pood.server.entity.meta.mapper.event.QEventDonationThumbnailMapper;
import com.pood.server.util.MarketingType;
import com.querydsl.core.types.ConstantImpl;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;


public class EventDonationCustomRepositoryImpl implements EventDonationCustomRepository {

    private final JPAQueryFactory queryFactory;

    public static final int DONATION = 26;
    public static final int EVENT_PAUSED = 3;
    public static final int EVENT_READY = 0;

    private static final QEventDonation eventDonation = QEventDonation.eventDonation;
    private static final QEventInfo eventInfo = QEventInfo.eventInfo;
    private static final QMarketingSort marketingSort = QMarketingSort.marketingSort;
    private static final QEventImage eventImage = QEventImage.eventImage;

    public EventDonationCustomRepositoryImpl(@Qualifier("metaQueryFactory") final JPAQueryFactory queryFactory) {
        this.queryFactory = queryFactory;
    }

    @Override
    public boolean existsByUserUuidAtToday(final String UserUuid) {

        final LocalDate now = LocalDate.now();

        return !queryFactory
            .selectFrom(eventDonation)
            .where(eventDonation.userUuid.eq(UserUuid)
                .and(Expressions.dateTemplate(String.class, "DATE_FORMAT({0}, {1})",
                    eventDonation.recordBirth,
                    ConstantImpl.create("%Y-%m-%d")).eq(now.toString()))
            )
            .fetchResults().isEmpty();
    }

    @Override
    public List<EventDonationThumbnailMapper> findThumbnailAll() {
        return queryFactory
            .select(
                new QEventDonationThumbnailMapper(
                    eventInfo.idx,
                    eventImage.url
                )
            )
            .from(eventInfo)
            .innerJoin(marketingSort).on(eventInfo.idx.eq(marketingSort.marketingInfoIdx))
            .innerJoin(eventImage)
            .on(eventImage.eventIdx.eq(eventInfo.idx).and(eventImage.type.eq(0)))
            .where(eventInfo.eventTypeIdx.eq(DONATION)
                .and(marketingSort.type.eq(MarketingType.DONATION))
                .and(isNotPaused())
                .and(isNotReady())
                .and(isNotDelete())
            )
            .groupBy(eventInfo.idx)
            .orderBy(eventInfo.idx.desc())
            .fetch();
    }

    private BooleanExpression isNotPaused() {
        return eventInfo.status.ne(EVENT_PAUSED);
    }

    private BooleanExpression isNotReady() {
        return eventInfo.status.ne(EVENT_READY);
    }

    private BooleanExpression isNotDelete() {
        return eventInfo.isDeleted.eq(false);
    }

}
