package com.pood.server.repository.meta;

import com.pood.server.entity.meta.AllergyData;
import java.util.Collection;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AllergyDataRepository extends JpaRepository<AllergyData, Integer> {

    List<AllergyData> findAllByIdxIn(final Collection<Integer> allergyList);

    List<AllergyData> findAllByVisibleNotOrderByPriorityAsc(final boolean visible);
}
