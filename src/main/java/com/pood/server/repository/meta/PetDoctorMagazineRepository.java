package com.pood.server.repository.meta;

import com.pood.server.entity.meta.PetDoctorMagazine;
import com.pood.server.repository.meta.customized.PetDoctorMagazineCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetDoctorMagazineRepository extends JpaRepository<PetDoctorMagazine, Long>,
    PetDoctorMagazineCustomRepository {

}
