package com.pood.server.repository.meta;

import com.pood.server.entity.meta.CouponCode;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CouponCodeRepository extends JpaRepository<CouponCode, Integer> {

    Optional<CouponCode> findByCode(final String code);
}