package com.pood.server.repository.meta.customized;

import com.pood.server.entity.meta.mapper.notice.NoticeDetailMapper;
import com.pood.server.entity.meta.mapper.notice.NoticePageMapper;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface NoticeCustomRepository {

    Page<NoticePageMapper> getNoticePage(final Pageable pageable);

    Optional<NoticeDetailMapper> getNoticeDetail(final int idx);

}
