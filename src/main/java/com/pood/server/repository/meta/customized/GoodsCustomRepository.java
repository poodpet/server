package com.pood.server.repository.meta.customized;


import com.pood.server.dto.meta.goods.GoodsDto;
import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.entity.meta.GoodsSubCt;
import com.pood.server.entity.meta.mapper.OrderGoodsMapper;
import com.pood.server.entity.meta.mapper.SuggestRandomGoodsMapper;
import com.pood.server.entity.meta.mapper.home.CustomRankInfoMapper;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GoodsCustomRepository {

    Page<GoodsDto.SortedGoodsList> getSortedGoodsList(Pageable pageable, Long goodSubCtIdx,
        String fieldKey, String fieldValue, String searchText, Integer pcIdx, Long goodsCtIdx);

    Page<GoodsDto.SortedGoodsList> goodsSearchList(final Pageable pageable, final String searchText,
        final Integer pcIdx);

    Page<GoodsDto.SortedGoodsList> goodsSearchList(final Pageable pageable, final String searchText,
        final Integer pcIdx, final Long goodsCtIdx);

    Page<GoodsDto.SortedGoodsList> goodsFilteredList(final Pageable pageable,
        final List<String> petWorryList, final Long goodsCtIdx, final Long goodsSubCtIdx
        , final List<String> proteinList, List<Integer> grainSizeList,
        final List<Integer> feedTargetList,
        final int pcIdx, final List<Integer> ageTypeList);

    List<GoodsDto.SortedGoodsList> getSortedGoodsInIdxList(final List<Integer> goodsIds);

    List<GoodsDto.SortedGoodsList> getrRcommendGoodsList(final int productIdx, final int pcIdx);

    List<GoodsDto.SortedGoodsList> getrRcommendGoodsListByProductIdxAndNotGoodsIdxList(
        final int pcIdx, final Integer productIdx, final List<Integer> goodsIdxList);

    Optional<GoodsDto.GoodsDetail> findByGoodsId(Integer idx);

    List<GoodsDto.SortedGoodsList> getMyOrderGoodsList(List<Integer> goodsIds);

    Page<GoodsDto.SortedGoodsList> getPoodHomeDeliveryGoodsList(final GoodsSubCt goodsSubCt,
        final Integer pcIdx, final Pageable pageable);

    List<SortedGoodsList> poodHomeSetGoodsList(final int petCategoryIdx);

    List<SortedGoodsList> getPoodHomeNewGoodsList(final int petCategoryIdx, final int saleStatus);

    List<SortedGoodsList> poodMarketGoodsList(final int petCategoryIdx);

    List<SortedGoodsList> poodMatchGoodsList(final int petCategoryIdx,
        final List<String> searchList);

    List<Integer> getRankGoodsIdxList(final int pcIdx);

    List<Integer> getRankGoodsIdxList(final GoodsSubCt goodsSubCt, final int pcIdx);

    boolean existsByPcIdxAndGoodsNameLikeInSearchList(final int pctIdx,
        final List<String> searchList);

    List<SuggestRandomGoodsMapper> suggestRandom10Goods();

    List<OrderGoodsMapper> myOrderGoodsInfoListByBasket(final List<Integer> goodsIds);

    List<SortedGoodsList> getSortedGoodsByProductIdxsOrderReviewCount(
        final List<Integer> productIdxList);

    Page<SortedGoodsList> getNewstGoodsList(final int pcIdx, final Pageable pageable);

    List<GoodsDto.SortedGoodsList> getRecommendGoodsListByIsSale(final int productIdx, final int pcIdx);

    List<CustomRankInfoMapper> getCustomRankListNotIn(final List<Integer> goodsIdsList, final int pcIdx);

    List<String> findGoodsNameBySearchText(final Integer pcIdx, final String goodsName);

    List<String> findGoodsNameBySearchText(final Integer pcIdx, final String goodsName,
        final Long goodsCt);

    List<String> findGoodsNameBySearchTextByFeedComparison(final Integer pcIdx,
        final String goodsName);

    Page<SortedGoodsList> goodsSearchListByFeedComparison(final Pageable pageable,
        final String searchText, final Integer pcIdx);

}
