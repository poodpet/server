package com.pood.server.repository.log;

import com.pood.server.entity.log.LogUserJoin;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogUserJoinRepository extends JpaRepository<LogUserJoin, Integer> {

    Optional<LogUserJoin> findBySnsKey(final String snsKey);
}