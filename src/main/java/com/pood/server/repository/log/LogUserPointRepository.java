package com.pood.server.repository.log;

import com.pood.server.entity.log.LogUserPoint;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogUserPointRepository extends JpaRepository<LogUserPoint, Integer> {

}
