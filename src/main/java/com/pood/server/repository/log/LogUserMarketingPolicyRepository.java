package com.pood.server.repository.log;

import com.pood.server.entity.log.LogUserMarketingPolicy;
import org.springframework.data.repository.CrudRepository;

public interface LogUserMarketingPolicyRepository extends CrudRepository<LogUserMarketingPolicy, Integer> {

}