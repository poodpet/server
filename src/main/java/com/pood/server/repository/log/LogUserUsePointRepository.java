package com.pood.server.repository.log;

import com.pood.server.entity.log.LogUserUsePoint;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface LogUserUsePointRepository extends JpaRepository<LogUserUsePoint, Integer> {
    List<LogUserUsePoint> findAllByUserUuid(String uuid);
}
