package com.pood.server.repository.log;

import com.pood.server.entity.log.LogUserLogin;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogUserLoginRepository extends JpaRepository<LogUserLogin, Integer> {

    Optional<LogUserLogin> findTopByUserUuidAndLoginOutOrderByIdxDesc(final String userUuid,
        final Integer loginOut);

    Optional<LogUserLogin> findTopByUserUuidOrderByIdxDesc(final String userUuid);
}