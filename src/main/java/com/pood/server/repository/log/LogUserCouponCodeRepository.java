package com.pood.server.repository.log;

import com.pood.server.entity.log.LogUserCouponCode;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogUserCouponCodeRepository extends JpaRepository<LogUserCouponCode, Integer> {

    boolean existsByUserUuidAndCode(final String userUuid, final String code);
}