package com.pood.server.repository.log;

import com.pood.server.entity.log.LogUserToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogUserTokenRepository extends JpaRepository<LogUserToken, Integer> {

}