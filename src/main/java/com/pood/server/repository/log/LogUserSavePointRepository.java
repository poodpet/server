package com.pood.server.repository.log;

import com.pood.server.entity.log.LogUserSavePoint;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogUserSavePointRepository extends JpaRepository<LogUserSavePoint, Integer> {

    List<LogUserSavePoint> findAllByUserUuidAndType(String uuid, final int type);
}
