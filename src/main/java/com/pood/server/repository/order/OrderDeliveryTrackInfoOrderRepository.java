package com.pood.server.repository.order;

import com.pood.server.entity.order.OrderDeliveryTrackInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderDeliveryTrackInfoOrderRepository extends JpaRepository<OrderDeliveryTrackInfo, Integer>{
    List<OrderDeliveryTrackInfo> findByOrderNumberIn (List<String> orderNumber);
}
