package com.pood.server.repository.order;

import com.pood.server.entity.order.OrderBasket;
import com.pood.server.repository.order.querydsl.OrderBasketRepositorySupport;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderBasketRepository extends JpaRepository<OrderBasket, Integer>, OrderBasketRepositorySupport {

    List<OrderBasket> findAllByOrderIdxIn(List<Integer> idxList);

    int countOrderBasketByOrderNumber(String orderNumber);

    List<OrderBasket> findAllByOrderNumber(String orderNumber);

}
