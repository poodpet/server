package com.pood.server.repository.order.customized;

import com.pood.server.entity.order.mapper.OrderPointMapper;
import java.util.List;

public interface OrderPointCustomRepository {
    List<OrderPointMapper> findAllByOrderNumberAndUsed(final String orderNumber);

}
