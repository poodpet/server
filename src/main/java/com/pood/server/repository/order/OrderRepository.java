package com.pood.server.repository.order;

import com.pood.server.entity.order.Order;
import com.pood.server.repository.order.customized.OrderCustomRepository;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface OrderRepository extends JpaRepository<Order, Integer>, OrderCustomRepository {

    Page<Order> findAllByUserIdxAndOrderStatusNot(final Integer userIdx, final Integer orderStatus,
        final Pageable pageable);

    boolean existsByUserIdxAndOrderStatus(final int userIdx, final int orderStatus);

    @Query("SELECT o.orderPrice FROM Order o where o.orderNumber=?1")
    Integer getOrderPriceByOrderNumber(final String orderNumber);

    Optional<Order> findByOrderNumber(final String orderNumber);


    Optional<Order> findByOrderNumberAndUserUuid(final String orderNumber, final String userUUID);
}
