package com.pood.server.repository.order;


import com.pood.server.entity.order.OrderCancel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;


public interface OrderCancelRepository extends JpaRepository<OrderCancel, Integer> {

    List<OrderCancel> findAllByOrderNumberAndGoodsIdxOrderByRecordbirthDesc(final String OrderNumber,
        Integer goodsIdx);

    int countOrderCancelByOrderNumber(final String orderNumber);

    List<OrderCancel> findByOrderNumberAndType(final String orderNumber, final int type);

}
