package com.pood.server.repository.order.customized;

import com.pood.server.entity.meta.dto.home.OrderRankDto;
import com.pood.server.entity.meta.mapper.order.QRefundDeliveryMapper;
import com.pood.server.entity.meta.mapper.order.RefundDeliveryMapper;
import com.pood.server.entity.order.QOrder;
import com.pood.server.entity.order.QOrderBasket;
import com.pood.server.entity.order.QOrderDelivery;
import com.pood.server.entity.order.QOrderRefund;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public class OrderRepositoryImpl implements
    OrderCustomRepository {

    private static final int DELIVERY_READY = 10;
    private static final int COMPLETE_PAYMENT = 1;

    private final JPAQueryFactory queryFactory;

    private final QOrder qOrder = QOrder.order;
    private final QOrderBasket qOrderBasket = QOrderBasket.orderBasket;
    private final QOrderRefund qOrderRefund = QOrderRefund.orderRefund;
    private final QOrderDelivery qOrderDelivery = QOrderDelivery.orderDelivery;

    public OrderRepositoryImpl(@Qualifier("orderQueryFactory") final JPAQueryFactory queryFactory) {
        this.queryFactory = queryFactory;
    }

    @Override
    public Page<OrderRankDto> findRankGoodsByInGoodsIdx(final List<Integer> goodsIdxList,
        final String searchType, final Pageable pageable) {

        QueryResults<OrderRankDto> sortedGoodsListQueryResults = queryFactory.select(
                Projections.constructor(OrderRankDto.class,
                    qOrderBasket.goodsIdx.as("goodsIdx"),
                    qOrderBasket.goodsIdx.count().as("rank")
                )).from(qOrder)
            .innerJoin(qOrderBasket)
            .on(qOrder.orderNumber.eq(qOrderBasket.orderNumber))
            .where(qOrder.orderStatus.goe(COMPLETE_PAYMENT)
                .and(qOrderBasket.goodsIdx.in(goodsIdxList)))
            .where(periodBetween(searchType))
            .groupBy(qOrderBasket.goodsIdx)
            .orderBy(qOrderBasket.goodsIdx.count().desc())
            .offset(pageable.getOffset())
            .limit(pageable.getPageSize())
            .fetchResults();

        long count = sortedGoodsListQueryResults.getTotal() > 100 ? 100
            : sortedGoodsListQueryResults.getTotal();
        return new PageImpl<>(sortedGoodsListQueryResults.getResults(), pageable, count);
    }

    @Override
    public Page<OrderRankDto> findUserBuyGoodsListByBuyCount(final int buyCount, final int userIdx,
        final List<Integer> goodsIdxList, final Pageable pageable) {

        List<OrderRankDto> result = queryFactory.select(
                Projections.constructor(OrderRankDto.class,
                    qOrderBasket.goodsIdx.as("goodsIdx"),
                    qOrderBasket.goodsIdx.count().as("rank")
                )).from(qOrderBasket)
            .innerJoin(qOrder)
            .on(qOrder.orderNumber.eq(qOrderBasket.orderNumber))
            .where(qOrder.orderStatus.goe(DELIVERY_READY)
                .and(qOrder.userIdx.eq(userIdx))
                .and(qOrderBasket.goodsIdx.in(goodsIdxList)))
            .orderBy(qOrderBasket.goodsIdx.count().desc())
            .groupBy(qOrderBasket.goodsIdx)
            .having(qOrderBasket.goodsIdx.count().goe(buyCount))
            .offset(pageable.getOffset())
            .limit(pageable.getPageSize())
            .fetch();

        return new PageImpl<>(result, pageable,
            countUserBuyGoodsListByBuyCount(buyCount, userIdx, goodsIdxList));
    }

    @Override
    public RefundDeliveryMapper getRefundDeliveryInfo(final int refundIdx, final int userIdx) {
        return queryFactory.select(new QRefundDeliveryMapper(qOrderRefund.idx,
                qOrderRefund.uuid,
                qOrderRefund.orderNumber,
                qOrderRefund.goodsIdx,
                qOrderRefund.qty,
                qOrderDelivery.nickname,
                qOrderDelivery.zipcode,
                qOrderDelivery.receiver,
                qOrderDelivery.address,
                qOrderDelivery.addressDetail,
                qOrderDelivery.phoneNumber
            ))
            .from(qOrder)
            .innerJoin(qOrderRefund).on(qOrder.orderNumber.eq(qOrderRefund.orderNumber))
            .innerJoin(qOrderDelivery).on(qOrderDelivery.refundNumber.eq(qOrderRefund.uuid))
            .where(qOrderRefund.idx.eq(refundIdx)
                .and(qOrder.userIdx.eq(userIdx))).fetchOne();
    }

    private long countUserBuyGoodsListByBuyCount(final int buyCount, final int userIdx,
        List<Integer> goodsIdxList) {

        return queryFactory.select(
                Projections.constructor(OrderRankDto.class,
                    qOrderBasket.goodsIdx.as("goodsIdx"),
                    qOrderBasket.goodsIdx.count().as("rank")
                )).from(qOrderBasket)
            .innerJoin(qOrder)
            .on(qOrder.orderNumber.eq(qOrderBasket.orderNumber))
            .where(qOrder.orderStatus.goe(DELIVERY_READY)
                .and(qOrder.userIdx.eq(userIdx))
                .and(qOrderBasket.goodsIdx.in(goodsIdxList)))

            .orderBy(qOrderBasket.goodsIdx.count().desc())
            .groupBy(qOrderBasket.goodsIdx)
            .having(qOrderBasket.goodsIdx.count().goe(buyCount))
            .fetch().size();
    }

    private Predicate periodBetween(final String searchType) {
        LocalDateTime yesterDay = LocalDateTime.now().minusDays(1L).withHour(23).withMinute(59)
            .withSecond(59);
        if (searchType.equals("DAY")) {
            return qOrder.recordbirth.between(yesterDay.minusDays(1), yesterDay);
        }
        if (searchType.equals("WEEK")) {
            return qOrder.recordbirth.between(yesterDay.minusDays(7), yesterDay);
        }
        if (searchType.equals("MONTH")) {
            return qOrder.recordbirth.between(yesterDay.minusDays(30), yesterDay);
        }
        return null;
    }

}