package com.pood.server.repository.order;

import com.pood.server.entity.order.OrderPoint;
import com.pood.server.repository.order.customized.OrderPointCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderPointRepository extends JpaRepository<OrderPoint, Integer>,
    OrderPointCustomRepository {
}
