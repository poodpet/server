package com.pood.server.repository.order.customized;

import com.pood.server.entity.meta.dto.home.OrderRankDto;
import com.pood.server.entity.meta.mapper.order.RefundDeliveryMapper;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface OrderCustomRepository {

    Page<OrderRankDto> findRankGoodsByInGoodsIdx(final List<Integer> goodsIdxList,
        final String searchType, final Pageable pageable);

    Page<OrderRankDto> findUserBuyGoodsListByBuyCount(final int buyCount, final int userIdx,
        final List<Integer> goodsIdxList, final Pageable pageable);

    RefundDeliveryMapper getRefundDeliveryInfo(final int refundUuid, final int userIdx);
}
