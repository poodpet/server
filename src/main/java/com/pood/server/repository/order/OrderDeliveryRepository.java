package com.pood.server.repository.order;

import com.pood.server.entity.order.OrderDelivery;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderDeliveryRepository extends JpaRepository<OrderDelivery, Integer>{
    List<OrderDelivery> findAllByOrderIdxInAndDeliveryType(List<Integer> orderIdxList, Integer deliveryType);

    Optional<OrderDelivery> findByOrderNumberAndDeliveryType(final String orderNumber, final int deliveryType);

}
