package com.pood.server.repository.order;


import com.pood.server.entity.order.OrderCancel;
import com.pood.server.entity.order.OrderExchange;
import com.pood.server.entity.order.OrderRefund;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface OrderRefundRepository extends JpaRepository<OrderRefund, Integer>{

    List<OrderRefund> findAllByOrderNumberAndGoodsIdxOrderByRecordbirthDesc(String OrderNumber, Integer goodsIdx);
}
