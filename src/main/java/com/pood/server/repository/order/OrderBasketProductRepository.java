package com.pood.server.repository.order;

import com.pood.server.entity.meta.Promotion;
import com.pood.server.entity.order.OrderBasketProduct;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderBasketProductRepository extends JpaRepository<OrderBasketProduct, Long>{
    
}
