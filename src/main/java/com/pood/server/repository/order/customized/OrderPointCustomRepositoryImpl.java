package com.pood.server.repository.order.customized;

import com.pood.server.entity.order.QOrderPoint;
import com.pood.server.entity.order.mapper.OrderPointMapper;
import com.pood.server.entity.order.mapper.QOrderPointMapper;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;

public class OrderPointCustomRepositoryImpl implements OrderPointCustomRepository {

    private static final int USE_POINT_TYPE = 0;
    private static final QOrderPoint qOrderPoint = QOrderPoint.orderPoint;

    private final JPAQueryFactory queryFactory;

    public OrderPointCustomRepositoryImpl(
        @Qualifier("orderQueryFactory") final JPAQueryFactory queryFactory) {
        this.queryFactory = queryFactory;
    }

    public List<OrderPointMapper> findAllByOrderNumberAndUsed(final String orderNumber) {
        return queryFactory.select(new QOrderPointMapper(
                qOrderPoint.idx,
                qOrderPoint.orderNumber,
                qOrderPoint.pointUuid,
                qOrderPoint.usedPoint,
                qOrderPoint.type
            ))
            .from(qOrderPoint)
            .where(
                qOrderPoint.orderNumber.eq(orderNumber),
                qOrderPoint.type.eq(USE_POINT_TYPE))
            .fetch();
    }

}
