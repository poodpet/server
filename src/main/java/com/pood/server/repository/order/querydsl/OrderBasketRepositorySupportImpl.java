package com.pood.server.repository.order.querydsl;

import static com.pood.server.entity.order.QOrder.order;
import static com.pood.server.entity.order.QOrderCancel.orderCancel;
import static com.pood.server.entity.order.QOrderBasket.orderBasket;
import static com.pood.server.util.OrderType.DELIVERY_COMPLETE;
import static com.pood.server.entity.order.QOrderBasket.orderBasket;
import static com.pood.server.util.OrderType.ORDER_CANCEL_APPLY;
import static com.pood.server.util.OrderType.ORDER_CANCEL_COMPLETE;
import static com.pood.server.util.OrderType.ORDER_READY;
import static com.pood.server.util.OrderType.RETURN_APPROVE;

import com.pood.server.dto.order.OrderBasketQuantity;
import com.pood.server.entity.order.mapper.OrderBasketMapper;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;

public class OrderBasketRepositorySupportImpl implements OrderBasketRepositorySupport {

    private final JPAQueryFactory jpaQueryFactory;

    public OrderBasketRepositorySupportImpl(
        @Qualifier("orderQueryFactory") final JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public List<OrderBasketQuantity> getBasketJoinOrder(final int userIdx, final int goodsIdx) {
        return jpaQueryFactory.select(
                Projections.constructor(
                    OrderBasketQuantity.class,
                    orderBasket.goodsIdx,
                    orderBasket.quantity.sum()))
            .from(orderBasket)
            .innerJoin(order).on(orderBasket.orderIdx.eq(order.idx))
            .where(order.userIdx.eq(userIdx))
            .where(orderBasket.goodsIdx.eq(goodsIdx))
            .where(notInStatus())
            .groupBy(orderBasket.goodsIdx)
            .fetch();
    }

    @Override
    public boolean isBuyAvailable(final int userIdx, final int goodsIdx) {
        final long boughtHistory = jpaQueryFactory
            .from(orderBasket)
            .innerJoin(order).on(orderBasket.orderIdx.eq(order.idx))
            .where(order.userIdx.eq(userIdx))
            .where(orderBasket.goodsIdx.eq(goodsIdx))
            .where(notInStatus())
            .fetchCount();

        return boughtHistory <= 0;
    }

    @Override
    public List<OrderBasketMapper> getUserOrderBasket(int userIdx) {
        return jpaQueryFactory.select(
                Projections.constructor(
                    OrderBasketMapper.class,
                    orderBasket.idx,
                    orderBasket.goodsIdx,
                    orderBasket.orderNumber,
                    orderBasket.status))
            .from(orderBasket)
            .innerJoin(order).on(orderBasket.orderIdx.eq(order.idx))
            .leftJoin(orderCancel)
            .on(orderCancel.orderNumber.eq(order.orderNumber).and(orderCancel.goodsIdx.eq(orderBasket.goodsIdx)))
            .where(order.userIdx.eq(userIdx)
                .and(order.orderStatus.eq(DELIVERY_COMPLETE.getValue()))
                .and(orderCancel.idx.isNull()))
            .fetch();
    }

    private BooleanExpression notInStatus() {
        return order.orderStatus.notIn(
            ORDER_READY.getValue(),
            ORDER_CANCEL_APPLY.getValue(),
            ORDER_CANCEL_COMPLETE.getValue(),
            RETURN_APPROVE.getValue()
        );
    }

}
