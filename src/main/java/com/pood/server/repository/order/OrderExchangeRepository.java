package com.pood.server.repository.order;


import com.pood.server.entity.order.OrderCancel;
import com.pood.server.entity.order.OrderExchange;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface OrderExchangeRepository extends JpaRepository<OrderExchange, Integer>{
    List<OrderExchange> findAllByOrderNumberAndGoodsIdxOrderByRecordbirthDesc(String OrderNumber, Integer goodsIdx);
}
