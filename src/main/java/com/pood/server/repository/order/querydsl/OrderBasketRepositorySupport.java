package com.pood.server.repository.order.querydsl;

import com.pood.server.dto.order.OrderBasketQuantity;
import com.pood.server.entity.order.mapper.OrderBasketMapper;
import java.util.List;

public interface OrderBasketRepositorySupport {

    List<OrderBasketQuantity> getBasketJoinOrder(final int userIdx, final int goodsIdx);

    boolean isBuyAvailable(final int userIdx, final int goodsIdx);

    List<OrderBasketMapper> getUserOrderBasket(final int userIdx);
}
