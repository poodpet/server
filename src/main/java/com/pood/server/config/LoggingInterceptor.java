package com.pood.server.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.util.ContentCachingRequestWrapper;

@Slf4j
@RequiredArgsConstructor
public class LoggingInterceptor implements HandlerInterceptor {

    private static final String JSON_TYPE = "application/json";
    private final ObjectMapper objectMapper;

    @Override
    public void afterCompletion(final HttpServletRequest request,
        final HttpServletResponse response, final Object handler,
        final Exception ex) throws Exception {
        final String method = request.getMethod();
        if (request.getRequestURI().equals("/")) {
            return;
        }
        log.info("Method = {}, URL = {}", method, request.getRequestURI());

        final String header = request.getHeader("content-type");

        if (!RequestMethod.GET.name().equals(method) && JSON_TYPE.equals(header)) {

            log.info("Request = {}", objectMapper.readTree(
                ((ContentCachingRequestWrapper) request).getContentAsByteArray()));
        }
    }
}
