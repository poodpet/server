package com.pood.server.config;

public class DATABASE {

    //ID:pood, PWD : vnemelql2021!@

    /********** 데이터베이스 이름 *************/
    private static final String DB_META = "meta_db";
    private static final String DB_USER = "user_db";
    private static final String DB_ORDER = "order_db";
    private static final String DB_LOG = "log_db";

    /************** 로그 테이블 : 테이블 이름  ***************/
    public static String TABLE_ERROR_ORDER_FAIL = "error_order_fail";
    public static String TABLE_ERROR_PROC = "error_proc";
    public static String TABLE_ERROR_API = "error_api";

    public static String TABLE_LOG_USER_NOTICE_ALARAM = "log_user_notice_alaram";
    public static String TABLE_LOG_USER_ORDER = "log_user_order";
    public static String TABLE_LOG_PRODUCT_OUT_OF_STOCK = "log_product_out_of_stock";
    public static String TABLE_LOG_USER_COUPON_CODE = "log_user_coupon_code";
    public static String TABLE_LOG_USER_ORDER_BASKET = "log_user_order_basket";
    public static String TABLE_LOG_USER_JOIN = "log_user_join";
    public static String TABLE_LOG_USER_LOGIN = "log_user_login";
    public static String TABLE_LOG_USER_TOKEN = "log_user_token";
    public static String TABLE_LOG_USER_POINT = "log_user_point";
    public static String TABLE_LOG_USER_POLICY = "log_user_marketing_policy";
    public static String TABLE_LOG_USER_SAVE_POINT = "log_user_save_point";
    public static String TABLE_LOG_USER_USE_POINT = "log_user_use_point";
    public static String TABLE_LOG_PRODUCT_HISTORY = "log_product_history";


    /********** 메타 테이블 : 테이블 이름  ***************/
    public static String TABLE_AAFCO_NRC = "aafco_nrc";
    public static String TABLE_AI_RECOMMEND_DIAGNOSIS = "ai_recommend_diagnosis";
    public static String TABLE_ALLERGY_DATA = "allergy_data";
    public static String TABLE_ATTRIBUTABLE = "attributable";
    public static String TABLE_BANK_CODE_STD = "bank_code_std";
    public static String TABLE_BRAND = "brand";
    public static String TABLE_BRAND_IMAGE = "brand_image";
    public static String TABLE_BANNER = "banner";
    public static String TABLE_BANNER_IMAGE = "banner_image";
    public static String TABLE_CATEGORY = "category";
    public static String TABLE_CATEGORY_TOP = "category_top";
    public static String TABLE_CATEGORY_IMAGE = "category_image";
    public static String TABLE_CATEGORY_FIELD = "category_field";
    public static String TABLE_COURIER = "courier";
    public static String TABLE_COUPON = "coupon";
    public static String TABLE_COUPON_BRAND = "coupon_brand";
    public static String TABLE_COUPON_GOODS = "coupon_goods";
    public static String TABLE_COUPON_CODE = "coupon_code";
    public static String TABLE_COUPON_PUBLISH_TYPE = "coupon_publish_type";
    public static String TABLE_DELIVERY_COURIER = "delivery_courier";
    public static String TABLE_EVENT_IMAGE = "event_image";
    public static String TABLE_EVENT_INFO = "event_info";
    public static String TABLE_EVENT_GOODS_TYPE = "event_goods_type";
    public static String TABLE_EVENT_GOODS_LIST = "event_goods_list";
    public static String TABLE_EVENT_TYPE = "event_type";
    public static String TABLE_FEED = "feed";
    public static String TABLE_FEED_DM = "feed_dm";
    public static String TABLE_FEED_OTHER_PRODUCT = "feed_other_product";
    public static String TABLE_FEED_OTHER_PRODUCT_IMAGE = "feed_other_product_image";
    public static String TABLE_GOODS = "goods";
    public static String TABLE_GOODS_IMAGE = "goods_image";
    public static String TABLE_GOODS_PRODUCT = "goods_product";
    public static String TABLE_GOODS_COUPON = "goods_coupon";
    public static String TABLE_GOODS_TYPE = "goods_type";
    public static String TABLE_GRAIN_SIZE = "grain_size";
    public static String TABLE_NOTICE = "notice";
    public static String TABLE_NOTICE_IMAGE = "notice_image";
    public static String TABLE_NUTI = "nuti";
    public static String TABLE_ORDER_TYPE = "order_type";
    public static String TABLE_PAYMENT_REQUEST_TEXT = "payment_request_text";
    public static String TABLE_PAYMENT_TYPE = "payment_type";
    public static String TABLE_PSK = "popular_search_keyword";
    public static String TABLE_PET = "pet";
    public static String TABLE_PET_IMAGE = "pet_image";
    public static String TABLE_PET_CATEGORY = "pet_category";
    public static String TABLE_PET_DOCTOR = "pet_doctor";
    public static String TABLE_PET_DOCTOR_FEED_DESC = "pet_doctor_feed_description";
    public static String TABLE_PET_DOCTOR_IMAGE = "pet_doctor_image";
    public static String TABLE_PET_DISEASE = "pet_disease";
    public static String TABLE_PET_DISEASE_GROUP = "pet_disease_group";
    public static String TABLE_POINT = "point";
    public static String TABLE_POOD_IMAGE = "pood_image";
    public static String TABLE_PRODUCT = "product";
    public static String TABLE_PRODUCT_CT = "product_ct";
    public static String TABLE_PRODUCT_SUB_CT = "product_sub_ct";
    public static String TABLE_PRODUCT_IMAGE = "product_image";
    public static String TABLE_PROMOTION = "promotion";
    public static String TABLE_PROMOTION_GROUP = "promotion_group";
    public static String TABLE_PROMOTION_GROUP_GOODS = "promotion_group_goods";
    public static String TABLE_PROMOTION_IMAGE = "promotion_image";
    public static String TABLE_SICK_INFO = "sick_info";
    public static String TABLE_SNACK = "snack";
    public static String TABLE_VIDEO = "video";


    /**************** 주문 테이블 : 테이블 이름  ***************/
    public static String TABLE_ORDER = "order";
    public static String TABLE_ORDER_CANCEL = "order_cancel";
    public static String TABLE_ORDER_BASKET = "order_basket";
    public static String TABLE_ORDER_DELIVERY_IMAGE = "order_delivery_image";
    public static String TABLE_ORDER_DELIVERY = "order_delivery";
    public static String TABLE_ORDER_DELIVERY_TRACK_INFO = "order_delivery_track_info";
    public static String TABLE_ORDER_EXCHANGE = "order_exchange";
    public static String TABLE_ORDER_EXREFUND = "order_exrefund";
    public static String TABLE_ORDER_POINT = "order_point";
    public static String TABLE_ORDER_REFUND = "order_refund";
    public static String TABLE_ORDER_REFUND_EXCHANGE = "order_refund_exchange";
    public static String TABLE_OTHER_ORDER_INFO = "other_order_info";
    public static String TABLE_ORDER_VIDEO = "order_video";
    public static String TABLE_SELLER = "seller";
    public static String TABLE_SELLER_IMAGE = "seller_image";

    public static String TABLE_MAINTAB = "maintab";
    public static String TABLE_MAINTAB_SUBTAB = "maintab_subtab";
    public static String TABLE_MAINTAB_CT = "maintab_ct";
    public static String TABLE_MAINTAB_BANNER = "maintab_banner";
    public static String TABLE_MAINTAB_BRAND = "maintab_brand";
    public static String TABLE_MAINTAB_GOODS = "maintab_goods";
    public static String TABLE_MAINTAB_PROMOTION = "maintab_promotion";
    public static String TABLE_MAINTAB_EVENT = "maintab_event";
    public static String TABLE_MAINTAB_TOP = "maintab_top";
    public static String TABLE_MAINTAB_TIMESALE = "maintab_timesale";
    public static String TABLE_MAINTAB_TYPE = "maintab_type";
    public static String TABLE_SUBTAB = "subtab";
    public static String TABLE_SUBTAB_BANNER = "subtab_banner";
    public static String TABLE_SUBTAB_CT = "subtab_ct";
    public static String TABLE_SUBTAB_GROUP = "subtab_group";
    public static String TABLE_SUBTAB_GROUP_GOODS = "subtab_group_goods";


    /**************** 회원 테이블 : 테이블 이름  ***************/
    public static String TABLE_ADMIN_USER = "admin_user";
    public static String TABLE_USER_BASKET = "user_basket";
    public static String TABLE_USER_CLAIM = "user_claim";
    public static String TABLE_USER_COUPON = "user_coupon";
    public static String TABLE_USER_DELIVERY_ADDRESS = "user_delivery_address";
    public static String TABLE_USER_FCM = "user_fcm_token";
    public static String TABLE_USER_INFO = "user_info";
    public static String TABLE_USER_PET = "user_pet";
    public static String TABLE_USER_PET_AI_DIAGNOSIS = "user_pet_ai_diagnosis";
    public static String TABLE_USER_PET_AI_FEED = "user_pet_ai_feed";
    public static String TABLE_USER_PET_ALLERGY = "user_pet_allergy";
    public static String TABLE_USER_PET_SICK = "user_pet_sick";
    public static String TABLE_USER_PET_GRAIN_SIZE = "user_pet_grain_size";
    public static String TABLE_USER_PET_FEED = "user_pet_feed";
    public static String TABLE_USER_PET_IMAGE = "user_pet_image";
    public static String TABLE_USER_POINT = "user_point";
    public static String TABLE_USER_NOTI = "user_noti";
    public static String TABLE_USER_NOTI_IMAGE = "user_noti_image";
    public static String TABLE_USER_REVIEW = "user_review";
    public static String TABLE_USER_REVIEW_CLAP = "user_review_clap";
    public static String TABLE_USER_REVIEW_IMAGE = "user_review_image";
    public static String TABLE_USER_SIMPLE_CARD = "user_simple_card";
    public static String TABLE_USER_SMS_AUTH = "user_sms_auth";
    public static String TABLE_USER_TOKEN = "user_token";
    public static String TABLE_USER_WISH = "user_wish";
    public static String TABLE_USER_REFERRAL_CODE = "user_referral_code";
    public static String TABLE_USER_BASE_IMAGE = "user_base_image";


    public static String VIEW_2 = "view_2";
    public static String VIEW_3 = "view_3";
    public static String VIEW_4 = "view_4";
    public static String VIEW_5 = "view_5";
    public static String VIEW_6 = "view_6";
    public static String VIEW_7 = "view_7";
    public static String VIEW_8 = "view_8";
    public static String VIEW_9 = "view_9";
    public static String VIEW_9_1 = "view_9_1";

    public static String getDBName(String table_name) {
        if (table_name.equals(VIEW_2)) {
            return DB_META;
        }
        if (table_name.equals(VIEW_3)) {
            return DB_META;
        }
        if (table_name.equals(VIEW_4)) {
            return DB_META;
        }
        if (table_name.equals(VIEW_5)) {
            return DB_META;
        }
        if (table_name.equals(VIEW_6)) {
            return DB_META;
        }
        if (table_name.equals(VIEW_7)) {
            return DB_META;
        }
        if (table_name.equals(VIEW_8)) {
            return DB_META;
        }
        if (table_name.equals(VIEW_9)) {
            return DB_META;
        }
        if (table_name.equals(VIEW_9_1)) {
            return DB_META;
        }

        if (table_name.equals(TABLE_ERROR_ORDER_FAIL)) {
            return DB_LOG;
        }
        if (table_name.equals(TABLE_ERROR_PROC)) {
            return DB_LOG;
        }
        if (table_name.equals(TABLE_ERROR_API)) {
            return DB_LOG;
        }

        /************** 로그 테이블 : 테이블 이름  ***************/
        if (table_name.equals(TABLE_LOG_USER_NOTICE_ALARAM)) {
            return DB_LOG;
        }
        if (table_name.equals(TABLE_LOG_USER_COUPON_CODE)) {
            return DB_LOG;
        }
        if (table_name.equals(TABLE_LOG_USER_ORDER)) {
            return DB_LOG;
        }
        if (table_name.equals(TABLE_LOG_USER_JOIN)) {
            return DB_LOG;
        }
        if (table_name.equals(TABLE_LOG_USER_LOGIN)) {
            return DB_LOG;
        }
        if (table_name.equals(TABLE_LOG_USER_POINT)) {
            return DB_LOG;
        }
        if (table_name.equals(TABLE_LOG_USER_POLICY)) {
            return DB_LOG;
        }
        if (table_name.equals(TABLE_LOG_USER_ORDER_BASKET)) {
            return DB_LOG;
        }
        if (table_name.equals(TABLE_LOG_USER_SAVE_POINT)) {
            return DB_LOG;
        }
        if (table_name.equals(TABLE_LOG_USER_USE_POINT)) {
            return DB_LOG;
        }
        if (table_name.equals(TABLE_LOG_PRODUCT_HISTORY)) {
            return DB_LOG;
        }
        if (table_name.equals(TABLE_LOG_PRODUCT_OUT_OF_STOCK)) {
            return DB_LOG;
        }
        if (table_name.equals(TABLE_LOG_USER_TOKEN)) {
            return DB_LOG;
        }

        /********** 메타 테이블 : 테이블 이름  ***************/
        if (table_name.equals(TABLE_AAFCO_NRC)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_AI_RECOMMEND_DIAGNOSIS)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_ALLERGY_DATA)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_ATTRIBUTABLE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_BANNER)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_BANNER_IMAGE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_BANK_CODE_STD)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_BRAND)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_BRAND_IMAGE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_CATEGORY)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_CATEGORY_FIELD)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_CATEGORY_TOP)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_CATEGORY_IMAGE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_COURIER)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_COUPON)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_COUPON_BRAND)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_COUPON_CODE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_COUPON_GOODS)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_COUPON_PUBLISH_TYPE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_DELIVERY_COURIER)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_EVENT_IMAGE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_EVENT_INFO)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_EVENT_TYPE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_EVENT_GOODS_TYPE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_EVENT_GOODS_LIST)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_FEED)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_FEED_OTHER_PRODUCT)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_FEED_OTHER_PRODUCT_IMAGE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_GOODS)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_GOODS_IMAGE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_GOODS_PRODUCT)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_GOODS_COUPON)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_GOODS_TYPE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_GRAIN_SIZE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_NOTICE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_NUTI)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_ORDER_TYPE)) {
            return DB_ORDER;
        }
        if (table_name.equals(TABLE_SNACK)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_PAYMENT_REQUEST_TEXT)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_PAYMENT_TYPE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_PSK)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_PET)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_PET_IMAGE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_PET_CATEGORY)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_PET_DOCTOR)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_PET_DOCTOR_FEED_DESC)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_PET_DOCTOR_IMAGE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_PET_DISEASE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_PET_DISEASE_GROUP)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_PROMOTION)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_PROMOTION_GROUP)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_PROMOTION_GROUP_GOODS)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_PROMOTION_IMAGE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_POINT)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_POOD_IMAGE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_PRODUCT)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_PRODUCT_CT)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_PRODUCT_SUB_CT)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_PRODUCT_IMAGE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_SICK_INFO)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_VIDEO)) {
            return DB_META;
        }

        if (table_name.equals(TABLE_MAINTAB)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_MAINTAB_SUBTAB)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_MAINTAB_CT)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_MAINTAB_BANNER)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_MAINTAB_BRAND)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_MAINTAB_GOODS)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_MAINTAB_PROMOTION)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_MAINTAB_EVENT)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_MAINTAB_TIMESALE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_MAINTAB_TOP)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_MAINTAB_TYPE)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_SUBTAB)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_SUBTAB_BANNER)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_SUBTAB_CT)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_SUBTAB_GROUP)) {
            return DB_META;
        }
        if (table_name.equals(TABLE_SUBTAB_GROUP_GOODS)) {
            return DB_META;
        }

        /**************** 주문 테이블 : 테이블 이름  ***************/
        if (table_name.equals(TABLE_ORDER)) {
            return DB_ORDER;
        }
        if (table_name.equals(TABLE_ORDER_BASKET)) {
            return DB_ORDER;
        }
        if (table_name.equals(TABLE_ORDER_CANCEL)) {
            return DB_ORDER;
        }
        if (table_name.equals(TABLE_ORDER_REFUND)) {
            return DB_ORDER;
        }
        if (table_name.equals(TABLE_ORDER_EXCHANGE)) {
            return DB_ORDER;
        }
        if (table_name.equals(TABLE_ORDER_DELIVERY_IMAGE)) {
            return DB_ORDER;
        }
        if (table_name.equals(TABLE_ORDER_DELIVERY)) {
            return DB_ORDER;
        }
        if (table_name.equals(TABLE_ORDER_DELIVERY_TRACK_INFO)) {
            return DB_ORDER;
        }
        if (table_name.equals(TABLE_ORDER_POINT)) {
            return DB_ORDER;
        }
        if (table_name.equals(TABLE_ORDER_VIDEO)) {
            return DB_ORDER;
        }
        if (table_name.equals(TABLE_SELLER)) {
            return DB_ORDER;
        }
        if (table_name.equals(TABLE_SELLER_IMAGE)) {
            return DB_ORDER;
        }
        if (table_name.equals(TABLE_ORDER_EXREFUND)) {
            return DB_ORDER;
        }

        /**************** 회원 테이블 : 테이블 이름  ***************/
        if (table_name.equals(TABLE_ADMIN_USER)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_BASKET)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_CLAIM)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_COUPON)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_DELIVERY_ADDRESS)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_FCM)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_INFO)) {
            return DB_USER;
        }

        if (table_name.equals(TABLE_USER_PET)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_PET_AI_DIAGNOSIS)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_PET_IMAGE)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_PET_AI_FEED)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_PET_ALLERGY)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_PET_SICK)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_PET_GRAIN_SIZE)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_PET_FEED)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_POINT)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_TOKEN)) {
            return DB_USER;
        }

        if (table_name.equals(TABLE_USER_NOTI)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_REVIEW)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_REVIEW_CLAP)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_REVIEW_IMAGE)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_SIMPLE_CARD)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_SMS_AUTH)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_WISH)) {
            return DB_USER;
        }
        if (table_name.equals(TABLE_USER_REFERRAL_CODE)) {
            return DB_USER;
        }

        return "";
    }
}