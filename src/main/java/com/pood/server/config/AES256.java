package com.pood.server.config;

import java.io.UnsupportedEncodingException;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import lombok.experimental.UtilityClass;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@UtilityClass
public class AES256 {

    Logger logger = LoggerFactory.getLogger(AES256.class);

    public static String iv = "0100001111000010";

    public static String key = "r/e9@s#0k*2M/@ua";

    public static Key keySpec;

    public void keySpecGenerator() throws UnsupportedEncodingException {
        byte[] keyBytes = new byte[16];
        byte[] b = key.getBytes("UTF-8");
        int length = b.length;
        if (length > keyBytes.length) {
            length = keyBytes.length;
        }

        System.arraycopy(b, 0, keyBytes, 0, length);
        keySpec = new SecretKeySpec(keyBytes, "AES");
    }


    public String aesEncode(String str) throws Exception {

        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
        c.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(iv.getBytes()));
        byte[] encrypted = c.doFinal(str.getBytes("UTF-8"));
        return new String(Base64.encodeBase64(encrypted));
    }

    public String aesDecode(String str) throws Exception {

        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
        c.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(iv.getBytes()));
        byte[] byteStr = Base64.decodeBase64(str.getBytes());

        return new String(c.doFinal(byteStr), "UTF-8");
    }

    public String getAesDecodeOrNull(final String value) {
        try {
            return aesDecode(value);
        } catch (Exception e) {
            logger.error("AES256 복호화 실패");
        }
        return null;
    }
}