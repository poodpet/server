package com.pood.server.config;

public class PROTOCOL_IDENTIFIER {

 
    public static Boolean PROTOCOL_IAMPORT_PAY_CALLBACK     = true;


    
    // 이벤트 이미지 조회
    public static Boolean PROTOCOL_ADMIN_EVENT_IMAGE_2       = true;
 

 
    // 이벤트 굿즈 타입 조회
    public static Boolean PROTOCOL_ADMIN_EVENT_GOODS_TYPE_2       = true;
 
 
    // 이벤트 타입 조회
    public static Boolean PROTOCOL_ADMIN_EVENT_TYPE_2       = true;

    // 이벤트 타입 조회
    public static Boolean PROTOCOL_ADMIN_EVENT_GOODS_LIST_2       = true;
 
    // 이벤트 정보 조회
    public static Boolean PROTOCOL_ADMIN_EVENT_INFO_2       = true;
 

    // 간편 결제 빌링키 발급
    public static boolean PROTOCOL_USER_SIMPLE_PAY_1 = true;
    // 간편 결제
    public static boolean PROTOCOL_USER_SIMPLE_PAY_2 = true;

    // 회원 주문 배송 기록 추적
	public static boolean PROTOCOL_USER_DELIVERY_TRACKER_2 = true;
    // 곡물 리스트 조회
	public static boolean PROTOCOL_ADMIN_GRAIN_SIZE_2 = true;
    // 알러지 데이터 조회
	public static boolean PROTOCOL_ADMIN_ALLERGY_DATA_2 = true;

	public static boolean PROTOCOL_ADMIN_GOODS_TYPE_1 = true;

	public static boolean PROTOCOL_ADMIN_GOODS_TYPE_2 = true;

	public static boolean PROTOCOL_ADMIN_GOODS_TYPE_3 = true;

	public static boolean PROTOCOL_ADMIN_GOODS_TYPE_4 = true;

	public static boolean PROTOCOL_ADMIN_COUPON_PUBLISH_TYPE_2 = true;

	public static final boolean PROTOCOL_ADMIN_PSK_1 = true;

	public static final boolean PROTOCOL_ADMIN_PSK_2 = true;

	public static final boolean PROTOCOL_ADMIN_PSK_3 = true;

	public static final boolean PROTOCOL_ADMIN_PSK_4 = true;

	public static final boolean PROTOCOL_ADMIN_COURIER_2 = true;

    public static final boolean PROTOCOL_ADMIN_NOTICE_2 = true;



	public static final boolean PROTOCOL_USER_PUSH_1 = true;

    public static final boolean PROTOCOL_USER_PUSH_1_1 = true;



	public static final boolean PROTOCOL_USER_COUPON_1_1 = true;



	public static final boolean PROTOCOL_USER_NOTI_2 = true;



	public static final boolean PROTOCOL_USER_MY_PAGE_2 = true;



	public static final boolean PROTOCOL_USER_NOTI_1 = true;



	public static final boolean PROTOCOL_ADMIN_PET_DOCTOR_DESC_2_1 = true;



    public static final boolean PROTOCOL_EVENT_LIST_2 = true;



    public static final boolean PROTOCOL_USER_PET_AI_FEED_1 = true;

    
    public static final boolean PROTOCOL_USER_PET_AI_FEED_2 = true;


    public static final boolean PROTOCOL_USER_PET_AI_FEED_3 = true;



    public static final boolean PROTOCOL_USER_ORDER_2_1 = true;



    public static final boolean PROTOCOL_EVENT_DETAIL_2 = true;



    public static final boolean PROTOCOL_USER_POINT_2_1 = true;



    public static final boolean PROTOCOL_USER_POINT_2_2 = true;



    public static final boolean PROTOCOL_USER_POINT_2_3 = true;



    public static final boolean PROTOCOL_USER_NOTI_1_1 = true;



    public static final boolean PROTOCOL_USER_PASSWORD_1 = true;



    public static final boolean PROTOCOL_USER_EXCHANGE_1 = true;



    public static final boolean PROTOCOL_ORDER_EXCHANGE_1 = true;



    public static final boolean PROTOCOL_ORDER_REFUND_1 = true;



    public static final boolean PROTOCOL_ADMIN_ATTRIBUTABLE_2 = true;



    public static final boolean PROTOCOL_PAYMENT_CANCEL_1_1 = true;



    public static final boolean PROTOCOL_USER_SMS_1_1 = true;



    public static final boolean PROTOCOL_USER_2_4 = true;



    public static final boolean PROTOCOL_ORDER_EXCHANGE_1_2 = true;

    public static final boolean PROTOCOL_ORDER_REFUND_1_2 = true;



    public static final boolean PROTOCOL_ADMIN_DELIVERY_COURIER_2 = true;



    public static final boolean PROTOCOL_ADMIN_MAIN_2_1 = true;



    public static final boolean PROTOCOL_ADMIN_MAINTAB_CT_2 = true;



    public static final boolean PROTOCOL_USER_REVIEW_CLAP_2 = true;


    public static final boolean PROTOCOL_USER_REVIEW_CLAP_3 = true;



    public static final boolean PROTOCOL_USER_2_3 = true;



    public static final boolean PROTOCOL_TEST_100 = true;

    public static final boolean PROTOCOL_TEST_101 = true;

    public static final boolean PROTOCOL_TEST_102 = true;

    public static final boolean PROTOCOL_TEST_103 = true;

    public static final boolean PROTOCOL_TEST_104 = true;

    public static final boolean PROTOCOL_TEST_105 = true;

    public static final boolean PROTOCOL_TEST_106 = true;

    public static final boolean PROTOCOL_TEST_107 = true;

    public static final boolean PROTOCOL_TEST_108 = true;

    public static final boolean PROTOCOL_TEST_109 = true;



    public static final boolean PROTOCOL_ADMIN_MAIN_2_2 = true;



    public static final boolean PROTOCOL_ADMIN_PROMOTION_2 = true;



    public static final boolean PROTOCOL_TEST_200 = true;



    public static final boolean PROTOCOL_USER_2_5 = true;



    public static final boolean PROTOCOL_ORDER_GOODS_2_3 = true;



    public static final boolean PROTOCOL_TEST_201 = true;



    public static final boolean PROTOCOL_TEST_202 = true;



    public static final boolean PROTOCOL_ADMIN_PRODUCT_2_1 = true;

    public static final boolean PROTOCOL_ADMIN_PRODUCT_2_2 = true;



    public static final boolean PROTOCOL_ORDER_GOODS_2_1_1 = true;



    public static final boolean PROTOCOL_USER_2_6 = true;



    public static final boolean PROTOCOL_ORDER_GOODS_2_2 = true;



    public static final boolean PROTOCOL_ADMIN_BANNER_2 = true;


	public static boolean PROTOCOL_ORDER_DELIVERY_IMAGE_1 = true;



	public static boolean PROTOCOL_USER_PET_IMAGE_3 = true;



	public static boolean PROTOCOL_USER_BASKET_4 = true;



	public static boolean PROTOCOL_USER_WISH_2 = true;
 
 
    // AAFCO-NRC 항목 조회
    public static Boolean PROTOCOL_ADMIN_AAFCO_NRC_2        = true;
 
 
    // AI 맞춤 추천 항목 조회
    public static Boolean PROTOCOL_ADMIN_AI_RECOMMEND_DIAGNOSIS_2 = true;
 
    // 은행 코드 항목 조회
    public static Boolean PROTOCOL_ADMIN_BANK_CODE_STD_2    = true;
  
    // 브랜드 조회
    public static Boolean PROTOCOL_ADMIN_BRAND_2            = true;
 
    // 쿠폰 조회
    public static Boolean PROTOCOL_ADMIN_COUPON_2       = true;
    // 쿠폰 조회
    public static Boolean PROTOCOL_ADMIN_COUPON_2_1     = true;
 
    // 질병 조회
    public static Boolean PROTOCOL_ADMIN_DISEASE_2      = true;
 
    // 질병 그룹 조회
    public static Boolean PROTOCOL_ADMIN_DISEASE_GROUP_2    = true;
 
 
    // 사료 조회
    public static Boolean PROTOCOL_ADMIN_FEED_2 = true;
 
    // 다른 사료 조회
    public static Boolean PROTOCOL_ADMIN_FEED_OTHER_PRODUCT_2 = true;
 
    //메인 화면 타입 조회
    public static Boolean PROTOCOL_ADMIN_MAIN_LIST_TYPE_2 = true;
 
    //메인 화면 정보 조회
    public static Boolean PROTOCOL_ADMIN_MAIN_LIST_2 = true;
 

    //메인 화면 조회 API
    public static Boolean PROTOCOL_ADMIN_MAIN_2 = true;
 
    //메인 굿즈 목록 조회
    public static Boolean PROTOCOL_ADMIN_MAIN_GOODS_LIST_2 = true;
 
    //메인 굿즈 타입 조회
    public static Boolean PROTOCOL_ADMIN_MAIN_GOODS_TYPE_2 = true;
 
    // 푸드 서비스 이미지 조회
    public static Boolean PROTOCOL_ADMIN_POOD_IMAGE_2 = true;
 
    // 영양소 조회
    public static Boolean PROTOCOL_ADMIN_NUTI_2 = true;    
    
    // 결제 주문 타입 조회
    public static Boolean PROTOCOL_ADMIN_ORDER_TYPE_2 = true;    
 
    // 결제 요청 문구 조회
    public static Boolean PROTOCOL_ADMIN_PAYMENT_REQUEST_TEXT_2 = true;
 
    // 결제 타입 조회
    public static Boolean PROTOCOL_ADMIN_PAYMENT_TYPE_2 = true;
 
    // 동물 조회
    public static Boolean PROTOCOL_ADMIN_PET_2 = true;
 
 
    // 동물 카테고리 조회
    public static Boolean PROTOCOL_ADMIN_PET_CATEGORY_2 = true;
 
    // 수의사 조회
    public static Boolean PROTOCOL_ADMIN_PET_DOCTOR_2 = true;
 
    // 수의사 사료 설명 조회
    public static Boolean PROTOCOL_ADMIN_PET_DOCTOR_DESC_2 = true;
 
    // 포인트 조회
    public static Boolean PROTOCOL_ADMIN_POINT_2 = true;
    // 포인트 삭제
 
    // 프로모션 조회
    public static Boolean PROTOCOL_ADMIN_PR_CODE_2 = true;
    // 프로모션 조회
    public static Boolean PROTOCOL_ADMIN_PR_CODE_2_1 = true;
    // 프로모션 조회
    public static Boolean PROTOCOL_ADMIN_PR_CODE_2_2 = true;
    // 프로모션 조회
    public static Boolean PROTOCOL_ADMIN_PR_CODE_2_3 = true;
 
    // 상품 조회
    public static Boolean PROTOCOL_ADMIN_PRODUCT_2 = true;
    // 상품 삭제
 
    


    // 상품 카테고리 조회
    public static Boolean PROTOCOL_ADMIN_PRODUCT_CATEGORY_2 = true;
    // 상품 서브 카테고리 조회
    public static Boolean PROTOCOL_ADMIN_PRODUCT_SUB_CATEGORY_2 = true;
 
    // 질병 정보 조회
    public static Boolean PROTOCOL_ADMIN_SICK_INFO_2 = true;
 
    // 굿즈 조회
    public static Boolean PROTOCOL_ORDER_GOODS_2_1 = true;
    // 굿즈 조회
    public static Boolean PROTOCOL_ORDER_GOODS_2 = true;
 
    // 주문 시작
    public static Boolean PROTOCOL_ORDER_DELIVERY_START_1 = true;
 
    // 셀러 조회
    public static Boolean PROTOCOL_ORDER_SELLER_2 = true;
 
    // 주문 조회
    public static Boolean PROTOCOL_ORDER_2 = true;
 
    // 관리자 조회
    public static Boolean PROTOCOL_USER_ADMIN_2 = true;
  


    /***************************************************************************** */

    // 추천인 코드 이메일 유효 인증 
    public static Boolean PROTOCOL_USER_CHECK_1 = true;

    // 장바구니 등록
    public static Boolean PROTOCOL_USER_BASKET_1 = true;
    // 장바구니 조회
    public static Boolean PROTOCOL_USER_BASKET_2 = true;
    // 장바구니 삭제
    public static Boolean PROTOCOL_USER_BASKET_3 = true;
    // 장바구니 수정
    public static Boolean PROTOCOL_USER_BASKET_4_1 = true;


    // 간편결제 카드 등록
    public static Boolean PROTOCOL_USER_CARD_1 = true;
    // 간편결제 카드 조회
    public static Boolean PROTOCOL_USER_CARD_2 = true;
    // 간편결제 카드 삭제
    public static Boolean PROTOCOL_USER_CARD_3 = true;
    // 간편결제 카드 수정
    public static Boolean PROTOCOL_USER_CARD_4 = true;


    // 회원 문의 등록
    public static Boolean PROTOCOL_USER_CLAIM_1 = true;
    // 회원 문의 조회
    public static Boolean PROTOCOL_USER_CLAIM_2 = true;
    // 회원 문의 삭제
    public static Boolean PROTOCOL_USER_CLAIM_3 = true;
    // 회원 문의 수정
    public static Boolean PROTOCOL_USER_CLAIM_4 = true;
    // 회원 문의 이미지 등록
    public static Boolean PROTOCOL_USER_CLAIM_IMAGE_1 = true;


    // 회원 쿠폰 등록
    public static Boolean PROTOCOL_USER_COUPON_1 = true;
    // 회원 쿠폰 조회
    public static Boolean PROTOCOL_USER_COUPON_2 = true;
    // 회원 쿠폰 삭제
    public static Boolean PROTOCOL_USER_COUPON_3 = true;


    // 회원 배송지 정보 등록
    public static Boolean PROTOCOL_USER_DELIVERY_1 = true;
    // 회원 배송지 정보 조회
    public static Boolean PROTOCOL_USER_DELIVERY_2 = true;
    // 회원 배송지 정보 삭제
    public static Boolean PROTOCOL_USER_DELIVERY_3 = true;
    // 회원 배송지 정보 수정
    public static Boolean PROTOCOL_USER_DELIVERY_4 = true;
 

    // 회원 FCM 토큰 등록
    public static Boolean PROTOCOL_USER_FCM_1 = true;


    // 회원 자동 로그인
    public static Boolean PROTOCOL_USER_LOGIN_AUTO_1 = true;
    // 회원 자동 로그인
    public static Boolean PROTOCOL_USER_LOGIN_AUTO_1_1 = true;
    // 회원 이메일 로그인
    public static Boolean PROTOCOL_USER_LOGIN_MAIL_1 = true;    
    // SNS 인증 로그인
    public static Boolean PROTOCOL_USER_LOGIN_1 = true;
    // 회원 주문 목록
    public static Boolean PROTOCOL_USER_ORDER_2 = true;
    // 회원 비밀번호 재설정 
    public static Boolean PROTOCOL_USER_REPASSWD_1 = true;



    // 회원 반려동물 등록
    public static Boolean PROTOCOL_USER_PET_1 = true;
    // 회원 반려동물 조회
    public static Boolean PROTOCOL_USER_PET_2 = true;
    // 회원 반려동물 삭제
    public static Boolean PROTOCOL_USER_PET_3 = true;
    // 회원 반려동물 수정
    public static Boolean PROTOCOL_USER_PET_4 = true;
    // 회원 반려동물 이미지 등록
    public static Boolean PROTOCOL_USER_PET_IMAGE_1 = true;


    // 회원 포인트 등록
    public static Boolean PROTOCOL_USER_POINT_1 = true;
    // 회원 포인트 조회
    public static Boolean PROTOCOL_USER_POINT_2 = true;

    // 회원 푸쉬 알림 여부 수정
    public static Boolean PROTOCOL_USER_PUSH_4 = true;


    // 회원 환불 계좌 정보 조회
    public static Boolean PROTOCOL_USER_REFUND_2 = true;
    // 회원 환불 계좌 정보 수정
    public static Boolean PROTOCOL_USER_REFUND_4 = true;
    // 회원 환불 계좌 인증
    public static Boolean PROTOCOL_USER_REFUND_5 = true;


    // 회원 리뷰 이미지 등록
    public static Boolean PROTOCOL_USER_REVIEW_IMAGE_1 = true;
    // 회원 리뷰 이미지 삭제
    public static Boolean PROTOCOL_USER_REVIEW_IMAGE_3 = true;
    // 회원 리뷰 등록
    public static Boolean PROTOCOL_USER_REVIEW_1 = true;
    // 회원 리뷰 조회
    public static Boolean PROTOCOL_USER_REVIEW_2 = true;
    // 회원 리뷰 조회
    public static Boolean PROTOCOL_USER_REVIEW_2_1 = true;
    // 회원 리뷰 조회
    public static Boolean PROTOCOL_USER_REVIEW_2_2 = true;
    // 회원 리뷰 조회
    public static Boolean PROTOCOL_USER_REVIEW_2_3 = true;
    // 회원 리뷰 삭제
    public static Boolean PROTOCOL_USER_REVIEW_3 = true;
    // 회원 리뷰 수정
    public static Boolean PROTOCOL_USER_REVIEW_4 = true;


    // 회원 리뷰 도움 추가
    public static Boolean PROTOCOL_USER_REVIEW_CLAP_1 = true;


    // 회원 문자 인증
    public static Boolean PROTOCOL_USER_SMS_1 = true;
    // 회원 문자 인증
    public static Boolean PROTOCOL_USER_SMS_2 = true;


    // 회원 찜 항목 추가 
    public static Boolean PROTOCOL_USER_WISH_1 = true;
    // 회원 찜 항목 삭제
    public static Boolean PROTOCOL_USER_WISH_3 = true;


    // 회원 등록
    public static Boolean PROTOCOL_USER_1 = true;
    // 회원 전체목록 조회
    public static Boolean PROTOCOL_USER_2 = false;
    // 특정 회원 조회
    public static Boolean PROTOCOL_USER_2_1 = true;
    // 회원 삭제
    public static Boolean PROTOCOL_USER_3 = true;
    // 회원 정보 수정 
    public static Boolean PROTOCOL_USER_4 = true;
    // 회원 상태 업데이트
    public static Boolean PROTOCOL_USER_4_1 = true;


    // 결제 취소
    public static Boolean PROTOCOL_PAYMENT_CANCEL_1 = true;
    // 바로구매 취소
    public static Boolean PROTOCOL_PAYMENT_CANCEL_BUY_NOW_1 = true;
    // 아임포트 결제
    public static final boolean PROTOCOL_PAYMENT_IAMPORT_1 = true;
    // 아임포트 결제 후 화면
    public static Boolean PROTOCOL_PAYMENT_RESULT_1 = true;
    
 
}
