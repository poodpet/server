package com.pood.server.config;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.pood.server.api.file.uploader.StorageService;
import com.pood.server.api.service.regex.*;
import com.pood.server.api.service.user.*;
import com.pood.server.object.pagingSet;
import com.pood.server.api.service.list.*;
import com.pood.server.api.service.meta.goods.*;
import com.pood.server.api.service.meta.image.*;
import com.pood.server.api.service.meta.order.*;
import com.pood.server.api.service.meta.product.*;
import com.pood.server.api.service.time.*;
import com.pood.server.api.service.record.*;
import com.pood.server.api.service.checkNull.*;
import com.pood.server.api.service.delivery.*;
import com.pood.server.api.service.sns.*;
import com.pood.server.api.service.noti.*;
import com.pood.server.api.service.error.*;
import com.pood.server.api.service.header.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pood.server.api.service.pay.*;

public class PROTOCOL {

    Logger logger = LoggerFactory.getLogger(PROTOCOL.class);

    @Autowired
    @Qualifier("retreieveService")
    public retreieveService retreieveService;

    @Autowired
    @Qualifier("listService")
    public listService listService;
    
    @Autowired
    @Qualifier("updateService")
    public updateService updateService;

    @Autowired
    @Qualifier("checkNullService")
    public checkNullService checkNullService;
 

    /**************** 푸쉬 알림 **************/
    @Autowired
    @Qualifier("pushService")
    public pushService pushService;

    @Autowired
    @Qualifier("snsService")
    public snsService snsService;
    

    /**************** AWS S3 **************/
    @Autowired
    @Qualifier("storageService")
    public StorageService storageService;







    @Autowired
    @Qualifier("regexService")
    public regexService regexService;

 

    @Autowired
    @Qualifier("userService")
    public userService userService;


    @Autowired
    @Qualifier("userTokenService")
    public userTokenService userTokenService;




    /**************** 굿즈 **************/
    @Autowired
    @Qualifier("goodsService")
    public goodsService goodsService;

    @Autowired
    @Qualifier("errorProcService")
    public errorProcService errorProcService;

 

    /**************** 주문 **************/
    @Autowired
    @Qualifier("orderService")
    public OrderService orderService;


 

    /**************** 이미지 **************/
    @Autowired
    @Qualifier("imageService")
    public imageService imageService;

    /**************** 상품 **************/
    @Autowired
    @Qualifier("productService")
    public productService productService;




    @Autowired
    @Qualifier("timeService")
    public timeService timeService;


    @Autowired
    @Qualifier("remoteDeliveryService")
    public remoteDeliveryService remoteDeliveryService;



    @Autowired
    @Qualifier("headerService")
    public headerService headerService;
 

    /** ************** API 응답 값 **************/
    public HashMap<String, Object> response = getResponse(200);;


    

    public static Boolean RECORD_UPDATE = false;

    public static Boolean RECORD_NOT_UPDATE = true;

 
    public String getHeader(HttpServletRequest request){
        String ip = request.getHeader("X-FORWARDED-FOR");
		if (ip == null)
			ip = request.getRemoteAddr();
        return ip;
    }

    /***************** 페이징 처리 **************/
    public String getResponseMessage(Integer status){
        switch(status){
            case 200:return "성공.";
            case 201:return "이메일이 유효하지 않습니다.";
            case 202:return "로그인 타입이 유효하지 않습니다.";
            case 203:return "회원 정보가 유효하지 않습니다.";
            case 205:return "회원 고유 번호가 유효하지 않습니다.";
            case 208:return "이미 발행된 쿠폰입니다.";
            case 209:return "쿠폰이 만료되었습니다.";
            case 210:return "쿠폰이 발급 제한 개수를 초과하였습니다.";
            case 211:return "이미지를 6장 이상 업로드하였습니다.";
            case 213:return "문자 인증번호가 일치하지 않습니다.";
            case 215:return "인증 요청 시간을 초과하였습니다.";
            case 216:return "이미 인증된 인증요청입니다.";
            case 217:return "회원의 인증정보가 존재하지 않습니다.";
            case 219:return "접근할 수 없는 프로토콜입니다.";
            case 220:return "SNS 키가 유효하지 않은 값입니다.";
            case 221:return "이미 존재하는 회원입니다.";
            case 222:return "추천인 코드가 유효하지 않습니다.";
            case 223:return "핸드폰 번호 기입이 필요합니다.";
            case 224:return "탈퇴한 계정입니다.";
            case 225:return "정책위반 계정입니다.";
            case 226:return "휴먼 계정입니다.";
            case 227:return "이미 가입된 핸드폰 번호가 있습니다.";
            case 228:return "동일한 패스워드입니다.";
            case 229:return "이미 탈퇴한 계정입니다.";
            case 231:return "쿼리 수행 오류. ";
            case 233:return "실명 계좌 인증 오류. ";
            case 234:return "새 비밀번호와 기존 비밀번호가 동일하지 않습니다. ";
            case 235:return "30초 전에 문자 발송이 이루어졌습니다.";
            case 242:return "유효하지 않은 회원 정보입니다.";
            case 244:return "교환/반품/취소 시간을 초과하였습니다.";
            case 245:return "교환/반품 철회시간을 초과하였습니다.";
            case 246:return "주문/반품 요청 건이 이미 접수가 되었습니다.";
            case 247:return "주문 번호에 대한 주문이 존재하지 않습니다.";
            case 248:return "API 토큰이 유효하지 않습니다.";
            case 249:return "주문이 아직 배송완료되지 않았습니다.";
            case 250:return "해당 택배사 코드와 운송장 번호에 해당하는 추적 정보가 존재하지 않습니다.";
            case 251:return "교환 요청하고자 하는 굿즈 목록이 파라미터로 기입되지 않았습니다.";
            case 252:return "해당 교환 요청건에 대해서 요청한 내역이 있습니다.";
            case 253:return "존재하지 않는 쿠폰 코드입니다.";
            case 254:return "이미 등록된 쿠폰 코드입니다.";
            case 255:return "쿠폰 이벤트에 이미 참여 했습니다.";
            case 256:return "사용기간이 만료된 쿠폰 입니다.";
            case 257:return "상품 취소 금액이 배송 금액보다 작습니다.";
            case 300:return "헤더 파라미터가 유효한 파라미터가 아닙니다.";

            default:return "";
        } 
    }

    public static String ADMIN_TOKEN = "vnemakstp2021!!";


    /***************** 페이징 처리 **************/
    public void pagingResult(pagingSet PAGING_SET) throws SQLException {

        Integer total_cnt   = PAGING_SET.getTotal_cnt();
        Integer page_size   = PAGING_SET.getPage_size();
        Integer page_number = PAGING_SET.getPage_number();
         
        response.put("total_cnt", total_cnt);

        // 페이징 처리에 따라서, 응답값으로 페이지 관련된 계산 값 내려줌
        if((page_size != null) && (page_number != null)){
            Integer total_page = (Integer)(total_cnt/page_size);
            Integer remainder = total_cnt % page_size;
            if(remainder != 0)total_page++;

            response.put("page_number", page_number);
            response.put("page_size", page_size);
            response.put("total_page", total_page);
        }else{
            response.put("page_number", 1);
            response.put("page_size", total_cnt);
            response.put("total_page", 1);
        }
    }


    /****************** 전체 페이지 개수 반환 *******************/
    public Integer getTotalPageNumber(Integer total_cnt, Integer page_size){
        Integer total_page = (Integer)(total_cnt/page_size);
        Integer remainder = total_cnt % page_size;
        if(remainder != 0)total_page++;

        return total_page;
    }

  
    /******************* 페이지 넘버 반환 *********************/
    public Integer getPageNumber(Integer page_size, Integer row_number){
        Integer page_number = (Integer)(row_number/page_size);
        page_number++;
        return page_number;

    }


   /************** STATUS CODE와 메세지 내용이 포함된 응답값을 반환하기 위한 함수 **********************/
    public HashMap<String, Object> getResponse(Integer statusCode){
        HashMap<String, Object> response = new HashMap<>();
        String msg = getResponseMessage(statusCode);
        response.put("status",statusCode);
        response.put("msg", msg);
        return response;
    }

    
    /************ API 초기화 ***************/
    public Integer procInit(HttpServletRequest request) throws Exception {

        String IP   = getHeader(request);

        String URL  = request.getServletPath();

        String USER_UUID = (String)request.getHeader("useruuid");

        String TOKEN = (String)request.getHeader("token");

        String HEADER_TOKEN = "";

        if((USER_UUID != null) && (!USER_UUID.equals("")))
            HEADER_TOKEN += "[USER_UUID="+USER_UUID+"]";
        if((TOKEN != null) && (!TOKEN.equals("")))
            HEADER_TOKEN += "[TOKEN="+TOKEN+"]";

        
        logger.info(timeService.getCurrentTime() + "INFO [" + IP + "][" + URL + "]" + HEADER_TOKEN + "[REQUEST]");


        Set<String> keySet = request.getParameterMap().keySet();
        if(keySet != null){
            for(String key: keySet)
                logger.info("PARAMETER : ["+key + ": " + request.getParameter(key)+"]");
        }
 
        if(TOKEN == null){
            response = getResponse(248);
            return 248;
        }

        if(TOKEN.equals(ADMIN_TOKEN)){
            response = getResponse(200);
            return 200;
        }

        if(USER_UUID == null){
            response = getResponse(248);
            return 248;
        }

        Boolean chk = userTokenService.authentication(USER_UUID, TOKEN);

        if(!chk){
            // 토큰 인증 체크
            response = getResponse(248);
            return 248;
        }

        response = getResponse(200);
    
        
        return 200;
    }



    /************ API 닫음 ***************/
    public void procClose(HttpServletRequest request, String RESULT) {
  
        String IP   = getHeader(request);

        String URL  =   request.getServletPath();

        // 헤더 파라미터 로그로 남김
        if(RESULT == null) {
            logger.info(timeService.getCurrentTime() + "INFO [" + IP + "][" + URL + "][RESEPONSE]");
        }
        else {
            logger.info(timeService.getCurrentTime() + "INFO [" + IP + "][" + URL + "][RESEPONSE]["  + RESULT + "]");
        }
    }
 
    public Map<String, Object> getJsonMap(String result){
        return new Gson().fromJson(
            result, new TypeToken<HashMap<String, Object>>() {}.getType()
        );
    }

    public void thExecption(String PATH, Exception e) throws SQLException{
        String ERROR_MESSAGE = e.getMessage();
        logger.error("[" + PATH + "] [MSG = " + ERROR_MESSAGE + "]");
        errorProcService.insertRecord(PATH, ERROR_MESSAGE, e.getStackTrace().toString());
    }
}
