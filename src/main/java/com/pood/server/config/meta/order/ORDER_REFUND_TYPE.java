package com.pood.server.config.meta.order;

public class ORDER_REFUND_TYPE {

    public static Integer DELIVERY_FEE_REQUEST = 34;

    public static Integer DELIVERY_FEE_COMPLETE = 35;

    public static Integer REFUND_REQUEST = 40;

    public static Integer REFUND_PROCEED = 41;

    public static Integer REFUND_REJECT = 42;

    public static Integer REFUND_COMPLETE = 43;

    public static Integer REFUND_DELIVERY_FEE_REQUEST = 44;

    public static Integer REFUND_DELIVERY_FEE_COMPLETE = 45;

}
