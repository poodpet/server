package com.pood.server.config.meta.user;

public class USER_POINT {

    public static Integer STATUS_READY_TO_BE_SAVED  = 0;

    public static Integer STATUS_SAVE_COMPLETE      = 1;

    public static Integer STATUS_SAVE_RETRIEVED     = 2;

    public static Integer STATUS_USED               = 3;

    public static Integer STATUS_USE_RETRIEVED      = 4;

    public static Integer STATUS_EXPIRED            = 5;
    
}
