package com.pood.server.config.meta;

public class META_MAIL {
    
    public static String SENDER_EMAIL   = "help@pood.pet";

    public static String SENDER_NAME    = "푸드 관리자";

    public static String TITLE_1(Integer goods_idx, Integer product_idx, Integer product_quantity, Integer product_qty){
       return "굿즈["+goods_idx+"] 판매 일시중지 알림"; 
    }  

    public static String TEXT_1(Integer goods_idx, Integer product_idx,  Integer product_quantity, Integer product_qty){
        return "* 상품 항목번호["+product_idx+"]의 재고 부족. \r\n* 현재 상품[" +product_idx+"]의 재고 수량 : "+product_quantity+"개.\r\n* 굿즈["+goods_idx+"] 구성품 수량 "+product_qty+" 개 미만이 되어 판매 일시 중지.";
    }
}
