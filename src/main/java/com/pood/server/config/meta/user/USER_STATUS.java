package com.pood.server.config.meta.user;

public class USER_STATUS {

    public static Integer DEFAULT = 0;

    public static Integer DEACTIVE = 1;

    public static Integer POLICY_VIOLATION = 2;

    public static Integer DORMANT_ACCOUNT = 3;

    public static Integer TO_RESIGN = 5;

    public static Integer RESIGNED = 6;

}
