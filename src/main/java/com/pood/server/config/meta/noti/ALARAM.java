package com.pood.server.config.meta.noti;

public class ALARAM {

    public static Integer  DELIVERY_START   = 11;

    public static Integer  DELIVERY_SUCCESS = 12;

    public static Integer  TO_BE_REFUNDED   = 13;

    public static Integer  TO_BE_EXCHANGED  = 14;

    public static Integer  CANCEL_SUCCESS   = 15;

    public static Integer  WRITE_REVIEW     = 31;

    public static Integer  CLAIM_ANSWER     = 41;

    public static Integer  SAVE_POINT       = 51;

    public static Integer  POINT_TO_BE_USED = 52;


    public String GET_DELIVERY_START(String ORDER_NUMBER){
        return "주문하신 상품(" + ORDER_NUMBER + ")의 배송이 시작되었습니다.";
    }

    public String GET_DELIVERY_SUCCESS(String ORDER_NUMBER){
        return "주문하신 상품(" + ORDER_NUMBER + ")의 배송을 완료하였습니다.";
    }

    public String GET_TO_BE_REFUNDED(String GOODS_NAME){
        return "요청하신 상품(" + GOODS_NAME +")의 반품 요청이 승인되었습니다. 결제 수단에 따라 평일기준 최대 7일이 소요될 수 있습니다.";
    }

    public String GET_TO_BE_EXCHANGED(String GOODS_NAME){
        return "요청하신 상품(" + GOODS_NAME + ")의 교환요청이 승인되었습니다.";
    }

    public String GET_CANCEL_SUCCESS(String ORDER_NUMBER){
        return "주문하신 상품(" + ORDER_NUMBER +")의 주문 취소가 완료되었습니다.";
    }

    public String GET_WRITE_REVIEW(String GOODS_NAME){
        return GOODS_NAME + "에 대한 리뷰를 남겨주세요!";
    }

    public String GET_CLAIM_ANSWER(){
        return "문의하신 내용에 대한 답변이 완료되었습니다.";
    }

    public String GET_SAVE_POINT(String ORDER_NUMBER, Integer POINT){
        return "주문하신 상품(" +ORDER_NUMBER+")에 대한 " +POINT+ " 포인트가 적립되었습니다.";
    }

    public String GET_POINT_TO_BE_USED(Integer POINT, String DATE){
        return "푸드 포인트가 " +POINT+"원이 " +DATE+ "에 소멸될 예정입니다.";
    }


}
