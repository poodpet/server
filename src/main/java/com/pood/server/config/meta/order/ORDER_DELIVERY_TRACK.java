package com.pood.server.config.meta.order;

public class ORDER_DELIVERY_TRACK {
 

    public static Integer SEND_TYPE_DEFAULT           = 10;

    public static Integer SEND_TYPE_EXCHANGE_REQUEST  = 31;

    public static Integer SEND_TYPE_REFUND_REQUEST    = 41;


    public static Integer DELIVERY_TYPE_DEFAULT = 0;

 

    public static Integer SEND_SUCCESS  = 1;

    public static Integer SEND_START    = 2;


    public static Integer MAIN_ADDRESS_TRUE     = 1;

    public static Integer MAIN_ADDRESS_FALSE    = 0;


}
