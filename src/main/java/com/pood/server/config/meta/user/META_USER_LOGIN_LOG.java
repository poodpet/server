package com.pood.server.config.meta.user;

public class META_USER_LOGIN_LOG {

    public static Integer LOG_IN            = 0;

    public static Integer LOG_OUT           = 1;

    public static Integer LOG_AUTO_IN       = 2;


    public static Integer OS_TYPE_UNKNOWN       = 0;

    public static Integer OS_TYPE_ANDROID       = 1;

    public static Integer OS_TYPE_IOS           = 2;

    public static Integer OS_TYPE_MOBILE        = 3;

    public static Integer OS_TYPE_PC            = 4;

}
