package com.pood.server.config.meta.coupon;

public class COUPON_TYPE {

    public static Integer WELCOME_COUPON        = 14;

    public static Integer GOODS_COUPON          = 15;

    public static Integer TEST_COUPON           = 16;

    public static Integer DELIVERY_FEE_COUPON   = 17;

    public static Integer BRAND_COUPON          = 18;

    public static Integer PET_TYPE_COUPON       = 19;
    
}
