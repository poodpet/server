package com.pood.server.config.meta.user;

public class USER_MEMBERSHIP_LEVEL {
    

    public static String    ML_BLUE_NAME = "BLUE";
    public static Float     ML_BLUE_POINT = (float)0.03;
    public static Integer   ML_BLUE_PRICE = 500000;
    public static Integer   ML_BLUE_MONTH = 2;

    
    public static String    ML_WELCOME_NAME = "WELCOME";
    public static Float     ML_WELCOME_POINT = (float)0.02;
    public static Integer   ML_WELCOME_PRICE = 0;
    public static Integer   ML_WELCOME_MONTH = 0;
    
}
