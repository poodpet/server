package com.pood.server.config.meta.point;

public class UP_STATUS {

    public static Integer SAVED                 = 1;

    public static Integer READY_TO_USE          = 3;

    public static Integer DEPRECATED            = 5;

    public static Integer TO_BE_EARNED_RETRIEVE = 7;

    public static Integer USED                  = 10;

    public static Integer EXPIRED               = 13;

}
