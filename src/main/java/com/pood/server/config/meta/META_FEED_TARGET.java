package com.pood.server.config.meta;

public class META_FEED_TARGET {

    public static Integer   ALL     = 0;

    public static Integer   SMALL   = 1;

    public static Integer   MEDIUM  = 2;

    public static Integer   BIG     = 3;

}
