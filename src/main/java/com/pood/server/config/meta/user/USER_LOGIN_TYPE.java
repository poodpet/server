package com.pood.server.config.meta.user;

public class USER_LOGIN_TYPE {

    public static Integer AUTO_LOGIN = 2;
    
    public static Integer SNS = 3;
    
    public static Integer EMAIL = 5;

}
