package com.pood.server.config.meta;

public class META_GOODS {

    public static Integer SALE_STATUS_READY = 0;            // 판매 대기

    public static Integer SALE_STATUS_ON    = 1;            // 판매중

    public static Integer SALE_STATUS_STOP  = 2;            // 판매 중지

    public static Integer SALE_STATUS_HOLD  = 3;            // 판매 일시 중지
    
    public static Integer GOODS_IMAGE_TYPE_PRODUCT = 0;

    public static Integer GOODS_IMAGE_TYPE_GOODS   = 1;


    
    public static Integer PURCHASE_TYPE_BUY_DEFAULT = 0;
    
    public static Integer PURCHASE_TYPE_BUY_ONE     = 1;



    public static Integer LIMIT_QUANTITY_OFF    = 0;

    public static Integer LIMIT_QUANTITY_ONE    = 1;

    public static Integer LIMIT_QUANTITY_LESS_THAN = 2;
    

    public static Integer GOODS_IMAGE_TYPE_MAIN = 0;



    public static Integer GOODS_TYPE_PROMOTION  = 28;

    public static Integer GOODS_TYPE_ALWAYS     = 29;

    public static Integer GOODS_SAMPLE_STORE    = 39;

    public static Integer GOODS_100             = 40;


}
