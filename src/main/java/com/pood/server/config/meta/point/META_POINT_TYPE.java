package com.pood.server.config.meta.point;

public class META_POINT_TYPE {

    public static Integer PURCHASE          = 12;

    public static Integer FIRST_REVIEW      = 21;

    public static Integer TEXT_REVIEW       = 22;

    public static Integer PHOTO_REVIEW      = 23;

    public static Integer FRIEND_INVITATION = 31;

    public static Integer PUSH              = 41;

    public static Integer DELIVERY_DELAY    = 51;

    public static Integer GET_POINT(Integer POINT_TYPE){
        
        if(POINT_TYPE.equals(FIRST_REVIEW))return 500;

        if(POINT_TYPE.equals(TEXT_REVIEW))return 300;

        if(POINT_TYPE.equals(PHOTO_REVIEW))return 500;

        if(POINT_TYPE.equals(FRIEND_INVITATION))return 5000;

        if(POINT_TYPE.equals(PUSH))return 50;

        if(POINT_TYPE.equals(DELIVERY_DELAY))return 1000;

        return 0;
    } 
}
