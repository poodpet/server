package com.pood.server.config.meta.order;

public class ORDER_STATUS_MESSAGE {
    

    public static String PAYMENT_SUCCESS        = "결제가 정상적으로 처리되었습니다.";
     
    public static String ORDER_CANCEL_FAILED    = "주문 취소가 실패 처리되었습니다. ";

    public static String EXCHANGE_REQUEST_MESSAGE(String EXCHANGE_NUMBER){
        return "교환이 정상적으로 요청되었습니다 : 교환번호=[" + EXCHANGE_NUMBER + "]";
    }
   
    public static String REFUND_REQUEST_MESSAGE(String REFUND_NUMBER){
        return "반품이 정상적으로 요청되었습니다. : 반품번호=[" + REFUND_NUMBER + "]";
    }

    public static String REFUND_SUCCESS_MESSAGE(String order_number){
        return "주문 번호[" + order_number + "] 정상적으로 환불처리 되었습니다. ";
    }
  

    public static String EXCHANGE_RETRIEVED(String EXCHANGE_NUMBER){
        return "교환[" + EXCHANGE_NUMBER + "]건에 대해서 정상적으로 철회요청이 되었습니다.";
    }

    public static String REFUND_RETRIEVED(String REFUND_NUMBER){
        return "반품[" + REFUND_NUMBER + "]건에 대해서 정상적으로 철회요청이 되었습니다. ";
    }

    public static String EXCHANGE_WITHDRAWAL_ACCEPTED(String EXCHANGE_NUMBER){
        return "교환 철회 요청[" + EXCHANGE_NUMBER + "] 건에 대해서 정상적으로 철회되었습니다. ";
    }

    public static String REFUND_WITHDRAWAL_ACCEPTED(String REFUND_NUMBER){
        return "반품 철회 요청[" + REFUND_NUMBER + "] 건에 대해서 정상적으로 철회되었습니다. ";
    }
}
