package com.pood.server.config.meta.order;

public class ORDER_STATUS {

    public static Integer ORDER_READY           = 0;    // 주문접수

    public static Integer ORDER_SUCCESS         = 1;    // 결제완료

    public static Integer ORDER_CANCEL_READY    = 2;    // 주문취소 신청

    public static Integer ORDER_CANCEL_SUCCESS  = 3;    // 주문취소 완료

    public static Integer RELEASE_READY         = 4;    // 출고대기
    
    public static Integer DELIVERY_START        = 11;   // 배송중
    
    public static Integer DELIVERY_SUCCESS      = 12;   // 배송완료

    public static Integer DELIVERY_DELAYED      = 13;   // 배송지연
    
    public static Integer EXCHANGE_REQUEST      = 14;   // 교환신청

    public static Integer REFUND_REQUEST        = 16;   // 반품신청
        
    public static Integer EXCHANGE_ACCEPT       = 18;   // 교환승인

    public static Integer EXCHANGE_REJECT       = 19;   // 교환거부

    public static Integer REJECT_ACCEPT         = 20;   // 반품승인

    public static Integer REJECT_REJECT         = 21;   // 반품거부

    public static Integer EXCHANGE_DELIVERY_READY   = 22;   // 교환배송준비

    public static Integer EXCHANGE_DELIVERY_START   = 23;   // 교환배송중

    public static Integer EXCHANGE_DELIVERY_SUCCESS = 24;   // 교환배송완료

    public static Integer EXCHANGE_DELIVERY_DELAYED = 25;   // 교환배송지연&불가

    public static Integer EXCHANGE_RETRIEVE         = 26;   // 교환철회
     
    public static Integer REFUND_RETRIEVE           = 27;   // 반품철회

    public static Integer EXCHANGE_ACCEPTANCE       = 28;   // 교환접수

    public static Integer REFUND_ACCEPTANCE         = 29;   // 반품접수

    public static Integer DELIVERY_IMP              = 30;   // 배송불가

    public static Integer EXCHANGE_DELIVERY_IMP     = 31;   // 교환배송불가

    public static Integer EXCHANGE_RELEASE_YES          = 34;   // 교환출고대기

    public static Integer EXCHANGE_RELEASE_NO           = 35;   // 교환출고거부

    public static Integer REFUND_COLLECTION             = 36;   // 반품회수접수

    public static Integer EXCHANGE_COLLECTION           = 37;   // 교환회수접수

    public static Integer EXCHANGE_COMPLETE             = 38;   // 교환완료

    public static Integer REFUND_COMPLETE               = 39;   // 반품완료

    public static Integer EXCHANGE_WITHDRAWAL_ACCEPT          = 40;   // 교환 철회 완료

    public static Integer REFUND_WITHDRAWAL_ACCEPT            = 41;   // 반품 철회 완료

}
