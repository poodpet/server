package com.pood.server.config.meta.point;

public class POINT_USE_STATUS {
 
    public static Integer USE_COMPLETE = 12;
    
    public static Integer USE_RETRIEVE   = 13;

    public static Integer USE_EXPIRED  = 21;


    public static String SET_TEXT(Integer POINT_STATUS){
        if(POINT_STATUS == 12)
            return "사용완료";
        if(POINT_STATUS == 13)
            return "사용취소";
        if(POINT_STATUS == 21)
            return "기한만료";
            
        return "";
    }
}
