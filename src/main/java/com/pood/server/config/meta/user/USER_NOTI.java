package com.pood.server.config.meta.user;

public class USER_NOTI {

    public static Integer DELIVERY_START    = 11;

    public static Integer DELIVERY_FINISHED = 12;

    public static Integer TO_BE_REFUNDED    = 13;

    public static Integer TO_BE_EXCHANGED   = 14;

    public static Integer CANCEL_FINISHED   = 15;

    public static Integer EVENT             = 60;

    public static Integer PROMOTION         = 61;

    public static Integer ORDER_LIST        = 62;

    public static Integer REVIEW_LIST       = 63;

    public static Integer GOODS_LIST        = 64;

    public static Integer COUPON_MESSAGE    = 65;

    public static Integer NOTICE            = 66;

    public static Integer PROMO             = 21;

    public static Integer ANSWER_FINISHED   = 41;

    public static Integer POINT_TO_BE_SAVED = 51;

    public static Integer POINT_TO_BE_USED  = 52;
}

