package com.pood.server.config.meta;

public class META_MAIN_TAB_TOP {

    public static Integer MAIN = 1;

    public static Integer MAIN_POOD_ITEM = 2;

    public static Integer MAIN_POOD_BEST = 3;

    public static Integer MAIN_EVENT = 4;

    public static Integer MAIN_PET_DOCTOR = 5;

    public static Integer MAIN_POOD_BRAND = 6;

    public static Integer MAIN_PROMOTION = 7;

}
