package com.pood.server.config.meta.order;

public class ORDER_DELIVERY_ADDRESS_TYPE {

    public static Integer   DEFAULT = 10;

    public static Integer   EXCHANGE_PICK_UP = 30;

    public static Integer   EXCHANGE_SENDER  = 31;

    public static Integer   REFUND_PICK_UP   = 40;

    public static Integer   REFUND_SENDER    = 41;


}
