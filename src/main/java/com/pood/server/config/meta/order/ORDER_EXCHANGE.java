package com.pood.server.config.meta.order;

public class ORDER_EXCHANGE {

    
    public static Integer TYPE_REQUEST  =   0;
    
    public static Integer TYPE_ACCEPT   =   1;
    
    public static Integer TYPE_SUCCESS  =   2;
    
    public static Integer TYPE_REJECT   =   3;

    // public static Integer TYPE_DELIVERY_READY   =   4;

    // public static Integer TYPE_DELIVERY_START   =   5;

    // public static Integer TYPE_DELIVERY_SUCCESS =   6;

    public static Integer TYPE_RETRIEVE         =   7;  // 교환 철회 요청

    public static Integer TYPE_ACCEPTANCE       =   8;

    // public static Integer TYPE_DELIVERY_DELAY   =   9;

    // public static Integer TYPE_DELIVERY_IMP     =   10;

    // public static Integer TYPE_COLLECTION       =   11;

    // public static Integer TYPE_RELEASE_YES      =   12;

    // public static Integer TYPE_RELEASE_NO       =   13;

    public static Integer TYPE_COMPLETE         =   14;

    public static Integer TYPE_WITHDRAWAL_ACCEPT    =   15;     // 교환 철회 승인

    public static Integer ATTR_SELLER   = 1;

    public static Integer ATTR_BUYER    = 0;
    
}
