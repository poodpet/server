package com.pood.server.config.meta.remote;

public class REMOTE_TYPE {

    public static Integer NOT_REMOTE = 0;

    public static Integer OTHER_ISLAND = 1;

    public static Integer JEJU = 2;


    public static String GET_REMOTE_TYPE_TITLE(Integer REMOTE_TYPE){

        switch(REMOTE_TYPE){
            case 0 : return "일반 배송지";
            case 1 : return "도서산간";
            case 2 : return "제주도";
        }

        return "";
    }
    
}
