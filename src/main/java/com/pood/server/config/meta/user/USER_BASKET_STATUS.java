package com.pood.server.config.meta.user;

public class USER_BASKET_STATUS {
    
    public static Integer DEFAULT = 1;

    public static Integer BUY_NOW = 2;

    public static Integer NOT_VISIBLE = 3;
}
