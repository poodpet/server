package com.pood.server.config.meta;

public class META_UNIT_SIZE {
    
    public static String A_MIN = "0";

    public static String A_MAX = "0.9";

    public static String B_MIN = "1";

    public static String B_MAX = "1.4";

    public static String C_MIN = "1.5";

    public static String C_MAX = "10";


}
