package com.pood.server.config.meta;

public class META_NOTICE {

    public static Integer SYSTEM_MAINTENANCE    = 11;

    public static Integer MAIN_UPDATE           = 12;

    public static Integer DELIVERY_DELAY        = 20;

    public static Integer DEFAULT               = 30;

}
