package com.pood.server.config.meta.point;

public class POINT_STATUS {

    public static Integer AVAILABLE = 0;

    public static Integer USED = 1;

    public static Integer NOT_AVAILABLE = 2;
    
}
