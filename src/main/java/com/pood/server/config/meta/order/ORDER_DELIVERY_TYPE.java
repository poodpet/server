package com.pood.server.config.meta.order;

public class ORDER_DELIVERY_TYPE {

    public static Integer DEFAULT = 0;

    public static Integer VISIT = 1;

    public static Integer QUICK = 2;

    public static Integer REGULAR = 3;
    
}
