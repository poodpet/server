package com.pood.server.config.meta.user;

public class USER_PET {

    public static Integer DEFAULT = 0;

    public static Integer UPLOADER = 1;

    public static Integer NOT_VISIBLE = 2;

    public static Integer POLICY_VIOLATION = 3;
}
