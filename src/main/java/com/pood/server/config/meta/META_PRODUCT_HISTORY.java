package com.pood.server.config.meta;

public class META_PRODUCT_HISTORY {

    public static String PRODUCT_IN(String PRODUCT_UUID, Integer QUANTITY){
        return "상품[" + PRODUCT_UUID + "]이 " + QUANTITY + "개 입고 되었습니다.";
    }

    public static String PRODUCT_OUT(String PRODUCT_UUID, Integer QUANTITY){
        return "상품[" + PRODUCT_UUID + "]이 " + QUANTITY + "개 출고 되었습니다.";
    }

    public static String PRODUCT_OUT_OF_STOCK_TITLE(String PRODUCT_UUID){
        return "상품 재고소진 안내 메일";
    }
    public static String PRODUCT_OUT_OF_STOCK_TEXT(String PRODUCT_UUID, Integer QUANTITY, Integer AVA_QUANTITY){
        return "상품[" + PRODUCT_UUID + "]의 현재 재고가 " + QUANTITY + "가 되어 가용 수량 " + AVA_QUANTITY + "개 이하가 되었습니다.";
    }

}
