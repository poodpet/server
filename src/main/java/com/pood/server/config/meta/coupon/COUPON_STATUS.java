package com.pood.server.config.meta.coupon;

public class COUPON_STATUS {

    public static Integer CANNOT_USE = 0;

    public static Integer AVAILABLE = 1;

    public static Integer USED =2;
    
}
