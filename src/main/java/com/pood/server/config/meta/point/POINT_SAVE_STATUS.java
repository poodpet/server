package com.pood.server.config.meta.point;

public class POINT_SAVE_STATUS {

    public static Integer TO_BE_SAVED       = 11;

    public static Integer SAVE_COMPLETE = 12;

    public static Integer SAVE_RETRIEVED   = 13;

    public static String SET_TEXT(Integer POINT_STATUS){
        if(POINT_STATUS == 11)
            return "적립예정";
        if(POINT_STATUS == 12)
            return "적립완료";
        if(POINT_STATUS == 13)
            return "적립취소";
            
        return "";
    }
    
}
