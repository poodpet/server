package com.pood.server.config.meta.order;

public class ORDER_REFUND {

    public static Integer TYPE_REQUEST          =   0;

    public static Integer TYPE_ACCEPT           =   1;

    public static Integer TYPE_SUCCESS          =   2;

    public static Integer TYPE_REJECT           =   3;
    
    public static Integer TYPE_RETRIEVE_REQ     =   4; // 반품 철회 요청

    public static Integer TYPE_ACCEPTANCE       =   5;

    public static Integer TYPE_COLLECTION       =   7;

    public static Integer TYPE_COMPLETE         =   9;

    public static Integer TYPE_WITHDRAWAL_ACCEPT         =   10;    // 반품 철회 승인

    public static Integer ATTR_SELLER           =   1;

    public static Integer ATTR_BUYER            =   0;
    
}
