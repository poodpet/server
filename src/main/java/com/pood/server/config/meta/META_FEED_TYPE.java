package com.pood.server.config.meta;

import java.util.Arrays;
import java.util.List;

public class META_FEED_TYPE {

    public static List<String> ALL      = Arrays.asList("M");

    public static List<String> PUPPY    = Arrays.asList("P", "PA", "PAL", "M");

    public static List<String> ADULT    = Arrays.asList("A", "PA", "PAL", "AS", "M");

    public static List<String> SENIOR   = Arrays.asList("S", "AS", "M");
        
}
