package com.pood.server.config.meta.user;

public class USER_REVIEW_IMAGE {

    public static Integer   DEFAULT = 0;

    public static Integer   ONLY_FOR_WRITER = 1;

    public static Integer   NOT_VISIBLE = 2;

    public static Integer   POLICY_VIOLATION = 3;
    
}
