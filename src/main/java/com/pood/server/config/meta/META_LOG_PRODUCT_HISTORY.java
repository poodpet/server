package com.pood.server.config.meta;

public class META_LOG_PRODUCT_HISTORY {

    public static Integer TYPE_OUT  = 0;

    public static Integer TYPE_IN   = 1;

    public static Integer TYPE_PAY_SUCCESS  = 2;

    public static Integer TYPE_PAY_RETRIEVE = 3;

    public static String ORDER_SUCCESS = "주문 완료 상품 재고 차감";

    public static String ORDER_RETRIEVE = "주문 취소 상품 재고 가감";

}
