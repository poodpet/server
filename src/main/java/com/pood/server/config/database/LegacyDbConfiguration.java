package com.pood.server.config.database;

import com.pood.server.config.AES256;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LegacyDbConfiguration {

    @Value("${spring.datasource.username}")
    private String userName;

    @Value("${spring.datasource.password}")
    private String password;

    @Value("${legacy.datasource}")
    private String url;

    /**
     * 레거시 jdbc connection bean으로 수정작업 진행
     *
     * @return HikariDataSource
     * @author lsj8367
     * @since 2022-02-09
     */
    @Bean(name = "dataSource")
    public DataSource poolInit() {

        HikariConfig config = new HikariConfig();

        config.setJdbcUrl(url);
        config.setUsername(userName);
        config.setPassword(AES256.getAesDecodeOrNull(password));

        config.setMaximumPoolSize(10);
        config.setAutoCommit(false);
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        config.setLeakDetectionThreshold(60 * 1000);
        config.setConnectionTimeout(5000);
        return new HikariDataSource(config);
    }
}
