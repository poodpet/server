package com.pood.server.config.database;

import com.pood.server.config.AES256;
import java.util.Map;
import java.util.Objects;
import javax.sql.DataSource;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
    basePackages = "com.pood.server.repository.log",
    entityManagerFactoryRef = "logEntityManager",
    transactionManagerRef = "logTransactionManager"
)
@RequiredArgsConstructor
public class LogDatabase {

    private final Environment env;

    @Bean
    public DataSource logDataSource() {
        return DataSourceBuilder.create()
            .driverClassName(env.getProperty("spring.datasource.driver-class-name"))
            .url(env.getProperty("spring.log_db.datasource.url"))
            .username(env.getProperty("spring.datasource.username"))
            .password(AES256.getAesDecodeOrNull(env.getProperty("spring.datasource.password")))
            .build();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean logEntityManager() {
        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(logDataSource());
        em.setPackagesToScan("com.pood.server.entity.log");
        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        em.setJpaPropertyMap(jpaPropertyMap());
        return em;
    }

    private Map<String, Object> jpaPropertyMap() {
        return Map.of(
            "hibernate.hbm2ddl.auto",
            Objects.requireNonNull(env.getProperty("spring.jpa.hibernate.ddl-auto")),
            "hibernate.format_sql",
            Objects.requireNonNull(env.getProperty("spring.jpa.properties.hibernate.format_sql")),
            "hibernate.dialect",
            Objects.requireNonNull(env.getProperty("spring.jpa.database-platform")),
            "hibernate.temp.use_jdbc_metadata_defaults", Objects.requireNonNull(
                env.getProperty("spring.jpa.properties.hibernate.temp.use_jdbc_metadata_defaults"))
        );
    }

    @Bean
    public PlatformTransactionManager logTransactionManager() {
        return new JpaTransactionManager(Objects.requireNonNull(logEntityManager().getObject()));
    }


}

