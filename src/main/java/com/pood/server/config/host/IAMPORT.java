package com.pood.server.config.host;

public class IAMPORT {

    public static String TOKEN_URL          = "https://api.iamport.kr/users/getToken";

    public static String REFUND_URL         = "https://api.iamport.kr/payments/cancel";
    
    public static String GET_BILLING_KEY    = "https://api.iamport.kr/subscribe/customers/";

    public static String DO_SIMPLE_PAY      = "https://api.iamport.kr/subscribe/payments/again";

    public static String FIND_PAYMENT       = "https://api.iamport.kr/payments/find/";

    public static String VBANK_HOLDER       = "https://api.iamport.kr/vbanks/holder";

    public static String POOD_CARD_NUMBER   = "4336-9200-0087-9839";
    
    public static String POOD_EXPIRY        = "2022-11";

    public static String POOD_BIRTH         = "126-86-81371";

    public static String POOD_PWD           = "99";

    public static String IMP_UID            = "imp70585969";

    public static String PG                 = "html5_inicis";

    public static String PG_TEST            = "INIBillTst";

    public static String PAY_METHOD         = "card";

}
