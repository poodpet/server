package com.pood.server.config.querydsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OrderQuerydslConfiguration {

    @PersistenceContext(unitName = "orderEntityManager")
    EntityManager orderEntityManager;

    @Bean(name = "orderQueryFactory")
    public JPAQueryFactory orderQueryFactory() {
        return new JPAQueryFactory(orderEntityManager);
    }
}
