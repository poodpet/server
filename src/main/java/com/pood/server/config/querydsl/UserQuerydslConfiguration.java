package com.pood.server.config.querydsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserQuerydslConfiguration {

    @PersistenceContext(unitName = "userEntityManager")
    EntityManager userEntityManager;

    @Bean(name = "userQueryFactory")
    public JPAQueryFactory userQueryFactory() {
        return new JPAQueryFactory(userEntityManager);
    }
}
