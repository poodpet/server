package com.pood.server.config.querydsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MetaQuerydslConfiguration {

    @PersistenceContext(unitName = "metaEntityManager")
    EntityManager metaEntityManager;

    @Bean(name = "metaQueryFactory")
    public JPAQueryFactory metaQueryFactory() {
        return new JPAQueryFactory(metaEntityManager);
    }
}
