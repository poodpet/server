package com.pood.server.config.querydsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LogQuerydslConfiguration {

    @PersistenceContext(unitName = "logEntityManager")
    EntityManager logEntityManager;

    @Bean(name = "logQueryFactory")
    public JPAQueryFactory logQueryFactory() {
        return new JPAQueryFactory(logEntityManager);
    }
}
