package com.pood.server.config.factory;

import com.pood.server.repository.meta.EventCommentRepository;
import com.pood.server.repository.meta.EventParticipationRepository;
import com.pood.server.repository.meta.EventPhotoRepository;
import com.pood.server.service.EventCommentSeparate;
import com.pood.server.service.EventParticipationSeparate;
import com.pood.server.service.EventPhotoSeparate;
import com.pood.server.service.factory.EventTypeFactory;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class EventTypeConfig {

    private final EventCommentRepository eventCommentRepository;
    private final EventParticipationRepository eventParticipationRepository;
    private final EventPhotoRepository eventPhotoRepository;

    @Bean
    public EventTypeFactory getEventTypeFactory() {
        return new EventTypeFactory(
            List.of(new EventCommentSeparate(eventCommentRepository),
                new EventPhotoSeparate(eventPhotoRepository),
                new EventParticipationSeparate(eventParticipationRepository))
        );
    }
}
