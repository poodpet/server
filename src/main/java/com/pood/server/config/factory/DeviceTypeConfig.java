package com.pood.server.config.factory;

import com.pood.server.api.service.APICall.APICallService;
import com.pood.server.api.service.error.errorProcService;
import com.pood.server.api.service.noti.pushService;
import com.pood.server.api.service.sms.SMSService;
import com.pood.server.api.service.sns.SnsServiceIosImpl;
import com.pood.server.api.service.sns.snsServiceImpl;
import com.pood.server.api.service.user.userNotiService;
import com.pood.server.api.service.user.userService;
import com.pood.server.service.factory.SnsFactory;
import java.util.Arrays;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
@RequiredArgsConstructor
@DependsOn(value = {"userService", "SMSService", "pushService", "userNotiService",
    "errorProcService", "APICallService"})
public class DeviceTypeConfig {

    private final userService userService;
    private final SMSService SMSService;
    private final pushService pushService;
    private final userNotiService userNotiService;
    private final errorProcService errorProcService;
    private final APICallService apiCallService;

    @Bean
    public SnsFactory getSnsFactory() {
        return new SnsFactory(Arrays.asList(
            new snsServiceImpl(userService, SMSService, pushService, userNotiService,
                errorProcService),
            new SnsServiceIosImpl(userService, pushService, userNotiService, errorProcService,
                apiCallService)));
    }
}
