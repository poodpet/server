package com.pood.server.config;

import com.pood.server.entity.user.UserInfo;
import com.pood.server.exception.NotFoundException;
import com.pood.server.service.UserSeparate;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Component
@Aspect
@RequiredArgsConstructor
public class ControllerTokenAspect {

    private final UserSeparate userSeparate;

    @Around("@annotation(UserTokenValid)")
    public Object tokenValidation(ProceedingJoinPoint joinPoint) throws Throwable {
        ServletRequestAttributes requestAttributes =
            (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();

        final String token = request.getHeader("token");
        if (Objects.isNull(token)) {
            throw new NotFoundException("토큰이 유효하지 않습니다.");
        }

        final Object[] args = joinPoint.getArgs();

        final UserInfo userInfo = userSeparate.getOptionalUserByToken(token);

        for (int i = 0; i < args.length; i++) {
            if (args[i] instanceof UserInfo) {
                args[i] = userInfo;
                break;
            }
        }

        return joinPoint.proceed(args);
    }
}
