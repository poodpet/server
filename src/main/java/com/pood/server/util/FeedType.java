package com.pood.server.util;

import com.pood.server.exception.NotFoundException;
import com.pood.server.service.pet_solution.UserPetBaseData;
import java.util.Arrays;

public enum FeedType {
    ALL("M") {
        @Override
        boolean isMonthRange(final UserPetBaseData userPetBaseData) {
            return true;
        }
    },
    PUPPY("P") {
        @Override
        boolean isMonthRange(final UserPetBaseData userPetBaseData) {
            return userPetBaseData.getAgeMonth() < 12;
        }
    },
    ADULT("A") {
        @Override
        boolean isMonthRange(final UserPetBaseData userPetBaseData) {
            return userPetBaseData.getAgeMonth() >= 12 && userPetBaseData.getAgeMonth() < 84;
        }
    },
    SENIOR("S") {
        @Override
        boolean isMonthRange(final UserPetBaseData userPetBaseData) {
            return userPetBaseData.getAgeMonth() > 84;
        }
    },
    PUPPY_ADULT("PA") {
        @Override
        boolean isMonthRange(final UserPetBaseData userPetBaseData) {
            return userPetBaseData.getAgeMonth() < 84;
        }
    },
    PUPPY_ADULT_EXCEPT_SENIOR("PAL") {
        @Override
        boolean isMonthRange(final UserPetBaseData userPetBaseData) {
            return userPetBaseData.getAgeMonth() < 12 && userPetBaseData.isPcBigSize();
        }
    },
    ADULT_SENIOR("AS") {
        @Override
        boolean isMonthRange(final UserPetBaseData userPetBaseData) {
            return userPetBaseData.getAgeMonth() >= 12;
        }
    };

    private final String value;

    FeedType(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static FeedType getFeedTypeByValue(final String value) {
        return Arrays.stream(values())
            .filter(feedType -> feedType.value.equals(value))
            .findFirst()
            .orElseThrow(() -> new NotFoundException(
                String.format("해당하는 펫 타입 [%s]가 없습니다.", value)
            ));
    }

    public static boolean isFlag(final String feedType, final UserPetBaseData userPetBaseData) {
        //퍼피(P)/어덜트(A)/시니어(S)/PA/PAL/AS/전체(M)
        return getFeedTypeByValue(feedType).isMonthRange(userPetBaseData);
    }

    abstract boolean isMonthRange(final UserPetBaseData userPetBaseData);
}
