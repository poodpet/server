package com.pood.server.util;

import com.pood.server.exception.NotFoundException;
import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserStatus {
    ACTIVE(0),
    NON_ACTIVE(1),
    POLICY_VIOLATION(2),
    DORMANT_ACCOUNT(3),

    TO_RESIGN(5),
    RESIGNED(6);

    private final int code;

    public static UserStatus ofLegacyStatus(Integer code) {
        return Arrays.stream(UserStatus.values())
            .filter(value -> value.getCode() == code)
            .findAny()
            .orElseThrow(() -> new NotFoundException(
                String.format("상태코드에 %s가 존재하지 않습니다.", code)));
    }
}
