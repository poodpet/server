package com.pood.server.util;

import com.pood.server.exception.NotFoundException;
import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OrderDeliveryType {

    DEFAULT(10),
    EXCHANGE_PICK_UP(30),
    EXCHANGE_SENDER(31),
    REFUND_PICK_UP(40);

    private final int code;

    public static OrderDeliveryType getOrderDeliveryTypeByCode(final Integer code) {
        return Arrays.stream(values())
            .filter(orderDeliveryType -> orderDeliveryType.code == code)
            .findFirst()
            .orElseThrow(() -> new NotFoundException(
                String.format("해당하는 배송 타입 [%s]가 없습니다.", code)
            ));
    }

}
