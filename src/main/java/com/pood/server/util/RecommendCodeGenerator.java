package com.pood.server.util;

import java.util.concurrent.ThreadLocalRandom;
import lombok.experimental.UtilityClass;

@UtilityClass
public class RecommendCodeGenerator {

    private final int MAX_LENGTH = 6;
    private final int START_STRING = 48;
    private final int END_STRING = 122;

    public String generate() {
        ThreadLocalRandom random = ThreadLocalRandom.current();
        return random.ints(START_STRING, END_STRING + 1)
            .filter(asciiNumber -> (asciiNumber <= 57 || asciiNumber >= 65) && (asciiNumber <= 90 || asciiNumber >= 97))
            .limit(MAX_LENGTH)
            .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
            .toString();
    }
}
