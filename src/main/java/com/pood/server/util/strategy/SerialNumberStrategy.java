package com.pood.server.util.strategy;

import java.util.concurrent.ThreadLocalRandom;

public class SerialNumberStrategy implements NumberStrategy {

    private static final int MIN_VALUE = 100_000;
    private static final int MAX_VALUE = 1_000_000;
    private final ThreadLocalRandom random = ThreadLocalRandom.current();

    @Override
    public int makeNumber() {
        return random.nextInt(MIN_VALUE, MAX_VALUE);
    }
}
