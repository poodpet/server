package com.pood.server.util.strategy;

import java.util.concurrent.ThreadLocalRandom;

public class PercentNumberStrategy implements NumberStrategy {

    private final ThreadLocalRandom random = ThreadLocalRandom.current();

    @Override
    public int makeNumber() {
        return random.nextInt(99) + 1;
    }
}
