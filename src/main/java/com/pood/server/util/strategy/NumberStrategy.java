package com.pood.server.util.strategy;

@FunctionalInterface
public interface NumberStrategy {

    int makeNumber();
}
