package com.pood.server.util.strategy;

@FunctionalInterface
public interface SliceStrategy {

    String sliceNumber();
}
