package com.pood.server.util.strategy;

import java.time.LocalDateTime;

public class YearSliceStrategy implements SliceStrategy {

    @Override
    public String sliceNumber() {
        LocalDateTime now = LocalDateTime.now();
        final int number = now.getYear() % 100;
        if (number < 10) {
            return "0" + number;
        }

        return String.valueOf(number);
    }
}
