package com.pood.server.util;

import com.pood.server.exception.NotFoundException;
import java.util.Arrays;
import lombok.Getter;

@Getter
public enum UserCouponStatus {
    CANNOT_BE_USE(0, "사용 불가"),
    AVAILABLE(1, "사용 가능"),
    USED(2, "사용함");

    private final int value;
    private final String message;

    UserCouponStatus(final int value, final String message) {
        this.value = value;
        this.message = message;
    }

    public static UserCouponStatus ofLegacyType(final int value) {
        return Arrays.stream(UserCouponStatus.values())
            .filter(v -> v.value == value)
            .findFirst()
            .orElseThrow(() -> new NotFoundException(
                String.format("쿠폰 상태 코드 %s가 존재하지 않습니다.", value)));
    }
}
