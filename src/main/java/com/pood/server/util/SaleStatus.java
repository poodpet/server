package com.pood.server.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SaleStatus {

    WAIT(0),
    SALE(1),
    PAUSED(2),
    INTERRUPT(3),
    NOT_TREATED(4);

    private final int status;
}
