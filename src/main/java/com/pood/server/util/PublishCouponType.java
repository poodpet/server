package com.pood.server.util;

public enum PublishCouponType {
    WELCOME(14, "웰컴쿠폰"),
    SINGLE(15, "단품쿠폰"),
    TEST(16, "테스트쿠폰"),
    DELIVERY_FEE(17, "배송비쿠폰"),
    BRAND(18, "브랜드쿠폰"),
    PET_TYPE(19, "펫타입쿠폰"),
    FIRST_PET_REGISTER(20, "첫펫등록쿠폰");

    private final int type;
    private final String text;

    PublishCouponType(final int type, final String text) {
        this.type = type;
        this.text = text;
    }

    public int getType() {
        return type;
    }
}
