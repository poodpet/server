package com.pood.server.util;

import com.pood.server.exception.SmsSendException;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.reactive.function.client.WebClient;

@Slf4j
public final class SmsGenerator {

    public static void findPasswordSmsSend(final String serverUrl, final String phoneNumber) {
        final Optional<Message> responseMessage = WebClient.create(serverUrl).get()
            .uri(uriBuilder ->
                uriBuilder.path("/pood/user/sms/1")
                    .queryParam("phone_number", phoneNumber)
                    .build()
            )
            .retrieve()
            .bodyToMono(Message.class)
            .blockOptional();

        responseMessage.orElseThrow(SmsSendException::new);
    }

    @Getter
    @NoArgsConstructor
    @AllArgsConstructor
    private static final class Message {

        private int status;
        private String msg;

        private boolean isNotOk() {
            return status != 200;
        }
    }

}
