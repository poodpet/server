package com.pood.server.util;

import com.pood.server.entity.log.LogUserSavePoint;
import java.util.List;
import lombok.experimental.UtilityClass;

@UtilityClass
public class PointCalculate {

    public static int totalSavePoint(List<LogUserSavePoint> countPointList) {
        return countPointList.stream()
            .mapToInt(LogUserSavePoint::getSavePoint)
            .sum();
    }

}
