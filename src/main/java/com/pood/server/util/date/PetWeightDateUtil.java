package com.pood.server.util.date;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.util.Pair;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class PetWeightDateUtil {

    private final LocalDate startDate;
    private final LocalDate endDate;

    public static PetWeightDateUtil of(final LocalDate startDate, final LocalDate endDate) {
        return new PetWeightDateUtil(startDate,
            endDate.plusDays(DateUtil.of(endDate).getCompareDayOfWeekNumber(DayOfWeek.FRIDAY)));
    }

    public List<Pair<LocalDate, Integer>> getWeekOfMonth() {
        List<Pair<LocalDate, Integer>> resultList = new ArrayList<>();
        LocalDate copyEndDate = LocalDate.of(endDate.getYear(), endDate.getMonth(),
            endDate.getDayOfMonth());
        while (startDate.isBefore(copyEndDate) || startDate.equals(copyEndDate)) {
            resultList.add(Pair.of(copyEndDate, getWeekNumber(copyEndDate)));
            copyEndDate = copyEndDate.minusDays(7L);
        }
        removeLastWeekLastYear(resultList);
        return resultList;
    }

    private void removeLastWeekLastYear(final List<Pair<LocalDate, Integer>> list) {
        if(!list.get(0).getFirst().getMonth().equals(Month.JANUARY)){
            return;
        }

        if (list.get(0).getSecond().equals(1) && list.size() != 1) {
            list.remove(0);
        }
    }

    private int getWeekNumber(final LocalDate baseDate) {
        LocalDate convertDate = baseDate.with(TemporalAdjusters.firstInMonth(DayOfWeek.FRIDAY));
        if (convertDate.isEqual(baseDate)) {
            return 1;
        }
        if (baseDate.isAfter(convertDate)) {
            int diffFromFirstfriday = baseDate.getDayOfMonth() - convertDate.getDayOfMonth();
            int weekNumber = (int) Math.ceil(diffFromFirstfriday / 7.0);
            if (DayOfWeek.FRIDAY.equals(baseDate.getDayOfWeek())) {
                weekNumber += 1;
            }
            return weekNumber;
        }
        return getWeekNumber(baseDate.minusMonths(1).with(TemporalAdjusters.lastDayOfMonth()));
    }

}
