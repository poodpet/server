package com.pood.server.util.date;

import java.time.Clock;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class DateUtil  {

    private final Clock clock;
    private final LocalDate date;

    public static DateUtil of(final LocalDate date) {
        return new DateUtil(Clock.systemDefaultZone(), date);
    }

    public static DateUtil of(final Clock clock, final LocalDate date) {
        return new DateUtil(clock, date);
    }
    public int getCompareDayOfWeekNumber(final DayOfWeek dayOfWeek) {
        return dayOfWeek.getValue() - date.getDayOfWeek().getValue();
    }

    public LocalDate convertWeeklyFridayDate() {
        return date.plusDays(this.getCompareDayOfWeekNumber(DayOfWeek.FRIDAY));
    }

    public int checkWeekAgoFromToday() {

        final LocalDate now = DateUtil.of(LocalDate.now(clock)).convertWeeklyFridayDate();

        if (Objects.isNull(date)) {
            return 0;
        }

        return (int) ChronoUnit.WEEKS.between(date, now);
    }
}
