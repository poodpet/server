package com.pood.server.util;

import com.pood.server.exception.NotFoundException;
import java.util.Arrays;
import lombok.Getter;

@Getter
public enum DeliveryRemoteType {

    NORMAL(0, "도서 산간 아님"),
    JEJU_NORMAL(1, "제주도 기본"),
    JEJU_MOUNTAIN(2, "제주도 산간"),
    ANOTHER_ISLAND(3, "기타 섬 지역"),
    MOUNTAIN(4, "산간");

    private final int value;
    private final String text;

    DeliveryRemoteType(final int value, final String text) {
        this.value = value;
        this.text = text;
    }

    public static DeliveryRemoteType ofLegacyType(int value) {
        return Arrays.stream(values())
            .filter(deliveryRemoteType -> deliveryRemoteType.value == value)
            .findFirst()
            .orElseThrow(() -> new NotFoundException(
                String.format("해당하는 배송지역 타입 [%s]가 없습니다.", value)
            ));
    }
}
