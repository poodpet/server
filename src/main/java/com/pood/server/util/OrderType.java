package com.pood.server.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OrderType {
    PAYMENT_COMPLETE(1, "결제 완료"),

    ORDER_READY(0, "주문 접수"),
    ORDER_CANCEL_APPLY(2, "주문 취소 신청"),
    ORDER_CANCEL_COMPLETE(3, "주문 취소 완료"),

    DELIVERY_READY(10, "배송 준비"),
    DELIVERY_SHIPPING(11, "배송중"),
    DELIVERY_COMPLETE(12, "배송 완료"),
    DELIVERY_DELAY(13, "배송 지연"),
    DELIVERY_IMPOSSIBLE(30, "배송불가"),

    EXCHANGE_APPLY(14, "교환 신청"),
    EXCHANGE_APPROVE(18, "교환 승인"),
    EXCHANGE_REFUSE(19, "교환 거부"),
    EXCHANGE_CANCEL(26, "교환 철회"),

    RETURN_READY(29, "반품접수"),
    RETURN_APPLY(16, "반품 신청"),
    RETURN_APPROVE(20, "반품 승인"),
    RETURN_REFUSE(21, "반품 거부"),
    RETURN_CANCEL(27, "반품철회"),

    EXCHANGE_READY(28, "교환접수"),
    EXCHANGE_DELIVERY_READY(22, "교환 배송 준비"),
    EXCHANGE_DELIVERY_SHIPPING(23, "교환 배송중"),
    EXCHANGE_DELIVERY_COMPLETE(24, "교환 배송 완료"),
    EXCHANGE_DELIVERY_DELAY(25, "교환 배송 지연"),
    EXCHANGE_DELIVERY_IMPOSSIBLE(31, "교환배송불가"),

    WAITING_FOR_SHIPMENT(4, "출고대기");

    private final int value;
    private final String statusText;
}
