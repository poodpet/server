package com.pood.server.util;

import com.pood.server.exception.NotFoundException;
import java.util.Arrays;

public enum RestockState {

    WAIT,
    DONE;

    public static RestockState find(final String dbData) {
        return Arrays.stream(RestockState.values())
            .filter(restockState -> restockState.name().equals(dbData))
            .findFirst().orElseThrow(
                () -> new NotFoundException(dbData + "는 재입고 요청 데이터에 잘못된 상태입니다.")
            );
    }

}
