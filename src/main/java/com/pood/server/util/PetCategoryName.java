package com.pood.server.util;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum PetCategoryName {
    PUPPY("어린 강아지", "퍼피"), KITTEN("어린 고양이", "키튼"),
    ADULT_DOG("성견", "어덜트"), ADULT_CAT("성묘", "어덜트"),
    OLD_DOG("노령견", "시니어"), OLD_CAT("노령묘", "시니어");
    private final String name;
    private final String age;

    public static PetCategoryName petGrowthState(final long month, final boolean isDog) {
        if (month < 12L && isDog) {
            return PetCategoryName.PUPPY;
        }

        if (month < 12L) {
            return PetCategoryName.KITTEN;
        }

        if (month < 84L && isDog) {
            return PetCategoryName.ADULT_DOG;
        }

        if (month < 84L) {
            return PetCategoryName.ADULT_CAT;
        }

        if (isDog) {
            return PetCategoryName.OLD_DOG;
        }

        return PetCategoryName.OLD_CAT;
    }
}
