package com.pood.server.util;

public enum AnimalSize {
    // 전체, 소형, 중형, 대형
    ALL(0),
    PUPPY(1),
    ADULT(2),
    SENIOR(3);

    private final int value;

    AnimalSize(final int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
