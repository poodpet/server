package com.pood.server.util;

public enum MarketingType {
    EVENT,
    PROMOTION,
    DONATION
}
