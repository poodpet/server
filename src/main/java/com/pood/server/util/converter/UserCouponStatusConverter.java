package com.pood.server.util.converter;

import com.pood.server.util.UserCouponStatus;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class UserCouponStatusConverter implements AttributeConverter<UserCouponStatus, Integer> {

    @Override
    public Integer convertToDatabaseColumn(final UserCouponStatus attribute) {
        return attribute.getValue();
    }

    @Override
    public UserCouponStatus convertToEntityAttribute(final Integer value) {
        return UserCouponStatus.ofLegacyType(value);
    }
}
