package com.pood.server.util.converter;

import com.pood.server.util.RestockState;
import java.util.Objects;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class RestockStateConverter implements AttributeConverter<RestockState, String> {

    @Override
    public String convertToDatabaseColumn(final RestockState attribute) {

        if (Objects.isNull(attribute)) {
            return null;
        }

        return attribute.name();
    }

    @Override
    public RestockState convertToEntityAttribute(final String dbData) {
        return RestockState.find(dbData);
    }
}
