package com.pood.server.util.converter;

import com.pood.server.util.DeliveryRemoteType;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class DeliveryRemoteConverter implements AttributeConverter<DeliveryRemoteType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(final DeliveryRemoteType attribute) {
        return attribute.getValue();
    }

    @Override
    public DeliveryRemoteType convertToEntityAttribute(final Integer value) {
        return DeliveryRemoteType.ofLegacyType(value);
    }
}
