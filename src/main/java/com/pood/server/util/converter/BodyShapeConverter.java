package com.pood.server.util.converter;

import com.pood.server.entity.user.BodyShape;
import java.util.Objects;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class BodyShapeConverter implements AttributeConverter<BodyShape, String> {

    @Override
    public String convertToDatabaseColumn(final BodyShape bodyShape) {
        if (Objects.isNull(bodyShape)) {
            return null;
        }

        return bodyShape.getType();
    }

    @Override
    public BodyShape convertToEntityAttribute(final String dbData) {

        return BodyShape.findType(dbData);
    }
}
