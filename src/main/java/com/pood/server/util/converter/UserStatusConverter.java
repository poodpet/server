package com.pood.server.util.converter;

import com.pood.server.util.UserStatus;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class UserStatusConverter implements AttributeConverter<UserStatus, Integer> {

    @Override
    public Integer convertToDatabaseColumn(final UserStatus attribute) {
        return attribute.getCode();
    }

    @Override
    public UserStatus convertToEntityAttribute(final Integer code) {
        return UserStatus.ofLegacyStatus(code);
    }
}
