package com.pood.server.util.converter;

import com.pood.server.entity.meta.Product.PackageType;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class PackageTypeConverter implements AttributeConverter<PackageType, String> {

    @Override
    public String convertToDatabaseColumn(final PackageType attribute) {
        return attribute.getType();
    }

    @Override
    public PackageType convertToEntityAttribute(final String dbData) {
        return PackageType.ofLegacyType(dbData);
    }
}
