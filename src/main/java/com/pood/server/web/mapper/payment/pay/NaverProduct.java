package com.pood.server.web.mapper.payment.pay;


import lombok.Getter;


@Getter
public class NaverProduct {

    private String categoryType;

    private String categoryId;

    private int uid;

    private String name;

    private int count;

    public NaverProduct(String categoryType, String categoryId, int uid, String name, int count) {
        this.categoryType = categoryType;
        this.categoryId = categoryId;
        this.uid = uid;
        this.name = name;
        this.count = count;
    }
}
