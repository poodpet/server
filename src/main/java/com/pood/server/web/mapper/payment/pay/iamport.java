package com.pood.server.web.mapper.payment.pay;

import com.pood.server.api.service.delivery.remoteDeliveryService;
import com.pood.server.api.service.meta.order.OrderService;
import com.pood.server.api.service.pay.checkOrderService;
import com.pood.server.api.service.pay.initPayService;
import com.pood.server.api.service.pay.payResultService;
import com.pood.server.config.PAYMENT;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import com.pood.server.facade.OrderFacade;
import com.pood.server.object.IMP.IMP;
import com.pood.server.object.IMP.IMP_RESULT;
import com.pood.server.object.resp.resp_pay_success;
import com.pood.server.service.OrderBasketSeparate;
import java.util.Base64;
import javax.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
public class iamport extends PROTOCOL {

    Logger logger = LoggerFactory.getLogger(iamport.class);

    @Value("${legacy.iamport-url}")
    private String iamportUrl;

    private static final String NAVER_PAY = "naverpay";

    private final payResultService payResultService;

    private final checkOrderService checkOrderService;

    private final OrderService orderService;

    private final remoteDeliveryService remoteDeliveryService;

    private final initPayService initPayService;

    private final OrderBasketSeparate orderBasketSeparate;

    private final OrderFacade orderFacade;



    @RequestMapping(value = "/pood/iamport/1", method = RequestMethod.POST)
    public String payment(@RequestParam String param, HttpServletRequest request, Model model)
        throws Exception {

        procInit(request);

        logger.info(param);

        Base64.Decoder decoder = Base64.getDecoder();
        byte[] decodedBytes = decoder.decode(param);

        logger.info(param);

        String tempStr = new String(decodedBytes);
        tempStr = tempStr.replaceAll("%21", "!")
            .replaceAll("%23", "#")
            .replaceAll("%25", "%")
            .replaceAll("%26", "&")
            .replaceAll("%28", "\\(")
            .replaceAll("%29", "\\)")
            .replaceAll("%2B", "\\+")
            .replaceAll("%5C", "\\\\")
            .replaceAll("%2C", ",")
            .replaceAll("%2E", "\\.")
            .replaceAll("%2F", "/")
            .replaceAll("%3A", ":")
            .replaceAll("%3B", ";")
            .replaceAll("%3C", "<")
            .replaceAll("%3E", ">")
            .replaceAll("%3D", "=")
            .replaceAll("%40", "@")
            .replaceAll("%7D", "}")
            .replaceAll("%5D", "]")
            .replaceAll("%60", "`")
            .replaceAll("%7E", "~")
            .replaceAll("%3F", "\\?")
            .replaceAll("%7B", "\\{")
            .replaceAll("%5B", "\\[")
            .replaceAll("%7C", "\\|");
        logger.info(tempStr);

        IMP IAMPORT = initPayService.initIMP(tempStr);

        String USER_UUID = IAMPORT.USER_UUID;

        String TOKEN = IAMPORT.USER_TOKEN;

        Integer STATUS_CODE = initPayService.tokenCheck(TOKEN, USER_UUID);

        if (STATUS_CODE != 200) {
            response = getResponse(STATUS_CODE);
            procClose(request, response.toString());
            return "iamport/fail.html";
        }

        if (PROTOCOL_IDENTIFIER.PROTOCOL_PAYMENT_IAMPORT_1) {

            IMP_RESULT imp_result = new IMP_RESULT();
            imp_result.setBUYER_EMAIL(IAMPORT.PAYMENT_BUYER_EMAIL);
            imp_result.setBUYER_NAME(IAMPORT.PAYMENT_BUYER_NAME);
            imp_result.setORDER_PRICE(IAMPORT.PAYMENT_ORDER_PRICE);
            imp_result.setPG(IAMPORT.PAYMENT_PG);
            imp_result.setPAY_METHOD(IAMPORT.PAYMENT_PAY_METHOD);
            imp_result.setORDER_NAME(IAMPORT.PAYMENT_ORDER_NAME);
            imp_result.setIMP_UID(PAYMENT.IMP_UID);
            imp_result.setMERCHANT_UID(IAMPORT.MERCHANT_UID);

            resp_pay_success result = checkOrderService.getOrderCheck(IAMPORT);

            imp_result.setERROR_MESSAGE(result.getMsg());
            imp_result.setIMP_SUCCESS(result.getIsSuccess());

            // 주문이 유효한 주문인 경우 주문 레코드 삽입
            if (imp_result.getIMP_SUCCESS()) {
                Integer status = initPayService.insertIMPorder(IAMPORT);

                if (status == -1) {
                    procClose(request, null);
                    return "iamport/fail.html";
                }

                status = null;
            }

            model.addAttribute("imp_uid", com.pood.server.config.host.IAMPORT.IMP_UID);
            model.addAttribute("merchant_uid", imp_result.getMERCHANT_UID());
            model.addAttribute("imp_success", imp_result.getIMP_SUCCESS());
            model.addAttribute("error_msg", imp_result.getERROR_MESSAGE());
            model.addAttribute("buyer_email", imp_result.getBUYER_EMAIL());
            model.addAttribute("buyer_name", imp_result.getBUYER_NAME());
            model.addAttribute("pg", imp_result.getPG());
            model.addAttribute("pay_method", imp_result.getPAY_METHOD());
            model.addAttribute("name", imp_result.getORDER_NAME());
            model.addAttribute("order_price", imp_result.getORDER_PRICE());
            model.addAttribute("url", iamportUrl);
            if (NAVER_PAY.equals(imp_result.getPG())) {
                model.addAttribute("naverProducts",  orderFacade.getIamportNaverProducts(imp_result.getMERCHANT_UID()));
            }

            logger.info("*********** ORDER RESULT ***********");
            logger.info("URL : " + iamportUrl);
            logger.info(result.toString());

            if (!result.getIsSuccess()) {
                procClose(request, null);
                return "iamport/fail.html";
            }

        } else {
            response = getResponse(219);
        }

        procClose(request, null);

        return "iamport/index.html";
    }
}
