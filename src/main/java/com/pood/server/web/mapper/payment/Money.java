package com.pood.server.web.mapper.payment;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Objects;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Money {

    private static final RoundingMode DEFAULT_ROUND_MODE = RoundingMode.HALF_UP;
    private BigDecimal amount;


    public Money(final int amount) {
        this.amount = new BigDecimal(amount, new MathContext(5, DEFAULT_ROUND_MODE));
    }

    public Money(final long amount) {
        this.amount = new BigDecimal(amount, new MathContext(5));
    }

    public Money(final BigDecimal amount) {
        this.amount = amount;
    }

    public Money copy() {
        return new Money(this.amount);
    }

    public Money minus(final Money money) {
        return new Money(this.amount.subtract(money.amount));
    }

    public Money plus(final Money money) {
        return new Money(this.amount.add(money.amount));
    }

    public Money multiply(final BigDecimal value) {
        return new Money(this.amount.multiply(value));
    }

    public Money divide(final BigDecimal value) {
        return new Money(this.amount.divide(value, new MathContext(5, DEFAULT_ROUND_MODE)));
    }

    public Money ratio(final Double retreieveRatio) {
        return new Money(amount.multiply(BigDecimal.valueOf(retreieveRatio),
            new MathContext(5, DEFAULT_ROUND_MODE)));
    }

    public long getLongValue() {
        return amount.longValue();
    }

    public double getDoubleValue() {
        return amount.doubleValue();
    }

    public int getIntValue() {
        return amount.intValue();
    }

    public boolean isLessThanOne() {
        return this.amount.longValue() < 1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Money money = (Money) o;
        return Objects.equals(amount, money.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount);
    }
}
