package com.pood.server.web.mapper.payment.pay_cancel;

import com.pood.server.api.req.header.pay.OrderCancelRequest;
import com.pood.server.api.req.header.pay.hPay_cancel_1_2;
import com.pood.server.api.service.iamport.resParserService;
import com.pood.server.api.service.log.logUserOrderService;
import com.pood.server.api.service.meta.order.orderBasketService;
import com.pood.server.api.service.meta.order.orderCancelService;
import com.pood.server.api.service.pay.retreieveService;
import com.pood.server.api.service.sms.SMSService;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import com.pood.server.dto.meta.order.dto_order;
import com.pood.server.entity.order.dto.OrderBaseDataDto;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.OrderFacade;
import com.pood.server.facade.SnsFacade;
import com.pood.server.object.req_retreieve;
import com.pood.server.service.UserSeparate;
import com.pood.server.service.factory.OrderCancelFactory;
import com.pood.server.service.factory.OrderCancelPriceInfo;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class cancel_1_1 extends PROTOCOL {

    Logger logger = LoggerFactory.getLogger(cancel_1_1.class);

    @Value("${legacy.order-cancel-work}")
    private boolean isOrderCancelWork;

    @Autowired
    @Qualifier("orderCancelService")
    orderCancelService orderCancelService;

    @Autowired
    @Qualifier("orderBasketService")
    orderBasketService orderBasketService;

    @Autowired
    @Qualifier("logUserOrderService")
    logUserOrderService logUserOrderService;

    @Autowired
    @Qualifier("resParserService")
    resParserService resParserService;

    @Autowired
    @Qualifier("retreieveService")
    retreieveService retreieveService;

    @Autowired
    OrderFacade orderFacade;

    @Autowired
    SnsFacade snsFacade;

    @Autowired
    SMSService smsService;

    @Autowired
    UserSeparate userSeparate;

    @ResponseBody
    @RequestMapping(value = "/pood/payment/cancel/1/1", method = RequestMethod.POST)
    public HashMap<String, Object> PAMENT_CANCEL(
        @RequestBody OrderCancelRequest orderCancelRequest,
        HttpServletRequest request, Model model)
        throws Exception {

        Integer resp_status = procInit(request);

        // 인증토큰에 대해서 유효성 검증에 실패한 경우
        if (resp_status != 200) {
            procClose(request, response.toString());
            return response;
        }

        if (PROTOCOL_IDENTIFIER.PROTOCOL_PAYMENT_CANCEL_1_1) {

            boolean isRefund = orderCancelRequest.isRefund();
            String merchantUid = orderCancelRequest.getMerchant_uid();
            String requestText = orderCancelRequest.getRequest_text();

            if (!orderCancelRequest.isRefund()) {
                if (isOrderCancelWork) {
                    Boolean timeLimit = orderService.checkRetreiveTimeLimit(merchantUid);
                    if (Boolean.FALSE.equals(timeLimit)) {
                        response = getResponse(244);
                        procClose(request, response.toString());
                        return response;
                    }
                }
            }

            logger.info(
                "MERCHANT UID : " + merchantUid +
                    ", requestText : " + requestText +
                    ", isRefund : " + isRefund);

            dto_order orderDto = orderService.getOrder(merchantUid);

            if (Objects.nonNull(orderDto)) {

                int retreiveStatus = 0;
                List<hPay_cancel_1_2> cancelList = orderCancelRequest.getData();

                final List<Integer> goodsIdxList = cancelList.stream()
                    .map(hPay_cancel_1_2::getGoods_idx)
                    .collect(Collectors.toList());
                final UserInfo userInfo = userSeparate.getUserByToken(
                    (String) request.getHeader("token"));

                final OrderBaseDataDto orderbaseData = orderFacade.getOrderInfoByOrderNumberAndUserUUID(
                    merchantUid, userInfo.getUserUuid());
                long completePoint = orderbaseData.refundCompletePoint();
                try {
                    for (hPay_cancel_1_2 cancelDate : cancelList) {
                        OrderCancelPriceInfo orderCancelPriceInfo = new OrderCancelFactory()
                            .create(orderbaseData, goodsIdxList, cancelDate.getGoods_idx());

                        Integer cancelGoodsPrice = orderBasketService.getPrice(merchantUid,
                            cancelDate.getGoods_idx());

                        req_retreieve retreieveDto = req_retreieve.createRetreieve(
                            orderCancelRequest, cancelDate, cancelGoodsPrice
                        );

                        boolean isFinal = orderBasketService.isFinalRetreieve(
                            retreieveDto.getMerchant_uid(), retreieveDto.getCancel_goods_idx());
                        orderCancelRequest.setFianlCancle(isFinal);
                        retreiveStatus = retreieveService.doRetreieve(orderCancelRequest,
                            cancelDate, cancelGoodsPrice, orderCancelPriceInfo, completePoint);
                        completePoint += orderCancelPriceInfo.getRetreievePoint();
                        response = getResponse(retreiveStatus);


                    }

                    if (!isRefund && retreiveStatus == 200) {
                        String goodsCancelMsg = orderFacade.getGoodsCancelMsg(orderDto, cancelList);
                        snsFacade.sendOrderCancel(userInfo, orderDto, goodsCancelMsg);
                        smsService.send(userInfo.getUserPhone(), goodsCancelMsg);
                    }
                } catch (IllegalArgumentException e) {
                    response = getResponse(257);
                }
            } else {
                response = getResponse(247);
            }
        } else {
            response = getResponse(219);
        }
        procClose(request, response.toString());
        return response;

    }

}