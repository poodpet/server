package com.pood.server.web.mapper.payment.pay;

import com.pood.server.api.service.delivery.remoteDeliveryService;
import com.pood.server.api.service.error.errorOrderFailService;
import com.pood.server.api.service.meta.order.OrderService;
import com.pood.server.api.service.pay.checkOrderService;
import com.pood.server.api.service.pay.initPayService;
import com.pood.server.api.service.pay.payResultService;
import com.pood.server.config.PAYMENT;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import com.pood.server.object.IMP.IMP;
import com.pood.server.object.IMP.IMP_RESULT;
import com.pood.server.object.resp.resp_pay_success;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class iamport_1 extends PROTOCOL {

    Logger logger = LoggerFactory.getLogger(iamport_1.class);

    @Value("${legacy.iamport-url}")
    private String iamportUrl;

    @Autowired
    @Qualifier("payResultService")
    public payResultService payResultService;


    @Autowired
    @Qualifier("orderService")
    public OrderService orderService;

    @Autowired
    @Qualifier("checkOrderService")
    checkOrderService checkOrderService;
    


    @Autowired
    @Qualifier("remoteDeliveryService")
    public remoteDeliveryService remoteDeliveryService;




    @Autowired
    @Qualifier("errorOrderFailService")
    errorOrderFailService errorOrderFailService;
  


    @Autowired
    @Qualifier("initPayService")
    initPayService initPayService;



    @RequestMapping(value = "/pood/iamport/1/2", method = RequestMethod.GET)
    public String real_payment(
        @RequestParam(value="user_uuid") String user_uuid,
        @RequestParam(value="used_point") Integer used_point,
        @RequestParam(value="uc_idx") Integer uc_idx,
        @RequestParam(value="qty") Integer qty,
        @RequestParam(value="qty_2", required=false) Integer qty_2,
        @RequestParam(value="goods_price") Integer goods_price,
        @RequestParam(value="goods_price_2", required=false) Integer goods_price_2,
        @RequestParam(value="discount_price") Integer discount_price,
        @RequestParam(value="delivery_fee")Integer delivery_fee,
        @RequestParam(value="zipcode")String zipcode,
        @RequestParam(value="remote_type")Integer remote_type,
        @RequestParam(value="pay")Boolean isPay,
        HttpServletRequest request, Model model)
            throws Exception {
      
        Integer total_price = (goods_price * qty) + (goods_price_2 * qty_2);

        Integer order_price = total_price - discount_price + delivery_fee;

        String param = returnParam(
                order_price,        // 주문 금액
                user_uuid,          // 회원 UUID
                uc_idx,             // 회원 쿠폰 IDX
                used_point,         // 소진 적립금
                qty,                // 굿즈 구매 수량
                total_price,        // 굿즈 총 가격
                discount_price,     // 총 할인 금액
                delivery_fee,       // 배송비
                remote_type,        // 배송지 타입
                zipcode,            // 우편 번호
                goods_price,        // 굿즈 가격
                goods_price_2,      // 두번째 굿즈 가격
                qty_2               // 두번째 굿즈 수량
                );
                
        procInit(request);

        logger.info(param);
            
        response = getResponse(200);

        if(PROTOCOL_IDENTIFIER.PROTOCOL_PAYMENT_IAMPORT_1){    

            IMP_RESULT result = setIMP(param);
            
            model = setModel(model, result);

            if(!result.getIMP_SUCCESS()){
                logger.info("결제 요청 실패하였습니다. ");
                procClose(request, "");
                return "iamport/fail.html";
            }

            result = null;

        }else response = getResponse(219);

        procClose(request, null);

        if(isPay)
            return "iamport/index.html";

        return "iamport/index3.html";
    }

    @RequestMapping(value = "/pood/iamport/1/2/1", method = RequestMethod.GET)
    public String real_payment_2(
        @RequestParam(value="user_uuid") String user_uuid,
        @RequestParam(value="used_point") Integer used_point,
        @RequestParam(value="uc_idx") Integer uc_idx,
        @RequestParam(value="qty") Integer qty,
        @RequestParam(value="goods_price") Integer goods_price,
        @RequestParam(value="discount_price") Integer discount_price,
        @RequestParam(value="delivery_fee")Integer delivery_fee,
        @RequestParam(value="zipcode")String zipcode,
        @RequestParam(value="remote_type")Integer remote_type,
        @RequestParam(value="pay")Boolean isPay,
        HttpServletRequest request, Model model)
            throws Exception {
      
        Integer total_price = goods_price * qty;

        Integer order_price = total_price - discount_price + delivery_fee;

        String param = returnParam2(
                order_price,        // 주문 금액
                user_uuid,          // 회원 UUID
                uc_idx,             // 회원 쿠폰 IDX
                used_point,         // 소진 적립금
                qty,                // 굿즈 구매 수량
                total_price,        // 굿즈 총 가격
                discount_price,     // 총 할인 금액
                delivery_fee,       // 배송비
                remote_type,        // 배송지 타입
                zipcode,            // 우편 번호
                goods_price         // 굿즈 가격
                );
                
        procInit(request);

        logger.info(param);
            
        response = getResponse(200);

        if(PROTOCOL_IDENTIFIER.PROTOCOL_PAYMENT_IAMPORT_1){    

            IMP_RESULT result = setIMP(param);
            
            model = setModel(model, result);

            if(!result.getIMP_SUCCESS()){
                logger.info("결제 요청 실패하였습니다. ");
                procClose(request, "");
                return "iamport/fail.html";
            }

            result = null;

        }else response = getResponse(219);

        procClose(request, null);

        if(isPay)
            return "iamport/index.html";

        return "iamport/index3.html";
    }



    public String returnParam(
        Integer order_price,        // 주문 금액
        String  user_uuid,          // 회원 UUID
        Integer uc_idx,             // 회원 쿠폰 IDX
        Integer used_point,         // 소진 적립금
        Integer qty,                // 굿즈 구매 수량
        Integer total_price,        // 굿즈 총 가격
        Integer discount_price,     // 총 할인 금액
        Integer delivery_fee,       // 배송비
        Integer remote_type,        // 배송지 타입
        String  zipcode,            // 우편 번호
        Integer goods_price,        // 굿즈 가격
        Integer goods_price_2,      // 굿즈 가격
        Integer qty_2               // 굿즈 가격
        ){
        return "{\"payment\":{\"buyer_email\":\"jasonyeong790@pood.pet\",\"buyer_name\":\"jaso***\",\"order_price\":" + order_price+ ",\""
            + "pg\":\"INIBillTst\",\"pay_method\":\"card\",\"order_name\":\"지위픽 메인 굿즈 007 한번만 환불불가 외 0\",\"app_scheme\":\"poodpet\"},\"user\":{\"user_uuid\":\"" 
            +user_uuid+ "\",\"ml_name\":\"WELCOME\",\"ml_rate\":0.02,\"delivery\":{\"user_uuid\":\"" 
            + user_uuid+ "\",\"default_type\":1,\"address\":\"서울 당산로 SKV1 센터\",\"detail_address\":\"1706호\",\"name\":\"이름\",\"nickname\":\"4\",\"zipcode\":\""+zipcode+ "\""
            + ",\"phone_number\":\"01012345678\",\"input_type\":0,\"remote_type\":" +remote_type+ "}},\"order\":{\"order_device\":\"aos\",\"memo\":\"h\",\""
            + "total_price\":" +total_price+ ",\"total_discount_price\":" +discount_price+ ",\"used_point\":" + used_point+ ",\"free_purchase\":0,\"delivery_fee\":" + delivery_fee +",\""
            + "coupon_idx\":" +uc_idx+ ",\"shoppingbag_data\":[{\"idx\":252,\"pr_code\":-1,\"coupon_idx\":-1,\"goods_idx\":505,\""
            + "goods_price\":" +goods_price+ ",\"qty\":" +qty+ ",\"limit_quantity\":1,\"seller_idx\":29},{\"idx\":253,\"pr_code\":-1,\"coupon_idx\":-1,\"goods_idx\":695,\""
            + "goods_price\":" +goods_price_2+ ", \"qty\":" +qty_2+ ", \"limit_quantity\":1,\"seller_idx\":29}]}}";
    }

    public String returnParam2(
        Integer order_price,        // 주문 금액
        String  user_uuid,          // 회원 UUID
        Integer uc_idx,             // 회원 쿠폰 IDX
        Integer used_point,         // 소진 적립금
        Integer qty,                // 굿즈 구매 수량
        Integer total_price,        // 굿즈 총 가격
        Integer discount_price,     // 총 할인 금액
        Integer delivery_fee,       // 배송비
        Integer remote_type,        // 배송지 타입
        String  zipcode,            // 우편 번호
        Integer goods_price        // 굿즈 가격
        ){
        return "{\"payment\":{\"buyer_email\":\"jasonyeong790@pood.pet\",\"buyer_name\":\"jaso***\",\"order_price\":" + order_price+ ",\""
            + "pg\":\"INIBillTst\",\"pay_method\":\"card\",\"order_name\":\"지위픽 메인 굿즈 007 한번만 환불불가 외 0\",\"app_scheme\":\"poodpet\"},\"user\":{\"user_uuid\":\"" 
            +user_uuid+ "\",\"ml_name\":\"WELCOME\",\"ml_rate\":0.02,\"delivery\":{\"user_uuid\":\"" 
            + user_uuid+ "\",\"default_type\":1,\"address\":\"서울 당산로 SKV1 센터\",\"detail_address\":\"1706호\",\"name\":\"이름\",\"nickname\":\"4\",\"zipcode\":\""+zipcode+ "\""
            + ",\"phone_number\":\"01012345678\",\"input_type\":0,\"remote_type\":" +remote_type+ "}},\"order\":{\"order_device\":\"aos\",\"memo\":\"h\",\""
            + "total_price\":" +total_price+ ",\"total_discount_price\":" +discount_price+ ",\"used_point\":" + used_point+ ",\"free_purchase\":0,\"delivery_fee\":" + delivery_fee +",\""
            + "coupon_idx\":" +uc_idx+ ",\"shoppingbag_data\":[{\"idx\":252,\"pr_code\":-1,\"coupon_idx\":-1,\"goods_idx\":505,\""
            + "goods_price\":" +goods_price+ ",\"qty\":" +qty+ ",\"limit_quantity\":1,\"seller_idx\":29}]}}";
    }


    public IMP_RESULT setIMP(String param) throws Exception{
        IMP IAMPORT = new IMP();
        IAMPORT.setIMP(param);
        IAMPORT.setUser_idx(userService.getUserIDX(IAMPORT.USER_UUID));
        IAMPORT.SET_MERCHANT_UID(orderService.generateMerchantUID(IAMPORT.ORDER_DEVICE));

        IMP_RESULT imp_result = new IMP_RESULT();
        imp_result.setBUYER_EMAIL(IAMPORT.PAYMENT_BUYER_EMAIL);
        imp_result.setBUYER_NAME(IAMPORT.PAYMENT_BUYER_NAME);
        imp_result.setORDER_PRICE(IAMPORT.PAYMENT_ORDER_PRICE);
        imp_result.setPG(IAMPORT.PAYMENT_PG);
        imp_result.setPAY_METHOD(IAMPORT.PAYMENT_PAY_METHOD);
        imp_result.setORDER_NAME(IAMPORT.PAYMENT_ORDER_NAME);
        imp_result.setIMP_UID(PAYMENT.IMP_UID);
        imp_result.setMERCHANT_UID(IAMPORT.MERCHANT_UID);

        resp_pay_success result = checkOrderService.getOrderCheck(IAMPORT);

        imp_result.setERROR_MESSAGE(result.getMsg()); 
        imp_result.setIMP_SUCCESS(result.getIsSuccess());

        logger.info(imp_result.toString());
        

        
        // 주문이 유효한 주문인 경우 주문 레코드 삽입 
         if(result.getIsSuccess()){
             Integer status = initPayService.insertIMPorder(IAMPORT);
             if(status == -1){
                imp_result.setIMP_SUCCESS(false);
                imp_result.setERROR_MESSAGE("결제 실패.");
                errorOrderFailService.insertRecord(IAMPORT.MERCHANT_UID, "", "주문 레코드 삽입 실패", 0);
             }
             status = null;
         }
         
        return imp_result;
    }



    public Model setModel(Model model, IMP_RESULT result){
        model.addAttribute("imp_uid", com.pood.server.config.host.IAMPORT.IMP_UID);
        model.addAttribute("merchant_uid", result.getMERCHANT_UID());
        model.addAttribute("imp_success", result.getIMP_SUCCESS());
        model.addAttribute("error_msg", result.getERROR_MESSAGE());
        model.addAttribute("buyer_email", result.getBUYER_EMAIL());
        model.addAttribute("buyer_name", result.getBUYER_NAME());
        model.addAttribute("pg", result.getPG());
        model.addAttribute("pay_method", result.getPAY_METHOD());
        model.addAttribute("name", result.getORDER_NAME());
        model.addAttribute("order_price", result.getORDER_PRICE());
        model.addAttribute("url", iamportUrl);
        return model;
    }


}
