package com.pood.server.web.mapper.payment.pay;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pood.server.api.service.error.errorOrderFailService;
import com.pood.server.api.service.meta.coupon.couponService;
import com.pood.server.api.service.meta.order.OrderService;
import com.pood.server.api.service.pay.payResultService;
import com.pood.server.api.service.user.userBasketService;
import com.pood.server.api.service.user.userCouponService;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import com.pood.server.entity.iamport.IamportPaymentInfo;
import com.pood.server.entity.meta.dto.OrderInfoDto;
import com.pood.server.exception.PaymentAmountCheckFailException;
import com.pood.server.exception.SearchPaymentInfoFailException;
import com.pood.server.object.IMP.PAYMENT_RESULT;
import com.pood.server.object.resp.resp_pay_result;
import com.pood.server.service.OrderBasketSeparate;
import com.pood.server.service.OrderServiceV2;
import com.pood.server.service.aligo.KakaoApi;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class result extends PROTOCOL {

    Logger logger = LoggerFactory.getLogger(result.class);

    @Value("${aligo.active}")
    Boolean isAligoKakaoActive;

    @Autowired
    @Qualifier("couponService")
    public couponService couponService;

    @Autowired
    @Qualifier("orderService")
    public OrderService orderService;

    @Autowired
    @Qualifier("userBasketService")
    public userBasketService userBasketService;

    @Autowired
    @Qualifier("userCouponService")
    public userCouponService userCouponService;

    @Autowired
    @Qualifier("payResultService")
    payResultService payResultService;

    @Autowired
    @Qualifier("errorOrderFailService")
    errorOrderFailService errorOrderFailService;

    @Autowired
    @Qualifier("iamportService")
    com.pood.server.api.service.iamport.iamportService iamportService;

    @Autowired
    OrderBasketSeparate orderBasketSeparate;

    @Autowired
    OrderServiceV2 orderServiceV2;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    KakaoApi kakaoApi;


    @ResponseBody
    @RequestMapping(value = "/pood/iamport/result/1", method = RequestMethod.GET)
    public PAYMENT_RESULT payment_result(HttpServletRequest request,
        @RequestParam(value = "imp_uid", required = false) String imp_uid,
        @RequestParam(value = "merchant_uid", required = false) String MERCHANT_UID,
        @RequestParam(value = "imp_success", required = false) Boolean imp_success,
        @RequestParam(value = "error_msg", required = false) String error_msg, Model model)
        throws Exception {

        PAYMENT_RESULT result = new PAYMENT_RESULT();
        result.setImp_uid(imp_uid);
        result.setMerchant_uid(MERCHANT_UID);
        result.setImp_success(imp_success);
        result.setError_msg(error_msg);

        // 결제 성공시에만 결제 후 처리
        if (imp_success) {

            procInit(request);

            if (PROTOCOL_IDENTIFIER.PROTOCOL_PAYMENT_RESULT_1) {
                resp_pay_result record = payResultService.GET_PAYMENT_RESULT(imp_uid, MERCHANT_UID);
                result.setError_msg(record.getMsg());
                result.setImp_success(record.getIsSuccess());
                record = null;
            } else {
                response = getResponse(219);
            }

            procClose(request, result.toString());

        } else {

            errorOrderFailService.insertRecord(MERCHANT_UID, imp_uid, error_msg, 1);

            logger.error("결제 실패 : " + result.toString());

        }

        return result;
    }


    @ResponseBody
    @RequestMapping(value = "/pood/iamport/result/1", method = RequestMethod.POST)
    public PAYMENT_RESULT payment_result_post(HttpServletRequest request,
        @RequestParam(value = "imp_uid", required = false) String imp_uid,
        @RequestParam(value = "merchant_uid", required = false) String MERCHANT_UID,
        @RequestParam(value = "imp_success", required = false) Boolean imp_success,
        @RequestParam(value = "error_msg", required = false) String error_msg, Model model)
        throws Exception {

        PAYMENT_RESULT result = new PAYMENT_RESULT();
        result.setImp_uid(imp_uid);
        result.setMerchant_uid(MERCHANT_UID);
        result.setImp_success(imp_success);
        result.setError_msg(error_msg);

        // 결제 성공시에만 결제 후 처리
        if (imp_success) {

            procInit(request);

            String payInfo = iamportService.getPayInfo(MERCHANT_UID);
            IamportPaymentInfo iamportPaymentInfo = objectMapper
                .readValue(payInfo,
                    IamportPaymentInfo.class);
            if (!iamportPaymentInfo.isSuccess()) {
                throw new SearchPaymentInfoFailException("결제 정보를 찾을수 없습니다.");
            }

            int orderPrice = orderServiceV2.getOrderPriceByOrderNumber(MERCHANT_UID);
            if (iamportPaymentInfo.amountCheckFail(orderPrice)) {
                retreieveService.requestRefund(
                    retreieveService.getToken(), iamportPaymentInfo.getPaymentAmount(),
                    MERCHANT_UID, "잘못된 결제 금액");
                throw new PaymentAmountCheckFailException("결제 금액이 올바르지 않습니다. 관리자에게 문의 부탁드립니다.");
            }

            if (PROTOCOL_IDENTIFIER.PROTOCOL_PAYMENT_RESULT_1) {
                resp_pay_result record = payResultService.GET_PAYMENT_RESULT(imp_uid, MERCHANT_UID);
                result.setError_msg(record.getMsg());
                result.setImp_success(record.getIsSuccess());
                record = null;
            } else {
                response = getResponse(219);
            }

            if (isAligoKakaoActive) {
                final OrderInfoDto orderInfoDto = orderServiceV2.getOrderInfo(MERCHANT_UID);
                kakaoApi.orderCompletedSend(orderInfoDto);

            }

            procClose(request, result.toString());

        } else {

            errorOrderFailService.insertRecord(MERCHANT_UID, imp_uid, error_msg, 1);

            logger.error("결제 실패 : " + result.toString());

        }

        return result;
    }


} 
 