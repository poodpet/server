package com.pood.server.web.mapper.payment.pay_cancel;

import com.pood.server.api.service.iamport.resParserService;
import com.pood.server.api.service.json.jsonService;
import com.pood.server.api.service.log.logUserOrderService;
import com.pood.server.api.service.log.logUserSavePointService;
import com.pood.server.api.service.log.logUserUsePointService;
import com.pood.server.api.service.meta.coupon.couponService;
import com.pood.server.api.service.meta.order.orderBasketService;
import com.pood.server.api.service.meta.order.orderCancelService;
import com.pood.server.api.service.pay.retreieveService;
import com.pood.server.api.service.user.userBasketService;
import com.pood.server.api.service.user.userCouponService;
import com.pood.server.api.service.user.userPointService;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import com.pood.server.object.meta.vo_order;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class cancel extends PROTOCOL {

    Logger logger = LoggerFactory.getLogger(cancel.class);

    @Value("${legacy.order-cancel-work}")
    private boolean isOrderCancelWork;

    @Autowired
    @Qualifier("couponService")
    couponService couponService;


    @Autowired
    @Qualifier("userCouponService")
    userCouponService userCouponService;

    @Autowired
    @Qualifier("userBasketService")
    userBasketService userBasketService;
 

    @Autowired
    @Qualifier("orderBasketService")
    orderBasketService orderBasketService;

    @Autowired
    @Qualifier("orderCancelService")
    orderCancelService orderCancelService;

    @Autowired
    @Qualifier("logUserOrderService")
    logUserOrderService logUserOrderService;

    @Autowired
    @Qualifier("logUserSavePointService")
    logUserSavePointService logUserSavePointService;
    
    @Autowired
    @Qualifier("jsonService")
    jsonService jsonService;

    @Autowired
    @Qualifier("resParserService")
    resParserService resParserService;
    
    @Autowired
    @Qualifier("logUserUsePointService")
    logUserUsePointService logUserUsePointService;

    @Autowired
    @Qualifier("retreieveService")
    retreieveService retreieveService;
 

    @Autowired
    @Qualifier("userPointService")
    userPointService userPointService;

    @ResponseBody
    @RequestMapping(value = "/pood/payment/cancel/1", method = RequestMethod.GET)
    public HashMap<String, Object> PAMENT_CANCEL(
        @RequestParam(value="merchant_uid", required=false) String MERCHANT_UID, 
        @RequestParam(value="request_text", required=false) String REQUEST_TEXT, 
        @RequestParam(value="isRefund",     required=false) Boolean IS_REFUND,
        @RequestParam(value="refund_uuid",  required=false) String REFUND_UUID,
        @RequestParam(value="attr_type",    required=false) Integer ATTR_TYPE,
        @RequestParam(value="isAdmin",    required=false) Boolean IS_ADMIN,
        HttpServletRequest request, Model model)
                throws Exception {
 
                    
        procInit(request);

        response = getResponse(200);
        
        if(PROTOCOL_IDENTIFIER.PROTOCOL_PAYMENT_CANCEL_1){    

            vo_order ORDER = orderService.getOrder(MERCHANT_UID);

            if(ORDER != null){
                Boolean adminForceCancle = false;
                logger.info("IS_ADMIN : " + IS_ADMIN);
                logger.info("IS_REFUND : " + IS_REFUND);
                if(IS_ADMIN == true) {
                    adminForceCancle = true;
                }

                if(IS_REFUND == null){
                    if (isOrderCancelWork) {
                        Boolean TIME_LIMIT = orderService.checkRetreiveTimeLimit(MERCHANT_UID);
                        if (!adminForceCancle && !TIME_LIMIT) {
                            response = getResponse(244);
                            procClose(request, response.toString());
                            return response;
                        }
                        TIME_LIMIT = null;
                    }
                }

                Integer RETREIVE_STATUS = 254;

                if(IS_REFUND == null) 
                    RETREIVE_STATUS = retreieveService.CANCEL_ALL(MERCHANT_UID,  REQUEST_TEXT);     
                else{

                   RETREIVE_STATUS = retreieveService.REFUND_ALL(
                            MERCHANT_UID,                   // 주문 번호
                            REQUEST_TEXT,                   // 환불 요청 사항
                            IS_REFUND,                      // 0 : 일반 취소, 1: 반품
                            REFUND_UUID,                    // 반품 고유 번호
                            ATTR_TYPE                       // (반품인 경우) 0 : 구매자 귀책, 1 : 판매자 귀책
                            );  

                }                   

                response = getResponse(RETREIVE_STATUS);

                ORDER               = null;

            }else response = getResponse(247);

        }else response = getResponse(219);


        procClose(request, response.toString());

        return response;

    }

}