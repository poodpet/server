package com.pood.server.web.mapper;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.pood.server.api.service.time.*;
import com.pood.server.config.PROTOCOL;

@Controller
public class postcode extends PROTOCOL{

    Logger logger = LoggerFactory.getLogger(postcode.class);

    @Autowired
    @Qualifier("timeService")
    timeService timeService;
    
    @GetMapping("/pood/postcode/1")
    public String request(HttpServletRequest request, Model model) throws SQLException {
        // 우편번호 검색 웹 뷰 호출
        logger.info(
            timeService.getCurrentTime() + "INFO [" 
            + request 
            + "][/pood/postcode/1]REQUEST");

		return "postcode";
	}

    @GetMapping("/pood/postcode/2")
    public String request2(HttpServletRequest request, Model model) throws SQLException {

        // 우편번호 검색 웹 뷰 호출
        logger.info(
                timeService.getCurrentTime() + "INFO ["
                        + request
                        + "][/pood/postcode/2]REQUEST");

        return "postcode2";
    }

}