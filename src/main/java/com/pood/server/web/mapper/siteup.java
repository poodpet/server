package com.pood.server.web.mapper;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.pood.server.api.service.meta.order.*;

import com.pood.server.api.service.user.*;
import com.pood.server.api.service.log.*;
import com.pood.server.api.service.pood_point.*;
import com.pood.server.api.service.holiday_check.*;
import com.pood.server.api.service.time.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@Controller
public class siteup {

    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("userPointService")
    userPointService userPointService;

    @Autowired
    @Qualifier("logUserPointService")
    logUserPointService logUserPointService;

    @Autowired
    @Qualifier("orderPointService")
    orderPointService orderPointService;

    @Autowired
    @Qualifier("logUserUsePointService")
    logUserUsePointService logUserUsePointService;

    @Autowired
    @Qualifier("orderService")
    OrderService orderService;
 
    @Autowired
    @Qualifier("poodPointService")
    poodPointService poodPointService;

    @Autowired
    @Qualifier("timeService")
    timeService timeService;

    @Autowired
    @Qualifier("holidayCheckService")
    holidayCheckService holidayCheckService;

    @GetMapping("/")
    public String request(HttpServletRequest request, Model model) throws SQLException {
      return "siteup";
    }
    
    @GetMapping("/hello")
    public String request2(HttpServletRequest request, Model model) throws Exception {       
      return "hello";
    }

}