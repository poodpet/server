package com.pood.server.web.mapper.payment.simple_pay;

import com.pood.server.api.service.error.errorOrderFailService;
import com.pood.server.api.service.iamport.iamportService;
import com.pood.server.api.service.iamport.resParserService;
import com.pood.server.api.service.json.jsonService;
import com.pood.server.api.service.pay.checkOrderService;
import com.pood.server.api.service.pay.initPayService;
import com.pood.server.api.service.pay.payResultService;
import com.pood.server.api.service.user.userCardService;
import com.pood.server.api.service.user.userService;
import com.pood.server.config.PROTOCOL;
import com.pood.server.config.PROTOCOL_IDENTIFIER;
import com.pood.server.object.IMP.IMP;
import com.pood.server.object.IMP.IMP_SIMPLE_PAY_FAIL;
import com.pood.server.object.IMP.IMP_SIMPLE_PAY_RESULT;
import com.pood.server.object.IMP.IMP_SIMPLE_PAY_SUCCESS;
import com.pood.server.object.IMP.KEYGEN_RESULT;
import com.pood.server.object.IMP.PAYMENT_RESULT;
import com.pood.server.object.resp.resp_pay_result;
import com.pood.server.object.resp.resp_pay_success;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class simple_pay extends PROTOCOL {

    Logger logger = LoggerFactory.getLogger(simple_pay.class);

    @Value("${legacy.iamport-url}")
    private String iamportUrl;

    @Autowired
    @Qualifier("iamportService")
    iamportService iamportService;

    @Autowired
    @Qualifier("payResultService")
    payResultService payResultService;

    @Autowired
    @Qualifier("checkOrderService")
    checkOrderService checkOrderService;

    @Autowired
    @Qualifier("resParserService")
    resParserService resParserService;

    @Autowired
    @Qualifier("userCardService")
    userCardService userCardService;

    @Autowired
    @Qualifier("jsonService")
    jsonService jsonService;

    @Autowired
    @Qualifier("userService")
    userService userService;

    @Autowired
    @Qualifier("initPayService")
    initPayService initPayService;

    @Autowired
    @Qualifier("errorOrderFailService")
    errorOrderFailService errorOrderFailService;

    @RequestMapping(value = "/pood/user/simple-pay/1", method = RequestMethod.GET)
    public String GET_BILLING_KEY(
        @RequestParam(value="buyer_email", required=false)  String  BUYER_EMAIL,
        @RequestParam(value="buyer_name", required=false)   String  BUYER_NAME,
        @RequestParam(value="user_uuid", required=false)    String  USER_UUID,
        HttpServletRequest request, Model model)throws Exception {


        // 아임포트 결제 요청 헤더 파라미터 로그로 남김
        Integer resp_status = procInit(request);
        
        // 인증토큰에 대해서 유효성 검증에 실패한 경우
        if(resp_status != 200){
            procClose(request, response.toString());
            return "iamport/fail.html";
        }

        String MERCHANT_UID = UUID.randomUUID().toString();
        String CUSTOMER_UID = UUID.randomUUID().toString();
		String CURRENT_DATETIME = new SimpleDateFormat("yyyyMMddHHmmssmmm").format(Calendar.getInstance().getTime());
        MERCHANT_UID = CURRENT_DATETIME + "-" + MERCHANT_UID;

        logger.info("MERCHANT UID GENERATE : " + MERCHANT_UID);

        model.addAttribute("imp_uid",           com.pood.server.config.host.IAMPORT.IMP_UID);
        model.addAttribute("merchant_uid",      MERCHANT_UID);
        model.addAttribute("buyer_email",       BUYER_EMAIL);
        model.addAttribute("buyer_name",        BUYER_NAME);
        model.addAttribute("pg",                com.pood.server.config.host.IAMPORT.PG);
        model.addAttribute("pay_method",        com.pood.server.config.host.IAMPORT.PAY_METHOD);
        model.addAttribute("customer_uid", CUSTOMER_UID);
        model.addAttribute("url", iamportUrl);

        return "keygen.html";
    }

    @ResponseBody
    @RequestMapping(value = "/pood/user/simple-pay/1/1", method = RequestMethod.GET)
    public KEYGEN_RESULT payment_result(HttpServletRequest request,
            @RequestParam(value = "imp_uid", required = false) String IMP_UID,
            @RequestParam(value = "customer_uid", required = false) String CUSTOMER_UID,
            @RequestParam(value = "merchant_uid", required = false) String MERCHANT_UID,
            @RequestParam(value = "imp_success", required = false) Boolean IMP_SUCCESS,
            @RequestParam(value = "error_msg", required = false) String ERROR_MESSAGE, Model model) throws Exception {
            
     
        logger.info("IMP UID : " + IMP_UID + ", CUSTOMER_UID : " + CUSTOMER_UID + ", MERCHANT_UID : " + MERCHANT_UID +", IMP_SUCCESS : " + IMP_SUCCESS + ", ERROR MESSAGE : " + ERROR_MESSAGE);
        KEYGEN_RESULT KEYGEN_RESULT = new KEYGEN_RESULT();
        KEYGEN_RESULT.setCUSTOMER_UID(CUSTOMER_UID);
        KEYGEN_RESULT.setMERCHANT_UID(MERCHANT_UID);
        KEYGEN_RESULT.setImp_success(IMP_SUCCESS);
        KEYGEN_RESULT.setImp_uid(IMP_UID);
        KEYGEN_RESULT.setError_msg(ERROR_MESSAGE);
            
        logger.info("KEY GENERATOR RESULT : " + KEYGEN_RESULT);

            
        return KEYGEN_RESULT;
    }
 
    @ResponseBody
    @RequestMapping(value = "/pood/user/simple-pay/2", method = RequestMethod.POST)
    public PAYMENT_RESULT SIMPLE_PAY(
            @RequestParam String param, 
            @RequestParam String CUSTOMER_UID, Model model,
            HttpServletRequest request) throws Exception {
 
        PAYMENT_RESULT result = new PAYMENT_RESULT();
        
        procInit(request);

        logger.info(param);

        IMP IAMPORT         = initPayService.initIMP(param);

        String USER_UUID    = IAMPORT.USER_UUID;

        String TOKEN        = IAMPORT.USER_TOKEN;

        Integer STATUS_CODE = initPayService.tokenCheck(TOKEN, USER_UUID);
        
    
        if(STATUS_CODE != 200){
            procClose(request, "");
            result.setImp_success(false);
            result.setError_msg("AUTH TOKEN INVALID.");
            return result;
        }


        if (PROTOCOL_IDENTIFIER.PROTOCOL_USER_SIMPLE_PAY_2) {
        
            // 주문 정보 유효성 검증
            resp_pay_success imp_result = checkOrderService.getOrderCheck(IAMPORT);

            if(!imp_result.getIsSuccess()){
                
                // 주문 정보 유효성 검증에서 실패할 경우 오류 반환
                procClose(request, "");
                result.setImp_success(false);
                result.setError_msg(imp_result.getMsg());
                return result;
            }


            // 주문 레코드 삽입
            Integer status = initPayService.insertIMPorder(IAMPORT);
            
            if(status == -1){
                result.setImp_success(false);
                result.setError_msg("결제 실패");
                procClose(request, null);
                errorOrderFailService.insertRecord(IAMPORT.MERCHANT_UID, "", "주문 레코드 생성 오류", 0);
                return result;
            }
            
            status = null;

             
            // 유효한 주문인 경우 간편결제 시도
            ResponseEntity<String> RESULT = 
                iamportService.doSimplePay(CUSTOMER_UID, IAMPORT);

            String PAYMENT_STATUS_CODE = resParserService.getStatusCode(RESULT.getBody());
 

            if(PAYMENT_STATUS_CODE.equals("0")){   
                
                // 간편결제가 성공했을 경우 성공 메세지 반환
                IMP_SIMPLE_PAY_SUCCESS SIMPLE_PAY_SUCCESS = 
                    (IMP_SIMPLE_PAY_SUCCESS)jsonService.getDecodedObject(RESULT.getBody(), IMP_SIMPLE_PAY_SUCCESS.class);
                IMP_SIMPLE_PAY_RESULT SUCCESS_RESULT = SIMPLE_PAY_SUCCESS.getResponse();
                
                String MERCHANT_UID = SUCCESS_RESULT.getMerchant_uid();
                String IMP_UID = SUCCESS_RESULT.getImp_uid();

                result.setMerchant_uid(MERCHANT_UID);
                result.setImp_success(true);
                result.setError_msg("success. ");
                result.setImp_uid(IMP_UID);

                resp_pay_result record = payResultService.GET_PAYMENT_RESULT(IMP_UID, MERCHANT_UID);
                result.setError_msg(record.getMsg());
                result.setImp_success(record.getIsSuccess());
                
                logger.info("SIMPLE PAY RESULT : "+ SUCCESS_RESULT);

                MERCHANT_UID = null;

                IMP_UID = null;
                
            }else{

                // 간편결제가 실패하였을 경우 FAIL 메세지 반환
                IMP_SIMPLE_PAY_FAIL FAIL_RESULT = 
                    (IMP_SIMPLE_PAY_FAIL)jsonService.getDecodedObject(RESULT.getBody(), IMP_SIMPLE_PAY_FAIL.class);
                result.setImp_success(false);
                result.setError_msg(FAIL_RESULT.getMessage());
            }

            procClose(request, result.toString());    

        } else procClose(request, "PROTOCOL IS DEACTIVATED");    
        
        
        return result;
    }
    
}
