package com.pood.server.web.mapper.payment.pay_cancel;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;

import com.pood.server.config.*;
import com.pood.server.api.service.user.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class cancel_buy_now extends PROTOCOL {
 
    @Autowired
    @Qualifier("userCouponService")
    userCouponService userCouponService;


    @Autowired
    @Qualifier("userBasketService")
    userBasketService userBasketService;
    
    
 
    // 바로 구매 취소하고 장바구니 신규 등록
    @ResponseBody
    @RequestMapping(value = "/pood/payment/cancel/buy-now/1", method = RequestMethod.GET)
    public HashMap<String, Object> PAYMENT_CANCEL_BUY_NOW(
        @RequestParam(value="basket_idx", required=false) Integer BASKET_A_IDX,
        HttpServletRequest request, Model model)
                throws Exception {

                    
        Integer resp_status = procInit(request);
        
        // 인증토큰에 대해서 유효성 검증에 실패한 경우
        if(resp_status != 200){
            procClose(request, response.toString());
            return response;
        }

        if(PROTOCOL_IDENTIFIER.PROTOCOL_PAYMENT_CANCEL_BUY_NOW_1){        
            userBasketService.deleteUserBasket(BASKET_A_IDX);
        }else response = getResponse(219);
        
        procClose(request, response.toString());

        return response;

    }

}