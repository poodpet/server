package com.pood.server.object.api;

public class API_RESPONSE {

    private String msg;

    private Integer status;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "API_RESPONSE [msg=" + msg + ", status=" + status + "]";
    }

    
}
