package com.pood.server.object.user;

public class vo_user_card {
    

    private String user_uuid        =   "";     // 회원 UUID

    private String billing_key      =   "";

    private String card_name        =   "";     // 회원 카드 이름

    private String card_number      =   "";     // 회원 카드 번호

    private String card_user        =   "";     // 회원 카드 사용자 이름

    private Boolean main_card       =   false;  // 대표 카드 식별자

     

    public String getBilling_key() {
        return billing_key;
    }

    public void setBilling_key(String billing_key) {
        this.billing_key = billing_key;
    }

    public String getCard_name() {
        return card_name;
    }

    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getCard_user() {
        return card_user;
    }

    public void setCard_user(String card_user) {
        this.card_user = card_user;
    }

    public Boolean getMain_card() {
        return main_card;
    }

    public void setMain_card(Boolean main_card) {
        this.main_card = main_card;
    }
 
    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    @Override
    public String toString() {
        return "user_card [billing_key=" + billing_key + ", card_name=" + card_name + ", card_number=" + card_number
                + ", card_user=" + card_user + ", main_card=" + main_card + ", user_uuid=" + user_uuid + "]";
    }

    
}
