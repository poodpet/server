package com.pood.server.object.user;

public class vo_user_point {
    
    private String  user_uuid;

    private String  order_number;

    private Integer review_idx;

    private Integer point_idx;
    
    private String  point_uuid;

    private String  point_name;

    private Integer point_type;

    private Integer point_price;

    private Integer point_rate;

    private String  expired_date;

    private Integer status;

    private Integer saved_point;

    private Integer used_point;

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public Integer getReview_idx() {
        return review_idx;
    }

    public void setReview_idx(Integer review_idx) {
        this.review_idx = review_idx;
    }

    public Integer getPoint_idx() {
        return point_idx;
    }

    public void setPoint_idx(Integer point_idx) {
        this.point_idx = point_idx;
    }

    public String getPoint_name() {
        return point_name;
    }

    public void setPoint_name(String point_name) {
        this.point_name = point_name;
    }

    public Integer getPoint_type() {
        return point_type;
    }

    public void setPoint_type(Integer point_type) {
        this.point_type = point_type;
    }

    public Integer getPoint_price() {
        return point_price;
    }

    public void setPoint_price(Integer point_price) {
        this.point_price = point_price;
    }

    public Integer getPoint_rate() {
        return point_rate;
    }

    public void setPoint_rate(Integer point_rate) {
        this.point_rate = point_rate;
    }

    public String getExpired_date() {
        return expired_date;
    }

    public void setExpired_date(String expired_date) {
        this.expired_date = expired_date;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSaved_point() {
        return saved_point;
    }

    public void setSaved_point(Integer saved_point) {
        this.saved_point = saved_point;
    }

    public Integer getUsed_point() {
        return used_point;
    }

    public void setUsed_point(Integer used_point) {
        this.used_point = used_point;
    }

    public String getPoint_uuid() {
        return point_uuid;
    }

    public void setPoint_uuid(String point_uuid) {
        this.point_uuid = point_uuid;
    }

    @Override
    public String toString() {
        return "user_point [expired_date=" + expired_date + ", order_number=" + order_number + ", point_idx="
                + point_idx + ", point_name=" + point_name + ", point_price=" + point_price + ", point_rate="
                + point_rate + ", point_type=" + point_type + ", point_uuid=" + point_uuid + ", review_idx="
                + review_idx + ", saved_point=" + saved_point + ", status=" + status + ", used_point=" + used_point
                + ", user_uuid=" + user_uuid + "]";
    }

    
}
