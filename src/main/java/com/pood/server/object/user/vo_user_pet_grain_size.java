package com.pood.server.object.user;

public class vo_user_pet_grain_size {
 
    private Integer user_pet_idx;

    private Integer grain_size_idx;

    private String  name;

    private Float   size_min;

    private Float   size_max;

    public Integer getUser_pet_idx() {
        return user_pet_idx;
    }

    public void setUser_pet_idx(Integer user_pet_idx) {
        this.user_pet_idx = user_pet_idx;
    }

    public Integer getGrain_size_idx() {
        return grain_size_idx;
    }

    public void setGrain_size_idx(Integer grain_size_idx) {
        this.grain_size_idx = grain_size_idx;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getSize_min() {
        return size_min;
    }

    public void setSize_min(Float size_min) {
        this.size_min = size_min;
    }

    public Float getSize_max() {
        return size_max;
    }

    public void setSize_max(Float size_max) {
        this.size_max = size_max;
    }

    
}
