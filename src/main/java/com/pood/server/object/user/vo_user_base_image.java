package com.pood.server.object.user;

public class vo_user_base_image {
    
    private Long idx;

    private String url;

    public Long getIdx() {
        return idx;
    }

    public String getUrl() {
        return url;
    }

    public void setIdx(Long idx) {
        this.idx = idx;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
