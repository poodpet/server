package com.pood.server.object.user;

public class vo_user_basket {
     
    private Integer user_idx;

    private String  user_uuid;

    private Integer goods_idx;

    private Integer pr_code_idx;

    private Integer qty;

    private Integer goods_price;

    private String  regular_date;

    private String  order_number;

    private Integer status;

    public Integer getUser_idx() {
        return user_idx;
    }

    public void setUser_idx(Integer user_idx) {
        this.user_idx = user_idx;
    }

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public Integer getGoods_idx() {
        return goods_idx;
    }

    public void setGoods_idx(Integer goods_idx) {
        this.goods_idx = goods_idx;
    }

    public Integer getPr_code_idx() {
        return pr_code_idx;
    }

    public void setPr_code_idx(Integer pr_code_idx) {
        this.pr_code_idx = pr_code_idx;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getGoods_price() {
        return goods_price;
    }

    public void setGoods_price(Integer goods_price) {
        this.goods_price = goods_price;
    }

    public String getRegular_date() {
        return regular_date;
    }

    public void setRegular_date(String regular_date) {
        this.regular_date = regular_date;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "vo_user_basket [goods_idx=" + goods_idx + ", goods_price=" + goods_price + ", order_number="
                + order_number + ", pr_code_idx=" + pr_code_idx + ", qty=" + qty + ", regular_date=" + regular_date
                + ", status=" + status + ", user_idx=" + user_idx + ", user_uuid=" + user_uuid + "]";
    }
 
}
