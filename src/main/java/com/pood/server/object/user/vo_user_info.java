package com.pood.server.object.user;

public class vo_user_info {
 
    private String  user_uuid;

    private String  user_nickname;

    private String  user_email;
 
    private String  user_password;

    private Boolean user_service_agree;
 
    private Integer user_status;

    private String  user_name;

    private Integer user_point;

    private String  user_phone;

    private String  referral_code;

    private Boolean order_push;

    private String  order_push_cg_time;

    private Boolean pood_push;

    private String  pood_push_cg_time;

    private Boolean service_push;

    private String  service_push_cg_time;

    private String  device_key;

    private Integer device_type;

    private String  token_arn;

    private String  ml_name;

    private Float   ml_rate;

    private Integer ml_price;

    private Integer ml_month;

    private String  status_update_date;

    private String  updatetime;

    private String  recordbirth;

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public String getUser_nickname() {
        return user_nickname;
    }

    public void setUser_nickname(String user_nickname) {
        this.user_nickname = user_nickname;
    }
 
    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public Boolean getUser_service_agree() {
        return user_service_agree;
    }

    public void setUser_service_agree(Boolean user_service_agree) {
        this.user_service_agree = user_service_agree;
    }
  
    public Integer getUser_status() {
        return user_status;
    }

    public void setUser_status(Integer user_status) {
        this.user_status = user_status;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
  
    public Integer getUser_point() {
        return user_point;
    }

    public void setUser_point(Integer user_point) {
        this.user_point = user_point;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getReferral_code() {
        return referral_code;
    }

    public void setReferral_code(String referral_code) {
        this.referral_code = referral_code;
    }
 
    public Boolean getOrder_push() {
        return order_push;
    }

    public void setOrder_push(Boolean order_push) {
        this.order_push = order_push;
    }

    public String getOrder_push_cg_time() {
        return order_push_cg_time;
    }

    public void setOrder_push_cg_time(String order_push_cg_time) {
        this.order_push_cg_time = order_push_cg_time;
    }

    public Boolean getPood_push() {
        return pood_push;
    }

    public void setPood_push(Boolean pood_push) {
        this.pood_push = pood_push;
    }

    public String getPood_push_cg_time() {
        return pood_push_cg_time;
    }

    public void setPood_push_cg_time(String pood_push_cg_time) {
        this.pood_push_cg_time = pood_push_cg_time;
    }

    public Boolean getService_push() {
        return service_push;
    }

    public void setService_push(Boolean service_push) {
        this.service_push = service_push;
    }

    public String getService_push_cg_time() {
        return service_push_cg_time;
    }

    public void setService_push_cg_time(String service_push_cg_time) {
        this.service_push_cg_time = service_push_cg_time;
    }

    public String getDevice_key() {
        return device_key;
    }

    public void setDevice_key(String device_key) {
        this.device_key = device_key;
    }

    public Integer getDevice_type() {
        return device_type;
    }

    public void setDevice_type(Integer device_type) {
        this.device_type = device_type;
    }

    public String getToken_arn() {
        return token_arn;
    }

    public void setToken_arn(String token_arn) {
        this.token_arn = token_arn;
    }

    public String getMl_name() {
        return ml_name;
    }

    public void setMl_name(String ml_name) {
        this.ml_name = ml_name;
    }

    public Float getMl_rate() {
        return ml_rate;
    }

    public void setMl_rate(Float ml_rate) {
        this.ml_rate = ml_rate;
    }

    public Integer getMl_price() {
        return ml_price;
    }

    public void setMl_price(Integer ml_price) {
        this.ml_price = ml_price;
    }

    public Integer getMl_month() {
        return ml_month;
    }

    public void setMl_month(Integer ml_month) {
        this.ml_month = ml_month;
    }

    public String getStatus_update_date() {
        return status_update_date;
    }

    public void setStatus_update_date(String status_update_date) {
        this.status_update_date = status_update_date;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    @Override
    public String toString() {
        return "vo_user_info [device_key=" + device_key + ", device_type=" + device_type + ", ml_month=" + ml_month
                + ", ml_name=" + ml_name + ", ml_price=" + ml_price + ", ml_rate=" + ml_rate + ", order_push="
                + order_push + ", order_push_cg_time=" + order_push_cg_time + ", pood_push=" + pood_push
                + ", pood_push_cg_time=" + pood_push_cg_time + ", recordbirth=" + recordbirth + ", referral_code="
                + referral_code + ", service_push=" + service_push + ", service_push_cg_time=" + service_push_cg_time
                + ", status_update_date=" + status_update_date + ", token_arn=" + token_arn + ", updatetime="
                + updatetime + ", user_email=" + user_email + ", user_name=" + user_name + ", user_nickname="
                + user_nickname + ", user_password=" + user_password + ", user_phone=" + user_phone + ", user_point="
                + user_point + ", user_service_agree=" + user_service_agree + ", user_status=" + user_status
                + ", user_uuid=" + user_uuid + "]";
    }
 
}
