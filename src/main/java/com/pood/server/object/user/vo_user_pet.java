package com.pood.server.object.user;

public class vo_user_pet {

    private String  user_uuid;

    private Integer pet_activity;

    private Integer pc_id;

    private Integer psc_id;

    private String  pet_name;

    private String  pet_birth;

    private Integer pet_gender;

    private Float pet_weight;

    private Integer pet_status;

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }
 
    public Integer getPet_activity() {
        return pet_activity;
    }

    public void setPet_activity(Integer pet_activity) {
        this.pet_activity = pet_activity;
    }

    public Integer getPc_id() {
        return pc_id;
    }

    public void setPc_id(Integer pc_id) {
        this.pc_id = pc_id;
    }

    public Integer getPsc_id() {
        return psc_id;
    }

    public void setPsc_id(Integer psc_id) {
        this.psc_id = psc_id;
    }

    public String getPet_name() {
        return pet_name;
    }

    public void setPet_name(String pet_name) {
        this.pet_name = pet_name;
    }

    public String getPet_birth() {
        return pet_birth;
    }

    public void setPet_birth(String pet_birth) {
        this.pet_birth = pet_birth;
    }

    public Integer getPet_gender() {
        return pet_gender;
    }

    public void setPet_gender(Integer pet_gender) {
        this.pet_gender = pet_gender;
    }

    public Float getPet_weight() {
        return pet_weight;
    }

    public void setPet_weight(Float pet_weight) {
        this.pet_weight = pet_weight;
    }

    public Integer getPet_status() {
        return pet_status;
    }

    public void setPet_status(Integer pet_status) {
        this.pet_status = pet_status;
    }

    @Override
    public String toString() {
        return "user_pet [pc_id=" + pc_id + ", pet_activity=" + pet_activity + ", pet_birth=" + pet_birth
                + ", pet_gender=" + pet_gender + ", pet_name=" + pet_name + ", pet_status=" + pet_status
                + ", pet_weight=" + pet_weight + ", psc_id=" + psc_id + ", user_uuid=" + user_uuid + "]";
    }

    
}
