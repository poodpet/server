package com.pood.server.object.user;

public class vo_user_pet_allergy {

    private Integer user_pet_idx;

    private Integer allergy_idx;

    private String  name;

    private Integer type;

    public Integer getUser_pet_idx() {
        return user_pet_idx;
    }

    public void setUser_pet_idx(Integer user_pet_idx) {
        this.user_pet_idx = user_pet_idx;
    }

    public Integer getAllergy_idx() {
        return allergy_idx;
    }

    public void setAllergy_idx(Integer allergy_idx) {
        this.allergy_idx = allergy_idx;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    
}
