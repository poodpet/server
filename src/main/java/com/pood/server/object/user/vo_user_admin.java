package com.pood.server.object.user;

public class vo_user_admin {
    

    private String name         = "";   // 관리자 이름

    private String phone        = "";   // 관리자 전화 번호

    private String email        = "";   // 관리자 이메일

    private String nick_name    = "";   // 관리자 닉네임

    private String user_grade   = "";   // 관리자 등급

    private String password     = "";   // 관리자 비밀번호

    private Integer auth        = 0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getUser_grade() {
        return user_grade;
    }

    public void setUser_grade(String user_grade) {
        this.user_grade = user_grade;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAuth() {
        return auth;
    }

    public void setAuth(Integer auth) {
        this.auth = auth;
    }

    @Override
    public String toString() {
        return "user_admin [auth=" + auth + ", email=" + email + ", name=" + name + ", nick_name=" + nick_name
                + ", password=" + password + ", phone=" + phone + ", user_grade=" + user_grade + "]";
    }

    
}
