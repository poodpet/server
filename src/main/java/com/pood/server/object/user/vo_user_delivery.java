package com.pood.server.object.user;

public class vo_user_delivery {

    private String  user_uuid;

    private String  address;

    private Boolean default_type;

    private String  detail_address;

    private String  detail_address_old;

    private String  name;

    private String  nickname;

    private String  zipcode;

    private Boolean input_type;

    private Integer remote_type;
    
    private String  phone_number;

    private String  updatetime;

    private String  recordbirth;
    
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getDefault_type() {
        return default_type;
    }

    public void setDefault_type(Boolean default_type) {
        this.default_type = default_type;
    }

    public String getDetail_address() {
        return detail_address;
    }

    public void setDetail_address(String detail_address) {
        this.detail_address = detail_address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public Boolean getInput_type() {
        return input_type;
    }

    public void setInput_type(Boolean input_type) {
        this.input_type = input_type;
    }

    public Integer getRemote_type() {
        return remote_type;
    }

    public void setRemote_type(Integer remote_type) {
        this.remote_type = remote_type;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getDetail_address_old() {
        return detail_address_old;
    }

    public void setDetail_address_old(String detail_address_old) {
        this.detail_address_old = detail_address_old;
    }

    @Override
    public String toString() {
        return "user_delivery [address=" + address + ", default_type=" + default_type + ", detail_address="
                + detail_address + ", detail_address_old=" + detail_address_old + ", input_type=" + input_type
                + ", name=" + name + ", nickname=" + nickname + ", phone_number=" + phone_number + ", recordbirth="
                + recordbirth + ", remote_type=" + remote_type + ", updatetime=" + updatetime + ", user_uuid="
                + user_uuid + ", zipcode=" + zipcode + "]";
    }
 
    
    
}
