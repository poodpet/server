package com.pood.server.object.user;

public class vo_user_delivery_address {

    private Integer user_idx;

    private String  address;

    private Integer default_type;
 
    private String  detail_address;

    private String  name;

    private String  nickname;

    private String  zipcode;

    private Integer input_type;

    private Integer remote_type;

    private String  phone_number;

    public Integer getUser_idx() {
        return user_idx;
    }

    public void setUser_idx(Integer user_idx) {
        this.user_idx = user_idx;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getDefault_type() {
        return default_type;
    }

    public void setDefault_type(Integer default_type) {
        this.default_type = default_type;
    }

    public String getDetail_address() {
        return detail_address;
    }

    public void setDetail_address(String detail_address) {
        this.detail_address = detail_address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public Integer getInput_type() {
        return input_type;
    }

    public void setInput_type(Integer input_type) {
        this.input_type = input_type;
    }

    public Integer getRemote_type() {
        return remote_type;
    }

    public void setRemote_type(Integer remote_type) {
        this.remote_type = remote_type;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    @Override
    public String toString() {
        return "user_delivery_address [address=" + address + ", default_type=" + default_type + ", detail_address="
                + detail_address + ", input_type=" + input_type + ", name=" + name + ", nickname=" + nickname
                + ", phone_number=" + phone_number + ", remote_type=" + remote_type + ", user_idx=" + user_idx
                + ", zipcode=" + zipcode + "]";
    }

    
    
}
