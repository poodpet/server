package com.pood.server.object.user;

public class vo_user_simple_card {

    private Integer user_idx;

    private String  customer_uid;

    private String  billing_key;

    private String  card_name;

    private String  card_number;

    private String  card_user;

    private String  card_bg;

    private Integer main_card;

    public Integer getUser_idx() {
        return user_idx;
    }

    public void setUser_idx(Integer user_idx) {
        this.user_idx = user_idx;
    }

    public String getCustomer_uid() {
        return customer_uid;
    }

    public void setCustomer_uid(String customer_uid) {
        this.customer_uid = customer_uid;
    }

    public String getBilling_key() {
        return billing_key;
    }

    public void setBilling_key(String billing_key) {
        this.billing_key = billing_key;
    }

    public String getCard_name() {
        return card_name;
    }

    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getCard_user() {
        return card_user;
    }

    public void setCard_user(String card_user) {
        this.card_user = card_user;
    }

    public String getCard_bg() {
        return card_bg;
    }

    public void setCard_bg(String card_bg) {
        this.card_bg = card_bg;
    }

    public Integer getMain_card() {
        return main_card;
    }

    public void setMain_card(Integer main_card) {
        this.main_card = main_card;
    }

    
    
}
