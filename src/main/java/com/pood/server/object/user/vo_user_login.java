package com.pood.server.object.user;

public class vo_user_login {

    private Integer login_type;

    private String sns_key;

    private String email;

    public Integer getLogin_type() {
        return login_type;
    }

    public void setLogin_type(Integer login_type) {
        this.login_type = login_type;
    }

    public String getSns_key() {
        return sns_key;
    }

    public void setSns_key(String sns_key) {
        this.sns_key = sns_key;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "user_login [login_type=" + login_type + ", sns_key=" + sns_key + ", email + "+email+"]";
    }

    
}
