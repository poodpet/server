package com.pood.server.object.user;

public class vo_user_review_clap {

    Integer idx;

    Boolean isReviewed;

    public vo_user_review_clap(){};
    
    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public Boolean getIsReviewed() {
        return isReviewed;
    }

    public void setIsReviewed(Boolean isReviewed) {
        this.isReviewed = isReviewed;
    }

    @Override
    public String toString() {
        return "review_clap [idx=" + idx + ", isReviewed=" + isReviewed + "]";
    }

    
}
