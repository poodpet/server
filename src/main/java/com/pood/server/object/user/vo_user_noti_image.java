package com.pood.server.object.user;

public class vo_user_noti_image {
 
    private Integer image_idx;

    private Integer visible;

    private Integer type;

    private Integer priority;

    private String  recordbirth;

    private String  updatetime;

    private String  url;

    public Integer getImage_idx() {
        return image_idx;
    }

    public void setImage_idx(Integer image_idx) {
        this.image_idx = image_idx;
    }

    public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    
}
