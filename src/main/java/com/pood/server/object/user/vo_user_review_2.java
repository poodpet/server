package com.pood.server.object.user;

public class vo_user_review_2 {
    
    private Integer user_idx;

    private Integer pet_idx;

    private Integer order_idx;

    private Integer goods_idx;

    private String  goods_name;

    private Integer product_idx;

    private Integer rating;

    private String  review_text;

    private Integer update_count;

    private Integer isDelete;

    private Integer isVisible;

    public Integer getUser_idx() {
        return user_idx;
    }

    public void setUser_idx(Integer user_idx) {
        this.user_idx = user_idx;
    }

    public Integer getPet_idx() {
        return pet_idx;
    }

    public void setPet_idx(Integer pet_idx) {
        this.pet_idx = pet_idx;
    }

    public Integer getOrder_idx() {
        return order_idx;
    }

    public void setOrder_idx(Integer order_idx) {
        this.order_idx = order_idx;
    }

    public Integer getGoods_idx() {
        return goods_idx;
    }

    public void setGoods_idx(Integer goods_idx) {
        this.goods_idx = goods_idx;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public Integer getProduct_idx() {
        return product_idx;
    }

    public void setProduct_idx(Integer product_idx) {
        this.product_idx = product_idx;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getReview_text() {
        return review_text;
    }

    public void setReview_text(String review_text) {
        this.review_text = review_text;
    }

    public Integer getUpdate_count() {
        return update_count;
    }

    public void setUpdate_count(Integer update_count) {
        this.update_count = update_count;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getIsVisible() {
        return isVisible;
    }

    public void setIsVisible(Integer isVisible) {
        this.isVisible = isVisible;
    }

    @Override
    public String toString() {
        return "user_review_2 [goods_idx=" + goods_idx + ", goods_name=" + goods_name + ", isDelete=" + isDelete
                + ", isVisible=" + isVisible + ", order_idx=" + order_idx + ", pet_idx=" + pet_idx + ", product_idx="
                + product_idx + ", rating=" + rating + ", review_text=" + review_text + ", update_count=" + update_count
                + ", user_idx=" + user_idx + "]";
    }

    
}
