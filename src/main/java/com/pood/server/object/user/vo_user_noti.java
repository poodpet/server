package com.pood.server.object.user;

public class vo_user_noti {
    
    private Integer user_idx;

    private String  user_uuid;

    private Integer noti_read;

    private String  noti_text;

    private String  noti_title;

    private String  noti_scheme;

    private String  noti_url;

    private Integer noti_status;

    public Integer getUser_idx() {
        return user_idx;
    }

    public void setUser_idx(Integer user_idx) {
        this.user_idx = user_idx;
    }

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public Integer getNoti_read() {
        return noti_read;
    }

    public void setNoti_read(Integer noti_read) {
        this.noti_read = noti_read;
    }

    public String getNoti_text() {
        return noti_text;
    }

    public void setNoti_text(String noti_text) {
        this.noti_text = noti_text;
    }

    public String getNoti_title() {
        return noti_title;
    }

    public void setNoti_title(String noti_title) {
        this.noti_title = noti_title;
    }

    public String getNoti_scheme() {
        return noti_scheme;
    }

    public void setNoti_scheme(String noti_scheme) {
        this.noti_scheme = noti_scheme;
    }

    public String getNoti_url() {
        return noti_url;
    }

    public void setNoti_url(String noti_url) {
        this.noti_url = noti_url;
    }

    public Integer getNoti_status() {
        return noti_status;
    }

    public void setNoti_status(Integer noti_status) {
        this.noti_status = noti_status;
    }

    @Override
    public String toString() {
        return "user_noti [noti_read=" + noti_read + ", noti_scheme=" + noti_scheme + ", noti_status=" + noti_status
                + ", noti_text=" + noti_text + ", noti_title=" + noti_title + ", noti_url=" + noti_url + ", user_idx="
                + user_idx + ", user_uuid=" + user_uuid + "]";
    }

    
    
}
