package com.pood.server.object.user;

public class vo_user_token {

    private String  token;

    private String  user_uuid;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    @Override
    public String toString() {
        return "user_token [token=" + token + ", user_uuid=" + user_uuid + "]";
    }

    
    
}
