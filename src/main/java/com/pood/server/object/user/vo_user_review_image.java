package com.pood.server.object.user;

public class vo_user_review_image {
 
    private Integer idx;
 
    private String  url;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    
}
