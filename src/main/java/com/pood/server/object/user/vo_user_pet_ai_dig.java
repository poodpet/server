package com.pood.server.object.user;

public class vo_user_pet_ai_dig {
    
    private Integer user_pet_idx;

    private Integer ai_diagnosis_idx;

    private String  ard_name;

    private String  ard_group;

    private Integer ard_group_code;

    public Integer getUser_pet_idx() {
        return user_pet_idx;
    }

    public void setUser_pet_idx(Integer user_pet_idx) {
        this.user_pet_idx = user_pet_idx;
    }

    public Integer getAi_diagnosis_idx() {
        return ai_diagnosis_idx;
    }

    public void setAi_diagnosis_idx(Integer ai_diagnosis_idx) {
        this.ai_diagnosis_idx = ai_diagnosis_idx;
    }

    public String getArd_name() {
        return ard_name;
    }

    public void setArd_name(String ard_name) {
        this.ard_name = ard_name;
    }

    public String getArd_group() {
        return ard_group;
    }

    public void setArd_group(String ard_group) {
        this.ard_group = ard_group;
    }

    public Integer getArd_group_code() {
        return ard_group_code;
    }

    public void setArd_group_code(Integer ard_group_code) {
        this.ard_group_code = ard_group_code;
    }

    
}
