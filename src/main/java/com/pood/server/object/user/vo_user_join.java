package com.pood.server.object.user;

public class vo_user_join {

    private Integer idx;

    private Integer user_idx;

    private Integer login_type;

    private String sns_key;

    private String recordbirth;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public Integer getUser_idx() {
        return user_idx;
    }

    public void setUser_idx(Integer user_idx) {
        this.user_idx = user_idx;
    }

    public Integer getLogin_type() {
        return login_type;
    }

    public void setLogin_type(Integer login_type) {
        this.login_type = login_type;
    }

    public String getSns_key() {
        return sns_key;
    }

    public void setSns_key(String sns_key) {
        this.sns_key = sns_key;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    @Override
    public String toString() {
        return "user_join [idx=" + idx + ", login_type=" + login_type + ", recordbirth=" + recordbirth + ", sns_key="
                + sns_key + ", user_idx=" + user_idx + "]";
    }

    
    
}
