package com.pood.server.object.user;

import java.util.List;

public class vo_user_pet_ai_feed {
    
    private Integer up_idx;

    private String  user_uuid;

    private List<Integer> product_idx;

    public Integer getUp_idx() {
        return up_idx;
    }
    
    public void setUp_idx(Integer up_idx) {
        this.up_idx = up_idx;
    }
    
    public List<Integer> getProduct_idx() {
        return product_idx;
    }

    public void setProduct_idx(List<Integer> product_idx) {
        this.product_idx = product_idx;
    }

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    @Override
    public String toString() {
        return "user_pet_ai_feed [product_idx=" + product_idx.toString() + ", up_idx=" + up_idx + ", user_uuid=" + user_uuid + "]";
    }
 
    
}
