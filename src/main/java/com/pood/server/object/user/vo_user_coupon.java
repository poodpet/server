package com.pood.server.object.user;

public class vo_user_coupon {
    
    private Integer idx;
    
    private String  updatetime;

    private String  recordbirth;

    private Integer over_type;

    private Integer coupon_idx;

    private Integer user_idx;

    private String  available_time;

    private String  user_uuid;

    private String  publish_time;

    private String  used_time;

    private Integer apply_cart_idx;

    private Integer status;

    public Integer getOver_type() {
        return over_type;
    }

    public void setOver_type(Integer over_type) {
        this.over_type = over_type;
    }

    public Integer getCoupon_idx() {
        return coupon_idx;
    }

    public void setCoupon_idx(Integer coupon_idx) {
        this.coupon_idx = coupon_idx;
    }

    public Integer getUser_idx() {
        return user_idx;
    }

    public void setUser_idx(Integer user_idx) {
        this.user_idx = user_idx;
    }

    public String getAvailable_time() {
        return available_time;
    }

    public void setAvailable_time(String available_time) {
        this.available_time = available_time;
    }

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public String getPublish_time() {
        return publish_time;
    }

    public void setPublish_time(String publish_time) {
        this.publish_time = publish_time;
    }

    public String getUsed_time() {
        return used_time;
    }

    public void setUsed_time(String used_time) {
        this.used_time = used_time;
    }

    public Integer getApply_cart_idx() {
        return apply_cart_idx;
    }

    public void setApply_cart_idx(Integer apply_cart_idx) {
        this.apply_cart_idx = apply_cart_idx;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    @Override
    public String toString() {
        return "user_coupon [apply_cart_idx=" + apply_cart_idx + ", available_time=" + available_time + ", coupon_idx="
                + coupon_idx + ", idx=" + idx + ", over_type=" + over_type + ", publish_time=" + publish_time
                + ", recordbirth=" + recordbirth + ", status=" + status + ", updatetime=" + updatetime + ", used_time="
                + used_time + ", user_idx=" + user_idx + ", user_uuid=" + user_uuid + "]";
    }

    

}
