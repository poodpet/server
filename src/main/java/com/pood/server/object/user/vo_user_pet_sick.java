package com.pood.server.object.user;

public class vo_user_pet_sick {

    private Integer user_pet_idx;

    private Integer sick_idx;

    public Integer getUser_pet_idx() {
        return user_pet_idx;
    }

    public void setUser_pet_idx(Integer user_pet_idx) {
        this.user_pet_idx = user_pet_idx;
    }

    public Integer getSick_idx() {
        return sick_idx;
    }

    public void setSick_idx(Integer sick_idx) {
        this.sick_idx = sick_idx;
    }

    
}
