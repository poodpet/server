package com.pood.server.object.user;

public class vo_user_review {
    
    private String user_uuid;

    private Integer pet_idx;

    private Integer goods_idx;

    private Integer product_idx;

    private String order_number;

    private Integer rating;

    private String review_text;


    public Integer getPet_idx() {
        return pet_idx;
    }

    public void setPet_idx(Integer pet_idx) {
        this.pet_idx = pet_idx;
    }

    public Integer getgoods_idx() {
        return goods_idx;
    }

    public void setgoods_idx(Integer goods_idx) {
        this.goods_idx = goods_idx;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getReview_text() {
        return review_text;
    }

    public void setReview_text(String review_text) {
        this.review_text = review_text;
    }


    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }
    
    public Integer getProduct_idx() {
        return product_idx;
    }

    public void setProduct_idx(Integer product_idx) {
        this.product_idx = product_idx;
    }

}
