package com.pood.server.object.user;

public class vo_user_delivery_2 {

    private String  zipcode;

    private String  user_uuid;

    private String  address;

    private String  detail_address;

    private Integer remote_type;

    private String  name;
    
    private String  nickname;

    private Integer input_type;

    private String  phone_number;

    private String  delivery_msg;

    private Integer type;

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDetail_address() {
        return detail_address;
    }

    public void setDetail_address(String detail_address) {
        this.detail_address = detail_address;
    }

    public Integer getRemote_type() {
        return remote_type;
    }

    public void setRemote_type(Integer remote_type) {
        this.remote_type = remote_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getInput_type() {
        return input_type;
    }

    public void setInput_type(Integer input_type) {
        this.input_type = input_type;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getDelivery_msg() {
        return delivery_msg;
    }

    public void setDelivery_msg(String delivery_msg) {
        this.delivery_msg = delivery_msg;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "user_delivery_2 [address=" + address + ", delivery_msg=" + delivery_msg + ", detail_address="
                + detail_address + ", input_type=" + input_type + ", name=" + name + ", nickname=" + nickname
                + ", phone_number=" + phone_number + ", remote_type=" + remote_type + ", type=" + type + ", user_uuid="
                + user_uuid + ", zipcode=" + zipcode + "]";
    }

    
}
