package com.pood.server.object.user;

import java.util.List;

import com.pood.server.dto.dto_image_1;

public class vo_user_pet_3 {

    private String  pet_name;

    private String  pet_birth;

    private Integer pet_status;

    private Integer pet_gender;

    private Integer pc_id;

    private Integer user_idx;

    private List<dto_image_1> image;

    public String getPet_name() {
        return pet_name;
    }

    public void setPet_name(String pet_name) {
        this.pet_name = pet_name;
    }

    public String getPet_birth() {
        return pet_birth;
    }

    public void setPet_birth(String pet_birth) {
        this.pet_birth = pet_birth;
    }

    public Integer getPet_status() {
        return pet_status;
    }

    public void setPet_status(Integer pet_status) {
        this.pet_status = pet_status;
    }

    public Integer getPet_gender() {
        return pet_gender;
    }

    public void setPet_gender(Integer pet_gender) {
        this.pet_gender = pet_gender;
    }

    public Integer getPc_id() {
        return pc_id;
    }

    public void setPc_id(Integer pc_id) {
        this.pc_id = pc_id;
    }

    public Integer getUser_idx() {
        return user_idx;
    }

    public void setUser_idx(Integer user_idx) {
        this.user_idx = user_idx;
    }

    public List<dto_image_1> getImage() {
        return image;
    }

    public void setImage(List<dto_image_1> image) {
        this.image = image;
    }

    


}
