package com.pood.server.object.user;

public class vo_user_claim {
    

    private String user_uuid;

    private Integer claim_type;

    private String claim_text;

    private Boolean visible;

    private String claim_title;

    private String claim_goods_name;

    private Integer claim_goods_idx;

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public Integer getClaim_type() {
        return claim_type;
    }

    public void setClaim_type(Integer claim_type) {
        this.claim_type = claim_type;
    }

    public String getClaim_text() {
        return claim_text;
    }

    public void setClaim_text(String claim_text) {
        this.claim_text = claim_text;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public String getClaim_title() {
        return claim_title;
    }

    public void setClaim_title(String claim_title) {
        this.claim_title = claim_title;
    }

    public String getClaim_goods_name() {
        return claim_goods_name;
    }

    public void setClaim_goods_name(String claim_goods_name) {
        this.claim_goods_name = claim_goods_name;
    }

    public Integer getClaim_goods_idx() {
        return claim_goods_idx;
    }

    public void setClaim_goods_idx(Integer claim_goods_idx) {
        this.claim_goods_idx = claim_goods_idx;
    }

    @Override
    public String toString() {
        return "user_claim [claim_goods_idx=" + claim_goods_idx + ", claim_goods_name=" + claim_goods_name
                + ", claim_text=" + claim_text + ", claim_title=" + claim_title + ", claim_type=" + claim_type
                + ", user_uuid=" + user_uuid + ", visible=" + visible + "]";
    }

    
}
