package com.pood.server.object.resp;

import com.pood.server.dto.meta.order.dto_order_cancel;
import com.pood.server.dto.meta.order.dto_order_exchange_2;
import com.pood.server.dto.meta.order.dto_order_refund_2;
import com.pood.server.object.meta.vo_order_info;
import com.pood.server.object.user.vo_user_delivery_2;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class resp_user_order {

    private Integer idx;

    private Integer free_purchase;

    private Integer total_price;

    private String order_number;

    private String order_device;

    private Integer save_point;

    private String order_type_name;

    private String memo;

    private Integer order_price;

    private String order_name;

    private Integer order_status;

    private Integer address_update;

    private Integer delivery_type;

    private Integer discount_coupon_price;

    private String delivery_number;

    private Integer order_type;

    private vo_order_info order_info;

    private List<resp_order_basket> goods;

    private List<dto_order_exchange_2> order_exchange;

    private List<dto_order_refund_2> order_refund;

    private List<dto_order_cancel> order_cancel;

    private vo_user_delivery_2 user_delivery;

    @Override
    public String toString() {
        return "resp_user_order [address_update=" + address_update + ", delivery_number=" + delivery_number
            + ", delivery_type=" + delivery_type + ", discount_coupon_price=" + discount_coupon_price
            + ", free_purchase=" + free_purchase + ", goods=" + goods + ", idx=" + idx + ", memo=" + memo
            + ", order_cancel=" + order_cancel + ", order_device=" + order_device + ", order_exchange="
            + order_exchange + ", order_info=" + order_info + ", order_name=" + order_name + ", order_number="
            + order_number + ", order_price=" + order_price + ", order_refund=" + order_refund + ", order_status="
            + order_status + ", order_type=" + order_type + ", order_type_name=" + order_type_name + ", save_point="
            + save_point + ", total_price=" + total_price + ", user_delivery=" + user_delivery + "]";
    }

}
