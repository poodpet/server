package com.pood.server.object.resp;

import com.pood.server.dto.meta.dto_ai_recommend_diagnosis_2;
import com.pood.server.dto.meta.dto_allergy_data;
import com.pood.server.dto.meta.dto_grain_size;
import com.pood.server.dto.meta.dto_sick_info;
import com.pood.server.dto.meta.pet.dto_pet_2;
import com.pood.server.object.meta.vo_pet_act;
import com.pood.server.object.user.vo_user_pet_3;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class resp_user_pet extends vo_user_pet_3 {

    private Integer idx;

    private String updatetime;

    private String recordbirth;

    private List<dto_allergy_data> allergy;

    private List<dto_ai_recommend_diagnosis_2> ai_diagnosis;

    private List<dto_grain_size> grain_size;

    private dto_pet_2 psc_info;

    private vo_pet_act pet_act;

    private List<dto_sick_info> sick_info;

    @Override
    public String toString() {
        return "resp_user_pet [ai_diagnosis=" + ai_diagnosis + ", allergy=" + allergy + ", grain_size=" + grain_size
            + ", idx=" + idx + ", pet_act=" + pet_act + ", psc_info=" + psc_info + ", recordbirth=" + recordbirth
            + ", sick_info=" + sick_info + ", updatetime=" + updatetime + "]";
    }

}
