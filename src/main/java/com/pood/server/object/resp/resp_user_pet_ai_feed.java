package com.pood.server.object.resp;

import com.pood.server.dto.meta.product.dto_product_8;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class resp_user_pet_ai_feed {

    private Integer idx;

    private Integer user_idx;

    private Integer up_idx;

    private String recordbirth;

    private dto_product_8 product;

    @Override
    public String toString() {
        return "resp_user_pet_ai_feed [idx=" + idx + ", product=" + product + ", recordbirth=" + recordbirth
            + ", up_idx=" + up_idx + ", user_idx=" + user_idx + "]";
    }

}
