package com.pood.server.object.resp;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class resp_product_rating {

    private Integer rating_1_cnt;

    private Integer rating_2_cnt;

    private Integer rating_3_cnt;

    private Integer rating_4_cnt;

    private Integer rating_5_cnt;

    private Double rating_1_ratio;

    private Double rating_2_ratio;

    private Double rating_3_ratio;

    private Double rating_4_ratio;

    private Double rating_5_ratio;

    private Integer total_review_cnt;

    private Integer total_rating;

    private Double average_rating;

    @Override
    public String toString() {
        return "product_rating [average_rating=" + average_rating + ", rating_1_cnt=" + rating_1_cnt
            + ", rating_1_ratio=" + rating_1_ratio + ", rating_2_cnt=" + rating_2_cnt + ", rating_2_ratio="
            + rating_2_ratio + ", rating_3_cnt=" + rating_3_cnt + ", rating_3_ratio=" + rating_3_ratio
            + ", rating_4_cnt=" + rating_4_cnt + ", rating_4_ratio=" + rating_4_ratio + ", rating_5_cnt="
            + rating_5_cnt + ", rating_5_ratio=" + rating_5_ratio + ", total_rating=" + total_rating
            + ", total_review_cnt=" + total_review_cnt + "]";
    }

}
