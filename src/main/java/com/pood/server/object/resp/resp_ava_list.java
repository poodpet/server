package com.pood.server.object.resp;

import com.pood.server.dto.meta.goods.dto_goods_3;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class resp_ava_list {

    private List<dto_goods_3> goods_info;

    private String order_date;

    private String order_number;

    @Override
    public String toString() {
        return "resp_ava_list [goods_info=" + goods_info + ", order_date=" + order_date + ", order_number="
            + order_number + "]";
    }

}
