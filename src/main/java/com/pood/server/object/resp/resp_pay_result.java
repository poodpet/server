package com.pood.server.object.resp;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class resp_pay_result {

    private Boolean isSuccess;

    private String msg;

    @Override
    public String toString() {
        return "resp_pay_success [isSuccess=" + isSuccess + ", msg=" + msg + "]";
    }

}
