package com.pood.server.object.resp;

import com.pood.server.object.meta.vo_category_image;
import com.pood.server.object.meta.vo_main_data;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class resp_main {

    private String main_title;

    private Integer priority;

    private Integer status;

    private Integer visible;

    private List<vo_category_image> category;

    private List<vo_main_data> main_data;

}
