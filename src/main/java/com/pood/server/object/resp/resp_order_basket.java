package com.pood.server.object.resp;

import com.pood.server.dto.meta.goods.dto_goods_15;
import com.pood.server.dto.user.dto_user_coupon_2;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class resp_order_basket {

    private dto_user_coupon_2 coupon;

    private dto_goods_15 goods_info;

    private Integer qty;

    private Integer shoppingbag_idx;

    @Override
    public String toString() {
        return "resp_order_basket [coupon=" + coupon + ", goods_info=" + goods_info + ", qty=" + qty
            + ", shoppingbag_idx=" + shoppingbag_idx + "]";
    }

}
