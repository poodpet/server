package com.pood.server.object.resp;

import com.pood.server.object.meta.vo_category_list;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class resp_category {
    
    private Integer pc_id;

    private List<vo_category_list> data;

}
