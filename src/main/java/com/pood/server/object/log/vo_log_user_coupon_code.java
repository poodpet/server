package com.pood.server.object.log;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_log_user_coupon_code {

    private String code;

    private String user_uuid;

    @Override
    public String toString() {
        return "vo_log_user_coupon_code [code=" + code + ", user_uuid=" + user_uuid + "]";
    }

}
