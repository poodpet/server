package com.pood.server.object.log;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_log_user_order {

    private Integer order_idx;

    private String user_uuid;

    private String order_number;

    private Integer order_status;

    private String order_text;

    private Integer goods_idx;

    private Integer qty;

}
