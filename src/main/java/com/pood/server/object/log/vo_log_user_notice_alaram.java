package com.pood.server.object.log;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_log_user_notice_alaram {

    private Integer user_idx;

    private String user_uuid;

    private Integer alaram_type;

    private String title;

    private String message;

    private String schem;

    private String link_url;

    @Override
    public String toString() {
        return "log_user_notice_alaram [alaram_type=" + alaram_type + ", link_url=" + link_url + ", message=" + message
            + ", schem=" + schem + ", title=" + title + ", user_idx=" + user_idx + ", user_uuid=" + user_uuid + "]";
    }

}
