package com.pood.server.object.log;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_log_user_save_point {

    private String user_uuid;

    private String point_name;

    private Integer point_type;

    private Integer point_price;

    private Integer point_rate;

    private Integer type;

    private String  text;

    private Integer save_point;

    private String  expired_date;

    @Override
    public String toString() {
        return "log_user_save_point [expired_date=" + expired_date + ", point_name=" + point_name + ", point_price="
            + point_price + ", point_rate=" + point_rate + ", point_type=" + point_type + ", save_point="
            + save_point + ", text=" + text + ", type=" + type + ", user_uuid=" + user_uuid + "]";
    }

}
