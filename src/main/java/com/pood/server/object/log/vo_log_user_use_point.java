package com.pood.server.object.log;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_log_user_use_point {

    private String user_uuid;

    private Integer type;

    private String text;

    private Integer use_point;

    @Override
    public String toString() {
        return "log_user_use_point [text=" + text + ", type=" + type + ", use_point=" + use_point + ", user_uuid="
            + user_uuid + "]";
    }

}
