package com.pood.server.object;

public class pagingSet {

    private Integer page_size;

    private Integer page_number;

    private String recordbirth;

    private Integer total_cnt;

    public pagingSet(){
        
    }

    public pagingSet(Integer page_size, Integer page_number, String recordbirth, Integer total_cnt){
        this.page_size = page_size;
        this.page_number = page_number;
        this.recordbirth = recordbirth;
        this.total_cnt = total_cnt;
    }

    public Integer getPage_size() {
        return page_size;
    }

    public void setPage_size(Integer page_size) {
        this.page_size = page_size;
    }

    public Integer getPage_number() {
        return page_number;
    }

    public void setPage_number(Integer page_number) {
        this.page_number = page_number;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public Integer getTotal_cnt() {
        return total_cnt;
    }

    public void setTotal_cnt(Integer total_cnt) {
        this.total_cnt = total_cnt;
    }

    @Override
    public String toString() {
        return "pagingSet [page_number=" + page_number + ", page_size=" + page_size + ", recordbirth=" + recordbirth
                + ", total_cnt=" + total_cnt + "]";
    }

    
}
