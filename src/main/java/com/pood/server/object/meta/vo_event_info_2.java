package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_event_info_2 {

    private Integer coupon_idx;

    private Integer event_type_idx;

    private String title;

    private String intro;

    private Integer status;

    private String  startdate;

    private String  end_date;

    private Integer pc_id;

    private String  scheme_uri;

    private Integer brand_idx;

    private String  bg_color;

    private String  event_btn;

    private Integer priority;

    @Override
    public String toString() {
        return "event_info_2 [bg_color=" + bg_color + ", brand_idx=" + brand_idx + ", coupon_idx=" + coupon_idx
            + ", end_date=" + end_date + ", event_btn=" + event_btn + ", event_type_idx=" + event_type_idx
            + ", intro=" + intro + ", pc_id=" + pc_id + ", priority=" + priority + ", scheme_uri=" + scheme_uri
            + ", startdate=" + startdate + ", status=" + status + ", title=" + title + "]";
    }


}
