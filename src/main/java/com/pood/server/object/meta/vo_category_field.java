package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_category_field {

    private Integer ct_idx;

    private String field_key;

    private String field_value;

    @Override
    public String toString() {
        return "category_field [ct_idx=" + ct_idx + ", field_key=" + field_key + ", field_value=" + field_value + "]";
    }

}
