package com.pood.server.object.meta;

import com.pood.server.dto.meta.order.dto_order_history;
import com.pood.server.dto.user.dto_user_coupon_2;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_order_info {

    private Integer used_point;

    private Integer delivery_fee;

    private Integer total_discount_price;

    private Integer total_pr_price;

    private dto_user_coupon_2 coupon;

    private vo_order_delivery order_delivery;

    private List<dto_order_history> order_history;

    @Override
    public String toString() {
        return "order_info [coupon=" + coupon + ", delivery_fee=" + delivery_fee + ", order_delivery=" + order_delivery
            + ", order_history=" + order_history + ", total_discount_price=" + total_discount_price
            + ", total_pr_price=" + total_pr_price + ", used_point=" + used_point + "]";
    }

}
