package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class banner {

    private Integer pc_id;

    private Integer page_landing_idx;

    private Integer importance;

    private Integer priority;

    private Integer type;

    @Override
    public String toString() {
        return "banner [importance=" + importance + ", page_landing_idx=" + page_landing_idx + ", pc_id=" + pc_id
            + ", priority=" + priority + ", type=" + type + "]";
    }

}
