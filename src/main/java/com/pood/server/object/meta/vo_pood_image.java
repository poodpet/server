package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_pood_image {

    private Integer idx;

    private String description;

    private String url;

    private String updatetime;

    private String recordbirth;

    @Override
    public String toString() {
        return "pood_image [description=" + description + ", idx=" + idx + ", recordbirth=" + recordbirth
            + ", updatetime=" + updatetime + ", url=" + url + "]";
    }

}
