package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_pet {

    private Integer pc_id;

    private String pc_kind;

    private String pc_kind_en;

    private String pc_tag;

    private String pc_size;

    private String pc_height_male;

    private String pc_height_female;

    private String pc_weight_male;

    private String pc_weight_female;

    private String pc_life_expectancy;

    private String pc_generic_weak;

    private String pc_country;

    @Override
    public String toString() {
        return "pet [pc_country=" + pc_country + ", pc_generic_weak=" + pc_generic_weak + ", pc_height_female="
            + pc_height_female + ", pc_height_male=" + pc_height_male + ", pc_id=" + pc_id + ", pc_kind=" + pc_kind
            + ", pc_kind_en=" + pc_kind_en + ", pc_life_expectancy=" + pc_life_expectancy + ", pc_size=" + pc_size
            + ", pc_tag=" + pc_tag + ", pc_weight_female=" + pc_weight_female + ", pc_weight_male=" + pc_weight_male
            + "]";
    }

}
