package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_grain_size {

    private String name;

    private Float size_min;

    private Float size_max;

    @Override
    public String toString() {
        return "grain_size [name=" + name + ", size_max=" + size_max + ", size_min=" + size_min + "]";
    }

}
