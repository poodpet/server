package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_pet_act {

    private Float pet_weight;

    private Integer pet_activity;

}
