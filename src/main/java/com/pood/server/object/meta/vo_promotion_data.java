package com.pood.server.object.meta;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_promotion_data {

    private Integer pc_id;

    private String title;

    private String type;

    private Integer priority;

    private List<vo_promotion_data_image> image;

    private List<vo_promotion_data_prgroup> group;

}
