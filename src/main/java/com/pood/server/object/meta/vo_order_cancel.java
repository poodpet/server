package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_order_cancel {

    private String order_number;

    private String uuid;

    private Integer goods_idx;

    private Integer qty;

    private String text;

    private Integer type;

    @Override
    public String toString() {
        return "vo_order_cancel [goods_idx=" + goods_idx + ", order_number=" + order_number + ", qty=" + qty + ", text="
            + text + ", type=" + type + ", uuid=" + uuid + "]";
    }

}
