package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_category_image {

    private Integer ct_idx;

    private String name;

    private String url;

    private Integer visible;

    private Integer type;

    private Integer priority;

}
