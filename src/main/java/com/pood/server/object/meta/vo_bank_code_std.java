package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_bank_code_std {

    private String code;

    private String name;

    @Override
    public String toString() {
        return "bank_code_std [code=" + code + ", name=" + name + "]";
    }

}
