package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_product extends vo_product_2 {

    private Integer idx;

    private Integer matchCnt;

}
