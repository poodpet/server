package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_brand_image {

    private Integer image_idx;

    private String recordbirth;

    private Integer visible;

    private Integer type;

    private Integer priority;

    private String updatetime;

    private String url;

}
