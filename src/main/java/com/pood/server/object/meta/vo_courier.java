package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_courier {

    private String code;

    private String name;

    private Boolean isDefault;

}
