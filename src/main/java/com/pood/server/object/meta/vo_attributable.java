package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_attributable {

    private Integer type1;

    private Integer type2;

    private Integer code;

    private String text;

}
