package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_order_delivery_track {

    private String order_number;

    private String courier;

    private Integer type;

    private Integer send;

    private String tracking_id;

    private Integer send_type;

    private Integer goods_idx;

    private Integer history_idx;

    private Boolean main_address;

    private Integer qty;

    private String startdate;

    @Override
    public String toString() {
        return "order_delivery_track [courier=" + courier + ", goods_idx=" + goods_idx + ", history_idx=" + history_idx
            + ", main_address=" + main_address + ", order_number=" + order_number + ", qty=" + qty + ", send="
            + send + ", send_type=" + send_type + ", startdate=" + startdate + ", tracking_id=" + tracking_id
            + ", type=" + type + "]";
    }

}
