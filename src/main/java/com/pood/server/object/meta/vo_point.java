package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_point {

    private String point_name;

    private Integer point_type;

    private Integer point_price;

    private Integer point_rate;

    private Integer expired_day;

}
