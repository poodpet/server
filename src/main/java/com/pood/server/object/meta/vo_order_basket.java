package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_order_basket {

    private Integer order_idx;

    private String order_number;

    private Integer coupon_idx;

    private Integer goods_idx;

    private Integer goods_price;

    private Integer quantity;

    private Integer basket_idx;

    private Integer seller_idx;

    @Override
    public String toString() {
        return "order_basket [basket_idx=" + basket_idx + ", coupon_idx=" + coupon_idx + ", goods_idx=" + goods_idx
            + ", goods_price=" + goods_price + ", order_idx=" + order_idx + ", order_number=" + order_number
            + ", quantity=" + quantity + ", seller_idx=" + seller_idx + "]";
    }

}
