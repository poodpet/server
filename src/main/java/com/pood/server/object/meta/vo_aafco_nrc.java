package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_aafco_nrc {

    private Integer pet_type;

    private Integer pet_idx;

    private Float pr_protein;

    private Float am_arginine;

    private Float am_histidine;

    private Float am_isoleucine;

    private Float am_methionine;

    private Float am_cystine;

    private Float am_leucine;

    private Float am_lysine;

    private Float am_phenylanlanine;

    private Float am_tyrosine;

    private Float am_threonine;

    private Float am_tryptophan;

    private Float am_valine;

    private Float am_glutamic_acid;

    private Float ot_taurine;

    private Float pr_fat;

    private Float fa_linoleicacid;

    private Float fa_arachidonic_acid;

    private Float fa_epa_dha;

    private Float mi_calcium;

    private Float mi_phosphours;

    private Float mi_magnessium;

    private Float mi_sodium;

    private Float mi_potassium;

    private Float mi_chloride;

    private Float mi_iron;

    private Float mi_copper;

    private Float mi_zinc;

    private Float mi_manganese;

    private Float mi_selenium;

    private Float mi_iodine;

    private Float vi_vitamin_a;

    private Float vi_vitamin_d;

    private Float vi_vitamin_e;

    private Float vi_vitamin_k3;

    private Float vi_vitamin_b1;

    private Float vi_vitamin_b2;

    private Float vi_vitamin_b3;

    private Float vi_vitamin_b5;

    private Float vi_vitamin_b6;

    private Float vi_vitamin_b7;

    private Float vi_vitamin_b9;

    private Float vi_vitamin_b12;

    private String mrs;

    @Override
    public String toString() {
        return "aafco_nrc [am_arginine=" + am_arginine + ", am_cystine=" + am_cystine + ", am_glutamic_acid="
            + am_glutamic_acid + ", am_histidine=" + am_histidine + ", am_isoleucine=" + am_isoleucine
            + ", am_leucine=" + am_leucine + ", am_lysine=" + am_lysine + ", am_methionine=" + am_methionine
            + ", am_phenylanlanine=" + am_phenylanlanine + ", am_threonine=" + am_threonine + ", am_tryptophan="
            + am_tryptophan + ", am_tyrosine=" + am_tyrosine + ", am_valine=" + am_valine + ", fa_arachidonic_acid="
            + fa_arachidonic_acid + ", fa_epa_dha=" + fa_epa_dha + ", fa_linoleicacid=" + fa_linoleicacid
            + ", mi_calcium=" + mi_calcium + ", mi_chloride=" + mi_chloride + ", mi_copper=" + mi_copper
            + ", mi_iodine=" + mi_iodine + ", mi_iron=" + mi_iron + ", mi_magnessium=" + mi_magnessium
            + ", mi_manganese=" + mi_manganese + ", mi_phosphours=" + mi_phosphours + ", mi_potassium="
            + mi_potassium + ", mi_selenium=" + mi_selenium + ", mi_sodium=" + mi_sodium + ", mi_zinc=" + mi_zinc
            + ", mrs=" + mrs + ", ot_taurine=" + ot_taurine + ", pet_idx=" + pet_idx + ", pet_type=" + pet_type
            + ", pr_fat=" + pr_fat + ", pr_protein=" + pr_protein + ", vi_vitamin_a=" + vi_vitamin_a
            + ", vi_vitamin_b1=" + vi_vitamin_b1 + ", vi_vitamin_b12=" + vi_vitamin_b12 + ", vi_vitamin_b2="
            + vi_vitamin_b2 + ", vi_vitamin_b3=" + vi_vitamin_b3 + ", vi_vitamin_b5=" + vi_vitamin_b5
            + ", vi_vitamin_b6=" + vi_vitamin_b6 + ", vi_vitamin_b7=" + vi_vitamin_b7 + ", vi_vitamin_b9="
            + vi_vitamin_b9 + ", vi_vitamin_d=" + vi_vitamin_d + ", vi_vitamin_e=" + vi_vitamin_e
            + ", vi_vitamin_k3=" + vi_vitamin_k3 + "]";
    }

}
