package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_product_sub_ct {

    private Integer ct_idx;

    private Integer show_index;

    private String ct_sub_name;

    private Integer pet_idx;

}
