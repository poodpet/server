package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_feed_other_product {

    private String feed_name;

    private String brand;

    private Integer category;

    private String main_ingredient;

    private String animal_ingredient;

    private String vegetable_ingredient;

    private Integer gluten_free;

    private Integer gmo_free;

    private Integer price;

    private Integer grade;

    private Integer additives;

    private String manufacturer;

    private String country;

    private String target;

    private String targetHint;

    private String unitSize;

    private Integer calorie;

    private Float calorie_calc;

    private Float protein;

    private Float fat;

    private Float ash;

    private Float fiber;

    private Float moisture;

    private Float carbohydrate;

    private Float calcium;

    private Float phosphorus;

    private Float omega3;

    private Float omega6;

    private String ingredients;

    @Override
    public String toString() {
        return "feed_other_product [additives=" + additives + ", animal_ingredient=" + animal_ingredient + ", ash="
            + ash + ", brand=" + brand + ", calcium=" + calcium + ", calorie=" + calorie + ", calorie_calc="
            + calorie_calc + ", carbohydrate=" + carbohydrate + ", category=" + category + ", country=" + country
            + ", fat=" + fat + ", feed_name=" + feed_name + ", fiber=" + fiber + ", gluten_free=" + gluten_free
            + ", gmo_free=" + gmo_free + ", grade=" + grade + ", ingredients=" + ingredients + ", main_ingredient="
            + main_ingredient + ", manufacturer=" + manufacturer + ", moisture=" + moisture + ", omega3=" + omega3
            + ", omega6=" + omega6 + ", phosphorus=" + phosphorus + ", price=" + price + ", protein=" + protein
            + ", target=" + target + ", targetHint=" + targetHint + ", unitSize=" + unitSize
            + ", vegetable_ingredient=" + vegetable_ingredient + "]";
    }

}
