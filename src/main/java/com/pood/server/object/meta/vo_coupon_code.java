package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_coupon_code {

    private Integer coupon_idx;

    private String code;

    private String user_uuid;

    @Override
    public String toString() {
        return "vo_coupon_code [code=" + code + ", coupon_idx=" + coupon_idx + ", user_uuid=" + user_uuid + "]";
    }

}
