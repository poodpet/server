package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_brand {

    private String brand_name;

    private String brand_name_en;

    private String brand_origin;

    private String brand_description;

    private String brand_intro;

    private String brand_logo;

    private String brand_tag;

    private String brand_menufac;

    private String brand_seller;

    private String brand_phone;

    private Integer brand_visible;

    private Integer pet_idx;

    private Integer pood_grade;

    @Override
    public String toString() {
        return "brand [brand_description=" + brand_description + ", brand_intro=" + brand_intro + ", brand_logo="
            + brand_logo + ", brand_menufac=" + brand_menufac + ", brand_name=" + brand_name + ", brand_name_en="
            + brand_name_en + ", brand_origin=" + brand_origin + ", brand_phone=" + brand_phone + ", brand_seller="
            + brand_seller + ", brand_tag=" + brand_tag + ", brand_visible=" + brand_visible + ", pet_idx="
            + pet_idx + ", pood_grade=" + pood_grade + "]";
    }

}
