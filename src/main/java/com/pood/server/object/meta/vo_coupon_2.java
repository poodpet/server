package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_coupon_2 {

    private Integer pc_id;

    private Integer issued_type;

    private Integer issued_count;

    private String available_time;

    private Integer over_type;

    private Integer publish_type;

    private Integer limit_price;

    private Integer max_price;

    private Integer discount_rate;

    private String description;

    private String name;

    private String tag;

    private Integer goods_type;

    private Integer type;

    private Integer payment_type;

    private Integer apply_coupon_type;

    private Integer publish_type_idx;

    private Integer available_day;

    @Override
    public String toString() {
        return "vo_coupon_2 [apply_coupon_type=" + apply_coupon_type + ", available_time="
            + available_time
            + ", description=" + description + ", discount_rate=" + discount_rate + ", goods_type="
            + goods_type
            + ", issued_count=" + issued_count + ", issued_type=" + issued_type + ", limit_price="
            + limit_price
            + ", max_price=" + max_price + ", name=" + name + ", over_type=" + over_type
            + ", payment_type="
            + payment_type + ", pc_id=" + pc_id + ", publish_type=" + publish_type
            + ", publish_type_idx="
            + publish_type_idx + ", tag=" + tag + ", type=" + type + "]";
    }

}
