package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_product_3 {

    private Integer weight;

    private Integer cup_weight;

    private String product_code;

    private String uuid;

    private Integer pc_idx;

    private Integer ct_idx;

    private Integer ct_sub_idx;

    private String ct_sub_name;

    private Integer show_index;

    private Float unit_size;

    private String product_name;

    private String tag;

    private String main_property;

    private String tasty;

    private String product_video;

    private Integer calorie;

    private Integer feed_target;

    private String feed_type;

    private Integer gluten_free;

    private String animal_protein;

    private String vegetable_protein;

    private String aafco;

    private Integer single_protein;

    private Integer all_nutrients;

    private String notice_desc;

    private String notice_title;

    private String package_type;

    private String ingredients;

    private String ingredients_search;

    private Integer is_recommend;

    private Integer quantity;

    private Integer ava_quantity;

    @Override
    public String toString() {
        return "product_3 [aafco=" + aafco + ", all_nutrients=" + all_nutrients + ", animal_protein=" + animal_protein
            + ", ava_quantity=" + ava_quantity + ", calorie=" + calorie + ", ct_idx=" + ct_idx + ", ct_sub_idx="
            + ct_sub_idx + ", ct_sub_name=" + ct_sub_name + ", cup_weight=" + cup_weight + ", feed_target="
            + feed_target + ", feed_type=" + feed_type + ", gluten_free=" + gluten_free + ", ingredients="
            + ingredients + ", ingredients_search=" + ingredients_search + ", is_recommend=" + is_recommend
            + ", main_property=" + main_property + ", notice_desc=" + notice_desc + ", notice_title=" + notice_title
            + ", package_type=" + package_type + ", pc_idx=" + pc_idx + ", product_code=" + product_code
            + ", product_name=" + product_name + ", product_video=" + product_video + ", quantity=" + quantity
            + ", show_index=" + show_index + ", single_protein=" + single_protein + ", tag=" + tag + ", tasty="
            + tasty + ", unit_size=" + unit_size + ", uuid=" + uuid + ", vegetable_protein=" + vegetable_protein
            + ", weight=" + weight + "]";
    }

}
