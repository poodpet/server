package com.pood.server.object.meta;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_main_data {

    private Integer visible;

    private String ct_name;

    private String main_type;

    private String displayType;

    private String title;

    private String description;

    private Integer number_tag;

    private Integer priority;

    private String main_tab_start_date;

    private String main_tab_end_date;

    private List<vo_main_data_goods> goods;

    private List<vo_main_data_brand> brand;

    private List<vo_main_data_banner> banner;

    private List<vo_main_data_promotion> promotion;

    private List<vo_main_data_event> event;

}
