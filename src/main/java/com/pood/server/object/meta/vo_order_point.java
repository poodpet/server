package com.pood.server.object.meta;

import lombok.Getter;

@Getter

public class vo_order_point {

    private String order_number;

    private String point_uuid;

    private Integer used_point;

    private Boolean type;

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public String getPoint_uuid() {
        return point_uuid;
    }

    public void setPoint_uuid(String point_uuid) {
        this.point_uuid = point_uuid;
    }

    public Integer getUsed_point() {
        return used_point;
    }

    public void setUsed_point(Integer used_point) {
        this.used_point = used_point;
    }

    public Boolean getType() {
        return type;
    }

    public void setType(Boolean type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "order_point [order_number=" + order_number + ", point_uuid=" + point_uuid + ", type=" + type
            + ", used_point=" + used_point + "]";
    }


}
