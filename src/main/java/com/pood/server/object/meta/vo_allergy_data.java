package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_allergy_data {

    private String name;

    private Integer type;

    @Override
    public String toString() {
        return "allergy_data [name=" + name + ", type=" + type + "]";
    }

}
