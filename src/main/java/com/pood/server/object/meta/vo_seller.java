package com.pood.server.object.meta;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_seller {

    private String seller_name;

    private String seller_intro;

    private Integer shipping_fee;

    private Integer free_shipping_limit;

    private Integer jeju_remote;

    private Integer jeju_remote_etc;

    private Integer island_remote;

    private Integer etc_remote;

    private String exchange_desc;

    private Integer exchange_fee;

    private Integer refund_day;

    private String refund_desc;

    private List<vo_seller_image> image;

    @Override
    public String toString() {
        return "seller [etc_remote=" + etc_remote + ", exchange_desc=" + exchange_desc + ", exchange_fee="
            + exchange_fee + ", free_shipping_limit=" + free_shipping_limit + ", image=" + image
            + ", island_remote=" + island_remote + ", jeju_remote=" + jeju_remote + ", jeju_remote_etc="
            + jeju_remote_etc + ", refund_day=" + refund_day + ", refund_desc=" + refund_desc + ", seller_intro="
            + seller_intro + ", seller_name=" + seller_name + ", shipping_fee=" + shipping_fee + "]";
    }

}
