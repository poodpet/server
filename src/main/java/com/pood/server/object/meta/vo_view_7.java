package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_view_7 {

    private Integer goods_idx;

    private Integer product_idx;

    private Integer product_qty;

    private Integer sale_status;

    @Override
    public String toString() {
        return "vo_view_7 [goods_idx=" + goods_idx + ", product_idx=" + product_idx + ", product_qty=" + product_qty
            + ", sale_status=" + sale_status + "]";
    }

}
