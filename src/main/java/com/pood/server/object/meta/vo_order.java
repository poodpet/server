package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_order {

    private Integer total_price;

    private Integer total_discount_price;

    private Integer address_update;

    private Integer free_purchase;

    private Integer over_coupon_idx;

    private String user_uuid;

    private Integer user_idx;

    private String order_device;

    private Integer order_status;

    private String order_number;

    private String recordbirth;

    private String order_name;

    private String tracking_id;

    private Integer order_price;

    private Integer order_price_2;

    private Integer order_type;

    private Integer saved_point;

    private Integer used_point;

    private String memo;

    private Integer delivery_fee;

    private Integer delivery_type;

    private Integer discount_coupon_price;

    @Override
    public String toString() {
        return "vo_order [address_update=" + address_update + ", delivery_fee=" + delivery_fee + ", delivery_type="
            + delivery_type + ", discount_coupon_price=" + discount_coupon_price + ", free_purchase="
            + free_purchase + ", memo=" + memo + ", order_device=" + order_device + ", order_name=" + order_name
            + ", order_number=" + order_number + ", order_price=" + order_price + ", order_price_2=" + order_price_2
            + ", order_status=" + order_status + ", order_type=" + order_type + ", over_coupon_idx="
            + over_coupon_idx + ", recordbirth=" + recordbirth + ", saved_point=" + saved_point
            + ", total_discount_price=" + total_discount_price + ", total_price=" + total_price + ", tracking_id="
            + tracking_id + ", used_point=" + used_point + ", user_idx=" + user_idx + ", user_uuid=" + user_uuid
            + "]";
    }

}
