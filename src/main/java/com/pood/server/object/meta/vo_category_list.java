package com.pood.server.object.meta;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_category_list {

    private String title;

    private Integer priority;

    private Integer visible;

    private List<vo_category_data> category;

}
