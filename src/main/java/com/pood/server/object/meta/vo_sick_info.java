package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_sick_info {

    private String sick_name;

    private Integer sick_index;

    private Integer sick_group_code;

    private Integer pc_id;

    private String  nutrition_info;

    private String  protein_range;

    private String  fat_range;

    private String  carbo_range;

    private String  fiber_range;

    private String  description;

}
