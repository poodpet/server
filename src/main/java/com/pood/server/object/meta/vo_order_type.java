package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_order_type {

    private Integer idx;

    private Integer value;

    private String text;

    @Override
    public String toString() {
        return "order_type [idx=" + idx + ", text=" + text + ", value=" + value + "]";
    }

}
