package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_notice {

    private String admin_uuid;

    private Integer type;

    private String title;

    private String text;

    private Boolean isVisible;

}
