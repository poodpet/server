package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_payment_request_text {

    private String text;

    private Integer type;

    @Override
    public String toString() {
        return "payment_request_text [text=" + text + ", type=" + type + "]";
    }

}
