package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class main_goods_list {

    private Integer main_idx;

    private Integer goods_idx;

    private Integer goods_type;

    private Integer priority;

    @Override
    public String toString() {
        return "main_goods_list [goods_idx=" + goods_idx + ", goods_type=" + goods_type + ", main_idx=" + main_idx
            + ", priority=" + priority + "]";
    }

}
