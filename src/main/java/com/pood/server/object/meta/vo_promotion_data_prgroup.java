package com.pood.server.object.meta;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_promotion_data_prgroup {

    private String name;

    private List<vo_promotion_data_prgroup_goods> goods;

}
