package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_goods {

    private Integer pc_idx;

    private Integer seller_idx;

    private Integer goods_type_idx;

    private Integer coupon_apply;

    private String display_type;

    private String goods_name;

    private String goods_descv;

    private Integer goods_origin_price;

    private Integer goods_price;

    private Integer discount_rate;

    private Integer discount_price;

    private Integer refund_type;

    private Integer quantity;

    private Integer purchase_type;

    private Integer sale_status;

    private String startdate;

    private String end_date;

    private Integer visible;

    private Integer isPromotion;

    private Integer pr_price;

    private Integer pr_discount_price;

    private Integer pr_discount_rate;

    private Integer limit_quantity;

    private Integer main_product;

    private Double average_rating;

    private Integer review_cnt;

    @Override
    public String toString() {
        return "vo_goods [average_rating=" + average_rating + ", coupon_apply=" + coupon_apply + ", discount_price="
            + discount_price + ", discount_rate=" + discount_rate + ", display_type=" + display_type + ", end_date="
            + end_date + ", goods_descv=" + goods_descv + ", goods_name=" + goods_name + ", goods_origin_price="
            + goods_origin_price + ", goods_price=" + goods_price + ", goods_type_idx=" + goods_type_idx
            + ", isPromotion=" + isPromotion + ", limit_quantity=" + limit_quantity + ", main_product="
            + main_product + ", pc_idx=" + pc_idx + ", pr_discount_price=" + pr_discount_price
            + ", pr_discount_rate=" + pr_discount_rate + ", pr_price=" + pr_price + ", purchase_type="
            + purchase_type + ", quantity=" + quantity + ", refund_type=" + refund_type + ", review_cnt="
            + review_cnt + ", sale_status=" + sale_status + ", seller_idx=" + seller_idx + ", startdate="
            + startdate + ", visible=" + visible + "]";
    }

}
