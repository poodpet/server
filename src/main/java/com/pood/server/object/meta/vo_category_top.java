package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_category_top {

    private String name;

    private Integer priority;

    private Boolean visible;

}
