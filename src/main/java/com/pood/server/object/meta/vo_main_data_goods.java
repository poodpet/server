package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_main_data_goods {

    private Integer goods_idx;

    private String goods_name;

    private Integer discount_rate;

    private Integer goods_price;

    private String image;

    private Integer priority;

    private Integer visible;

    private Double average_rating;

    private Integer review_cnt;

}
