package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_disease_group {

    private Integer pet_idx;

    private String dgn_name;

    private String dgn_group;

    private Integer dgn_group_code;

    private String dgn_nutrition;

    private String dgn_nutrition_core;

    @Override
    public String toString() {
        return "disease_group [dgn_group=" + dgn_group + ", dgn_group_code=" + dgn_group_code + ", dgn_name=" + dgn_name
            + ", dgn_nutrition=" + dgn_nutrition + ", dgn_nutrition_core=" + dgn_nutrition_core + ", pet_idx="
            + pet_idx + "]";
    }

}
