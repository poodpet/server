package com.pood.server.object.meta.maintab;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_maintab_banner {

    private Integer maintab_idx;

    private Integer banner_type;

    private Integer page_landing_idx;

    private Integer priority;

    private String title;

    private String url;

}
