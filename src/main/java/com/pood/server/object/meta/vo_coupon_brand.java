package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_coupon_brand {

    private Integer coupon_idx;

    private Integer brand_idx;

    @Override
    public String toString() {
        return "vo_coupon_brand [brand_idx=" + brand_idx + ", coupon_idx=" + coupon_idx + "]";
    }

}
