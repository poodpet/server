package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_promotion {

    private Integer pc_id;

    private String name;

    private Integer status;

    private String type;

    private Integer priority;

    private String start_date;

    private String end_date;

    @Override
    public String toString() {
        return "vo_promotion [end_date=" + end_date + ", name=" + name + ", pc_id=" + pc_id + ", priority=" + priority
            + ", start_date=" + start_date + ", status=" + status + ", type=" + type + "]";
    }

}
