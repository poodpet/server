package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_promotion_data_prgroup_goods {

    private Integer discount_rate;

    private Integer goods_idx;

    private String goods_name;

    private Integer goods_price;

    private String      image;

    private Integer     priority;

    private Integer     visible;

    private String      display_type;

    private Double  average_rating;

    private Integer review_cnt;

    @Override
    public String toString() {
        return "vo_promotion_data_prgroup_goods [discount_rate=" + discount_rate + ", goods_idx=" + goods_idx
            + ", goods_name=" + goods_name + ", goods_price=" + goods_price + ", image=" + image + ", priority="
            + priority + ", visible=" + visible + "]";
    }

}
