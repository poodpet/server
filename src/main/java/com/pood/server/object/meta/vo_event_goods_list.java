package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_event_goods_list {

    private Integer event_idx;

    private Integer goods_idx;

    private Integer priority;

    private Integer goods_type;

    private String sub_title;

    private Integer event_image_idx;

}
