package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_event_type {

    private Integer idx;

    private String type_name;

    @Override
    public String toString() {
        return "event_type [idx=" + idx + ", type_name=" + type_name + "]";
    }

}
