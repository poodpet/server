package com.pood.server.object.meta.maintab;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_maintab_goods {

    private Integer maintab_idx;

    private Integer goods_idx;

    private String goods_name;

    private Integer goods_price;

    private Integer discount_rate;

    private String display_type;

    private String image;

    private Integer priority;

    private Integer visible;

    private Double average_rating;

    private Integer review_cnt;

    @Override
    public String toString() {
        return "vo_maintab_goods [average_rating=" + average_rating + ", discount_rate=" + discount_rate
            + ", display_type=" + display_type + ", goods_idx=" + goods_idx + ", goods_name=" + goods_name
            + ", goods_price=" + goods_price + ", image=" + image + ", maintab_idx=" + maintab_idx + ", priority="
            + priority + ", review_cnt=" + review_cnt + ", visible=" + visible + "]";
    }

}
