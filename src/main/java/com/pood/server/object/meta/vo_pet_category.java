package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_pet_category {

    private String pc_name;

    private String pc_name_en;

    private String pc_species;

    @Override
    public String toString() {
        return "pet_category [pc_name=" + pc_name + ", pc_name_en=" + pc_name_en + ", pc_species=" + pc_species + "]";
    }

}
