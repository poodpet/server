package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_order_delivery {

    private String address;

    private String receiver;

    private Integer remote_type;

    private Integer delivery_type;

    private String regular_date;

    private Integer input_type;

    private String startdate;

    private String zipcode;

    private String address_detail;

    private Integer default_address;

    private String courier;

    private String nickname;

    private String phone_number;

    @Override
    public String toString() {
        return "order_delivery [address=" + address + ", address_detail=" + address_detail + ", courier=" + courier
            + ", default_address=" + default_address + ", delivery_type=" + delivery_type + ", input_type="
            + input_type + ", nickname=" + nickname + ", phone_number=" + phone_number + ", receiver=" + receiver
            + ", regular_date=" + regular_date + ", remote_type=" + remote_type + ", startdate=" + startdate
            + ", zipcode=" + zipcode + "]";
    }

}
