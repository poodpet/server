package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_category {

    private Integer idx;

    private Integer tp_idx;

    private Integer pc_id;

    private String title;

    private String sub_title;

    private Integer priority;

    private Integer visible;

    private String ct_name;

    @Override
    public String toString() {
        return "category [ct_name=" + ct_name + ", idx=" + idx + ", pc_id=" + pc_id + ", priority=" + priority
            + ", sub_title=" + sub_title + ", title=" + title + ", tp_idx=" + tp_idx + ", visible=" + visible + "]";
    }

}
