package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_pet_doctor {

    private String pd_name;

    private String pd_class;

    private String pd_profile_desc;

    @Override
    public String toString() {
        return "pet_doctor [pd_class=" + pd_class + ", pd_name=" + pd_name + ", pd_profile_desc=" + pd_profile_desc
            + "]";
    }

}
