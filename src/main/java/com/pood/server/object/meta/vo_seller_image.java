package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_seller_image {

    private Integer image_idx;

    private Integer visible;

    private Integer type;

    private String updatetime;

    private String  recordbirth;

    private String  url;

}
