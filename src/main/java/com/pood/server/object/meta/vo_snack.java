package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_snack {

    private Integer type;

    private Float per_cal;

    private Float per_grams;

    private Integer min;

    private Integer max;

    private String  description;

    @Override
    public String toString() {
        return "snack [description=" + description + ", max=" + max + ", min=" + min + ", per_cal=" + per_cal
            + ", per_grams=" + per_grams + ", type=" + type + "]";
    }

}
