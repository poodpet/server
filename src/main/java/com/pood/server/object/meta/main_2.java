package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class main_2 {

    private String end_date;

    private Integer item_cnt;

    private String title;

    private Integer priority;

    private String start_date;

    private Integer status;

    private Integer pc_id;

}
