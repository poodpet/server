package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_pet_doctor_desc {

    private Integer pet_doctor_idx;

    private Integer ct_idx;

    private Integer ct_sub_idx;

    private Integer product_idx;

    private String pdfd_title;

    private String pdfd_desc;

    private String product_name;

    private Integer pc_id;

    private String position_1;

    private String position_2;

    private String position_3;

    private Integer ard_group_122;

    private Integer ard_group_421;

    private Integer ard_group_501;

    private Integer ard_group_201;

    private Integer ard_group_301;

    private Integer ard_group_961;

    private Integer ard_group_241;

    private Integer ard_group_121;

    private Integer ard_group_941;

    @Override
    public String toString() {
        return "pet_doctor_desc [ard_group_121=" + ard_group_121 + ", ard_group_122=" + ard_group_122
            + ", ard_group_201=" + ard_group_201 + ", ard_group_241=" + ard_group_241 + ", ard_group_301="
            + ard_group_301 + ", ard_group_421=" + ard_group_421 + ", ard_group_501=" + ard_group_501
            + ", ard_group_941=" + ard_group_941 + ", ard_group_961=" + ard_group_961 + ", ct_idx=" + ct_idx
            + ", ct_sub_idx=" + ct_sub_idx + ", pc_id=" + pc_id + ", pdfd_desc=" + pdfd_desc + ", pdfd_title="
            + pdfd_title + ", pet_doctor_idx=" + pet_doctor_idx + ", position_1=" + position_1 + ", position_2="
            + position_2 + ", position_3=" + position_3 + ", product_idx=" + product_idx + ", product_name="
            + product_name + "]";
    }

}
