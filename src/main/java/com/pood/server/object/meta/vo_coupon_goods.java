package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_coupon_goods {

    private Integer coupon_idx;

    private Integer goods_idx;

    @Override
    public String toString() {
        return "vo_coupon_goods [coupon_idx=" + coupon_idx + ", goods_idx=" + goods_idx + "]";
    }

}
