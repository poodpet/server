package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_promotion_group {

    private Integer pr_idx;

    private String name;

    @Override
    public String toString() {
        return "vo_promotion_group [name=" + name + ", pr_idx=" + pr_idx + "]";
    }

}
