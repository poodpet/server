package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_disease {

    private Integer pet_idx;

    private String pc_ds_name;

    private String pc_ds_group;

    private Integer pc_ds_group_code;

    private String  pc_ds_desc;

    private Integer pc_ds_hospital;

    private Integer pc_ds_priority;

    @Override
    public String toString() {
        return "disease [pc_ds_desc=" + pc_ds_desc + ", pc_ds_group=" + pc_ds_group + ", pc_ds_group_code="
            + pc_ds_group_code + ", pc_ds_hospital=" + pc_ds_hospital + ", pc_ds_name=" + pc_ds_name
            + ", pc_ds_priority=" + pc_ds_priority + ", pet_idx=" + pet_idx + "]";
    }

}
