package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_nuti {

    private String ni_name;

    private String ni_name_en;

    private String ni_tag;

    private String ni_group;

    private Integer ni_grade;

    private String ni_ds_dog;

    private String ni_ds_group_dog;

    private String ni_ds_group_dog_code;

    private String ni_ds_cat;

    private String ni_ds_group_cat;

    private String ni_ds_group_cat_code;

    private String ni_desc;

    private String ni_effect;

    private String ni_cautions;

    private String ni_nt_group;

    private Integer ni_priority;

    @Override
    public String toString() {
        return "nuti [ni_cautions=" + ni_cautions + ", ni_desc=" + ni_desc + ", ni_ds_cat=" + ni_ds_cat + ", ni_ds_dog="
            + ni_ds_dog + ", ni_ds_group_cat=" + ni_ds_group_cat + ", ni_ds_group_cat_code=" + ni_ds_group_cat_code
            + ", ni_ds_group_dog=" + ni_ds_group_dog + ", ni_ds_group_dog_code=" + ni_ds_group_dog_code
            + ", ni_effect=" + ni_effect + ", ni_grade=" + ni_grade + ", ni_group=" + ni_group + ", ni_name="
            + ni_name + ", ni_name_en=" + ni_name_en + ", ni_nt_group=" + ni_nt_group + ", ni_priority="
            + ni_priority + ", ni_tag=" + ni_tag + "]";
    }

}
