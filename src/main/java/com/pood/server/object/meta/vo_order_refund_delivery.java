package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_order_refund_delivery {

    private String user_uuid;

    private String address;

    private String address_detail;

    private String receiver;

    private String nickname;

    private String zipcode;

    private String phone_number;

}
