package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_delivery_courier {

    private String area;

    private Integer delivery_fee;

    private String courier_code;

    private String start_zipcode;

    private String  end_zipcode;

    private Integer area_type;

}
