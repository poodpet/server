package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_order_refund {

    private String order_number;

    private String uuid;

    private Integer goods_idx;

    private Integer qty;

    private Integer send_type;

    private Integer type;

    private String text;

    private Integer attr;

    @Override
    public String toString() {
        return "order_refund [attr=" + attr + ", goods_idx=" + goods_idx + ", order_number=" + order_number + ", qty="
            + qty + ", send_type=" + send_type + ", text=" + text + ", type=" + type + ", uuid=" + uuid + "]";
    }

}
