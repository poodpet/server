package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_payment_type {

    private String name;

    private String code;

    private Integer visible;

    private Integer priority;

    private Integer type;

    private String type_name;

    private String pg;

    @Override
    public String toString() {
        return "payment_type [code=" + code + ", name=" + name + ", pg=" + pg + ", priority=" + priority + ", type="
            + type + ", type_name=" + type_name + ", visible=" + visible + "]";
    }

}
