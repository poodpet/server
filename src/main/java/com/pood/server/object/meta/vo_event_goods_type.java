package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_event_goods_type {

    private Integer idx;

    private String name;

    private String brand_idx;

}
