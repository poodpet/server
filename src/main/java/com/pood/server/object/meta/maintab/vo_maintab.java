package com.pood.server.object.meta.maintab;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_maintab {

    private Integer ct_idx;

    private String ct_name;

    private Integer pc_id;

    private String start_date;

    private String end_date;

    private Integer status;

    private String title;

    private String title_desc;

    private String display_type;

    private Integer number_tag;

    private Integer visible;

    private Integer type;

    private String type_name;

    private Integer priority;

}
