package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_promotion_group_goods {

    private Integer pr_idx;

    private Integer pr_group_idx;

    private Integer goods_idx;

    private String goods_name;

    private Integer goods_price;

    private Integer discount_rate;

    private String image;

    private Integer priority;

    private Integer visible;

    private String display_type;

    private Double average_rating;

    private Integer review_cnt;

    private Integer pr_price;

    private Integer pr_discount_rate;

    private Integer pr_discount_price;

    @Override
    public String toString() {
        return "vo_promotion_group_goods [average_rating=" + average_rating + ", discount_rate=" + discount_rate
            + ", display_type=" + display_type + ", goods_idx=" + goods_idx + ", goods_name=" + goods_name
            + ", goods_price=" + goods_price + ", image=" + image + ", pr_group_idx=" + pr_group_idx + ", pr_idx="
            + pr_idx + ", priority=" + priority + ", review_cnt=" + review_cnt + ", visible=" + visible + "]";
    }

}
