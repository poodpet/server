package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_main_data_brand {

    private String title;

    private String description;

    private String image;

    private Integer priority;

    private Integer visible;

    private Integer brand_idx;

}
