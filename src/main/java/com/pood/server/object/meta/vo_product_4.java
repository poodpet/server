package com.pood.server.object.meta;

import com.pood.server.dto.meta.brand.dto_brand_3;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_product_4 extends vo_product_2 {

    private dto_brand_3 brand;
}
