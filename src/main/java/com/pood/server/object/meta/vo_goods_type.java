package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_goods_type {

    private String goods_type_name;

    @Override
    public String toString() {
        return "goods_type [goods_type_name=" + goods_type_name + "]";
    }

}