package com.pood.server.object.meta.maintab;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_maintab_brand {

    private Integer maintab_idx;

    private Integer brand_idx;

    private Integer priority;

    private String image;

    private Boolean visible;

    private String title;

    private String description;

}
