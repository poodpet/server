package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_ai_recommend_diagnosis {

    private String ard_name;

    private String ard_group;

    private Integer ard_group_code;

    @Override
    public String toString() {
        return "ai_recommend_diagnosis [ard_group=" + ard_group + ", ard_group_code=" + ard_group_code + ", ard_name="
            + ard_name + "]";
    }

}
