package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_main_data_banner {

    private String image;

    private Integer priority;

    private Integer page_landing_idx;

    private Integer type;

}
