package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_promotion_data_image {

    private String url;

    private Integer visible;

    private Integer type;

    private Integer priority;

    @Override
    public String toString() {
        return "vo_promotion_data_image [priority=" + priority + ", type=" + type + ", url=" + url + ", visible="
            + visible + "]";
    }

}
