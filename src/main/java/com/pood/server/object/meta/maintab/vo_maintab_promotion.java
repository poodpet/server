package com.pood.server.object.meta.maintab;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_maintab_promotion {

    private Integer maintab_idx;

    private Integer pr_idx;

    private Boolean visible;

    private Integer priority;

    private String name;

    private String start_date;

    private String end_date;

    private String url;

    private Integer page_landing_idx;

}