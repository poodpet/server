package com.pood.server.object.meta;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_category_data {

    private String title;

    private Integer priority;

    private Integer type;

    private String url;

    private Integer visible;

    private String sub_title;

    private List<vo_category_data_field> field;

    @Override
    public String toString() {
        return "category_data [field=" + field + ", priority=" + priority + ", sub_title=" + sub_title + ", title="
            + title + ", type=" + type + ", url=" + url + ", visible=" + visible + "]";
    }

}
