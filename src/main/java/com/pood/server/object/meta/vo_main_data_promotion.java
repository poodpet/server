package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_main_data_promotion {

    private String title;

    private String image;

    private Integer priority;

    private Integer visible;

    private String startTime;

    private String endTime;

    private Integer page_landing_idx;

    @Override
    public String toString() {
        return "vo_main_data_promotion [endTime=" + endTime + ", image=" + image + ", page_landing_idx="
            + page_landing_idx + ", priority=" + priority + ", startTime=" + startTime + ", title=" + title
            + ", visible=" + visible + "]";
    }

}
