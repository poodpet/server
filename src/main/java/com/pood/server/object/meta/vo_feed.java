package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_feed {

    private Integer product_idx;

    private String feed_name;

    private Float pr_protein;

    private Float pr_fat;

    private Float pr_fiber;

    private Float pr_ash;

    private Float pr_moisture;

    private Float pr_carbo;

    private Float am_arginine;

    private Float am_histidine;

    private Float am_isoleucine;

    private Float am_leucine;

    private Float am_lysine;

    private Float am_met_cys;

    private Float am_methionine;

    private Float am_phe_tyr;

    private Float am_phenylanlanine;

    private Float am_threonine;

    private Float am_tryptophan;

    private Float am_valine;

    private Float am_cystine;

    private Float am_tyrosine;

    private Float am_l_carnitine;

    private Float am_glutamic_acid;

    private Float mi_calcium;

    private Float mi_phosphours;

    private Float mi_potassium;

    private Float mi_sodium;

    private Float mi_chloride;

    private Float mi_magnessium;

    private Float mi_iron;

    private Float mi_copper;

    private Float mi_manganese;

    private Float mi_zinc;

    private Float mi_iodine;

    private Float mi_selenium;

    private Float mi_ca_ph;

    private Float vi_vitamin_a;

    private Float vi_vitamin_d;

    private Float vi_vitamin_e;

    private Float vi_vitamin_b1;

    private Float vi_vitamin_b2;

    private Float vi_vitamin_b3;

    private Float vi_vitamin_b5;

    private Float vi_vitamin_b6;

    private Float vi_vitamin_b7;

    private Float vi_vitamin_b9;

    private Float vi_vitamin_b12;

    private Float vi_Choline;

    private Float vi_vitamin_c;

    private Float vi_vitamin_k3;

    private Float fa_omega3;

    private Float fa_epa;

    private Float fa_dha;

    private Float fa_omega6;

    private Float fa_arachidonic_acid;

    private Float fa_linoleicacid;

    private Float fa_a_linoleicacid;

    private Float fa_omega6_3;

    private Float ot_taurine;

    private Float ot_glucosamine;

    private Float ot_chondroitin;

    private Float ot_msm;

    private String ot_probiotics;

}
