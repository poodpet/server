package com.pood.server.object.meta.maintab;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_maintab_ct {

    private Integer maintab_idx;

    private Integer ct_idx;

    private String ct_name;

}
