package com.pood.server.object.meta.maintab;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_maintab_event {

    private Integer maintab_idx;

    private Integer event_idx;

    private Integer priority;

    private String title;

    private String image;

    private Boolean visible;

    private String start_date;

    private String end_date;

    private Integer page_landing_idx;

}
