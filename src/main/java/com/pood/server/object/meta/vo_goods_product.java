package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_goods_product {

    private Integer goods_idx;

    private Integer product_idx;

    private Integer product_qty;

    @Override
    public String toString() {
        return "vo_goods_product [goods_idx=" + goods_idx + ", product_idx=" + product_idx + ", product_qty="
            + product_qty + "]";
    }

}
