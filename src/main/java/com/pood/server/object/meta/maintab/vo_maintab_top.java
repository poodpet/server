package com.pood.server.object.meta.maintab;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_maintab_top {

    private String main_title;

    private Integer priority;

    private Integer visible;

    private Integer status;

}
