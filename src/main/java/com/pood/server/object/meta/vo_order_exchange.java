package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_order_exchange {

    private String order_number;

    private String uuid;

    private Integer goods_idx;

    private Integer qty;

    private Integer send_type;

    private Integer type;

    private Integer attr;

    private String text;

}
