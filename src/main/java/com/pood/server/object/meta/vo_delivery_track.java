package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_delivery_track {

    private String order_number;

    private String courier;

    private Integer type;

    private String tracking_id;

    private Integer send_type;

    private Integer goods_idx;

    private Integer history_idx;

    private Boolean main_address;

    private Integer qty;

    private String  startdate;

}
