package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_psk {

    private String search_word;

    private Integer show_index;

    private Integer pc_idx;

    @Override
    public String toString() {
        return "psk [pc_idx=" + pc_idx + ", search_word=" + search_word + ", show_index=" + show_index + "]";
    }

}
