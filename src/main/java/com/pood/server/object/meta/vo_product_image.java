package com.pood.server.object.meta;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vo_product_image {

    private Integer idx;

    private Integer product_idx;

    private String recordbirth;

    private Integer visible;

    private Integer type;

    private String updatetime;

    private String url;

    private Integer priority;

    @Override
    public String toString() {
        return "vo_product_image [idx=" + idx + ", priority=" + priority + ", product_idx=" + product_idx
            + ", recordbirth=" + recordbirth + ", type=" + type + ", updatetime=" + updatetime + ", url=" + url
            + ", visible=" + visible + "]";
    }

}
