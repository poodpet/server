package com.pood.server.object.IMP;

public class PAYMENT_RESULT {
    
    private String imp_uid;
    
    private String merchant_uid;
    
    private Boolean imp_success;
    
    private String error_msg;

    public String getImp_uid() {
        return imp_uid;
    }

    public void setImp_uid(String imp_uid) {
        this.imp_uid = imp_uid;
    }

    public String getMerchant_uid() {
        return merchant_uid;
    }

    public void setMerchant_uid(String merchant_uid) {
        this.merchant_uid = merchant_uid;
    }

    
    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }
 
    public Boolean getImp_success() {
        return imp_success;
    }

    public void setImp_success(Boolean imp_success) {
        this.imp_success = imp_success;
    }

    @Override
    public String toString() {
        return "PAYMENT_RESULT [error_msg=" + error_msg + ", imp_success=" + imp_success + ", imp_uid=" + imp_uid
                + ", merchant_uid=" + merchant_uid + "]";
    }

    
}
