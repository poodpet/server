package com.pood.server.object.IMP;

import java.sql.SQLException;
import java.text.ParseException;

public class IMP_BASKET {
 
    
    public Integer  BRAND_IDX;                          // 굿즈의 브랜드 항목 번호
    public Integer  GOODS_IDX;                          // 굿즈 항목 번호
    public String   GOODS_NAME;                         // 굿즈 이름
    public Integer  GOODS_PURCHASE_TYPE;                // 굿즈 구매 타입
    public Integer  GOODS_PRICE;                        // 굿즈 가격
    public Integer  GOODS_SALE_STATUS;                  // 굿즈 판매 여부

    public Integer  DIFF_PRICE;                         // 구매 차액 계산
    public Integer  DIFF_QUANTITY;                      // 구매 수량 계산

    public Integer  USER_COUPON_IDX;                    // 사용자 쿠폰 항목 번호
    public Integer  DISCOUNT_PRICE;                     // 할인 금액


    public Integer  COUPON_LIMIT_PRICE;                 // 쿠폰 적용 최소 금액
    public Integer  COUPON_IDX;                         // 쿠폰 항목 번호
    public Integer  COUPON_TYPE;                        // 쿠폰 타입
    public String   COUPON_NAME;                        // 쿠폰 이름
    public String   COUPON_AVAILABLE_TIME;              // 쿠폰 사용 시간
    public Boolean  COUPON_IS_EXPIRED;                  // 쿠폰 만료 체크 여부
    public Integer  COUPON_MAX_PRICE;                   // 쿠폰 할인 금액 
    public Integer  COUPON_DISCOUNT_RATE;               // 쿠폰 정률

    public Integer  PURCHASED_GOODS_PRICE;              // 장바구니당 구매시 굿즈 가격
    public Integer  PURCHASED_QTY;                      // 장바구니당 구매시 굿즈 수량
    public Integer  PURCHASED_AMOUNT;                   // 장바구니당 구매시 금액 = 굿즈 가격 * 굿즈 수량

    public Integer PROMOTION_IDX;                       // 프로모션 항목 번호

 


    public IMP_BASKET(IMP_SHOP e1) throws SQLException, ParseException {
 
        DISCOUNT_PRICE = 0;

        GOODS_IDX               = e1.getgoods_idx();

        PURCHASED_GOODS_PRICE   = e1.getgoods_price();
        PURCHASED_QTY           = e1.getQty();
        PURCHASED_AMOUNT        = PURCHASED_GOODS_PRICE * PURCHASED_QTY;

        USER_COUPON_IDX         = e1.getCoupon_idx();


        COUPON_LIMIT_PRICE = 0;
        COUPON_IDX = 0;
        COUPON_TYPE = 0;
        COUPON_NAME = "";
        COUPON_AVAILABLE_TIME = "";
        COUPON_IS_EXPIRED = false;
        COUPON_MAX_PRICE = 0;
        COUPON_DISCOUNT_RATE = 0;
        PROMOTION_IDX = e1.getPr_code();
    }


    /************ 장바구니당 할인 금액 계산 **********************/
    public void setDiscountPrice(){
        DISCOUNT_PRICE = calcDiscount(COUPON_TYPE, DISCOUNT_PRICE, PURCHASED_AMOUNT, COUPON_MAX_PRICE, COUPON_DISCOUNT_RATE);
    }

    public Boolean checkPurchasePriceLimit(){
        if(PURCHASED_AMOUNT < COUPON_LIMIT_PRICE)
            return false;

        return true;
    }



   /*************** 구매 타입이 1인 경우 이미 구매가 된 상품인지 확인 ***************/
    public Boolean checkgoodsAlreadyPurchased(){

        
        if(GOODS_PURCHASE_TYPE == 1){
 
            
        }

        return true;
    }


 


   /*************** 구매시 가격 변동이 있을 경우 페일 ***************/
    public Boolean checkgoodsPriceChanged(){
        DIFF_PRICE = GOODS_PRICE - PURCHASED_GOODS_PRICE;
        if(DIFF_PRICE != 0)return false;
        return true;
    }




   
   /**************** 판매중이지 않은 상태인 경우 구매가 안 되도록 ***************/
    public Boolean checkgoodsSaleStatus(){
        if(GOODS_SALE_STATUS != 1)return false;
        return true;
    }





 
 


    /********************* 일정금액 이상으로 구입이 되고 있는지 확인 *******************/
    public Boolean checkPurchaseLimit(){
        Integer DIFF_COUPON_PRICE = PURCHASED_AMOUNT - COUPON_LIMIT_PRICE;

        if(DIFF_COUPON_PRICE <= 0)return false;

        return true;
    }
    
 


     /*********************** 할인 금액 계산 ****************/
    public Integer calcDiscount(Integer coupon_type, Integer discount_price, Integer total_price, Integer max_price, Integer coupon_rate){

        // 중복 쿠폰 할인 금액 계산 : 정액, 정률 
        if(coupon_type == 0)
            discount_price += max_price;
        else if(coupon_type == 1){       
            Double price = (double)((coupon_rate * 0.01) * (total_price - discount_price));         

            if(price < max_price)discount_price += price.intValue();   
            else discount_price = max_price;                            // 최대 할인금액 넘지 않도록
        }

        return discount_price;
    }


    public void setBrandIDX(Integer brand_idx){
        this.BRAND_IDX = brand_idx;
    }
 


    /******************* 쿠폰 저장 ******************/
    public void setCouponIDX(Integer coupon_idx){
        this.COUPON_IDX = coupon_idx;
    }

    public void setCouponLimitPrice(Integer limit_price){
        this.COUPON_LIMIT_PRICE = limit_price;
    }

    public void setCouponName(String coupon_name){
        this.COUPON_NAME = coupon_name;
    }

    public void setCouponAvailableTime(String available_time){
        this.COUPON_AVAILABLE_TIME = available_time;
    }

    public void setCouponType(Integer coupon_type){
        this.COUPON_TYPE = coupon_type;

    }

    public void setCouponMaxPrice(Integer max_price){
        this.COUPON_MAX_PRICE = max_price;

    }

    public void setCouponDiscountRate(Integer discount_rate){
        this.COUPON_DISCOUNT_RATE = discount_rate;
    }





    /******************* 굿즈 정보 저장 ******************/
    public void setgoodsName(String goods_name){
        this.GOODS_NAME = goods_name;
    }

    public void setgoodsPurchaseType(Integer purchase_type){
        this.GOODS_PURCHASE_TYPE = purchase_type;
    }

 
    public void setgoodsPrice(Integer price){
        this.GOODS_PRICE = price;
    }

    public void setgoodsSaleStatus(Integer sale_status){
        this.GOODS_SALE_STATUS = sale_status;
    }


    @Override
    public String toString() {
        return "IMP_BASKET [COUPON_AVAILABLE_TIME=" + COUPON_AVAILABLE_TIME + ", COUPON_DISCOUNT_RATE="
                + COUPON_DISCOUNT_RATE + ", COUPON_IDX=" + COUPON_IDX + ", COUPON_IS_EXPIRED=" + COUPON_IS_EXPIRED
                + ", COUPON_LIMIT_PRICE=" + COUPON_LIMIT_PRICE + ", COUPON_MAX_PRICE=" + COUPON_MAX_PRICE
                + ", COUPON_NAME=" + COUPON_NAME + ", COUPON_TYPE=" + COUPON_TYPE + ", DIFF_PRICE=" + DIFF_PRICE
                + ", DIFF_QUANTITY=" + DIFF_QUANTITY + ", DISCOUNT_PRICE=" + DISCOUNT_PRICE + ", GOODS_IDX=" + GOODS_IDX
                + ", PURCHASED_AMOUNT=" + PURCHASED_AMOUNT + ", PURCHASED_QTY=" + PURCHASED_QTY
                + ", PURCHASED_GOODS_PRICE=" + PURCHASED_GOODS_PRICE + ", USER_COUPON_IDX=" + USER_COUPON_IDX
                + ", goods_NAME=" + GOODS_NAME + ", GOODS_PRICE=" + GOODS_PRICE + ", GOODS_PURCHASE_TYPE="
                + GOODS_PURCHASE_TYPE + ", GOODS_SALE_STATUS=" + GOODS_SALE_STATUS + "]";
    }

    

}
