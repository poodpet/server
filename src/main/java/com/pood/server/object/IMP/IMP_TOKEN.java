package com.pood.server.object.IMP;

public class IMP_TOKEN {

    private Integer code;

    private String message;

    private IMP_TOKEN_RESPONSE response;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public IMP_TOKEN_RESPONSE getResponse() {
        return response;
    }

    public void setResponse(IMP_TOKEN_RESPONSE response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "IMP_TOKEN [code=" + code + ", message=" + message + ", response=" + response + "]";
    }

    
    
}
