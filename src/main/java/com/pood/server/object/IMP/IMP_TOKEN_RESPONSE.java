package com.pood.server.object.IMP;

public class IMP_TOKEN_RESPONSE {
    
    private String access_token;

    private Integer now;

    private Integer expired_at;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public Integer getNow() {
        return now;
    }

    public void setNow(Integer now) {
        this.now = now;
    }

    public Integer getExpired_at() {
        return expired_at;
    }

    public void setExpired_at(Integer expired_at) {
        this.expired_at = expired_at;
    }

    @Override
    public String toString() {
        return "IMP_TOKEN_RESPONSE [access_token=" + access_token + ", expired_at=" + expired_at + ", now=" + now + "]";
    }

    
}
