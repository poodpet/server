package com.pood.server.object.IMP;

public class IMP_SIMPLE_PAY_SUCCESS {

    private Integer code;

    private String message;

    private IMP_SIMPLE_PAY_RESULT response;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public IMP_SIMPLE_PAY_RESULT getResponse() {
        return response;
    }

    public void setResponse(IMP_SIMPLE_PAY_RESULT response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "IMP_SIMPLE_PAY_SUCCESS [code=" + code + ", message=" + message + ", response=" + response + "]";
    }

    
    
}
