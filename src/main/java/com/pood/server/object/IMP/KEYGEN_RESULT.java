package com.pood.server.object.IMP;

public class KEYGEN_RESULT {

    private String  imp_uid;

    private String  CUSTOMER_UID;
    
    private String  MERCHANT_UID;

    private Boolean imp_success;

    private String  error_msg;

    public String getImp_uid() {
        return imp_uid;
    }

    public void setImp_uid(String imp_uid) {
        this.imp_uid = imp_uid;
    }

    public String getCUSTOMER_UID() {
        return CUSTOMER_UID;
    }

    public void setCUSTOMER_UID(String cUSTOMER_UID) {
        CUSTOMER_UID = cUSTOMER_UID;
    }

    public String getMERCHANT_UID() {
        return MERCHANT_UID;
    }

    public void setMERCHANT_UID(String mERCHANT_UID) {
        MERCHANT_UID = mERCHANT_UID;
    }

    public Boolean getImp_success() {
        return imp_success;
    }

    public void setImp_success(Boolean imp_success) {
        this.imp_success = imp_success;
    }

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }

    @Override
    public String toString() {
        return "KEYGEN_RESULT [CUSTOMER_UID=" + CUSTOMER_UID + ", MERCHANT_UID=" + MERCHANT_UID + ", error_msg="
                + error_msg + ", imp_success=" + imp_success + ", imp_uid=" + imp_uid + "]";
    }

    
}
