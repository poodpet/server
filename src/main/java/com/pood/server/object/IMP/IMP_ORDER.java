package com.pood.server.object.IMP;

import java.util.List;

public class IMP_ORDER {
    
    private String order_device;

    private List<IMP_SHOP> shoppingbag_data;

    private String memo;

    private Integer total_price;

    private Integer total_discount_price;

    private Integer used_point;

    private Boolean free_purchase;

    private Integer delivery_fee;

    private Integer coupon_idx;

    public String getOrder_device() {
        return order_device;
    }

    public void setOrder_device(String order_device) {
        this.order_device = order_device;
    }
 

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Integer getTotal_price() {
        return total_price;
    }

    public void setTotal_price(Integer total_price) {
        this.total_price = total_price;
    }

    public Integer getTotal_discount_price() {
        return total_discount_price;
    }

    public void setTotal_discount_price(Integer total_discount_price) {
        this.total_discount_price = total_discount_price;
    }

    public Integer getUsed_point() {
        return used_point;
    }

    public void setUsed_point(Integer used_point) {
        this.used_point = used_point;
    }

    public Boolean getFree_purchase() {
        return free_purchase;
    }

    public void setFree_purchase(Boolean free_purchase) {
        this.free_purchase = free_purchase;
    }

    public Integer getDelivery_fee() {
        return delivery_fee;
    }

    public void setDelivery_fee(Integer delivery_fee) {
        this.delivery_fee = delivery_fee;
    }

    public Integer getCoupon_idx() {
        return coupon_idx;
    }

    public void setCoupon_idx(Integer coupon_idx) {
        this.coupon_idx = coupon_idx;
    }

    public List<IMP_SHOP> getShoppingbag_data() {
        return shoppingbag_data;
    }

    public void setShoppingbag_data(List<IMP_SHOP> shoppingbag_data) {
        this.shoppingbag_data = shoppingbag_data;
    }

    @Override
    public String toString() {
        return "IMP_ORDER [coupon_idx=" + coupon_idx + ", delivery_fee=" + delivery_fee + ", free_purchase="
                + free_purchase + ", memo=" + memo + ", order_device=" + order_device + ", shoppingbag_data="
                + shoppingbag_data + ", total_discount_price=" + total_discount_price + ", total_price=" + total_price
                + ", used_point=" + used_point + "]";
    }

    
    
}
