package com.pood.server.object.IMP;

public class IMP_BANK_HOLDER {

    private String bank_holder;

    public String getBank_holder() {
        return bank_holder;
    }

    public void setBank_holder(String bank_holder) {
        this.bank_holder = bank_holder;
    }

    @Override
    public String toString() {
        return "IMP_BANK_HOLDER [bank_holder=" + bank_holder + "]";
    }

    
}
