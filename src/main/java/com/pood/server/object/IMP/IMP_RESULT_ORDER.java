package com.pood.server.object.IMP;

public class IMP_RESULT_ORDER {
    
    private Integer ORDER_IDX;

    private Integer ORDER_STATUS;

    private Boolean VALIDATION_ORDER_STATUS;

    public IMP_RESULT_ORDER(){}

    public Integer getORDER_IDX() {
        return ORDER_IDX;
    }

    public void setORDER_IDX(Integer oRDER_IDX) {
        ORDER_IDX = oRDER_IDX;
    }

    public Integer getORDER_STATUS() {
        return ORDER_STATUS;
    }

    public void setORDER_STATUS(Integer oRDER_STATUS) {
        ORDER_STATUS = oRDER_STATUS;
    }

 
    public Boolean getVALIDATION_ORDER_STATUS() {
        return VALIDATION_ORDER_STATUS;
    }

    public void setVALIDATION_ORDER_STATUS(Boolean vALIDATION_ORDER_STATUS) {
        VALIDATION_ORDER_STATUS = vALIDATION_ORDER_STATUS;
    }

    @Override
    public String toString() {
        return "IMP_RESULT_ORDER [ORDER_IDX=" + ORDER_IDX + ", ORDER_STATUS=" + ORDER_STATUS
                + ", VALIDATION_ORDER_STATUS=" + VALIDATION_ORDER_STATUS + "]";
    }
 
}
