package com.pood.server.object.IMP;

public class IMP_SIMPLE_PAY_FAIL{

    private Integer code;

    private String message;

    private String response;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "IMP_SIMPLE_PAY [code=" + code + ", message=" + message + ", response=" + response + "]";
    }

    
}
