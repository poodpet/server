package com.pood.server.object.IMP;

public class IMP_CANCEL_HISTORY {

    private String  pg_tid;

    private Integer amount;

    private Integer cancelled_at;

    private String  reason;

    private String receipt_url;

    public String getPg_tid() {
        return pg_tid;
    }

    public void setPg_tid(String pg_tid) {
        this.pg_tid = pg_tid;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getCancelled_at() {
        return cancelled_at;
    }

    public void setCancelled_at(Integer cancelled_at) {
        this.cancelled_at = cancelled_at;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReceipt_url() {
        return receipt_url;
    }

    public void setReceipt_url(String receipt_url) {
        this.receipt_url = receipt_url;
    }

    @Override
    public String toString() {
        return "IMP_CANCEL_HISTORY [amount=" + amount + ", cancelled_at=" + cancelled_at + ", pg_tid=" + pg_tid
                + ", reason=" + reason + ", receipt_url=" + receipt_url + "]";
    }

    
}
