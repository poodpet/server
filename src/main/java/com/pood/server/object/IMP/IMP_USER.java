package com.pood.server.object.IMP;

public class IMP_USER {

    private String  token;

    private String  user_uuid;

    private String  ml_name;

    private Float   ml_rate;

    private IMP_DELIVERY delivery; 

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public String getMl_name() {
        return ml_name;
    }

    public void setMl_name(String ml_name) {
        this.ml_name = ml_name;
    }

    public Float getMl_rate() {
        return ml_rate;
    }

    public void setMl_rate(Float ml_rate) {
        this.ml_rate = ml_rate;
    }

    public IMP_DELIVERY getDelivery() {
        return delivery;
    }

    public void setDelivery(IMP_DELIVERY delivery) {
        this.delivery = delivery;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "IMP_USER [delivery=" + delivery + ", ml_name=" + ml_name + ", ml_rate=" + ml_rate + ", token=" + token
                + ", user_uuid=" + user_uuid + "]";
    }

    
    
}
