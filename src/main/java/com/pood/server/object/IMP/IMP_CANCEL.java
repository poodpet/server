package com.pood.server.object.IMP;

public class IMP_CANCEL {
    
    public Integer ORDER_IDX;

    public Integer OVER_COUPON_IDX;

    public String  ORDER_NUMBER;

    public Integer FROM_ORDER_STATUS;
    
    public String  ORDER_NAME;

    public Integer USER_IDX;


    public IMP_CANCEL(){
        ORDER_IDX = 0;
        OVER_COUPON_IDX = 0;
        ORDER_NUMBER = "";
        ORDER_NAME = "";
        USER_IDX = 0;
    }
    
    public Integer getORDER_IDX() {
        return ORDER_IDX;
    }

    public void setORDER_IDX(Integer oRDER_IDX) {
        ORDER_IDX = oRDER_IDX;
    }

    public Integer getOVER_COUPON_IDX() {
        return OVER_COUPON_IDX;
    }

    public void setOVER_COUPON_IDX(Integer oVER_COUPON_IDX) {
        OVER_COUPON_IDX = oVER_COUPON_IDX;
    }

    public String getORDER_NUMBER() {
        return ORDER_NUMBER;
    }

    public void setORDER_NUMBER(String oRDER_NUMBER) {
        ORDER_NUMBER = oRDER_NUMBER;
    }

    public String getORDER_NAME() {
        return ORDER_NAME;
    }

    public void setORDER_NAME(String oRDER_NAME) {
        ORDER_NAME = oRDER_NAME;
    }

    public Integer getUSER_IDX() {
        return USER_IDX;
    }

    public void setUSER_IDX(Integer uSER_IDX) {
        USER_IDX = uSER_IDX;
    }

    public Integer getFROM_ORDER_STATUS() {
        return FROM_ORDER_STATUS;
    }

    public void setFROM_ORDER_STATUS(Integer fROM_ORDER_STATUS) {
        FROM_ORDER_STATUS = fROM_ORDER_STATUS;
    }

    @Override
    public String toString() {
        return "IMP_CANCEL [FROM_ORDER_STATUS=" + FROM_ORDER_STATUS + ", ORDER_IDX=" + ORDER_IDX + ", ORDER_NAME="
                + ORDER_NAME + ", ORDER_NUMBER=" + ORDER_NUMBER + ", OVER_COUPON_IDX=" + OVER_COUPON_IDX + ", USER_IDX="
                + USER_IDX + "]";
    }

    
    

}
