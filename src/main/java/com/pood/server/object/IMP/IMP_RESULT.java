package com.pood.server.object.IMP;

public class IMP_RESULT {
    
    private String  IMP_UID;

    private String  MERCHANT_UID;

    private Boolean IMP_SUCCESS;

    private String  ERROR_MESSAGE;

    private String  BUYER_EMAIL;

    private String  BUYER_NAME;

    private Integer ORDER_PRICE;

    private String  PG;

    private String  PAY_METHOD;

    private String  ORDER_NAME;

	public String getIMP_UID() {
		return IMP_UID;
	}

	public void setIMP_UID(String iMP_UID) {
		IMP_UID = iMP_UID;
	}

	public String getMERCHANT_UID() {
		return MERCHANT_UID;
	}

	public void setMERCHANT_UID(String mERCHANT_UID) {
		MERCHANT_UID = mERCHANT_UID;
	}

	public Boolean getIMP_SUCCESS() {
		return IMP_SUCCESS;
	}

	public void setIMP_SUCCESS(Boolean iMP_SUCCESS) {
		IMP_SUCCESS = iMP_SUCCESS;
	}

	public String getERROR_MESSAGE() {
		return ERROR_MESSAGE;
	}

	public void setERROR_MESSAGE(String eRROR_MESSAGE) {
		ERROR_MESSAGE = eRROR_MESSAGE;
	}

	public String getBUYER_EMAIL() {
		return BUYER_EMAIL;
	}

	public void setBUYER_EMAIL(String bUYER_EMAIL) {
		BUYER_EMAIL = bUYER_EMAIL;
	}

	public String getBUYER_NAME() {
		return BUYER_NAME;
	}

	public void setBUYER_NAME(String bUYER_NAME) {
		BUYER_NAME = bUYER_NAME;
	}

	public Integer getORDER_PRICE() {
		return ORDER_PRICE;
	}

	public void setORDER_PRICE(Integer oRDER_PRICE) {
		ORDER_PRICE = oRDER_PRICE;
	}

	public String getPG() {
		return PG;
	}

	public void setPG(String pG) {
		PG = pG;
	}

	public String getPAY_METHOD() {
		return PAY_METHOD;
	}

	public void setPAY_METHOD(String pAY_METHOD) {
		PAY_METHOD = pAY_METHOD;
	}

	public String getORDER_NAME() {
		return ORDER_NAME;
	}

	public void setORDER_NAME(String oRDER_NAME) {
		ORDER_NAME = oRDER_NAME;
	}

	@Override
	public String toString() {
		return "IMP_RESULT [BUYER_EMAIL=" + BUYER_EMAIL + ", BUYER_NAME=" + BUYER_NAME + ", ERROR_MESSAGE="
				+ ERROR_MESSAGE + ", IMP_SUCCESS=" + IMP_SUCCESS + ", IMP_UID=" + IMP_UID + ", MERCHANT_UID="
				+ MERCHANT_UID + ", ORDER_NAME=" + ORDER_NAME + ", ORDER_PRICE=" + ORDER_PRICE + ", PAY_METHOD="
				+ PAY_METHOD + ", PG=" + PG + "]";
	}

 
 
    
}
