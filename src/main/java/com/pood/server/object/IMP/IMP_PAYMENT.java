package com.pood.server.object.IMP;
 
public class IMP_PAYMENT {
    
    private String buyer_email;

    private String buyer_name;

    private Integer order_price;

    private String pg;

    private String pay_method;

    private String order_name;

    private String app_scheme;

    public String getBuyer_email() {
        return buyer_email;
    }

    public void setBuyer_email(String buyer_email) {
        this.buyer_email = buyer_email;
    }

    public String getBuyer_name() {
        return buyer_name;
    }

    public void setBuyer_name(String buyer_name) {
        this.buyer_name = buyer_name;
    }

    public Integer getOrder_price() {
        return order_price;
    }

    public void setOrder_price(Integer order_price) {
        this.order_price = order_price;
    }

    public String getPg() {
        return pg;
    }

    public void setPg(String pg) {
        this.pg = pg;
    }

    public String getPay_method() {
        return pay_method;
    }

    public void setPay_method(String pay_method) {
        this.pay_method = pay_method;
    }

    public String getOrder_name() {
        return order_name;
    }

    public void setOrder_name(String order_name) {
        this.order_name = order_name;
    }

    public String getApp_scheme() {
        return app_scheme;
    }

    public void setApp_scheme(String app_scheme) {
        this.app_scheme = app_scheme;
    }

    @Override
    public String toString() {
        return "hAM_iamport_payment [app_scheme=" + app_scheme + ", buyer_email=" + buyer_email + ", buyer_name="
                + buyer_name + ", order_name=" + order_name + ", order_price=" + order_price + ", pay_method="
                + pay_method + ", pg=" + pg + "]";
    }

    
    
}
