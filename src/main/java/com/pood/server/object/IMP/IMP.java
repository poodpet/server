package com.pood.server.object.IMP;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Base64;

public class IMP {

    public String MERCHANT_UID; // 주문 번호

    /************* 결제 정보 ***********/
    public String   PAYMENT_BUYER_EMAIL;

    public String   PAYMENT_BUYER_NAME;

    public Integer  PAYMENT_ORDER_PRICE;

    public String   PAYMENT_PG;

    public String   PAYMENT_PAY_METHOD;

    public String   PAYMENT_ORDER_NAME;

    /********** 배송지 정보 ***********/
    // Integer DELIVERY_DEFAULT_TYPE = delivery.getDefault_type();
    public String   DELIVERY_ADDRESS;

    public String   DELIVERY_DETAIL_ADDRESS;

    public String   DELIVERY_NAME;

    public String   DELIVERY_NICKNAME;

    public String   DELIVERY_ZIPCODE;

    public Integer  DELIVERY_INPUT_TYPE;

    public Integer  DELIVERY_REMOTE_TYPE;

    public String   DELIVERY_PHONE_NUMBER;




    /********** 주문 정보 ***********/
    public String   ORDER_DEVICE;

    public String   ORDER_MEMO;

    public Integer  ORDER_TOTAL_DISCOUNT_PRICE;

    public Integer  ORDER_USED_POINT;

    public Boolean  ORDER_FREE_PURCHASE;

    public Integer  ORDER_DELIVERY_FEE;

    public Integer  ORDER_TOTAL_PRICE;

    public Integer  ORDER_OVER_COUPON_IDX;

    public Integer  ORDER_IDX;




    /*********** 중복 쿠폰 정보 ***********/
    public Integer  OVER_COUPON_IDX;

    public Integer  OVER_COUPON_TYPE;

    public Integer  OVER_COUPON_DISCOUNT_RATE;

    public Integer  OVER_COUPON_DISCOUNT_PRICE;

    public Integer  OVER_COUPON_MAX_PRICE;

    public String   USER_UUID;

    public String   USER_TOKEN;

    public Integer  USER_IDX;

    public Integer  CALC_TOTAL_DISCOUNT_PRICE = 0;

    public Integer  CALC_AMOUNT = 0;




    /************ 구매자 회원 정보 ***********/
    public IMP_USER user;

    public IMP_PAYMENT payment;

    public IMP_ORDER order;

    public IMP_DELIVERY delivery;
 



    public void setIMP(String param) throws ParseException, JsonMappingException, JsonProcessingException {
        
        // 결제 정보 
        JSONParser parser = new JSONParser();
        Object obj = parser.parse( param ); 
        JSONObject jsonObj = (JSONObject) obj;
     
        ObjectMapper objectMapper = new ObjectMapper();
     
        user        =   objectMapper.readValue(jsonObj.get("user").toString(), IMP_USER.class); 
        payment     =   objectMapper.readValue(jsonObj.get("payment").toString(), IMP_PAYMENT.class);
        order       =   objectMapper.readValue(jsonObj.get("order").toString(), IMP_ORDER.class);
        delivery    =   user.getDelivery();

        USER_UUID   = user.getUser_uuid();

        USER_TOKEN  = user.getToken();

        Base64.Decoder decoder = Base64.getDecoder();
        byte[] decodedBytes = decoder.decode(payment.getOrder_name());
        String ORG_PAYMENT_ORDER_NAME = new String(decodedBytes);


        PAYMENT_BUYER_EMAIL =           payment.getBuyer_email();
        PAYMENT_BUYER_NAME =            payment.getBuyer_name();
        PAYMENT_ORDER_PRICE =           payment.getOrder_price();
        PAYMENT_PG =                    payment.getPg();
        PAYMENT_PAY_METHOD =            payment.getPay_method();
        PAYMENT_ORDER_NAME =            ORG_PAYMENT_ORDER_NAME;


    
        //Integer DELIVERY_DEFAULT_TYPE =     delivery.getDefault_type();
        DELIVERY_ADDRESS =              delivery.getAddress();
        DELIVERY_DETAIL_ADDRESS =       delivery.getDetail_address();
        DELIVERY_NAME =                 delivery.getName();
        DELIVERY_NICKNAME =             delivery.getNickname();
        DELIVERY_ZIPCODE =              delivery.getZipcode();
        DELIVERY_INPUT_TYPE =           delivery.getInput_type();
        DELIVERY_REMOTE_TYPE =          delivery.getRemote_type();
        DELIVERY_PHONE_NUMBER =         delivery.getPhone_number();

 
        ORDER_DEVICE =                  order.getOrder_device();
        ORDER_MEMO =                    order.getMemo();
        ORDER_TOTAL_DISCOUNT_PRICE =    order.getTotal_discount_price();
        ORDER_USED_POINT =              order.getUsed_point();
        ORDER_FREE_PURCHASE =           order.getFree_purchase();
        ORDER_DELIVERY_FEE =            order.getDelivery_fee();
        ORDER_TOTAL_PRICE =             order.getTotal_price();
        ORDER_OVER_COUPON_IDX =         order.getCoupon_idx();

        OVER_COUPON_IDX             =   0;              // 회원의 쿠폰 항목 번호
        OVER_COUPON_TYPE            =   0;
        OVER_COUPON_DISCOUNT_RATE   =   0;
        OVER_COUPON_DISCOUNT_PRICE  =   0;
        OVER_COUPON_MAX_PRICE       =   0;

        CALC_TOTAL_DISCOUNT_PRICE = 0;  
 
    }
 

    public void SET_OVER_COUPON(IMP_OVER_COUPON IMP_OVER_COUPON){
        this.OVER_COUPON_IDX = IMP_OVER_COUPON.getOVER_COUPON_IDX();
        this.OVER_COUPON_TYPE = IMP_OVER_COUPON.getOVER_COUPON_TYPE();
        this.OVER_COUPON_MAX_PRICE = IMP_OVER_COUPON.getOVER_COUPON_MAX_PRICE();
        this.OVER_COUPON_DISCOUNT_RATE = IMP_OVER_COUPON.getOVER_COUPON_DISCOUNT_RATE();
    }

    public void ADD_CALC_TOTAL_DISCOUNT_PRICE(Integer value){
        CALC_TOTAL_DISCOUNT_PRICE += value;
    }

    public void SET_OVER_COUPON_TYPE(Integer over_coupon_type){
        OVER_COUPON_TYPE = over_coupon_type;
    }

    public void SET_OVER_COUPON_DISCOUNT_RATE(Integer discount_rate){
        OVER_COUPON_DISCOUNT_RATE = discount_rate;
    }

    public void SET_OVER_COUPON_MAX_PRICE(Integer max_price){
        OVER_COUPON_MAX_PRICE = max_price;
    }

    public void SET_TOTAL_DISCOUNT_PRICE(Integer value){
        CALC_TOTAL_DISCOUNT_PRICE = value;
    }

    public void SET_ORDER_IDX(Integer order_idx){
        ORDER_IDX = order_idx;
    }

    public void setUser_idx(Integer user_idx){
        this.USER_IDX = user_idx;
    }

    public void SET_CALC_AMOUNT(Integer value){
        this.CALC_AMOUNT = value;
    }

    public void SET_OVER_COUPON_INDEX(Integer over_coupon_idx){
        this.OVER_COUPON_IDX = over_coupon_idx;
    }

    public void SET_MERCHANT_UID(String MERCHANT_UID){
        this.MERCHANT_UID = MERCHANT_UID;
    }

    public void SET_DELIVERY_REMOTE_TYPE(Integer delivery_remote_type){
        this.DELIVERY_REMOTE_TYPE = delivery_remote_type;
    }

    public void SET_OVER_COUPON_DISCOUNT_PRICE(Integer DISCOUNT_PRICE){
        this.OVER_COUPON_DISCOUNT_PRICE = DISCOUNT_PRICE;
    }


    @Override
    public String toString() {
        return "IMP [CALC_AMOUNT=" + CALC_AMOUNT + ", CALC_TOTAL_DISCOUNT_PRICE=" + CALC_TOTAL_DISCOUNT_PRICE
                + ", DELIVERY_ADDRESS=" + DELIVERY_ADDRESS + ", DELIVERY_DETAIL_ADDRESS=" + DELIVERY_DETAIL_ADDRESS
                + ", DELIVERY_INPUT_TYPE=" + DELIVERY_INPUT_TYPE + ", DELIVERY_NAME=" + DELIVERY_NAME
                + ", DELIVERY_NICKNAME=" + DELIVERY_NICKNAME + ", DELIVERY_PHONE_NUMBER=" + DELIVERY_PHONE_NUMBER
                + ", DELIVERY_REMOTE_TYPE=" + DELIVERY_REMOTE_TYPE + ", DELIVERY_ZIPCODE=" + DELIVERY_ZIPCODE
                + ", MERCHANT_UID=" + MERCHANT_UID + ", ORDER_DELIVERY_FEE=" + ORDER_DELIVERY_FEE + ", ORDER_DEVICE="
                + ORDER_DEVICE + ", ORDER_FREE_PURCHASE=" + ORDER_FREE_PURCHASE + ", ORDER_IDX=" + ORDER_IDX
                + ", ORDER_MEMO=" + ORDER_MEMO + ", ORDER_OVER_COUPON_IDX=" + ORDER_OVER_COUPON_IDX
                + ", ORDER_TOTAL_DISCOUNT_PRICE=" + ORDER_TOTAL_DISCOUNT_PRICE + ", ORDER_TOTAL_PRICE="
                + ORDER_TOTAL_PRICE + ", ORDER_USED_POINT=" + ORDER_USED_POINT + ", OVER_COUPON_DISCOUNT_PRICE="
                + OVER_COUPON_DISCOUNT_PRICE + ", OVER_COUPON_DISCOUNT_RATE=" + OVER_COUPON_DISCOUNT_RATE
                + ", OVER_COUPON_IDX=" + OVER_COUPON_IDX + ", OVER_COUPON_MAX_PRICE=" + OVER_COUPON_MAX_PRICE
                + ", OVER_COUPON_TYPE=" + OVER_COUPON_TYPE + ", PAYMENT_BUYER_EMAIL=" + PAYMENT_BUYER_EMAIL
                + ", PAYMENT_BUYER_NAME=" + PAYMENT_BUYER_NAME + ", PAYMENT_ORDER_NAME=" + PAYMENT_ORDER_NAME
                + ", PAYMENT_ORDER_PRICE=" + PAYMENT_ORDER_PRICE + ", PAYMENT_PAY_METHOD=" + PAYMENT_PAY_METHOD
                + ", PAYMENT_PG=" + PAYMENT_PG + ", USER_IDX=" + USER_IDX + ", USER_TOKEN=" + USER_TOKEN
                + ", USER_UUID=" + USER_UUID + ", delivery=" + delivery + ", order=" + order + ", payment=" + payment
                + ", user=" + user + "]";
    }
}
