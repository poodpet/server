package com.pood.server.object.IMP;

public class IMP_VBANK {
    
    private Integer         code;

    private String          message;

    private IMP_BANK_HOLDER response;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public IMP_BANK_HOLDER getResponse() {
        return response;
    }

    public void setResponse(IMP_BANK_HOLDER response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "IMP_VBANK [code=" + code + ", message=" + message + ", response=" + response.toString()+ "]";
    }

    


}
