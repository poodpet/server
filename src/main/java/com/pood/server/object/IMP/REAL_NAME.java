package com.pood.server.object.IMP;

public class REAL_NAME {

    private String api_tran_id;

    private String api_tran_dtm;

    private String rsp_code;

    private String rsp_message;

    private String bank_tran_id;

    private String bank_tran_date;

    private String bank_code_tran;

    private String bank_rsp_code;

    private String bank_rsp_message;

    private String bank_code_std;

    private String bank_code_sub;

    private String bank_name;

    private String savings_bank_name;

    private String account_num;

    private String account_seq;

    private char account_holder_info_type;

    private String account_holder_info;

    private String account_holder_name;

    private char account_type;

    public String getApi_tran_id() {
        return api_tran_id;
    }

    public void setApi_tran_id(String api_tran_id) {
        this.api_tran_id = api_tran_id;
    }

    public String getApi_tran_dtm() {
        return api_tran_dtm;
    }

    public void setApi_tran_dtm(String api_tran_dtm) {
        this.api_tran_dtm = api_tran_dtm;
    }

    public String getRsp_code() {
        return rsp_code;
    }

    public void setRsp_code(String rsp_code) {
        this.rsp_code = rsp_code;
    }

    public String getRsp_message() {
        return rsp_message;
    }

    public void setRsp_message(String rsp_message) {
        this.rsp_message = rsp_message;
    }

    public String getBank_tran_id() {
        return bank_tran_id;
    }

    public void setBank_tran_id(String bank_tran_id) {
        this.bank_tran_id = bank_tran_id;
    }

    public String getBank_tran_date() {
        return bank_tran_date;
    }

    public void setBank_tran_date(String bank_tran_date) {
        this.bank_tran_date = bank_tran_date;
    }

    public String getBank_code_tran() {
        return bank_code_tran;
    }

    public void setBank_code_tran(String bank_code_tran) {
        this.bank_code_tran = bank_code_tran;
    }

    public String getBank_rsp_code() {
        return bank_rsp_code;
    }

    public void setBank_rsp_code(String bank_rsp_code) {
        this.bank_rsp_code = bank_rsp_code;
    }

    public String getBank_rsp_message() {
        return bank_rsp_message;
    }

    public void setBank_rsp_message(String bank_rsp_message) {
        this.bank_rsp_message = bank_rsp_message;
    }

    public String getBank_code_std() {
        return bank_code_std;
    }

    public void setBank_code_std(String bank_code_std) {
        this.bank_code_std = bank_code_std;
    }

    public String getBank_code_sub() {
        return bank_code_sub;
    }

    public void setBank_code_sub(String bank_code_sub) {
        this.bank_code_sub = bank_code_sub;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getSavings_bank_name() {
        return savings_bank_name;
    }

    public void setSavings_bank_name(String savings_bank_name) {
        this.savings_bank_name = savings_bank_name;
    }

    public String getAccount_num() {
        return account_num;
    }

    public void setAccount_num(String account_num) {
        this.account_num = account_num;
    }

    public String getAccount_seq() {
        return account_seq;
    }

    public void setAccount_seq(String account_seq) {
        this.account_seq = account_seq;
    }

    public char getAccount_holder_info_type() {
        return account_holder_info_type;
    }

    public void setAccount_holder_info_type(char account_holder_info_type) {
        this.account_holder_info_type = account_holder_info_type;
    }

    public String getAccount_holder_info() {
        return account_holder_info;
    }

    public void setAccount_holder_info(String account_holder_info) {
        this.account_holder_info = account_holder_info;
    }

    public String getAccount_holder_name() {
        return account_holder_name;
    }

    public void setAccount_holder_name(String account_holder_name) {
        this.account_holder_name = account_holder_name;
    }

    public char getAccount_type() {
        return account_type;
    }

    public void setAccount_type(char account_type) {
        this.account_type = account_type;
    }

    @Override
    public String toString() {
        return "REAL_NAME [account_holder_info=" + account_holder_info + ", account_holder_info_type="
                + account_holder_info_type + ", account_holder_name=" + account_holder_name + ", account_num="
                + account_num + ", account_seq=" + account_seq + ", account_type=" + account_type + ", api_tran_dtm="
                + api_tran_dtm + ", api_tran_id=" + api_tran_id + ", bank_code_std=" + bank_code_std
                + ", bank_code_sub=" + bank_code_sub + ", bank_code_tran=" + bank_code_tran + ", bank_name=" + bank_name
                + ", bank_rsp_code=" + bank_rsp_code + ", bank_rsp_message=" + bank_rsp_message + ", bank_tran_date="
                + bank_tran_date + ", bank_tran_id=" + bank_tran_id + ", rsp_code=" + rsp_code + ", rsp_message="
                + rsp_message + ", savings_bank_name=" + savings_bank_name + "]";
    }

    
}
