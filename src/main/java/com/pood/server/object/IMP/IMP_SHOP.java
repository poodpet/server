package com.pood.server.object.IMP;

public class IMP_SHOP {

    private Integer idx;

    private Integer pr_code;

    private Integer coupon_idx;

    private Integer goods_idx;

    private Integer goods_price;

    private Integer qty;

    private Integer limit_quantity;

    private Integer seller_idx;
    
    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public Integer getPr_code() {
        return pr_code;
    }

    public void setPr_code(Integer pr_code) {
        this.pr_code = pr_code;
    }

    public Integer getCoupon_idx() {
        return coupon_idx;
    }

    public void setCoupon_idx(Integer coupon_idx) {
        this.coupon_idx = coupon_idx;
    }

    public Integer getgoods_idx() {
        return goods_idx;
    }

    public void setgoods_idx(Integer goods_idx) {
        this.goods_idx = goods_idx;
    }

    public Integer getgoods_price() {
        return goods_price;
    }

    public void setgoods_price(Integer goods_price) {
        this.goods_price = goods_price;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getLimit_quantity() {
        return limit_quantity;
    }

    public void setLimit_quantity(Integer limit_quantity) {
        this.limit_quantity = limit_quantity;
    }

    public Integer getSeller_idx() {
        return seller_idx;
    }

    public void setSeller_idx(Integer seller_idx) {
        this.seller_idx = seller_idx;
    }

    @Override
    public String toString() {
        return "hAM_iamport_shoppingbag_data [coupon_idx=" + coupon_idx + ", goods_idx=" + goods_idx + ", goods_price="
                + goods_price + ", idx=" + idx + ", limit_quantity=" + limit_quantity + ", pr_code=" + pr_code + ", qty="
                + qty + ", seller_idx=" + seller_idx + "]";
    }

    public Boolean isPromotionGoods(){
        return !getPr_code().equals(-1);
    }
    
    
}
