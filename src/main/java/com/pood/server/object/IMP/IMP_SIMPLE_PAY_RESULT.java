package com.pood.server.object.IMP;

import java.util.List;

public class IMP_SIMPLE_PAY_RESULT {

    private Integer amount;

    private String  apply_num;

    private String  bank_code;

    private String  bank_name;

    private String  buyer_addr;

    private String  buyer_email;

    private String  buyer_name;

    private String  buyer_postcode;

    private String  buyer_tel;

    private Integer cancel_amount;

    private String  cancel_reason;

    private Integer cancelled_at;

    private String  card_code;

    private String  card_name;

    private String  card_number;
    
    private Integer card_quota;

    private Integer card_type;

    private Boolean cash_receipt_issued;

    private String  channel;

    private String  currency;
    
    private String  custom_data;
    
    private String  customer_uid;
    
    private String  customer_uid_usage;

    private Boolean escrow;

    private String  fail_reason;

    private Integer failed_at;

    private String  imp_uid;
    
    private String  merchant_uid;

    private String  name;

    private Integer paid_at;
    
    private String  pay_method;
    
    private String  pg_id;

    private String   pg_provider;

    private String  pg_tid;
    
    private String  receipt_url;
    
    private Integer started_at;

    private String  status;

    private String  user_agent;
    
    private String  vbank_code;
    
    private Integer vbank_date;

    private String  vbank_holder;
    
    private Integer vbank_issued_at;

    private String  vbank_name;

    private String  vbank_num;

    private List<String> cancel_receipt_urls;

    private List<IMP_CANCEL_HISTORY> cancel_history;

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getApply_num() {
        return apply_num;
    }

    public void setApply_num(String apply_num) {
        this.apply_num = apply_num;
    }

    public String getBank_code() {
        return bank_code;
    }

    public void setBank_code(String bank_code) {
        this.bank_code = bank_code;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getBuyer_addr() {
        return buyer_addr;
    }

    public void setBuyer_addr(String buyer_addr) {
        this.buyer_addr = buyer_addr;
    }

    public String getBuyer_email() {
        return buyer_email;
    }

    public void setBuyer_email(String buyer_email) {
        this.buyer_email = buyer_email;
    }

    public String getBuyer_name() {
        return buyer_name;
    }

    public void setBuyer_name(String buyer_name) {
        this.buyer_name = buyer_name;
    }

    public String getBuyer_postcode() {
        return buyer_postcode;
    }

    public void setBuyer_postcode(String buyer_postcode) {
        this.buyer_postcode = buyer_postcode;
    }

    public String getBuyer_tel() {
        return buyer_tel;
    }

    public void setBuyer_tel(String buyer_tel) {
        this.buyer_tel = buyer_tel;
    }

    public Integer getCancel_amount() {
        return cancel_amount;
    }

    public void setCancel_amount(Integer cancel_amount) {
        this.cancel_amount = cancel_amount;
    }

    public String getCancel_reason() {
        return cancel_reason;
    }

    public void setCancel_reason(String cancel_reason) {
        this.cancel_reason = cancel_reason;
    }

    public Integer getCancelled_at() {
        return cancelled_at;
    }

    public void setCancelled_at(Integer cancelled_at) {
        this.cancelled_at = cancelled_at;
    }

    public String getCard_code() {
        return card_code;
    }

    public void setCard_code(String card_code) {
        this.card_code = card_code;
    }

    public String getCard_name() {
        return card_name;
    }

    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public Integer getCard_quota() {
        return card_quota;
    }

    public void setCard_quota(Integer card_quota) {
        this.card_quota = card_quota;
    }

    public Integer getCard_type() {
        return card_type;
    }

    public void setCard_type(Integer card_type) {
        this.card_type = card_type;
    }

    public Boolean getCash_receipt_issued() {
        return cash_receipt_issued;
    }

    public void setCash_receipt_issued(Boolean cash_receipt_issued) {
        this.cash_receipt_issued = cash_receipt_issued;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCustom_data() {
        return custom_data;
    }

    public void setCustom_data(String custom_data) {
        this.custom_data = custom_data;
    }

    public String getCustomer_uid() {
        return customer_uid;
    }

    public void setCustomer_uid(String customer_uid) {
        this.customer_uid = customer_uid;
    }

    public String getCustomer_uid_usage() {
        return customer_uid_usage;
    }

    public void setCustomer_uid_usage(String customer_uid_usage) {
        this.customer_uid_usage = customer_uid_usage;
    }

    public Boolean getEscrow() {
        return escrow;
    }

    public void setEscrow(Boolean escrow) {
        this.escrow = escrow;
    }

    public String getFail_reason() {
        return fail_reason;
    }

    public void setFail_reason(String fail_reason) {
        this.fail_reason = fail_reason;
    }

    public Integer getFailed_at() {
        return failed_at;
    }

    public void setFailed_at(Integer failed_at) {
        this.failed_at = failed_at;
    }

    public String getImp_uid() {
        return imp_uid;
    }

    public void setImp_uid(String imp_uid) {
        this.imp_uid = imp_uid;
    }

    public String getMerchant_uid() {
        return merchant_uid;
    }

    public void setMerchant_uid(String merchant_uid) {
        this.merchant_uid = merchant_uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPaid_at() {
        return paid_at;
    }

    public void setPaid_at(Integer paid_at) {
        this.paid_at = paid_at;
    }

    public String getPay_method() {
        return pay_method;
    }

    public void setPay_method(String pay_method) {
        this.pay_method = pay_method;
    }

    public String getPg_id() {
        return pg_id;
    }

    public void setPg_id(String pg_id) {
        this.pg_id = pg_id;
    }

    public String getPg_provider() {
        return pg_provider;
    }

    public void setPg_provider(String pg_provider) {
        this.pg_provider = pg_provider;
    }

    public String getPg_tid() {
        return pg_tid;
    }

    public void setPg_tid(String pg_tid) {
        this.pg_tid = pg_tid;
    }

    public String getReceipt_url() {
        return receipt_url;
    }

    public void setReceipt_url(String receipt_url) {
        this.receipt_url = receipt_url;
    }

    public Integer getStarted_at() {
        return started_at;
    }

    public void setStarted_at(Integer started_at) {
        this.started_at = started_at;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser_agent() {
        return user_agent;
    }

    public void setUser_agent(String user_agent) {
        this.user_agent = user_agent;
    }

    public String getVbank_code() {
        return vbank_code;
    }

    public void setVbank_code(String vbank_code) {
        this.vbank_code = vbank_code;
    }

    public Integer getVbank_date() {
        return vbank_date;
    }

    public void setVbank_date(Integer vbank_date) {
        this.vbank_date = vbank_date;
    }

    public String getVbank_holder() {
        return vbank_holder;
    }

    public void setVbank_holder(String vbank_holder) {
        this.vbank_holder = vbank_holder;
    }

    public Integer getVbank_issued_at() {
        return vbank_issued_at;
    }

    public void setVbank_issued_at(Integer vbank_issued_at) {
        this.vbank_issued_at = vbank_issued_at;
    }

    public String getVbank_name() {
        return vbank_name;
    }

    public void setVbank_name(String vbank_name) {
        this.vbank_name = vbank_name;
    }

    public String getVbank_num() {
        return vbank_num;
    }

    public void setVbank_num(String vbank_num) {
        this.vbank_num = vbank_num;
    }

    public List<String> getCancel_receipt_urls() {
        return cancel_receipt_urls;
    }

    public void setCancel_receipt_urls(List<String> cancel_receipt_urls) {
        this.cancel_receipt_urls = cancel_receipt_urls;
    }

    public List<IMP_CANCEL_HISTORY> getCancel_history() {
        return cancel_history;
    }

    public void setCancel_history(List<IMP_CANCEL_HISTORY> cancel_history) {
        this.cancel_history = cancel_history;
    }

    @Override
    public String toString() {
        return "IMP_SIMPLE_PAY_RESULT [amount=" + amount + ", apply_num=" + apply_num + ", bank_code=" + bank_code
                + ", bank_name=" + bank_name + ", buyer_addr=" + buyer_addr + ", buyer_email=" + buyer_email
                + ", buyer_name=" + buyer_name + ", buyer_postcode=" + buyer_postcode + ", buyer_tel=" + buyer_tel
                + ", cancel_amount=" + cancel_amount + ", cancel_history=" + cancel_history + ", cancel_reason="
                + cancel_reason + ", cancel_receipt_urls=" + cancel_receipt_urls + ", cancelled_at=" + cancelled_at
                + ", card_code=" + card_code + ", card_name=" + card_name + ", card_number=" + card_number
                + ", card_quota=" + card_quota + ", card_type=" + card_type + ", cash_receipt_issued="
                + cash_receipt_issued + ", channel=" + channel + ", currency=" + currency + ", custom_data="
                + custom_data + ", customer_uid=" + customer_uid + ", customer_uid_usage=" + customer_uid_usage
                + ", escrow=" + escrow + ", fail_reason=" + fail_reason + ", failed_at=" + failed_at + ", imp_uid="
                + imp_uid + ", merchant_uid=" + merchant_uid + ", name=" + name + ", paid_at=" + paid_at
                + ", pay_method=" + pay_method + ", pg_id=" + pg_id + ", pg_provider=" + pg_provider + ", pg_tid="
                + pg_tid + ", receipt_url=" + receipt_url + ", started_at=" + started_at + ", status=" + status
                + ", user_agent=" + user_agent + ", vbank_code=" + vbank_code + ", vbank_date=" + vbank_date
                + ", vbank_holder=" + vbank_holder + ", vbank_issued_at=" + vbank_issued_at + ", vbank_name="
                + vbank_name + ", vbank_num=" + vbank_num + "]";
    }

    

    
}
