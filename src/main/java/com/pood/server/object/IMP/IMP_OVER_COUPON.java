package com.pood.server.object.IMP;

public class IMP_OVER_COUPON {
 
    private String  OVER_COUPON_NAME;
    
    private Integer OVER_COUPON_IDX;

    private Integer OVER_COUPON_TYPE;

    private Integer OVER_COUPON_PUBLISH_TYPE;

    private Integer OVER_COUPON_DISCOUNT_RATE;

    private Integer OVER_COUPON_MAX_PRICE;

    private Integer OVER_COUPON_LIMIT_PRICE;

    public Integer getOVER_COUPON_IDX() {
        return OVER_COUPON_IDX;
    }

    public void setOVER_COUPON_IDX(Integer oVER_COUPON_IDX) {
        OVER_COUPON_IDX = oVER_COUPON_IDX;
    }

    public Integer getOVER_COUPON_TYPE() {
        return OVER_COUPON_TYPE;
    }

    public void setOVER_COUPON_TYPE(Integer oVER_COUPON_TYPE) {
        OVER_COUPON_TYPE = oVER_COUPON_TYPE;
    }

    public Integer getOVER_COUPON_DISCOUNT_RATE() {
        return OVER_COUPON_DISCOUNT_RATE;
    }

    public void setOVER_COUPON_DISCOUNT_RATE(Integer oVER_COUPON_DISCOUNT_RATE) {
        OVER_COUPON_DISCOUNT_RATE = oVER_COUPON_DISCOUNT_RATE;
    }

    public Integer getOVER_COUPON_MAX_PRICE() {
        return OVER_COUPON_MAX_PRICE;
    }

    public void setOVER_COUPON_MAX_PRICE(Integer oVER_COUPON_MAX_PRICE) {
        OVER_COUPON_MAX_PRICE = oVER_COUPON_MAX_PRICE;
    }

    public Integer getOVER_COUPON_LIMIT_PRICE() {
        return OVER_COUPON_LIMIT_PRICE;
    }

    public void setOVER_COUPON_LIMIT_PRICE(Integer oVER_COUPON_LIMIT_PRICE) {
        OVER_COUPON_LIMIT_PRICE = oVER_COUPON_LIMIT_PRICE;
    } 

    public String getOVER_COUPON_NAME() {
        return OVER_COUPON_NAME;
    }

    public void setOVER_COUPON_NAME(String oVER_COUPON_NAME) {
        OVER_COUPON_NAME = oVER_COUPON_NAME;
    }

    public Integer getOVER_COUPON_PUBLISH_TYPE() {
        return OVER_COUPON_PUBLISH_TYPE;
    }

    public void setOVER_COUPON_PUBLISH_TYPE(Integer oVER_COUPON_PUBLISH_TYPE) {
        OVER_COUPON_PUBLISH_TYPE = oVER_COUPON_PUBLISH_TYPE;
    }

    @Override
    public String toString() {
        return "IMP_OVER_COUPON [OVER_COUPON_DISCOUNT_RATE=" + OVER_COUPON_DISCOUNT_RATE + ", OVER_COUPON_IDX="
                + OVER_COUPON_IDX + ", OVER_COUPON_LIMIT_PRICE=" + OVER_COUPON_LIMIT_PRICE + ", OVER_COUPON_MAX_PRICE="
                + OVER_COUPON_MAX_PRICE + ", OVER_COUPON_NAME=" + OVER_COUPON_NAME + ", OVER_COUPON_PUBLISH_TYPE="
                + OVER_COUPON_PUBLISH_TYPE + ", OVER_COUPON_TYPE=" + OVER_COUPON_TYPE + "]";
    }
 
    
}
