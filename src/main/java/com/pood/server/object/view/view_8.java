package com.pood.server.object.view;

public class view_8 {

    private String  user_uuid;

    private String  order_number;

    private String  recordbirth;

    private Integer order_status;

    private String  order_text;

    private Integer goods_idx;

    private Integer qty;

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public Integer getOrder_status() {
        return order_status;
    }

    public void setOrder_status(Integer order_status) {
        this.order_status = order_status;
    }

    public String getOrder_text() {
        return order_text;
    }

    public void setOrder_text(String order_text) {
        this.order_text = order_text;
    }

    public Integer getGoods_idx() {
        return goods_idx;
    }

    public void setGoods_idx(Integer goods_idx) {
        this.goods_idx = goods_idx;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    @Override
    public String toString() {
        return "view_8 [goods_idx=" + goods_idx + ", order_number=" + order_number + ", order_status=" + order_status
                + ", order_text=" + order_text + ", qty=" + qty + ", recordbirth=" + recordbirth + ", user_uuid="
                + user_uuid + "]";
    }

    
}
