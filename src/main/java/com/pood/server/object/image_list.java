package com.pood.server.object;
 
import java.util.List;

import com.pood.server.dto.dto_image_2;

public class image_list {

    private List<dto_image_2> MAIN_IMAGE_LIST;

    private List<dto_image_2> IMAGE_LIST;

    public List<dto_image_2> getMAIN_IMAGE_LIST() {
        return MAIN_IMAGE_LIST;
    }

    public void setMAIN_IMAGE_LIST(List<dto_image_2> mAIN_IMAGE_LIST) {
        MAIN_IMAGE_LIST = mAIN_IMAGE_LIST;
    }

    public List<dto_image_2> getIMAGE_LIST() {
        return IMAGE_LIST;
    }

    public void setIMAGE_LIST(List<dto_image_2> iMAGE_LIST) {
        IMAGE_LIST = iMAGE_LIST;
    }

    @Override
    public String toString() {
        return "image_list [IMAGE_LIST=" + IMAGE_LIST + ", MAIN_IMAGE_LIST=" + MAIN_IMAGE_LIST + "]";
    }
    
    
    
}