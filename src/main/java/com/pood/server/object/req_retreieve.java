package com.pood.server.object;

import com.pood.server.api.req.header.pay.OrderCancelRequest;
import com.pood.server.api.req.header.pay.hPay_cancel_1_2;
import com.pood.server.web.mapper.payment.Money;

public class req_retreieve {
    
    /*
        String  MERCHANT_UID,                   // 주문 번호
        String  REQUEST_TEXT,                   // 환불 요청 사항
        Integer CANCEL_GOODS_IDX,               // 주문시 굿즈 항목 번호
        Integer CANCEL_GOODS_PRICE,             // 주문시 굿즈 가격
        Integer CANCEL_GOODS_QTY,               // 주문시 굿즈 개수
        Boolean IS_PARTIAL,                     // 부분 환불인지 아닌지
        Boolean IS_REFUND,                      // 0 : 일반 취소, 1: 반품
        String  REFUND_UUID                     // 환불 고유 번호
        Integer ATTR_TYPE                       // 0 : 구매자 귀책, 1 : 판매자 귀책
    */

    private String merchant_uid;

    private String request_text;

    private Integer cancel_goods_idx;

    private Integer cancel_goods_price;

    private Integer cancel_goods_qty;

    private Boolean is_partial;

    private Boolean is_refund;

    private String refund_uuid;

    private Integer attr_type;

    private Boolean callback;

    public String getMerchant_uid() {
        return merchant_uid;
    }

    public void setMerchant_uid(String merchant_uid) {
        this.merchant_uid = merchant_uid;
    }

    public String getRequest_text() {
        return request_text;
    }

    public void setRequest_text(String request_text) {
        this.request_text = request_text;
    }

    public Integer getCancel_goods_idx() {
        return cancel_goods_idx;
    }

    public void setCancel_goods_idx(Integer cancel_goods_idx) {
        this.cancel_goods_idx = cancel_goods_idx;
    }

    public Integer getCancel_goods_qty() {
        return cancel_goods_qty;
    }

    public void setCancel_goods_qty(Integer cancel_goods_qty) {
        this.cancel_goods_qty = cancel_goods_qty;
    }

    public Boolean getIs_partial() {
        return is_partial;
    }

    public void setIs_partial(Boolean is_partial) {
        this.is_partial = is_partial;
    }

    public Boolean getIs_refund() {
        return is_refund;
    }

    public void setIs_refund(Boolean is_refund) {
        this.is_refund = is_refund;
    }

    public String getRefund_uuid() {
        return refund_uuid;
    }

    public void setRefund_uuid(String refund_uuid) {
        this.refund_uuid = refund_uuid;
    }

    public Integer getCancel_goods_price() {
        return cancel_goods_price;
    }

    public void setCancel_goods_price(Integer cancel_goods_price) {
        this.cancel_goods_price = cancel_goods_price;
    }

    public Integer getAttr_type() {
        return attr_type;
    }

    public void setAttr_type(Integer attr_type) {
        this.attr_type = attr_type;
    }

    public Boolean getCallback() {
        return callback;
    }

    public void setCallback(Boolean callback) {
        this.callback = callback;
    }

    @Override
    public String toString() {
        return "req_retreieve [attr_type=" + attr_type + ", callback=" + callback
            + ", cancel_goods_idx="
            + cancel_goods_idx + ", cancel_goods_price=" + cancel_goods_price
            + ", cancel_goods_qty="
            + cancel_goods_qty + ", is_partial=" + is_partial + ", is_refund=" + is_refund
            + ", merchant_uid="
            + merchant_uid + ", refund_uuid=" + refund_uuid + ", request_text=" + request_text
            + "]";
    }

    public static req_retreieve createRetreieve(final OrderCancelRequest orderCancelRequest,
        final hPay_cancel_1_2 cancelDate, final int cancelGoodsPrice) {
        req_retreieve record = new req_retreieve();
        record.setIs_refund(orderCancelRequest.isRefund());
        record.setMerchant_uid(orderCancelRequest.getMerchant_uid());
        record.setRequest_text(orderCancelRequest.getRequest_text());
        record.setCancel_goods_price(cancelGoodsPrice);
        record.setCancel_goods_idx(cancelDate.getGoods_idx());
        record.setCancel_goods_qty(cancelDate.getQty());
        record.setRefund_uuid(cancelDate.getRefund_uuid());
        record.setAttr_type(cancelDate.getAttr_type());
        record.setIs_partial(true);
        record.setCallback(false);
        return record;
    }

    public long totalCancelPrice(){
        Money money = new Money(cancel_goods_price * cancel_goods_qty);
        return money.getLongValue();
    }

}