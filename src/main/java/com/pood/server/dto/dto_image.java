package com.pood.server.dto;

public class dto_image {

    private Integer image_idx;

    private String  recordbirth;

    private Integer visible;

    private Integer type;

    private String  updatetime;

    private String  url;

    public Integer getImage_idx() {
        return image_idx;
    }

    public void setImage_idx(Integer image_idx) {
        this.image_idx = image_idx;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "dto_image [image_idx=" + image_idx + ", recordbirth=" + recordbirth + ", type=" + type + ", updatetime="
                + updatetime + ", url=" + url + ", visible=" + visible + "]";
    }

    
    
}
