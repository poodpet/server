package com.pood.server.dto.user.pet;

import com.pood.server.entity.user.PetDiagnosisJoinView;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class UserPetAiDiagnosisDtoGroup {

    private final List<PetDiagnosisJoinView> petDiagnosisJoinViewList;

    public UserPetAiDiagnosisDtoGroup(final List<PetDiagnosisJoinView> petDiagnosisJoinViewList) {
        this.petDiagnosisJoinViewList = petDiagnosisJoinViewList;
    }

    public Map<Integer, List<UserPetAiDiagnosisDto>> createAndGroupByUserPetIdxMap() {
        return petDiagnosisJoinViewList.stream()
            .map(petDiagnosisJoinView -> UserPetAiDiagnosisDto.of(
                petDiagnosisJoinView.getIdx(),
                petDiagnosisJoinView.getUserPetIdx(),
                petDiagnosisJoinView.getPetWorryIdx(),
                petDiagnosisJoinView.getName(),
                petDiagnosisJoinView.getDogUrl(),
                petDiagnosisJoinView.getCatUrl()
            )).collect(Collectors.groupingBy(UserPetAiDiagnosisDto::getUserPetIdx));
    }

}
