package com.pood.server.dto.user.basket;

import lombok.Value;

@Value(staticConstructor = "of")
public class BasketQuantity {

    int quantity;

    public BasketQuantity update(final int quantity) {
        return new BasketQuantity(quantity);
    }

}
