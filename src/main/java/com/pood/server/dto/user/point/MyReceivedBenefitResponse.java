package com.pood.server.dto.user.point;

import lombok.Value;

@Value(staticConstructor = "of")
public class MyReceivedBenefitResponse {

    int writeByMyRecommendCodeListSize;
    int givenPoint;
    int firstBoughtUser;
    String imageUrl;

}
