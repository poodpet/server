package com.pood.server.dto.user.review;

import java.util.List;

import com.pood.server.dto.dto_image_2;
import com.pood.server.dto.dto_image_3;

public class dto_user_review {

    private Integer idx;

    private Integer user_idx;

    private Integer pet_idx;

    private Integer order_idx;

    private Integer goods_idx;

    private String  goods_name;

    private Integer goods_pc_idx;

    private Integer product_idx;

    private Integer rating;

    private String  review_text;

    private Integer update_count;

    private Integer isDelete;

    private Integer isVisible;

    private String  updatetime;

    private String  recordbirth;

    private Integer clap_cnt;

    private String display_type;

    private List<dto_image_2> goods_image;

    private List<dto_image_2> product_image;

    private List<dto_image_3> image;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public Integer getUser_idx() {
        return user_idx;
    }

    public void setUser_idx(Integer user_idx) {
        this.user_idx = user_idx;
    }

    public Integer getPet_idx() {
        return pet_idx;
    }

    public void setPet_idx(Integer pet_idx) {
        this.pet_idx = pet_idx;
    }

    public Integer getOrder_idx() {
        return order_idx;
    }

    public void setOrder_idx(Integer order_idx) {
        this.order_idx = order_idx;
    }

    public Integer getGoods_idx() {
        return goods_idx;
    }

    public void setGoods_idx(Integer goods_idx) {
        this.goods_idx = goods_idx;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public Integer getProduct_idx() {
        return product_idx;
    }

    public void setProduct_idx(Integer product_idx) {
        this.product_idx = product_idx;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getReview_text() {
        return review_text;
    }

    public void setReview_text(String review_text) {
        this.review_text = review_text;
    }

    public Integer getUpdate_count() {
        return update_count;
    }

    public void setUpdate_count(Integer update_count) {
        this.update_count = update_count;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getIsVisible() {
        return isVisible;
    }

    public void setIsVisible(Integer isVisible) {
        this.isVisible = isVisible;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    

    public Integer getClap_cnt() {
        return clap_cnt;
    }

    public void setClap_cnt(Integer clap_cnt) {
        this.clap_cnt = clap_cnt;
    }

    public List<dto_image_3> getImage() {
        return image;
    }

    public void setImage(List<dto_image_3> image) {
        this.image = image;
    }
    
    public List<dto_image_2> getGoods_image() {
        return goods_image;
    }

    public void setGoods_image(List<dto_image_2> goods_image) {
        this.goods_image = goods_image;
    }

    public List<dto_image_2> getProduct_image() {
        return product_image;
    }

    public void setProduct_image(List<dto_image_2> product_image) {
        this.product_image = product_image;
    }

    public String getDisplay_type() {
        return display_type;
    }

    public void setDisplay_type(String display_type) {
        this.display_type = display_type;
    }


    public Integer getGoods_pc_idx() {
        return goods_pc_idx;
    }

    public void setGoods_pc_idx(Integer goods_pc_idx) {
        this.goods_pc_idx = goods_pc_idx;
    }

    @Override
    public String toString() {
        return "dto_user_review [clap_cnt=" + clap_cnt + ", display_type=" + display_type + ", goods_idx=" + goods_idx
                + ", goods_image=" + goods_image + ", goods_name=" + goods_name + ", idx=" + idx + ", image=" + image
                + ", isDelete=" + isDelete + ", isVisible=" + isVisible + ", order_idx=" + order_idx + ", pet_idx="
                + pet_idx + ", product_idx=" + product_idx + ", product_image=" + product_image + ", rating=" + rating
                + ", recordbirth=" + recordbirth + ", review_text=" + review_text + ", update_count=" + update_count
                + ", updatetime=" + updatetime + ", user_idx=" + user_idx + "]";
    } 

    
    
 
}
