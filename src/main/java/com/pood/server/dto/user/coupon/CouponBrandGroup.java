package com.pood.server.dto.user.coupon;

import com.pood.server.entity.meta.CouponBrand;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;

@Getter
public class CouponBrandGroup {

    private final List<CouponBrand> couponBrandList;

    public CouponBrandGroup(final List<CouponBrand> couponBrandList) {
        this.couponBrandList = Collections.unmodifiableList(couponBrandList);
    }

    public List<Integer> couponBrandIdxList(final int couponIdx) {
        return couponBrandList.stream()
            .filter(couponBrand -> couponBrand.getCouponIdx() == couponIdx)
            .map(CouponBrand::getBrandIdx)
            .collect(Collectors.toList());
    }

}
