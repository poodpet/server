package com.pood.server.dto.user.pet;

import com.pood.server.controller.request.userpet.PetWorryRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

public class PetWorryCreateGroup {

    private static final int MAX_WORRY_SIZE = 3;

    private final List<PetWorryRequest> petWorryRequestList;

    public PetWorryCreateGroup(final List<PetWorryRequest> petWorryRequestList) {
        this.petWorryRequestList = Collections.unmodifiableList(petWorryRequestList);
    }

    public List<Long> petWorryIdxList() {
        return petWorryRequestList.stream().map(PetWorryRequest::getIdx)
            .collect(Collectors.toList());
    }

    public List<PetWorryRequest> getPetWorryCreateRequestList() {
        return petWorryRequestList;
    }

    public PetWorryCreateGroup replaceWorry(final List<Long> replaceWorryIdx,
        final List<Long> deleteWorryList) {

        Queue<Long> addWorryIdxQueue = new LinkedList<>(
            getReplaceIdx(replaceWorryIdx, deleteWorryList.size())
        );

        List<PetWorryRequest> replace = new ArrayList<>();

        for (PetWorryRequest request : this.petWorryRequestList) {
            setReplaceWorryRequest(replace, deleteWorryList, addWorryIdxQueue, request);
        }

        return new PetWorryCreateGroup(replace);
    }

    private List<Long> getReplaceIdx(final List<Long> worryIdxList, final int deleteSize) {

        final int addAvailableSize = MAX_WORRY_SIZE - (petWorryRequestList.size() - deleteSize);

        return worryIdxList.stream().limit(addAvailableSize).collect(Collectors.toList());
    }

    void setReplaceWorryRequest(final List<PetWorryRequest> replaceWorryRequest,
        final List<Long> deleteWorryList, final Queue<Long> addWorryList,
        final PetWorryRequest request) {

        if (deleteWorryList.contains(request.getIdx())) {
            getReplaceRequest(replaceWorryRequest,addWorryList);
            return;
        }

        replaceWorryRequest.add(
            new PetWorryRequest(request.getIdx(), replaceWorryRequest.size() + 1)
        );

    }

    private void getReplaceRequest(final List<PetWorryRequest> replaceWorryRequest,
        final Queue<Long> addWorryList) {

        if (addWorryList.isEmpty()) {
            return;
        }

        replaceWorryRequest.add(
            new PetWorryRequest(addWorryList.poll(), replaceWorryRequest.size() + 1)
        );

    }
}
