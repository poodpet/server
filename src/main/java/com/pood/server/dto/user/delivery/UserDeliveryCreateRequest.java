package com.pood.server.dto.user.delivery;

import com.pood.server.entity.user.UserDeliveryAddress;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.util.DeliveryRemoteType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserDeliveryCreateRequest {

    @NotEmpty(message = "주소값은 필수값입니다.")
    private String address;

    private int defaultType;

    private String detailAddress;

    private String name;

    private String nickname;

    @NotEmpty(message = "우편번호는 필수값입니다.")
    private String zipcode;

    @Pattern(regexp = "^01(?:0|1|[6-9])?([0-9]{3,4})?([0-9]{4})$",
        message = "휴대폰 번호 입력방식이 잘못되었습니다.")
    private String phoneNumber;

    public UserDeliveryAddress toEntity(final UserInfo userInfo, final int remoteType) {
        return UserDeliveryAddress.builder()
            .address(address)
            .defaultType(defaultType == 1)
            .detailAddress(detailAddress)
            .name(name)
            .nickname(nickname)
            .zipcode(zipcode)
            .remoteType(DeliveryRemoteType.ofLegacyType(remoteType))
            .phoneNumber(phoneNumber)
            .userInfo(userInfo)
            .build();
    }

}
