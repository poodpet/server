package com.pood.server.dto.user.pet;

import com.pood.server.object.user.vo_user_pet_grain_size;

public class dto_user_pet_grain_size extends vo_user_pet_grain_size{

    private Integer idx;

    private String  recordbirth;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    
}
