package com.pood.server.dto.user;

public class dto_user_sms_auth {

    private Integer idx;

    private String  phone_number;

    private String  sms_auth;

    private Integer sms_status;

    private String  updatetime;

    private String  recordbirth;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getSms_auth() {
        return sms_auth;
    }

    public void setSms_auth(String sms_auth) {
        this.sms_auth = sms_auth;
    }

    public Integer getSms_status() {
        return sms_status;
    }

    public void setSms_status(Integer sms_status) {
        this.sms_status = sms_status;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    @Override
    public String toString() {
        return "dto_user_sms_auth [idx=" + idx + ", phone_number=" + phone_number + ", recordbirth=" + recordbirth
                + ", sms_auth=" + sms_auth + ", sms_status=" + sms_status + ", updatetime=" + updatetime + "]";
    }

    
    
}
