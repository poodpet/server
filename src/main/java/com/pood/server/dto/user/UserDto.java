package com.pood.server.dto.user;

import com.pood.server.dto.meta.goods.GoodsDto;
import com.pood.server.dto.meta.goods.PromotionDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class UserDto {

    @Setter
    @Getter
    public static class PointDto {
        private Integer type;
        private String text;
        private Integer point;
        private String expiredDate;
        private String recordBirth;
        private String viewType;

        public PointDto(Integer type, String text, Integer point, String expiredDate, String recordBirth, String viewType) {
            this.type = type;
            this.text = text;
            this.point = point;
            this.expiredDate = expiredDate;
            this.recordBirth = recordBirth;
            this.viewType = viewType;
        }
    }


    @Setter
    @Getter
    public static class Phone {
        private String userPhone;
    }
}
