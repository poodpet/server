package com.pood.server.dto.user.coupon;

import com.pood.server.entity.meta.Coupon;
import com.pood.server.entity.user.UserCoupon;
import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
public class CouponResponseDto {

    private UserCoupon userCoupon;

    private Coupon coupon;

    private List<Integer> brandIdx;

    private List<Integer> goodsIdx;

    private long expirationDate;

}
