package com.pood.server.dto.user.pet;

public class dto_user_pet_ai_feed {
    
    private Integer idx;

    private Integer up_idx;

    private Integer product_idx;

    private Integer user_idx;

    private String  recordbirth;

    

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public Integer getUser_idx() {
        return user_idx;
    }

    public void setUser_idx(Integer user_idx) {
        this.user_idx = user_idx;
    }

    public Integer getUp_idx() {
        return up_idx;
    }

    public void setUp_idx(Integer up_idx) {
        this.up_idx = up_idx;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public Integer getProduct_idx() {
        return product_idx;
    }

    public void setProduct_idx(Integer product_idx) {
        this.product_idx = product_idx;
    }

    @Override
    public String toString() {
        return "dto_user_pet_ai_feed [idx=" + idx + ", product_idx=" + product_idx + ", recordbirth=" + recordbirth
                + ", up_idx=" + up_idx + ", user_idx=" + user_idx + "]";
    } 

    
    
}
