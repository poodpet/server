package com.pood.server.dto.user.review;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

@Getter
@Setter
public class GoodsDetailReviewDtoGroup {

    private final Page<GoodsDetailReviewDto> page;

    public GoodsDetailReviewDtoGroup(
        Page<GoodsDetailReviewDto> page) {
        this.page = page;
    }

    public List<Integer> getUserPscId() {
        return page.stream()
            .map(GoodsDetailReviewDto::getUserPscId)
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    public List<Integer> getUserPetIdx() {
        return page.stream()
            .map(GoodsDetailReviewDto::getUserPetIdx)
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    public Page<GoodsDetailReviewDto> getPage() {
        return page;
    }
}
