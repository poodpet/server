package com.pood.server.dto.user;

import com.pood.server.object.user.vo_user_token;

public class dto_user_token extends vo_user_token{

    private Integer idx;

 
    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }
    
}
