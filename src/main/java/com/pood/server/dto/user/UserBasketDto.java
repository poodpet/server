package com.pood.server.dto.user;

import com.pood.server.dto.meta.goods.GoodsDto;
import com.pood.server.dto.meta.goods.PromotionDto;
import com.pood.server.dto.meta.product.ProductDto;
import com.pood.server.entity.meta.GoodsImage;
import com.pood.server.entity.meta.ProductImage;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
public class UserBasketDto {

    @Setter
    @Getter
    public static class UserBasketListDto {

        @ApiModelProperty(value = "레코드 번호")
        private Integer idx;

        @ApiModelProperty(value = "굿즈 항목 번호 : order_db.goods.idx")
        private Integer goodsIdx;

        @ApiModelProperty(value = "meta_db.promotion.idx")
        private Integer prCodeIdx;

        @ApiModelProperty(value = "장바구니 수량")
        private Integer qty;

        @ApiModelProperty(value = "굿즈 금액")
        private Integer goodsPrice;

        @ApiModelProperty(value = "1: 일반, 2: 바로구매(안보임), 3: 임시저장(안보임)")
        private Integer status;

        @ApiModelProperty(value = "장바구니 담긴 상태의 딜상품 정보")
        private PromotionDto.PromotionGroupGoods2 promotionGroupGoods;

        @ApiModelProperty(value = "적용된 프로모션 상태값")
        private Boolean isPromotionAvailable;

        @ApiModelProperty(value = "프로모션 적용 유무")
        private Boolean isPromotion;

        private GoodsDto.GoodsDetail goodsInfo;

        public void setPromotionAvailable(Boolean promotionAvailable) {
            isPromotionAvailable = promotionAvailable;
        }

        public void setPromotion(Boolean promotion) {
            isPromotion = promotion;
        }

        public void setGoodsInfo(GoodsDto.GoodsDetail goodsInfo) {
            this.goodsInfo = goodsInfo;
        }

        public UserBasketListDto(Integer idx, Integer goodsIdx, Integer prCodeIdx, Integer qty, Integer goodsPrice, Integer status, PromotionDto.PromotionGroupGoods2 promotionGroupGoods) {
            this.idx = idx;
            this.goodsIdx = goodsIdx;
            this.prCodeIdx = prCodeIdx;
            this.qty = qty;
            this.goodsPrice = goodsPrice;
            this.status = status;
            this.promotionGroupGoods = promotionGroupGoods;
        }
    }

    @Setter
    @Getter
    public static class GoodsInfo {
        private Integer idx;
        private String name;
        //9000
        private Integer price;
        //1000
        private Integer goodsPrice;
        //10%
        private Integer discountRate;

        public GoodsInfo(Integer idx, String name, Integer price, Integer goodsPrice, Integer discountRate) {
            this.idx = idx;
            this.name = name;
            this.price = price;
            this.goodsPrice = goodsPrice;
            this.discountRate = discountRate;
        }
    }


}
