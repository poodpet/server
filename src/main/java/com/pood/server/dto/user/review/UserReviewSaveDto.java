package com.pood.server.dto.user.review;

import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserReview;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserReviewSaveDto {

    private int petIdx;

    @NotEmpty(message = "주문 번호는 비어있거나 null 일 수 없습니다.")
    private String orderNumber;
    private int goodsIdx;
    private int rating;

    @NotEmpty(message = "리뷰 코멘트는 비어있거나 null 일 수 없습니다.")
    private String reviewText;

    public UserReview toEntity(final UserInfo userInfo) {
        return UserReview.builder()
            .userIdx(userInfo.getIdx())
            .goodsIdx(goodsIdx)
            .rating(rating)
            .petIdx(petIdx)
            .orderNumber(orderNumber)
            .reviewText(reviewText)
            .build();
    }
}
