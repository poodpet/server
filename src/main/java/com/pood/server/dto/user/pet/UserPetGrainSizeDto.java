package com.pood.server.dto.user.pet;

import lombok.Value;

@Value(staticConstructor = "of")
public class UserPetGrainSizeDto {

    int idx;
    int userPetIdx;
    String name;
    double sizeMin;
    double sizeMax;
}
