package com.pood.server.dto.user.basket;

import com.pood.server.entity.user.UserBasket;
import java.util.Collections;
import java.util.List;

public class UserBasketGroup {

    private final List<UserBasket> userBaskets;

    public UserBasketGroup(final List<UserBasket> userBaskets) {
        this.userBaskets = Collections.unmodifiableList(userBaskets);
    }

    public int totalBoughtQuantity() {
        return userBaskets.stream()
            .mapToInt(UserBasket::getQty)
            .sum();
    }

}
