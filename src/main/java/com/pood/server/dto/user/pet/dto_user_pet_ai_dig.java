package com.pood.server.dto.user.pet;

import com.pood.server.object.user.vo_user_pet_ai_dig;

public class dto_user_pet_ai_dig extends vo_user_pet_ai_dig{

    private Integer idx;

    private String  recordbirth;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    
    
}
