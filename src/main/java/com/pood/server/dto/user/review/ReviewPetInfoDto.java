package com.pood.server.dto.user.review;

import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.user.UserPetImage;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReviewPetInfoDto {

    private static final long MINUTE_MS = 60 * 1000;
    private static final long HOUR_MS = MINUTE_MS * 60;
    private static final long DAY_MS = HOUR_MS * 24;
    private static final long MONTH_MS = HOUR_MS * 24L * 30L;

    private Integer idx;
    private String petName;
    private LocalDate petBirth;
    private Pet petInfo;
    private UserPetImage userPetImage;
    private Integer pscId;

    public boolean eqPscId(final Integer idx) {
        return !Objects.isNull(pscId) && pscId.equals(idx);
    }

    public Integer getPetIdx() {
        return idx;
    }

    public void setUserPetImage(final List<UserPetImage> userPetImages) {
        this.userPetImage = userPetImages.stream()
            .filter(x -> x.getUserPetIdx().equals(idx)).max(Comparator.comparing(
                UserPetImage::getRecordbirth)).orElse(null);
    }
}
