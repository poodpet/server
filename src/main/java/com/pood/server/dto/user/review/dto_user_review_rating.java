package com.pood.server.dto.user.review;

public class dto_user_review_rating {
    
    private Integer rating_1_cnt;

    private Float   rating_1_ratio;
    
    private Float   rating_5_ratio;

    private Float   average_rating;
    
    private Integer rating_3_cnt;
    
    private Integer total_review_cnt;
    
    private Integer rating_5_cnt;
    
    private Float   rating_4_ratio;

    private Integer rating_2_cnt;
    
    private Float   rating_3_ratio;

    private Float   rating_2_ratio;
    
    private Integer rating_4_cnt;

    private Integer total_rating;

    public Integer getRating_1_cnt() {
        return rating_1_cnt;
    }

    public void setRating_1_cnt(Integer rating_1_cnt) {
        this.rating_1_cnt = rating_1_cnt;
    }

    public Float getRating_1_ratio() {
        return rating_1_ratio;
    }

    public void setRating_1_ratio(Float rating_1_ratio) {
        this.rating_1_ratio = rating_1_ratio;
    }

    public Float getRating_5_ratio() {
        return rating_5_ratio;
    }

    public void setRating_5_ratio(Float rating_5_ratio) {
        this.rating_5_ratio = rating_5_ratio;
    }

    public Float getAverage_rating() {
        return average_rating;
    }

    public void setAverage_rating(Float average_rating) {
        this.average_rating = average_rating;
    }

    public Integer getRating_3_cnt() {
        return rating_3_cnt;
    }

    public void setRating_3_cnt(Integer rating_3_cnt) {
        this.rating_3_cnt = rating_3_cnt;
    }

    public Integer getTotal_review_cnt() {
        return total_review_cnt;
    }

    public void setTotal_review_cnt(Integer total_review_cnt) {
        this.total_review_cnt = total_review_cnt;
    }

    public Integer getRating_5_cnt() {
        return rating_5_cnt;
    }

    public void setRating_5_cnt(Integer rating_5_cnt) {
        this.rating_5_cnt = rating_5_cnt;
    }

    public Float getRating_4_ratio() {
        return rating_4_ratio;
    }

    public void setRating_4_ratio(Float rating_4_ratio) {
        this.rating_4_ratio = rating_4_ratio;
    }

    public Integer getRating_2_cnt() {
        return rating_2_cnt;
    }

    public void setRating_2_cnt(Integer rating_2_cnt) {
        this.rating_2_cnt = rating_2_cnt;
    }

    public Float getRating_3_ratio() {
        return rating_3_ratio;
    }

    public void setRating_3_ratio(Float rating_3_ratio) {
        this.rating_3_ratio = rating_3_ratio;
    }

    public Float getRating_2_ratio() {
        return rating_2_ratio;
    }

    public void setRating_2_ratio(Float rating_2_ratio) {
        this.rating_2_ratio = rating_2_ratio;
    }

    public Integer getRating_4_cnt() {
        return rating_4_cnt;
    }

    public void setRating_4_cnt(Integer rating_4_cnt) {
        this.rating_4_cnt = rating_4_cnt;
    }

    public Integer getTotal_rating() {
        return total_rating;
    }

    public void setTotal_rating(Integer total_rating) {
        this.total_rating = total_rating;
    }

    

}
