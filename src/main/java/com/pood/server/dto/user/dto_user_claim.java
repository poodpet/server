package com.pood.server.dto.user;

import java.util.List;

import com.pood.server.dto.dto_image_2;

public class dto_user_claim{
    
    private Integer idx;
        
    private Integer user_idx;

    private String  user_uuid;

    private Integer claim_type;

    private String  claim_text;

    private Integer visible;

    private Integer isAnswer;

    private String  claim_title;

    private String  claim_answer;

    private String  claim_goods_name;

    private Integer claim_goods_idx;

    private String  display_type;

    private String  updatetime;

    private String  recordbirth;
    
    private List<dto_image_2>  main_image;

    private List<dto_image_2>  goods_image;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public Integer getUser_idx() {
        return user_idx;
    }

    public void setUser_idx(Integer user_idx) {
        this.user_idx = user_idx;
    }

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public Integer getClaim_type() {
        return claim_type;
    }

    public void setClaim_type(Integer claim_type) {
        this.claim_type = claim_type;
    }

    public String getClaim_text() {
        return claim_text;
    }

    public void setClaim_text(String claim_text) {
        this.claim_text = claim_text;
    }
 
    public Integer getIsAnswer() {
        return isAnswer;
    }

    public void setIsAnswer(Integer isAnswer) {
        this.isAnswer = isAnswer;
    }

    public String getClaim_title() {
        return claim_title;
    }

    public void setClaim_title(String claim_title) {
        this.claim_title = claim_title;
    }

    public String getClaim_answer() {
        return claim_answer;
    }

    public void setClaim_answer(String claim_answer) {
        this.claim_answer = claim_answer;
    }

    public String getClaim_goods_name() {
        return claim_goods_name;
    }

    public void setClaim_goods_name(String claim_goods_name) {
        this.claim_goods_name = claim_goods_name;
    }

    public Integer getClaim_goods_idx() {
        return claim_goods_idx;
    }

    public void setClaim_goods_idx(Integer claim_goods_idx) {
        this.claim_goods_idx = claim_goods_idx;
    }
    

    public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public List<dto_image_2> getMain_image() {
        return main_image;
    }

    public void setMain_image(List<dto_image_2> main_image) {
        this.main_image = main_image;
    }

    public List<dto_image_2> getGoods_image() {
        return goods_image;
    }

    public void setGoods_image(List<dto_image_2> goods_image) {
        this.goods_image = goods_image;
    }

    public String getDisplay_type() {
        return display_type;
    }

    public void setDisplay_type(String display_type) {
        this.display_type = display_type;
    }

    @Override
    public String toString() {
        return "dto_user_claim [claim_answer=" + claim_answer + ", claim_goods_idx=" + claim_goods_idx
                + ", claim_goods_name=" + claim_goods_name + ", claim_text=" + claim_text + ", claim_title="
                + claim_title + ", claim_type=" + claim_type + ", display_type=" + display_type + ", goods_image="
                + goods_image + ", idx=" + idx + ", isAnswer=" + isAnswer + ", main_image=" + main_image
                + ", recordbirth=" + recordbirth + ", updatetime=" + updatetime + ", user_idx=" + user_idx
                + ", user_uuid=" + user_uuid + ", visible=" + visible + "]";
    }
 
    
 
}
