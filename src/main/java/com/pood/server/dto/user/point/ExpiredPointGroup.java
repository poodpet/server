package com.pood.server.dto.user.point;

import com.pood.server.entity.user.UserPoint;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

public class ExpiredPointGroup {

    private final List<UserPoint> expiredPoints;

    public ExpiredPointGroup(final List<UserPoint> expiredPoints) {
        this.expiredPoints = expiredPoints;
    }

    public int removedPoint() {
        int availablePoint = 0;

        for (final UserPoint userPoint : expiredPoints) {
            final long expiredTime = Duration.between(LocalDateTime.now(), userPoint.getExpiredDate())
                .toDays();
            if (expiredTime < 30) {
                availablePoint += userPoint.getSavedPoint() - userPoint.getUsedPoint();
            }
        }

        return availablePoint;
    }
}
