package com.pood.server.dto.user.review;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;


public class GoodsImagesReviewDtoGroup {

    private final Page<GoodsImagesReviewDto> page;

    public GoodsImagesReviewDtoGroup(
        Page<GoodsImagesReviewDto> page) {
        this.page = page;
    }

    public List<Integer> getUserPscId() {
        return page.stream()
            .map(GoodsImagesReviewDto::getUserPscId)
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    public List<Integer> getUserPetIdx() {
        return page.stream()
            .map(GoodsImagesReviewDto::getUserPetIdx)
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    public Page<GoodsImagesReviewDto> getPage() {
        return page;
    }
}
