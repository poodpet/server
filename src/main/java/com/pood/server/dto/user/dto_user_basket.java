package com.pood.server.dto.user;

import com.pood.server.dto.meta.goods.dto_goods_8;
import com.pood.server.object.user.vo_user_basket;

public class dto_user_basket extends vo_user_basket{
 
    private Integer idx;

    private String  updatetime;

    private String  recordbirth;

    private dto_goods_8 goods_info;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public dto_goods_8 getGoods_info() {
        return goods_info;
    }

    public void setGoods_info(dto_goods_8 goods_info) {
        this.goods_info = goods_info;
    }
     
}
