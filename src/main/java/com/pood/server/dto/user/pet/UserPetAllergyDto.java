package com.pood.server.dto.user.pet;

import lombok.Value;

@Value(staticConstructor = "of")
public class UserPetAllergyDto {

    int idx;
    int userPetIdx;
    int type;
    String name;
}
