package com.pood.server.dto.user.delivery;

import com.pood.server.entity.user.UserDeliveryAddress;
import com.pood.server.util.DeliveryRemoteType;
import java.io.Serializable;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class UserDeliveryAddressResponse implements Serializable {

    private int idx;
    private String address;
    private int defaultType;
    private String detailAddress;
    private String name;
    private String nickname;
    private String zipcode;
    private Integer remoteType;
    private String phoneNumber;

    @Builder(access = AccessLevel.PRIVATE)
    private UserDeliveryAddressResponse(final int idx, final String address, final boolean defaultType,
        final String detailAddress, final String name, final String nickname, final String zipcode,
        final DeliveryRemoteType remoteType, final String phoneNumber) {
        this.idx = idx;
        this.address = address;
        this.defaultType = defaultType ? 1 : 0;
        this.detailAddress = detailAddress;
        this.name = name;
        this.nickname = nickname;
        this.zipcode = zipcode;
        this.remoteType = remoteType.getValue();
        this.phoneNumber = phoneNumber;
    }

    public static UserDeliveryAddressResponse toResponse(
        final UserDeliveryAddress userDeliveryAddress) {
        return UserDeliveryAddressResponse.builder()
            .idx(userDeliveryAddress.getIdx())
            .address(userDeliveryAddress.getAddress())
            .defaultType(userDeliveryAddress.isDefaultType())
            .detailAddress(userDeliveryAddress.getDetailAddress())
            .name(userDeliveryAddress.getName())
            .nickname(userDeliveryAddress.getNickname())
            .zipcode(userDeliveryAddress.getZipcode())
            .remoteType(userDeliveryAddress.getRemoteType())
            .phoneNumber(userDeliveryAddress.getPhoneNumber())
            .build();
    }
}
