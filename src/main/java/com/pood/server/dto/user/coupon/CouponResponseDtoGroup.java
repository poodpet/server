package com.pood.server.dto.user.coupon;

import java.util.Collections;
import java.util.List;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CouponResponseDtoGroup {

    private List<CouponResponseDto> couponResponseDtoList;

    public CouponResponseDtoGroup(final List<CouponResponseDto> couponResponseDtoList) {
        this.couponResponseDtoList = Collections.unmodifiableList(couponResponseDtoList);
    }
}
