package com.pood.server.dto.user.pet;

import com.pood.server.entity.user.PetAllergyJoinView;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class UserPetAllergyDtoGroup {

    private final List<PetAllergyJoinView> petAllergyJoinViewList;

    public UserPetAllergyDtoGroup(final List<PetAllergyJoinView> petAllergyJoinViewList) {
        this.petAllergyJoinViewList = petAllergyJoinViewList;
    }

    public Map<Integer, List<UserPetAllergyDto>> createAndGroupByUserPetIdxMap() {
        return petAllergyJoinViewList.stream()
            .map(allergyData ->
                UserPetAllergyDto.of(
                    allergyData.getAllergyIdx(),
                    allergyData.getUserPetIdx(),
                    allergyData.getType(),
                    allergyData.getName()
                ))
            .collect(Collectors.groupingBy(UserPetAllergyDto::getUserPetIdx));
    }
}
