package com.pood.server.dto.user.coupon;

import com.pood.server.entity.meta.CouponGoods;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;

@Getter
public class CouponGoodsGroup {

    private final List<CouponGoods> couponGoodsList;

    public CouponGoodsGroup(final List<CouponGoods> couponGoodsList) {
        this.couponGoodsList = Collections.unmodifiableList(couponGoodsList);
    }

    public List<Integer> getCouponGoodsIdxList(final int couponIdx) {
        return couponGoodsList.stream()
            .filter(couponGoods -> couponGoods.getCouponIdx() == couponIdx)
            .map(CouponGoods::getGoodsIdx)
            .collect(Collectors.toList());
    }
}
