package com.pood.server.dto.user.review;

public class dto_user_review_image {

    private Integer order_idx;

    private String  recordbirth;

    private Integer visible;

    private Integer image_idx;

    private String  updatetime;

    private String  url;

    private Integer seq;

    public Integer getOrder_idx() {
        return order_idx;
    }

    public void setOrder_idx(Integer order_idx) {
        this.order_idx = order_idx;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public Integer getImage_idx() {
        return image_idx;
    }

    public void setImage_idx(Integer image_idx) {
        this.image_idx = image_idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    @Override
    public String toString() {
        return "dto_user_review_image [image_idx=" + image_idx + ", order_idx=" + order_idx + ", recordbirth="
                + recordbirth + ", seq=" + seq + ", updatetime=" + updatetime + ", url=" + url + ", visible=" + visible
                + "]";
    }

    
}
