package com.pood.server.dto.user.review;

public class dto_user_review_clap {

    private Integer idx;

    private Integer user_idx;

    private Integer review_idx;

    private String  updatetime;

    private String  recordbirth;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public Integer getUser_idx() {
        return user_idx;
    }

    public void setUser_idx(Integer user_idx) {
        this.user_idx = user_idx;
    }

    public Integer getReview_idx() {
        return review_idx;
    }

    public void setReview_idx(Integer review_idx) {
        this.review_idx = review_idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    @Override
    public String toString() {
        return "dto_user_review_clap [idx=" + idx + ", recordbirth=" + recordbirth + ", review_idx=" + review_idx
                + ", updatetime=" + updatetime + ", user_idx=" + user_idx + "]";
    }

    
}
