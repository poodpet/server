package com.pood.server.dto.user.basket;

import com.pood.server.exception.LimitQuantityException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum BuyLimit {
    INFINITY(0) {
        @Override
        int remainQuantity(final int totalQuantity, final int limitQuantity,
            final BasketQuantity quantity) {
            return 0;
        }
    },
    ONLY_ONE(1) {
        @Override
        int remainQuantity(final int totalQuantity, final int limitQuantity,
            final BasketQuantity basketQuantity) {

            if (totalQuantity >= limitQuantity) {
                throw new LimitQuantityException("장바구니에 해당 상품이 이미 최대치까지 들어있습니다.");
            }

            if (basketQuantity.getQuantity() >= limitQuantity) {
                throw new LimitQuantityException("이미 이전에 물품을 " + limitQuantity + "개 구매한 이력이 있습니다.");
            }

            return 1;
        }
    },
    LIMIT_AMOUNT(2) {
        @Override
        int remainQuantity(final int totalQuantity, final int limitQuantity,
            final BasketQuantity basketQuantity) {

            if (totalQuantity >= limitQuantity) {
                throw new LimitQuantityException("장바구니에 해당 상품이 이미 최대치까지 들어있습니다.");
            }

            int available = limitQuantity - totalQuantity;

            if (basketQuantity.getQuantity() >= available) {
                throw new LimitQuantityException("이미 이전에 물품을 " + limitQuantity + "개 구매한 이력이 있습니다.");
            }

            return available - basketQuantity.getQuantity();
        }
    };

    private final int buyLimit;

    public static int of(final int limitType, final int totalQuantity, final int limitQuantity,
        final BasketQuantity basketQuantity) {
        if (limitType == 1) {
            return ONLY_ONE.remainQuantity(totalQuantity, limitQuantity, basketQuantity);
        }

        if (limitType == 2) {
            return LIMIT_AMOUNT.remainQuantity(totalQuantity, limitQuantity, basketQuantity);
        }

        return INFINITY.remainQuantity(totalQuantity, limitQuantity, basketQuantity);
    }

    abstract int remainQuantity(final int totalQuantity, final int limitQuantity,
        final BasketQuantity quantity);

}
