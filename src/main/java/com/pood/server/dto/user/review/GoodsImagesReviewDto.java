package com.pood.server.dto.user.review;

import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.user.UserPetImage;
import java.util.List;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GoodsImagesReviewDto {

    private String url;

    private ReviewUserInfoDto userInfo;

    private ReviewPetInfoDto userPetInfo;

    private String reviewText;

    public Integer getUserPetIdx() {
        return userPetInfo.getPetIdx();
    }

    public Integer getUserPscId() {
        return userPetInfo.getPscId();
    }

    public void setPetInfo(final List<Pet> petList) {
        for (Pet pet : petList) {
            if (userPetInfo.eqPscId(pet.getIdx())) {
                userPetInfo.setPetInfo(pet);
                break;
            }
        }
    }

    public void setPetImages(final List<UserPetImage> userPetImages) {
        if (Objects.nonNull(userPetInfo)) {
            userPetInfo.setUserPetImage(userPetImages);
        }
    }

}
