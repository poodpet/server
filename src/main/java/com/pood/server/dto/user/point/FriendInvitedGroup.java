package com.pood.server.dto.user.point;

import com.pood.server.entity.user.UserPoint;
import java.util.Collections;
import java.util.List;

public class FriendInvitedGroup {

    private final List<UserPoint> friendInvitedList;

    public FriendInvitedGroup(final List<UserPoint> friendInvitedList) {
        this.friendInvitedList = Collections.unmodifiableList(friendInvitedList);
    }

    public int totalFriendInvitePoint() {
        return friendInvitedList.stream()
            .filter(UserPoint::isNotRewardPoint)
            .mapToInt(UserPoint::getPointPrice).sum();
    }
}
