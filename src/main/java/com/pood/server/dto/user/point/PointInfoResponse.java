package com.pood.server.dto.user.point;

import lombok.Value;

@Value(staticConstructor = "of")
public class PointInfoResponse {

    int presentPoint;
    int receivedBenefitPoint;
    int willBeRewardPoint;
    int removedPoint;
}
