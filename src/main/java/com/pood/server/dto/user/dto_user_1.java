package com.pood.server.dto.user;

public class dto_user_1 {

    private Integer idx;

    private String  referral_code;
    
    private String  user_email;
    
    private String  user_nickname;
    
    private Integer user_status;

    private String  user_uuid;
    
    private Integer user_login_type;
    
    private String  recordbirth;

    private String  user_name;
    
    private String  ml_name;
    
    private Float   ml_rate;
    
    private Boolean user_service_agree;

    private Boolean service_push;

    private Boolean pood_push;

    private Boolean order_push;

    private Boolean cat_pood_push;

    private Boolean dog_pood_push;

    public Boolean getCat_pood_push() {
        return cat_pood_push;
    }

    public void setCat_pood_push(Boolean cat_pood_push) {
        this.cat_pood_push = cat_pood_push;
    }

    public Boolean getDog_pood_push() {
        return dog_pood_push;
    }

    public void setDog_pood_push(Boolean dog_pood_push) {
        this.dog_pood_push = dog_pood_push;
    }

    private String  token;

    private String user_phone;

    private Long user_base_image_idx;

    private String userBaseImg;

    public void setUserBaseImg(String userBaseImg) {
        this.userBaseImg = userBaseImg;
    }

    public String getUserBaseImg() {
        return userBaseImg;
    }

    public Long getUser_base_image_idx() {
        return user_base_image_idx;
    }

    public void setUser_base_image_idx(Long userBaseImageIdx) {
        this.user_base_image_idx = userBaseImageIdx;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getReferral_code() {
        return referral_code;
    }

    public void setReferral_code(String referral_code) {
        this.referral_code = referral_code;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_nickname() {
        return user_nickname;
    }

    public void setUser_nickname(String user_nickname) {
        this.user_nickname = user_nickname;
    }

    public Integer getUser_status() {
        return user_status;
    }

    public void setUser_status(Integer user_status) {
        this.user_status = user_status;
    }

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public Integer getUser_login_type() {
        return user_login_type;
    }

    public void setUser_login_type(Integer user_login_type) {
        this.user_login_type = user_login_type;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }
 
 
    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getMl_name() {
        return ml_name;
    }

    public void setMl_name(String ml_name) {
        this.ml_name = ml_name;
    }

    public Float getMl_rate() {
        return ml_rate;
    }

    public void setMl_rate(Float ml_rate) {
        this.ml_rate = ml_rate;
    }
 
    public Boolean getUser_service_agree() {
        return user_service_agree;
    }

    public void setUser_service_agree(Boolean user_service_agree) {
        this.user_service_agree = user_service_agree;
    }

    public Boolean getService_push() {
        return service_push;
    }

    public void setService_push(Boolean service_push) {
        this.service_push = service_push;
    }

    public Boolean getPood_push() {
        return pood_push;
    }

    public void setPood_push(Boolean pood_push) {
        this.pood_push = pood_push;
    }

    public Boolean getOrder_push() {
        return order_push;
    }

    public void setOrder_push(Boolean order_push) {
        this.order_push = order_push;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    @Override
    public String toString() {
        return "dto_user_1 [idx=" + idx + ", ml_name=" + ml_name + ", ml_rate=" + ml_rate + ", order_push=" + order_push
                + ", pood_push=" + pood_push + ", recordbirth=" + recordbirth + ", referral_code=" + referral_code
                + ", service_push=" + service_push + ", token=" + token + ", user_email=" + user_email
                + ", user_login_type=" + user_login_type + ", user_name=" + user_name + ", user_nickname="
                + user_nickname + ", user_service_agree=" + user_service_agree + ", user_status=" + user_status
                + ", user_uuid=" + user_uuid + "]";
    }
 
    
}
