package com.pood.server.dto.user.coupon;

import com.pood.server.entity.meta.Coupon;
import com.pood.server.exception.NotFoundException;
import java.util.Collections;
import java.util.List;

public class CouponGroup {

    private final List<Coupon> couponList;

    public CouponGroup(final List<Coupon> couponList) {
        this.couponList = Collections.unmodifiableList(couponList);
    }

    public Coupon getByCouponIdx(final int idx) {
        return couponList.stream()
            .filter(coupon -> coupon.getIdx() == idx)
            .findFirst()
            .orElseThrow(() -> new NotFoundException("쿠폰을 찾을 수 없습니다."));
    }
}
