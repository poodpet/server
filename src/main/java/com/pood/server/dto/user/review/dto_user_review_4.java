package com.pood.server.dto.user.review;

import java.util.HashMap;
import java.util.List;

import com.pood.server.dto.dto_image_3;
import com.pood.server.object.resp.resp_user_pet;
import com.pood.server.object.user.vo_user_review_2;

public class dto_user_review_4 extends vo_user_review_2{

    private Integer idx;

    private String  updatetime;

    private String  recordbirth;

    private Integer isClap;

    private Integer rc_cnt;

    private List<dto_image_3> image;

    private HashMap<String, Object> user_info;

    private resp_user_pet pet_info;

    public HashMap<String, Object> getUser_info() {
        return user_info;
    }

    public void setUser_info(HashMap<String, Object> user_info) {
        this.user_info = user_info;
    }

    public Integer getIsClap() {
        return isClap;
    }

    public void setIsClap(Integer isClap) {
        this.isClap = isClap;
    }

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public List<dto_image_3> getImage() {
        return image;
    }

    public void setImage(List<dto_image_3> image) {
        this.image = image;
    }

    public resp_user_pet getPet_info() {
        return pet_info;
    }

    public void setPet_info(resp_user_pet pet_info) {
        this.pet_info = pet_info;
    }

    public Integer getRc_cnt() {
        return rc_cnt;
    }

    public void setRc_cnt(Integer rc_cnt) {
        this.rc_cnt = rc_cnt;
    }

    
    
}
