package com.pood.server.dto.user.pet;

import lombok.Value;

@Value(staticConstructor = "of")
public class UserPetAiDiagnosisDto {

    int idx;
    int userPetIdx;
    Long petWorryIdx;
    String name;
    String dogUrl;
    String catUrl;
}
