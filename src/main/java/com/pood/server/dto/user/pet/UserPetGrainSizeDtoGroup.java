package com.pood.server.dto.user.pet;

import com.pood.server.entity.user.PetGrainSizeJoinView;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class UserPetGrainSizeDtoGroup {

    private final List<PetGrainSizeJoinView> petGrainSizeJoinViewList;

    public UserPetGrainSizeDtoGroup(final List<PetGrainSizeJoinView> petGrainSizeJoinViewList) {
        this.petGrainSizeJoinViewList = petGrainSizeJoinViewList;
    }

    public Map<Integer, List<UserPetGrainSizeDto>> createAndGroupByUserPetIdxMap() {
        return petGrainSizeJoinViewList.stream()
            .map(grainSize ->
                UserPetGrainSizeDto.of(
                    grainSize.getGrainSizeIdx(),
                    grainSize.getUserPetIdx(),
                    grainSize.getName(),
                    grainSize.getSizeMin(),
                    grainSize.getSizeMax()
                ))
            .collect(Collectors.groupingBy(UserPetGrainSizeDto::getUserPetIdx));
    }
}
