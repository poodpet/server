package com.pood.server.dto.user.review;

import com.pood.server.entity.user.UserBaseImage;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReviewUserInfoDto {

    private Integer idx;
    private String userNickname;
    private UserBaseImage userBaseImage;

}
