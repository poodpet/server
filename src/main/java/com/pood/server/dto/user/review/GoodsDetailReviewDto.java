package com.pood.server.dto.user.review;

import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.user.UserPetImage;
import com.pood.server.entity.user.UserReviewImage;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GoodsDetailReviewDto {

    private Integer idx;

    private String reviewText;

    private ReviewUserInfoDto userInfo;

    private ReviewPetInfoDto userPetInfo;

    private long likeCount;

    private LocalDateTime recordbirth;

    private List<UserReviewImage> userReviewImages = new ArrayList<>();

    private boolean isClap;

    public void setPetInfo(final List<Pet> petList) {
        for (Pet pet : petList) {
            if (userPetInfo.eqPscId(pet.getIdx())) {
                userPetInfo.setPetInfo(pet);
                break;
            }
        }
    }

    public Integer getUserPetIdx() {
        return userPetInfo.getPetIdx();
    }

    public Integer getUserPscId() {
        return userPetInfo.getPscId();
    }

    public void addUserReviewImagesEqidx(final List<UserReviewImage> userReviewImages) {
        final Map<Integer, List<UserReviewImage>> userReviewImagesGroup = userReviewImages.stream()
            .collect(Collectors.groupingBy(userReview -> userReview.getUserReview().getIdx()));
        this.userReviewImages = userReviewImagesGroup.get(idx);
    }

    public void setPetImages(final List<UserPetImage> userPetImages) {
        if (Objects.nonNull(userPetInfo)) {
            userPetInfo.setUserPetImage(userPetImages);
        }
    }

}
