package com.pood.server.dto.user.pet;
import java.util.List;
import com.pood.server.dto.dto_image_1;
import com.pood.server.object.user.vo_user_pet_1;

public class dto_user_pet extends vo_user_pet_1{

    private Integer idx;

    private String  updatetime;

    private String  recordbirth;

    private List<dto_image_1> image;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public List<dto_image_1> getImage() {
        return image;
    }

    public void setImage(List<dto_image_1> image) {
        this.image = image;
    }

    
    
    
}
