package com.pood.server.dto;

public class dto_column_list {

    private String  Field;

    private String  Type;

    private String  Null;

    private String  Extra;

    private String  Default;

    private String  Key;

    public String getField() {
        return Field;
    }

    public void setField(String field) {
        Field = field;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getNull() {
        return Null;
    }

    public void setNull(String null1) {
        Null = null1;
    }

    public String getExtra() {
        return Extra;
    }

    public void setExtra(String extra) {
        Extra = extra;
    }

    public String getDefault() {
        return Default;
    }

    public void setDefault(String default1) {
        Default = default1;
    }

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    @Override
    public String toString() {
        return "dto_column_list [Default=" + Default + ", Extra=" + Extra + ", Field=" + Field + ", Key=" + Key
                + ", Null=" + Null + ", Type=" + Type + "]";
    }

    
}
