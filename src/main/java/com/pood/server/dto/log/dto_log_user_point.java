package com.pood.server.dto.log;

public class dto_log_user_point {

    private Integer idx;

    private String  user_uuid;

    private String  point_uuid;

    private Integer point;

    private String  recordbirth;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public String getPoint_uuid() {
        return point_uuid;
    }

    public void setPoint_uuid(String point_uuid) {
        this.point_uuid = point_uuid;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    @Override
    public String toString() {
        return "dto_log_user_point [idx=" + idx + ", point=" + point + ", point_uuid=" + point_uuid + ", recordbirth="
                + recordbirth + ", user_uuid=" + user_uuid + "]";
    }

    

}
