package com.pood.server.dto.log;

public class dto_log_user_login {

    private Integer idx;

    private Integer user_idx;

    private String  user_uuid;

    private Integer inout;

    private Integer os_type;

    private String  device;

    private String  version;

    private String  recordbirth;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public Integer getUser_idx() {
        return user_idx;
    }

    public void setUser_idx(Integer user_idx) {
        this.user_idx = user_idx;
    }

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public Integer getInout() {
        return inout;
    }

    public void setInout(Integer inout) {
        this.inout = inout;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public Integer getOs_type() {
        return os_type;
    }

    public void setOs_type(Integer os_type) {
        this.os_type = os_type;
    }

    @Override
    public String toString() {
        return "dto_log_user_login [device=" + device + ", idx=" + idx + ", inout=" + inout + ", os_type=" + os_type
                + ", recordbirth=" + recordbirth + ", user_idx=" + user_idx + ", user_uuid=" + user_uuid + ", version="
                + version + "]";
    }
 
    
    
    
}
