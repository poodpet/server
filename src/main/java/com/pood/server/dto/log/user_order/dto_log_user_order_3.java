package com.pood.server.dto.log.user_order;

import com.pood.server.object.log.vo_log_user_order;
import com.pood.server.object.meta.vo_delivery_track;

public class dto_log_user_order_3 extends vo_log_user_order{

    private Integer idx;

    private String  recordbirth;

    private vo_delivery_track delivery_track;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public vo_delivery_track getDelivery_track() {
        return delivery_track;
    }

    public void setDelivery_track(vo_delivery_track delivery_track) {
        this.delivery_track = delivery_track;
    }

    
    
}
