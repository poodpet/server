package com.pood.server.dto.log;

public class dto_log_user_join {

    private Integer idx;

    private Integer user_idx;

    private String  user_uuid;

    private Integer login_type;

    private String  sns_key;

    private String  recordbirth;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public Integer getUser_idx() {
        return user_idx;
    }

    public void setUser_idx(Integer user_idx) {
        this.user_idx = user_idx;
    }

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public Integer getLogin_type() {
        return login_type;
    }

    public void setLogin_type(Integer login_type) {
        this.login_type = login_type;
    }

    public String getSns_key() {
        return sns_key;
    }

    public void setSns_key(String sns_key) {
        this.sns_key = sns_key;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    @Override
    public String toString() {
        return "dto_log_user_join [idx=" + idx + ", login_type=" + login_type + ", recordbirth=" + recordbirth
                + ", sns_key=" + sns_key + ", user_idx=" + user_idx + ", user_uuid=" + user_uuid + "]";
    }

    
}
