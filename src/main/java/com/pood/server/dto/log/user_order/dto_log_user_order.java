package com.pood.server.dto.log.user_order;

import com.pood.server.object.log.vo_log_user_order;

public class dto_log_user_order extends vo_log_user_order{

    private Integer idx;

    private String  recordbirth;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    
    
}
