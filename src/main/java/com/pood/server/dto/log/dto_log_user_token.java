package com.pood.server.dto.log;

public class dto_log_user_token {

    private Integer idx;

    private String  token;

    private String  user_uuid;

    private String  recordbirth;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    @Override
    public String toString() {
        return "dto_log_user_token [idx=" + idx + ", recordbirth=" + recordbirth + ", token=" + token + ", user_uuid="
                + user_uuid + "]";
    }

    
}
