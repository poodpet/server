package com.pood.server.dto.log.user_order;

public class dto_log_user_order_2 {

    private Integer goods_idx;

    private Integer order_status;

    private Integer history_idx;

    private String  user_uuid;

    private String  recordbirth;

    private String  order_number;

    private Integer qty;

    private String  order_text;

    public Integer getGoods_idx() {
        return goods_idx;
    }

    public void setGoods_idx(Integer goods_idx) {
        this.goods_idx = goods_idx;
    }

    public Integer getOrder_status() {
        return order_status;
    }

    public void setOrder_status(Integer order_status) {
        this.order_status = order_status;
    }

    public Integer getHistory_idx() {
        return history_idx;
    }

    public void setHistory_idx(Integer history_idx) {
        this.history_idx = history_idx;
    }

    public String getUser_uuid() {
        return user_uuid;
    }

    public void setUser_uuid(String user_uuid) {
        this.user_uuid = user_uuid;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public String getOrder_text() {
        return order_text;
    }

    public void setOrder_text(String order_text) {
        this.order_text = order_text;
    }

    
}
