package com.pood.server.dto.log.user_order;

import com.pood.server.entity.order.OrderCancel;
import com.pood.server.entity.order.OrderExchange;
import com.pood.server.entity.order.OrderRefund;
import java.time.LocalDateTime;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class OrderDto {


    @Setter
    @Getter
    public static class UserOrderDto {
        private Integer idx;
        private String orderName;
        private String orderNumber;
        private LocalDateTime recordbirth;
        private Integer totalPrice;
        private Integer orderPrice;
        private Integer deliveryFee;
        private Integer savedPoint;
        private Integer usedPoint;
        private Integer discountCouponPrice;
        private String memo;
        private String orderTypeName;
        private OrderAddress orderAddress;
        private Delivery delivery;
        private List<OrderDto.UserOrderInfo> userOrderInfoList;
        private Integer orderStatus;
        public UserOrderDto(Integer idx, String orderName, String orderNumber, LocalDateTime recordbirth, Integer totalPrice, Integer orderPrice, Integer deliveryFee, Integer savedPoint, Integer usedPoint, Integer discountCouponPrice, String memo, String orderTypeName,OrderAddress orderAddress,Delivery delivery, List<UserOrderInfo> userOrderInfoList, Integer orderStatus) {
            this.idx = idx;
            this.orderName = orderName;
            this.orderNumber = orderNumber;
            this.recordbirth = recordbirth;
            this.totalPrice = totalPrice;
            this.orderPrice = orderPrice;
            this.deliveryFee = deliveryFee;
            this.savedPoint = savedPoint;
            this.usedPoint = usedPoint;
            this.discountCouponPrice = discountCouponPrice;
            this.memo = memo;
            this.orderTypeName = orderTypeName;
            this.orderAddress = orderAddress;
            this.delivery =delivery;
            this.userOrderInfoList = userOrderInfoList;
            this.orderStatus = orderStatus;
        }
        public UserOrderDto(Integer idx, String orderName, String orderNumber, LocalDateTime recordbirth, Integer totalPrice, Integer orderPrice, Integer deliveryFee, Integer savedPoint, Integer usedPoint, Integer discountCouponPrice, String memo, String orderTypeName,OrderAddress orderAddress,Delivery delivery) {
            this.idx = idx;
            this.orderName = orderName;
            this.orderNumber = orderNumber;
            this.recordbirth = recordbirth;
            this.totalPrice = totalPrice;
            this.orderPrice = orderPrice;
            this.deliveryFee = deliveryFee;
            this.savedPoint = savedPoint;
            this.usedPoint = usedPoint;
            this.discountCouponPrice = discountCouponPrice;
            this.memo = memo;
            this.orderTypeName = orderTypeName;
            this.orderAddress = orderAddress;
            this.delivery =delivery;
        }
        public void setOrderAddress(OrderAddress orderAddress) {
            this.orderAddress = orderAddress;
        }
    }


    @Setter
    @Getter
    public static class OrderAddress {
        private String address;
        private String detailAddress;
        private String name;
        private String nickName;
        private String phoneNumber;
        private String zipcode;
    }

    @Setter
    @Getter
    public static class Delivery {
        private String courier;
        private String trackingId;
    }

    @Setter
    @Getter
    public static class UserOrderInfo {
        private Integer idx;
        private Integer goodsIdx;
        private String displayType;
        private String goodsName;
        private Integer goodsPrice;
        private String image;
        private Integer quantity;
        private OrderCancel orderCancel;
        private OrderExchange orderExchange;
        private OrderRefund orderRefund;
        private boolean possibleWriteReview;
        private boolean directDelivery;
    }



    @Setter
    @Getter
    public static class OrderRecordBirthData {
        private Integer idx;
        private String type;
        private LocalDateTime recordBirth;

        public OrderRecordBirthData(Integer idx, String type, LocalDateTime recordBirth) {
            this.idx = idx;
            this.type = type;
            this.recordBirth = recordBirth;
        }
    }
}
