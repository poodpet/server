package com.pood.server.dto.log;

import com.pood.server.object.log.vo_log_user_notice_alaram;

public class dto_log_user_notice_alaram extends vo_log_user_notice_alaram{

    private Integer idx;

    private String  recordbirth;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    @Override
    public String toString() {
        return "dto_log_user_notice_alaram [idx=" + idx + ", recordbirth=" + recordbirth + "]";
    }

    
}
