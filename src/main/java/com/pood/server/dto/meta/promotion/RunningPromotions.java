package com.pood.server.dto.meta.promotion;

import java.util.Collections;
import java.util.List;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RunningPromotions {

    private final List<RunningPromotion> promotionList;

    public List<RunningPromotion> sortedList() {
        Collections.sort(promotionList);
        return promotionList;
    }
}
