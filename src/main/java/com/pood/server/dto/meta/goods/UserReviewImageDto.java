package com.pood.server.dto.meta.goods;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class UserReviewImageDto {

    @Setter
    @Getter
    public static class ReviewPhotoList {
        private Integer idx;
        private String url;
    }

}
