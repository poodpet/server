package com.pood.server.dto.meta.event;

import com.querydsl.core.annotations.QueryProjection;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class EndPhotoAward {

    Integer idx;
    String imgUrl;

    @QueryProjection
    public EndPhotoAward(final Integer idx, final String imgUrl) {
        this.idx = idx;
        this.imgUrl = imgUrl;
    }

}
