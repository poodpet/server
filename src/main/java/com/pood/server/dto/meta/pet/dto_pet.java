package com.pood.server.dto.meta.pet;

import java.util.List;

import com.pood.server.object.meta.vo_pet;
import com.pood.server.dto.dto_image_1;

public class dto_pet extends vo_pet{
    
    private Integer idx;

    private String  updatetime;

    private String  recordbirth;

    private List<dto_image_1> image;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public List<dto_image_1> getImage() {
        return image;
    }

    public void setImage(List<dto_image_1> image) {
        this.image = image;
    }

    
}
