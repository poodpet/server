package com.pood.server.dto.meta.order;

import com.pood.server.object.meta.vo_order_delivery;

public class dto_order_delivery extends vo_order_delivery{

    private Integer delivery_idx;

    private String  recordbirth;

    private String  updatetime;

    public Integer getDelivery_idx() {
        return delivery_idx;
    }

    public void setDelivery_idx(Integer delivery_idx) {
        this.delivery_idx = delivery_idx;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    @Override
    public String toString() {
        return "dto_order_delivery [delivery_idx=" + delivery_idx + ", recordbirth=" + recordbirth + ", updatetime="
                + updatetime + "]";
    }

    


}
