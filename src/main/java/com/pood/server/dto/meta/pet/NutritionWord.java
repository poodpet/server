package com.pood.server.dto.meta.pet;

import lombok.Value;

@Value
public class NutritionWord {

    String title;
    String suitable;
    String baseValue;
}
