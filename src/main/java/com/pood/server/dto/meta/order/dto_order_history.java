package com.pood.server.dto.meta.order;

import java.util.List;

import com.pood.server.dto.log.user_order.dto_log_user_order;
import com.pood.server.object.meta.vo_order_delivery_track;

public class dto_order_history extends dto_log_user_order{

    public List<vo_order_delivery_track> delivery_track;

    public List<vo_order_delivery_track> getDelivery_track() {
        return delivery_track;
    }

    public void setDelivery_track(List<vo_order_delivery_track> delivery_track) {
        this.delivery_track = delivery_track;
    }

    public void setObject(dto_log_user_order e){
        setIdx(e.getIdx());
        setOrder_idx(e.getOrder_idx());
        setUser_uuid(e.getUser_uuid());
        setOrder_number(e.getOrder_number());
        setOrder_status(e.getOrder_status());
        setOrder_text(e.getOrder_text());
        setGoods_idx(e.getGoods_idx());
        setQty(e.getQty());
        setRecordbirth(e.getRecordbirth());
    }

    @Override
    public String toString() {
        return "dto_order_history [delivery_track=" + delivery_track + "]";
    }

    
}
