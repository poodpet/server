package com.pood.server.dto.meta.event;

import com.pood.server.dto.meta.goods.dto_goods_10;

public class dto_event_goods_list_2 {
    
    private Integer idx;

    private Integer event_idx;

    private Integer priority;

    private dto_goods_10 goods_info;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public Integer getEvent_idx() {
        return event_idx;
    }

    public void setEvent_idx(Integer event_idx) {
        this.event_idx = event_idx;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public dto_goods_10 getGoods_info() {
        return goods_info;
    }

    public void setGoods_info(dto_goods_10 goods_info) {
        this.goods_info = goods_info;
    }

    @Override
    public String toString() {
        return "dto_event_goods_list_2 [event_idx=" + event_idx + ", goods_info=" + goods_info + ", idx=" + idx
                + ", priority=" + priority + "]";
    }

    
}
