package com.pood.server.dto.meta.promotion;

import com.pood.server.dto.meta.goods.PromotionDto;
import com.pood.server.dto.meta.goods.PromotionDto.PromotionGoods;
import com.pood.server.entity.meta.PromotionGroup;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PromotionGroupDtoGroup {

    private final List<PromotionDto.PromotionGroup> promotionGroupList;

    public PromotionGroupDtoGroup() {
        this.promotionGroupList = new ArrayList<>();
    }

    public void add(final PromotionGroup promotionGroup,
        final List<PromotionDto.PromotionGoods> promotionGroupGoodsList) {

        List<PromotionGoods> list = promotionGroupGoodsList
            .stream()
            .filter(
                promotionGoods -> promotionGoods.eqPrGroupIdx(promotionGroup.getIdx()))
            .collect(Collectors.toList());

        promotionGroupList.add(new PromotionDto.PromotionGroup(
            promotionGroup.getName(), list));
    }

    public List<PromotionDto.PromotionGroup> getData() {
        return promotionGroupList;
    }
}
