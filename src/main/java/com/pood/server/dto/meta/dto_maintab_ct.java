package com.pood.server.dto.meta;

import com.pood.server.object.meta.maintab.vo_maintab_ct;

public class dto_maintab_ct extends vo_maintab_ct{

    private Integer idx;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }
    
}
