package com.pood.server.dto.meta.goods;

import com.pood.server.controller.response.goods.SuggestRandomGoodsResponse;
import com.pood.server.dto.meta.goods.PromotionDto.PromotionGoodsProductInfo;
import com.pood.server.entity.meta.mapper.SuggestRandomGoodsMapper;
import java.util.List;
import java.util.stream.Collectors;

public class SuggestRandomGoodsGroup {

    private final List<SuggestRandomGoodsMapper> suggestGoodsList;

    public SuggestRandomGoodsGroup(final List<SuggestRandomGoodsMapper> suggestGoodsList) {
        this.suggestGoodsList = suggestGoodsList;
    }

    public List<Integer> goodsIdxList() {
        return suggestGoodsList.stream()
            .map(SuggestRandomGoodsMapper::getIdx)
            .collect(Collectors.toList());
    }

    public void isPromotionThenUpdatePrice(final List<PromotionGoodsProductInfo> activePromotionList) {
        for (PromotionGoodsProductInfo promotionGoods : activePromotionList) {
            for (SuggestRandomGoodsMapper suggestRandomGoods : suggestGoodsList) {
                if (suggestRandomGoods.eqIdx(promotionGoods.getGoodsIdx())) {

                    suggestRandomGoods.promotionIfExistUpdatePrice(promotionGoods.getPrPrice(),
                        promotionGoods.getPrDiscountPrice(), promotionGoods.getPrDiscountRate());
                }
            }
        }
    }

    public List<SuggestRandomGoodsResponse> toResponse() {
        return suggestGoodsList.stream()
            .map(suggestRandomGoods -> new SuggestRandomGoodsResponse(
                suggestRandomGoods.getIdx(),
                suggestRandomGoods.getGoodsName(),
                suggestRandomGoods.getGoodsOriginPrice(),
                suggestRandomGoods.getGoodsPrice(),
                suggestRandomGoods.getDiscountRate(),
                suggestRandomGoods.getDiscountPrice(),
                suggestRandomGoods.getReviewCnt(),
                suggestRandomGoods.getAverageRating(),
                suggestRandomGoods.getMainImage(),
                suggestRandomGoods.getSaleStatus(),
                suggestRandomGoods.getBrandName(),
                suggestRandomGoods.isNewest()
            )).collect(Collectors.toList());
    }
}
