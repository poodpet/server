package com.pood.server.dto.meta.goods;

import java.util.List;

import com.pood.server.dto.dto_image_2;
import com.pood.server.object.meta.vo_goods_3;

public class dto_goods_6 extends vo_goods_3{

    private String      updatetime;

    private String      recordbirth;
    
    private List<Integer>       pr_code;

    private List<Integer>  coupon;

    private List<Integer>       product;

    private List<dto_image_2>  goods_image;

    private List<dto_image_2>  product_image;

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public List<Integer> getPr_code() {
        return pr_code;
    }

    public void setPr_code(List<Integer> pr_code) {
        this.pr_code = pr_code;
    }

    public List<Integer> getCoupon() {
        return coupon;
    }

    public void setCoupon(List<Integer> coupon) {
        this.coupon = coupon;
    }

    public List<Integer> getProduct() {
        return product;
    }

    public void setProduct(List<Integer> product) {
        this.product = product;
    }

    public List<dto_image_2> getGoods_image() {
        return goods_image;
    }

    public void setGoods_image(List<dto_image_2> goods_image) {
        this.goods_image = goods_image;
    }

    public List<dto_image_2> getProduct_image() {
        return product_image;
    }

    public void setProduct_image(List<dto_image_2> product_image) {
        this.product_image = product_image;
    }
     
    
}
