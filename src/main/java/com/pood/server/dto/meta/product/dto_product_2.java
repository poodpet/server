package com.pood.server.dto.meta.product;

import java.util.List;

import com.pood.server.object.meta.vo_product_2;
import com.pood.server.dto.dto_image_2;

public class dto_product_2 extends vo_product_2{
    
    private Integer idx;

    private List<dto_image_2> image;
    
    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public List<dto_image_2> getImage() {
        return image;
    }

    public void setImage(List<dto_image_2> image) {
        this.image = image;
    }

    

}
