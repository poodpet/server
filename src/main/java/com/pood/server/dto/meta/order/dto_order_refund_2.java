package com.pood.server.dto.meta.order;

import java.util.List;

import com.pood.server.object.meta.vo_order_refund;
import com.pood.server.object.meta.vo_order_refund_delivery;

public class dto_order_refund_2 extends vo_order_refund{

    private Integer                                 idx;

    private String                                  recordbirth;

    private List<vo_order_refund_delivery>          delivery;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public List<vo_order_refund_delivery> getDelivery() {
        return delivery;
    }

    public void setDelivery(List<vo_order_refund_delivery> delivery) {
        this.delivery = delivery;
    }

    
    
    
}
