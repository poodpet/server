package com.pood.server.dto.meta.brand;

import java.util.List;

import com.pood.server.object.meta.vo_brand;
import com.pood.server.object.meta.vo_brand_image;

public class dto_brand_4 extends vo_brand{

    private List<vo_brand_image> brand_image;

    private Integer idx;
 

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public List<vo_brand_image> getBrand_image() {
        return brand_image;
    }

    public void setBrand_image(List<vo_brand_image> brand_image) {
        this.brand_image = brand_image;
    }

        
    
}
