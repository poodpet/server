package com.pood.server.dto.meta.user.pet;

import com.pood.server.controller.response.pet.UserPetInfoResponse;
import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.user.UserPet;
import com.pood.server.entity.user.UserPetImage;
import com.pood.server.exception.NotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.Builder;

public class UserPetResponseCreateGroup {

    private final List<UserPet> userPetList;
    private final List<Pet> petInfoList;
    private final UserPetProblem userPetWorry;
    private final UserPetProblem userPetAllergy;
    private final UserPetProblem userPetGrainSize;

    private final Map<Integer, List<UserPetImage>> userPetImageMap;

    @Builder
    private UserPetResponseCreateGroup(final List<UserPet> userPetList, final List<Pet> petInfoList, final UserPetProblem userPetWorry,
        final UserPetProblem userPetAllergy, final UserPetProblem userPetGrainSize,
        final Map<Integer, List<UserPetImage>> userPetImageMap) {
        this.userPetList = userPetList;
        this.petInfoList = petInfoList;
        this.userPetWorry = userPetWorry;
        this.userPetAllergy = userPetAllergy;
        this.userPetGrainSize = userPetGrainSize;
        this.userPetImageMap = userPetImageMap;
    }

    public List<UserPetInfoResponse> toResponseList() {
        List<UserPetInfoResponse> responseList = new ArrayList<>();
        for (UserPet userPet : userPetList) {
            final Pet petInfo = petInfoList.stream()
                .filter(pet -> pet.getIdx().equals(userPet.getPscId()))
                .findFirst().orElseThrow(() -> new NotFoundException("해당하는 펫 품종 정보를 찾을 수 없습니다."));

            final UserPetInfoResponse userPetInfoResponse = UserPetInfoResponse.of(userPet, petInfo);

            if (userPetImageMap.containsKey(userPet.getIdx())) {
                final List<UserPetImage> userPetImages = userPetImageMap.get(userPet.getIdx());
                userPetInfoResponse.setUserPetImage(userPetImages.get(0));
            }

            userPetWorry.ifExistKeyThenSetProblems(userPet, userPetInfoResponse);
            userPetAllergy.ifExistKeyThenSetProblems(userPet, userPetInfoResponse);
            userPetGrainSize.ifExistKeyThenSetProblems(userPet, userPetInfoResponse);
            responseList.add(userPetInfoResponse);
        }
        return responseList;
    }
}
