package com.pood.server.dto.meta.goods;

import com.pood.server.object.meta.vo_goods_3;

public class dto_goods_11 extends vo_goods_3{

    private String  updatetime;

    private String  recordbirth;

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    
    
}
