package com.pood.server.dto.meta.pet;

import java.util.Objects;
import lombok.Value;

@Value
public class PetProductWord {

    Word age;
    Word petSize;
    Word neutered;

    public PetProductWord(final String ageType, final String age,
        final String sizeType, final String size, final String neuteredType,
        final String neutered) {
        this.age = new Word(ageType, age);
        this.petSize = setProductWordOrNull(sizeType, size);
        this.neutered = setProductWordOrNull(neuteredType, neutered);

    }

    private Word setProductWordOrNull(final String type, final String text) {
        if (Objects.nonNull(type)) {
            return new Word(type, text);
        }
        return null;
    }

    @Value
    private class Word {

        String type;
        String message;
    }

}
