package com.pood.server.dto.meta.user.pet;

import com.pood.server.controller.response.pet.UserPetInfoResponse;
import com.pood.server.entity.user.UserPet;

public interface UserPetProblem {

    void ifExistKeyThenSetProblems(final UserPet userPet, final UserPetInfoResponse userPetInfoResponse);
}
