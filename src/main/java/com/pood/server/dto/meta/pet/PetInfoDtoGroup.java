package com.pood.server.dto.meta.pet;

import com.pood.server.entity.meta.Pet;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
public class PetInfoDtoGroup {

    private final List<PetInfoDto> petInfoDtoList;

    public PetInfoDtoGroup() {
        this.petInfoDtoList = new ArrayList<>();
    }

    public List<PetInfoDto> addPetInfo(final List<Pet> petList) {
        for (final Pet pet : petList) {
            petInfoDtoList.add(new PetInfoDto(pet.getIdx(), pet.getPcId(), pet.getPcKind()));
        }

        return this.petInfoDtoList;
    }

}
