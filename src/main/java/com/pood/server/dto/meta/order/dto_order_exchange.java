package com.pood.server.dto.meta.order;

import com.pood.server.object.meta.vo_order_exchange;

public class dto_order_exchange extends vo_order_exchange{
    
    private Integer idx;

    private String  recordbirth;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    

}
