package com.pood.server.dto.meta.event;

import com.pood.server.entity.meta.EventImage;
import com.pood.server.entity.user.UserBaseImage;
import com.pood.server.entity.user.UserPetImage;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class EventDto {

    private static final int EVENT_PAUSE = 3;

    @Setter
    @Getter
    public static class EventIdx {
        private Integer idx;
    }

    @Setter
    @Getter
    public static class EventDetail {
        private Integer idx;
        private String title;
        private String intro;
        private Integer status;
        private String startDate;
        private String endDate;
        private String imgUrl;
        private Integer imgType;
        private String typeName;
        private String schemeUri;
        private String eventBtn;
        private Integer attendLimit;
        private boolean participationPossible;
        private String details;

        private List<EventImage> eventImageList;

        public boolean isWaiting() {
            LocalDateTime dateTime = getLocalDateTimeConvert(startDate);
            return dateTime.isAfter(LocalDateTime.now());
        }

        public boolean isPause() {
            return status.equals(EVENT_PAUSE);
        }

        public void setEventImageList(List<EventImage> eventImageList) {
            this.eventImageList = eventImageList;
        }

        private LocalDateTime getLocalDateTimeConvert(String dateTime) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            return LocalDateTime.parse(dateTime, formatter);
        }
    }

    @Setter
    @Getter
    public static class EventComment {
        private Integer eventInfoIdx;
        private String comment;
    }

    @Setter
    @Getter
    public static class EventUserComment {
        private Long idx;
        private Integer userInfoIdx;
//        private String userNickname;
        private String comment;
        private LocalDateTime recordbirth;
    }

    @Setter
    @Getter
    public static class EventUserCommentDto {
        private Long idx;
        private Integer userInfoIdx;
        private String userNickname;
        private String comment;
        private String recordbirth;
        private String url;

        public EventUserCommentDto(Long idx, Integer userInfoIdx, String userNickname, String comment, LocalDateTime recordbirth, String url) {
            this.idx = idx;
            this.userInfoIdx = userInfoIdx;
            this.userNickname = userNickname;
            this.comment = comment;
            this.recordbirth = recordbirth.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            this.url =url;
        }
    }

    @Setter
    @Getter
    public static class EventUserPhoto {
        private Long idx;
        private Integer userInfoIdx;
        //        private String userNickname;
        private String url;
        private LocalDateTime recordbirth;
    }

    @Setter
    @Getter
    public static class EventUserPhotoDto {
        private Long idx;
        private Integer userInfoIdx;
        private String userNickname;
        private String url;
        private String recordbirth;

        public EventUserPhotoDto(Long idx, Integer userInfoIdx, String userNickname, String url, LocalDateTime recordbirth) {
            this.idx = idx;
            this.userInfoIdx = userInfoIdx;
            this.userNickname = userNickname;
            this.url = url;
            this.recordbirth =  recordbirth.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        }
    }

    @Setter
    @Getter
    public static class EventPhotoUserDetail {
        private List<UserPetSimpleData> userPetSimpleDataList;
        private List<EventPhotoSimpleData> photoSimpleDataList;
        private UserInfoSimpleData userInfoSimpleData;

        public EventPhotoUserDetail(List<UserPetSimpleData> userPetSimpleDataList, List<EventPhotoSimpleData> photoSimpleDataList , UserInfoSimpleData userInfoSimpleData) {
            this.userPetSimpleDataList = userPetSimpleDataList;
            this.photoSimpleDataList = photoSimpleDataList;
            this.userInfoSimpleData = userInfoSimpleData;
        }
    }

    @Setter
    @Getter
    public static class EventPhotoSimpleData {
        private Long idx;
        private String url;

        public EventPhotoSimpleData(Long idx, String url) {
            this.idx = idx;
            this.url = url;
        }
    }

    @Setter
    @Getter
    public static class UserInfoSimpleData {
        private Integer idx;
        private String nickName;
        private UserBaseImage userBaseImage;

        public UserInfoSimpleData(Integer idx, String nickName, UserBaseImage userBaseImage) {
            this.idx = idx;
            this.nickName = nickName;
            this.userBaseImage = userBaseImage;
        }
    }

    @Setter
    @Getter
    public static class UserPetSimpleData {
        private Integer idx;
        private UserPetImage userPetImage;
        private LocalDate petBirth;
        private String pcSize;
        private String petName;
        private String pcKind;
        private Integer pcCtIdx;
        public UserPetSimpleData(Integer idx, UserPetImage userPetImage, LocalDate petBirth, String pcSize, String petName, String pcKind, Integer pcCtIdx) {
            this.idx = idx;
            this.userPetImage = userPetImage;
            this.petBirth = petBirth;
            this.pcSize = pcSize;
            this.petName = petName;
            this.pcKind = pcKind;
            this.pcCtIdx = pcCtIdx;
        }
    }

    @Setter
    @Getter
    public static class MyEventList {
        private Integer idx;
        private String title;
        private String intro;
        private String imgUrl;
        private String innerTitle;
        private String type;
        private LocalDateTime startDate;
        private LocalDateTime endDate;
    }

    @Setter
    @Getter
    public static class MyJoinEventDetail {
        private Integer idx;
        private String title;
        private String intro;
        private String innerTitle;
        private String ranking;
        private LocalDateTime startDate;
        private LocalDateTime endDate;
        private EventParticipationDetail eventParticipation;
    }

    @Setter
    @Getter
    public static class EventParticipationDetail {
        private Long idx;
        private Integer userInfoIdx;
        private LocalDateTime recordbirth;
    }

    @Setter
    @Getter
    public static class MyCommentEventDetail {
        private Integer idx;
        private String title;
        private String intro;
        private String innerTitle;
        private String ranking;
        private LocalDateTime startDate;
        private LocalDateTime endDate;
        private List<CommentEventDetail> commentEventDetails;

        public void setRanking(String ranking) {
            this.ranking = ranking;
        }

        public void setCommentEventDetails(List<CommentEventDetail> commentEventDetails) {
            this.commentEventDetails = commentEventDetails;
        }
    }

    @Setter
    @Getter
    public static class CommentEventDetail {

        private Long idx;
        private Integer userInfoIdx;
        private String comment;
        private LocalDateTime recordbirth;

        public CommentEventDetail(Long idx, Integer userInfoIdx, String comment,LocalDateTime recordbirth) {
            this.idx = idx;
            this.userInfoIdx = userInfoIdx;
            this.comment = comment;
            this.recordbirth = recordbirth;
        }
    }


    @Setter
    @Getter
    public static class MyPhotoEventDetail {
        private Integer idx;
        private String title;
        private String intro;
        private String innerTitle;
        private String ranking;
        private LocalDateTime startDate;
        private LocalDateTime endDate;
        private List<PhotoEventDetail> photoEventDetails;

        public void setRanking(String ranking) {
            this.ranking = ranking;
        }

        public void setPhotoEventDetails(List<PhotoEventDetail> photoEventDetails) {
            this.photoEventDetails = photoEventDetails;
        }
    }
    @Setter
    @Getter
    public static class PhotoEventDetail {
        private Long idx;
        private Integer userInfoIdx;
        private String url;
        private LocalDateTime recordbirth;

        public PhotoEventDetail(Long idx, Integer userInfoIdx, String url, LocalDateTime recordbirth) {
            this.idx = idx;
            this.userInfoIdx = userInfoIdx;
            this.url = url;
            this.recordbirth = recordbirth;
        }
    }
}
