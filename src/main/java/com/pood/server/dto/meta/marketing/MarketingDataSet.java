package com.pood.server.dto.meta.marketing;

import com.pood.server.util.MarketingType;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MarketingDataSet implements Comparable<MarketingDataSet> {

    private Integer marketingIdx;

    private String url;

    private int priority;

    private MarketingType type;

    @Override
    public int compareTo(final MarketingDataSet o) {
        if (this.priority == o.priority) {
            return o.marketingIdx - this.marketingIdx;
        }
        return Integer.compare(this.priority, o.priority);
    }
}
