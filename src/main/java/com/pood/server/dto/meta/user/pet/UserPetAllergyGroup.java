package com.pood.server.dto.meta.user.pet;

import com.pood.server.dto.meta.user.util.EntityObjectToList;
import com.pood.server.entity.meta.AllergyData;
import com.pood.server.entity.user.UserPetAllergy;
import java.util.ArrayList;
import java.util.List;

public class UserPetAllergyGroup implements EntityObjectToList<UserPetAllergy, AllergyData> {

    private final List<UserPetAllergy> list = new ArrayList<>();

    @Override
    public List<UserPetAllergy> saveAllObjects(final List<AllergyData> inputEntityList,
        final int userPetIdx) {
        for (AllergyData allergyData : inputEntityList) {
            list.add(
                UserPetAllergy.toEntity(
                    userPetIdx,
                    allergyData.getIdx(),
                    allergyData.getName(),
                    allergyData.getType()
                )
            );
        }
        return list;
    }
}
