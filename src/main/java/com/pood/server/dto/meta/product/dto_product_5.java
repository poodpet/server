package com.pood.server.dto.meta.product;

import com.pood.server.object.meta.vo_product_2;
import com.pood.server.object.meta.vo_snack;

import java.util.List;

import com.pood.server.dto.dto_image_2;
import com.pood.server.dto.meta.dto_feed;
import com.pood.server.dto.meta.pet.dto_pet_doctor_desc;

public class dto_product_5 extends vo_product_2{
    
    private Integer idx;

    private Integer product_qty;

    private String  updatetime;

    private String  recordbirth;

    private Integer product_price;

    private List<dto_feed> product_feed;

    private dto_pet_doctor_desc pet_doctor_desc;

    private List<dto_image_2> main_image;

    private List<dto_image_2> product_image;

    private vo_snack feedingDetail;
 
    public List<dto_image_2> getMain_image() {
        return main_image;
    }

    public void setMain_image(List<dto_image_2> main_image) {
        this.main_image = main_image;
    }

    public List<dto_image_2> getProduct_image() {
        return product_image;
    }

    public void setProduct_image(List<dto_image_2> product_image) {
        this.product_image = product_image;
    }

    public List<dto_feed> getProduct_feed() {
        return product_feed;
    }

    public void setProduct_feed(List<dto_feed> product_feed) {
        this.product_feed = product_feed;
    }

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public dto_pet_doctor_desc getPet_doctor_desc() {
        return pet_doctor_desc;
    }

    public void setPet_doctor_desc(dto_pet_doctor_desc pet_doctor_desc) {
        this.pet_doctor_desc = pet_doctor_desc;
    }

    public vo_snack getFeedingDetail() {
        return feedingDetail;
    }

    public void setFeedingDetail(vo_snack feedingDetail) {
        this.feedingDetail = feedingDetail;
    }

    public Integer getProduct_qty() {
        return product_qty;
    }

    public void setProduct_qty(Integer product_qty) {
        this.product_qty = product_qty;
    }

    public Integer getProduct_price() {
        return product_price;
    }

    public void setProduct_price(Integer product_price) {
        this.product_price = product_price;
    }
}
