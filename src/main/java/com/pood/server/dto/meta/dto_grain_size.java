package com.pood.server.dto.meta;

import com.pood.server.object.meta.vo_grain_size;

public class dto_grain_size extends vo_grain_size{

    private Integer idx;
 
    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }
 
}
