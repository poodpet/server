package com.pood.server.dto.meta.goods;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pood.server.entity.meta.GoodsCt;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class GoodsFilterCtForm {

    Long idx;
    String url;
    String name;
    String type;
    String fieldKey;
    String fieldValue;
    Integer priority;

    @JsonIgnore
    GoodsCt goodsCt;

    public boolean idGoodsCtIdxEq(long idx) {
        return this.goodsCt.isIdxEq(idx);
    }
}
