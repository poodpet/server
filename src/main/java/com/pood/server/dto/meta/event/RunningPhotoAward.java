package com.pood.server.dto.meta.event;

import com.querydsl.core.annotations.QueryProjection;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class RunningPhotoAward {

    private Integer idx;

    @QueryProjection
    public RunningPhotoAward(final Integer idx) {
        this.idx = idx;
    }
}
