package com.pood.server.dto.meta;

import com.pood.server.entity.meta.NoticeImage;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoticeDetailDto {
   private List<NoticeImage> noticeImages;
}
