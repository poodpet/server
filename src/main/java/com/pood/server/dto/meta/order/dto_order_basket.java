package com.pood.server.dto.meta.order;

public class dto_order_basket {

    private Integer goods_idx;

    private Integer basket_idx;

    private String  recordbirth;

    private Integer quantity;

    private Integer goods_price;

    private String  order_number;

    private Integer seller_idx;

    private Integer pr_code_idx;

    private Integer coupon_idx;

    private Integer status;

    public Integer getGoods_idx() {
        return goods_idx;
    }

    public void setGoods_idx(Integer goods_idx) {
        this.goods_idx = goods_idx;
    }

    public Integer getBasket_idx() {
        return basket_idx;
    }

    public void setBasket_idx(Integer basket_idx) {
        this.basket_idx = basket_idx;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getGoods_price() {
        return goods_price;
    }

    public void setGoods_price(Integer goods_price) {
        this.goods_price = goods_price;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public Integer getSeller_idx() {
        return seller_idx;
    }

    public void setSeller_idx(Integer seller_idx) {
        this.seller_idx = seller_idx;
    }

    public Integer getPr_code_idx() {
        return pr_code_idx;
    }

    public void setPr_code_idx(Integer pr_code_idx) {
        this.pr_code_idx = pr_code_idx;
    }

    public Integer getCoupon_idx() {
        return coupon_idx;
    }

    public void setCoupon_idx(Integer coupon_idx) {
        this.coupon_idx = coupon_idx;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "dto_order_basket [basket_idx=" + basket_idx + ", coupon_idx=" + coupon_idx + ", goods_idx=" + goods_idx
                + ", goods_price=" + goods_price + ", order_number=" + order_number + ", pr_code_idx=" + pr_code_idx
                + ", quantity=" + quantity + ", recordbirth=" + recordbirth + ", seller_idx=" + seller_idx + ", status="
                + status + "]";
    }

    
}
