package com.pood.server.dto.meta;

public class dto_main_image {

    private String  recordbirth;

    private Integer main_idx;

    private Integer width;

    private Integer priority;

    private Integer type;

    private String  updatetime;

    private Integer idx;

    private String  url;

    private Integer height;

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public Integer getMain_idx() {
        return main_idx;
    }

    public void setMain_idx(Integer main_idx) {
        this.main_idx = main_idx;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "dto_main_image [height=" + height + ", idx=" + idx + ", main_idx=" + main_idx + ", priority=" + priority
                + ", recordbirth=" + recordbirth + ", type=" + type + ", updatetime=" + updatetime + ", url=" + url
                + ", width=" + width + "]";
    }

    
}
