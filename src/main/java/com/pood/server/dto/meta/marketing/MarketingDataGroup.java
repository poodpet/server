package com.pood.server.dto.meta.marketing;

import java.util.Collections;
import java.util.List;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MarketingDataGroup {

    private final List<MarketingDataSet> marketingDataList;

    public void addAll(List<MarketingDataSet> inputDataList) {
        marketingDataList.addAll(inputDataList);
    }

    public List<MarketingDataSet> sortedList() {
        Collections.sort(marketingDataList);
        return marketingDataList;
    }
}
