package com.pood.server.dto.meta.brand;

import java.util.List;

import com.pood.server.object.meta.vo_brand;
import com.pood.server.object.meta.vo_brand_image;

public class dto_brand_3 extends vo_brand{
     
    private Integer idx;

    private String  updatetime;

    private String  recordbirth;

    private List<vo_brand_image> image;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public List<vo_brand_image> getImage() {
        return image;
    }

    public void setImage(List<vo_brand_image> image) {
        this.image = image;
    }
    
}
