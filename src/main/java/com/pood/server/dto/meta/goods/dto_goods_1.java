package com.pood.server.dto.meta.goods;

import java.util.List;

import com.pood.server.object.meta.vo_goods_3;

public class dto_goods_1 extends vo_goods_3{

    private List<Integer> product;

    public List<Integer> getProduct() {
        return product;
    }

    public void setProduct(List<Integer> product) {
        this.product = product;
    }

}
