package com.pood.server.dto.meta.pet;

import com.pood.server.controller.response.pet.GrainSizeResponse;
import com.pood.server.entity.meta.GrainSize;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class GrainSizeGroup {

    private final List<GrainSize> grainSizeList;

    public GrainSizeGroup(final List<GrainSize> grainSizeList) {
        this.grainSizeList = Collections.unmodifiableList(grainSizeList);
    }

    public List<GrainSizeResponse> toResponse() {
        return grainSizeList.stream()
            .map(grainSize -> GrainSizeResponse.of(grainSize.getIdx(), grainSize.getTitle(), grainSize.getUrl()))
            .collect(Collectors.toList());
    }

}
