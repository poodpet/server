package com.pood.server.dto.meta.goods;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class UserBaseImageDto {

    @Setter
    @Getter
    public static class IdxDto {

        private Long idx;
    }

    @Setter
    @Getter
    public static class UserBaseImageList {

        private Long idx;
        private String url;

        public UserBaseImageList(Long idx, String url) {
            this.idx = idx;
            this.url = url;
        }
    }
}
