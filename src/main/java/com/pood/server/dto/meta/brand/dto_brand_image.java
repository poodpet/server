package com.pood.server.dto.meta.brand;
 
public class dto_brand_image{

    private Integer idx;

    private Integer brand_idx;

    private String  recordbirth;

    private Integer visible;

    private Integer type;

    private Integer priority;

    private String  updatetime;

    private String  url;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public Integer getBrand_idx() {
        return brand_idx;
    }

    public void setBrand_idx(Integer brand_idx) {
        this.brand_idx = brand_idx;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "dto_brand_image [brand_idx=" + brand_idx + ", idx=" + idx + ", priority=" + priority + ", recordbirth="
                + recordbirth + ", type=" + type + ", updatetime=" + updatetime + ", url=" + url + ", visible="
                + visible + "]";
    }
    
    
    
}
