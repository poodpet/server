package com.pood.server.dto.meta.event;

public class dto_event_info{

    private Integer idx;

    private String  updatetime;

    private String  recordbirth;

    private Integer coupon_idx;

    private Integer event_type_idx;

    private String  title;

    private String  intro;

    private Integer status;

    private String  startdate;

    private String  end_date;

    private Integer pc_id;

    private String  scheme_uri;

    private Integer brand_idx;

    private String  bg_color;

    private String  event_btn;

    private Integer priority;

    public Integer getCoupon_idx() {
        return coupon_idx;
    }

    public void setCoupon_idx(Integer coupon_idx) {
        this.coupon_idx = coupon_idx;
    }

    public Integer getEvent_type_idx() {
        return event_type_idx;
    }

    public void setEvent_type_idx(Integer event_type_idx) {
        this.event_type_idx = event_type_idx;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    
    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public Integer getPc_id() {
        return pc_id;
    }

    public void setPc_id(Integer pc_id) {
        this.pc_id = pc_id;
    }

    public String getScheme_uri() {
        return scheme_uri;
    }

    public void setScheme_uri(String scheme_uri) {
        this.scheme_uri = scheme_uri;
    }

    public Integer getBrand_idx() {
        return brand_idx;
    }

    public void setBrand_idx(Integer brand_idx) {
        this.brand_idx = brand_idx;
    }

    public String getBg_color() {
        return bg_color;
    }

    public void setBg_color(String bg_color) {
        this.bg_color = bg_color;
    }

    public String getEvent_btn() {
        return event_btn;
    }

    public void setEvent_btn(String event_btn) {
        this.event_btn = event_btn;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    @Override
    public String toString() {
        return "dto_event_info [bg_color=" + bg_color + ", brand_idx=" + brand_idx + ", coupon_idx=" + coupon_idx
                + ", end_date=" + end_date + ", event_btn=" + event_btn + ", event_type_idx=" + event_type_idx
                + ", idx=" + idx + ", intro=" + intro + ", pc_id=" + pc_id + ", priority=" + priority + ", recordbirth="
                + recordbirth + ", scheme_uri=" + scheme_uri + ", startdate=" + startdate + ", status=" + status
                + ", title=" + title + ", updatetime=" + updatetime + "]";
    }
    
    
}
