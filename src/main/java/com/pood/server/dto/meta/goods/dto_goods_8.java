package com.pood.server.dto.meta.goods;

import java.util.List;

import com.pood.server.dto.dto_image_2;
import com.pood.server.dto.meta.product.dto_product_1;
import com.pood.server.object.meta.vo_goods_3;

public class dto_goods_8 extends vo_goods_3{
 
    private String  updatetime;

    private String  recordbirth;
    
    private List<dto_image_2>   goods_image;

    private List<dto_image_2>   product_image;

    private List<dto_product_1> product;

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public List<dto_image_2> getGoods_image() {
        return goods_image;
    }

    public void setGoods_image(List<dto_image_2> goods_image) {
        this.goods_image = goods_image;
    }

    public List<dto_image_2> getProduct_image() {
        return product_image;
    }

    public void setProduct_image(List<dto_image_2> product_image) {
        this.product_image = product_image;
    }

    public List<dto_product_1> getProduct() {
        return product;
    }

    public void setProduct(List<dto_product_1> product) {
        this.product = product;
    }

    
     
}
