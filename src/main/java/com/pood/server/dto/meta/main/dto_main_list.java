package com.pood.server.dto.meta.main;

public class dto_main_list {

    private Integer idx;

    private Integer type_idx;

    private String  title;

    private String  startdate;

    private String  end_date;

    private Integer status;

    private Integer type_priority;

    private Integer priority;

    private Integer item_cnt;

    private Integer pc_id;

    private String  scheme_uri;

    private String  updatetime;

    private String  recordbirth;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public Integer getType_idx() {
        return type_idx;
    }

    public void setType_idx(Integer type_idx) {
        this.type_idx = type_idx;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getType_priority() {
        return type_priority;
    }

    public void setType_priority(Integer type_priority) {
        this.type_priority = type_priority;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getItem_cnt() {
        return item_cnt;
    }

    public void setItem_cnt(Integer item_cnt) {
        this.item_cnt = item_cnt;
    }

    public Integer getPc_id() {
        return pc_id;
    }

    public void setPc_id(Integer pc_id) {
        this.pc_id = pc_id;
    }

    public String getScheme_uri() {
        return scheme_uri;
    }

    public void setScheme_uri(String scheme_uri) {
        this.scheme_uri = scheme_uri;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    @Override
    public String toString() {
        return "dto_main_list [end_date=" + end_date + ", idx=" + idx + ", item_cnt=" + item_cnt + ", pc_id=" + pc_id
                + ", priority=" + priority + ", recordbirth=" + recordbirth + ", scheme_uri=" + scheme_uri
                + ", startdate=" + startdate + ", status=" + status + ", title=" + title + ", type_idx=" + type_idx
                + ", type_priority=" + type_priority + ", updatetime=" + updatetime + "]";
    }

    
    
}
