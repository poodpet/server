package com.pood.server.dto.meta;

import com.pood.server.dto.meta.brand.dto_brand_4;

public class dto_main_goods_type {
    
    private Integer idx;

    private String  name;

    private dto_brand_4 brand;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public dto_brand_4 getBrand() {
        return brand;
    }

    public void setBrand(dto_brand_4 brand) {
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "dto_main_goods_type [brand=" + brand + ", idx=" + idx + ", name=" + name + "]";
    }
 
    
    
}
