package com.pood.server.dto.meta.pet;

import com.pood.server.entity.Triple;
import com.pood.server.entity.TripleAndType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
public class PetDto {



    @Setter
    @Getter
    public static class userPetSolutionDto {
        List<TripleAndType> petDiseaseList;
        Triple petLifeCycle;

        public userPetSolutionDto(List<TripleAndType> petDiseaseList, Triple petLifeCycle) {
            this.petDiseaseList = petDiseaseList;
            this.petLifeCycle = petLifeCycle;
        }
    }


}
