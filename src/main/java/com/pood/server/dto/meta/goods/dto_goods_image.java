package com.pood.server.dto.meta.goods;

public class dto_goods_image {

    private Integer goods_idx;

    private String  recordbirth;

    private Integer visible;

    private Integer idx;

    private Integer type;

    private String  updatetime;

    private Integer priority;

    private String  url;

    public Integer getGoods_idx() {
        return goods_idx;
    }

    public void setGoods_idx(Integer goods_idx) {
        this.goods_idx = goods_idx;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "dto_goods_image [goods_idx=" + goods_idx + ", idx=" + idx + ", priority=" + priority + ", recordbirth="
                + recordbirth + ", type=" + type + ", updatetime=" + updatetime + ", url=" + url + ", visible="
                + visible + "]";
    }

    

}
