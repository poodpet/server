package com.pood.server.dto.meta.user.pet;

import com.pood.server.entity.user.UserPet;
import java.util.List;
import java.util.stream.Collectors;

public class UserPetGroup {

    private final List<UserPet> userPetList;

    public UserPetGroup(final List<UserPet> userPetList) {
        this.userPetList = userPetList;
    }

    public List<Integer> petInfoIdxList() {
        return userPetList.stream()
            .map(UserPet::getPscId)
            .collect(Collectors.toList());
    }

    public List<Integer> userPetIdxList() {
        return userPetList.stream()
            .map(UserPet::getIdx)
            .collect(Collectors.toList());
    }

    public List<UserPet> getUserPetList() {
        return userPetList;
    }

}
