package com.pood.server.dto.meta.faq;

import com.pood.server.controller.response.faq.FaqCategoryResponse;
import com.pood.server.entity.meta.FaqCategory;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;

@Getter
public class FaqCategoryResponseGroup {

    private final List<FaqCategoryResponse> faqCategoryResponseList;

    public FaqCategoryResponseGroup(final List<FaqCategory> faqCategories) {

        this.faqCategoryResponseList = faqCategories
            .stream()
            .map(faqCategory ->
                new FaqCategoryResponse(faqCategory.getIdx(), faqCategory.getName()))
            .collect(Collectors.toList());
    }

}
