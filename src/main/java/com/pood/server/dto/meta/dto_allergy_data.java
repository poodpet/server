package com.pood.server.dto.meta;

import com.pood.server.object.meta.vo_allergy_data;

public class dto_allergy_data extends vo_allergy_data{

    private Integer idx;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }
    
}
