package com.pood.server.dto.meta.user.pet;

import com.pood.server.controller.response.pet.UserPetInfoResponse;
import com.pood.server.dto.user.pet.UserPetGrainSizeDto;
import com.pood.server.entity.user.UserPet;
import java.util.List;
import java.util.Map;

public class UserPetGrainSizeMap implements UserPetProblem {

    final Map<Integer, List<UserPetGrainSizeDto>> userPetGrainSizeDtoMap;

    public UserPetGrainSizeMap(final Map<Integer, List<UserPetGrainSizeDto>> userPetGrainSizeDtoMap) {
        this.userPetGrainSizeDtoMap = userPetGrainSizeDtoMap;
    }

    @Override
    public void ifExistKeyThenSetProblems(final UserPet userPet, final UserPetInfoResponse userPetInfoResponse) {
        if (userPetGrainSizeDtoMap.containsKey(userPet.getIdx())) {
            userPetInfoResponse.setGrainSizeList(userPetGrainSizeDtoMap.get(userPet.getIdx()));
        }
    }
}
