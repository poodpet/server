package com.pood.server.dto.meta.event;

import com.querydsl.core.annotations.QueryProjection;
import lombok.Getter;

@Getter
public class EndPhotoImgUrl {

    private final String url;

    @QueryProjection
    public EndPhotoImgUrl(final String url) {
        this.url = url;
    }
}
