package com.pood.server.dto.meta.worry;

import com.querydsl.core.annotations.QueryProjection;
import lombok.Value;

@Value
public class PetWorryReadDto {

    Long id;
    String name;
    String dogUrl;
    String catUrl;
    @QueryProjection
    public PetWorryReadDto(final Long id, final String name, final String dogUrl, final String catUrl) {
        this.id = id;
        this.name = name;
        this.dogUrl = dogUrl;
        this.catUrl = catUrl;
    }
}
