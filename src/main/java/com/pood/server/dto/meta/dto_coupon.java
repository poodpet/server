package com.pood.server.dto.meta;

public class dto_coupon {

    private Integer idx;

    private String  description;

    private String  name;

    private Integer type;

    private Integer goods_type;

    private Integer apply_coupon_type;

    private Integer publish_type;

    private Integer publish_type_idx;

    private Integer over_type;

    private Integer discount_rate;

    private Integer max_price;

    private Integer limit_price;

    private Integer payment_type;

    private String  tag;

    private Integer issued_count;

    private Integer issued_type;

    private String  available_time;

    private String  updatetime;

    private String  recordbirth;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getGoods_type() {
        return goods_type;
    }

    public void setGoods_type(Integer goods_type) {
        this.goods_type = goods_type;
    }

    public Integer getApply_coupon_type() {
        return apply_coupon_type;
    }

    public void setApply_coupon_type(Integer apply_coupon_type) {
        this.apply_coupon_type = apply_coupon_type;
    }

    public Integer getPublish_type() {
        return publish_type;
    }

    public void setPublish_type(Integer publish_type) {
        this.publish_type = publish_type;
    }

    public Integer getPublish_type_idx() {
        return publish_type_idx;
    }

    public void setPublish_type_idx(Integer publish_type_idx) {
        this.publish_type_idx = publish_type_idx;
    }

    public Integer getOver_type() {
        return over_type;
    }

    public void setOver_type(Integer over_type) {
        this.over_type = over_type;
    }

    public Integer getDiscount_rate() {
        return discount_rate;
    }

    public void setDiscount_rate(Integer discount_rate) {
        this.discount_rate = discount_rate;
    }

    public Integer getMax_price() {
        return max_price;
    }

    public void setMax_price(Integer max_price) {
        this.max_price = max_price;
    }

    public Integer getLimit_price() {
        return limit_price;
    }

    public void setLimit_price(Integer limit_price) {
        this.limit_price = limit_price;
    }

    public Integer getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(Integer payment_type) {
        this.payment_type = payment_type;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Integer getIssued_count() {
        return issued_count;
    }

    public void setIssued_count(Integer issued_count) {
        this.issued_count = issued_count;
    }

    public Integer getIssued_type() {
        return issued_type;
    }

    public void setIssued_type(Integer issued_type) {
        this.issued_type = issued_type;
    }

    public String getAvailable_time() {
        return available_time;
    }

    public void setAvailable_time(String available_time) {
        this.available_time = available_time;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    
}
