package com.pood.server.dto.meta;

import com.pood.server.object.meta.vo_coupon_2;
import java.util.List;


public class dto_coupon_2 extends vo_coupon_2 {

    private Integer idx;

    private String updatetime;

    private String recordbirth;


    private List<Integer> goods_idx;

    private List<Integer> brand_idx;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public List<Integer> getGoods_idx() {
        return goods_idx;
    }

    public void setGoods_idx(List<Integer> goods_idx) {
        this.goods_idx = goods_idx;
    }

    public List<Integer> getBrand_idx() {
        return brand_idx;
    }

    public void setBrand_idx(List<Integer> brand_idx) {
        this.brand_idx = brand_idx;
    }

}
