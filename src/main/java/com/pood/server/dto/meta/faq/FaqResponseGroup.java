package com.pood.server.dto.meta.faq;

import com.pood.server.controller.response.faq.FaqResponse;
import com.pood.server.entity.meta.Faq;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;

@Getter
public class FaqResponseGroup {

    private final List<FaqResponse> faqResponseList;

    public FaqResponseGroup(final List<Faq> faqList) {

        this.faqResponseList = faqList
            .stream()
            .map(faqCategory ->
                new FaqResponse(faqCategory.getIdx(),
                    faqCategory.getTitle(),
                    faqCategory.getContents()))
            .collect(Collectors.toList());
    }

}
