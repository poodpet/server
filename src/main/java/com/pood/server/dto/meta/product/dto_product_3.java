package com.pood.server.dto.meta.product;
 
import java.util.List;

import com.pood.server.dto.dto_image_2;
import com.pood.server.dto.meta.dto_feed;

public class dto_product_3 extends dto_product{
 
    private List<dto_feed> product_feed;

    private List<dto_image_2> main_image;

    private List<dto_image_2> product_image;
 
    public List<dto_image_2> getMain_image() {
        return main_image;
    }

    public void setMain_image(List<dto_image_2> main_image) {
        this.main_image = main_image;
    }

    public List<dto_image_2> getProduct_image() {
        return product_image;
    }

    public void setProduct_image(List<dto_image_2> product_image) {
        this.product_image = product_image;
    }

    public List<dto_feed> getProduct_feed() {
        return product_feed;
    }

    public void setProduct_feed(List<dto_feed> product_feed) {
        this.product_feed = product_feed;
    }
  
}
