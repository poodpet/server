package com.pood.server.dto.meta.product;

import java.util.List;

import com.pood.server.object.meta.vo_product_2;
import com.pood.server.dto.dto_image_2;

public class dto_product extends vo_product_2{

    private Integer idx;

    private String  updatetime;

    private String  recordbirth;

    private List<dto_image_2> image;


    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public List<dto_image_2> getImage() {
        return image;
    }

    public void setImage(List<dto_image_2> image) {
        this.image = image;
    }
    
    
}
