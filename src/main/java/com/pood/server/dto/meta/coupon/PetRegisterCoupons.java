package com.pood.server.dto.meta.coupon;

import com.pood.server.entity.meta.Coupon;
import com.pood.server.util.strategy.NumberStrategy;
import java.util.List;
import java.util.stream.Collectors;

public class PetRegisterCoupons {

    private final List<Coupon> petRegisterList;
    private final int percent;

    public PetRegisterCoupons(final List<Coupon> petRegisterList, final NumberStrategy numberStrategy) {
        couponListValid(petRegisterList);
        this.petRegisterList = petRegisterList;
        this.percent = numberStrategy.makeNumber();
    }

    private void couponListValid(final List<Coupon> petRegisterList) {
        if (petRegisterList.size() < 5) {
            throw new IllegalStateException();
        }
    }

    public Coupon findPercent() {

        /*
            1 ~ 10퍼센트면 1000원
            11 ~ 50퍼센트면 3000원
            51 ~ 80퍼센트면 5000원
            81 ~ 90퍼센트면 7000원
            91 ~ 100퍼센트면 10000원
        */

        if (percent <= 10) {
            return petRegisterList.get(0);
        }

        if (percent <= 50) {
            return petRegisterList.get(1);
        }

        if (percent <= 80) {
            return petRegisterList.get(2);
        }

        if (percent <= 90) {
            return petRegisterList.get(3);
        }

        return petRegisterList.get(4);
    }

    public List<Integer> findIdxList() {
        return petRegisterList.stream()
            .map(Coupon::getIdx)
            .collect(Collectors.toList());
    }
}
