package com.pood.server.dto.meta.user.pet;

import com.pood.server.controller.response.pet.UserPetInfoResponse;
import com.pood.server.dto.user.pet.UserPetAllergyDto;
import com.pood.server.entity.user.UserPet;
import java.util.List;
import java.util.Map;

public class UserPetAllergyMap implements UserPetProblem {

    private final Map<Integer, List<UserPetAllergyDto>> userPetAllergyDtoMap;

    public UserPetAllergyMap(final Map<Integer, List<UserPetAllergyDto>> userPetAllergyDtoMap) {
        this.userPetAllergyDtoMap = userPetAllergyDtoMap;
    }

    @Override
    public void ifExistKeyThenSetProblems(final UserPet userPet, final UserPetInfoResponse userPetInfoResponse) {
        if (userPetAllergyDtoMap.containsKey(userPet.getIdx())) {
            userPetInfoResponse.setAllergyDtoList(userPetAllergyDtoMap.get(userPet.getIdx()));
        }
    }
}
