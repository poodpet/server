package com.pood.server.dto.meta.goods;

import com.pood.server.controller.request.search.LatestGoodsSearchRequest;
import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.dto.meta.goods.PromotionDto.PromotionGoodsProductInfo;
import com.pood.server.entity.GoodsDiscountRateSort;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;

@Getter
public class SortedGoodsGroup {

    private final List<SortedGoodsList> sortedGoodsList;

    public SortedGoodsGroup(final List<SortedGoodsList> sortedGoodsList) {
        this.sortedGoodsList = sortedGoodsList;
    }

    public List<SortedGoodsList> getSortedListByLatestGoods(
        final LatestGoodsSearchRequest latestGoodsSearch) {
        return this.sortedGoodsList.stream()
            .sorted(getSortedGoodsListComparator(latestGoodsSearch))
            .collect(Collectors.toList());
    }

    private Comparator<SortedGoodsList> getSortedGoodsListComparator(
        final LatestGoodsSearchRequest latestGoodsSearch) {
        return (goodsInfo1, goodsInfo2) -> {
            if (latestGoodsSearch.getPriorityByIdx(goodsInfo1.getIdx())
                > latestGoodsSearch.getPriorityByIdx(goodsInfo2.getIdx())) {
                return 1;
            } else {
                return -1;
            }
        };
    }

    public List<Integer> getIdxList() {
        return sortedGoodsList.stream().map(SortedGoodsList::getIdx).collect(Collectors.toList());
    }


    public SortedGoodsGroup setPromotionData(final List<PromotionGoodsProductInfo> promotionList) {
        List<SortedGoodsList> sortedGoodsLists = Collections.unmodifiableList(sortedGoodsList);
        promotionList.forEach(promotionInfo ->
            sortedGoodsLists.forEach(goodsInfo -> {
                if (goodsInfo.eqIdx(promotionInfo.getGoodsIdx())) {
                    goodsInfo.promotionIfExistUpdatePrice(promotionInfo);
                }
            })
        );
        return new SortedGoodsGroup(sortedGoodsLists);
    }

    public SortedGoodsGroup limitSize(final int size) {
        return new SortedGoodsGroup(
            sortedGoodsList.stream().limit(size).collect(Collectors.toList()));
    }

    public List<SortedGoodsList> getSortedGoodsListOrderByPrDiscountRateDesc() {
        return sortedGoodsList.stream()
            .sorted(new GoodsDiscountRateSort().reversed())
            .collect(Collectors.toList());
    }
}
