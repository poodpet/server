package com.pood.server.dto.meta;

import com.pood.server.object.meta.vo_ai_recommend_diagnosis;

public class dto_ai_recommend_diagnosis extends vo_ai_recommend_diagnosis{
    
    private Integer idx;

    private String  updatetime;

    private String  recordbirth;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }
    
    
}
