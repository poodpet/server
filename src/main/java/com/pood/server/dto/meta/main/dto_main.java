package com.pood.server.dto.meta.main;

import java.util.HashMap;
import java.util.List;

import com.pood.server.dto.meta.dto_main_image;
import com.pood.server.object.meta.main_2;

public class dto_main extends main_2{
    
    private List<dto_main_image> main_image;

    private HashMap<String, Object> type;

    private List<dto_main_items> items;

    private Integer idx;
 
    public List<dto_main_image> getMain_image() {
        return main_image;
    }

    public void setMain_image(List<dto_main_image> main_image) {
        this.main_image = main_image;
    }

    public HashMap<String, Object> getType() {
        return type;
    }

    public void setType(HashMap<String, Object> type) {
        this.type = type;
    }
     
    public List<dto_main_items> getItems() {
        return items;
    }

    public void setItems(List<dto_main_items> items) {
        this.items = items;
    }

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    @Override
    public String toString() {
        return "dto_main [idx=" + idx + ", items=" + items + ", main_image=" + main_image + ", type=" + type + "]";
    }

    

}
