package com.pood.server.dto.meta.event;

public class dto_event_image_2 {

    private Integer idx;

    private Integer event_idx;

    private String  url;

    private Integer type;

    private Integer priority;

    private Integer height;

    private Integer width;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public Integer getEvent_idx() {
        return event_idx;
    }

    public void setEvent_idx(Integer event_idx) {
        this.event_idx = event_idx;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }
    
    
}
