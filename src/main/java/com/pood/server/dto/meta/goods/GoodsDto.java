package com.pood.server.dto.meta.goods;

import com.pood.server.dto.meta.goods.PromotionDto.PromotionGoodsProductInfo;
import com.pood.server.dto.meta.product.ProductDto;
import com.pood.server.entity.meta.GoodsCt;
import com.pood.server.entity.meta.GoodsImage;
import com.pood.server.entity.meta.ProductImage;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class GoodsDto {

    @Setter
    @Getter
    @NoArgsConstructor
    public static class SortedGoodsList {

        private Integer idx;
        private Integer pcIdx;
        private Integer goodsTypeIdx;
        private String displayType;
        private String goodsName;
        private Integer goodsOriginPrice;
        private Integer goodsPrice;
        private Integer discountRate;
        private Integer discountPrice;
        private Integer saleStatus;
        private Integer visible;
        private Integer mainProduct;
        private Double averageRating;
        private Long reviewCnt;
        private LocalDateTime updatetime;
        private LocalDateTime recordbirth;
        private String mainImage;
        private PromotionDto.PromotionGoodsProductInfo promotionInfo;
        private Boolean isRecommend;
        private int score;
        private long buyCount;
        private String brandName;
        private boolean newest;
        private Integer weight;

        @Builder
        public SortedGoodsList(final Integer idx, final Integer pcIdx, final Integer goodsTypeIdx,
            final String displayType, final String goodsName, final Integer goodsOriginPrice,
            final Integer goodsPrice,
            final Integer discountRate, final Integer discountPrice, final Integer saleStatus,
            final Integer visible,
            final Integer mainProduct, final Double averageRating, final Long reviewCnt,
            final LocalDateTime updatetime, final LocalDateTime recordbirth, final String mainImage,
            final PromotionGoodsProductInfo promotionInfo, final Boolean isRecommend,
            final int score, final long buyCount) {
            this.idx = idx;
            this.pcIdx = pcIdx;
            this.goodsTypeIdx = goodsTypeIdx;
            this.displayType = displayType;
            this.goodsName = goodsName;
            this.goodsOriginPrice = goodsOriginPrice;
            this.goodsPrice = goodsPrice;
            this.discountRate = discountRate;
            this.discountPrice = discountPrice;
            this.saleStatus = saleStatus;
            this.visible = visible;
            this.mainProduct = mainProduct;
            this.averageRating = averageRating;
            this.reviewCnt = reviewCnt;
            this.updatetime = updatetime;
            this.recordbirth = recordbirth;
            this.mainImage = mainImage;
            this.promotionInfo = promotionInfo;
            this.isRecommend = isRecommend;
            this.score = score;
            this.buyCount = buyCount;
        }

        public void promotionIfExistUpdatePrice(
            final PromotionGoodsProductInfo promotionGoodsProductInfo) {
            this.promotionInfo = promotionGoodsProductInfo;
            this.discountPrice = promotionGoodsProductInfo.getPrDiscountPrice();
            this.discountRate = promotionGoodsProductInfo.getPrDiscountRate();
            this.goodsPrice = promotionGoodsProductInfo.getPrPrice();
        }

        public boolean eqIdx(final int idx) {
            return this.idx.equals(idx);
        }

    }


    @Setter
    @Getter
    public static class GoodsDetail {

        private Integer idx;
        private Integer pcIdx;
        private Integer goodsTypeIdx;
        private String displayType;
        private String goodsName;
        private String goodsDescv;
        private Integer goodsOriginPrice;
        private Integer goodsPrice;
        private Integer discountRate;
        private Integer quantity;
        private Integer saleStatus;
        private Integer limitQuantity;
        private Integer purchaseType;
        private Integer mainProduct;
        private Double averageRating;
        private Long reviewCnt;
        private Boolean isPromotion;
        private List<ProductDto.ProductDetail> productList;
        private PromotionDto.PromotionProductInfo promotion;
        private List<GoodsImage> goodsImages;
        private List<ProductImage> mainImages;
        private Integer weight;

    }


    @Setter
    @Getter
    public static class GoodsFilterCtDto {

        private Long idx;
        private Long goodsCtIdx;
        private Integer petIdx;
        private String url;
        private String name;
        private String type;
        private String fieldKey;
        private String fieldValue;

        public GoodsFilterCtDto(Long idx, Long goodsCtIdx, Integer petIdx, String url, String name,
            String type, String fieldKey, String fieldValue) {
            this.idx = idx;
            this.goodsCtIdx = goodsCtIdx;
            this.petIdx = petIdx;
            this.url = url;
            this.name = name;
            this.type = type;
            this.fieldKey = fieldKey;
            this.fieldValue = fieldValue;
        }
    }

    @Getter
    @Setter
    public static class GoodsCtDto {

        private long goodsFilterIdx;
        private List<List<GoodsFilterCtForm>> goodsFilterCtTypeList;

        private GoodsCtDto(final long goodsFilterIdx,
            final List<List<GoodsFilterCtForm>> goodsFilterCtTypeList) {
            this.goodsFilterIdx = goodsFilterIdx;
            this.goodsFilterCtTypeList = goodsFilterCtTypeList;
        }

        public static GoodsCtDto create(final GoodsCt goodsCt,
            final List<GoodsFilterCtForm> goodsFilterCts) {

            Map<String, List<GoodsFilterCtForm>> goodsFilterCtMap = goodsFilterCts
                .stream()
                .filter(goodsFilterCt -> goodsFilterCt.idGoodsCtIdxEq(goodsCt.getIdx()))
                .collect(Collectors.groupingBy(GoodsFilterCtForm::getType));

            GoodsFilterListDtoGroup goodsFilterCtList = new GoodsFilterListDtoGroup();
            goodsFilterCtList.addGoodsFilterCtList(goodsFilterCtMap);

            return new GoodsCtDto(goodsCt.getIdx(), goodsFilterCtList.getGoodsFilterCtTypeList());
        }
    }


}
