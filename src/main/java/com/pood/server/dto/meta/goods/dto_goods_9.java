package com.pood.server.dto.meta.goods;
 
import java.util.List;

import com.pood.server.dto.dto_image_2;
import com.pood.server.dto.meta.dto_coupon_2;
import com.pood.server.object.meta.vo_goods_3;
import com.pood.server.object.meta.vo_goods_pr_code;

public class dto_goods_9 extends vo_goods_3{

    private String  updatetime;
    
    private String  recordbirth;

    private List<dto_coupon_2> coupon_list;

    private List<vo_goods_pr_code> pr_code;
 
    private List<dto_image_2>  goods_image;

    private List<dto_image_2>  product_image;

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public List<dto_coupon_2> getCoupon_list() {
        return coupon_list;
    }

    public void setCoupon_list(List<dto_coupon_2> coupon_list) {
        this.coupon_list = coupon_list;
    }

    public List<vo_goods_pr_code> getPr_code() {
        return pr_code;
    }

    public void setPr_code(List<vo_goods_pr_code> pr_code) {
        this.pr_code = pr_code;
    }

    public List<dto_image_2> getGoods_image() {
        return goods_image;
    }

    public void setGoods_image(List<dto_image_2> goods_image) {
        this.goods_image = goods_image;
    }

    public List<dto_image_2> getProduct_image() {
        return product_image;
    }

    public void setProduct_image(List<dto_image_2> product_image) {
        this.product_image = product_image;
    }
     
    
    

}
