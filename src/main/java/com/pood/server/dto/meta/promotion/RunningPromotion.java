package com.pood.server.dto.meta.promotion;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
public class RunningPromotion implements Comparable<RunningPromotion> {

    private Integer promotionIdx;
    private String imageUrl;
    private int priority;
    private String innerTitle;
    private String type;
    private LocalDateTime startPeriod;
    private LocalDateTime endPeriod;
    private String details;

    @Override
    public int compareTo(final RunningPromotion o) {
        if (this.priority == o.priority) {
            return o.promotionIdx - this.promotionIdx;
        }
        return Integer.compare(this.priority, o.priority);
    }
}