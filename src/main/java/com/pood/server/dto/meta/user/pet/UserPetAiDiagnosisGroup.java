package com.pood.server.dto.meta.user.pet;

import com.pood.server.controller.request.userpet.PetWorryRequest;
import com.pood.server.entity.meta.PetWorry;
import com.pood.server.entity.user.UserPetAiDiagnosis;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Value;

@Value
public class UserPetAiDiagnosisGroup {

    List<UserPetAiDiagnosis> list;

    public UserPetAiDiagnosisGroup() {
        this.list = new ArrayList<>();
    }

    private UserPetAiDiagnosisGroup(final List<UserPetAiDiagnosis> list) {
        this.list = list;
    }

    public static UserPetAiDiagnosisGroup of(final List<UserPetAiDiagnosis> list) {
        return new UserPetAiDiagnosisGroup(list);
    }

    public List<UserPetAiDiagnosis> saveAllObjects(final List<PetWorry> inputEntityList, final List<PetWorryRequest> requestList,
        final int userPetIdx) {
        for (PetWorry petWorry : inputEntityList) {
            for (PetWorryRequest petWorryRequest : requestList) {
                if (petWorry.getId().equals(petWorryRequest.getIdx())) {
                    list.add(
                        UserPetAiDiagnosis.builder()
                            .userPetIdx(userPetIdx)
                            .aiDiagnosisIdx(null)
                            .ardGroupCode(null)
                            .ardGroup(null)
                            .ardName(null)
                            .petWorryIdx(petWorry.getId())
                            .priority(petWorryRequest.getPriority())
                            .build());
                    break;
                }
            }
        }
        return list;
    }

    public List<Long> getWorryIdxList() {
        return list.stream().map(UserPetAiDiagnosis::getPetWorryIdx).collect(Collectors.toList());
    }

    public List<PetWorryRequest> toRequestList() {
        return this.list.stream()
            .map(userPetAiDiagnosis -> new PetWorryRequest(
                userPetAiDiagnosis.getPetWorryIdx(),
                userPetAiDiagnosis.getPriority())
            )
            .collect(Collectors.toList());
    }
}
