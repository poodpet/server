package com.pood.server.dto.meta.product;

import com.pood.server.entity.UserPetCupInfo;
import com.pood.server.entity.meta.Feed;
import com.pood.server.entity.meta.ProductImage;
import com.pood.server.service.pet_solution.nutrition.NutritionDetailModelLegacy;
import java.util.HashMap;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class ProductDto {



    @Setter
    @Getter
    public static class ProductDetail {
        private Integer idx;
        private Integer brandIdx;
        private Long sellerIdx;
        private String productName;
        private Integer quantity;
        private Integer productQty;
        private Integer ctSubIdx;
        private Integer ctIdx;

        private List<ProductImage> productImageList;
        Feed feed;
        String ingredients;
    }

    @Setter
    @Getter
    @Deprecated
    public static class AiSolutionProduct {
        private Integer productIdx;
        private float score;

        private float dailyCupAnalysisResult = 0f; // 하루 사료 섭취량
        private int allDayAnalysisResult = 0;// 먹는데 걸리는 기간
        private double dailySnackAnalysisResult = 0f;//하루 간식 섭취량

        private HashMap<String, Boolean> petUserWorry;
        private HashMap<String, Boolean> petAnalysis;
        private UserPetCupInfo userPetCupInfo;
        private List<NutritionDetailModelLegacy> nutritionDetailModelList;

        public AiSolutionProduct(Integer productIdx, float score, List<NutritionDetailModelLegacy> nutritionDetailModelList) {
            this.productIdx = productIdx;
            this.score = score;
            this.nutritionDetailModelList = nutritionDetailModelList;
        }

        public void setPetUserWorry(HashMap<String, Boolean> petUserWorry) {
            this.petUserWorry = petUserWorry;
        }

        public void setPetAnalysis(HashMap<String, Boolean> petAnalysis) {
            this.petAnalysis = petAnalysis;
        }

        public void setUserPetCupInfo(UserPetCupInfo userPetCupInfo) {
            this.userPetCupInfo = userPetCupInfo;
        }
    }


    @Setter
    @Getter
    public static class DiscontinuedProductList {
        private Integer idx;
        private Long count;

        public boolean isEmptyProduct(){
            return count == 0L;
        }
    }
}
