package com.pood.server.dto.meta.pet;

import java.util.List;

import com.pood.server.object.meta.vo_pet_doctor;
import com.pood.server.object.meta.vo_pet_doctor_image;

public class dto_pet_doctor extends vo_pet_doctor{

    private Integer idx;

    private String  updatetime;

    private String  recordbirth;

    private List<vo_pet_doctor_image> image;
    
    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public List<vo_pet_doctor_image> getImage() {
        return image;
    }

    public void setImage(List<vo_pet_doctor_image> image) {
        this.image = image;
    }

    
}
