package com.pood.server.dto.meta.goods;

import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class PromotionDto {

    @Setter
    @Getter
    public static class PromotionProductInfo {

        private Integer idx;
        private String name;
        private Integer prPrice;
        private Integer prDiscountPrice;
        private Integer prDiscountRate;
        private String promotionImageUrl;
        private String startPeriod;
        private String endPeriod;
        private String type;
    }

    @Setter
    @Getter
    public static class PromotionGoodsProductInfo {

        private Integer idx;
        private String name;
        private Integer prPrice;
        private Integer prDiscountPrice;
        private Integer prDiscountRate;
        private String startPeriod;
        private String endPeriod;
        private Integer goodsIdx;

        public PromotionGoodsProductInfo() {
        }

        public PromotionGoodsProductInfo(Integer idx, String name, Integer prPrice,
            Integer prDiscountPrice, Integer prDiscountRate, String startPeriod, String endPeriod,
            Integer goodsIdx) {
            this.idx = idx;
            this.name = name;
            this.prPrice = prPrice;
            this.prDiscountPrice = prDiscountPrice;
            this.prDiscountRate = prDiscountRate;
            this.startPeriod = startPeriod;
            this.endPeriod = endPeriod;
            this.goodsIdx = goodsIdx;
        }
    }

    @Setter
    @Getter
    public static class PromotionGoodsListInfo {

        private Integer idx;
        private String name;
        private String subName;
        private String startPeriod;
        private String endPeriod;
        private String type;
        private Integer pcIdx;
        private String details;
        private List<PromotionGroup> promotionGroupGoods;
        private List<PromotionImage> promotionImageList;
        private String mainImages;

        public void setPromotionGroupGoods(List<PromotionGroup> promotionGroupGoods) {
            this.promotionGroupGoods = promotionGroupGoods;
        }

        public void setPromotionImageList(List<PromotionImage> promotionImageList) {
            this.promotionImageList = promotionImageList;
        }
    }

    @Setter
    @Getter
    public static class PromotionGroupGoods {

        private Integer promotionGroupGoodsIdx;
        private String name;

        private Integer goodsIdx;
        private String goodsName;
        private Integer prPrice;
        private Integer prDiscountPrice;
        private Integer prDiscountRate;
        private String mainImage;
    }

    @Setter
    @Getter
    public static class PromotionGroup {

        private String name;
        private List<PromotionGoods> promotionGoods;

        public PromotionGroup(String name, List<PromotionGoods> promotionGoods) {
            this.name = name;
            this.promotionGoods = promotionGoods;
        }


    }

    @Setter
    @Getter
    public static class PromotionGoods {

        private Integer goodsIdx;
        private Integer prGroupIdx;
        private String name;
        private String url;
        private Integer prPrice;
        private Integer prDiscountPrice;
        private Integer prDiscountRate;

        private Integer goodsOriginPrice;
        private Integer goodsPrice;
        private Integer discountRate;
        private Integer discountPrice;

        private Long reviewCnt;
        private Double averageRating;
        private Integer saleStatus;

        private String brandName;
        private boolean newest;

        public boolean eqPrGroupIdx(final Integer prGroupIdx) {
            return this.prGroupIdx.equals(prGroupIdx);
        }

        public PromotionGoods(final Integer goodsIdx, final Integer prPrice,
            final Integer prDiscountPrice,
            Integer prDiscountRate) {
            this.goodsIdx = goodsIdx;
            this.prPrice = prPrice;
            this.prDiscountPrice = prDiscountPrice;
            this.prDiscountRate = prDiscountRate;
        }

        public PromotionGoods() {
        }
    }

    @Setter
    @Getter
    public static class PromotionGroupGoods2 {

        private Integer promotionGroupGoodsIdx;
        private String name;

        private Integer goodsIdx;
        private String goodsName;
        private Integer prPrice;
        private Integer prDiscountPrice;
        private Integer prDiscountRate;
        private String mainImage;
        private String startPeriod;
        private String endPeriod;
    }

    @Setter
    @Getter
    public static class PromotionImage {

        private Integer idx;
        private String url;
        private Integer type;

        public PromotionImage(final Integer idx, final String url, final Integer type) {
            this.idx = idx;
            this.url = url;
            this.type = type;
        }
    }

}
