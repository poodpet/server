package com.pood.server.dto.meta;

import com.pood.server.object.meta.vo_coupon_brand;

public class dto_coupon_brand extends vo_coupon_brand{

    private Integer idx;

    private String  recordbirth;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }
    
}
