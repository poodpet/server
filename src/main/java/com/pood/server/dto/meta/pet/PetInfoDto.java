package com.pood.server.dto.meta.pet;

import lombok.Getter;

@Getter
public class PetInfoDto {

    private final int idx;
    private final int pcId;
    private final String pcKind;

    public PetInfoDto(final int idx, final int pcId, final String pcKind) {
        this.idx = idx;
        this.pcId = pcId;
        this.pcKind = pcKind;
    }
}
