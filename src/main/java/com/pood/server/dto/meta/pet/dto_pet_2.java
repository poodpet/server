package com.pood.server.dto.meta.pet;

import java.util.List;

import com.pood.server.dto.dto_image_4;
import com.pood.server.object.meta.vo_pet;

public class dto_pet_2 extends vo_pet{

    private Integer idx;

    private String  updatetime;

    private String  recordbirth;

    private List<dto_image_4> image;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public List<dto_image_4> getImage() {
        return image;
    }

    public void setImage(List<dto_image_4> image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "dto_pet_2 [idx=" + idx + ", image=" + image + ", recordbirth=" + recordbirth + ", updatetime="
                + updatetime + "]";
    }

    
    
    
}
