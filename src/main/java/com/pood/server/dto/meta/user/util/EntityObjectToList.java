package com.pood.server.dto.meta.user.util;

import java.util.List;

public interface EntityObjectToList<T, F> {

    List<T> saveAllObjects(List<F> inputEntityList, final int userPetIdx);

}
