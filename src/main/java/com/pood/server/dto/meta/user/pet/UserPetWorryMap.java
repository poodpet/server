package com.pood.server.dto.meta.user.pet;

import com.pood.server.controller.response.pet.UserPetInfoResponse;
import com.pood.server.dto.user.pet.UserPetAiDiagnosisDto;
import com.pood.server.entity.user.UserPet;
import java.util.List;
import java.util.Map;

public class UserPetWorryMap implements UserPetProblem {

    private final Map<Integer, List<UserPetAiDiagnosisDto>> userPetAiDiagnosisDtoMap;

    public UserPetWorryMap(final Map<Integer, List<UserPetAiDiagnosisDto>> userPetAiDiagnosisDtoMap) {
        this.userPetAiDiagnosisDtoMap = userPetAiDiagnosisDtoMap;
    }

    @Override
    public void ifExistKeyThenSetProblems(final UserPet userPet, final UserPetInfoResponse userPetInfoResponse) {
        if (userPetAiDiagnosisDtoMap.containsKey(userPet.getIdx())) {
            userPetInfoResponse.setAiDiagnosisList(
                userPetAiDiagnosisDtoMap.get(userPet.getIdx()));
        }
    }
}
