package com.pood.server.dto.meta.goods;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.Getter;

@Getter
public class GoodsFilterListDtoGroup {

    private List<List<GoodsFilterCtForm>> goodsFilterCtTypeList;

    public GoodsFilterListDtoGroup() {
        this.goodsFilterCtTypeList = new ArrayList<>();
    }

    public void addGoodsFilterCtList(
        final Map<String, List<GoodsFilterCtForm>> goodsFilterCtMap) {
        for (List<GoodsFilterCtForm> categoryFilter : goodsFilterCtMap.values()) {
            goodsFilterCtTypeList.add(
                categoryFilter.stream()
                    .sorted(Comparator.comparingInt(GoodsFilterCtForm::getPriority))
                    .collect(Collectors.toList()));
        }
    }


}
