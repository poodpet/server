package com.pood.server.dto.meta.product;

import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.ProductType;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;

@Getter
public class ProductBaseDataGroup {

    private final List<ProductBaseData> productBaseDataList;

    public ProductBaseDataGroup(final List<ProductBaseData> productBaseDataList) {
        this.productBaseDataList = Collections.unmodifiableList(productBaseDataList);
    }

    public List<ProductBaseData> getProductTypeAndAllergyFilter(final ProductType productType,
        final List<String> allergyNameList) {
        return productBaseDataList.stream()
            .filter(productBaseData -> productBaseData.eqProductType(productType))
            .filter(productBaseData -> productBaseData.isSupplies()
                || !productBaseData.isIncludeAllergy(allergyNameList))
            .collect(Collectors.toList());
    }

    public List<ProductBaseData> getAllergyFilter(final List<String> allergyNameList) {
        return productBaseDataList.stream()
            .filter(productBaseData -> productBaseData.isSupplies()
                || !productBaseData.isIncludeAllergy(allergyNameList))
            .collect(Collectors.toList());
    }

    public ProductBaseDataGroup getAllergyFilterAndShuffle(final List<String> allergyNameList,
        final int size) {
        List<ProductBaseData> allergyFilter = getAllergyFilter(allergyNameList);
        Collections.shuffle(allergyFilter);
        return new ProductBaseDataGroup(allergyFilter).getLimitBySize(size);
    }

    public ProductBaseDataGroup getProductTypeFilter(final ProductType productType) {
        return new ProductBaseDataGroup(productBaseDataList.stream()
            .filter(productBaseData -> productBaseData.eqProductType(productType))
            .collect(Collectors.toList()));
    }

    public ProductBaseDataGroup getLimitBySize(final int size) {
        return new ProductBaseDataGroup(
            productBaseDataList.stream()
                .limit(size)
                .collect(Collectors.toList()));
    }

    public List<Integer> getProductIdxList() {
        return productBaseDataList
            .stream()
            .map(ProductBaseData::getIdx)
            .collect(Collectors.toList());
    }

    public ProductBaseDataGroup getBestScoreByArdCode(final Integer ardCode) {
        return new ProductBaseDataGroup(productBaseDataList
            .stream()
            .filter(x -> x.getBestScoreByArdCode(ardCode))
            .collect(Collectors.toList())
        );
    }

}
