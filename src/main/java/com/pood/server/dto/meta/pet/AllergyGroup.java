package com.pood.server.dto.meta.pet;

import com.pood.server.controller.response.pet.AllergyDataResponse;
import com.pood.server.entity.meta.AllergyData;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class AllergyGroup {

    private final List<AllergyData> allergyDataList;

    public AllergyGroup(final List<AllergyData> allergyDataList) {
        this.allergyDataList = Collections.unmodifiableList(allergyDataList);
    }

    public List<AllergyDataResponse> toResponse() {
        return allergyDataList.stream()
            .map(allergyData -> new AllergyDataResponse(allergyData.getIdx(), allergyData.getName(), allergyData.getUrl()))
            .collect(Collectors.toList());
    }
}
