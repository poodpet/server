package com.pood.server.dto.meta;

import com.pood.server.object.meta.maintab.vo_maintab_goods;

public class dto_maintab_goods extends vo_maintab_goods{

    private Integer idx;

    private String recordbirth;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    
    
}
