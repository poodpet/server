package com.pood.server.dto.meta.event;

import com.pood.server.util.MarketingType;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import lombok.Getter;

@Getter
public class EndEvent {

    private final Integer idx;
    private final String title;
    private final String intro;
    private final String startDate;
    private final String endDate;
    private final String url;

    private final MarketingType type;
    private final String typeName;

    private final Integer pcIdx;

    public EndEvent(final Integer idx, final String title, final String intro, final LocalDateTime startDate, final LocalDateTime endDate,
        final String url, final MarketingType type, final String typeName, final Integer pcIdx) {
        this.idx = idx;
        this.title = title;
        this.intro = intro;
        this.startDate = startDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        this.endDate = endDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        this.url = url;
        this.type = type;
        this.typeName = typeName;
        this.pcIdx = pcIdx;
    }
}
