package com.pood.server.dto.meta.event;

import java.util.List;

import com.pood.server.object.meta.vo_event_info_2;
import com.pood.server.object.meta.vo_event_type;

public class dto_event_info_3 extends vo_event_info_2{

    private Integer idx;

    private vo_event_type type;

    private String  updatetime;

    private String  recordbirth;

    private List<dto_event_image_2> image;

    List<dto_event_goods_list_2> item;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public List<dto_event_image_2> getImage() {
        return image;
    }

    public void setImage(List<dto_event_image_2> image) {
        this.image = image;
    }

    public vo_event_type getType() {
        return type;
    }

    public void setType(vo_event_type type) {
        this.type = type;
    }

    public List<dto_event_goods_list_2> getItem() {
        return item;
    }

    public void setItem(List<dto_event_goods_list_2> item) {
        this.item = item;
    }

    
    
}
