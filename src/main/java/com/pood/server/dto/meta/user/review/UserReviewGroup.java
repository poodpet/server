package com.pood.server.dto.meta.user.review;

import com.pood.server.entity.order.OrderBasket;
import com.pood.server.entity.order.group.OrderBasketGroup;
import com.pood.server.entity.user.UserReview;
import java.util.List;

public class UserReviewGroup {

    private final List<UserReview> list;

    public UserReviewGroup(final List<UserReview> userReviewList) {
        this.list = userReviewList;
    }

    public boolean isPossibleWriteReview(
        final OrderBasket orderBasket) {
        return list.stream()
            .filter(userReview -> userReview.eqOrderNumber(orderBasket.getOrderNumber())
                && userReview.eqGoodsIdx(orderBasket.getGoodsIdx()))
            .findFirst()
            .isEmpty();
    }

    public boolean isReviewPossible(final OrderBasketGroup orderBasketGroup) {
        if (orderBasketGroup.isEmpty()) {
            return false;
        }
        return orderBasketGroup.getSize() > list.size();

    }
}
