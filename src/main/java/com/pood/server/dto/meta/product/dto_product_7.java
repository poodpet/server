package com.pood.server.dto.meta.product;

import java.util.List;

import com.pood.server.object.meta.vo_product_2;
import com.pood.server.object.meta.vo_product_image;
import com.pood.server.dto.meta.dto_feed;
import com.pood.server.dto.meta.pet.dto_pet_doctor_desc;

public class dto_product_7 extends vo_product_2{

    private Integer idx;

    private String  updatetime;

    private String  recordbirth;

    private dto_pet_doctor_desc pet_doctor_desc;

    private List<dto_feed>    product_feed;

    private List<vo_product_image> image;


    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }
  
    public List<vo_product_image> getImage() {
        return image;
    }

    public void setImage(List<vo_product_image> image) {
        this.image = image;
    }

    public dto_pet_doctor_desc getPet_doctor_desc() {
        return pet_doctor_desc;
    }

    public void setPet_doctor_desc(dto_pet_doctor_desc pet_doctor_desc) {
        this.pet_doctor_desc = pet_doctor_desc;
    }

    public List<dto_feed> getProduct_feed() {
        return product_feed;
    }

    public void setProduct_feed(List<dto_feed> product_feed) {
        this.product_feed = product_feed;
    }

 
    
    
}
