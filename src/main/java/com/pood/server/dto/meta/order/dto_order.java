package com.pood.server.dto.meta.order;

import com.pood.server.object.meta.vo_order;

public class dto_order extends vo_order{

    private Integer idx;

    private String  recordbirth;
    
    private String  updatetime;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

  
}
