package com.pood.server.dto.meta.goods;

public class dto_goods_12 {

    private String  end_date;

    private Integer coupon_apply;

    private Integer purchase_type;

    private Integer goods_type_idx;

    private String  goods_name;

    private String  recordbirth;

    private Integer visible;

    private Integer quantity;

    private Integer refund_type;

    private Integer main_product;

    private Integer discount_price;

    private Integer goods_price;

    private Integer weight;

    private Integer sale_status;

    private String  goods_descv;

    private String  startdate;

    private Integer discount_rate;

    private Integer pc_idx;

    private Integer seller_idx;

    private Integer limit_quantity;

    private String  updatetime;

    private Integer idx;

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public Integer getCoupon_apply() {
        return coupon_apply;
    }

    public void setCoupon_apply(Integer coupon_apply) {
        this.coupon_apply = coupon_apply;
    }

    public Integer getPurchase_type() {
        return purchase_type;
    }

    public void setPurchase_type(Integer purchase_type) {
        this.purchase_type = purchase_type;
    }

    public Integer getGoods_type_idx() {
        return goods_type_idx;
    }

    public void setGoods_type_idx(Integer goods_type_idx) {
        this.goods_type_idx = goods_type_idx;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getRefund_type() {
        return refund_type;
    }

    public void setRefund_type(Integer refund_type) {
        this.refund_type = refund_type;
    }

    public Integer getMain_product() {
        return main_product;
    }

    public void setMain_product(Integer main_product) {
        this.main_product = main_product;
    }

    public Integer getDiscount_price() {
        return discount_price;
    }

    public void setDiscount_price(Integer discount_price) {
        this.discount_price = discount_price;
    }

    public Integer getGoods_price() {
        return goods_price;
    }

    public void setGoods_price(Integer goods_price) {
        this.goods_price = goods_price;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getSale_status() {
        return sale_status;
    }

    public void setSale_status(Integer sale_status) {
        this.sale_status = sale_status;
    }

    public String getGoods_descv() {
        return goods_descv;
    }

    public void setGoods_descv(String goods_descv) {
        this.goods_descv = goods_descv;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public Integer getDiscount_rate() {
        return discount_rate;
    }

    public void setDiscount_rate(Integer discount_rate) {
        this.discount_rate = discount_rate;
    }

    public Integer getPc_idx() {
        return pc_idx;
    }

    public void setPc_idx(Integer pc_idx) {
        this.pc_idx = pc_idx;
    }

    public Integer getSeller_idx() {
        return seller_idx;
    }

    public void setSeller_idx(Integer seller_idx) {
        this.seller_idx = seller_idx;
    }

    public Integer getLimit_quantity() {
        return limit_quantity;
    }

    public void setLimit_quantity(Integer limit_quantity) {
        this.limit_quantity = limit_quantity;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    @Override
    public String toString() {
        return "dto_goods_12 [coupon_apply=" + coupon_apply + ", discount_price=" + discount_price + ", discount_rate="
                + discount_rate + ", end_date=" + end_date + ", goods_descv=" + goods_descv + ", goods_name="
                + goods_name + ", goods_price=" + goods_price + ", goods_type_idx=" + goods_type_idx + ", idx=" + idx
                + ", limit_quantity=" + limit_quantity + ", main_product=" + main_product + ", pc_idx=" + pc_idx
                + ", purchase_type=" + purchase_type + ", quantity=" + quantity + ", recordbirth=" + recordbirth
                + ", refund_type=" + refund_type + ", sale_status=" + sale_status + ", seller_idx=" + seller_idx
                + ", startdate=" + startdate + ", updatetime=" + updatetime + ", visible=" + visible + ", weight="
                + weight + "]";
    }

    
}
