package com.pood.server.dto.meta.user.pet;

import com.pood.server.dto.meta.user.util.EntityObjectToList;
import com.pood.server.entity.meta.GrainSize;
import com.pood.server.entity.user.UserPetGrainSize;
import java.util.ArrayList;
import java.util.List;

public class UserPetGrainSizeGroup implements EntityObjectToList<UserPetGrainSize, GrainSize> {

    private final List<UserPetGrainSize> list = new ArrayList<>();

    @Override
    public List<UserPetGrainSize> saveAllObjects(final List<GrainSize> inputEntityList,
        final int userPetIdx) {
        for (GrainSize grainSize : inputEntityList) {
            list.add(
                UserPetGrainSize.toEntity(
                    userPetIdx,
                    grainSize.getIdx(), grainSize.getName(), grainSize.getSizeMin(),
                    grainSize.getSizeMax())
            );
        }
        return list;
    }
}
