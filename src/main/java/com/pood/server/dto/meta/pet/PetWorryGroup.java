package com.pood.server.dto.meta.pet;

import com.pood.server.controller.response.pet.PetWorryResponse;
import com.pood.server.dto.meta.worry.PetWorryReadDto;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;

@Getter
public class PetWorryGroup {

    private final List<PetWorryReadDto> petWorryList;

    public PetWorryGroup(final List<PetWorryReadDto> petWorryList) {
        this.petWorryList = Collections.unmodifiableList(petWorryList);
    }

    public List<PetWorryResponse> toResponse() {
        return petWorryList.stream()
            .map(petWorry -> PetWorryResponse.of(petWorry.getId(), petWorry.getName(), petWorry.getDogUrl(),
                petWorry.getCatUrl()))
            .collect(Collectors.toList());
    }
}
