package com.pood.server.dto.meta;

import java.util.List;

import com.pood.server.object.meta.vo_notice;
import com.pood.server.dto.dto_image;

public class dto_notice extends vo_notice{
    
    private Integer idx;

    private String  updatetime;

    private String  recordbirth;

    private List<dto_image> image;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }

    public List<dto_image> getImage() {
        return image;
    }

    public void setImage(List<dto_image> image) {
        this.image = image;
    }
    

}
