package com.pood.server.dto.meta.main;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class HomeDto {


    @Setter
    @Getter
    public static class HomeEventAndPromotionDto {
        private Integer idx;
        private String url;
        private String displayType;
        private String type;
        private String startDate;
        private String endDate;

        public HomeEventAndPromotionDto(Integer idx, String url, String type,String displayType, String startDate, String endDate) {
            this.idx = idx;
            this.url = url;
            this.type = type;
            this.displayType = displayType;
            this.startDate = startDate;
            this.endDate = endDate;
        }
    }


}
