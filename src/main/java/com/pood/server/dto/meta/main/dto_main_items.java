package com.pood.server.dto.meta.main;

import java.util.List;

import com.pood.server.dto.meta.dto_main_goods_type;
import com.pood.server.dto.meta.goods.dto_goods_12;
import com.pood.server.dto.meta.goods.dto_goods_image;

public class dto_main_items {
    
    private Integer idx;

    private Integer main_idx;

    private dto_goods_12 goods;

    private dto_main_goods_type goods_type;

    private List<dto_goods_image> main_image;

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public Integer getMain_idx() {
        return main_idx;
    }

    public void setMain_idx(Integer main_idx) {
        this.main_idx = main_idx;
    }  
    public dto_goods_12 getGoods() {
        return goods;
    }

    public void setGoods(dto_goods_12 goods) {
        this.goods = goods;
    }

    public dto_main_goods_type getGoods_type() {
        return goods_type;
    }

    public void setGoods_type(dto_main_goods_type goods_type) {
        this.goods_type = goods_type;
    }

    public List<dto_goods_image> getMain_image() {
        return main_image;
    }

    public void setMain_image(List<dto_goods_image> main_image) {
        this.main_image = main_image;
    }

    @Override
    public String toString() {
        return "dto_main_items [goods=" + goods + ", goods_type=" + goods_type + ", idx=" + idx + ", main_idx="
                + main_idx + ", main_image=" + main_image + "]";
    }
 
    
    
}
