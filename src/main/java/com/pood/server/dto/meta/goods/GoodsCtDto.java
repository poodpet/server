package com.pood.server.dto.meta.goods;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
public class GoodsCtDto {

    @Setter
    @Getter
    public static class GoodsCtListDto {
        private Long idx;
        private String name;
        private String url;

        public GoodsCtListDto(Long idx, String name, String url) {
            this.idx = idx;
            this.name = name;
            this.url = url;
        }
    }

    @Setter
    @Getter
    public static class GoodsSubCtListDto {
        private Long idx;
        private Long goodsCtIdx;
        private String name;
        private String url;

        public GoodsSubCtListDto(Long idx, Long goodsCtIdx, String name, String url) {
            this.idx = idx;
            this.goodsCtIdx = goodsCtIdx;
            this.name = name;
            this.url = url;
        }
    }
    @Setter
    @Getter
    public static class GoodsCtDetailDto extends GoodsCtListDto{

        public GoodsCtDetailDto(Long idx, String name, String url) {
            super(idx, name, url);
        }
        List<GoodsSubCtListDto> goodsSubCtListDtos;

        public void setGoodsSubCtListDtos(List<GoodsSubCtListDto> goodsSubCtListDtos) {
            this.goodsSubCtListDtos = goodsSubCtListDtos;
        }
    }
}
