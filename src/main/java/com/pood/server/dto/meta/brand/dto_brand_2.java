package com.pood.server.dto.meta.brand;

import com.pood.server.object.meta.vo_brand;

public class dto_brand_2 extends vo_brand{    

    private Integer brand_idx;

    private String  updatetime;

    private String  recordbirth;

    
    public Integer getBrand_idx() {
        return brand_idx;
    }

    public void setBrand_idx(Integer brand_idx) {
        this.brand_idx = brand_idx;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getRecordbirth() {
        return recordbirth;
    }

    public void setRecordbirth(String recordbirth) {
        this.recordbirth = recordbirth;
    }
    
}
