package com.pood.server.dto.view;

public class dto_view_2 {

    private String  goods_name;

    private Integer brand_idx;

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public Integer getBrand_idx() {
        return brand_idx;
    }

    public void setBrand_idx(Integer brand_idx) {
        this.brand_idx = brand_idx;
    }

    
}
