package com.pood.server.dto.order;

import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.entity.meta.dto.home.OrderRankDto;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public class  OrderRankGroup {

    private final Page<OrderRankDto> rankGoods;

    public OrderRankGroup(final Page<OrderRankDto> rankGoods) {
        this.rankGoods = rankGoods;
    }

    public OrderRankGroup addCustomRankContents(final List<OrderRankDto> customRankItem) {
        final List<OrderRankDto> newContents = Stream
            .concat(rankGoods.getContent().stream(), customRankItem.stream())
            .collect(Collectors.toList());

        return new OrderRankGroup(
            new PageImpl<>(newContents, this.rankGoods.getPageable(), newContents.size()));
    }

    public List<Integer> getGoodsIdsList() {
        return rankGoods.map(OrderRankDto::getGoodsIdx)
            .stream()
            .collect(Collectors.toList());
    }

    public Page<SortedGoodsList> convertSortedGoods(final List<SortedGoodsList> goodsInfoList) {
        return rankGoods.map(
            orderRankDto -> getSortedGoodsList(goodsInfoList, orderRankDto));
    }

    public Page<SortedGoodsList> convertFrequentlyGoods(final List<SortedGoodsList> goodsInfoList) {
        return rankGoods.map(
            orderRankDto -> getFrequentlyGoodsList(goodsInfoList, orderRankDto));
    }

    private SortedGoodsList getSortedGoodsList(final List<SortedGoodsList> goodsInfoList,
        final OrderRankDto orderRankDto) {

        for (SortedGoodsList sortedGoodsList : goodsInfoList) {
            if (sortedGoodsList.eqIdx(orderRankDto.getGoodsIdx())) {
                return sortedGoodsList;
            }
        }
        return null;
    }

    private SortedGoodsList getFrequentlyGoodsList(final List<SortedGoodsList> goodsInfoList,
        final OrderRankDto orderRankDto) {
        for (SortedGoodsList sortedGoodsList : goodsInfoList) {
            if (sortedGoodsList.eqIdx(orderRankDto.getGoodsIdx())) {
                sortedGoodsList.setBuyCount(orderRankDto.getRank());
                return sortedGoodsList;
            }
        }
        return null;
    }

    public long getSize() {
        return this.rankGoods.getTotalElements();
    }
}
