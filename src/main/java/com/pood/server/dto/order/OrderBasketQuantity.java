package com.pood.server.dto.order;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class OrderBasketQuantity {

    private int goodsIdx;
    private int orderQuantity;
}
