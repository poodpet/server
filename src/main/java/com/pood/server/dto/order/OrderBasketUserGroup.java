package com.pood.server.dto.order;

import java.util.List;

public class OrderBasketUserGroup {

    private final List<OrderBasketQuantity> basketQuantityList;

    public OrderBasketUserGroup(final List<OrderBasketQuantity> basketQuantityList) {
        this.basketQuantityList = basketQuantityList;
    }

    public OrderBasketQuantity filterGoodsIdx(final int goodsIdx) {
        return basketQuantityList.stream()
            .filter(orderBasketQuantity -> orderBasketQuantity.getGoodsIdx() == goodsIdx)
            .findFirst().orElse(null);
    }

}
