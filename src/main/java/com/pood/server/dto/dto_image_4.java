package com.pood.server.dto;

public class dto_image_4 {

    private Integer image_idx;

    private Integer visible;

    private String  url;

    public Integer getImage_idx() {
        return image_idx;
    }

    public void setImage_idx(Integer image_idx) {
        this.image_idx = image_idx;
    }

    public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "dto_image_4 [image_idx=" + image_idx + ", url=" + url + ", visible=" + visible + "]";
    }

    
}
