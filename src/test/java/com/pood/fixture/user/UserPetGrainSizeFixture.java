package com.pood.fixture.user;

import com.pood.server.entity.user.UserPetGrainSize;
import java.util.List;

public class UserPetGrainSizeFixture {

    private static final Integer MEDIUM = 0;
    private static final Integer LARGE = 1;
    private static final Integer SMALL = 2;
    private static final Integer WET_FEED = 3;

    public static UserPetGrainSize ofWetFeed() {
        return new UserPetGrainSize(1, 1, WET_FEED, "습식", 0.0, 0.0, "");
    }

    public static UserPetGrainSize ofSmall() {
        return new UserPetGrainSize(1, 1, SMALL, "작은 알갱이", 0.0, 0.9, "");
    }

    public static UserPetGrainSize ofMedium() {
        return new UserPetGrainSize(1, 1, MEDIUM, "중간 알갱이", 1.0, 1.5, "");
    }

    public static UserPetGrainSize ofLarge() {
        return new UserPetGrainSize(1, 1, LARGE, "큰 알갱이", 1.6, 5.0, "");
    }

    public static List<UserPetGrainSize> allGrainSize() {
        return List.of(ofLarge(), ofMedium(), ofSmall(), ofWetFeed());
    }
}
