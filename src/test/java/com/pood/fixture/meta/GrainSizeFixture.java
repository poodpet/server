package com.pood.fixture.meta;

import com.pood.server.entity.meta.GrainSize;
import java.time.LocalDateTime;
import java.util.List;

public class GrainSizeFixture {

    private static final Integer MEDIUM = 0;
    private static final Integer LARGE = 1;
    private static final Integer SMALL = 2;
    private static final Integer WET_FEED = 3;

    private static GrainSize of(final Integer idx, final String name, final double sizeMin, final double sizeMAX) {
        return new GrainSize(idx, name, sizeMin, sizeMAX, name, "", 0, LocalDateTime.now(), LocalDateTime.now());
    }

    public static GrainSize ofWetFeed() {
        return of(WET_FEED, "습식", 0.0, 0.0);
    }

    public static GrainSize ofSmall() {
        return of(SMALL, "작은 알갱이", 0.0, 0.9);
    }

    public static GrainSize ofMedium() {
        return of(MEDIUM, "중간 알갱이", 1.0, 1.5);
    }

    public static GrainSize ofLarge() {
        return of(LARGE, "큰 알갱이", 1.6, 5.0);
    }

    public static List<GrainSize> allGrainSize() {
        return List.of(ofLarge(), ofMedium(), ofSmall(), ofWetFeed());
    }
}
