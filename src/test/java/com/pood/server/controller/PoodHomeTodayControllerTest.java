package com.pood.server.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import com.pood.server.controller.home.today.PoodHomeTodayController;
import com.pood.server.controller.response.home.icon.ShortCutIconResponse;
import com.pood.server.entity.meta.PetCategory;
import com.pood.server.entity.meta.ShortcutIcon;
import com.pood.server.facade.ShortCutFacade;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@WebMvcTest(PoodHomeTodayController.class)
class PoodHomeTodayControllerTest extends ControllerTests {

    private static final int DOG = 1;

    @MockBean
    private ShortCutFacade shortCutFacade;

    @Test
    @DisplayName("short cut icon 리스트 불러오기 (priority 순으로 정렬)")
    void shortCutIconTest() throws Exception {
        //given
        final PetCategory common = new PetCategory(0, "강아지, 고양이", "pet", "포유류", LocalDateTime.now(), LocalDateTime.now());
        final PetCategory dog = new PetCategory(1, "강아지", "dog", "포유류", LocalDateTime.now(), LocalDateTime.now());

        List<ShortcutIcon> list = new ArrayList<>();
        list.add(new ShortcutIcon(2L, "imageUrl2", "iconName2", "/redirectUrl2", 0, LocalDateTime.now(), dog));
        list.add(new ShortcutIcon(4L, "imageUrl4", "iconName4", "/redirectUrl4", 1, LocalDateTime.now(), common));
        list.add(new ShortcutIcon(3L, "imageUrl3", "iconName3", "/redirectUrl3", 2, LocalDateTime.now(), dog));
        list.add(new ShortcutIcon(1L, "YrNu3U", "pHMPNFK", "/redirectUrl1", 3, LocalDateTime.now(), common));

        //when
        List<ShortCutIconResponse> resultList = new ArrayList<>();
        for (ShortcutIcon shortcutIcon : list) {
            resultList.add(ShortCutIconResponse.of(shortcutIcon));
        }

        when(shortCutFacade.findIconList(DOG)).thenReturn(resultList);

        //then
        mockMvc.perform(get("/api/pood/v1-0/home/icon/" + DOG))
            .andExpect(ok())
            .andExpect(jsonPath("$[0].idx").value(2))
            .andExpect(jsonPath("$[1].idx").value(4))
            .andExpect(jsonPath("$[2].idx").value(3))
            .andExpect(jsonPath("$[3].idx").value(1));
    }

}