package com.pood.server.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.pood.server.config.AopConfiguration;
import com.pood.server.config.ControllerTokenAspect;
import com.pood.server.controller.user.UserDeliveryController;
import com.pood.server.facade.UserDeliveryFacade;
import com.pood.server.service.UserSeparate;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;

@WebMvcTest(UserDeliveryController.class)
@Import({AopConfiguration.class, ControllerTokenAspect.class})
class UserDeliveryControllerTest extends ControllerTests {

    @MockBean
    private UserDeliveryFacade userDeliveryFacade;

    @MockBean
    private UserSeparate userSeparate;

    @Test
    void saveDeliveryAddress() throws Exception {
        //given
        final String json = "{"
            + " \"address\": \"address\","
            + " \"defaultType\": 0,"
            + " \"detailAddress\": \"detailAddress\","
            + " \"name\": \"name\","
            + " \"nickname\": \"nickname\","
            + " \"zipcode\": \"12345\","
            + " \"inputType\": false,"
            + " \"remoteType\": 0,"
            + " \"phoneNumber\": \"01012345678\""
            + "}";
        //when
        doNothing().when(userDeliveryFacade).saveAddress(any(), any());

        //then
        mockMvc.perform(post("/api/pood/v1-0/delivery")
                .header("token", "c1318b9b-f924-4ad7-8362-b5d25764f6fe")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());
    }

}