package com.pood.server.controller;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doCallRealMethod;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pood.server.config.ControllerTokenAspect;
import com.pood.server.controller.request.user.UserInfoUpdateRequest;
import com.pood.server.controller.user.UserController;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.goods.GoodsFacade;
import com.pood.server.facade.UserFacade;
import com.pood.server.facade.user.info.UserPasswordChangeRequest;
import com.pood.server.handler.ControllerExceptionHandler;
import com.pood.server.service.MockTests;
import com.pood.server.service.UserNotiSeparate;
import com.pood.server.service.UserSeparate;
import com.pood.server.service.UserServiceV2;
import com.pood.server.util.UserStatus;
import java.time.LocalDateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.filter.CharacterEncodingFilter;

@WebMvcTest(UserController.class)
@Import(value = ControllerTokenAspect.class)
class UserControllerTest extends MockTests {

    ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    UserController userController;

    MockMvc mockMvc;

    @MockBean
    private UserServiceV2 userService;

    @MockBean
    private UserNotiSeparate userNotiSeparate;

    @MockBean
    private UserFacade userFacade;

    @MockBean
    private GoodsFacade goodsFacade;

    @MockBean
    private UserSeparate userSeparate;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(userController)
            .addFilters(new CharacterEncodingFilter("UTF-8", true))
            .setControllerAdvice(ControllerExceptionHandler.class)
            .alwaysDo(print())
            .build();
    }

    @Test
    @DisplayName("uuid로 유저 이메일 찾기")
    void findEmailTest() throws Exception {
        //given
        UserInfo userInfo = makeFakeUser();

        Mockito.when(userFacade.getUserByUUID(anyString()))
            .thenReturn(userInfo);

        //when
        final ResultActions result = mockMvc.perform(get("/api/pood/user/v1-0/email")
            .param("userUuid", "82f44cf0-0b9e-44e4-801a-64772e485c2d"));

        //then
        result.andExpect(status().isOk())
            .andExpect(jsonPath("$.email").value("email@pood.pet"));
    }

    @Test
    @DisplayName("유저 암호 변경")
    void changePasswordTest() throws Exception {
        //given
        UserInfo userInfo = makeFakeUser();

        UserPasswordChangeRequest request = new UserPasswordChangeRequest("password", "12345555");

        //when
        doCallRealMethod().when(userFacade).changePassword(request, userInfo);

        mockMvc.perform(patch("/api/pood/user/v1-0/password")
                .header("token", "270cfcb2-40b2-4273-9227-e59444c9f4de")
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isOk());
    }

    private UserInfo makeFakeUser() {
        return new UserInfo(
            1,
            "82f44cf0-0b9e-44e4-801a-64772e485c2d",
            "nickName",
            "email@pood.pet",
            "password",
            1,
            UserStatus.ACTIVE,
            "name",
            1000,
            "phone",
            "referralCode",
            true,
            LocalDateTime.now(),
            true, LocalDateTime.now(),
            true, LocalDateTime.now(),
            "key", 1, "tokenArn",
            "mlName", 0.0, 1,
            1, LocalDateTime.now(),
            LocalDateTime.now(), LocalDateTime.now(),
            null, false, null,
            false, null, false, null

        );
    }

    @Test
    @DisplayName("유저 정보 변경")
    void userChangeTest() throws Exception {
        //given
        final UserInfoUpdateRequest request = new UserInfoUpdateRequest(
            "f740cf34-45d8-4a1d-ac21-3c28b62d78f6", "newNickName",
            "01012345678");

        //when
        final ResultActions result = mockMvc.perform(put("/api/pood/user/v1-0/change")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(objectMapper.writeValueAsString(request)));

        //then
        result.andExpect(status().isOk());

    }

}