package com.pood.server.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import com.pood.server.controller.response.event.photoaward.AwardParticipateResponse;
import com.pood.server.controller.response.event.photoaward.EndPhotoAwardDetailResponse;
import com.pood.server.controller.response.event.photoaward.EndPhotoAwardWinner;
import com.pood.server.dto.meta.event.EndPhotoImgUrl;
import com.pood.server.facade.event.EventFacade;
import com.pood.server.service.EventService;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;

@WebMvcTest(EventController.class)
class EventControllerTest extends ControllerTests {

    @MockBean
    private EventFacade eventFacade;

    @MockBean
    private EventService eventService;

    @Test
    @DisplayName("포토 어워즈 명예의 전당 조회")
    void photoAwardsWinner() throws Exception {
        //given
        final List<EndPhotoAwardWinner> winnerList = List.of(
            new EndPhotoAwardWinner(1, "winnerImageUrl1"),
            new EndPhotoAwardWinner(2, "winnerImageUrl2"),
            new EndPhotoAwardWinner(3, "winnerImageUrl3")
        );

        //when
        when(eventFacade.findEndPhotoAwardWinners(anyInt()))
            .thenReturn(winnerList);

        //then
        mockMvc.perform(getMapping("/api/pood/v1-0/event/photo-awards/winner")
                .param("year", "2022"))
            .andExpect(ok())
            .andExpect(jsonPath("$[0].idx").value(1))
            .andExpect(jsonPath("$[0].imgUrl").value("winnerImageUrl1"));
    }

    @Test
    @DisplayName("포토 어워즈 상세조회")
    void photoAwardsDetail() throws Exception {
        //given
        final EndPhotoImgUrl winnerImageUrl = new EndPhotoImgUrl("winnerImageUrl");

        //when
        when(eventFacade.endPhotoAwardsDetail(anyInt()))
            .thenReturn(new EndPhotoAwardDetailResponse(2,
                "intro", "2022.06.10", "2022.06.10", "eventImageUrl", winnerImageUrl));

        //then
        mockMvc.perform(getMapping("/api/pood/v1-0/event/photo-awards/" + 1))
            .andExpect(ok())
            .andExpect(jsonPath("$.countAwards").value(2))
            .andExpect(jsonPath("$.intro").value("intro"))
            .andExpect(jsonPath("$.startDate").value("2022.06.10"))
            .andExpect(jsonPath("$.endDate").value("2022.06.10"))
            .andExpect(jsonPath("$.winnerImage.url").value("winnerImageUrl"));
    }

    @Test
    @DisplayName("참여작 페이징 처리")
    void awardParticipateList() throws Exception {
        final List<EndPhotoImgUrl> normalImageUrlList = List.of(new EndPhotoImgUrl("normalImageUrl"));

        when(eventFacade.awardParticipateList(anyInt(), any()))
            .thenReturn(new AwardParticipateResponse(new PageImpl<>(normalImageUrlList)));

        mockMvc.perform(getMapping("/api/pood/v1-0/event/photo-awards/join/" + 1))
            .andExpect(ok())
            .andExpect(jsonPath("$.normalImageList.content[0].url").value("normalImageUrl"));
    }

}