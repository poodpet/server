package com.pood.server.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pood.server.controller.request.userinfo.UserLoginRequest;
import com.pood.server.controller.request.userinfo.UserLoginResponseDto;
import com.pood.server.entity.DeviceType;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserToken;
import com.pood.server.facade.UserFacade;
import com.pood.server.util.UserStatus;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;

@WebMvcTest(LoginController.class)
class LoginControllerTest extends ControllerTests {

    @MockBean
    private UserFacade userFacade;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void test() throws Exception {
        doNothing().when(userFacade).checkAuthentication(anyString(), anyString());

        mockMvc.perform(get("/api/pood/v1-0/login/sms/auth-check")
                .param("phoneNumber", "01012345678")
                .param("authNumber", "1111"))
            .andExpect(ok());
    }

    @Test
    void loginTest() throws Exception {
        //given
        final UserLoginRequest userLoginRequest = new UserLoginRequest("test@email.com", "qqqq1111", 1, DeviceType.IOS);
        final UserInfo userInfo = new UserInfo(
            1,
            "82f44cf0-0b9e-44e4-801a-64772e485c2d",
            "nickName",
            "email",
            "password",
            1,
            UserStatus.ACTIVE,
            "name",
            1000,
            "phone",
            "referralCode",
            true,
            LocalDateTime.now(),
            true, LocalDateTime.now(),
            true, LocalDateTime.now(),
            "key", 1, "tokenArn",
            "mlName", 0.0, 1,
            1, LocalDateTime.now(),
            LocalDateTime.now(), LocalDateTime.now(),
            null, false, null,
            true,null,
            true,null
        );

        final String token = "1fb1ce26-f909-4aeb-81ed-69b2d290356f";
        final UserLoginResponseDto userLoginResponseDto = UserLoginResponseDto.toResponse(userInfo,
            new UserToken(1, "6d93780f-b1b8-4f8b-a04c-3c6f0a969abd", token, LocalDateTime.now(), LocalDateTime.now()),
            1);

        //when
        Mockito.when(userFacade.login(any()))
            .thenReturn(userLoginResponseDto);
        final ResultActions result = mockMvc.perform(post("/api/pood/v1-0/login")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(objectMapper.writeValueAsString(userLoginRequest)));

        //then
        result
            .andExpect(ok())
            .andExpect(jsonPath("$.token").value(token));
    }

}