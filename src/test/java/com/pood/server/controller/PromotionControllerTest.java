package com.pood.server.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import com.pood.server.dto.meta.promotion.RunningPromotion;
import com.pood.server.facade.PromotionFacade;
import com.pood.server.service.PromotionServiceV2;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.ResultActions;

@WebMvcTest(PromotionController.class)
class PromotionControllerTest extends ControllerTests {

    @MockBean
    private PromotionServiceV2 promotionServiceV2;

    @MockBean
    private PromotionFacade promotionFacade;

    @Test
    @DisplayName("진행중인 프로모션 전체 조회 pcIdx 값 3 이상일 경우 에러")
    void pcIdxMaxException() throws Exception {
        //given
        final int pcIdx = 3;

        //when
        final ResultActions resultActions = mockMvc.perform(getMapping("/api/pood/v1-0/promotion/all/" + pcIdx))
            .andExpect(badRequest());

        //then
        resultActions
            .andExpect(jsonPath("$.error").value("Bad Request"))
            .andExpect(jsonPath("$.message").value("펫 카테고리는 3 이상일 수 없습니다."));
    }

    @Test
    @DisplayName("진행중인 프로모션 전체 조회 pcIdx 값 0 이하 경우 에러")
    void pcIdxMinException() throws Exception {
        //given
        final int pcIdx = 0;

        //when
        final ResultActions resultActions = mockMvc.perform(getMapping("/api/pood/v1-0/promotion/all/" + pcIdx))
            .andExpect(badRequest());

        //then
        resultActions
            .andExpect(jsonPath("$.error").value("Bad Request"))
            .andExpect(jsonPath("$.message").value("펫 카테고리는 0 이하일 수 없습니다."));
    }

    @Test
    @DisplayName("진행중인 프로모션 전체 조회")
    void proceedPromotion() throws Exception {
        //given
        final int pcIdx = 1;
        final List<RunningPromotion> expected = List.of(
            new RunningPromotion(125, "imageUrl", 2, "name2", "PROMOTION",
                LocalDateTime.now(), LocalDateTime.now(), "details"),
            new RunningPromotion(123, "imageUrl", 1, "name1", "PROMOTION",
                LocalDateTime.now(), LocalDateTime.now(), "details"),
            new RunningPromotion(124, "imageUrl", 0, "name0", "PROMOTION",
                LocalDateTime.now(), LocalDateTime.now(), "details")
        );
        //when
        BDDMockito.given(promotionFacade.proceedingPromotion(pcIdx))
            .willReturn(expected);

        final ResultActions resultActions = mockMvc.perform(getMapping("/api/pood/v1-0/promotion/all/" + pcIdx))
            .andExpect(ok());

        //then
        resultActions.andExpect(jsonPath("$.promotionList[0].innerTitle").value("name2"))
            .andExpect(jsonPath("$.promotionList[1].innerTitle").value("name1"))
            .andExpect(jsonPath("$.promotionList[2].innerTitle").value("name0"));
    }
}