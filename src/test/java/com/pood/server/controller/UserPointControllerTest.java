package com.pood.server.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.pood.server.controller.user.UserPointController;
import com.pood.server.dto.user.point.PointInfoResponse;
import com.pood.server.facade.UserPointFacade;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.ResultActions;

@WebMvcTest(UserPointController.class)
class UserPointControllerTest extends ControllerTests {

    @MockBean
    private UserPointFacade userPointFacade;

    @Test
    void userPointInfo() throws Exception {
        //given
        final String token = "36671b45-7378-4188-9e17-a7f6585a1990";
        given(userPointFacade.userPointInfoAll(any()))
            .willReturn(PointInfoResponse.of(
                7000,
                15000,
                5000,
                2000
            ));

        //when
        final ResultActions request = mockMvc.perform(get("/api/pood/v1-0/point")
            .header("token", token));

        //then
        request
            .andExpect(status().isOk())
            .andExpect(jsonPath("$..presentPoint").value(7000))
            .andExpect(jsonPath("$..receivedBenefitPoint").value(15000))
            .andExpect(jsonPath("$..willBeRewardPoint").value(5000))
            .andExpect(jsonPath("$..removedPoint").value(2000));
    }
}