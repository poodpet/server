package com.pood.server.controller;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.pood.server.config.AopConfiguration;
import com.pood.server.config.ControllerTokenAspect;
import com.pood.server.controller.request.userwish.UserWishCreateDto;
import com.pood.server.controller.user.UserWishController;
import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.facade.UserFacade;
import com.pood.server.service.UserSeparate;
import com.pood.server.util.UserStatus;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;

@WebMvcTest(UserWishController.class)
@Import({AopConfiguration.class, ControllerTokenAspect.class})
class UserWishControllerTest extends ControllerTests {

    @MockBean
    private UserFacade userFacade;

    @MockBean
    private UserSeparate userSeparate;

    @Test
    @DisplayName("유저 찜 정보 저장")
    void saveTest() throws Exception {
        final UserWishCreateDto userWishCreateDto = new UserWishCreateDto(123);
        final String content = objectMapper.writeValueAsString(userWishCreateDto);
        final String token = "873bd97f-9737-43cc-b3ac-bb2c9acff352";

        doCallRealMethod().when(userFacade).saveUserWish(token, userWishCreateDto);

        mockMvc.perform(post("/api/pood/v1-0/wish")
                .header("token", token)
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());
    }

    @Test
    @DisplayName("유저 찜 정보 있는경우 조회")
    void wishValidationTrueTest() throws Exception {
        //given
        final int goodsIdx = 586;

        final UserInfo userInfo = new UserInfo(
            1,
            "82f44cf0-0b9e-44e4-801a-64772e485c2d",
            "nickName",
            "email",
            "password",
            1,
            UserStatus.ACTIVE,
            "name",
            1000,
            "phone",
            "referralCode",
            true,
            LocalDateTime.now(),
            true, LocalDateTime.now(),
            true, LocalDateTime.now(),
            "key", 1, "tokenArn",
            "mlName", 0.0, 1,
            1, LocalDateTime.now(),
            LocalDateTime.now(), LocalDateTime.now(),
            null, false, null,
            false, null,
            false, null
        );

        when(userSeparate.getOptionalUserByToken(anyString())).thenReturn(userInfo);

        //when
        when(userFacade.userWishValidation(userInfo, goodsIdx))
            .thenReturn(true);

        //then
        mockMvc.perform(get("/api/pood/v1-0/wish/" + goodsIdx)
                .header("token", "c8a119b7-ae55-4f8e-a792-8bf6e6217f3d"))
            .andExpect(jsonPath("$..isWished").value(true))
            .andExpect(status().isOk());
    }

    @Test
    @DisplayName("유저 찜 정보 없는 경우 조회")
    void wishValidationFalseTest() throws Exception {
        //given
        final String token = "8cc1b1f2-61fe-4bca-90f8-ea1c57d8f025";
        final int goodsIdx = 41;

        UserInfo userInfo = new UserInfo(
            1,
            "82f44cf0-0b9e-44e4-801a-64772e485c2d",
            "nickName",
            "email",
            "password",
            1,
            UserStatus.ACTIVE,
            "name",
            1000,
            "phone",
            "referralCode",
            true,
            LocalDateTime.now(),
            true, LocalDateTime.now(),
            true, LocalDateTime.now(),
            "key", 1, "tokenArn",
            "mlName", 0.0, 1,
            1, LocalDateTime.now(),
            LocalDateTime.now(), LocalDateTime.now(),
            null, false, null,
            false, null,
            false, null
        );

        //when
        when(userFacade.userWishValidation(userInfo, goodsIdx))
            .thenReturn(false);

        //then
        mockMvc.perform(get("/api/pood/v1-0/wish/" + goodsIdx)
                .header("token", "c8a119b7-ae55-4f8e-a792-8bf6e6217f3d"))
            .andExpect(jsonPath("$..isWished").value(false))
            .andExpect(status().isOk());
    }

    @Test
    @DisplayName("유저 찜 삭제")
    void deleteUserWish() throws Exception {
        //given
        final String token = "c567844b-b8a9-4eba-bd5d-be419edc1eb7";
        final int goodsIdx = 969;

        //when
        doNothing().when(userFacade).deleteUserWish(token, goodsIdx);

        //then
        mockMvc.perform(delete("/api/pood/v1-0/wish/" + goodsIdx)
                .header("token", token))
            .andExpect(status().isOk());

    }

    @Test
    @DisplayName("유저 찜 전체 조회")
    void findAllWishList() throws Exception {
        //given
        final String token = "c567844b-b8a9-4eba-bd5d-be419edc1eb7";
        final SortedGoodsList sortedGoodsList = SortedGoodsList.builder()
            .idx(1)
            .pcIdx(1)
            .goodsTypeIdx(29)
            .displayType("a")
            .goodsName("로우즈 밀프리 하이프로틴 캣 연어 790g")
            .goodsOriginPrice(32000)
            .goodsPrice(32000)
            .discountRate(0)
            .discountPrice(0)
            .saleStatus(1)
            .visible(0)
            .mainProduct(119)
            .averageRating(0.0)
            .reviewCnt(0L)
            .updatetime(LocalDateTime.now())
            .recordbirth(LocalDateTime.now())
            .mainImage(
                "https://pood-bucket.s3.ap-northeast-2.amazonaws.com/product/20210526104117041-9c1521eb-645d-4349-a396-844695254a79.jpeg")
            .promotionInfo(null)
            .isRecommend(null)
            .build();

        //when
        when(userFacade.getMyGoodsWishList(token))
            .thenReturn(List.of(sortedGoodsList));

        //then
        mockMvc.perform(get("/api/pood/v1-0/wish")
                .header("token", token))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].idx").value(1))
            .andExpect(jsonPath("$[0].pcIdx").value(1))
            .andExpect(jsonPath("$[0].goodsTypeIdx").value(29))
            .andExpect(jsonPath("$[0].displayType").value("a"))
            .andExpect(jsonPath("$[0].goodsName").value("로우즈 밀프리 하이프로틴 캣 연어 790g"))
            .andExpect(jsonPath("$[0].goodsOriginPrice").value(32000))
            .andExpect(jsonPath("$[0].goodsPrice").value(32000))
            .andExpect(jsonPath("$[0].discountRate").value(0))
            .andExpect(jsonPath("$[0].discountPrice").value(0))
            .andExpect(jsonPath("$[0].saleStatus").value(1))
            .andExpect(jsonPath("$[0].visible").value(0))
            .andExpect(jsonPath("$[0].mainProduct").value(119))
            .andExpect(jsonPath("$[0].averageRating").value(0.0))
            .andExpect(jsonPath("$[0].reviewCnt").value(0))
            .andExpect(jsonPath("$[0].mainImage").value(
                "https://pood-bucket.s3.ap-northeast-2.amazonaws.com/product/20210526104117041-9c1521eb-645d-4349-a396-844695254a79.jpeg"));

    }
}