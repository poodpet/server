package com.pood.server.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.pood.server.controller.request.userpet.PetWorryRequest;
import com.pood.server.controller.request.userpet.UserPetCreateDto;
import com.pood.server.controller.response.pet.AllergyDataResponse;
import com.pood.server.controller.response.pet.GrainSizeResponse;
import com.pood.server.controller.response.pet.UserPetCreateResponse;
import com.pood.server.controller.response.pet.UserPetInfoResponse;
import com.pood.server.controller.user.UserPetController;
import com.pood.server.dto.meta.pet.PetWorryGroup;
import com.pood.server.dto.meta.worry.PetWorryReadDto;
import com.pood.server.entity.group.UserPetWeightDiaryGroup;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.entity.user.UserPetWeightDiary;
import com.pood.server.facade.UserPetFacade;
import com.pood.server.facade.coupon.response.CouponCreateResponse;
import com.pood.server.facade.user.pet.request.BodyShapeManageCreateRequest;
import com.pood.server.facade.user.pet.request.WeightUpdateRequest;
import com.pood.server.util.UserStatus;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.ResultActions;

@WebMvcTest(UserPetController.class)
class UserPetControllerTest extends ControllerTests {

    @MockBean
    private UserPetFacade userPetFacade;

    private static final String GLOBAL_REQUEST_MAPPING = "/api/pood/v1-0/pet";

    @Test
    @DisplayName("유저 펫 생성")
    void create() throws Exception {
        //given
        final String content = objectMapper.writeValueAsString(
            new UserPetCreateDto(2, 189, 0, "petName",
                LocalDate.of(2019, 9, 10), 4, 3.5f, 0,
                List.of(new PetWorryRequest(1L, 1),
                    new PetWorryRequest(1L, 1),
                    new PetWorryRequest(1L, 1)),
                List.of(0, 1), List.of(0, 1, 2), 0));

        final MockMultipartFile userPetImage = new MockMultipartFile("file", "testFile.jpeg",
            "image/jpeg",
            "hello Test".getBytes());

        final MockMultipartFile userPetCreateDto = new MockMultipartFile("request",
            "testCreateDto", MediaType.APPLICATION_JSON_VALUE, content.getBytes());

        given(userPetFacade.createUserPet(any(), any(), any()))
            .willReturn(UserPetCreateResponse.of("펫이름", "22-101111",
                CouponCreateResponse.of(1000, 10000, 30, 1)));

        //when
        final ResultActions result = mockMvc.perform(
            multipart(GLOBAL_REQUEST_MAPPING + "/create")
                .file(userPetImage)
                .file(userPetCreateDto)
                .contentType(MediaType.APPLICATION_JSON)
                .header("token", "ba386d82-2f41-4809-97ed-1b616878e080"));

        //then
        result.andExpect(status().isCreated())
            .andExpect(jsonPath("$.petName").value("펫이름"));
    }

    @Test
    @DisplayName("유저 펫 조회")
    void info() throws Exception {
        //given
        final String token = "fb562d34-19aa-4841-89e0-8dbe8ef28f2d";

        List<UserPetInfoResponse> userPetInfoResponses = List.of(UserPetInfoResponse.builder()
            .idx(1)
            .petGender(1)
            .userIdx(1)
            .pcId(1)
            .petName("petName")
            .recordbirth(LocalDateTime.now())
            .build());

        UserInfo userInfo = new UserInfo(
            1,
            "82f44cf0-0b9e-44e4-801a-64772e485c2d",
            "nickName",
            "email",
            "password",
            1,
            UserStatus.ACTIVE,
            "name",
            1000,
            "phone",
            "referralCode",
            true,
            LocalDateTime.now(),
            true, LocalDateTime.now(),
            true, LocalDateTime.now(),
            "key", 1, "tokenArn",
            "mlName", 0.0, 1,
            1, LocalDateTime.now(),
            LocalDateTime.now(), LocalDateTime.now(),
            null, false, null,
            false, null, false, null
        );

        given(userPetFacade.userPetInfoList(userInfo))
            .willReturn(userPetInfoResponses);

        //when
        final ResultActions result = mockMvc.perform(get(GLOBAL_REQUEST_MAPPING)
            .header("token", token));

        //then
        result.andExpect(status().isOk());
    }

    @Test
    @DisplayName("유저 펫 삭제")
    void deletePet() throws Exception {
        //given
        final int userPetIdx = 1;
        final String token = "fb562d34-19aa-4841-89e0-8dbe8ef28f2d";

        //when
        final ResultActions result = mockMvc.perform(
            delete(GLOBAL_REQUEST_MAPPING + "/{userPetIdx}", userPetIdx)
                .header("token", token));

        //then
        result.andExpect(status().isOk());
    }

    @Test
    @DisplayName("알갱이 사이즈 조회")
    void findAllergy() throws Exception {
        given(userPetFacade.findAllergyDataList())
            .willReturn(List.of(
                new AllergyDataResponse(1, "name", "url"),
                new AllergyDataResponse(2, "name1", "url2"),
                new AllergyDataResponse(3, "name2", "url3"),
                new AllergyDataResponse(4, "name3", "url4")
            ));

        mockMvc.perform(get(GLOBAL_REQUEST_MAPPING + "/allergy"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].idx").value(1));
    }

    @Test
    @DisplayName("알갱이 사이즈 조회")
    void findGrainSize() throws Exception {
        given(userPetFacade.findGrainSizeDataList())
            .willReturn(List.of(
                GrainSizeResponse.of(1, "title", "url"),
                GrainSizeResponse.of(2, "타이틀", "url2"),
                GrainSizeResponse.of(3, "테스트", "url3")
            ));

        mockMvc.perform(get(GLOBAL_REQUEST_MAPPING + "/grainsize"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].idx").value(1));
    }

    @Test
    @DisplayName("고민거리 리스트 조회")
    void petWorryList() throws Exception {
        final int petCategoryIdx = 1;
        given(userPetFacade.findWorry(anyInt()))
            .willReturn(new PetWorryGroup(
                List.of(new PetWorryReadDto(1L, null, "dogUrl", "catUrl"))
            ));

        mockMvc.perform(get(GLOBAL_REQUEST_MAPPING + "/worry/" + petCategoryIdx))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].idx").value(1));
    }

    @Test
    @DisplayName("유저 펫의 무게 기록을 조회")
    void findWeightDiaryTest() throws Exception {
        //given
        final UserPetWeightDiaryGroup expectedGroup = new UserPetWeightDiaryGroup(
            List.of(
                new UserPetWeightDiary(1L, null, 3.5f, LocalDate.now(), LocalDateTime.now(),
                    LocalDateTime.of(2022, 5, 4, 0, 0, 0)),
                new UserPetWeightDiary(2L, null, 4.0f, LocalDate.now(), LocalDateTime.now(),
                    LocalDateTime.of(2022, 5, 5, 0, 0, 0)),
                new UserPetWeightDiary(3L, null, 4.5f, LocalDate.now(), LocalDateTime.now(),
                    LocalDateTime.of(2022, 5, 6, 0, 0, 0))
            )
        );

        //when
        when(userPetFacade.findWeightDiary(anyInt(), any()))
            .thenReturn(expectedGroup);

        //then
        mockMvc.perform(get(GLOBAL_REQUEST_MAPPING + "/weight/" + 1))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].idx").value(1))
            .andExpect(jsonPath("$[0].recordBirth").value("2022.05.04"))
            .andExpect(jsonPath("$[1].idx").value(2))
            .andExpect(jsonPath("$[1].recordBirth").value("2022.05.05"))
            .andExpect(jsonPath("$[2].idx").value(3))
            .andExpect(jsonPath("$[2].recordBirth").value("2022.05.06"));
    }

    @Test
    @DisplayName("(성공) 유저 체형 관리 생성")
    void success_createBodyShapeInfo() throws Exception {

        BodyShapeManageCreateRequest request = new BodyShapeManageCreateRequest(1, 5.4f, "A");

        mockMvc.perform(post(GLOBAL_REQUEST_MAPPING + "/body-shape-management")
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());

    }

    @ParameterizedTest
    @NullSource
    @DisplayName("(실패) 유저 체형 관리 생성 - 체형 Null Check")
    void fail_createBodyShapeInfo_bodyNull(final String body) throws Exception {

        BodyShapeManageCreateRequest request = new BodyShapeManageCreateRequest( 1, 5.4f, body);

        mockMvc.perform(post(GLOBAL_REQUEST_MAPPING + "/body-shape-management")
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());

    }

    @Test
    @DisplayName("체중관리 등록 시, 유저 펫 무게는 0보다 커야한다 (성공)")
    void positiveUserPetWeightRequestForBodyManagement_success() throws Exception {

        BodyShapeManageCreateRequest request = new BodyShapeManageCreateRequest(1, 0.1f, "A");

        mockMvc.perform(postMapping(GLOBAL_REQUEST_MAPPING + "/body-shape-management")
            .content(objectMapper.writeValueAsString(request))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());
    }

    @ParameterizedTest
    @ValueSource(floats = {0f, -0.1f})
    @DisplayName("체중관리 등록 시, 유저 펫 무게는 0보다 커야한다 (실패)")
    void positiveUserPetWeightRequestForBodyManagement_fail(float weight) throws Exception {

        BodyShapeManageCreateRequest request = new BodyShapeManageCreateRequest(1, weight, "A");

        mockMvc.perform(postMapping(GLOBAL_REQUEST_MAPPING + "/body-shape-management")
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }
    @Test
    @DisplayName("유저 펫 무게는 0보다 커야한다 (성공)")
    void positiveUserPetWeightRequest_success() throws Exception {

        WeightUpdateRequest request = new WeightUpdateRequest(1L, 1, LocalDate.now(), 1, 0.1f);

        mockMvc.perform(putMapping(GLOBAL_REQUEST_MAPPING + "/weight")
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @ParameterizedTest
    @ValueSource(floats = {0f, -0.1f})
    @DisplayName("유저 펫 무게는 0보다 커야한다 (실패)")
    void positiveUserPetWeightRequest_fail(float weight) throws Exception {

        WeightUpdateRequest request = new WeightUpdateRequest(1L, 1, LocalDate.now(), 1, weight);

        mockMvc.perform(putMapping(GLOBAL_REQUEST_MAPPING + "/weight")
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }
}