package com.pood.server.controller.request.userpet;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.controller.request.RequestDtoValidationTests;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserPetUpdateRequestTest extends RequestDtoValidationTests {

    @Test
    @DisplayName("펫 이름 없을 때 예외처리")
    void petNameValidation() {
        //given
        UserPetUpdateRequest userPetUpdateRequest = new UserPetUpdateRequest(1, null, 1, List.of(new PetWorryRequest(1L, 1),
            new PetWorryRequest(2L, 2),
            new PetWorryRequest(3L, 3)), Set.of(1, 2),
            Set.of(1, 2, 3));

        //when
        final List<ConstraintViolation<UserPetUpdateRequest>> validate = new ArrayList<>(
            validator.validate(
                userPetUpdateRequest));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("펫이름이 없습니다.");
    }

    @Test
    @DisplayName("고민 목록이 3개 이상일 경우")
    void aiDiagnosisSizeTest() {
        //given
        UserPetUpdateRequest userPetUpdateRequest = new UserPetUpdateRequest(1, "미미", 1, List.of(new PetWorryRequest(1L, 1),
            new PetWorryRequest(2L, 2),
            new PetWorryRequest(3L, 3),
            new PetWorryRequest(4L, 4)),
            Set.of(1, 2),
            Set.of(1, 2, 3));

        //when
        final List<ConstraintViolation<UserPetUpdateRequest>> validate = new ArrayList<>(
            validator.validate(
                userPetUpdateRequest));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("고민은 1가지에서 3가지가 존재해야 합니다.");
    }

    @Test
    @DisplayName("고민 목록이 null 인 경우")
    void aiDiagnosisNullTest() {
        //given
        UserPetUpdateRequest userPetUpdateRequest = new UserPetUpdateRequest(1, "미미", 1, null,
            Set.of(1, 2),
            Set.of(1, 2, 3));

        //when
        final List<ConstraintViolation<UserPetUpdateRequest>> validate = new ArrayList<>(
            validator.validate(
                userPetUpdateRequest));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("고민은 최소 1개 있어야 합니다.");
    }

    @Test
    @DisplayName("고민 목록이 빈배열 인 경우")
    void aiDiagnosisZeroTest() {
        //given
        UserPetUpdateRequest userPetUpdateRequest = new UserPetUpdateRequest(1, "미미", 1, List.of(),
            Set.of(1, 2),
            Set.of(1, 2, 3));

        //when
        final List<ConstraintViolation<UserPetUpdateRequest>> validate = new ArrayList<>(
            validator.validate(
                userPetUpdateRequest));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("고민은 1가지에서 3가지가 존재해야 합니다.");
    }

    @Test
    @DisplayName("알러지 목록이 3개 이상일 경우")
    void allergySizeTest() {
        //given
        final UserPetUpdateRequest userPetUpdateRequest = new UserPetUpdateRequest(1, "미미", 1, List.of(new PetWorryRequest(1L, 1),
            new PetWorryRequest(2L, 2),
            new PetWorryRequest(3L, 3)),
            Set.of(1, 2, 3, 4),
            Set.of(1, 2, 3));

        //when
        final List<ConstraintViolation<UserPetUpdateRequest>> validate = new ArrayList<>(
            validator.validate(
                userPetUpdateRequest));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("알러지는 3가지가 최대입니다.");
    }

    @Test
    @DisplayName("알갱이 사이즈 목록이 3개 이상일 경우")
    void grainSizeTest() {
        //given
        final UserPetUpdateRequest userPetUpdateRequest = new UserPetUpdateRequest(1, "미미", 1, List.of(new PetWorryRequest(1L, 1),
            new PetWorryRequest(2L, 2),
            new PetWorryRequest(3L, 3)),
            Set.of(1, 2, 3),
            Set.of(1, 2, 3, 5));

        //when
        final List<ConstraintViolation<UserPetUpdateRequest>> validate = new ArrayList<>(
            validator.validate(
                userPetUpdateRequest));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("알갱이 사이즈는 3가지가 최대입니다.");
    }
}