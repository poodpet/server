package com.pood.server.controller.request.userwish;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class UserWishCreateDtoTest {

    @Test
    void test() {
        UserWishCreateDto userWishCreateDto = new UserWishCreateDto(2);

        assertThat(userWishCreateDto.getGoodsIdx()).isEqualTo(2);
    }
}