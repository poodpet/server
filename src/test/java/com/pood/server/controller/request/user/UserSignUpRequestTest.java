package com.pood.server.controller.request.user;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.controller.request.RequestDtoValidationTests;
import com.pood.server.entity.user.UserInfo;
import java.util.ArrayList;
import java.util.List;
import javax.validation.ConstraintViolation;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserSignUpRequestTest extends RequestDtoValidationTests {

    @Test
    @DisplayName("로그인 타입이 null 일 경우 예외 테스트")
    void loginTypeTest() {
        UserSignUpRequest request = new UserSignUpRequest(null, "email@test.com", null, "key",
            null, true, true, true, true, true, "01065303466", "123412");

        final List<ConstraintViolation<UserSignUpRequest>> constraintViolations = new ArrayList<>(
            validator.validate(request));

        assertThat(constraintViolations.get(0).getMessage())
            .isEqualTo("로그인 타입은 비어있거나 null일 수 없습니다.");
    }

    @Test
    @DisplayName("이메일 형식이 맞지않을 경우")
    void emailTest() {
        UserSignUpRequest request = new UserSignUpRequest(5, "email@123123123.com", null, "key",
            null, true, true, true, true, true, "01065303466", "123412");

        final List<ConstraintViolation<UserSignUpRequest>> constraintViolations = new ArrayList<>(
            validator.validate(request));

        assertThat(constraintViolations.get(0).getMessage())
            .isEqualTo("이메일 주소 형식이 잘못되었습니다.");
    }

    @Test
    @DisplayName("이메일 null인 경우")
    void emailNullTest() {
        UserSignUpRequest request = new UserSignUpRequest(5, null, null, "key", null, true,
            true, true, true, true, "01065303466", "123412");

        final List<ConstraintViolation<UserSignUpRequest>> constraintViolations = new ArrayList<>(
            validator.validate(request));

        assertThat(constraintViolations.get(0).getMessage())
            .isEqualTo("이메일 주소가 null일 수 없습니다.");
    }

    @Test
    @DisplayName("이메일이 빈 문자열일 경우")
    void emailEmptyTest() {
        UserSignUpRequest request = new UserSignUpRequest(5, "", null, "key", null, true,
            true, true, true, true, "01065303466", "123412");

        final List<ConstraintViolation<UserSignUpRequest>> constraintViolations = new ArrayList<>(
            validator.validate(request));

        assertThat(constraintViolations.get(0).getMessage())
            .isEqualTo("이메일 주소 형식이 잘못되었습니다.");
    }

    @Test
    @DisplayName("핸드폰 번호가 null인 경우")
    void phoneNumberNullTest() {
        UserSignUpRequest request = new UserSignUpRequest(5, "email@test.com", null, "key", null,
            true, true, true, true, true, null, "123412");

        final List<ConstraintViolation<UserSignUpRequest>> constraintViolations = new ArrayList<>(
            validator.validate(request));

        assertThat(constraintViolations.get(0).getMessage())
            .isEqualTo("휴대폰 번호는 비어있거나 null일 수 없습니다.");
    }

    @Test
    @DisplayName("핸드폰 번호가 빈 문자열일 경우")
    void phoneNumberEmptyTest() {
        UserSignUpRequest request = new UserSignUpRequest(5, "email@test.com", null, "key", null,
            true, true, true, true, true, "", "123412");

        final List<ConstraintViolation<UserSignUpRequest>> constraintViolations = new ArrayList<>(
            validator.validate(request));

        assertThat(constraintViolations.get(0).getMessage())
            .isEqualTo("휴대폰 번호는 비어있거나 null일 수 없습니다.");
    }

    @Test
    @DisplayName("유저 이름이 null 일 경우")
    void userNameNullTest() {
        UserSignUpRequest request = new UserSignUpRequest(5, "email@test.com", null, "key", null,
            true, true, true, true, true, "01065303466", null);

        final List<ConstraintViolation<UserSignUpRequest>> constraintViolations = new ArrayList<>(
            validator.validate(request));

        assertThat(constraintViolations.get(0).getMessage())
            .isEqualTo("유저 이름은 비어있거나 null일 수 없습니다.");
    }

    @Test
    @DisplayName("유저 이름이 빈 문자열일 경우")
    void userNameEmptyTest() {
        UserSignUpRequest request = new UserSignUpRequest(5, "email@test.com", null, "key", null,
            true, true, true, true, true, "01065303466", "");

        final List<ConstraintViolation<UserSignUpRequest>> constraintViolations = new ArrayList<>(
            validator.validate(request));

        assertThat(constraintViolations.get(0).getMessage())
            .isEqualTo("유저 이름은 비어있거나 null일 수 없습니다.");
    }

    @Test
    @DisplayName("유저 이름으로 닉네임이 설정되는지 확인")
    void userNameSetNickNameTest() {
        UserSignUpRequest request = new UserSignUpRequest(5, "email@test.com", null, "key", null,
            true, true, true, true, true, "01065303466", "test");

        final UserInfo userInfo = request.toEntity(null, null);

        assertThat(userInfo.getUserNickname())
            .isEqualTo("test");
    }
}