package com.pood.server.controller.request.userinfo;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.controller.request.RequestDtoValidationTests;
import com.pood.server.entity.DeviceType;
import java.util.ArrayList;
import java.util.List;
import javax.validation.ConstraintViolation;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserLoginRequestTest extends RequestDtoValidationTests {

    @Test
    @DisplayName("로그인 유형 에러 검증")
    void validTest() {
        UserLoginRequest userLoginRequest = new UserLoginRequest("email", "123", null, DeviceType.AOS);

        final List<ConstraintViolation<UserLoginRequest>> validate = new ArrayList<>(
            validator.validate(userLoginRequest));

        //then
        if (!validate.isEmpty()) {
            assertThat(validate.get(0).getMessage()).isEqualTo("로그인 유형은 필수 값 입니다.");
        }
    }

    @Test
    @DisplayName("기기 접속정보 유형 에러 검증")
    void osTypeExceptionTest() {
        UserLoginRequest userLoginRequest = new UserLoginRequest("email", "123", 1, null);

        final List<ConstraintViolation<UserLoginRequest>> validate = new ArrayList<>(
            validator.validate(userLoginRequest));

        //then
        if (!validate.isEmpty()) {
            assertThat(validate.get(0).getMessage()).isEqualTo("접속기기 유형은 필수 값 입니다.");
        }
    }
}