package com.pood.server.controller.request.report;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.controller.request.RequestDtoValidationTests;
import java.util.ArrayList;
import java.util.List;
import javax.validation.ConstraintViolation;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserReportSaveRequestTest extends RequestDtoValidationTests {

    @Test
    @DisplayName("유저 idx null 유효성 검증")
    void userInfoIdxNullTest() {
        final UserReportSaveRequest userReportSaveRequest = new UserReportSaveRequest(null, 1, "asdasd");
        final List<ConstraintViolation<UserReportSaveRequest>> validate = new ArrayList<>(
            validator.validate(userReportSaveRequest));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("유저의 idx는 null일 수 없습니다.");
    }

    @Test
    @DisplayName("이벤트 idx null 유효성 검증")
    void eventInfoIdxTest() {
        final UserReportSaveRequest userReportSaveRequest = new UserReportSaveRequest(1, null, "asdasd");
        final List<ConstraintViolation<UserReportSaveRequest>> validate = new ArrayList<>(
            validator.validate(userReportSaveRequest));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("이벤트 idx는 null일 수 없습니다.");
    }

    @Test
    @DisplayName("신고 메세지 null 유효성 검증")
    void reportMessageNullTest() {
        final UserReportSaveRequest userReportSaveRequest = new UserReportSaveRequest(1, 1, null);
        final List<ConstraintViolation<UserReportSaveRequest>> validate = new ArrayList<>(
            validator.validate(userReportSaveRequest));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("신고 메세지는 null 이거나 빈 문자열일 수 없습니다.");
    }

    @Test
    @DisplayName("신고 메세지 빈 문자열 유효성 검증")
    void reportMessageEmptyTest() {
        final UserReportSaveRequest userReportSaveRequest = new UserReportSaveRequest(1, 1, "");
        final List<ConstraintViolation<UserReportSaveRequest>> validate = new ArrayList<>(
            validator.validate(userReportSaveRequest));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("신고 메세지는 null 이거나 빈 문자열일 수 없습니다.");
    }
}