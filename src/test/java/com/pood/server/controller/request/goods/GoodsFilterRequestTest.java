package com.pood.server.controller.request.goods;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.controller.request.RequestDtoValidationTests;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class GoodsFilterRequestTest extends RequestDtoValidationTests {

    @Test
    @DisplayName("필터는 건강특징 사이즈는 최소 1이어야 한다.")
    void worryMinimumCannotBeEmpty() {
        final List<ConstraintViolation<GoodsFilterRequest>> validate =
            new ArrayList<>(validator.validate(GoodsFilterRequest.of(Collections.emptyList()
                , 1L, null,
                null, null, null, 1, null)));

        isEqualToMessage(validate, "고민거리는 최소 1가지 선택해야 합니다.");
    }


    @Test
    @DisplayName("건강 특징 사이즈는 null(전체)이 될 수 있다.")
    void worryCanBeNull() {
        final Set<ConstraintViolation<GoodsFilterRequest>> validate = validator.validate(GoodsFilterRequest.of(null, 1L, null,
            null, null, null, 1, null));
        isEmpty(validate);
    }

    @Test
    @DisplayName("굿즈 대분류 카테고리가 필수 값이다.")
    void goodsCategoryCannotBeNull() {
        final List<ConstraintViolation<GoodsFilterRequest>> validate = new ArrayList<>(
            validator.validate(GoodsFilterRequest.of(null, null, null,
                null, null, null, 1, null)));
        isEqualToMessage(validate, "대분류 카테고리가 없습니다.");
    }

    @Test
    @DisplayName("단백질은 최소 1가지 선택해야 한다.")
    void proteinCanBeEmpty() {
        //given
        final List<ConstraintViolation<GoodsFilterRequest>> validate = new ArrayList<>(
            validator.validate(GoodsFilterRequest.of(null, 1L, null,
                Collections.emptyList(), null, null, 1, null)));
        //when, then
        isEqualToMessage(validate, "단백질은 최소 1가지 선택해야 합니다.");
    }

    @Test
    @DisplayName("알갱이는 최소 1가지 선택해야 한다.")
    void grainSizeCannotBeEmpty() {
        //given
        final List<ConstraintViolation<GoodsFilterRequest>> validate = new ArrayList<>(
            validator.validate(GoodsFilterRequest.of(null, 1L, null,
                null, Collections.emptyList(), null, 1, null)));
        //when, then
        isEqualToMessage(validate, "알갱이는 최소 1가지 선택해야 합니다.");
    }

    @Test
    @DisplayName("체구 리스트는 1가지 선택해야 합니다.")
    void feedTargetCannotBeEmpty() {
        //given
        final List<ConstraintViolation<GoodsFilterRequest>> validate = new ArrayList<>(
            validator.validate(GoodsFilterRequest.of(null, 1L, null,
                null, null, Collections.emptyList(), 1, null)));
        //when, then
        isEqualToMessage(validate, "체구 리스트는 1가지 선택해야 합니다.");
    }

    @Test
    @DisplayName("반려견 체구 프리사이즈 추가")
    void addFreeSizeTest() {
        final GoodsFilterRequest request = GoodsFilterRequest.of(null, 1L, null,
            Collections.emptyList(), null, new ArrayList<>(), 1, null);
        request.addFreeSize();
        Assertions.assertThat(request.getFeedTargetList()).contains(0);
    }

    @Test
    @DisplayName("펫 카테고리는 필수 값")
    void petCategoryCannotBeNull() {
        //given
        final List<ConstraintViolation<GoodsFilterRequest>> validate = new ArrayList<>(
            validator.validate(GoodsFilterRequest.of(null, 1L, null,
                null, null, null, null, null)));
        //when, then
        isEqualToMessage(validate, "펫 카테고리는 필수 값 입니다.");
    }

    @Test
    @DisplayName("펫 연령대는 필수 값")
    void ageTypeCannotBeEmpty() {
        //given
        final List<ConstraintViolation<GoodsFilterRequest>> validate = new ArrayList<>(
            validator.validate(GoodsFilterRequest.of(null, 1L, null,
                null, null, null, 1, Collections.emptyList())));
        //when, then
        isEqualToMessage(validate, "연령 선택은 최소 1가지 선택해야 합니다.");
    }

    private void isEqualToMessage(final List<ConstraintViolation<GoodsFilterRequest>> validate, final String message) {
        assertThat(validate.get(0).getMessage()).isEqualTo(message);
    }

    private void isEmpty(final Set<ConstraintViolation<GoodsFilterRequest>> validate) {
        assertThat(validate).isEmpty();
    }

}
