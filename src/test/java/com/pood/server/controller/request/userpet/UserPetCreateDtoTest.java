package com.pood.server.controller.request.userpet;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.controller.request.RequestDtoValidationTests;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.validation.ConstraintViolation;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserPetCreateDtoTest extends RequestDtoValidationTests {

    @Test
    @DisplayName("ai diagnosis 리스트 크기가 3이상일 때 예외")
    void getAiDiagnosisList() {
        //given
        UserPetCreateDto userPetCreateDto = new UserPetCreateDto(
            2,
            2, 2, "petName", LocalDate.of(2021, 1, 18), 4,
            4.0f, 1,
            List.of(new PetWorryRequest(1L, 1),
                new PetWorryRequest(2L, 2),
                new PetWorryRequest(3L, 3),
                new PetWorryRequest(4L, 4)),
            List.of(1, 2, 3),
            List.of(1, 2, 3),
            0
        );
        //when
        final List<ConstraintViolation<UserPetCreateDto>> validate = new ArrayList<>(
            validator.validate(userPetCreateDto));

        //then
        if (!validate.isEmpty()) {
            assertThat(validate.get(0).getMessage()).isEqualTo("최소 0 에서 최대 3개 까지만 고를 수 있습니다.");
        }
    }

    @Test
    @DisplayName("알갱이 크기 리스트 크기가 3이상일 때 예외")
    void getGrainSizeList() {
        //given
        UserPetCreateDto userPetCreateDto = new UserPetCreateDto(
            2,
            2, 2, "petName", LocalDate.of(2021, 1, 18), 4,
            4.0f, 1,
            List.of(new PetWorryRequest(1L, 1),
                new PetWorryRequest(2L, 2),
                new PetWorryRequest(3L, 3)),
            List.of(1, 2, 3, 4),
            List.of(1, 2, 3),
            0
        );
        //when
        final List<ConstraintViolation<UserPetCreateDto>> validate = new ArrayList<>(
            validator.validate(userPetCreateDto));

        //then
        if (!validate.isEmpty()) {
            assertThat(validate.get(0).getMessage()).isEqualTo("최소 0 에서 최대 3개 까지만 고를 수 있습니다.");
        }
    }

    @Test
    @DisplayName("알러지 리스트 크기가 3이상일 때 예외")
    void getAllergyList() {
        //given
        UserPetCreateDto userPetCreateDto = new UserPetCreateDto(
            2,
            2, 2, "petName", LocalDate.of(2021, 1, 18), 4,
            4.0f, 1,
            List.of(new PetWorryRequest(1L, 1),
                new PetWorryRequest(2L, 2),
                new PetWorryRequest(3L, 3)),
            List.of(1, 2, 3),
            List.of(1, 2, 3, 4),
            0
        );
        //when
        final List<ConstraintViolation<UserPetCreateDto>> validate = new ArrayList<>(
            validator.validate(userPetCreateDto));

        //then
        if (!validate.isEmpty()) {
            assertThat(validate.get(0).getMessage()).isEqualTo("최소 0 에서 최대 3개 까지만 고를 수 있습니다.");
        }
    }

}