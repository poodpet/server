package com.pood.server.controller.request.coupon;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.controller.request.RequestDtoValidationTests;
import java.util.Optional;
import java.util.Set;
import javax.validation.ConstraintViolation;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class CouponCreateDtoTest extends RequestDtoValidationTests {

    @Test
    @DisplayName("code가 null일 때")
    void codeIsNull() {
        //given
        final CouponCreateDto couponCreateDto = new CouponCreateDto(null);

        //when
        final Set<ConstraintViolation<CouponCreateDto>> validate = validator.validate(
            couponCreateDto);
        final Optional<ConstraintViolation<CouponCreateDto>> first = validate.stream().findFirst();

        //then
        first.ifPresent(couponCreateDtoConstraintViolation ->
            assertThat(couponCreateDtoConstraintViolation.getMessage())
                .isEqualTo("code는 null일 수 없습니다."));
    }

}