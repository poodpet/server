package com.pood.server.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;

import com.pood.server.controller.review.ReviewController;
import com.pood.server.dto.user.review.UserReviewSaveDto;
import com.pood.server.facade.ReviewFacade;
import com.pood.server.facade.review.request.ReviewTextUpdateRequest;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.bind.MethodArgumentNotValidException;

@WebMvcTest(ReviewController.class)
class ReviewControllerTest extends ControllerTests {

    @MockBean
    private ReviewFacade reviewFacade;

    @Test
    @DisplayName("유저 리뷰 저장하기 API")
    void saveReviewTest() throws Exception {
        //given
        final String content = objectMapper.writeValueAsString(
            new UserReviewSaveDto(1, "주문번호", 1, 5, "리뷰 코멘트"));

        final MockMultipartFile saveDto = new MockMultipartFile("userReviewSaveDto",
            "userReviewSaveDto", MediaType.APPLICATION_JSON_VALUE, content.getBytes());

        final MockMultipartFile multipartFile = new MockMultipartFile("image",
            "test.png",
            "image/png",
            "test image".getBytes());

        //when
        doNothing().when(reviewFacade)
            .saveUserReview(any(UserReviewSaveDto.class), anyString(), anyList());

        //then
        mockMvc.perform(multipart("/api/pood/v1-0/review")
                .file(multipartFile)
                .file(saveDto)
                .contentType(MediaType.APPLICATION_JSON)
                .header("token", "ba386d82-2f41-4809-97ed-1b616878e080"))
            .andExpect(ok());

    }



    @ParameterizedTest
    @MethodSource
    @DisplayName("리뷰 글 수정 시 파라미터 null 체크")
    void review_modify_paramValid_null(ReviewTextUpdateRequest request) throws Exception {
        //when
        ResultActions resultActions = mockMvc.perform(
                MockMvcRequestBuilders.patch("/api/pood/v1-0/review")
                    .content(objectMapper.writeValueAsBytes(request))
                    .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isBadRequest());

        //then
        resultActions.andExpect(result -> Assertions.assertThat(
            result.getResolvedException().getClass().getCanonicalName()).isEqualTo(
            MethodArgumentNotValidException.class.getCanonicalName()
        ));
    }

    private static Stream<Arguments> review_modify_paramValid_null() {
        return Stream.of(
            Arguments.of(new ReviewTextUpdateRequest(null, null)),
            Arguments.of(new ReviewTextUpdateRequest(1, null)),
            Arguments.of(new ReviewTextUpdateRequest(null, "테스트 1234567"))
        );
    }

    @ParameterizedTest
    @ValueSource(strings = {"","aa"})
    @DisplayName("리뷰 글 수정 시 글자 파라미터 size 체크")
    void review_modify_paramValid_text(String text) throws Exception {
        ReviewTextUpdateRequest request = new ReviewTextUpdateRequest(1, text);
        //when
        ResultActions resultActions = mockMvc.perform(
                MockMvcRequestBuilders.patch("/api/pood/v1-0/review")
                    .content(objectMapper.writeValueAsBytes(request))
                    .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isBadRequest());

        //then
        resultActions.andExpect(result -> Assertions.assertThat(
            result.getResolvedException().getClass().getCanonicalName()).isEqualTo(
            MethodArgumentNotValidException.class.getCanonicalName()
        ));
    }
}