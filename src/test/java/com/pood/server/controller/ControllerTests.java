package com.pood.server.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;

public abstract class ControllerTests {

    @Autowired
    protected WebApplicationContext ctx;

    @Autowired
    protected ObjectMapper objectMapper;

    protected MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(ctx)
            .addFilters(new CharacterEncodingFilter("UTF-8", true))
            .alwaysDo(print())
            .build();
    }

    protected ResultMatcher ok() {
        return status().isOk();
    }

    protected ResultMatcher badRequest() {
        return status().isBadRequest();
    }

    protected MockHttpServletRequestBuilder getMapping(final String url) {
        return MockMvcRequestBuilders.get(url);
    }

    protected MockHttpServletRequestBuilder postMapping(final String url) {
        return MockMvcRequestBuilders.post(url);
    }

    protected MockHttpServletRequestBuilder putMapping(final String url) {
        return MockMvcRequestBuilders.put(url);
    }

    protected MockHttpServletRequestBuilder patchMapping(final String url) {
        return MockMvcRequestBuilders.patch(url);
    }

    protected MockHttpServletRequestBuilder deleteMapping(final String url) {
        return MockMvcRequestBuilders.delete(url);
    }
}
