package com.pood.server.controller.response.pet;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserPetCreateResponseTest {

    @Test
    @DisplayName("첫 펫등록이 아닐 시 response coupon null")
    void isNotFirstRegister() {
        UserPetCreateResponse response = UserPetCreateResponse.isNotFirst("petName", "11-11111");
        assertThat(response.getCouponInfo()).isNull();
    }

}