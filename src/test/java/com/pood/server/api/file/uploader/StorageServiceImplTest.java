package com.pood.server.api.file.uploader;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class StorageServiceImplTest {

    StorageService storageService;
    imageResize newImageProperty;

    @BeforeEach
    void set() {
        storageService = new StorageServiceImpl();
        newImageProperty = storageService.getNewImageProperty(360, 360, 100, 200);
    }

    @Test
    @DisplayName("이미지 리사이즈 계산 로직 확인(width가 작을 경우)")
    void imageResize1() {
        org.junit.jupiter.api.Assertions.assertAll(
            () -> Assertions.assertThat(newImageProperty.getNew_width()).isEqualTo(180),
            () -> Assertions.assertThat(newImageProperty.getNew_height()).isEqualTo(360)
        );
    }

    @Test
    @DisplayName("이미지 리사이즈 계산 로직 확인(height가 작을 경우)")
    void imageResize2() {
        newImageProperty = storageService.getNewImageProperty(360, 360, 200, 100);
        org.junit.jupiter.api.Assertions.assertAll(
            () -> Assertions.assertThat(newImageProperty.getNew_width()).isEqualTo(360),
            () -> Assertions.assertThat(newImageProperty.getNew_height()).isEqualTo(180)
        );
    }
}