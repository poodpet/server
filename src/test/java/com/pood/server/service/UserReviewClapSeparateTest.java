package com.pood.server.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import com.pood.server.controller.request.reviewclap.ReviewClapCreateDto;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.repository.user.UserReviewClapRepository;
import com.pood.server.util.UserStatus;
import java.time.LocalDateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class UserReviewClapSeparateTest extends MockTests {

    @Mock
    private UserReviewClapRepository userReviewClapRepository;

    @InjectMocks
    private UserReviewClapSeparate userReviewClapSeparate;

    private UserInfo userInfo;

    @BeforeEach
    void setUp() {
        userInfo = new UserInfo(
            1,
            "82f44cf0-0b9e-44e4-801a-64772e485c2d",
            "nickName",
            "email",
            "password",
            1,
            UserStatus.ACTIVE,
            "name",
            1000,
            "phone",
            "referralCode",
            true,
            LocalDateTime.now(),
            true, LocalDateTime.now(),
            true, LocalDateTime.now(),
            "key", 1, "tokenArn",
            "mlName", 0.0, 1,
            1, LocalDateTime.now(),
            LocalDateTime.now(), LocalDateTime.now(),
            null, false, null,
            false, null,
            false, null
        );
    }

    @Test
    @DisplayName("리뷰가 이미 존재하는 경우")
    void isExistReviewLike() {
        //given
        ReviewClapCreateDto reviewClapCreateDto = ReviewClapCreateDto.of(1);

        //when
        when(userReviewClapRepository.existsByUserIdxAndUserReviewIdx(anyInt(), anyInt()))
            .thenReturn(true);

        //then
        assertThat(
            userReviewClapSeparate.isExistReviewLike(reviewClapCreateDto, userInfo)).isTrue();
    }

}