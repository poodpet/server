package com.pood.server.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.pood.server.dto.meta.pet.PetInfoDto;
import com.pood.server.dto.meta.pet.PetInfoDtoGroup;
import com.pood.server.entity.meta.Pet;
import com.pood.server.repository.meta.PetRepository;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class PetServiceTest extends MockTests {

    @Mock
    private PetRepository petRepository;

    @InjectMocks
    private PetService petService;

    @Test
    @DisplayName("믹스 우선 정렬 데이터 반환")
    void mixPrioritySort() {
        //given
        final List<Pet> pets = List.of(
            new Pet(1, "믹스 우선 정렬", "품종영어", "pcTag", "pcSize", 1, null,
                null, null, null, null, null, null
                , LocalDateTime.now(), LocalDateTime.now()),

            new Pet(2, "믹스", "품종영어", "pcTag", "pcSize", 1, null,
                null, null, null, null, null, null
                , LocalDateTime.now(), LocalDateTime.now()),

            new Pet(3, "일반", "품종영어", "pcTag", "pcSize", 1, null,
                null, null, null, null, null, null
                , LocalDateTime.now(), LocalDateTime.now())
        );
        when(petRepository.findAllOrderByPcKindDesc()).thenReturn(pets);
        //when
        final List<Pet> petList = petService.allPetInfo();
        final List<PetInfoDto> petInfoDtos = new PetInfoDtoGroup().addPetInfo(petList);
        //then
        verify(petRepository).findAllOrderByPcKindDesc();
        assertThat(petInfoDtos.get(0).getPcKind()).isEqualTo("믹스 우선 정렬");
        assertThat(petInfoDtos.get(1).getPcKind()).isEqualTo("믹스");
        assertThat(petInfoDtos.get(2).getPcKind()).isEqualTo("일반");
    }
}