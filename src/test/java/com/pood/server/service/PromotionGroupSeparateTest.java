package com.pood.server.service;

import com.pood.server.entity.meta.PromotionGroup;
import com.pood.server.repository.meta.PromotionGroupRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class PromotionGroupSeparateTest extends MockTests{

    @Mock
    private PromotionGroupRepository promotionGroupRepository;

    @InjectMocks
    private PromotionGroupSeparate promotionGroupSeparate;

    @Test
    @DisplayName("기획전 탭 리스트 조회")
    void success_promotion_group_list_by_promotion_idx() {

        List<PromotionGroup> tempPromotionGroupList = new ArrayList<>();
        PromotionGroup promotionGroup1 = new PromotionGroup(1, 1, "침대", 5,
            LocalDateTime.of(2021, 1, 31, 1, 10, 10), LocalDateTime.of(2021, 1, 31, 1, 10, 10));
        PromotionGroup promotionGroup2 = new PromotionGroup(2, 1, "사료", 5,
            LocalDateTime.of(2021, 1, 31, 1, 10, 10), LocalDateTime.of(2021, 1, 31, 1, 10, 10));
        PromotionGroup promotionGroup3 = new PromotionGroup(3, 1, "영양식", 5,
            LocalDateTime.of(2021, 1, 31, 1, 10, 10), LocalDateTime.of(2021, 1, 31, 1, 10, 10));

        tempPromotionGroupList.add(promotionGroup1);
        tempPromotionGroupList.add(promotionGroup2);
        tempPromotionGroupList.add(promotionGroup3);

        BDDMockito.when(promotionGroupRepository.findByPrIdx(ArgumentMatchers.anyInt()))
            .thenReturn(tempPromotionGroupList);

        List<PromotionGroup> promotionGroupList = promotionGroupSeparate.promotionGroupListByPromotionIdx(
            1);

        //비교 then
        Assertions.assertThat(promotionGroupList.size()).isEqualTo(3);
    }


    @Test
    @DisplayName("기획전 탭 정보가 없는 리스트 조회")
    void success_zero_promotion_group_list_by_promotion_idx() {

        BDDMockito.when(promotionGroupRepository.findByPrIdx(ArgumentMatchers.anyInt()))
            .thenReturn(Collections.emptyList());

        List<PromotionGroup> promotionGroupList = promotionGroupSeparate.promotionGroupListByPromotionIdx(
            1);

        //비교 then
        Assertions.assertThat(promotionGroupList).isEmpty();

    }
}