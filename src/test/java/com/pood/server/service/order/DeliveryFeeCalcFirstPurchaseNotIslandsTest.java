package com.pood.server.service.order;

import com.pood.server.web.mapper.payment.Money;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class DeliveryFeeCalcFirstPurchaseNotIslandsTest {

    @Test
    @DisplayName("계산이 정상적으로 작동하는지 테스트")
    void calc() {
        DeliveryFeeCalcFirstPurchaseNotIslands money = new DeliveryFeeCalcFirstPurchaseNotIslands(
            new Money(30000));
        Money calc = money.calc(new Money(333333));
        Assertions.assertThat(calc.getIntValue()).isEqualTo(0);
    }
}