package com.pood.server.service.order;

import static org.junit.jupiter.api.Assertions.*;

import com.pood.server.web.mapper.payment.Money;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class DeliveryFeeCalcNotIslandsAndBasicFreeTest {

    @Test
    @DisplayName("계산이 정상적으로 작동하는지 테스트")
    void calc(){
        DeliveryFeeCalcNotIslandsAndBasicFree money = new DeliveryFeeCalcNotIslandsAndBasicFree(
            new Money(30000));
        Money calc = money.calc(new Money(333333));
        Assertions.assertThat(calc.getIntValue()).isEqualTo(0);
    }
}