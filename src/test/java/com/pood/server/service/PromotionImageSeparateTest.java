package com.pood.server.service;


import com.pood.server.dto.meta.goods.PromotionDto;
import com.pood.server.entity.meta.PromotionImage;
import com.pood.server.repository.meta.PromotionImageRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class PromotionImageSeparateTest extends MockTests {

    @Mock
    PromotionImageRepository promotionImageRepository;

    @InjectMocks
    PromotionImageSeparate promotionImageSeparate;

    @Test
    @DisplayName("프로모션 이미지 리스트 조회")
    void success_promotion_image_list() {
        List<PromotionImage> list = new ArrayList<>();
        PromotionImage promotionImage1 = new PromotionImage(
            1, 1, "http:..", 1, 0, 0, LocalDateTime.of(2021, 1, 31, 1, 10, 10), LocalDateTime.of(2021, 1, 31, 1, 10, 10));
        list.add(promotionImage1);

        //객체 생성 given
        BDDMockito.when(promotionImageRepository.findAllByPrIdx(ArgumentMatchers.anyInt()))
            .thenReturn(list);

        //실행 when
        List<PromotionDto.PromotionImage> promotionImages = promotionImageSeparate.getPromoitionImagesByPromotionIdx(
            0);

        //비교 then
        Assertions.assertThat(promotionImages.size()).isEqualTo(1);
    }

    @Test
    @DisplayName("프로모션 이미지가 없는 리스트 조회")
    void success_zero_promotion_image_list() {

        //객체 생성 given
        BDDMockito.when(promotionImageRepository.findAllByPrIdx(ArgumentMatchers.anyInt()))
            .thenReturn(
                Collections.emptyList());

        //실행 when
        List<PromotionDto.PromotionImage> promotionImages = promotionImageSeparate.getPromoitionImagesByPromotionIdx(
            0);

        //비교 then
        Assertions.assertThat(promotionImages).isEmpty();

    }

}