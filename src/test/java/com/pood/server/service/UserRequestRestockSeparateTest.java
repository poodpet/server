package com.pood.server.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.pood.server.repository.user.UserRequestRestockRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UserRequestRestockSeparateTest {

    @Mock
    UserRequestRestockRepository repository;

    @InjectMocks
    UserRequestRestockSeparate userRequestRestockSeparate;

    @Test
    @DisplayName("재입고 요청 인수 테스트")
    void restock() {

        int goodsIdx = 1;
        String userUuid = "user-uuid";

        when(repository.existsByGoodsIdxAndUserUuidAndState(anyInt(), anyString(), any())).thenReturn(false);

        userRequestRestockSeparate.insertRequestRestock(goodsIdx, userUuid);

        //then
        verify(repository).save(any());
    }
}