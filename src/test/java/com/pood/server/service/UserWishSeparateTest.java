package com.pood.server.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.pood.server.entity.user.UserWish;
import com.pood.server.repository.user.UserWishRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

public class UserWishSeparateTest extends MockTests {

    @Mock
    private UserWishRepository userWishRepository;

    @InjectMocks
    private UserWishSeparate userWishSeparate;

    @Test
    @DisplayName("유저 찜 저장")
    void saveUserWishTest() {
        final UserWish userWish = UserWish.builder()
            .userIdx(111)
            .goodsIdx(555)
            .build();

        when(userWishRepository.save(any()))
            .thenReturn(userWish);

        userWishSeparate.saveUserWish(userWish);

        verify(userWishRepository).save(userWish);
    }

    @Test
    @DisplayName("유저가 찜 했을 때 true 반환 테스트")
    void wishValidationTrueTest() {
        //given
        final int userIdx = 278;
        final int goodsIdx = 41;
        when(userWishRepository.existsByUserIdxAndGoodsIdx(anyInt(), anyInt()))
            .thenReturn(true);
        //when
        final boolean isWished = userWishSeparate.wishValidation(userIdx, goodsIdx);

        //then
        assertThat(isWished).isTrue();
    }

    @Test
    @DisplayName("유저가 찜 하지 않았을 때 false 반환 테스트")
    void wishValidationFalseTest() {
        //given
        final int userIdx = 272;
        final int goodsIdx = 323;
        when(userWishRepository.existsByUserIdxAndGoodsIdx(anyInt(), anyInt()))
            .thenReturn(false);
        //when
        final boolean isWished = userWishSeparate.wishValidation(userIdx, goodsIdx);

        //then
        assertThat(isWished).isFalse();
    }

    @Test
    @DisplayName("유저 찜 목록을 삭제")
    void deleteUserWishTest() {
        final int userIdx = 559;
        final int goodsIdx = 218;

        when(userWishRepository.deleteByUserIdxAndGoodsIdx(anyInt(), anyInt()))
            .thenReturn(userIdx);

        //when
        final int deleteUserIdx = userWishSeparate.deleteUserWish(userIdx, goodsIdx);

        assertThat(deleteUserIdx).isEqualTo(userIdx);
    }
}
