package com.pood.server.service;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.user.UserPetRepository;
import org.assertj.core.api.AbstractThrowableAssert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class UserPetSeparateTest extends MockTests {

    @Mock
    private UserPetRepository userPetRepository;

    @InjectMocks
    private UserPetSeparate userPetSeparate;

    @Test
    @DisplayName("유저와 유저 펫의 id값으로 유저가 펫을 소유하고 있는지 검증")
    void petOwnerValidation() {
        //given
        int userPetIdx = 1;
        int userIdx = 1;
        given(userPetRepository.existsByIdxAndUserIdx(anyInt(), anyInt()))
            .willReturn(true);

        //when
        userPetSeparate.petOwnerValidation(userPetIdx, userIdx);

        //then
        verify(userPetRepository).existsByIdxAndUserIdx(userPetIdx, userIdx);
    }

    @Test
    @DisplayName("유저와 유저 펫의 id값으로 유저가 펫을 소유하고 있지 않을 때 예외")
    void petOwnerValidationException() {
        //given
        int userPetIdx = 1;
        int userIdx = 1;
        given(userPetRepository.existsByIdxAndUserIdx(anyInt(), anyInt()))
            .willReturn(false);

        //when
        final AbstractThrowableAssert<?, ? extends Throwable> abstractThrowableAssert = assertThatThrownBy(
            () -> userPetSeparate.petOwnerValidation(userPetIdx, userIdx));

        //then
        verify(userPetRepository).existsByIdxAndUserIdx(userPetIdx, userIdx);
        abstractThrowableAssert
            .isInstanceOf(NotFoundException.class)
            .hasMessage("해당하는 유저의 펫이 없습니다.");
    }
}