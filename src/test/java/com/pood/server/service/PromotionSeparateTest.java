package com.pood.server.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.pood.server.dto.meta.goods.PromotionDto;
import com.pood.server.dto.meta.goods.PromotionDto.PromotionGoods;
import com.pood.server.dto.meta.goods.PromotionDto.PromotionGoodsListInfo;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.meta.PromotionRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.assertj.core.api.AbstractThrowableAssert;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;


class PromotionSeparateTest extends MockTests {

    @Mock
    private PromotionRepository promotionRepository;

    @InjectMocks
    private PromotionSeparate promotionSeparate;

    @Test
    @DisplayName("기획 상품 성공 조회")
    void success_promotion_gooods_info() {

        //객체 생성 given
        PromotionDto.PromotionGoodsListInfo promotionGoodsListInfo = new PromotionDto.PromotionGoodsListInfo();
        promotionGoodsListInfo.setIdx(1);
        promotionGoodsListInfo.setName("미초바");

        when(promotionRepository.findListInfoById(ArgumentMatchers.anyInt()))
            .thenReturn(promotionGoodsListInfo);

        //실행 when
        PromotionGoodsListInfo promotionGooodsInfo = promotionSeparate.getPromotionGooodsInfo(1);

        //비교 then
        assertThat(promotionGooodsInfo.getIdx()).isEqualTo(1);

    }

    @Test
    @DisplayName("기획 상품 예외 조회")
    void fail_promotion_gooods_info() {

        //객체 생성 given
        when(promotionRepository.findListInfoById(ArgumentMatchers.anyInt()))
            .thenThrow(new NotFoundException("프로모션 상품 정보를 찾을 수 없습니다."));

        //실행 when
        AbstractThrowableAssert<?, ? extends Throwable> abstractThrowableAssert = Assertions.assertThatThrownBy(
            () -> promotionSeparate.getPromotionGooodsInfo(1));

        //비교 then
        abstractThrowableAssert
            .isInstanceOf(NotFoundException.class)
            .hasMessage("프로모션 상품 정보를 찾을 수 없습니다.");

    }

    @Test
    @DisplayName("프로모션 상품 리스트 조회")
    void success_promotion_goods_list() {

        //객체 생성 given
        List<PromotionGoods> promotionGoodsList = new ArrayList<>();
        PromotionGoods promotionGoods1 = new PromotionGoods();
        PromotionGoods promotionGoods2 = new PromotionGoods();
        promotionGoodsList.add(promotionGoods1);
        promotionGoodsList.add(promotionGoods2);
        promotionGoods1.setGoodsIdx(1);
        promotionGoods1.setGoodsPrice(100);
        promotionGoods1.setAverageRating(5.0d);
        promotionGoods1.setName("상품1");
        promotionGoods1.setGoodsOriginPrice(10);
        promotionGoods1.setDiscountPrice(1000);
        promotionGoods1.setDiscountRate(10);
        promotionGoods1.setPrDiscountPrice(1);
        promotionGoods1.setPrDiscountRate(1);
        promotionGoods1.setReviewCnt(1L);
        promotionGoods1.setSaleStatus(1);
        promotionGoods1.setUrl("http:..");
        promotionGoods1.setGoodsIdx(65);

        promotionGoods2.setGoodsIdx(2);
        promotionGoods2.setGoodsPrice(100);
        promotionGoods2.setAverageRating(5.0d);
        promotionGoods2.setName("상품2");
        promotionGoods2.setGoodsOriginPrice(10);
        promotionGoods2.setDiscountPrice(1000);
        promotionGoods2.setDiscountRate(10);
        promotionGoods2.setPrDiscountPrice(1);
        promotionGoods2.setPrDiscountRate(1);
        promotionGoods2.setReviewCnt(1L);
        promotionGoods2.setSaleStatus(1);
        promotionGoods2.setUrl("http:..");
        promotionGoods2.setGoodsIdx(72);

        when(promotionRepository.findPromotionGoodsDetail(ArgumentMatchers.anyInt()))
            .thenReturn(promotionGoodsList);

        //실행 when
        List<PromotionGoods> promotionGoodsDetail = promotionSeparate.getPromotionGoodsDetail(1);

        //비교 then
        assertThat(promotionGoodsDetail.size()).isEqualTo(2);

    }

    @Test
    @DisplayName("프로모션 상품 정보가 없는 리스트 조회")
    void success_zero_promotion_goods_list() {

        //객체 생성 given
        when(promotionRepository.findPromotionGoodsDetail(ArgumentMatchers.anyInt()))
            .thenReturn(
                Collections.emptyList());

        //실행 when
        List<PromotionGoods> promotionGoodsDetail = promotionSeparate.getPromotionGoodsDetail(0);

        //비교 then
        assertThat(promotionGoodsDetail).isEmpty();
    }

}