package com.pood.server.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.verify;

import com.pood.server.entity.user.UserInfo;
import com.pood.server.exception.CustomStatusException;
import com.pood.server.exception.ErrorMessage;
import com.pood.server.repository.user.UserInfoRepository;
import com.pood.server.util.UserStatus;
import java.time.LocalDateTime;
import java.util.Optional;
import org.assertj.core.api.AbstractThrowableAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

class UserSeparateTest extends MockTests {

    @Mock
    private UserInfoRepository userInfoRepository;

    @InjectMocks
    private UserSeparate userSeparate;

    private String userUUID;
    private UserInfo userInfo;

    @BeforeEach
    void setUp() {
        userUUID = "50963bed-4455-4fdb-b600-3acb167cbb34";
        userInfo = new UserInfo(
            1,
            userUUID,
            "nickName",
            "email",
            "password",
            1,
            UserStatus.ACTIVE,
            "name",
            1000,
            "phone",
            "referralCode",
            true,
            LocalDateTime.now(),
            true, LocalDateTime.now(),
            true, LocalDateTime.now(),
            "key", 1, "tokenArn",
            "mlName", 0.0, 1,
            1, LocalDateTime.now(),
            LocalDateTime.now(), LocalDateTime.now(),
            null, false, null,
            false, null,
            false, null
        );
    }

    @Test
    @DisplayName("uuid로 user를 조회한다")
    void getUser() {
        //given
        Mockito.when(userInfoRepository.findByUserUuid(userUUID))
            .thenReturn(Optional.ofNullable(userInfo));

        //when
        final UserInfo user = userSeparate.getUserByUUID(userUUID);

        //then
        verify(userInfoRepository).findByUserUuid(userUUID);
        assertThat(user.getUserUuid()).isEqualTo(userUUID);
    }

    @Test
    @DisplayName("uuid로 user를 찾을 수 없을 때 예외")
    void getUserException() {
        //given
        Mockito.when(userInfoRepository.findByUserUuid(userUUID))
            .thenThrow(CustomStatusException.badRequest(ErrorMessage.USER));

        //when
        final AbstractThrowableAssert<?, ? extends Throwable> abstractThrowableAssert = assertThatThrownBy(
            () -> userSeparate.getUserByUUID(userUUID));
        //then
        abstractThrowableAssert.isInstanceOf(CustomStatusException.class)
            .hasMessage("400 BAD_REQUEST \"사용자가 존재하지 않습니다.\"");
    }
}