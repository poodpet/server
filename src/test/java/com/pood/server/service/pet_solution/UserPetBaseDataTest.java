package com.pood.server.service.pet_solution;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.dto.meta.user.pet.UserPetAllergyGroup;
import com.pood.server.dto.meta.user.pet.UserPetGrainSizeGroup;
import com.pood.server.entity.meta.AllergyData;
import com.pood.server.entity.meta.GrainSize;
import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.user.UserPet;
import com.pood.server.entity.user.UserPetAiDiagnosis;
import com.pood.server.entity.user.UserPetAllergy;
import com.pood.server.entity.user.UserPetGrainSize;
import com.pood.server.entity.user.UserPetWeightDiary;
import com.pood.server.service.pet_solution.group.UserPetAiDiagnosisAiSolutionGroup;
import com.pood.server.service.pet_solution.group.UserPetAllergyAiSolutionGroup;
import com.pood.server.service.pet_solution.group.UserPetGrainSizeAiSolutionGroup;
import com.pood.server.util.FeedType;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class UserPetBaseDataTest {

    private final UserPetGrainSizeGroup userPetGrainSizeGroup = new UserPetGrainSizeGroup();
    private final UserPetAllergyGroup userPetAllergyGroup = new UserPetAllergyGroup();

    UserPetBaseData userPetBaseData;

    @BeforeEach
    void setUp() {
        List<UserPetAiDiagnosis> aiRecommendDiagnosisList = List.of(
            new UserPetAiDiagnosis(0, 20, 0, "알레르기", "501", 0, 1L, 2,
                LocalDateTime.now().toString()),
            new UserPetAiDiagnosis(1, 20, 1, "눈", "501", 0, 1L, 3, LocalDateTime.now().toString()),
            new UserPetAiDiagnosis(2, 20, 2, "뼈/관절/근육", "501", 0, 1L, 1,
                LocalDateTime.now().toString())
        );
        UserPetAiDiagnosisAiSolutionGroup userPetAiDiagnosisAiSolutionGroup = new UserPetAiDiagnosisAiSolutionGroup(
            aiRecommendDiagnosisList);

        List<AllergyData> allergyList = List.of(
            new AllergyData(1, "소고기", 1, true, "url", 1, LocalDateTime.now(), LocalDateTime.now()),
            new AllergyData(2, "name2", 2, true, "url", 2, LocalDateTime.now(),
                LocalDateTime.now()),
            new AllergyData(3, "name3", 3, true, "url", 3, LocalDateTime.now(), LocalDateTime.now())
        );
        final List<UserPetAllergy> userPetAllergies = userPetAllergyGroup.saveAllObjects(
            allergyList, 1);
        UserPetAllergyAiSolutionGroup userPetAllergyAiSolutionGroup = new UserPetAllergyAiSolutionGroup(
            userPetAllergies);

        List<GrainSize> grainSizes = List.of(
            new GrainSize(1, "name", 1.0, 1.5, "title", "url", 1, LocalDateTime.now(),
                LocalDateTime.now()),
            new GrainSize(2, "name", 1.0, 1.5, "title", "url", 2, LocalDateTime.now(),
                LocalDateTime.now()),
            new GrainSize(3, "name", 1.0, 1.9, "title", "url", 3, LocalDateTime.now(),
                LocalDateTime.now())
        );
        final List<UserPetGrainSize> userPetGrainSizeList = userPetGrainSizeGroup.saveAllObjects(
            grainSizes, 1);

        UserPetGrainSizeAiSolutionGroup userPetGrainSizeAiSolutionGroup = new UserPetGrainSizeAiSolutionGroup(
            userPetGrainSizeList);

        Pet pet = new Pet(1, "믹스 우선 정렬", "품종영어", "pcTag", "pcSize", 1, null,
            null, null, null, null, null, null
            , LocalDateTime.now(), LocalDateTime.now());
        final UserPet userPet = new UserPet(1, "allergy", 1, 1, 1, 1, "펫이름",
            LocalDate.now().minusMonths(11), 1, 0);
        userPet.setPetWeightDiaryLists(
            List.of(
                new UserPetWeightDiary(1L, userPet, 3.5f, LocalDate.now(), LocalDateTime.now(),
                    LocalDateTime.now()),
                new UserPetWeightDiary(1L, userPet, 2f, LocalDate.now(), LocalDateTime.now(),
                    LocalDateTime.of(2021, 2, 3, 10, 10)))
        );
        UserPetCalcDog userPetCalcDog = new UserPetCalcDog();
        userPetBaseData = new UserPetBaseData(userPet, userPetGrainSizeAiSolutionGroup,
            userPetAllergyAiSolutionGroup, userPetAiDiagnosisAiSolutionGroup, pet, userPetCalcDog,
            Collections.emptyList());
    }

    @Test
    void getPetCalorie() {
        assertThat(userPetBaseData.getPetCalorie()).isEqualTo(313.4636f);
    }

    @Test
    void isPetTypeDog() {
        assertThat(userPetBaseData.isPetTypeDog()).isTrue();
    }

    @Test
    void isPetTypeCat() {
        assertThat(userPetBaseData.isPetTypeCat()).isFalse();
    }

    @Test
    void isPetTypeDogAndIsNeutering() {
        assertThat(userPetBaseData.isDogAndIsNeutered()).isFalse();
    }

    @Test
    void isPetTypeDogAndIsNotNeutering() {
        assertThat(userPetBaseData.isDogAndNotNeutered()).isTrue();
    }

    @Test
    void isPetTypeCatAndIsNotNeutering() {
        assertThat(userPetBaseData.isPetTypeCatAndIsNotNeutering()).isFalse();
    }

    @Test
    void isPetTypeCatAndIsNeutering() {
        assertThat(userPetBaseData.isCatAndNeutered()).isFalse();
    }

    @Test
    void getPetSurgeryScore() {
        assertThat(userPetBaseData.getPetSurgeryScore()).isEqualTo(20d);
    }

    @Test
    void getPetMoistureSurgeryScore() {
        assertThat(userPetBaseData.getPetMoistureSurgeryScore()).isEqualTo(25d);
    }

    @Test
    void getPetAllergySurgeryScore() {
        assertThat(userPetBaseData.getPetAllergySurgeryScore()).isEqualTo(25d);
    }

    @Test
    void getPetUnitSizeSurgeryScore() {
        assertThat(userPetBaseData.getPetUnitSizeSurgeryScore()).isEqualTo(25d);
    }

    @Test
    void getPetUnitSizeWebSurgeryScore() {
        assertThat(userPetBaseData.getPetUnitSizeWebSurgeryScore()).isEqualTo(18d);
    }

    @Test
    void getFeedTargetSurgeryScore() {
        assertThat(userPetBaseData.getFeedTargetSurgeryScore()).isEqualTo(25d);
    }

    @Test
    void getPetAgeSurgeryScore() {
        assertThat(userPetBaseData.getPetAgeSurgeryScore()).isEqualTo(25d);
    }

    @Test
    void getPetUnitSizeScore() {
        assertThat(userPetBaseData.getPetUnitSizeScore(1.0, true)).isEqualTo(18d);
    }

    @Test
    void getPetAllergyScore() {
        assertThat(userPetBaseData.getPetAllergyScore("소고기", "닭고기")).isZero();
    }

    @Test
    void getPetFeedType() {
        assertThat(userPetBaseData.getPetFeedType()).isEqualTo(FeedType.PUPPY);
    }

}