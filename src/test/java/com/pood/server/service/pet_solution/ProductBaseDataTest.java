package com.pood.server.service.pet_solution;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.entity.meta.Feed;
import com.pood.server.entity.meta.PetDoctorFeedDescription;
import com.pood.server.entity.meta.Product.PackageType;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ProductBaseDataTest {

    ProductBaseData productBaseData;

    @BeforeEach
    void setUp() {
        PetDoctorFeedDescription petDoctorFeedDescription = PetDoctorFeedDescription.builder()
            .idx(12).petDoctorIdx(10)
            .ctIdx(0).ctSubIdx(0).productIdx(12)
            .baseScore(40).position1("1").position2("2").position3("-")
            .ardGroup122(1).ardGroup421(1).ardGroup501(1).ardGroup201(1).ardGroup301(1)
            .ardGroup961(1).ardGroup241(1).ardGroup121(1).ardGroup941(1).ardGroup422(1)
            .ardGroup145(1)
            .ardGroup125(1).ardGroup942(1).ardGroup126(1).ardGroup601(0).ardGroup423(1)
            .ardGroup502(1)
            .build();
        Feed feed = Feed.builder()
            .idx(1).productIdx(12)
            .prMoisture(10d).prProtein(34d).prFat(13d).prAsh(0d)
            .prFiber(0d).prCarbo(27.5d).amArginine(2.68d).amHistidine(0.8d)
            .amIsoleucine(1.39d).amLeucine(2.35d).amLysine(2.14d).amMetCys(1.07d)
            .amMethionine(0.6d).amPheTyr(2.53d).amPhenylanlanine(1.32d).amThreonine(1.36d)
            .amTryptophan(0.37d).amValine(1.64d).amCystine(0.47d).amTyrosine(1.21d)
            .amLCarnitine(0d).amGlutamicAcid(0d).miCalcium(0.8d).miPhosphours(0.6d)
            .miPotassium(0.81d).miSodium(0.26d).miChloride(0.3d).miMagnessium(0.159d)
            .miIron(266d).miCopper(26.7).miManganese(30.5d).miZinc(231d)
            .miIodine(2.25).miSelenium(0.53).miCaPh(1.33).viVitaminA(61423.9).viVitaminD(2159.63)
            .viVitaminE(400.0).viVitaminB1(29.81).viVitaminB2(14.81).viVitaminB3(250.06)
            .viVitaminB5(28.54).viVitaminB6(14.13).viVitaminB7(0.0).viVitaminB9(3.1)
            .viVitaminB12(0.08).viCholine(1932.14).viVitaminC(0d).viVitaminK3(0d)
            .faOmega3(1.25).faEpa(0d).faDha(0d).faEpaDha(-1d).faOmega6(3.75)
            .faLinoleicacid(4.03).faArachidonicAcid(0d).faALinoleicacid(0d)
            .faOmega63(3d).otTaurine(0.19).otGlucosamine(250d).otChondroitin(200d).otMsm(0d)
            .otProbiotics("0")
            .build();

        productBaseData = ProductBaseData.builder()
            .idx(12)
            .uuid("3f4e6f94-11b3-49dd-b599-c99a78361795")
            .brand(null)
            .productName("웰니스 코어 스몰브리드 헬시 웨이트 1.8kg")
            .productCode("PDFE02211")
            .barcode("076344884439").pcIdx(1).ctIdx(0).ctSubIdx(0).ctSubName("건식사료")
            .showIndex(1).tag("-").allNutrients(0)
            .mainProperty("칠면조").tasty("칠면조").productVideo("-1")
            .calorie(3425).feedTarget(1).feedType("A").glutenFree(1)
            .animalProtein("칠면조,닭")
            .vegetableProtein("감자,완두콩").aafco("M")
            .singleProtein(0).ingredients("test").unitSize(1.0)
            .ingredientsSearch("test").noticeDesc("-")
            .noticeTitle("-").packageType(PackageType.NORMAL).cupWeight(92)
            .isRecommend(1).grainFree(0)
            .avaQuantity(5).quantity(2).weight(1800).approve(true)
            .snack(null)
            .feed(feed)
            .petDoctorFeedDescription(petDoctorFeedDescription)
            .build();
    }


    @Test
    void isWetFeed() {
        assertThat(productBaseData.isWetFeed()).isFalse();
    }

    @Test
    void eqFeedTarget() {
        assertThat(productBaseData.eqFeedTarget(1)).isTrue();
    }

    @Test
    void getProductType() {
        assertThat(productBaseData.getProductType()).isEqualTo(ProductType.FEED);
    }

    @Test
    void eqProductType() {
        assertThat(productBaseData.eqProductType(ProductType.SNACK)).isFalse();
    }

    @Test
    void getAiDiagnosisScore() {
        assertThat(productBaseData.getPetWorryScore(List.of(942))).isEqualTo(100);
    }

    @Test
    void getAiDiagnosisScore2() {
        assertThat(productBaseData.getPetWorryScore(List.of(942, 601))).isEqualTo(50);
    }

    @Test
    void getAiDiagnosisScore3() {
        assertThat(productBaseData.getPetWorryScore(List.of(942, 961, 601))).isEqualTo(80);
    }

    @Test
    @DisplayName("컵 사이즈 계산 값이 있는경우")
    void calculateCupSizeTest() {
        final float size = productBaseData.calculateCupSize();
        assertThat(size).isEqualTo(92.0f);
    }

    @Test
    void defaultCupSize() {
        productBaseData.setCupWeight(0);
        final float size = productBaseData.calculateCupSize();
        assertThat(size).isEqualTo(100.0f);
    }

    @Test
    @DisplayName("해당 상품이 용품 인지 확인")
    void isSupplies() {
        final float size = productBaseData.calculateCupSize();
        assertThat(productBaseData.isSupplies()).isFalse();
    }
    
}