package com.pood.server.service.pet_solution.calc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.BDDMockito.mock;
import static org.mockito.BDDMockito.when;

import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.ProductType;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.util.FeedType;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

class FeedCompareProductTest {

    private UserPetBaseData userPetBaseData;
    private ProductBaseData productBaseData;
    private FeedCompareProduct feedCompareProduct;

    @BeforeEach
    void init() {
        userPetBaseData = mock(UserPetBaseData.class);
        productBaseData = mock(ProductBaseData.class);

        feedCompareProduct = new FeedCompareProduct(userPetBaseData, productBaseData);
    }

    @ParameterizedTest
    @MethodSource
    @DisplayName("분석하는 상품은 사료이여야 함")
    void fitWordValidFeed(final ProductType productType) {

        //given
        when(productBaseData.getProductType()).thenReturn(productType);

        //then
        assertThatThrownBy(feedCompareProduct::fitWord)
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessageMatching("비교 상품이 사료가 아닙니다.");
    }

    private static Stream<Arguments> fitWordValidFeed() {
        return Stream.of(
            Arguments.of(ProductType.SUPPLIES),
            Arguments.of(ProductType.SAMPLE),
            Arguments.of(ProductType.NUTRIENTS),
            Arguments.of(ProductType.SNACK)
        );
    }

    @ParameterizedTest
    @ValueSource(doubles = {25.0, 25.1, 26.0})
    @DisplayName("사료비교 상품은, 기본점수에서 기호성 점수를 뺀 점수가 100점이 넘으면 100점")
    void feedCompareScoreAbove100(final double grainScore) {

        //given
        //petSize 점수
        when(userPetBaseData.isPetTypeCat()).thenReturn(false);
        when(productBaseData.getFeedTarget()).thenReturn(0);    //모두 잘 맞는 알갱이
        when(userPetBaseData.getFeedTargetSurgeryScore()).thenReturn(25.0);

        //peg age 점수
        when(productBaseData.getFeedType()).thenReturn(FeedType.ALL.getValue());
        when(userPetBaseData.getPetAgeSurgeryScore()).thenReturn(25.0);

        //기호성 점수
        when(userPetBaseData.getPetUnitSizeScore(anyDouble(), anyBoolean())).thenReturn(grainScore);

        //중성화 점수
        when(productBaseData.getCalorie()).thenReturn(3000);
        when(userPetBaseData.isNeutered()).thenReturn(false);
        when(userPetBaseData.getPetSurgeryScore()).thenReturn(25.0);

        //수분공급
        when(productBaseData.isWetFeed()).thenReturn(true);
        when(userPetBaseData.getPetMoistureSurgeryScore()).thenReturn(25.0);

        //알러지 점수 - 0점나오게
        when(productBaseData.getProductType()).thenReturn(ProductType.SUPPLIES);


        //then
        assertThat(feedCompareProduct.getScore()).isEqualTo(100);
    }

    @Test
    @DisplayName("사료비교 상품은, 기본점수에서 기호성 점수를 뺀 점수가 100점이 안넘으면 백분율 점수 - 기호성이 0 점일때 100점")
    void feedCompareScoreUnder100_atGrainZero() {

        double mockScore = 25.0;

        //given
        //petSize 점수
        when(userPetBaseData.isPetTypeCat()).thenReturn(false);
        when(productBaseData.getFeedTarget()).thenReturn(0);    //모두 잘 맞는 알갱이
        when(userPetBaseData.getFeedTargetSurgeryScore()).thenReturn(mockScore);

        //peg age 점수
        when(productBaseData.getFeedType()).thenReturn(FeedType.ALL.getValue());
        when(userPetBaseData.getPetAgeSurgeryScore()).thenReturn(mockScore);

        //기호성 점수
        when(userPetBaseData.getPetUnitSizeScore(anyDouble(), anyBoolean())).thenReturn(0.0);

        //중성화 점수
        when(productBaseData.getCalorie()).thenReturn(3000);
        when(userPetBaseData.isNeutered()).thenReturn(false);
        when(userPetBaseData.getPetSurgeryScore()).thenReturn(mockScore);

        //수분공급
        when(productBaseData.isWetFeed()).thenReturn(true);
        when(userPetBaseData.getPetMoistureSurgeryScore()).thenReturn(mockScore);

        //알러지 점수 - 0점나오게
        when(productBaseData.getProductType()).thenReturn(ProductType.SUPPLIES);


        //then
        assertThat(feedCompareProduct.getScore()).isEqualTo(100);
    }


    @Test
    @DisplayName("사료비교 상품은, 기본점수에서 기호성 점수를 뺀 점수가 100점이 안넘으면 백분율 점수 - 기호성이 0 점이 아닐 때 백분율 점수")
    void feedCompareScoreUnder100_atGrainNotZero() {

        double mockScore = 15.0;
        double grainScore = 20.0;


        //given
        //petSize 점수
        when(userPetBaseData.isPetTypeCat()).thenReturn(false);
        when(productBaseData.getFeedTarget()).thenReturn(0);    //모두 잘 맞는 알갱이
        when(userPetBaseData.getFeedTargetSurgeryScore()).thenReturn(mockScore);

        //peg age 점수
        when(productBaseData.getFeedType()).thenReturn(FeedType.ALL.getValue());
        when(userPetBaseData.getPetAgeSurgeryScore()).thenReturn(mockScore);

        //기호성 점수
        when(userPetBaseData.getPetUnitSizeScore(anyDouble(), anyBoolean())).thenReturn(grainScore);

        //중성화 점수
        when(productBaseData.getCalorie()).thenReturn(3000);
        when(userPetBaseData.isNeutered()).thenReturn(false);
        when(userPetBaseData.getPetSurgeryScore()).thenReturn(mockScore);

        //수분공급
        when(productBaseData.isWetFeed()).thenReturn(true);
        when(userPetBaseData.getPetMoistureSurgeryScore()).thenReturn(mockScore);

        //알러지 점수 - 0점나오게
        when(productBaseData.getProductType()).thenReturn(ProductType.SUPPLIES);

        //then
        //1점당 1.25 점으로 환산
        assertThat(feedCompareProduct.getScore()).isEqualTo(1.25 * 60);
    }
}