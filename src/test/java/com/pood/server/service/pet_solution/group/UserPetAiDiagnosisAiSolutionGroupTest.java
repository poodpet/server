package com.pood.server.service.pet_solution.group;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.entity.user.UserPetAiDiagnosis;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserPetAiDiagnosisAiSolutionGroupTest {

    @Test
    @DisplayName("유저 펫의 고민거리 우선수위로 정렬")
    void getAiDiagnosisIdxOrderByPriority() {

        //given
        List<UserPetAiDiagnosis> aiRecommendDiagnosisList = List.of(
            new UserPetAiDiagnosis(0, 20, 0, "알레르기", "501", 0, 0L, 2, LocalDateTime.now().toString()),
            new UserPetAiDiagnosis(1, 20, 1, "눈", "501", 0, 1L, 3, LocalDateTime.now().toString()),
            new UserPetAiDiagnosis(2, 20, 2, "뼈/관절/근육", "501", 0, 2L, 1, LocalDateTime.now().toString())
        );
        UserPetAiDiagnosisAiSolutionGroup userPetAiDiagnosisAiSolutionGroup = new UserPetAiDiagnosisAiSolutionGroup(
            aiRecommendDiagnosisList);
        //when
        List<Long> aiDiagnosisIdxOrderByPriority = userPetAiDiagnosisAiSolutionGroup.getWorryIdxOrderByPriority();
        //then
        assertThat(aiDiagnosisIdxOrderByPriority).isEqualTo(List.of(2l, 0l, 1l));
    }
}