package com.pood.server.service.pet_solution.group;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.fixture.meta.GrainSizeFixture;
import com.pood.fixture.user.UserPetGrainSizeFixture;
import com.pood.server.dto.meta.user.pet.UserPetGrainSizeGroup;
import com.pood.server.entity.meta.GrainSize;
import com.pood.server.entity.user.UserPetGrainSize;
import com.pood.server.service.pet_solution.group.UserPetGrainSizeAiSolutionGroup.GrainSizeStr;
import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Predicate;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserPetGrainSizeAiSolutionGroupTest {

    private final static double UNIT_SIZE = 0.7;


    private final UserPetGrainSizeGroup userPetGrainSizeGroup = new UserPetGrainSizeGroup();

    @Test
    void isUnitSizeCompatibility() {
        List<GrainSize> inputEntityList = List.of(
            new GrainSize(1, "name", 1.0, 1.5, "title", "url", 1, LocalDateTime.now(),
                LocalDateTime.now()),
            new GrainSize(1, "name", 1.0, 1.5, "title", "url", 2, LocalDateTime.now(),
                LocalDateTime.now()),
            new GrainSize(1, "name", 1.0, 1.9, "title", "url", 3, LocalDateTime.now(),
                LocalDateTime.now())
        );
        final List<UserPetGrainSize> userPetGrainSizeList = userPetGrainSizeGroup.saveAllObjects(
            inputEntityList, 1);

        UserPetGrainSizeAiSolutionGroup userPetGrainSizeAiSolutionGroup = new UserPetGrainSizeAiSolutionGroup(
            userPetGrainSizeList);
        assertThat(userPetGrainSizeAiSolutionGroup.isUnitSizeCompatibility(1.8)).isTrue();
    }

    @Test
    void isWetFeedCompatibility() {
        List<GrainSize> inputEntityList = List.of(
            new GrainSize(1, "name", 1.0, 1.5, "title", "url", 1, LocalDateTime.now(),
                LocalDateTime.now()),
            new GrainSize(1, "name", 1.0, 1.5, "title", "url", 2, LocalDateTime.now(),
                LocalDateTime.now()),
            new GrainSize(3, "name", 1.0, 1.9, "title", "url", 3, LocalDateTime.now(),
                LocalDateTime.now())
        );
        final List<UserPetGrainSize> userPetGrainSizeList = userPetGrainSizeGroup.saveAllObjects(
            inputEntityList, 1);

        UserPetGrainSizeAiSolutionGroup userPetGrainSizeAiSolutionGroup = new UserPetGrainSizeAiSolutionGroup(
            userPetGrainSizeList);
        assertThat(userPetGrainSizeAiSolutionGroup.isWetFeedCompatibility(true)).isTrue();
    }

    @Test
    @DisplayName("유저펫의 사료 기호성(알갱이 사이즈) 반환 리스트 필터링 - 습식이 아닐 경우 건식 알갱이 기호성만 반환")
    void wetFeedTypeFiltering() {
        //given
        List<GrainSize> grainSizeList = GrainSizeFixture.allGrainSize();
        List<UserPetGrainSize> userPetGrainSizeList = UserPetGrainSizeFixture.allGrainSize();
        UserPetGrainSizeAiSolutionGroup group = new UserPetGrainSizeAiSolutionGroup(
            userPetGrainSizeList);

        //when
        Predicate<UserPetGrainSize> isNotWetType = UserPetGrainSize::isNotWetType;
        List<GrainSizeStr> fitEachUnitSize = group.isFitEachUnitSize(UNIT_SIZE, grainSizeList,
            isNotWetType);

        //then
        assertThat(fitEachUnitSize)
            .extracting("grainSizeName")
            .containsOnly(
                UserPetGrainSizeFixture.ofLarge().getName(),
                UserPetGrainSizeFixture.ofMedium().getName(),
                UserPetGrainSizeFixture.ofSmall().getName()
            );
    }

    @Test
    @DisplayName("유저펫의 사료 기호성(알갱이 사이즈) 반환 리스트 필터링 - 습식일경우 습식 기호성만 반환")
    void notWetFeedTypeFiltering() {
        //given
        List<GrainSize> grainSizeList = GrainSizeFixture.allGrainSize();
        List<UserPetGrainSize> userPetGrainSizeList = UserPetGrainSizeFixture.allGrainSize();
        UserPetGrainSizeAiSolutionGroup group = new UserPetGrainSizeAiSolutionGroup(
            userPetGrainSizeList);

        //when
        Predicate<UserPetGrainSize> isNotWetType = UserPetGrainSize::isWetType;
        List<GrainSizeStr> fitEachUnitSize = group.isFitEachUnitSize(UNIT_SIZE, grainSizeList,
            isNotWetType);

        //then
        assertThat(fitEachUnitSize)
            .extracting("grainSizeName")
            .containsOnly(UserPetGrainSizeFixture.ofWetFeed().getName());
    }

    @Test
    @DisplayName("유저펫의 사료 기호성(알갱이 사이즈) 반환 리스트 필터링 - 필터링을 걸지 않을 경우 모든 기호성 반환")
    void filteringNothing() {
        //given
        List<GrainSize> grainSizeList = GrainSizeFixture.allGrainSize();
        List<UserPetGrainSize> userPetGrainSizeList = UserPetGrainSizeFixture.allGrainSize();
        UserPetGrainSizeAiSolutionGroup group = new UserPetGrainSizeAiSolutionGroup(
            userPetGrainSizeList);

        //when
        Predicate<UserPetGrainSize> isNotWetType = userPetGrainSize -> true;
        List<GrainSizeStr> fitEachUnitSize = group.isFitEachUnitSize(UNIT_SIZE, grainSizeList,
            isNotWetType);

        //then
        assertThat(fitEachUnitSize)
            .extracting("grainSizeName")
            .containsOnly(
                UserPetGrainSizeFixture.ofLarge().getName(),
                UserPetGrainSizeFixture.ofMedium().getName(),
                UserPetGrainSizeFixture.ofSmall().getName(),
                UserPetGrainSizeFixture.ofWetFeed().getName()
            );
    }
}