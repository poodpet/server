package com.pood.server.service.pet_solution.group;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.dto.meta.user.pet.UserPetAllergyGroup;
import com.pood.server.entity.meta.AllergyData;
import com.pood.server.entity.user.UserPetAllergy;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.Test;

class UserPetAllergyAiSolutionGroupTest {

    private final UserPetAllergyGroup userPetAllergyGroup = new UserPetAllergyGroup();

    @Test
    void isPetAllergyCompatibility() {
        //given
        List<AllergyData> inputEntityList = List.of(
            new AllergyData(1, "소고기", 1, true, "url", 1, LocalDateTime.now(), LocalDateTime.now()),
            new AllergyData(2, "name2", 2, true, "url", 2, LocalDateTime.now(),
                LocalDateTime.now()),
            new AllergyData(3, "name3", 3, true, "url", 3, LocalDateTime.now(), LocalDateTime.now())
        );
        //when
        final List<UserPetAllergy> userPetAllergies = userPetAllergyGroup.saveAllObjects(
            inputEntityList, 1);
        UserPetAllergyAiSolutionGroup userPetAllergyAiSolutionGroup = new UserPetAllergyAiSolutionGroup(
            userPetAllergies);
        assertThat(userPetAllergyAiSolutionGroup.isPetAllergyCompatibility("소고기", "닭고기")).isTrue();
    }
}