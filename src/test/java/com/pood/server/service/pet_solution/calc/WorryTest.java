package com.pood.server.service.pet_solution.calc;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.dto.meta.user.pet.UserPetAllergyGroup;
import com.pood.server.dto.meta.user.pet.UserPetGrainSizeGroup;
import com.pood.server.entity.meta.AllergyData;
import com.pood.server.entity.meta.Feed;
import com.pood.server.entity.meta.GrainSize;
import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.meta.PetDoctorFeedDescription;
import com.pood.server.entity.meta.Product.PackageType;
import com.pood.server.entity.user.UserPet;
import com.pood.server.entity.user.UserPetAiDiagnosis;
import com.pood.server.entity.user.UserPetAllergy;
import com.pood.server.entity.user.UserPetGrainSize;
import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.UserPetBaseData;
import com.pood.server.service.pet_solution.UserPetCalcDog;
import com.pood.server.service.pet_solution.group.UserPetAiDiagnosisAiSolutionGroup;
import com.pood.server.service.pet_solution.group.UserPetAllergyAiSolutionGroup;
import com.pood.server.service.pet_solution.group.UserPetGrainSizeAiSolutionGroup;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class WorryTest {

    Worry worry;

    @BeforeEach
    void setUp() {
        final UserPetAllergyGroup userPetAllergyGroup = new UserPetAllergyGroup();
        final UserPetGrainSizeGroup userPetGrainSizeGroup = new UserPetGrainSizeGroup();
        List<UserPetAiDiagnosis> aiRecommendDiagnosisList = List.of(
            new UserPetAiDiagnosis(0, 20, 0, "알레르기", "501", 501, 1L, 2),
            new UserPetAiDiagnosis(1, 20, 1, "눈", "501", 422, 1L, 3),
            new UserPetAiDiagnosis(2, 20, 2, "뼈/관절/근육", "501", 423, 1L, 1)
        );
        UserPetAiDiagnosisAiSolutionGroup userPetAiDiagnosisAiSolutionGroup = new UserPetAiDiagnosisAiSolutionGroup(
            aiRecommendDiagnosisList);

        List<AllergyData> allergyList = List.of(
            new AllergyData(1, "소고기", 1, true, "url", 1, LocalDateTime.now(), LocalDateTime.now()),
            new AllergyData(2, "name2", 2, true, "url", 2, LocalDateTime.now(),
                LocalDateTime.now()),
            new AllergyData(3, "name3", 3, true, "url", 3, LocalDateTime.now(), LocalDateTime.now())
        );
        final List<UserPetAllergy> userPetAllergies = userPetAllergyGroup.saveAllObjects(
            allergyList, 1);
        UserPetAllergyAiSolutionGroup userPetAllergyAiSolutionGroup = new UserPetAllergyAiSolutionGroup(
            userPetAllergies);

        List<GrainSize> grainSizes = List.of(
            new GrainSize(1, "name", 1.0, 1.5, "title", "url", 1, LocalDateTime.now(),
                LocalDateTime.now()),
            new GrainSize(2, "name", 1.0, 1.5, "title", "url", 2, LocalDateTime.now(),
                LocalDateTime.now()),
            new GrainSize(3, "name", 1.0, 1.9, "title", "url", 3, LocalDateTime.now(),
                LocalDateTime.now())
        );
        final List<UserPetGrainSize> userPetGrainSizeList = userPetGrainSizeGroup.saveAllObjects(
            grainSizes, 1);

        UserPetGrainSizeAiSolutionGroup userPetGrainSizeAiSolutionGroup = new UserPetGrainSizeAiSolutionGroup(
            userPetGrainSizeList);

        Pet pet = new Pet(1, "믹스 우선 정렬", "품종영어", "pcTag", "pcSize", 1, null,
            null, null, null, null, null, null
            , LocalDateTime.now(), LocalDateTime.now());
        final UserPet userPet = new UserPet(1, "allergy", 1, 1, 1, 1, "펫이름", LocalDate.now(), 1, 0);

        UserPetCalcDog userPetCalcDog = new UserPetCalcDog();
        UserPetBaseData userPetBaseData = new UserPetBaseData(userPet,
            userPetGrainSizeAiSolutionGroup,
            userPetAllergyAiSolutionGroup, userPetAiDiagnosisAiSolutionGroup, pet, userPetCalcDog, Collections.emptyList());

        PetDoctorFeedDescription petDoctorFeedDescription = PetDoctorFeedDescription.builder()
            .idx(12).petDoctorIdx(10)
            .ctIdx(0).ctSubIdx(0).productIdx(12)
            .baseScore(40).position1("1").position2("2").position3("-")
            .ardGroup122(1).ardGroup421(1).ardGroup501(1).ardGroup201(1).ardGroup301(1)
            .ardGroup961(1).ardGroup241(1).ardGroup121(1).ardGroup941(1).ardGroup422(1).ardGroup145(1)
            .ardGroup125(1).ardGroup942(1).ardGroup126(1).ardGroup601(1).ardGroup423(1).ardGroup502(1)
            .build();
        Feed feed = Feed.builder()
            .idx(1).productIdx(12)
            .prMoisture(10d).prProtein(34d).prFat(13d).prAsh(0d)
            .prFiber(0d).prCarbo(27.5d).amArginine(2.68d).amHistidine(0.8d)
            .amIsoleucine(1.39d).amLeucine(2.35d).amLysine(2.14d).amMetCys(1.07d)
            .amMethionine(0.6d).amPheTyr(2.53d).amPhenylanlanine(1.32d).amThreonine(1.36d)
            .amTryptophan(0.37d).amValine(1.64d).amCystine(0.47d).amTyrosine(1.21d)
            .amLCarnitine(0d).amGlutamicAcid(0d).miCalcium(0.8d).miPhosphours(0.6d)
            .miPotassium(0.81d).miSodium(0.26d).miChloride(0.3d).miMagnessium(0.159d)
            .miIron(266d).miCopper(26.7).miManganese(30.5d).miZinc(231d)
            .miIodine(2.25).miSelenium(0.53).miCaPh(1.33).viVitaminA(61423.9).viVitaminD(2159.63)
            .viVitaminE(400.0).viVitaminB1(29.81).viVitaminB2(14.81).viVitaminB3(250.06)
            .viVitaminB5(28.54).viVitaminB6(14.13).viVitaminB7(0.0).viVitaminB9(3.1)
            .viVitaminB12(0.08).viCholine(1932.14).viVitaminC(0d).viVitaminK3(0d)
            .faOmega3(1.25).faEpa(0d).faDha(0d).faEpaDha(-1d).faOmega6(3.75)
            .faLinoleicacid(4.03).faArachidonicAcid(0d).faALinoleicacid(0d)
            .faOmega63(3d).otTaurine(0.19).otGlucosamine(250d).otChondroitin(200d).otMsm(0d)
            .otProbiotics("0")
            .build();

        ProductBaseData productBaseData = ProductBaseData.builder()
            .idx(12)
            .uuid("3f4e6f94-11b3-49dd-b599-c99a78361795")
            .brand(null)
            .productName("웰니스 코어 스몰브리드 헬시 웨이트 1.8kg")
            .productCode("PDFE02211")
            .barcode("076344884439").pcIdx(1).ctIdx(0).ctSubIdx(0).ctSubName("건식사료")
            .showIndex(1).tag("-").allNutrients(0)
            .mainProperty("칠면조").tasty("칠면조").productVideo("-1")
            .calorie(3425).feedTarget(1).feedType("A").glutenFree(1)
            .animalProtein("칠면조,닭")
            .vegetableProtein("감자,완두콩").aafco("M")
            .singleProtein(0).ingredients("test").unitSize(1.0)
            .ingredientsSearch("test").noticeDesc("-")
            .noticeTitle("-").packageType(PackageType.NORMAL).cupWeight(92)
            .isRecommend(1).grainFree(0)
            .avaQuantity(5).quantity(2).weight(1800).approve(true)
            .snack(null)
            .feed(feed)
            .petDoctorFeedDescription(petDoctorFeedDescription)
            .build();

        worry = new Worry(userPetBaseData, productBaseData);
    }

    @Test
    void getProductTypeScorePercent() {
        assertThat(worry.getProductTypeScorePercent()).isEqualTo(30);
    }

    @Test
    void getScore() {
        assertThat(worry.getScore()).isEqualTo(100);
    }

    @Test
    void calcProductTypeScore() {
        assertThat(worry.calcProductTypeScore()).isEqualTo(30);
    }


}