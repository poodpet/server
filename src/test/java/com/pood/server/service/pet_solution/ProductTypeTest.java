package com.pood.server.service.pet_solution;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class ProductTypeTest {

    @Test
    void getProductTypeByIdx() {
        assertThat(ProductType.getProductTypeByIdx(0)).isEqualTo(ProductType.FEED);
    }

}