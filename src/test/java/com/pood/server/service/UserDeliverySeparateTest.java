package com.pood.server.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.pood.server.entity.user.UserDeliveryAddress;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.repository.user.UserDeliveryAddressRepository;
import com.pood.server.util.DeliveryRemoteType;
import com.pood.server.util.UserStatus;
import java.time.LocalDateTime;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class UserDeliverySeparateTest extends MockTests {

    @Mock
    private UserDeliveryAddressRepository userDeliveryAddressRepository;

    @InjectMocks
    private UserDeliverySeparate userDeliverySeparate;

    @Test
    @DisplayName("삭제 했을때 남은 배송지가 1개인 경우 기본배송지로 변경")
    void onlyOneAddressThenChangeDefault() {
        //given
        UserInfo userInfo = new UserInfo(
            1,
            "82f44cf0-0b9e-44e4-801a-64772e485c2d",
            "nickName",
            "email",
            "password",
            1,
            UserStatus.ACTIVE,
            "name",
            1000,
            "phone",
            "referralCode",
            true,
            LocalDateTime.now(),
            true, LocalDateTime.now(),
            true, LocalDateTime.now(),
            "key", 1, "tokenArn",
            "mlName", 0.0, 1,
            1, LocalDateTime.now(),
            LocalDateTime.now(), LocalDateTime.now(),
            null, false, null,
            true, null,
            true, null
        );

        //when
        final List<UserDeliveryAddress> mockList = List.of(
            UserDeliveryAddress.builder()
                .address("address")
                .defaultType(false)
                .detailAddress("detailAddress")
                .name("name")
                .nickname("nickname")
                .zipcode("zipcode")
                .inputType(false)
                .remoteType(DeliveryRemoteType.NORMAL)
                .phoneNumber("01023121231")
                .userInfo(userInfo)
                .build()
        );
        when(userDeliveryAddressRepository.findAllByUserInfo(any()))
            .thenReturn(mockList);

        userDeliverySeparate.onlyOneAddressThenChangeDefault(userInfo);

        //then
        Assertions.assertThat(mockList.get(0).isDefaultType()).isTrue();

    }
}