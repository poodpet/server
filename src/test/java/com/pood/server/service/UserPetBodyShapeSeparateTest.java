package com.pood.server.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.pood.server.entity.user.BodyShape;
import com.pood.server.entity.user.UserPet;
import com.pood.server.entity.user.UserPetBodyShapeDiary;
import com.pood.server.repository.user.UserPetBodyShapeDiaryRepository;
import com.pood.server.util.date.DateUtil;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UserPetBodyShapeSeparateTest {

    @Mock
    UserPetBodyShapeDiaryRepository userPetBodyShapeDiaryRepository;

    @InjectMocks
    UserPetBodyShapeSeparate userPetBodyShapeSeparate;

    @Test
    @DisplayName("주차 첫 체형 정보 등록 - 체형 테스트")
    void newCreateBodyShapeManagement() {

        //given
        UserPet userPet = mock(UserPet.class);
        when(userPet.getIdx()).thenReturn(1);

        BodyShape bodyShape = BodyShape.SKINNY;

        //given
        when(userPetBodyShapeDiaryRepository.getUserPetBodyShape(anyInt(), any())).thenReturn(
            Optional.empty());

        //when
        userPetBodyShapeSeparate.recordBodyManagement(userPet, bodyShape);

        //then
        verify(userPetBodyShapeDiaryRepository).save(any());
    }

    @Test
    @DisplayName("주차 체형 정보가 존재할때 등록 시 기존데이터 업데이트")
    void createUpdateBodyShapeManagement() {

        //given
        UserPet userPet = mock(UserPet.class);
        when(userPet.getIdx()).thenReturn(1);

        BodyShape originBodyShape = BodyShape.NORMAL;

        BodyShape updateBodyShape = BodyShape.OBESITY;

        UserPetBodyShapeDiary entity = UserPetBodyShapeDiary.builder()
            .userPet(userPet)
            .bodyShape(originBodyShape)
            .baseDate(
                LocalDate.now().plusDays(
                    DateUtil.of(LocalDate.now()).getCompareDayOfWeekNumber(DayOfWeek.FRIDAY)))
            .build();

        //given
        when(userPetBodyShapeDiaryRepository.getUserPetBodyShape(anyInt(), any())).thenReturn(
            Optional.of(entity));

        //when
        userPetBodyShapeSeparate.recordBodyManagement(userPet, updateBodyShape);

        //then
        verify(userPetBodyShapeDiaryRepository, never()).save(any());

        assertThat(entity.getBodyShape()).isEqualTo(updateBodyShape);
    }

    @Test
    @DisplayName("유저 펫의 체형 정보가 없을 때 빈 엔티티 반환")
    void checkNullSafeBodyShape() {

        when(userPetBodyShapeDiaryRepository
            .findFirstByUserPetIdxAndBodyShapeIsNotNullOrderByBaseDateDesc(anyInt())
        ).thenReturn(Optional.empty());

        assertThat(userPetBodyShapeSeparate.findRecentInfoOrElseNull(1).getBodyShape()).isNull();
    }
}