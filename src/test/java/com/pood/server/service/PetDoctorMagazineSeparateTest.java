package com.pood.server.service;

import com.pood.server.entity.meta.PetDoctorMagazineCategory;
import com.pood.server.repository.meta.PetDoctorMagazineCategoryRepository;
import com.pood.server.service.doctor.magazine.PetDoctorMagazineCategorySeparate;
import java.time.LocalDateTime;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class PetDoctorMagazineSeparateTest {


    @Mock
    private PetDoctorMagazineCategoryRepository petDoctorMagazineCategoryRepository;

    @InjectMocks
    private PetDoctorMagazineCategorySeparate petDoctorMagazineCategorySeparate;


    @Test
    @DisplayName("카테고리 리스트 반환 테스트")
    void findAllByOrderByPriorityAsc() {

        List<PetDoctorMagazineCategory> list = List.of(
            new PetDoctorMagazineCategory(1L, "test_1", 2, "https:...", LocalDateTime.now()),
            new PetDoctorMagazineCategory(2L, "test_1", 1, "https:...", LocalDateTime.now()));

        BDDMockito.when(petDoctorMagazineCategoryRepository.findAllByOrderByPriorityAsc())
            .thenReturn(list);

        List<PetDoctorMagazineCategory> allList = petDoctorMagazineCategorySeparate.getAllList();

        Assertions.assertThat(allList.get(0).getName()).isEqualTo("test_1");
    }
}