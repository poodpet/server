package com.pood.server.service;

import com.pood.server.entity.meta.Brand;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.meta.BrandRepository;
import java.time.LocalDateTime;
import java.util.Optional;
import org.assertj.core.api.AbstractThrowableAssert;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;


class BrandSeparateTest extends MockTests {

    @Mock
    private BrandRepository brandRepository;

    @InjectMocks
    private BrandSeparate brandSeparate;

    @Test
    @DisplayName("브랜드 정보 성공 조회")
    void success_brand_info() {

        //객체 생성 given
        Brand brand = new Brand(1, "고스비", "gosbi", "스페인",
            "고스비는 저온에서 균형잡힌 조리과정을 통해 영양분을 극대화 하여 만들고 있으며, 유해할 수 있는 인공 부산물이나 방부제를 사용하지 않고 천연 재료로 음식을 제조하고 있습니다. 남유럽에서 최신 설비 자체공장에서 우수한 품질의 제품을 생산하고 있으며, 항상 최고의 품질에 초점을 맞추어서 끊임없이 혁신을 지속하고 있습니다.",
            "지중해식 저자극 장수식단", "-", "고스비", "gosbi petfood", "(주)지앤원", "070-7780-4403",
            1, 0, 0, LocalDateTime.now(), LocalDateTime.now());

        BDDMockito.when(brandRepository.findById(ArgumentMatchers.anyInt()))
            .thenReturn(Optional.of(brand));

        //실행 when
        Brand brandInfo = brandSeparate.getBrandInfo(1);

        //비교 then
        Assertions.assertThat(brandInfo.getIdx()).isEqualTo(1);

    }

    @Test
    @DisplayName("브랜드 정보 예외 조회")
    void fail_brand_info() {

        //객체 생성 given
        BDDMockito.when(brandRepository.findById(ArgumentMatchers.anyInt()))
            .thenThrow(new NotFoundException("브랜드 정보를 찾을수 없습니다."));

        //실행 when
        AbstractThrowableAssert<?, ? extends Throwable> abstractThrowableAssert = Assertions.assertThatThrownBy(
            () -> brandSeparate.getBrandInfo(1));

        //비교 then
        abstractThrowableAssert
            .isInstanceOf(NotFoundException.class)
            .hasMessage("브랜드 정보를 찾을수 없습니다.");

    }

}