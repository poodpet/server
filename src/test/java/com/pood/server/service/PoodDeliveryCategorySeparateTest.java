package com.pood.server.service;

import com.pood.server.controller.response.home.delivery.PoodDeliveryCategoryDto;
import com.pood.server.repository.meta.PoodDeliveryCategoryRepository;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class PoodDeliveryCategorySeparateTest extends MockTests {

    @Mock
    PoodDeliveryCategoryRepository poodDeliveryCategoryRepository;

    @InjectMocks
    PoodDeliveryCategorySeparate poodDeliveryCategorySeparate;

    @Test
    @DisplayName("푸드 홈 카테고리 리스트 조회")
    void success_pood_delivery_category_list() {

        //객체 생성 given
        List<PoodDeliveryCategoryDto> list = List.of(
            new PoodDeliveryCategoryDto(1L, 2L, "세트상품", 1, LocalDateTime.now()),
            new PoodDeliveryCategoryDto(2L, 5L, "사료", 2, LocalDateTime.now())
        );

        BDDMockito.when(poodDeliveryCategoryRepository.getCategoryList(ArgumentMatchers.anyInt()))
            .thenReturn(list);

        //실행 when
        List<PoodDeliveryCategoryDto> categoryList = poodDeliveryCategorySeparate.getCategoryList(
            0);

        //비교 then
        Assertions.assertThat(categoryList.size()).isEqualTo(2);

    }

    @Test
    @DisplayName("푸드 홈 카테고리 없는 리스트 조회")
    void success_zero_pood_delivery_category_list() {

        //객체 생성 given
        BDDMockito.when(poodDeliveryCategoryRepository.getCategoryList(ArgumentMatchers.anyInt()))
            .thenReturn(
                Collections.emptyList());

        //실행 when
        List<PoodDeliveryCategoryDto> categoryList = poodDeliveryCategorySeparate.getCategoryList(
            1);


        //비교 then
        Assertions.assertThat(categoryList).isEmpty();

    }

}