package com.pood.server.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import com.pood.server.repository.order.OrderRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class OrderSeparateTest extends MockTests {

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderSeparate orderSeparate;

    @Test
    @DisplayName("order history 존재할 때")
    void isExistOrderHistoryThenReturnTrue() {
        //given
        final int userIdx = 958;

        //when
        when(orderRepository.existsByUserIdxAndOrderStatus(userIdx, 12))
            .thenReturn(true);

        //then
        assertThat(orderSeparate.isExistOrderHistory(userIdx)).isTrue();
    }

    @Test
    @DisplayName("order history 존재하지 않을 때")
    void isExistOrderHistoryThenReturnFalse() {
        //given
        int userIdx = 958;

        //when
        when(orderRepository.existsByUserIdxAndOrderStatus(anyInt(), anyInt()))
            .thenReturn(false);

        //then
        assertThat(orderSeparate.isExistOrderHistory(userIdx)).isFalse();
    }
}