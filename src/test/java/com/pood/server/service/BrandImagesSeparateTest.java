package com.pood.server.service;

import com.pood.server.entity.meta.Brand;
import com.pood.server.entity.meta.BrandImage;
import com.pood.server.repository.meta.BrandImageRepository;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;


class BrandImagesSeparateTest extends MockTests {

    @Mock
    private BrandImageRepository brandImageRepository;

    @InjectMocks
    private BrandImagesSeparate brandImagesSeparate;


    @Test
    @DisplayName("브랜드 정보 이미지 리스트 조회")
    void success_brand_list_by_brand_idx() {

        //객체 생성 given

        Brand brand = new Brand(1, "고스비", "gosbi", "스페인",
            "고스비는 저온에서 균형잡힌 조리과정을 통해 영양분을 극대화 하여 만들고 있으며, 유해할 수 있는 인공 부산물이나 방부제를 사용하지 않고 천연 재료로 음식을 제조하고 있습니다. 남유럽에서 최신 설비 자체공장에서 우수한 품질의 제품을 생산하고 있으며, 항상 최고의 품질에 초점을 맞추어서 끊임없이 혁신을 지속하고 있습니다.",
            "지중해식 저자극 장수식단", "-", "고스비", "gosbi petfood", "(주)지앤원", "070-7780-4403",
            1, 0, 0, LocalDateTime.now(), LocalDateTime.now());

        List<BrandImage> tempBrandImageList = List.of(
            new BrandImage(1, brand, "https://....", 0, 0, 1,
                LocalDateTime.now(), LocalDateTime.now()),
            new BrandImage(2, brand, "https://....", 0, 1, 2,
                LocalDateTime.now(), LocalDateTime.now()));

        BDDMockito.when(brandImageRepository.findAllByBrandIdx(ArgumentMatchers.anyInt()))
            .thenReturn(tempBrandImageList);

        //실행 when
        List<BrandImage> brandImageList = brandImagesSeparate.getBrandImages(
            1);

        //비교 then
        Assertions.assertThat(brandImageList.size()).isEqualTo(2);
    }


    @Test
    @DisplayName("브랜드 정보 이미지가 없는 리스트 조회")
    void success_zero_brand_list_by_brand_idx() {

        //객체 생성 given
        BDDMockito.when(brandImageRepository.findAllByBrandIdx(ArgumentMatchers.anyInt()))
            .thenReturn(Collections.emptyList());

        //실행 when
        List<BrandImage> brandImageList = brandImagesSeparate.getBrandImages(
            1);

        //비교 then
        Assertions.assertThat(brandImageList).isEmpty();

    }

}