package com.pood.server.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.any;
import static org.mockito.BDDMockito.anyInt;
import static org.mockito.BDDMockito.mock;
import static org.mockito.BDDMockito.never;
import static org.mockito.BDDMockito.verify;
import static org.mockito.BDDMockito.when;

import com.pood.server.entity.user.UserPet;
import com.pood.server.entity.user.UserPetWeightDiary;
import com.pood.server.repository.user.UserPetWeightDiaryRepository;
import com.pood.server.util.date.DateUtil;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UserPetWeightDiarySeparateTest {

    @Mock
    UserPetWeightDiaryRepository userPetWeightDiaryRepository;

    @InjectMocks
    UserPetWeightDiarySeparate userPetWeightDiarySeparate;

    @Test
    @DisplayName("주차 첫 체형 정보 등록 - 체중 테스트")
    void newCreateBodyShapeManagement(){

        //given
        UserPet userPet = mock(UserPet.class);
        when(userPet.getIdx()).thenReturn(1);

        float weight = 1.0f;

        //given
        when(userPetWeightDiaryRepository.getUserPetWeight(anyInt(),any())).thenReturn(Optional.empty());

        //when
        userPetWeightDiarySeparate.recordBodyManagement(userPet, weight);

        //then
        verify(userPetWeightDiaryRepository).save(any());
    }

    @Test
    @DisplayName("주차 체형 정보가 존재할때 등록 시 기존데이터 업데이트")
    void createUpdateBodyShapeManagement(){

        //given
        UserPet userPet = mock(UserPet.class);
        when(userPet.getIdx()).thenReturn(1);

        float originWeight = 1.0f;

        float updateWeight = 2.0f;

        UserPetWeightDiary entity = UserPetWeightDiary.builder()
            .userPet(userPet)
            .weight(originWeight)
            .baseDate(
                LocalDate.now().plusDays(
                    DateUtil.of(LocalDate.now()).getCompareDayOfWeekNumber(DayOfWeek.FRIDAY)))
            .build();

        //given
        when(userPetWeightDiaryRepository.getUserPetWeight(anyInt(),any())).thenReturn(Optional.of(entity));

        //when
        userPetWeightDiarySeparate.recordBodyManagement(userPet, updateWeight);

        //then
        verify(userPetWeightDiaryRepository, never()).save(any());

        assertThat(entity.getWeight()).isEqualTo(updateWeight);
    }
    @Test
    @DisplayName("유저 펫의 체중 정보가 없을 때 빈 엔티티 반환")
    void checkNullSafeWeightInfo() {

        when(userPetWeightDiaryRepository
            .findFirstByUserPetIdxAndWeightIsNotNullOrderByBaseDateDesc(anyInt())
        ).thenReturn(Optional.empty());

        assertThat(userPetWeightDiarySeparate.findRecentInfo(1).getWeight()).isNull();
    }
}