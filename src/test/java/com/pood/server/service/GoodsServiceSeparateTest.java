package com.pood.server.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.pood.server.entity.meta.Goods;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.meta.GoodsRepository;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.assertj.core.api.AbstractThrowableAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class GoodsServiceSeparateTest extends MockTests {

    private List<Goods> initList;

    @Mock
    private GoodsRepository goodsRepository;

    @InjectMocks
    private GoodsSeparate goodsSeparate;

    @BeforeEach
    void setUp() {
        Goods goods1 = new Goods(1, 1, 0, null, null, "고스비 테스트", null, 10000,
            10000, 0, 0, null, null, null, null, 0, LocalDateTime.now()
            , null, null, null, null, null, null, null
            , null
            , null
            , null
            , false
            , LocalDateTime.now()
            , LocalDateTime.now()
        );
        Goods goods2 = new Goods(2, 1, 0, null, null, "고스비 테스트2", null, 10000,
            10000, 0, 0, null, null, null, null, 0, LocalDateTime.now()
            , null, null, null, null, null, null, null
            , null
            , null
            , null
            , false
            , LocalDateTime.now()
            , LocalDateTime.now()
        );

        Goods goods3 = new Goods(3, 1, 0, null, null, "더미", null, 10000,
            10000, 0, 0, null, null, null, null, 0, LocalDateTime.now()
            , null, null, null, null, null, null, null
            , null
            , null
            , null
            , false
            , LocalDateTime.now()
            , LocalDateTime.now()
        );

        Goods goods4 = new Goods(4, 1, 0, null, null, "더미 데이터", null, 10000,
            10000, 0, 0, null, null, null, null, 0, LocalDateTime.now()
            , null, null, null, null, null, null, null
            , null
            , null
            , null
            , false
            , LocalDateTime.now()
            , LocalDateTime.now()
        );

        initList = List.of(goods1, goods2, goods3, goods4);
    }

    @Test
    @DisplayName("goodsCt만 null 일때 호출 쿼리문 테스트")
    void searchCategoryAndKeyword_goodsCtNull() {
        //when
        Long goodsCt = null;
        goodsSeparate.searchCategoryAndKeyword(1, "", goodsCt);

        //then
        verify(goodsRepository).findGoodsNameBySearchText(anyInt(), anyString());

    }

    @Test
    @DisplayName("statusList 만 null 일때 호출 쿼리문 테스트")
    void searchCategoryAndKeyword_statusNull() {
        //when
        Long goodsCt = 1L;
        goodsSeparate.searchCategoryAndKeyword(1, "", goodsCt);

        //then
        verify(goodsRepository).findGoodsNameBySearchText(anyInt(), anyString(), anyLong());

    }

    @Test
    @DisplayName("statusList 와 goodsCt가 null 일때 호출 쿼리문 테스트")
    void searchCategoryAndKeyword_allNull() {
        //when
        Long goodsCt = null;
        goodsSeparate.searchCategoryAndKeyword(1, "", goodsCt);

        //then
        verify(goodsRepository).findGoodsNameBySearchText(anyInt(), anyString());

    }

    @Test
    @DisplayName("goodsCt 값에 따른 호출 쿼리문 테스트(not null)")
    void searchCategoryAndKeyword_goodsCtNotNull() {
        //when
        Long goodsCt = 1L;
        goodsSeparate.searchCategoryAndKeyword(1, "", goodsCt);

        //then
        verify(goodsRepository).findGoodsNameBySearchText(anyInt(), anyString(), anyLong());

    }

    @Test
    void findByIdx() {
        //given
        when(goodsRepository.findById(anyInt()))
            .thenReturn(Optional.of(initList.get(0)));

        //when
        final Goods goods = goodsSeparate.findById(1);

        //then
        assertThat(goods).isEqualToComparingFieldByField(initList.get(0));
    }

    @Test
    void findByIdxException() {
        //given
        when(goodsRepository.findById(anyInt()))
            .thenThrow(new NotFoundException("해당하는 딜 상품이 없습니다."));

        //when
        final AbstractThrowableAssert<?, ? extends Throwable> expected = assertThatThrownBy(
            () -> goodsSeparate.findById(1));

        //then
        expected
            .isInstanceOf(NotFoundException.class)
            .hasMessage("해당하는 딜 상품이 없습니다.");
    }
}