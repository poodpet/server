package com.pood.server.service.notice;

import com.pood.server.entity.meta.NoticeType;
import com.pood.server.repository.meta.NoticeTypeRepository;
import java.util.Optional;
import org.assertj.core.api.AbstractThrowableAssert;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class NoticeTypeSeparateTest {

    @Mock
    NoticeTypeRepository noticeTypeRepository;

    @InjectMocks
    NoticeTypeSeparate noticeTypeSeparate;

    @Test
    @DisplayName("공지사항 타입 조회")
    void getSystemType() {

        NoticeType noticeType = BDDMockito.mock(NoticeType.class);
        BDDMockito.when(noticeTypeRepository.findByCode(Mockito.anyString()))
            .thenReturn(Optional.ofNullable(noticeType));

        BDDMockito.when(noticeType.getTypeName()).thenReturn("test");

        NoticeType systemType = noticeTypeSeparate.getSystemType();

        Assertions.assertThat(noticeType.getTypeName()).isEqualTo("test");
    }

    @Test
    @DisplayName("공지사항 타입 조회시 예외 처리")
    void getSystemType_Exception() {

        BDDMockito.when(noticeTypeRepository.findByCode(Mockito.anyString()))
            .thenReturn(Optional.empty());

        final AbstractThrowableAssert<?, ? extends Throwable> expected = Assertions.assertThatThrownBy(
            () -> noticeTypeSeparate.getSystemType());

        expected
            .isInstanceOf(NullPointerException.class)
            .hasMessage("해당 공지사항 타입을 찾을수 없습니다.");

    }
}