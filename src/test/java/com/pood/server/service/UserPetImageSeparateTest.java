package com.pood.server.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.pood.server.api.file.uploader.StorageService;
import com.pood.server.entity.user.UserPetImage;
import com.pood.server.exception.NotFoundException;
import com.pood.server.repository.user.UserPetImageRepository;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.assertj.core.api.AbstractThrowableAssert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

class UserPetImageSeparateTest extends MockTests {

    @Mock
    private UserPetImageRepository userPetImageRepository;

    @Mock
    private StorageService storageService;

    @InjectMocks
    private UserPetImageSeparate userPetImageSeparate;

    @Test
    @DisplayName("find by id 정상 동작")
    void findByIdx() {
        //given
        UserPetImage userPetImage = new UserPetImage(1, 1, "url", 0, LocalDateTime.now(),
            LocalDateTime.now());

        when(userPetImageRepository.findById(anyInt()))
            .thenReturn(Optional.of(userPetImage));

        //when
        final UserPetImage expected = userPetImageSeparate.findByIdx(1);

        //then
        assertThat(expected).isEqualTo(userPetImage);
    }

    @Test
    @DisplayName("find by id 찾지 못했을 때 예외")
    void findByIdxException() {
        //given
        when(userPetImageRepository.findById(anyInt()))
            .thenThrow(new NotFoundException("해당하는 이미지를 찾을 수 없습니다."));

        //when
        final AbstractThrowableAssert<?, ? extends Throwable> abstractThrowableAssert = assertThatThrownBy(
            () -> userPetImageSeparate.findByIdx(1));
        //then
        abstractThrowableAssert.isInstanceOf(NotFoundException.class)
            .hasMessageContaining("해당하는 이미지를 찾을 수 없습니다.");
    }

    @Test
    @DisplayName("유저 펫 이미지 생성 테스트")
    void saveUserPetImage() throws Exception {
        //given
        final UserPetImage userPetImage = new UserPetImage(1, 1, "url", 0, LocalDateTime.now(),
            LocalDateTime.now());
        final String url = "url";
        final List<MultipartFile> multipartFile = List.of(new MockMultipartFile("image",
            "test.png",
            "image/png",
            "test image".getBytes()));

        //when
        when(userPetImageRepository.save(any(UserPetImage.class)))
            .thenReturn(userPetImage);

        when(storageService.multiPartFileStore(multipartFile.get(0), "user_pet"))
            .thenReturn(url);

        userPetImageSeparate.saveUserPetImage(1, multipartFile);

        //then
        verify(storageService).multiPartFileStore(multipartFile.get(0), "user_pet");
    }

}