package com.pood.server.service;

import static org.mockito.BDDMockito.anyInt;

import com.pood.server.entity.meta.Notice;
import com.pood.server.entity.meta.NoticeType;
import com.pood.server.entity.meta.mapper.notice.NoticeDetailMapper;
import com.pood.server.entity.meta.mapper.notice.NoticePageMapper;
import com.pood.server.repository.meta.NoticeRepository;
import com.pood.server.service.notice.NoticeSeparate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
class NoticeSeparateTest {

    @Mock
    private NoticeRepository noticeRepository;

    @InjectMocks
    private NoticeSeparate noticeSeparate;

    @Test
    @DisplayName("공지사항 리스트 반환 테스트")
    void getNoticePage() {
        PageRequest page = PageRequest.of(1, 1);

        Page<NoticePageMapper> noticePageMapper = new PageImpl<>(
            List.of(new NoticePageMapper(1, "시스템", "title", LocalDateTime.now())));

        BDDMockito.when(noticeRepository.getNoticePage(page))
            .thenReturn(noticePageMapper);

        Page<NoticePageMapper> noticePage = noticeSeparate.getNoticePage(page);

        Assertions.assertThat(noticePage.getContent().get(0).getTitle()).isEqualTo("title");
    }

    @Test
    @DisplayName("공지사항 상세 정보 반환 테스트(정상)")
    void getNoticeDetail() {
        Optional<NoticeDetailMapper> noticeOptional = Optional.ofNullable(
            new NoticeDetailMapper(1, "title", "contents", LocalDateTime.now()));

        BDDMockito.when(noticeRepository.getNoticeDetail(anyInt()))
            .thenReturn(noticeOptional);

        Optional<NoticeDetailMapper> noticeDetail = noticeSeparate.getNoticeDetail(anyInt());

        Assertions.assertThat(noticeDetail.get().getTitle()).isEqualTo("title");

    }

    @Test
    @DisplayName("공지사항 팝업 테스트(정상 데이터가 출력 됬을 경우)")
    void getNoticePopupInfoOrNull() {

        Optional<Notice> noticeOptional = Optional.ofNullable(
            new Notice(1, null, "uuid", "title", "contents", true,
                LocalDateTime.now(), LocalDateTime.now(), false, null));
        NoticeType type = BDDMockito.mock(NoticeType.class);
        BDDMockito.when(noticeRepository.findTopByIsVisibleTrueAndNoticeTypeAndIsDeletedFalseOrderByIdxDesc(type))
            .thenReturn(noticeOptional);

        Optional<Notice> noticePopupInfoOrNull = noticeSeparate.getNoticePopupInfoByTypeOrNull(type);
        Assertions.assertThat(noticePopupInfoOrNull.get().getTitle()).isEqualTo("title");
    }

    @Test
    @DisplayName("공지사항 팝업 테스트(Null 이 들어왔을 경우)")
    void getNoticePopupInfoOrNull2() {

        Optional<Notice> noticeOptional = Optional.ofNullable(null);
        NoticeType type = BDDMockito.mock(NoticeType.class);

        BDDMockito.when(noticeRepository.findTopByIsVisibleTrueAndNoticeTypeAndIsDeletedFalseOrderByIdxDesc(type))
            .thenReturn(noticeOptional);

        Optional<Notice> noticePopupInfoOrNull = noticeSeparate.getNoticePopupInfoByTypeOrNull(type);
        Assertions.assertThat(noticePopupInfoOrNull.isEmpty()).isTrue();
    }
}