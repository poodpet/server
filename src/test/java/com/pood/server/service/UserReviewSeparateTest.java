package com.pood.server.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import com.pood.server.entity.user.UserReview;
import com.pood.server.exception.CustomStatusException;
import com.pood.server.exception.ErrorMessage;
import com.pood.server.repository.user.UserReviewRepository;
import java.time.LocalDateTime;
import java.util.Optional;
import org.assertj.core.api.AbstractThrowableAssert;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class UserReviewSeparateTest extends MockTests {

    @Mock
    private UserReviewRepository userReviewRepository;

    @InjectMocks
    private UserReviewSeparate userReviewSeparate;

    private UserReview userReview;

    @BeforeEach
    void setUp() {
        userReview = new UserReview(1, 1, 1, 1, "123",
            1, "goodsName", 1, 1, "reviewText",
            1, 0, 0, LocalDateTime.now(), LocalDateTime.now());
    }

    @Test
    @DisplayName("userReview Id로 조회 정상 리턴")
    void findByIdxSuccess() {
        //given
        when(userReviewRepository.findById(anyInt()))
            .thenReturn(Optional.ofNullable(userReview));

        //when
        final UserReview expected = userReviewSeparate.findByIdx(1);

        //then
        assertThat(expected.getIdx()).isEqualTo(userReview.getIdx());
    }

    @Test
    @DisplayName("userReview 찾지 못했을 때 예외")
    void findByIdx() {
        //given
        when(userReviewRepository.findById(anyInt()))
            .thenThrow(CustomStatusException.badRequest(ErrorMessage.REVIEW));

        //when
        final AbstractThrowableAssert<?, ? extends Throwable> abstractThrowableAssert = Assertions.assertThatThrownBy(
            () -> userReviewSeparate.findByIdx(1));

        //then
        abstractThrowableAssert.hasMessage("400 BAD_REQUEST \"리뷰가 존재하지 않습니다.\"");
    }

}