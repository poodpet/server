package com.pood.server.service;

import com.pood.server.controller.response.home.rank.PoodRankCategoryDto;
import com.pood.server.repository.meta.PoodRankCategoryRepository;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class PoodRankCategorySeparateTest extends MockTests {

    @Mock
    PoodRankCategoryRepository poodRankCategoryRepository;

    @InjectMocks
    PoodRankCategorySeparate poodRankCategorySeparate;

    @Test
    @DisplayName("푸드 랭크 홈 카테고리 리스트 조회")
    void success_pood_rank_catagory_list() {

        //객체 생성 given
        List<PoodRankCategoryDto> list = List.of(
            new PoodRankCategoryDto(1L, 2L, "세트상품", 1, LocalDateTime.now()),
            new PoodRankCategoryDto(2L, 5L, "사료", 2, LocalDateTime.now())
        );
        BDDMockito.when(poodRankCategoryRepository.getCategoryList(ArgumentMatchers.anyInt()))
            .thenReturn(list);

        //실행 when
        List<PoodRankCategoryDto> catagoryList = poodRankCategorySeparate.getCategoryList(
            0);

        //비교 then
        Assertions.assertThat(catagoryList.size()).isEqualTo(2);

    }

    @Test
    @DisplayName("푸드 랭크 홈 카테고리 없는 리스트 조회")
    void success_zero_pood_rank_catagory_list() {

        //객체 생성 given
        BDDMockito.when(poodRankCategoryRepository.getCategoryList(ArgumentMatchers.anyInt()))
            .thenReturn(
                Collections.emptyList());

        //실행 when
        List<PoodRankCategoryDto> catagoryList = poodRankCategorySeparate.getCategoryList(
            1);

        //비교 then
        Assertions.assertThat(catagoryList).isEmpty();

    }

}
