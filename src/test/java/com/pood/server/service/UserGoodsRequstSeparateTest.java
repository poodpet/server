package com.pood.server.service;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.pood.server.entity.user.UserInfo;
import com.pood.server.exception.AlreadyExistException;
import com.pood.server.repository.user.UserGoodsRequstRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UserGoodsRequstSeparateTest {


    @Mock
    UserGoodsRequstRepository userGoodsRequstRepository;

    @InjectMocks
    UserGoodsRequstSeparate userGoodsRequstSeparate;

    @Test
    @DisplayName("중복된 상품 요청시 AlreadyExistException 발생 테스트")
    void save_AlreadyExistException() {
        UserInfo userInfo = Mockito.mock(UserInfo.class);

        Mockito.when(userGoodsRequstRepository.existsByUserInfoIdxAndGoodsIdx(Mockito.anyInt(),
            Mockito.anyInt())).thenReturn(true);

        assertThatThrownBy(() -> userGoodsRequstSeparate.save(userInfo, 1))
            .isInstanceOf(AlreadyExistException.class)
            .hasMessageContaining("이미 요청하신 상품 입니다.");
    }

}