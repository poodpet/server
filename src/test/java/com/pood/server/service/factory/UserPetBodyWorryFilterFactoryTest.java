package com.pood.server.service.factory;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.entity.meta.dto.ProductAndWorryFilterDto;
import com.pood.server.entity.user.BodyShape;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserPetBodyWorryFilterFactoryTest {

    @Test
    @DisplayName("체중 맞춤 사료 추천 필터링 테스트 : 마름 → 습식사료 중 칼로리 높은 것 (사료)")
    void getFilteringRequireBySkinnyBody() {

        //given
        BodyShape userPetBody = BodyShape.SKINNY;
        ProductAndWorryFilterDto conditionsForCustomizedProducts = new UserPetBodyWorryFilterFactory(userPetBody).create();

        //then
        List<Long> wetFoodGoodsIdxCatAndDog = List.of(7L, 53L);

        long feedGoodsCtIdx = 2;

        assertThat(conditionsForCustomizedProducts.getGoodsCtIdx()).isEqualTo(feedGoodsCtIdx);
        assertThat(conditionsForCustomizedProducts.getGoodsSubCtIdxList()).isEqualTo(
            wetFoodGoodsIdxCatAndDog);
        assertThat(conditionsForCustomizedProducts.getWorryIdx()).isNull();

    }

    @Test
    @DisplayName("체중 맞춤 사료 추천 필터링 테스트 : 적정 → 영양제 중 면역고민 우선")
    void getFilteringRequireByNormalBody() {

        //given
        BodyShape userPetBody = BodyShape.NORMAL;
        ProductAndWorryFilterDto conditionsForCustomizedProducts =  new UserPetBodyWorryFilterFactory(userPetBody).create();

        //then
        int immunityWorry = 17;
        long nutrientsGoodsCtIdx = 4;

        assertThat(conditionsForCustomizedProducts.getGoodsCtIdx()).isEqualTo(nutrientsGoodsCtIdx);
        assertThat(conditionsForCustomizedProducts.getGoodsSubCtIdxList()).isNull();
        assertThat(conditionsForCustomizedProducts.getWorryIdx()).isEqualTo(immunityWorry);

    }

    @Test
    @DisplayName("체중 맞춤 사료 추천 필터링 테스트 : 비만 → 사료 중 다이어트 고민 우선순위")
    void getFilteringRequireByObesityBody() {

        //given
        BodyShape userPetBody = BodyShape.OBESITY;
        ProductAndWorryFilterDto conditionsForCustomizedProducts = new UserPetBodyWorryFilterFactory(userPetBody).create();

        //then
        int dietWorries = 6;
        long feedGoodsCtIdx = 2;

        assertThat(conditionsForCustomizedProducts.getGoodsCtIdx()).isEqualTo(feedGoodsCtIdx);
        assertThat(conditionsForCustomizedProducts.getGoodsSubCtIdxList()).isNull();
        assertThat(conditionsForCustomizedProducts.getWorryIdx()).isEqualTo(dietWorries);

    }

    @Test
    @DisplayName("체중 맞춤 사료 추천 필터링 테스트 : 고도 비만 → 처방식 다이어트 필터링 + 다이어트 고민 우선순위")
    void getFilteringRequireByHighObesityBody() {

        //given
        BodyShape userPetBody = BodyShape.HIGHLY_OBESE;
        ProductAndWorryFilterDto conditionsForCustomizedProducts =  new UserPetBodyWorryFilterFactory(userPetBody).create();

        //then
        List<Long> prescriptionGoodsIdxCatAndDog = List.of(101L, 102L);
        int dietWorries = 6;
        long feedGoodsCtIdx = 2;

        assertThat(conditionsForCustomizedProducts.getGoodsCtIdx()).isEqualTo(feedGoodsCtIdx);
        assertThat(conditionsForCustomizedProducts.getGoodsSubCtIdxList()).isEqualTo(
            prescriptionGoodsIdxCatAndDog);
        assertThat(conditionsForCustomizedProducts.getWorryIdx()).isEqualTo(dietWorries);

    }
}