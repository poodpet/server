package com.pood.server.service.factory;

import com.pood.server.service.order.DeliveryFeeCalc;
import com.pood.server.service.order.DeliveryFeeCalcFirstPurchaseIslands;
import com.pood.server.service.order.DeliveryFeeCalcFirstPurchaseNotIslands;
import com.pood.server.service.order.DeliveryFeeCalcIslandsAndBasicFree;
import com.pood.server.service.order.DeliveryFeeCalcIslandsAndBasicPaid;
import com.pood.server.service.order.DeliveryFeeCalcNotIslandsAndBasicFree;
import com.pood.server.service.order.DeliveryFeeCalcNotIslandsAndBasicPaid;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class DeliveryFeeFactoryTest {

    @Test
    @DisplayName("첫 구매 O , 산간지역 X 인스턴스 생성 테스트")
    void DeliveryFeeCalcFirstPurchaseNotIslands() {
        DeliveryFeeCalc deliveryFeeCalc = new DeliveryFeeFactory()
            .create(false, true, 30000);
        Assertions.assertThat(deliveryFeeCalc instanceof DeliveryFeeCalcFirstPurchaseNotIslands)
            .isTrue();

    }

    @Test
    @DisplayName("첫 구매 O , 산간지역 O 인스턴스 생성 테스트")
    void DeliveryFeeCalcFirstPurchaseIslands() {
        DeliveryFeeCalc deliveryFeeCalc = new DeliveryFeeFactory()
            .create(false, false, 30000);
        Assertions.assertThat(deliveryFeeCalc instanceof DeliveryFeeCalcFirstPurchaseIslands)
            .isTrue();
    }

    @Test
    @DisplayName("30000이하 구매 , 산간지역 X 인스턴스 생성 테스트")
    void DeliveryFeeCalcNotIslandsAndBasicPaid() {
        DeliveryFeeCalc deliveryFeeCalc = new DeliveryFeeFactory()
            .create(true, true, 10000);
        Assertions.assertThat(deliveryFeeCalc instanceof DeliveryFeeCalcNotIslandsAndBasicPaid)
            .isTrue();
    }

    @Test
    @DisplayName("30000이상 구매 , 산간지역 X 인스턴스 생성 테스트")
    void DeliveryFeeCalcNotIslandsAndBasicFree() {
        DeliveryFeeCalc deliveryFeeCalc = new DeliveryFeeFactory()
            .create(true, true, 30000);
        Assertions.assertThat(deliveryFeeCalc instanceof DeliveryFeeCalcNotIslandsAndBasicFree)
            .isTrue();
    }
    @Test
    @DisplayName("30000이상 구매 , 산간지역 O 인스턴스 생성 테스트")
    void DeliveryFeeCalcIslandsAndBasicPaid() {
        DeliveryFeeCalc deliveryFeeCalc = new DeliveryFeeFactory()
            .create(true, false, 10000);
        Assertions.assertThat(deliveryFeeCalc instanceof DeliveryFeeCalcIslandsAndBasicPaid)
            .isTrue();
    }
    @Test
    @DisplayName("30000이상 구매 , 산간지역 O 인스턴스 생성 테스트")
    void DeliveryFeeCalcIslandsAndBasicFree() {
        DeliveryFeeCalc deliveryFeeCalc = new DeliveryFeeFactory()
            .create(true, false, 30000);
        Assertions.assertThat(deliveryFeeCalc instanceof DeliveryFeeCalcIslandsAndBasicFree)
            .isTrue();
    }
}