package com.pood.server.config;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class AES256Test {

    @BeforeEach
    void test() throws Exception {
        AES256.keySpecGenerator();
    }

    @Test
    @DisplayName("암/복호화 테스트")
    void encryptDecryptTest() throws Exception {
        //given
        String userName = "YcRq0uf";
        final String test = AES256.aesEncode(userName);

        final String result = AES256.aesDecode(test);
        Assertions.assertThat(result).isEqualTo(userName);
    }

    @Test
    @DisplayName("암/복호화 테스트_실패시 null 반환(성공)")
    void getAesDecodeOrNull() throws Exception {
        //given
        String userName = "YcRq0uf";
        final String test = AES256.aesEncode(userName);

        final String result = AES256.getAesDecodeOrNull(test);
        Assertions.assertThat(result).isEqualTo(userName);
    }

    @Test
    @DisplayName("암/복호화 테스트_실패시 null 반환(실패)")
    void getAesDecodeOrNull2() {
        //given
        String userName = "YcRq0uf";
        final String result = AES256.getAesDecodeOrNull(userName);

        Assertions.assertThat(result).isNull();
    }

}