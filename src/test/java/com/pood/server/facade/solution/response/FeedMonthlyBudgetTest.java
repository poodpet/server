package com.pood.server.facade.solution.response;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class FeedMonthlyBudgetTest {

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 5})
    @DisplayName("사료비교 한달예산 응답값 : goods saleStatus 가 4가 아니면 푸드 상품")
    void monthlyBudgetUnhandledProductTest_isPood(Integer status) {

        FeedMonthlyBudgetResponse result = FeedMonthlyBudgetResponse.of(1, 1, status);

        //then
        assertThat(result.isPoodGoods()).isTrue();
    }

    @Test
    @DisplayName("사료비교 한달예산 응답값 : goods saleStatus 가 4 이면 미취급 상품")
    void monthlyBudgetUnhandledProductTest_isNotPood() {

        FeedMonthlyBudgetResponse result = FeedMonthlyBudgetResponse.of(1, 1, 4);

        //then
        assertThat(result.isPoodGoods()).isFalse();
    }
}