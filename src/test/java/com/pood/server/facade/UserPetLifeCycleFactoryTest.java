package com.pood.server.facade;

import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.meta.dto.user_pet_life_cycle.UserPetLifeCycle;
import com.pood.server.entity.user.UserPet;
import com.pood.server.service.factory.UserPetLifeCycleFactory;
import com.pood.server.service.pet_solution.UserPetBaseData;
import java.time.LocalDate;
import java.time.LocalDateTime;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserPetLifeCycleFactoryTest {

    @Test
    @DisplayName("빅사이즈 시니어 강아지의 수면 정보가 잘 나오는지 확인")
    void big_senior_info() {
        UserPetBaseData userPetData = new UserPetBaseData(UserPet.builder()
            .pcId(1).pscId(1).petBirth(LocalDate.now().minusYears(10)).build(),
            null, null, null,
            new Pet(1, "믹스 우선 정렬", "품종영어", "pcTag", "대형견", 1, null,
                null, null, null, null, null, null
                , LocalDateTime.now(), LocalDateTime.now()),
            null, null);
        UserPetLifeCycle userPetLifeCycle = new UserPetLifeCycleFactory(userPetData).create();
        Assertions.assertThat(userPetLifeCycle.getSleep())
            .isEqualTo("시니어 시기에 접어들면, 활동성이 떨어지고 자는 시간이 길어져요. 질병이 있으면 자는 시간이 더 길어질 수 있습니다.");
    }

    @Test
    @DisplayName("소형 키튼의 수면 정보가 잘 나오는지 확인")
    void kitten_sleep_info() {
        UserPetBaseData userPetData = new UserPetBaseData(UserPet.builder()
            .pcId(2).pscId(1).petBirth(LocalDate.now().minusDays(50)).build(),
            null, null, null,
            new Pet(1, "믹스 우선 정렬", "품종영어", "pcTag", "소형", 2, null,
                null, null, null, null, null, null
                , LocalDateTime.now(), LocalDateTime.now()),
            null, null);
        UserPetLifeCycle userPetLifeCycle = new UserPetLifeCycleFactory(userPetData).create();
        Assertions.assertThat(userPetLifeCycle.getSleep()).isEqualTo(
            "하루 중 20시간 이상 자는 데 시간을 보낼 거예요. 그러니 너무 많이 잔다고 해서 걱정하지 마세요. 성장에 필요한 수면이랍니다.");
    }
}