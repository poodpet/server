package com.pood.server.facade.notice;

import static org.mockito.ArgumentMatchers.anyInt;

import com.pood.server.entity.meta.Notice;
import com.pood.server.entity.meta.NoticeType;
import com.pood.server.entity.meta.mapper.notice.NoticeDetailMapper;
import com.pood.server.entity.meta.mapper.notice.NoticePageMapper;
import com.pood.server.exception.NotFoundException;
import com.pood.server.facade.notice.response.NoticeDetailResponse;
import com.pood.server.facade.notice.response.NoticePageResponse;
import com.pood.server.service.notice.NoticeSeparate;
import com.pood.server.service.notice.NoticeTypeSeparate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
class NoticeFacadeTest {

    @Mock
    private NoticeSeparate noticeSeparate;

    @Mock
    private NoticeTypeSeparate noticeTypeSeparate;

    @InjectMocks
    private NoticeFacade noticeFacade;

    @Test
    @DisplayName("공지사항 리스트 반환 테스트")
    void getNoticePage() {
        PageRequest page = PageRequest.of(1, 1);

        Page<NoticePageMapper> noticePageMapper = new PageImpl<>(
            List.of(new NoticePageMapper(1, "시스템", "title", LocalDateTime.now())));

        BDDMockito.when(noticeSeparate.getNoticePage(page))
            .thenReturn(noticePageMapper);

        Page<NoticePageResponse> noticePage = noticeFacade.getNoticePage(page);

        Assertions.assertThat(noticePage.getContent().get(0).getTitle()).isEqualTo("title");
    }

    @Test
    @DisplayName("공지사항 상세 정보 반환 테스트")
    void getNoticeDetail() {

        Optional<NoticeDetailMapper> notice =
            Optional.of(new NoticeDetailMapper(1, "title", "contents", LocalDateTime.now()));

        BDDMockito.when(noticeSeparate.getNoticeDetail(anyInt()))
            .thenReturn(notice);

        NoticeDetailResponse noticeDetail = noticeFacade.getNoticeDetail(anyInt());

        Assertions.assertThat(noticeDetail.getTitle()).isEqualTo("title");

    }

    @Test
    @DisplayName("공지사항 상세 정보 반환 테스트(해당 공지사항이 없는 경우)")
    void getNoticeDetail_Error() {
        Optional<NoticeDetailMapper> noticeOptional = Optional.ofNullable(null);

        BDDMockito.when(noticeSeparate.getNoticeDetail(anyInt()))
            .thenReturn(noticeOptional);

        Assertions.assertThatThrownBy(() -> noticeFacade.getNoticeDetail(anyInt()))
            .isInstanceOf(NotFoundException.class)
            .hasMessage("해당 하는 공지 사항이 존재하지 않습니다.");

    }

    @Test
    @DisplayName("공지사항 팝업 테스트")
    void getNoticePopupInfoOrNull() {

        Optional<Notice> notice =
            Optional.of(new Notice(1, null, "uuid", "title", "contents", true,
                LocalDateTime.now(), LocalDateTime.now(), false, null));

        NoticeType type = BDDMockito.mock(NoticeType.class);

        BDDMockito.when(noticeSeparate.getNoticePopupInfoByTypeOrNull(type))
            .thenReturn(notice);

        BDDMockito.when(noticeTypeSeparate.getSystemType())
            .thenReturn(type);

        NoticeDetailResponse noticePopupInfoOrNull = noticeFacade.getNoticePopupInfo();
        Assertions.assertThat(noticePopupInfoOrNull.getTitle()).isEqualTo("title");

    }
}