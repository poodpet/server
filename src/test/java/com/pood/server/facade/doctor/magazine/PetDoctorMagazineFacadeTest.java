package com.pood.server.facade.doctor.magazine;

import com.pood.server.entity.meta.PetDoctorMagazineCategory;
import com.pood.server.entity.meta.mapper.doctor.PetDoctorMagazineMapper;
import com.pood.server.facade.doctor.magazine.response.PetDoctorMagazineCtResponse;
import com.pood.server.facade.doctor.magazine.response.PetDoctorMagazineResponse;
import com.pood.server.service.doctor.magazine.PetDoctorMagazineCategorySeparate;
import com.pood.server.service.doctor.magazine.PetDoctorMagazineSeparate;
import java.time.LocalDateTime;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
class PetDoctorMagazineFacadeTest {

    @Mock
    private PetDoctorMagazineCategorySeparate petDoctorMagazineCtSeparate;

    @Mock
    private PetDoctorMagazineSeparate petDoctorMagazineSeparate;

    @InjectMocks
    private PetDoctorMagazineFacade petDoctorMagazineFacade;

    @Test
    @DisplayName("카테고리 리스트 반환 테스트")
    void getCategory() {
        List<PetDoctorMagazineCategory> list = List.of(
            new PetDoctorMagazineCategory(1L, "test_1", 2, "https:...", LocalDateTime.now()),
            new PetDoctorMagazineCategory(2L, "test_1", 1, "https:...", LocalDateTime.now()));

        BDDMockito.when(petDoctorMagazineCtSeparate.getAllList())
            .thenReturn(list);

        List<PetDoctorMagazineCtResponse> allList = petDoctorMagazineFacade.getCategory();

        Assertions.assertThat(allList.get(0).getName()).isEqualTo("test_1");
    }

    @Test
    @DisplayName("수의사의 발견 리스트 반환 테스트")
    void getMagazineList() {
        PageRequest page = PageRequest.of(1, 1);

        Page<PetDoctorMagazineMapper> petDoctorMagazineMappers = new PageImpl<>(
            List.of(new PetDoctorMagazineMapper(1, "url", "기초", "test1", LocalDateTime.now())));

        BDDMockito.when(petDoctorMagazineSeparate.getMagazineListByCtIdx(null, 1, page))
            .thenReturn(petDoctorMagazineMappers);

        Page<PetDoctorMagazineResponse> magazineList = petDoctorMagazineFacade.getMagazineList(null,
            1,
            page);
        Assertions.assertThat(magazineList.getContent().get(0).getTitle()).isEqualTo("test1");
    }
}