package com.pood.server.facade.doctor.magazine.response;

import com.pood.server.entity.meta.PetDoctorMagazineCategory;
import java.time.LocalDateTime;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class PetDoctorMagazineCtResponseTest {

    @Test
    @DisplayName("PetDoctorMagazineCtResponse 생성 테스트")
    void createPetDoctorMagazineCtResponse() {
        PetDoctorMagazineCtResponse petDoctorMagazineCtResponse =
            new PetDoctorMagazineCtResponse(
                new PetDoctorMagazineCategory(1L, "test", 1, "http...", LocalDateTime.now()));
        Assertions.assertThat(petDoctorMagazineCtResponse.getName())
            .isEqualTo("test");
    }
}