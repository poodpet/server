package com.pood.server.facade.doctor.magazine.response;

import java.time.LocalDateTime;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class PetDoctorMagazineResponseTest {

    @Test
    @DisplayName("PetDoctorMagazineResponse 생성 테스트")
    void createPetDoctorMagazineResponse() {
        PetDoctorMagazineResponse petDoctorMagazineResponse =
            new PetDoctorMagazineResponse(1, "http:..", "ctName", "title", LocalDateTime.now());
        Assertions.assertThat(petDoctorMagazineResponse.getTitle())
            .isEqualTo("title");
    }
}