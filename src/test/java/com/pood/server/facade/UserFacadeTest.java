package com.pood.server.facade;

import static org.mockito.BDDMockito.anyInt;
import static org.mockito.BDDMockito.anyString;
import static org.mockito.BDDMockito.mock;
import static org.mockito.BDDMockito.times;
import static org.mockito.BDDMockito.verify;
import static org.mockito.Mockito.when;

import com.pood.server.entity.meta.Goods;
import com.pood.server.entity.user.UserInfo;
import com.pood.server.service.GoodsSeparate;
import com.pood.server.service.UserRequestRestockSeparate;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UserFacadeTest {

    @Mock
    GoodsSeparate goodsSeparate;

    @Mock
    UserRequestRestockSeparate userRequestRestockSeparate;

    @InjectMocks
    UserFacade userFacade;

    @Test
    @DisplayName("재입고 요청 인수 테스트")
    void restock() {

        //when
        UserInfo userInfo = mock(UserInfo.class);
        when(userInfo.getUserUuid()).thenReturn("");

        Goods goods = mock(Goods.class);
        when(goods.getIdx()).thenReturn(1);
        when(goodsSeparate.findById(anyInt())).thenReturn(goods);

        userFacade.requestReStock(1, userInfo);

        verify(goodsSeparate).findById(1);
        verify(userRequestRestockSeparate, times(1)).insertRequestRestock(anyInt(), anyString());
    }
}