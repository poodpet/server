package com.pood.server.facade;

import com.pood.server.entity.meta.Pet;
import com.pood.server.entity.meta.dto.user_pet_worry_manaer.UserPetWorryManager;
import com.pood.server.entity.user.UserPet;
import com.pood.server.service.factory.UserPetWorryManagerFactory;
import com.pood.server.service.pet_solution.UserPetBaseData;
import java.time.LocalDate;
import java.time.LocalDateTime;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserPetWorryManagerFactoryTest {

    @Test
    @DisplayName("강아지 피부 고민 솔루션 확인")
    void userPetWorryManagerDogBySkinWorry() {
        UserPetBaseData userPetData = new UserPetBaseData(UserPet.builder()
            .pcId(1).pscId(1).petBirth(LocalDate.now().minusYears(10)).build(),
            null, null, null,
            new Pet(1, "믹스 우선 정렬", "품종영어", "pcTag", "대형견", 1, null,
                null, null, null, null, null, null
                , LocalDateTime.now(), LocalDateTime.now()),
            null, null);
        UserPetWorryManager userPetWorryManager = new UserPetWorryManagerFactory(userPetData,501).create();
        Assertions.assertThat(userPetWorryManager.getDailyLife())
            .isEqualTo("잦은 목욕은 피부에 더 자극을 주고 건조하게 만들 수 있어요. 2주일에 한 번 이하의 목욕을 추천해요. 피부 알러지는 동물병원에서 정밀한 검진을 받아보아야 그 원인을 찾을 수 있어요. 보습제는 세라마이드 성분이 함유된 제품이 도움이 될 거예요.");
    }

    @Test
    @DisplayName("고양이 피부 고민 솔루션 확인")
    void userPetWorryManagerCatBySkinWorry() {
        UserPetBaseData userPetData = new UserPetBaseData(UserPet.builder()
            .pcId(2).pscId(1).petBirth(LocalDate.now().minusDays(50)).build(),
            null, null, null,
            new Pet(1, "믹스 우선 정렬", "품종영어", "pcTag", "소형", 2, null,
                null, null, null, null, null, null
                , LocalDateTime.now(), LocalDateTime.now()),
            null, null);
        UserPetWorryManager userPetWorryManager = new UserPetWorryManagerFactory(userPetData,501).create();
        Assertions.assertThat(userPetWorryManager.getDailyLife()).isEqualTo(
            "되도록 고양이는 목욕을 추천하지 않아요. 목욕을 해야 하는 상황이라면 EWG 점수가 낮아서 안전하다고 설명하는 샴푸를 선택해보세요. 보습제는 세라마이드 성분이 함유된 제품이 도움이 됩니다.");
    }

    @Test
    @DisplayName("고양이 발을 자주 핥아요 솔루션 확인")
    void userPetWorryManagerCatByLickFeet() {
        UserPetBaseData userPetData = new UserPetBaseData(UserPet.builder()
            .pcId(2).pscId(1).petBirth(LocalDate.now().minusDays(50)).build(),
            null, null, null,
            new Pet(1, "믹스 우선 정렬", "품종영어", "pcTag", "소형", 2, null,
                null, null, null, null, null, null
                , LocalDateTime.now(), LocalDateTime.now()),
            null, null);
        UserPetWorryManager userPetWorryManager = new UserPetWorryManagerFactory(userPetData,502).create();
        Assertions.assertThat(userPetWorryManager.getDailyLife()).isEqualTo(
            "최근에 사료를 교체한 후에 유독 발을 많이 핥는다면 음식 알러지가 있을 가능성이 높아요. 자주 소독해주는 것도 방법이지만 근본적인 해결방법이 될 수는 없어요. 동물병원에 방문하여 감염은 없는 지, 음식 알러지 혹은 행동학적인 원인인지 파악하는 것이 중요해요.");
    }
}