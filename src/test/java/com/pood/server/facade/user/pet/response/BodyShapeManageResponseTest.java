package com.pood.server.facade.user.pet.response;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.entity.user.BodyShape;
import java.time.LocalDate;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

class BodyShapeManageResponseTest {

    @Test
    @DisplayName("체형관리 정보가 없을 때 → empty 생성자 값 테스트")
    void checkEmpty() {
        BodyShapeManageResponse response = BodyShapeManageResponse.emptyInfo();

        assertThat(response.getBodyShapeRecordAgoWeek()).isZero();
        assertThat(response.getWeightRecordAgoWeek()).isZero();
        assertThat(response.getWeight()).isZero();
        assertThat(response.getBodyType()).isEmpty();
    }

    @Test
    @DisplayName("체형 값이 없을 때 null → 빈값 convert")
    void convertNullSafe() {
        BodyShapeManageResponse response = BodyShapeManageResponse.of(1f, LocalDate.now(), null,
            LocalDate.now());

        assertThat(response.getBodyType()).isEmpty();
    }

    @ParameterizedTest
    @EnumSource
    @DisplayName("BodyShape null 이 아닐 때 type 값 반환")
    void convertBodyShapeType(BodyShape bodyShape) {
        BodyShapeManageResponse response = BodyShapeManageResponse.of(1f, LocalDate.now(), bodyShape,
            LocalDate.now());

        assertThat(response.getBodyType()).isEqualTo(bodyShape.getType());
    }

    @Test
    @DisplayName("체중 값이 없을 때 null → 0.0f convert")
    void convertNullSafeWeight() {
        BodyShapeManageResponse response = BodyShapeManageResponse.of(1f, LocalDate.now(), null,
            LocalDate.now());

        assertThat(response.getBodyType()).isEmpty();
    }
}