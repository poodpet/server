package com.pood.server.facade.user.pet.request;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.pood.server.entity.user.BodyShape;
import com.pood.server.exception.NotFoundException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;

class BodyShapeManageCreateRequestTest {

    @ParameterizedTest
    @EnumSource
    @DisplayName("체형 타입 string to enum 테스트 (성공)")
    void successConvertBodyShape(final BodyShape bodyShape) {
        BodyShapeManageCreateRequest request = new BodyShapeManageCreateRequest( 1, 5.4f,
            bodyShape.getType());

        assertThat(request.getBodyShape()).isInstanceOf(BodyShape.class);
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " ", "E", "F", "AE", "a", " A "})
    @DisplayName("체형 타입 string to enum 테스트 (실패)")
    void failConvertBodyShape(final String bodyShape) {
        assertThatThrownBy(() -> new BodyShapeManageCreateRequest( 1, 5.4f, bodyShape))
            .isInstanceOf(NotFoundException.class)
            .hasMessageMatching(String.format("해당하는 체형 타입 \\[%s\\]가 존재하지 않습니다.", bodyShape));
    }

}