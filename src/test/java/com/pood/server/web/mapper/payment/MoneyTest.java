package com.pood.server.web.mapper.payment;

import java.math.BigDecimal;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class MoneyTest {

    @Test
    @DisplayName("Money 클레스 비교")
    void equals() {
        Money money = new Money(10000);
        Assertions.assertThat(money).isEqualTo(new Money(10000));
    }

    @Test
    @DisplayName("Money 복제")
    void copy() {
        Money moneyOrg = new Money(10000);
        Money moneyCopy = moneyOrg.copy();
        Assertions.assertThat(moneyOrg.getIntValue()).isEqualTo(moneyCopy.getIntValue());
    }

    @Test
    @DisplayName("Money 더하기")
    void plus() {
        Money money = new Money(10000);
        Assertions.assertThat(money.plus(new Money(1000)).getIntValue()).isEqualTo(11000);
    }

    @Test
    @DisplayName("Money 빼기")
    void minus() {
        Money money = new Money(10000);
        Assertions.assertThat(money.minus(new Money(1000)).getIntValue()).isEqualTo(9000);
    }

    @Test
    @DisplayName("Money 곱하기")
    void multiply() {
        Money money = new Money(10000);
        Assertions.assertThat(money.multiply(new BigDecimal(2)).getIntValue()).isEqualTo(20000);
    }

    @Test
    @DisplayName("Money 나누기")
    void divide() {
        Money money = new Money(10000);
        Assertions.assertThat(money.divide(new BigDecimal(2)).getIntValue()).isEqualTo(5000);
    }

    @Test
    @DisplayName("Money 비율 계산")
    void ratio() {
        Money money = new Money(10000);
        Assertions.assertThat(money.ratio(0.1d).getIntValue()).isEqualTo(1000);
    }

    @Test
    @DisplayName("Money 값이 1보다 아래인지 확인")
    void isLessThanOne() {
        Money money = new Money(new BigDecimal("0.5"));
        Assertions.assertThat(money.isLessThanOne()).isTrue();
    }

}