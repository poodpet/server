package com.pood.server.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.pood.server.exception.NotFoundException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;

class RestockStateTest {

    @ParameterizedTest
    @EnumSource(RestockState.class)
    @DisplayName("재입고 요청 상태 string to enum (성공)")
    void find(final RestockState state) {

        String name = state.name();

        assertThat(RestockState.find(name)).isEqualTo(state);
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " ", "아무거나"})
    @DisplayName("재입고 요청 상태 string to enum (실패)")
    void find_fail(final String name) {

        assertThatThrownBy(() -> RestockState.find(name))
            .isInstanceOf(NotFoundException.class)
            .hasMessageContaining(name + "는 재입고 요청 데이터에 잘못된 상태입니다.");

    }
}