package com.pood.server.util;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class DeliveryRemoteTypeTest {

    @Test
    @DisplayName("존재하지 않는 경우 예외 발생")
    void test() {
        Assertions.assertThatThrownBy(() -> DeliveryRemoteType.ofLegacyType(5))
            .isInstanceOf(RuntimeException.class)
            .hasMessage("해당하는 배송지역 타입 [5]가 없습니다.");
    }

}