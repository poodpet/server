package com.pood.server.util.date;

import java.time.LocalDate;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class PetWeightDateUtilTest {

    @Test
    @DisplayName("해당 년도의 달의 주 계산")
    void getWeekOfMonth(){
        LocalDate nowDate = LocalDate.of(2022,6,16);
        LocalDate startDate = LocalDate.of(nowDate.getYear(), 1, 1);

        Assertions.assertThat(PetWeightDateUtil.of(startDate, nowDate)
                .getWeekOfMonth().get(1).getSecond()).isEqualTo(2);
    }

}