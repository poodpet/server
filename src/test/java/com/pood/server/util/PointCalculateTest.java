package com.pood.server.util;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.entity.log.LogUserSavePoint;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.Test;

class PointCalculateTest {

    @Test
    void totalPoints() {
        //given
        final List<LogUserSavePoint> logUserSavePoints = List.of(
            new LogUserSavePoint(1, "03de3e3b-c29d-4574-ab39-42f5a390634d", 1,
                "pointName", 1, 1, 1, 1, "text", 1000, LocalDateTime.now(), LocalDateTime.now()),
            new LogUserSavePoint(1, "03de3e3b-c29d-4574-ab39-42f5a390634d", 1,
                "pointName", 1, 1, 1, 1, "text", 1000, LocalDateTime.now(), LocalDateTime.now())
        );
        //when
        final int totalSavePoint = PointCalculate.totalSavePoint(logUserSavePoints);

        //then
        assertThat(totalSavePoint).isEqualTo(2000);
    }
}