package com.pood.server.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.mock;
import static org.mockito.BDDMockito.when;

import com.pood.server.util.date.DateUtil;
import java.time.Clock;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

class DateUtilTest {

    @Test
    @DisplayName("해당 날짜의 주에 대한 일 비교")
    void getCompareDayOfWeekValue() {

        int compareDay = DateUtil.of(LocalDate.of(2022, 6, 16))
            .getCompareDayOfWeekNumber(DayOfWeek.FRIDAY);
        assertThat(LocalDate.of(2022, 6, 16).plusDays(compareDay).getDayOfWeek())
            .isEqualTo(DayOfWeek.FRIDAY);
    }

    @ParameterizedTest
    @ValueSource(ints = {2, 3, 4, 5, 6, 7, 8})
    @DisplayName("그 주의 금요일 날짜 반환")
    void convertWeeklyFridayDate(final int day) {

        Clock clockMock = mock(Clock.class);
        when(clockMock.instant()).thenReturn(Instant.parse("2023-01-27T00:00:00Z"));
        when(clockMock.getZone()).thenReturn(ZoneId.systemDefault());

        LocalDate friday = LocalDate.now(clockMock).with(TemporalAdjusters.firstInMonth(DayOfWeek.FRIDAY));

        LocalDate date = LocalDate.of(2023, 1, day);

        assertThat(DateUtil.of(date).convertWeeklyFridayDate()).isEqualTo(friday);
    }

    @ParameterizedTest
    @CsvSource({
        "2022-12-24T00:00:00Z, -5",
        "2023-01-27T00:00:00Z, 0",
        "2023-01-29T00:00:00Z, 0",
        "2023-01-23T00:00:00Z, 0",
        "2023-01-30T00:00:00Z, 1",
    })
    @DisplayName("주차 차이 테스트")
    void calculateWeekDiffFromToday(final String dateStr, final int result) {

        Clock clockMock = mock(Clock.class);

        when(clockMock.instant()).thenReturn(Instant.parse(dateStr));
        when(clockMock.getZone()).thenReturn(ZoneId.systemDefault());

        LocalDate anyDate = LocalDate.of(2023, 1, 27);
        DateUtil dateUtil = DateUtil.of(clockMock, anyDate);

        assertThat(dateUtil.checkWeekAgoFromToday()).isEqualTo(result);

    }
}