package com.pood.server.entity.group;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class EventExceptUserGroupTest {

    @Test
    @DisplayName("리스트에 데이터가 없을경우 null이 나오는지 확인")
    void getListIsNull() {
        EventExceptUserGroup exceptUserGroup = new EventExceptUserGroup();
        assertThat(exceptUserGroup.getList()).isNull();
    }

    @Test
    @DisplayName("리스트에 데이터가 정상적으로 들어가는지 확인")
    void getList() {
        EventExceptUserGroup exceptUserGroup = new EventExceptUserGroup(List.of(1, 2, 2), null);
        assertThat(exceptUserGroup.getList().size()).isEqualTo(2);
    }

}