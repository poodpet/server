package com.pood.server.entity.group;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.entity.user.UserPetWeightDiary;
import com.pood.server.facade.user.pet.response.UserPetWeightResponse;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.data.util.Pair;

class UserPetWeightGroupTest {

    UserPetWeightDiaryGroup group;
    List<Pair<LocalDate, Integer>> weekOfMonth;

    @BeforeEach
    void setUp() {
        group = new UserPetWeightDiaryGroup(
            List.of(
                new UserPetWeightDiary(1L, null, 3.5f, LocalDate.of(2022, 6, 16),
                    LocalDateTime.now(),
                    LocalDateTime.of(2022, 5, 4, 0, 0, 0)),
                new UserPetWeightDiary(2L, null, 4.0f, LocalDate.of(2022, 6, 9),
                    LocalDateTime.now(),
                    LocalDateTime.now()),
                new UserPetWeightDiary(3L, null, 4.5f, LocalDate.of(2022, 6, 2),
                    LocalDateTime.now(),
                    LocalDateTime.now()
                )
            )
        );
        weekOfMonth = List.of(Pair.of(LocalDate.of(2022, 6, 16), 3));
    }

    @Test
    @DisplayName("요청객체 파싱 테스트")
    void toResponse() {
        UserPetWeightGroup userPetWeightGroup = new UserPetWeightGroup(group, weekOfMonth);
        List<UserPetWeightResponse> actualList = userPetWeightGroup.toResponse();
        assertThat(actualList.get(0).getWeek()).isEqualTo(3);
    }

}
