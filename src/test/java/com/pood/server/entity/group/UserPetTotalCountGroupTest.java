package com.pood.server.entity.group;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.controller.response.pet.UserPetTotalCountResponse;
import com.pood.server.entity.user.mapper.pet.UserPetTotalCountMapper;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserPetTotalCountGroupTest {

    @Test
    @DisplayName("getResponse 값이 정상적으로 표출되는지 확인")
    void getResponse() {
        UserPetTotalCountGroup userPetTotalCountGroup = new UserPetTotalCountGroup(
            List.of(new UserPetTotalCountMapper(1, 31l),
                new UserPetTotalCountMapper(2, 62l)));
        UserPetTotalCountResponse response = userPetTotalCountGroup.getResponse();
        assertThat(response.getCatCount()).isEqualTo(62);
    }

}