package com.pood.server.entity.order.group;

import com.pood.server.entity.order.mapper.OrderBasketMapper;
import com.pood.server.entity.user.UserReview;
import java.util.Arrays;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class OrderBasketGroupTest {

    @Test
    @DisplayName("해당 객체가 비어 있는지 확인")
    void isEmpty() {
        OrderBasketGroup orderBasketGroup = new OrderBasketGroup(null);
        Assertions.assertThat(orderBasketGroup.isEmpty()).isTrue();
    }

    @Test
    @DisplayName("해당 객체가 비어 있지 않은지 확인")
    void isNotEmpty() {
        OrderBasketGroup orderBasketGroup = new OrderBasketGroup(
            Arrays.asList(new OrderBasketMapper(1, 300, "stsx", 1)));
        Assertions.assertThat(orderBasketGroup.isEmpty()).isFalse();
    }

    @Test
    @DisplayName("리뷰 작성 가능 여부")
    void isReviewPossible() {
        OrderBasketGroup orderBasketGroup = new OrderBasketGroup(
            Arrays.asList(new OrderBasketMapper(1, 300, "stsx", 1)));

        Assertions.assertThat(
                orderBasketGroup.isReviewPossible(new UserReview(1, 1, "stsx", 300, 3, "text")))
            .isFalse();
    }

}