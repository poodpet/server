package com.pood.server.entity.order.dto;

import com.pood.server.entity.order.Order;
import com.pood.server.entity.order.OrderBasket;
import com.pood.server.entity.order.OrderCancel;
import com.pood.server.entity.order.OrderDelivery;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;

class OrderBaseDataDtoTest {

    Order order;
    OrderBasket orderBasket;
    OrderCancel orderCancel;
    OrderDelivery orderDelivery;

    @BeforeEach
    void setUp() {
        order = BDDMockito.mock(Order.class);
        BDDMockito.when(order.getTotalPrice()).thenReturn(40000);
        BDDMockito.when(order.getUsedPoint()).thenReturn(10000);
        BDDMockito.when(order.getOrderPrice()).thenReturn(30000);
        orderBasket = BDDMockito.mock(OrderBasket.class);
        orderCancel = BDDMockito.mock(OrderCancel.class);
        orderDelivery = BDDMockito.mock(OrderDelivery.class);
    }

    @Test
    @DisplayName("주문에 대한 주문 상품 건 수 출력")
    void orderBasketSize() {
        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket),
            List.of(orderCancel), 0);

        Assertions.assertThat(orderBaseData.orderBasketSize()).isEqualTo(1);
    }

    @Test
    @DisplayName("주문에 대한 주문 상품의 취소 건수 출력")
    void orderCancelList() {
        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket),
            List.of(orderCancel), 0);

        Assertions.assertThat(orderBaseData.orderCancelSize()).isEqualTo(1);
    }

    @Test
    @DisplayName("요청 goods idx를 취소할 경우 해당 주문에 모든 상품이 취소되어 있는지 확인(모두 취소 됨)")
    void isAllCancel_true() {
        OrderCancel orderCancel1 = BDDMockito.mock(OrderCancel.class);
        BDDMockito.when(orderCancel1.getGoodsIdx()).thenReturn(1);

        OrderBasket orderBasket1 = BDDMockito.mock(OrderBasket.class);
        OrderBasket orderBasket2 = BDDMockito.mock(OrderBasket.class);
        BDDMockito.when(orderBasket1.getGoodsIdx()).thenReturn(1);
        BDDMockito.when(orderBasket2.getGoodsIdx()).thenReturn(2);

        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket1, orderBasket2),
            List.of(orderCancel1), 0);

        Assertions.assertThat(orderBaseData.isAllCancel(List.of(2))).isTrue();
    }

    @Test
    @DisplayName("요청 goods idx를 취소할 경우 해당 주문에 모든 상품이 취소되어 있는지 확인(모두 취소 되지 않음)")
    void isAllCancel_false() {
        OrderCancel orderCancel1 = BDDMockito.mock(OrderCancel.class);
        BDDMockito.when(orderCancel1.getGoodsIdx()).thenReturn(1);

        OrderBasket orderBasket1 = BDDMockito.mock(OrderBasket.class);
        OrderBasket orderBasket2 = BDDMockito.mock(OrderBasket.class);
        OrderBasket orderBasket3 = BDDMockito.mock(OrderBasket.class);
        BDDMockito.when(orderBasket1.getGoodsIdx()).thenReturn(1);
        BDDMockito.when(orderBasket2.getGoodsIdx()).thenReturn(2);
        BDDMockito.when(orderBasket3.getGoodsIdx()).thenReturn(3);

        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket1, orderBasket2, orderBasket3),
            List.of(orderCancel1), 0);

        Assertions.assertThat(orderBaseData.isAllCancel(List.of(2))).isFalse();
    }

    @Test
    @DisplayName("배송비가 존재하는지 확인(있음)")
    void isDeliveryFee_ture() {
        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket),
            List.of(orderCancel), 0);
        BDDMockito.when(order.getDeliveryFee()).thenReturn(0);

        Assertions.assertThat(orderBaseData.isDeliveryFee()).isTrue();
    }

    @Test
    @DisplayName("배송비가 존재하는지 확인(없음)")
    void isDeliveryFee_false() {
        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket),
            List.of(orderCancel), 1);
        BDDMockito.when(order.getDeliveryFee()).thenReturn(0);

        Assertions.assertThat(orderBaseData.isDeliveryFee()).isFalse();
    }

    @Test
    @DisplayName("배송비가 무료인지 확인(무료)")
    void isDeliveryFeeFree_true() {
        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket),
            List.of(orderCancel), 0);
        BDDMockito.when(order.getDeliveryFee()).thenReturn(0);

        Assertions.assertThat(orderBaseData.isDeliveryFeeFree()).isTrue();
    }

    @Test
    @DisplayName("배송비가 무료인지 확인(무료 아님)")
    void isDeliveryFeeFree_false() {
        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket),
            List.of(orderCancel), 0);
        BDDMockito.when(order.getDeliveryFee()).thenReturn(1);

        Assertions.assertThat(orderBaseData.isDeliveryFeeFree()).isFalse();

    }

    @Test
    @DisplayName("할인 금액을 제외 한 반환 값")
    void getRetreievePrice() {
        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket),
            List.of(orderCancel), 0);

        Assertions.assertThat(orderBaseData.getRetreievePrice(100).getLongValue())
            .isEqualTo(75);
    }

    @Test
    @DisplayName("취소 가격에 대한 전체금액의 비율")
    void getRetreieveRatio_double() {
        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket),
            List.of(orderCancel), 0);

        Assertions.assertThat(orderBaseData.getRetreieveRatio(10000))
            .isEqualTo(0.25d);
    }

    @Test
    @DisplayName("취소 가격에 대한 반환되는 포인트 값")
    void getRetreievePoint_long() {
        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket),
            List.of(orderCancel), 0);

        Assertions.assertThat(orderBaseData.getRetreievePoint(10000))
            .isEqualTo(2500);
    }

    @Test
    @DisplayName("취소 가격이 배송비와 비교하여 작을 경우")
    void isTotlaMoneyGreaterThanDeliveryFee_false() {
        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket),
            List.of(orderCancel), 1000);
        BDDMockito.when(order.getDeliveryFee()).thenReturn(1000);
        BDDMockito.when(orderBasket.getGoodsIdx()).thenReturn(1);
        BDDMockito.when(orderBasket.getGoodsPrice()).thenReturn(3100);
        BDDMockito.when(orderBasket.getQuantity()).thenReturn(1);

        Assertions.assertThat(orderBaseData.isTotalMoneyGreaterThanDeliveryFee(
            List.of(1))).isFalse();
    }

    @Test
    @DisplayName("취소 가격이 배송비와 비교하여 클경우")
    void isTotalMoneyGreaterThanDeliveryFee_true() {

        BDDMockito.when(order.getDeliveryFee()).thenReturn(1000);
        BDDMockito.when(orderBasket.getGoodsIdx()).thenReturn(1);
        BDDMockito.when(orderBasket.getGoodsPrice()).thenReturn(3000);
        BDDMockito.when(orderBasket.getQuantity()).thenReturn(1);
        OrderBasket orderBasket2 = BDDMockito.mock(OrderBasket.class);
        BDDMockito.when(orderBasket2.getGoodsIdx()).thenReturn(2);
        BDDMockito.when(orderBasket2.getGoodsPrice()).thenReturn(2000);
        BDDMockito.when(orderBasket2.getQuantity()).thenReturn(1);

        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket, orderBasket2),
            List.of(orderCancel), 0);

        Assertions.assertThat(orderBaseData.isTotalMoneyGreaterThanDeliveryFee(
            List.of(1, 2))).isTrue();
    }

    @Test
    @DisplayName("상품의 포인트를 제외한 가격을 반환한다.")
    void getGoodsPrice() {
        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket),
            List.of(orderCancel), 0);

        OrderBasket orderBasket = BDDMockito.mock(OrderBasket.class);
        BDDMockito.when(orderBasket.getGoodsPrice()).thenReturn(1000);
        BDDMockito.when(orderBasket.getQuantity()).thenReturn(2);

        Assertions.assertThat(orderBaseData.getGoodsPrice(orderBasket)).isEqualTo(1500);
    }

    @Test
    @DisplayName("상품의 포인트를 제외한 가격을 반환한다.")
    void getDeliveryFee() {
        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket),
            List.of(orderCancel), 0);

        OrderBasket orderBasket = BDDMockito.mock(OrderBasket.class);
        BDDMockito.when(orderBasket.getGoodsPrice()).thenReturn(1000);
        BDDMockito.when(orderBasket.getQuantity()).thenReturn(2);

        Assertions.assertThat(orderBaseData.getDeliveryFee(1000, 500))
            .isEqualTo(375);
    }

    @Test
    @DisplayName("배송비 보다 가격이 높은 상품이 존재함")
    void isGoodsPriceGreaterThanDeliveryFee_true() {
        OrderBasket orderBasket1 = BDDMockito.mock(OrderBasket.class);
        OrderBasket orderBasket2 = BDDMockito.mock(OrderBasket.class);
        OrderBasket orderBasket3 = BDDMockito.mock(OrderBasket.class);
        BDDMockito.when(orderBasket1.getGoodsIdx()).thenReturn(1);
        BDDMockito.when(orderBasket2.getGoodsIdx()).thenReturn(2);
        BDDMockito.when(orderBasket3.getGoodsIdx()).thenReturn(3);
        BDDMockito.when(orderBasket1.getGoodsPrice()).thenReturn(1000);
        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket1, orderBasket2, orderBasket3),
            List.of(orderCancel), 900);

        Assertions.assertThat(orderBaseData.isGoodsPriceGreaterThanDeliveryFee(
            List.of(1, 2, 3))
        ).isFalse();
    }

    @Test
    @DisplayName("배송비 보다 가격이 높은 상품이 존재하지 않음")
    void isGoodsPriceGreaterThanDeliveryFee_false() {
        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket),
            List.of(orderCancel), 0);

        Assertions.assertThat(orderBaseData.isGoodsPriceGreaterThanDeliveryFee(
            List.of(1, 2))
        ).isFalse();
    }


    @Test
    @DisplayName("상품 취소시 남은 금액이 무료 배송정책 금액보다 높다.")
    void isOrderPriceThenDefualtPrice_true() {
        OrderBasket orderBasket1 = BDDMockito.mock(OrderBasket.class);
        OrderBasket orderBasket2 = BDDMockito.mock(OrderBasket.class);
        OrderBasket orderBasket3 = BDDMockito.mock(OrderBasket.class);
        BDDMockito.when(orderBasket1.getGoodsIdx()).thenReturn(1);
        BDDMockito.when(orderBasket2.getGoodsIdx()).thenReturn(2);
        BDDMockito.when(orderBasket3.getGoodsIdx()).thenReturn(3);
        BDDMockito.when(orderBasket1.getGoodsPrice()).thenReturn(10000);
        BDDMockito.when(orderBasket1.getQuantity()).thenReturn(1);
        BDDMockito.when(orderBasket2.getGoodsPrice()).thenReturn(30000);
        BDDMockito.when(orderBasket2.getQuantity()).thenReturn(2);

        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket1, orderBasket2, orderBasket3),
            List.of(orderCancel), 900);

        Assertions.assertThat(orderBaseData.isOrderPriceThenDefualtPrice(
            List.of(1))
        ).isTrue();
    }

    @Test
    @DisplayName("상품 취소시 남은 금액이 무료 배송정책 금액보다 낮다.")
    void isOrderPriceThenDefualtPrice_false() {
        OrderBasket orderBasket1 = BDDMockito.mock(OrderBasket.class);
        OrderBasket orderBasket2 = BDDMockito.mock(OrderBasket.class);
        OrderBasket orderBasket3 = BDDMockito.mock(OrderBasket.class);
        BDDMockito.when(orderBasket1.getGoodsIdx()).thenReturn(1);
        BDDMockito.when(orderBasket2.getGoodsIdx()).thenReturn(2);
        BDDMockito.when(orderBasket3.getGoodsIdx()).thenReturn(3);
        BDDMockito.when(orderBasket1.getGoodsPrice()).thenReturn(10000);
        BDDMockito.when(orderBasket1.getQuantity()).thenReturn(3);
        BDDMockito.when(orderBasket2.getGoodsPrice()).thenReturn(30000);
        BDDMockito.when(orderBasket2.getQuantity()).thenReturn(1);

        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket1, orderBasket2, orderBasket3),
            List.of(orderCancel), 900);

        Assertions.assertThat(orderBaseData.isOrderPriceThenDefualtPrice(
            List.of(1))
        ).isFalse();
    }

    @Test
    @DisplayName("상품가격이 배송비 보다 큰경우 해당 상품에 부과되는 배송비(배송비 전액)")
    void getDeliveryFee_isTotalMoneyGreaterThanDeliveryFee() {

        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket),
            List.of(orderCancel), 0);

        Assertions.assertThat(orderBaseData.getDeliveryFee(1000, 1900)
        ).isEqualTo(1000);
    }

    @Test
    @DisplayName("상품가격이 배송비 보다 작은경우 해당 상품에 부과되는 배송비(배송비가는 상품가격만)")
    void getDeliveryFee_isNotTotalMoneyGreaterThanDeliveryFee() {
        BDDMockito.when(order.getUsedPoint()).thenReturn(0);
        BDDMockito.when(order.getOrderPrice()).thenReturn(40000);
        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket),
            List.of(orderCancel), 0);

        Assertions.assertThat(orderBaseData.getDeliveryFee(10000, 1000)
        ).isEqualTo(1000);
    }

    @Test
    @DisplayName("취소 되지 않는 상품 idx 리스트 호출")
    void getNonCancelledGoodsIdxList(){
        OrderBasket orderBasket1 = BDDMockito.mock(OrderBasket.class);
        OrderBasket orderBasket2 = BDDMockito.mock(OrderBasket.class);
        OrderBasket orderBasket3 = BDDMockito.mock(OrderBasket.class);
        BDDMockito.when(orderBasket1.getGoodsIdx()).thenReturn(1);
        BDDMockito.when(orderBasket2.getGoodsIdx()).thenReturn(2);
        BDDMockito.when(orderBasket3.getGoodsIdx()).thenReturn(3);
        BDDMockito.when(orderBasket1.getGoodsPrice()).thenReturn(10000);
        BDDMockito.when(orderBasket1.getQuantity()).thenReturn(3);
        BDDMockito.when(orderBasket2.getGoodsPrice()).thenReturn(30000);
        BDDMockito.when(orderBasket2.getQuantity()).thenReturn(1);

        OrderCancel orderCancel1 = BDDMockito.mock(OrderCancel.class);
        OrderCancel orderCancel2 = BDDMockito.mock(OrderCancel.class);
        BDDMockito.when(orderCancel1.getGoodsIdx()).thenReturn(1);
        BDDMockito.when(orderCancel2.getGoodsIdx()).thenReturn(2);

        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket1,orderBasket2,orderBasket3),
            List.of(orderCancel1,orderCancel2), 0);

        Assertions.assertThat(orderBaseData.getNonCancelledGoodsIdxList()).hasSize(1);

    }

    @Test
    @DisplayName("주문 정보를 hPay_cancel_1_2 정보 리스트로 변환")
    void getPayCancelInfoList(){
        OrderBasket orderBasket1 = BDDMockito.mock(OrderBasket.class);
        OrderBasket orderBasket2 = BDDMockito.mock(OrderBasket.class);
        OrderBasket orderBasket3 = BDDMockito.mock(OrderBasket.class);
        BDDMockito.when(orderBasket1.getGoodsIdx()).thenReturn(1);
        BDDMockito.when(orderBasket2.getGoodsIdx()).thenReturn(2);
        BDDMockito.when(orderBasket3.getGoodsIdx()).thenReturn(3);
        BDDMockito.when(orderBasket3.getQuantity()).thenReturn(2);
        BDDMockito.when(orderBasket1.getGoodsPrice()).thenReturn(10000);
        BDDMockito.when(orderBasket1.getQuantity()).thenReturn(3);
        BDDMockito.when(orderBasket2.getGoodsPrice()).thenReturn(30000);
        BDDMockito.when(orderBasket2.getQuantity()).thenReturn(1);

        OrderCancel orderCancel1 = BDDMockito.mock(OrderCancel.class);
        OrderCancel orderCancel2 = BDDMockito.mock(OrderCancel.class);
        BDDMockito.when(orderCancel1.getGoodsIdx()).thenReturn(1);
        BDDMockito.when(orderCancel2.getGoodsIdx()).thenReturn(2);

        OrderBaseDataDto orderBaseData = new OrderBaseDataDto(order, orderDelivery,
            List.of(orderBasket1,orderBasket2,orderBasket3),
            List.of(orderCancel1,orderCancel2), 0);

        Assertions.assertThat(orderBaseData.getPayCancelInfoList().get(0).getGoods_idx())
            .isEqualTo(3);
    }

}