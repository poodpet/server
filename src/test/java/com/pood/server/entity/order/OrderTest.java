package com.pood.server.entity.order;

import java.time.LocalDateTime;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class OrderTest {

    @Test
    @DisplayName("배송비가 업데이트 되었는지 확인")
    void updateDeliveryFee_getDeliveryFee() {
        Order orderInfo = new Order(1, 1, "uuid", 1, "name", "number", "device", 1000, 3000, 3000,
            2000, 1, 0, 1000, 10, 0, "", 0, 0, 0, 0, 0, "memo",
            LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now());

        orderInfo.addDeliveryFee(3000);

        Assertions.assertThat(orderInfo.getDeliveryFee()).isEqualTo(3000);

    }

}