package com.pood.server.entity.order.group;

import com.pood.server.service.factory.OrderCancelPriceInfo;
import com.pood.server.web.mapper.payment.Money;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class OrderCancelPriceInfoGroupTest {

    OrderCancelPriceInfoGroup orderCancelPriceInfoGroup;

    @BeforeEach
    void setUp() {
        OrderCancelPriceInfo orderCancelPriceInfo1 = new OrderCancelPriceInfo(5, 1, 1, new Money(1),
            1, 1, true);
        OrderCancelPriceInfo orderCancelPriceInfo2 = new OrderCancelPriceInfo(5, 1, 1, new Money(1),
            1, 1, true);

        orderCancelPriceInfoGroup = new OrderCancelPriceInfoGroup();
        orderCancelPriceInfoGroup.add(orderCancelPriceInfo1);
        orderCancelPriceInfoGroup.add(orderCancelPriceInfo2);
    }

    @Test
    @DisplayName("orderCancelPriceInfoGroup에 OrderCancelPriceInfo가 정상적으로 추가되는지 확인")
    void add() {
        OrderCancelPriceInfo orderCancelPriceInfo3 = new OrderCancelPriceInfo(1, 1, 1, new Money(1),
            1, 1, false);
        
        orderCancelPriceInfoGroup.add(orderCancelPriceInfo3);

        Assertions.assertThat(orderCancelPriceInfoGroup.getOrderCancelPriceInfoList())
            .hasSize(3);
    }

    @Test
    @DisplayName("주문 상품 원가 확인")
    void getOrgGoodsPrice() {
        Assertions.assertThat(orderCancelPriceInfoGroup.getOrgGoodsPrice())
            .isEqualTo(10);
    }

    @Test
    @DisplayName("배송비 금액 확인")
    void getDeliveryFee() {
        Assertions.assertThat(orderCancelPriceInfoGroup.getDeliveryFee())
            .isEqualTo(2);
    }

    @Test
    @DisplayName("실제 반환 금액 확인")
    void getGoodsPrice() {
        Assertions.assertThat(orderCancelPriceInfoGroup.getGoodsPrice()).isEqualTo(8);
    }

    @Test
    @DisplayName("실제 반환 포인트 확인")
    void getPoint() {
        Assertions.assertThat(orderCancelPriceInfoGroup.getPoint()).isEqualTo(2);
    }

    @Test
    @DisplayName("포인트를 제외한 반환 상품 금액 확인")
    void getRetreievePrice(){
        OrderCancelPriceInfo orderCancelPriceInfo3 = new OrderCancelPriceInfo(100, 1, 1, new Money(50),
            1, 0.5, false);

        orderCancelPriceInfoGroup.add(orderCancelPriceInfo3);
        int retreievePrice = orderCancelPriceInfoGroup.getRetreievePrice();
        Assertions.assertThat(orderCancelPriceInfoGroup.getRetreievePrice()).isEqualTo(50);

    }
}