package com.pood.server.entity.meta;

import com.pood.server.entity.meta.Product.PackageType;
import java.time.LocalDateTime;
import org.junit.jupiter.api.BeforeEach;

class ProductTest {

    Product product;

    @BeforeEach
    void setUp() {
        product = Product.builder()
            .idx(1)
            .uuid("07ad3e1a-ffaf-4022-b0a3-1cc5ce90a101")
            .brand(new Brand(1, "brandName", "en", "origin", "description",
                "brandIntro", "logo", "tag", "menuFac", "seller", "phone", 1,
                1, 1, LocalDateTime.now(), LocalDateTime.now()))
            .productName("아이보리코트")
            .productCode("PDFE00503")
            .barcode("9348601000052")
            .pcIdx(1)
            .ctIdx(0)
            .ctSubIdx(0)
            .ctSubName("")
            .showIndex(-1)
            .tag("-")
            .allNutrients(0)
            .mainProperty("연어/생선")
            .tasty("연어")
            .productVideo("-1")
            .calorie(3265).feedTarget(0).feedType("A")
            .glutenFree(1).animalProtein("오션피쉬, 연어")
            .vegetableProtein("완두콩, 감자")
            .aafco("M")
            .singleProtein(0)
            .ingredients("오션피쉬")
            .unitSize(1.4)
            .ingredientsSearch("아마씨")
            .noticeDesc("-")
            .noticeTitle("-")
            .packageType(PackageType.ZIPPER_BAG).cupWeight(92).isRecommend(1)
            .grainFree(0).avaQuantity(5).quantity(185)
            .weight(2000)
            .seller(new Seller(1L, "name"))
            .approve(true)
            .build();
    }

}