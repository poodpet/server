package com.pood.server.entity.meta;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.pood.server.exception.ClosedEventException;
import com.pood.server.exception.PauseEventException;
import java.time.LocalDateTime;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class EventInfoTest {

    @Test
    @DisplayName("이벤트 시작 전 인지 확인")
    void haveStarted() {
        final EventInfo eventInfo = new EventInfo(1, null, 26, "테스트", "테스트", "테스트", 3,
            LocalDateTime.now().plusDays(1L), LocalDateTime.now(), 0, null, null, null, null, 0, 0, 1, "테스트",
            false, LocalDateTime.now(), LocalDateTime.now());

        assertThatThrownBy(eventInfo::checkValidation)
            .isInstanceOf(ClosedEventException.class)
            .hasMessage("시작되지 않은 이벤트 입니다.");
    }

    @Test
    @DisplayName("종료된 이벤트인지 확인")
    void haveEnded() {
        EventInfo eventInfo = new EventInfo(1, null, 26, "테스트", "테스트", "테스트", 3,
            LocalDateTime.now(), LocalDateTime.now().minusDays(1L), 0, null, null, null, null, 0, 0, 1, "테스트",
            false, LocalDateTime.now(), LocalDateTime.now());

        assertThatThrownBy(eventInfo::checkValidation)
            .isInstanceOf(ClosedEventException.class)
            .hasMessage("이미 종료된 이벤트 입니다.");
    }

    @Test
    @DisplayName("일시 정지 이벤트인지 확인")
    void isPause() {
        EventInfo eventInfo = new EventInfo(1, null, 26, "테스트", "테스트", "테스트", 3,
            LocalDateTime.MIN, LocalDateTime.MAX, 0, null, null, null, null, 0, 0, 1, "테스트",
            false, LocalDateTime.now(), LocalDateTime.now());

        assertThatThrownBy(eventInfo::checkValidation)
            .isInstanceOf(PauseEventException.class)
            .hasMessage("일시 중지 된 이벤트 입니다.");
    }

    @Test
    @DisplayName("이벤트가 진행중인지")
    void isRunningTrueTest() {
        final EventInfo eventInfo = new EventInfo(1, null, 26, "테스트", "테스트", "테스트", 3,
            LocalDateTime.of(2022, 6, 1, 0, 0, 0)
            , LocalDateTime.of(2022, 7, 1, 0, 0, 0), 0, null, null, null, null, 0, 0, 1, "테스트",
            false, LocalDateTime.now(), LocalDateTime.now());

        final boolean actual = eventInfo.isRunning(LocalDateTime.of(2022, 6, 15, 0, 0, 0));

        assertThat(actual).isTrue();
    }

    @Test
    @DisplayName("이벤트가 진행중이지 않은지")
    void isRunningFalseTest() {
        final EventInfo eventInfo = new EventInfo(1, null, 26, "테스트", "테스트", "테스트", 3,
            LocalDateTime.of(2022, 6, 1, 0, 0, 0)
            , LocalDateTime.of(2022, 7, 1, 0, 0, 0), 0, null, null, null, null, 0, 0, 1, "테스트",
            false, LocalDateTime.now(), LocalDateTime.now());

        final boolean actual = eventInfo.isRunning(LocalDateTime.of(2022, 7, 2, 0, 0, 0));

        assertThat(actual).isFalse();
    }
}