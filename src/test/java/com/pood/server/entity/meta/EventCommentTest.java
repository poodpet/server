package com.pood.server.entity.meta;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class EventCommentTest {

    @Test
    @DisplayName("null이면 당첨이 아닌건지 테스트")
    void prizeTest() {
        EventComment comment = new EventComment(1L, new EventInfo(), 1, "comment", LocalDateTime.now(),
            null);

        assertThat(comment.isPrize()).isFalse();
    }

    @Test
    @DisplayName("당첨이 됐는지 테스트")
    void prizeTrueTest() {
        EventComment comment = new EventComment(1L, new EventInfo(), 1, "comment", LocalDateTime.now(),
            "O");

        assertThat(comment.isPrize()).isTrue();
    }
}