package com.pood.server.entity.meta.group;

import static org.assertj.core.api.Assertions.assertThat;

import autoparams.AutoSource;
import autoparams.Repeat;
import com.pood.server.entity.meta.dto.home.CustomRankInfoDto;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;

class CustomRankInfoGroupTest {

    @ParameterizedTest
    @AutoSource
    @Repeat(10)
    @DisplayName("커스텀 랭킹 리스트는 점수별로 정렬된 리스트를 반환")
    void rankInfoGroupByScoreSorted(List<CustomRankInfoDto> customRankInfoDtoList) {

        CustomRankInfoGroup group = new CustomRankInfoGroup(customRankInfoDtoList);

        assertThat(group.sortedScoreAndSize(100).getCustomRankInfoDtoList())
            .isSortedAccordingTo((o1, o2) -> o1.getScore() < o2.getScore() ? 1 : 0);
    }

    @ParameterizedTest
    @AutoSource
    @DisplayName("커스텀 랭킹 리스트는 매개변수로 들어오는 size 만큼의 리스트를 반환")
    void rankInfoGroupBySize(List<CustomRankInfoDto> customRankInfoDtoList) {

        CustomRankInfoGroup group = new CustomRankInfoGroup(customRankInfoDtoList);

        int paramSize = 2;

        assertThat(group.sortedScoreAndSize(paramSize).getCustomRankInfoDtoList()).hasSize(paramSize);
    }

    @Test
    @DisplayName("커스텀 랭킹 그룹, idxList 에 맞는 리스트 탐색")
    void findContainObjectByIdxList() {
        CustomRankInfoGroup group = new CustomRankInfoGroup(
            List.of(
                new CustomRankInfoDto(1,1L,1L,1L,1L,1.0),
                new CustomRankInfoDto(2,1L,1L,1L,1L,1.0),
                new CustomRankInfoDto(3,1L,1L,1L,1L,1.0),
                new CustomRankInfoDto(4,1L,1L,1L,1L,1.0),
                new CustomRankInfoDto(5,1L,1L,1L,1L,1.0),
                new CustomRankInfoDto(6,1L,1L,1L,1L,1.0),
                new CustomRankInfoDto(7,1L,1L,1L,1L,1.0),
                new CustomRankInfoDto(8,1L,1L,1L,1L,1.0),
                new CustomRankInfoDto(9,1L,1L,1L,1L,1.0),
                new CustomRankInfoDto(10,1L,1L,1L,1L,1.0),
                new CustomRankInfoDto(11,1L,1L,1L,1L,1.0),
                new CustomRankInfoDto(12,1L,1L,1L,1L,1.0),
                new CustomRankInfoDto(13,1L,1L,1L,1L,1.0)
            )
        );

        List<Integer> idxList = List.of(10, 11, 12);

        assertThat(group.findContainIdxList(idxList))
            .extracting("goodsIdx")
            .contains(10, 11, 12);
    }

}