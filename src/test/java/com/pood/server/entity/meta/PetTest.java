package com.pood.server.entity.meta;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class PetTest {

    Pet small;
    Pet medium;
    Pet big;

    @BeforeEach
    void setUp() {
        small = new Pet(1, "pcKind", "pcKindEn", "pcTag", "소형", 1, "pcHeightMale",
            "pcHeightFemale", "pcWeightMale", "pcWeightFemale", "pcCountry", "pcLifeExpectancy",
            "pcGenericWeak", LocalDateTime.now(), LocalDateTime.now());
        medium = new Pet(1, "pcKind", "pcKindEn", "pcTag", "중형", 1, "pcHeightMale",
            "pcHeightFemale", "pcWeightMale", "pcWeightFemale", "pcCountry", "pcLifeExpectancy",
            "pcGenericWeak", LocalDateTime.now(), LocalDateTime.now());
        big = new Pet(1, "pcKind", "pcKindEn", "pcTag", "대형", 1, "pcHeightMale",
            "pcHeightFemale", "pcWeightMale", "pcWeightFemale", "pcCountry", "pcLifeExpectancy",
            "pcGenericWeak", LocalDateTime.now(), LocalDateTime.now());
    }

    @Test
    @DisplayName("펫이 소형인 경우 1을 반환")
    void smallSizeThenReturn1() {
        assertThat(small.getPcSizeCode()).isEqualTo(1);
    }

    @Test
    @DisplayName("펫이 중형인 경우 2을 반환")
    void middleSizeThenReturn2() {
        assertThat(medium.getPcSizeCode()).isEqualTo(2);
    }

    @Test
    @DisplayName("펫이 대형인 경우 3을 반환")
    void bigSizeThenReturn1() {
        assertThat(big.getPcSizeCode()).isEqualTo(3);
    }

    @Test
    @DisplayName("사이즈가 소형인지 체크")
    void pcSizeSmallTest() {
        assertThat(small.isPcSmallSize()).isTrue();
    }

    @Test
    @DisplayName("사이즈가 소형인지 체크")
    void pcSizeMediumTest() {
        assertThat(medium.isMediumPcSize()).isTrue();
    }

    @Test
    @DisplayName("사이즈가 소형인지 체크")
    void pcSizeBigTest() {
        assertThat(big.isPcBigSize()).isTrue();
    }
}