package com.pood.server.entity.meta;

import java.time.LocalDateTime;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class PromotionGroupTest {


    @Test
    @DisplayName("객체 생성 테스트")
    void create_test() {

        //given when
        PromotionGroup promotionGroup = new PromotionGroup(1, 1, "침대", 5, LocalDateTime.of(2022,1,1,10,10),
            LocalDateTime.of(2022,1,1,10,10));
        PromotionGroup promotionGroup2 = new PromotionGroup(1, 1, "침대", 5, LocalDateTime.of(2022,1,1,10,10),
            LocalDateTime.of(2022,1,1,10,10));

        //then
        Assertions.assertThat(promotionGroup).isEqualToComparingFieldByField(promotionGroup2);

    }

}