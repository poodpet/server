package com.pood.server.entity.meta;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.pood.server.exception.UsageTimeExpiredException;
import java.time.LocalDateTime;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class CouponTest {

    @Test
    @DisplayName("현재 시간보다 이전인 경우")
    void isBeforeCurrentTimeTest() {
        Coupon coupon = new Coupon(1, "description", "name", 0, 1, 1, 1, 1, 1, 1, 1, 10000, 20, 1,
            "tag",
            1, 1, 5, LocalDateTime.now().minusDays(1L), LocalDateTime.now(), LocalDateTime.now());

        assertThatThrownBy(coupon::isBeforeCurrentTime)
            .isInstanceOf(UsageTimeExpiredException.class)
            .hasMessage("사용기간이 지난 쿠폰입니다.");
    }

}