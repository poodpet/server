package com.pood.server.entity.meta.mapper.event;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class EventDonationMapperTest {

    @Test
    @DisplayName("기부이벤트 mapper 생성자 생성 시 이벤트가 기간이 종료되었으면 3을 반환")
    void createEndEventMapper() {

        EventDonationMapper mapper = new EventDonationMapper(
            1, "", "", "2023-01-01 00:00:00", "2023-01-02 00:00:00", 1, 1
        );

        assertThat(mapper.getStatus()).isEqualTo(2);
    }

    @Test
    @DisplayName("기부이벤트 mapper 생성자 생성 시 이벤트가 기간이 종료되지않았으면 status 그대로 반환")
    void createINGEventMapper() {

        LocalDateTime nowPlusOneDay = LocalDateTime.now().plusDays(1);

        EventDonationMapper mapper = new EventDonationMapper(
            1, "", "", "2023-01-01 00:00:00",
            nowPlusOneDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")), 3, 1
        );

        assertThat(mapper.getStatus()).isEqualTo(3);
    }
}