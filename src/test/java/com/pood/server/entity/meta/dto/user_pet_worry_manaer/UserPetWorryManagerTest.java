package com.pood.server.entity.meta.dto.user_pet_worry_manaer;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserPetWorryManagerTest {
    public UserPetWorryManager userPetWorryManager;

    public UserPetWorryManagerTest() {
        this.userPetWorryManager = new UnderWeightWorryCat();
    }

    @Test
    @DisplayName("일상 관리를 정상적으로 불러오는지 확인")
    void getDailyLife() {
        assertThat(userPetWorryManager.getDailyLife()).isEqualTo("잘 먹어도 살이 찌지 않는다면, 호르몬성 질환, 감염과 같은 전신질환이 있을 가능성이 있어요. 병원에서 정밀 검진을 받아보는 것을 받아보세요.");
    }

    @Test
    @DisplayName("식단 정보를 정상적으로 불러오는지 확인")
    void getDiet() {
        assertThat(userPetWorryManager.getDiet("이름")).isEqualTo("사료는 칼로리가 높은 사료를 추천해요.");
    }

    @Test
    @DisplayName("솔루션 정보를 정상적으로 불러오는지 확인")
    void getSolution() {
        assertThat(userPetWorryManager.getSolution().get(0)).isEqualTo("열량이 높은 식단 챙기기");
    }


}