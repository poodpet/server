package com.pood.server.entity.meta.dto.home;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.entity.meta.mapper.home.CustomRankInfoMapper;
import java.util.stream.Stream;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class CustomRankInfoDtoTest {

    @Test
    @DisplayName("of 메소드 - mapper의 요소에 null이 들어올 땐 0 으로 치환")
    void convertNulltoZero() {

        CustomRankInfoMapper mapper = new CustomRankInfoMapper(1, null, null, null, null);

        CustomRankInfoDto customRankInfoDto = CustomRankInfoDto.of(mapper);

        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(customRankInfoDto.getReviewCnt()).isEqualTo(0);
        softAssertions.assertThat(customRankInfoDto.getWishCnt()).isEqualTo(0);
        softAssertions.assertThat(customRankInfoDto.getTotalPurchaseUserCnt()).isEqualTo(0);
        softAssertions.assertThat(customRankInfoDto.getRePurchaseUserCnt()).isEqualTo(0);

        softAssertions.assertAll();
    }

    @ParameterizedTest
    @MethodSource
    @DisplayName("custom rank socre 점수 계산 = (리뷰수 + 찜하기) * (1 + 재구매율[재구매 유저 수 / 총 구매 유저 수])")
    void calculateCustomRankScore(CustomRankInfoMapper mapper) {
        assertThat(CustomRankInfoDto.of(mapper).getScore()).isEqualTo(equationRankScore(mapper));
    }

    private static Stream<Arguments> calculateCustomRankScore() {
        return Stream.of(
            Arguments.of(new CustomRankInfoMapper(1, 0L, 0L, 0L, 0L)),
            Arguments.of(new CustomRankInfoMapper(1, 0L, 1L, 0L, 0L)),
            Arguments.of(new CustomRankInfoMapper(1, 0L, 0L, 0L, 1L)),
            Arguments.of(new CustomRankInfoMapper(1, 0L, 0L, 1L, 0L)),
            Arguments.of(new CustomRankInfoMapper(1, 1L, 0L, 0L, 0L)),
            Arguments.of(new CustomRankInfoMapper(1, 411L, 23L, 12231L, 123L)),
            Arguments.of(new CustomRankInfoMapper(1, 1000000L, 100000L, 100000000L, 1000000L))
        );
    }


    private double equationRankScore(CustomRankInfoMapper mapper) {
        if (mapper.getTotalPurchaseUserCnt() == 0) {
            return mapper.getReviewCnt() + mapper.getWishCnt();
        }

        return (mapper.getReviewCnt() + mapper.getWishCnt())
            * (1f + mapper.getRePurchaseUserCnt() / (double) mapper.getTotalPurchaseUserCnt());
    }

    @Test
    @DisplayName("custom rank score 수식 검증")
    void formulaVerification() {
        CustomRankInfoMapper mapper = new CustomRankInfoMapper(1, 4L, 2L, 2L, 1L);
        assertThat(CustomRankInfoDto.of(mapper).getScore()).isEqualTo(9.0);
    }
}