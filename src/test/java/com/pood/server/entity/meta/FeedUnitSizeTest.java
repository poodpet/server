package com.pood.server.entity.meta;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class FeedUnitSizeTest {

    @ParameterizedTest
    @ValueSource(doubles = {0.9f, 0.89f, 0.1f})
    @DisplayName("알갱이 사이즈 체크 - small")
    void smallSizeTest(double unitSize) {
        String unitSizeName = FeedUnitSize.getUnitSizeName(unitSize);
        Assertions.assertThat(unitSizeName).isEqualTo(FeedUnitSize.SMALL.getName());
    }

    @ParameterizedTest
    @ValueSource(doubles = {1f, 1.1f, 1.5f, 1.49f, 1.5f})
    @DisplayName("알갱이 사이즈 체크 - medium")
    void mediumSizeTest(double unitSize) {
        String unitSizeName = FeedUnitSize.getUnitSizeName(unitSize);
        Assertions.assertThat(unitSizeName).isEqualTo(FeedUnitSize.MEDIUM.getName());
    }

    @ParameterizedTest
    @ValueSource(doubles = {1.6f, 1.61f, 5.0f})
    @DisplayName("알갱이 사이즈 체크 - large")
    void largeSizeTest(double unitSize) {
        String unitSizeName = FeedUnitSize.getUnitSizeName(unitSize);
        Assertions.assertThat(unitSizeName).isEqualTo(FeedUnitSize.LARGE.getName());
    }
}