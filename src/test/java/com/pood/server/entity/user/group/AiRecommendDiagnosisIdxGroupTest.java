package com.pood.server.entity.user.group;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.entity.user.BodyShape;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;

class AiRecommendDiagnosisIdxGroupTest {

    private static final Integer DIET_DIAGNOSIS_ARD_GROUP_CODE = 941;

    private static final Integer WEIGHT_GAIN_DIAGNOSIS_ARD_GROUP_CODE = 942;

    @Test
    @DisplayName("체형선택-고민 충돌 테스트 → 마름체형 선택 시, 내가 선택한 고민이 살을 빼야해요일 때 (true)")
    void isSkinnyTypeCheckMyWorryConflict_true() {

        BodyShape myChoiceBodyShape = BodyShape.SKINNY;

        AiRecommendDiagnosisArdCodeGroup group = new AiRecommendDiagnosisArdCodeGroup(
            List.of(DIET_DIAGNOSIS_ARD_GROUP_CODE, 1, 2, 3));

        //when
        boolean result = group.isConflictDiagnosisByBodyShape(myChoiceBodyShape);

        //then
        assertThat(result).isTrue();
    }

    @Test
    @DisplayName("체형선택-고민 충돌 테스트 → 마름체형 선택 시, 내가 선택한 고민이 살을 빼야해요가 아닐 떄 (false)")
    void isSkinnyTypeCheckMyWorryConflict_false() {

        BodyShape myChoiceBodyShape = BodyShape.SKINNY;

        AiRecommendDiagnosisArdCodeGroup group = new AiRecommendDiagnosisArdCodeGroup(
            List.of(WEIGHT_GAIN_DIAGNOSIS_ARD_GROUP_CODE, 1, 2, 3));

        //when
        boolean result = group.isConflictDiagnosisByBodyShape(myChoiceBodyShape);

        //then
        assertThat(result).isFalse();
    }

    @Test
    @DisplayName("체형선택-고민 충돌 테스트 → 적정체형 선택 시, 항상 (false)")
    void isNormalTypeCheckMyWorryConflict_false() {

        BodyShape myChoiceBodyShape = BodyShape.NORMAL;

        AiRecommendDiagnosisArdCodeGroup group = new AiRecommendDiagnosisArdCodeGroup(
            List.of(WEIGHT_GAIN_DIAGNOSIS_ARD_GROUP_CODE, DIET_DIAGNOSIS_ARD_GROUP_CODE, 1, 2, 3, 4, 5));

        //when
        boolean result = group.isConflictDiagnosisByBodyShape(myChoiceBodyShape);

        //then
        assertThat(result).isFalse();
    }

    @ParameterizedTest
    @EnumSource
    @DisplayName("체형선택-고민 충돌 테스트 → 내가 선택한 고민이 없을 떄, 항상 (false)")
    void isNormalTypeCheckMyWorryEmpty(BodyShape myChoiceBodyShape) {

        AiRecommendDiagnosisArdCodeGroup group = new AiRecommendDiagnosisArdCodeGroup(List.of());

        //when
        boolean result = group.isConflictDiagnosisByBodyShape(myChoiceBodyShape);

        //then
        assertThat(result).isFalse();
    }

    @ParameterizedTest
    @EnumSource(value = BodyShape.class, names = {"OBESITY", "HIGHLY_OBESE"})
    @DisplayName("체형선택-고민 충돌 테스트 → 비만,고도미만 일 떄, 내가 선택한 고민이 체증증량 (true)")
    void isObeseTypeCheckMyWorryConflict_true(BodyShape myChoiceBodyShape) {

        AiRecommendDiagnosisArdCodeGroup group = new AiRecommendDiagnosisArdCodeGroup(
            List.of(WEIGHT_GAIN_DIAGNOSIS_ARD_GROUP_CODE, 1, 3));

        //when
        boolean result = group.isConflictDiagnosisByBodyShape(myChoiceBodyShape);

        //then
        assertThat(result).isTrue();
    }

    @ParameterizedTest
    @EnumSource(value = BodyShape.class, names = {"OBESITY", "HIGHLY_OBESE"})
    @DisplayName("체형선택-고민 충돌 테스트 → 비만,고도미만 일 떄, 내가 선택한 고민이 체증증량이 아님 (false)")
    void isObeseTypeCheckMyWorryConflict_false(BodyShape myChoiceBodyShape) {

        AiRecommendDiagnosisArdCodeGroup group = new AiRecommendDiagnosisArdCodeGroup(
            List.of(DIET_DIAGNOSIS_ARD_GROUP_CODE, 1, 3));

        //when
        boolean result = group.isConflictDiagnosisByBodyShape(myChoiceBodyShape);

        //then
        assertThat(result).isFalse();
    }

    @Test
    @DisplayName("고민선택화면 표시 선택 : 적정체형 선택 → 체중고민이 없을 떄 (false)")
    void isVisibleWorryChoiceTap_whenNormalCase_x() {

        BodyShape myChoice = BodyShape.NORMAL;

        AiRecommendDiagnosisArdCodeGroup group = new AiRecommendDiagnosisArdCodeGroup(List.of(1,2,3));

        assertThat(group.isVisibleSelectPetWorry(myChoice)).isFalse();
    }

    @ParameterizedTest
    @ValueSource(ints = {942, 941})
    @DisplayName("고민선택화면 표시 선택 : 적정체형 선택 → 체중고민이 있을 때 (true)")
    void isVisibleWorryChoiceTap_whenNormalCase_o(Integer ardGroupCode) {

        BodyShape myChoice = BodyShape.NORMAL;

        AiRecommendDiagnosisArdCodeGroup group = new AiRecommendDiagnosisArdCodeGroup(
            List.of(1, ardGroupCode));

        assertThat(group.isVisibleSelectPetWorry(myChoice)).isTrue();

    }

    @ParameterizedTest
    @EnumSource(value = BodyShape.class, names = {"HIGHLY_OBESE", "OBESITY", "SKINNY"})
    @DisplayName("고민선택화면 표시 선택 : 적정체형 선택 X → 체중고민이 없을 떄 (true)")
    void isVisibleWorryChoiceTap_whenNotNormalCase_o(BodyShape myChoice) {

        AiRecommendDiagnosisArdCodeGroup group = new AiRecommendDiagnosisArdCodeGroup(List.of(1,2,3));

        assertThat(group.isVisibleSelectPetWorry(myChoice)).isTrue();
    }

    @ParameterizedTest
    @EnumSource(value = BodyShape.class, names = {"HIGHLY_OBESE", "OBESITY", "SKINNY"})
    @DisplayName("고민선택화면 표시 선택 : 적정체형 선택 X → 체중고민 있을 때 (false)")
    void isVisibleWorryChoiceTap_whenNotNormalCase_x(BodyShape myChoice) {

        AiRecommendDiagnosisArdCodeGroup group1 = new AiRecommendDiagnosisArdCodeGroup(List.of(
            DIET_DIAGNOSIS_ARD_GROUP_CODE, WEIGHT_GAIN_DIAGNOSIS_ARD_GROUP_CODE));
        AiRecommendDiagnosisArdCodeGroup group2 = new AiRecommendDiagnosisArdCodeGroup(List.of(
            DIET_DIAGNOSIS_ARD_GROUP_CODE));
        AiRecommendDiagnosisArdCodeGroup group3 = new AiRecommendDiagnosisArdCodeGroup(List.of(
            WEIGHT_GAIN_DIAGNOSIS_ARD_GROUP_CODE));

        assertThat(group1.isVisibleSelectPetWorry(myChoice)).isFalse();
        assertThat(group2.isVisibleSelectPetWorry(myChoice)).isFalse();
        assertThat(group3.isVisibleSelectPetWorry(myChoice)).isFalse();

    }
}