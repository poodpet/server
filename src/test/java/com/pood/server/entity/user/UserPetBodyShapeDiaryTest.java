package com.pood.server.entity.user;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserPetBodyShapeDiaryTest {

    @Test
    @DisplayName("체형 업데이트")
    void updateBodyShape() {
        BodyShape update = BodyShape.HIGHLY_OBESE;

        UserPetBodyShapeDiary entity = UserPetBodyShapeDiary.builder().build();
        entity.updateBodyShape(update);

        assertThat(entity.getBodyShape()).isEqualTo(update);
    }

    @Test
    @DisplayName("빈 엔티티 객체 생성")
    void emptyEntity() {
        UserPetBodyShapeDiary entity = UserPetBodyShapeDiary.empty();

        assertThat(entity.getIdx()).isNull();
        assertThat(entity.getUserPet()).isNull();
        assertThat(entity.getBodyShape()).isNull();
    }
}