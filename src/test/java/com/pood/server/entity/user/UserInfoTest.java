package com.pood.server.entity.user;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;


import com.pood.server.exception.PasswordNotMatchException;
import com.pood.server.util.UserStatus;
import java.time.LocalDateTime;
import org.assertj.core.api.AbstractThrowableAssert;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserInfoTest {

    @Test
    void softDeleteTest() {
        //given
        UserInfo userInfo = setUpMockUser();

        //when
        userInfo.softDelete();

        //then
        assertThat(userInfo.getDeviceKey()).isNull();
        assertThat(userInfo.getTokenArn()).isNull();
    }

    @Test
    @DisplayName("비밀번호 일치할 때")
    void isMatchedPasswordSuccess() {
        //given
        UserInfo userInfo = setUpMockUser();

        //when
        final UserInfo actual = userInfo.isMatchedPassword("password");

        //then
        assertThat(actual).isEqualTo(userInfo);
    }

    @Test
    @DisplayName("비밀번호 일치하지 않을 때 에러")
    void isMatchedPasswordException() {
        //given
        UserInfo userInfo = setUpMockUser();

        //when
        final AbstractThrowableAssert<?, ? extends Throwable> validation = Assertions.assertThatThrownBy(
            () -> userInfo.isMatchedPassword("nZ7mjHUT"));

        //then
        validation
            .isInstanceOf(PasswordNotMatchException.class)
            .hasMessage("비밀번호가 일치하지 않습니다.");
    }

    @Test
    @DisplayName("비밀번호 변경 테스트")
    void passwordChangeTest() {
        //given
        UserInfo userInfo = setUpMockUser();

        //when
        userInfo.changePassword("changePassword");

        //then
        assertThat(userInfo.getUserPassword()).isEqualTo("changePassword");
    }

    @Test
    @DisplayName("유저 회원 상태 비활성화")
    void test() {
        //given
        UserInfo userInfo = setUpMockUser();

        //when
        userInfo.toResignUser();

        //then
        assertThat(userInfo.getUserStatus()).isEqualTo(UserStatus.TO_RESIGN);
    }

    @Test
    @DisplayName("현재 비밀번호와 입력받은 비밀번호가 맞는지 검증")
    void passwordMatchTest() {
        //given
        UserInfo userInfo = setUpMockUser();

        //when
        final boolean actual = userInfo.currentPasswordValidate("password");

        //then
        assertThat(actual).isTrue();
    }

    @Test
    @DisplayName("현재 비밀번호와 입력받은 비밀번호가 맞지 않는 경우 예외")
    void passwordNotMatchTest() {
        //given
        UserInfo userInfo = setUpMockUser();

        //when, then
        assertThatThrownBy(() -> userInfo.currentPasswordValidate("notMatchPassword"))
            .isInstanceOf(PasswordNotMatchException.class)
            .hasMessage("현재 비밀번호가 서로 일치하지 않습니다.");
    }

    private UserInfo setUpMockUser() {
        return new UserInfo(
            1,
            "82f44cf0-0b9e-44e4-801a-64772e485c2d",
            "nickName",
            "email",
            "password",
            1,
            UserStatus.ACTIVE,
            "name",
            1000,
            "phone",
            "referralCode",
            true,
            LocalDateTime.now(),
            true, LocalDateTime.now(),
            true, LocalDateTime.now(),
            "key", 1, "tokenArn",
            "mlName", 0.0, 1,
            1, LocalDateTime.now(),
            LocalDateTime.now(), LocalDateTime.now(),
            null, true,
            null,true,
            null,true, null
        );
    }

}