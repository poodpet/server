package com.pood.server.entity.user;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Objects;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class BodyShapeTest {

    @ParameterizedTest
    @CsvSource({
        "A, 941",
        "C, 942",
        "D, 942",
    })
    @DisplayName("체형과 반대되는 진단 코드 반환 테스트")
    void checkBodyShapeReverseDiagnosis(String bodyType, Integer aiRecommendDiagnosisIdx) {
        assertThat(Objects.requireNonNull(BodyShape.findType(bodyType))
            .getReverseArdGroupCode()).isEqualTo(aiRecommendDiagnosisIdx);
    }

    @ParameterizedTest
    @CsvSource({
        "A, 942",
        "C, 941",
        "D, 941",
    })
    @DisplayName("체형에 맞는 진단 코드 반환 테스트")
    void checkBodyShapeDiagnosis(String bodyType, Integer aiRecommendDiagnosisIdx) {
        assertThat(Objects.requireNonNull(BodyShape.findType(bodyType))
            .getArdGroupCode()).isEqualTo(aiRecommendDiagnosisIdx);
    }

    @Test
    @DisplayName("적정 체형은, 진단 idx 가 null 을 반환")
    void checkGetNormalBodyShapeReturnDiagnosis() {
        assertThat(BodyShape.NORMAL.getReverseArdGroupCode()).isNull();
        assertThat(BodyShape.NORMAL.getArdGroupCode()).isNull();
    }
}