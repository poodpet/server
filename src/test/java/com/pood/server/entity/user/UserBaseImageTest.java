package com.pood.server.entity.user;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserBaseImageTest {

    @Test
    @DisplayName("생성 테스트")
    void create(){
        UserBaseImage userBaseImage = UserBaseImage.create("url");
        Assertions.assertThat(userBaseImage.getUrl()).isEqualTo("url");
    }

    @Test
    @DisplayName("기본으로 설정되어 있는지 확인")
    void isBasic(){
        UserBaseImage userBaseImage = UserBaseImage.create("url");
        Assertions.assertThat(userBaseImage.isBasic()).isFalse();
    }
}