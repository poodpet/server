package com.pood.server.entity.user;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.mock;

import java.time.LocalDate;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class UserPetWeightDiaryTest {

    @Test
    @DisplayName("UserPetWeightDiary builder weight > 0 (성공)")
    void successCreateUserPetWeightDiary() {

        UserPet userPet = mock(UserPet.class);
        Float petWeight = 0.1f;
        LocalDate baseDate = LocalDate.now();

        UserPetWeightDiary userPetWeightDiary = UserPetWeightDiary.builder()
            .userPet(userPet)
            .weight(petWeight)
            .baseDate(baseDate)
            .build();

        assertThat(userPetWeightDiary.getUserPet()).isEqualTo(userPet);
        assertThat(userPetWeightDiary.getWeight()).isEqualTo(petWeight);
        assertThat(userPetWeightDiary.getBaseDate()).isEqualTo(baseDate);

    }

    @ParameterizedTest
    @ValueSource(floats = {0f, -0.1f})
    @DisplayName("UserPetWeightDiary builder weight > 0 (실패)")
    void failCreateUserPetWeightDiary(float petWeight) {

        UserPet userPet = mock(UserPet.class);
        LocalDate baseDate = LocalDate.now();

        assertThatThrownBy(() ->  UserPetWeightDiary.builder()
            .userPet(userPet)
            .weight(petWeight)
            .baseDate(baseDate)
            .build()
        ).isInstanceOf(IllegalArgumentException.class);
    }
}