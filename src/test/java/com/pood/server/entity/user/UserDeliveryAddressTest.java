package com.pood.server.entity.user;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserDeliveryAddressTest {

    @Test
    @DisplayName("기본 배송지 아닌거로 바꾸는 테스트")
    void updateNotDefaultType() {
        //given
        UserDeliveryAddress userDeliveryAddress = new UserDeliveryAddress();

        //when
        userDeliveryAddress.updateNotDefaultType();

        //then
        Assertions.assertThat(userDeliveryAddress.isDefaultType()).isFalse();
    }

    @Test
    void updateDefaultType() {
        //given
        UserDeliveryAddress userDeliveryAddress = new UserDeliveryAddress();

        //when
        userDeliveryAddress.updateDefaultType();

        //then
        Assertions.assertThat(userDeliveryAddress.isDefaultType()).isTrue();
    }
}