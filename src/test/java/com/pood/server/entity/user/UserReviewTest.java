package com.pood.server.entity.user;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserReviewTest {

    UserReview userReview;

    @BeforeEach
    void setUp() {
        userReview = new UserReview(1, 1, "orderNum", 1, 1, "test1");
    }

    @Test
    @DisplayName("리뷰의 글을 수정합니다.")
    void modifyReviewText() {
        userReview.modifyReviewText("test2");
        Assertions.assertThat(userReview.getReviewText()).isEqualTo("test2");
    }

    @Test
    @DisplayName("리뷰의 주문번호를 검증합니다.(성공)")
    void eqOrderNumber_is_true() {
        Assertions.assertThat(userReview.eqOrderNumber("orderNum")).isTrue();
    }

    @Test
    @DisplayName("리뷰의 주문번호를 검증합니다.(실패)")
    void eqOrderNumber_is_false() {
        Assertions.assertThat(userReview.eqOrderNumber("orderNum2")).isFalse();
    }

    @Test
    @DisplayName("리뷰의 상품 idx를 검증합니다.(성공)")
    void eqGoodsIdx_is_true() {
        Assertions.assertThat(userReview.eqGoodsIdx(1)).isTrue();
    }

    @Test
    @DisplayName("리뷰의 상품 idx를 검증합니다.(실패)")
    void eqGoodsIdx_is_false() {
        Assertions.assertThat(userReview.eqGoodsIdx(2)).isFalse();
    }
}