package com.pood.server.entity.user;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.exception.AlreadyChangedException;
import com.pood.server.exception.NumberNotMatchException;
import com.pood.server.exception.UsageTimeExpiredException;
import java.time.LocalDateTime;
import org.assertj.core.api.AbstractThrowableAssert;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserSmsAuthTest {

    @Test
    @DisplayName("인증 시간이 만료가 안됐는지 확인")
    void timeIsNotExpired() {
        //given
        final UserSmsAuth smsAuth = new UserSmsAuth(1, "01012345678", "3333", 0,
            LocalDateTime.now(), LocalDateTime.of(2022, 3, 24, 16, 28, 0));
        //when
        final AbstractThrowableAssert<?, ? extends Throwable> validate = Assertions.assertThatThrownBy(
            smsAuth::isNotExpired);

        //then
        validate.isInstanceOf(UsageTimeExpiredException.class)
            .hasMessage("인증 시간이 초과되었습니다.");
    }

    @Test
    @DisplayName("인증 상태 업데이트")
    void updateTest() {
        final UserSmsAuth smsAuth = new UserSmsAuth(1, "01012345678", "3333", 0,
            LocalDateTime.now(), LocalDateTime.now());

        smsAuth.updateAuthenticationStatus();

        assertThat(smsAuth.getSmsStatus()).isEqualTo(1);
    }

    @Test
    @DisplayName("입력한 인증번호가 가지고 있는 번호와 다를 때")
    void authenticationTest() {
        //given
        final UserSmsAuth smsAuth = new UserSmsAuth(1, "01012345678", "3333", 0,
            LocalDateTime.now(), LocalDateTime.now());
        //when

        final AbstractThrowableAssert<?, ? extends Throwable> expected = Assertions.assertThatThrownBy(
            () -> smsAuth.authenticationCheck("3131"));
        //then
        expected
            .isInstanceOf(NumberNotMatchException.class)
            .hasMessage("인증번호가 일치하지 않습니다.");
    }

    @Test
    @DisplayName("이미 성공상태일 경우")
    void alreadySuccess() {
        //given
        final UserSmsAuth smsAuth = new UserSmsAuth(1, "01012345678", "3333", 1,
            LocalDateTime.now(), LocalDateTime.now());
        //when

        final AbstractThrowableAssert<?, ? extends Throwable> expected = Assertions.assertThatThrownBy(
            () -> smsAuth.authenticationCheck("3333"));
        //then
        expected
            .isInstanceOf(AlreadyChangedException.class)
            .hasMessage("이미 인증이 성공한 상태입니다.");
    }

    @Test
    @DisplayName("인증번호가 일치하며 인증이 성공한 상태가 아닐 때 (인증완료)")
    void authenticationComplete() {
        //given
        final UserSmsAuth smsAuth = new UserSmsAuth(1, "01012345678", "3333", 0,
            LocalDateTime.now(), LocalDateTime.now());
        //when
        smsAuth.authenticationCheck("3333");
    }
}