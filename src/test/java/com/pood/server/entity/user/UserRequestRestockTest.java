package com.pood.server.entity.user;

import static org.assertj.core.api.Assertions.assertThat;

import autoparams.AutoSource;
import com.pood.server.util.RestockState;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;

class UserRequestRestockTest {

    @ParameterizedTest
    @AutoSource
    @DisplayName("신규 생성 테스트")
    void create(int goodsIdx, String userUuid) {

        UserRequestRestock entity = UserRequestRestock.create(goodsIdx, userUuid);
        assertThat(entity.getGoodsIdx()).isEqualTo(goodsIdx);
        assertThat(entity.getUserUuid()).isEqualTo(userUuid);
        assertThat(entity.getState()).isEqualTo(RestockState.WAIT);
    }
}