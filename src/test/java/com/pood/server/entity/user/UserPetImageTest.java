package com.pood.server.entity.user;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserPetImageTest {

    @Test
    @DisplayName("유저 펫 url 업데이트")
    void update() {
        //given
        UserPetImage userPetImage = UserPetImage.builder()
            .userPetIdx(null)
            .visible(1)
            .url("awsS3 Saved Url")
            .build();
        //when
        userPetImage.updateImageUrl("update Url");

        //then
        assertThat(userPetImage.getUrl()).isEqualTo("update Url");
    }
}