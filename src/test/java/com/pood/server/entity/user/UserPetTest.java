package com.pood.server.entity.user;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.pood.server.controller.request.userpet.PetWorryRequest;
import com.pood.server.controller.request.userpet.UserPetUpdateRequest;
import com.pood.server.exception.IndexNotMatchException;
import com.pood.server.exception.NumberCreationException;
import java.time.LocalDate;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class UserPetTest {

    UserPet userPet = UserPet.builder()
        .userIdx(1)
        .allergy("allergy")
        .grainSize(1)
        .petActivity(1)
        .pcId(1)
        .pscId(1)
        .petName("petName")
        .petBirth(LocalDate.now())
        .petGender(1)
        .petStatus(0)
        .build();

    @Test
    @DisplayName("펫 이름, 성별 변경")
    void updatePetName() {
        //given
        final String updateName = "변환된이름";
        final UserPetUpdateRequest userPetUpdateRequest = new UserPetUpdateRequest(1, updateName, 3,
            List.of(new PetWorryRequest(1L, 1),
                new PetWorryRequest(2L, 2),
                new PetWorryRequest(3L, 3)),
            null, null);
        final UserPet userPet = new UserPet(1, "allergy", 1, 1, 1, 1, "펫이름", LocalDate.now(), 1, 0);

        //when
        userPet.updatePetNameByDto(userPetUpdateRequest);

        //then
        assertThat(userPet.getPetName()).isEqualTo(updateName);
        assertThat(userPet.getPetGender()).isEqualTo(3);
    }

    @Test
    @DisplayName("정상 일련번호 생성")
    void serialNumberTest() {
        final String actual = userPet.serialNumber(() -> "22", () -> 123456);
        assertThat(actual).isEqualTo("22-123456");
    }

    @ParameterizedTest
    @ValueSource(strings = {"0", "100"})
    @DisplayName("두자리 년도가 아닌경우 예외")
    void yearLimitTest(final String year) {
        assertThatThrownBy(() -> userPet.serialNumber(() -> year, () -> 123456))
            .isInstanceOf(NumberCreationException.class)
            .hasMessage("년도는 2자리여야만 합니다.");
    }

    @ParameterizedTest
    @ValueSource(ints = {99_999, 1_000_000})
    @DisplayName("일련번호 6자리가 아닌경우 예외")
    void notSixDigitsExceptionTest(final int randomNumber) {
        assertThatThrownBy(() -> userPet.serialNumber(() -> "22", () -> randomNumber))
            .isInstanceOf(NumberCreationException.class)
            .hasMessage("랜덤숫자는 6자리만 가능합니다.");
    }

    @Test
    @DisplayName("펫 소유자가 아닌경우 예외")
    void isNotPetOwner() {
        assertThatThrownBy(() -> userPet.petOwnerIsCorrectValid(2))
            .isInstanceOf(IndexNotMatchException.class)
            .hasMessage("해당하는 유저의 펫이 아닙니다.");
    }

    @Test
    @DisplayName("펫 소유자인 경우 정상처리")
    void isPetOwner() {
        assertThatCode(() -> userPet.petOwnerIsCorrectValid(1))
            .doesNotThrowAnyException();
    }

    @Test
    @DisplayName("펫의 개월수 구하기")
    void getAgeMonth() {
        final UserPet userPet = new UserPet(1, "allergy", 1, 1, 1, 1, "펫이름",
            LocalDate.now().minusMonths(11), 1, 0);
        assertThat(userPet.getAgeMonth()).isEqualTo(11);
    }

    @Test
    @DisplayName("펫의 타입이 고양이인지 확인")
    void isPetTypeCat() {
        assertThat(userPet.isDog()).isTrue();
    }

    @Test
    @DisplayName("펫의 타입이 강아지인지 확인")
    void isPetTypeDog() {
        assertThat(userPet.isCat()).isFalse();
    }

    @Test
    @DisplayName("펫의 중성화 여부")
    void isNeutering() {
        assertThat(userPet.isNeutering()).isFalse();
    }

    @Test
    @DisplayName("펫의 성장 단계를 표시")
    void getPetGrowthState() {
        final UserPet userPet = new UserPet(1, "allergy", 1, 1, 1, 1, "펫이름",
            LocalDate.now().minusMonths(11), 1, 0);
        assertThat(userPet.getPetGrowthState().getName()).isEqualTo("어린 강아지");
    }

}