package com.pood.server.entity.user;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.entity.meta.AiRecommendDiagnosis;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserPetAiDiagnosisTest {

    @Test
    @DisplayName("list의 사이즈 만큼 diagnosis 생성")
    void test() {
        //given
        List<AiRecommendDiagnosis> aiRecommendDiagnosisList = List.of(
            new AiRecommendDiagnosis(0, "알러지/아토피", "알레르기", 501,
                "2021-01-18 00:00:00", "2021-01-18 00:00:00"),
            new AiRecommendDiagnosis(1, "눈", "안과질환1", 201,
                "2021-01-18 00:00:00", "2021-01-18 00:00:00"),
            new AiRecommendDiagnosis(2, "뼈/관절/근육", "정형", 301,
                "2021-01-18 00:00:00", "2021-01-18 00:00:00")
        );
        List<UserPetAiDiagnosis> userPetAiDiagnosisList = new ArrayList<>();
        final int userPetIdx = 342;
        //when
        for (AiRecommendDiagnosis aiRecommendDiagnosis : aiRecommendDiagnosisList) {
            userPetAiDiagnosisList.add(
                UserPetAiDiagnosis.builder()
                    .userPetIdx(userPetIdx)
                    .ardGroup(aiRecommendDiagnosis.getArdGroup())
                    .ardGroupCode(aiRecommendDiagnosis.getArdGroupCode())
                    .aiDiagnosisIdx(aiRecommendDiagnosis.getIdx())
                    .build()
            );
        }
        //then
        assertThat(userPetAiDiagnosisList.size()).isEqualTo(3);
    }
}