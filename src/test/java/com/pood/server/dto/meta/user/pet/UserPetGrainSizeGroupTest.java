package com.pood.server.dto.meta.user.pet;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.entity.meta.GrainSize;
import com.pood.server.entity.user.UserPetGrainSize;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserPetGrainSizeGroupTest {

    private final UserPetGrainSizeGroup userPetGrainSizeGroup = new UserPetGrainSizeGroup();

    @Test
    @DisplayName("알갱이 3개인 경우 파싱 테스트")
    void saveObjectGrainSize3() {
        //given
        List<GrainSize> inputEntityList = List.of(
            new GrainSize(1, "name", 1.0, 1.5, "title", "url", 1, LocalDateTime.now(), LocalDateTime.now()),
            new GrainSize(1, "name", 1.0, 1.5, "title", "url", 2, LocalDateTime.now(), LocalDateTime.now()),
            new GrainSize(1, "name", 1.0, 1.5, "title", "url", 3, LocalDateTime.now(), LocalDateTime.now())
        );
        //when
        final List<UserPetGrainSize> userPetAllergies = userPetGrainSizeGroup.saveAllObjects(
            inputEntityList, 1);

        //then
        assertThat(userPetAllergies.size()).isEqualTo(3);
        for (UserPetGrainSize userPetGrainSize : userPetAllergies) {
            assertThat(userPetGrainSize.getUserPetIdx()).isEqualTo(1);
        }
    }

    @Test
    @DisplayName("알갱이 2개인 경우 파싱 테스트")
    void saveObjectGrainSize2() {
        //given
        List<GrainSize> inputEntityList = List.of(
            new GrainSize(1, "name", 1.0, 1.5, "title", "url", 1, LocalDateTime.now(), LocalDateTime.now()),
            new GrainSize(1, "name", 1.0, 1.5, "title", "url", 2, LocalDateTime.now(), LocalDateTime.now())
        );
        //when
        final List<UserPetGrainSize> userPetAllergies = userPetGrainSizeGroup.saveAllObjects(
            inputEntityList, 1);

        //then
        assertThat(userPetAllergies.size()).isEqualTo(2);
        for (UserPetGrainSize userPetGrainSize : userPetAllergies) {
            assertThat(userPetGrainSize.getUserPetIdx()).isEqualTo(1);
        }
    }

    @Test
    @DisplayName("알갱이 1개인 경우 파싱 테스트")
    void saveObjectGrainSize1() {
        //given
        List<GrainSize> inputEntityList = List.of(
            new GrainSize(1, "name", 1.0, 1.5, "title", "url", 1, LocalDateTime.now(), LocalDateTime.now())
        );
        //when
        final List<UserPetGrainSize> userPetAllergies = userPetGrainSizeGroup.saveAllObjects(
            inputEntityList, 1);

        //then
        assertThat(userPetAllergies.size()).isEqualTo(1);
        for (UserPetGrainSize userPetGrainSize : userPetAllergies) {
            assertThat(userPetGrainSize.getUserPetIdx()).isEqualTo(1);
        }
    }
}