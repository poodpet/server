package com.pood.server.dto.meta.user.pet;

import static org.assertj.core.api.Assertions.assertThat;

import autoparams.AutoSource;
import com.pood.server.controller.request.userpet.PetWorryRequest;
import com.pood.server.entity.meta.PetWorry;
import com.pood.server.entity.user.UserPetAiDiagnosis;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;

class UserPetAiDiagnosisGroupTest {

    private final UserPetAiDiagnosisGroup userPetAiDiagnosisGroup = new UserPetAiDiagnosisGroup();

    @Test
    @DisplayName("저장객체 알러지 3개인 경우 파싱 테스트")
    void saveObjectAllergySize3() {
        //given
        List<PetWorry> inputEntityList = List.of(
            new PetWorry(1L, null, null, "name", "url", "catUrl", 1, LocalDateTime.now()),
            new PetWorry(2L, null, null, "name", "dogUrl", "catUrl", 1, LocalDateTime.now()),
            new PetWorry(3L, null, null, "name", "dogUrl", "catUrl", 1, LocalDateTime.now())
        );
        //when
        final List<UserPetAiDiagnosis> userPetAllergies = userPetAiDiagnosisGroup.saveAllObjects(
            inputEntityList, List.of(new PetWorryRequest(1L, 1),
                new PetWorryRequest(2L, 2),
                new PetWorryRequest(3L, 3)
            ), 1);

        //then
        assertThat(userPetAllergies).hasSize(3);
        for (UserPetAiDiagnosis userPetAiDiagnosis : userPetAllergies) {
            assertThat(userPetAiDiagnosis.getUserPetIdx()).isEqualTo(1);
        }

    }

    @Test
    @DisplayName("저장객체 알러지 2개인 경우 파싱 테스트")
    void saveObjectAllergySize2() {
        //given
        List<PetWorry> inputEntityList = List.of(
            new PetWorry(1L, null, null, "name", "dogUrl", "catUrl", 1, LocalDateTime.now()),
            new PetWorry(2L, null, null, "name", "dogUrl", "catUrl", 1, LocalDateTime.now())
        );
        //when
        final List<UserPetAiDiagnosis> userPetAllergies = userPetAiDiagnosisGroup.saveAllObjects(
            inputEntityList,
            List.of(new PetWorryRequest(1L, 1),
                new PetWorryRequest(2L, 2),
                new PetWorryRequest(3L, 3)
            ), 1);

        //then
        assertThat(userPetAllergies).hasSize(2);
        for (UserPetAiDiagnosis userPetAiDiagnosis : userPetAllergies) {
            assertThat(userPetAiDiagnosis.getUserPetIdx()).isEqualTo(1);
        }
    }

    @Test
    @DisplayName("저장객체 알러지 1개인 경우 파싱 테스트")
    void saveObjectAllergySize1() {
        //given
        List<PetWorry> inputEntityList = List.of(
            new PetWorry(1L, null, null, "name", "dogUrl", "catUrl", 1, LocalDateTime.now())
        );
        //when
        final List<UserPetAiDiagnosis> userPetAllergies = userPetAiDiagnosisGroup.saveAllObjects(
            inputEntityList, List.of(new PetWorryRequest(1L, 1)), 1);

        //then
        for (UserPetAiDiagnosis userPetAiDiagnosis : userPetAllergies) {
            assertThat(userPetAiDiagnosis.getUserPetIdx()).isEqualTo(1);
        }
    }

    @ParameterizedTest
    @AutoSource
    @DisplayName("유저가 선택한 펫 고민 idx List 반환")
    void getPetWorryIdsList(List<UserPetAiDiagnosis> list) {
        UserPetAiDiagnosisGroup group = UserPetAiDiagnosisGroup.of(list);

        int i = 0;
        for (Long idx : group.getWorryIdxList()) {
            assertThat(idx).isEqualTo(list.get(i++).getPetWorryIdx());
        }
    }
}