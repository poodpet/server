package com.pood.server.dto.meta.pet;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.entity.meta.Pet;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.Test;

class PetInfoDtoGroupTest {

    @Test
    void 펫정보_간추린_데이터_추출() {
        Pet pet = new Pet(1, "품종", "품종영어", "pcTag", "pcSize", 1, null,
            null, null, null, null, null, null
            , LocalDateTime.now(), LocalDateTime.now());

        PetInfoDtoGroup petInfoDtoGroup = new PetInfoDtoGroup();
        final List<PetInfoDto> petInfoDtos = petInfoDtoGroup.addPetInfo(List.of(pet));

        assertThat(petInfoDtos.get(0).getIdx()).isEqualTo(1);
        assertThat(petInfoDtos.get(0).getPcId()).isEqualTo(1);
        assertThat(petInfoDtos.get(0).getPcKind()).isEqualTo("품종");
    }

}