package com.pood.server.dto.meta.goods;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class PromotionImageDtoTest {

    @Test
    @DisplayName("PromotionImage 객체 생성 테스트")
    void create() {

        //given when
        PromotionDto.PromotionImage promotionDto1 = new PromotionDto.PromotionImage(1, "http:..",
            0);
        PromotionDto.PromotionImage promotionDto2 = new PromotionDto.PromotionImage(1, "http:..",
            0);

        //then
        Assertions.assertThat(promotionDto1).isEqualToComparingFieldByField(promotionDto2);

    }

}