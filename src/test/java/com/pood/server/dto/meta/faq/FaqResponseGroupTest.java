package com.pood.server.dto.meta.faq;

import com.pood.server.entity.meta.Faq;
import com.pood.server.entity.meta.FaqCategory;
import java.time.LocalDateTime;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class FaqResponseGroupTest {

    @Test
    @DisplayName("faqResponseGroup 변환 테스트")
    void faqResponseGroupConvert() {

        FaqCategory faqCategory = new FaqCategory(1l, "test1", 2, LocalDateTime.now(),
            LocalDateTime.now());

        FaqResponseGroup faqResponseGroup = new FaqResponseGroup(
            List.of(new Faq(1l, faqCategory, "test1", "test1", 2, LocalDateTime.now(),
                    LocalDateTime.now()),
                new Faq(2l, faqCategory, "test2", "test2", 1, LocalDateTime.now(),
                    LocalDateTime.now())
            ));

        Assertions.assertThat(
                faqResponseGroup.getFaqResponseList().get(1).getTitle())
            .isEqualTo("test2");
    }
}

