package com.pood.server.dto.meta.goods;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.dto.meta.goods.PromotionDto.PromotionGoodsProductInfo;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class GoodsDtoTest {

    @Test
    @DisplayName("SortedGoodsList 기획전 있을 시 가격 변경 테스트")
    void goodsListTest() {
        //given
        GoodsDto.SortedGoodsList sortedGoodsList = new SortedGoodsList();
        sortedGoodsList.setPromotionInfo(new PromotionGoodsProductInfo());
        sortedGoodsList.setDiscountPrice(1);
        sortedGoodsList.setDiscountRate(1);
        sortedGoodsList.setGoodsPrice(10000);

        final PromotionGoodsProductInfo promotionGoodsProductInfo = new PromotionGoodsProductInfo(1,
            "name", 15000, 1000, 10, "startPeriod", "endPeriod", 1);
        //when
        sortedGoodsList.promotionIfExistUpdatePrice(promotionGoodsProductInfo);

        //then
        assertThat(sortedGoodsList.getPromotionInfo()).isEqualTo(promotionGoodsProductInfo);
        assertThat(sortedGoodsList.getDiscountPrice()).isEqualTo(
            promotionGoodsProductInfo.getPrDiscountPrice());
        assertThat(sortedGoodsList.getDiscountRate()).isEqualTo(
            promotionGoodsProductInfo.getPrDiscountRate());
        assertThat(sortedGoodsList.getGoodsPrice()).isEqualTo(
            promotionGoodsProductInfo.getPrPrice());
    }
}