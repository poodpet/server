package com.pood.server.dto.meta.coupon;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.entity.meta.Coupon;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class PetRegisterCouponsTest {

    private static final Coupon coupon1000 = new Coupon(1, "description", "name", 0, 1,
        1, 1, 1, 1, 1, 1,
        10000, 20, 1, "tag",
        1, 1, 5, LocalDateTime.now().minusDays(1L),
        LocalDateTime.now(), LocalDateTime.now());

    private static final Coupon coupon3000 = new Coupon(2, "description", "name", 0,
        1, 1, 1, 1, 1, 1,
        1, 10000, 20, 1,
        "tag", 1, 1, 5, LocalDateTime.now().minusDays(1L),
        LocalDateTime.now(), LocalDateTime.now());

    private static final Coupon coupon5000 = new Coupon(3, "description", "name", 0, 1,
        1, 1, 1, 1, 1, 1,
        10000, 20, 1, "tag", 1, 1, 5,
        LocalDateTime.now().minusDays(1L), LocalDateTime.now(), LocalDateTime.now());

    private static final Coupon coupon7000 = new Coupon(4, "description", "name", 0, 1,
        1, 1, 1, 1, 1, 1,
        10000, 20, 1, "tag", 1, 1, 5,
        LocalDateTime.now().minusDays(1L), LocalDateTime.now(), LocalDateTime.now());

    private static final Coupon coupon10000 = new Coupon(5, "description", "name", 0, 1,
        1, 1, 1, 1, 1, 1,
        10000, 100000, 1, "tag", 1, 1, 5,
        LocalDateTime.now().minusDays(1L), LocalDateTime.now(), LocalDateTime.now());

    private static final List<Coupon> couponList = List.of(coupon1000, coupon3000, coupon5000, coupon7000, coupon10000);

    private static Stream<Arguments> findByDiscountPrice() {
        return Stream.of(
            Arguments.of(new PetRegisterCoupons(couponList, () -> 10), coupon1000),
            Arguments.of(new PetRegisterCoupons(couponList, () -> 11), coupon3000),
            Arguments.of(new PetRegisterCoupons(couponList, () -> 51), coupon5000),
            Arguments.of(new PetRegisterCoupons(couponList, () -> 81), coupon7000),
            Arguments.of(new PetRegisterCoupons(couponList, () -> 91), coupon10000)
        );
    }

    @ParameterizedTest
    @MethodSource
    @DisplayName("할인 금액에 맞는 쿠폰을 하나 뽑아옴")
    void findByDiscountPrice(final PetRegisterCoupons coupons, final Coupon coupon) {
        assertThat(coupons.findPercent()).isEqualTo(coupon);
    }

    @Test
    @DisplayName("쿠폰 idx 리스트 반환하기")
    void test() {
        PetRegisterCoupons coupons = new PetRegisterCoupons(couponList, () -> 10);
        Assertions.assertThat(coupons.findIdxList()).containsExactly(1, 2, 3, 4, 5);
    }

}