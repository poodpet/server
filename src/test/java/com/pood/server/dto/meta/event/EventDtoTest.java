package com.pood.server.dto.meta.event;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.dto.meta.event.EventDto.EventDetail;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class EventDtoTest {

    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Test
    @DisplayName("진행중인지 여부 확인")
    void eventIsRunning() {
        EventDto.EventDetail eventDetail = new EventDetail();
        eventDetail.setStartDate(dateTimeFormatter.format(LocalDateTime.MAX));
        assertThat(eventDetail.isWaiting()).isTrue();
    }

}