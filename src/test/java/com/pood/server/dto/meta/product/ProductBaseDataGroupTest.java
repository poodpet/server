package com.pood.server.dto.meta.product;

import com.pood.server.service.pet_solution.ProductBaseData;
import com.pood.server.service.pet_solution.ProductType;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ProductBaseDataGroupTest {

    ProductBaseDataGroup productBaseDataGroup;

    @BeforeEach
    void set() {
        productBaseDataGroup = new ProductBaseDataGroup(
            List.of(ProductBaseData.builder().idx(1).ctIdx(1).mainProperty("콩").animalProtein("닭고기")
                    .build(),
                ProductBaseData.builder().idx(2).ctIdx(1).mainProperty("연어/생선").animalProtein("소고기")
                    .build(),
                ProductBaseData.builder().idx(3).ctIdx(0).mainProperty("기타").animalProtein("돼지고기")
                    .build()));
    }

    @Test
    @DisplayName("product idx 리스트가 정상 반환되는지 확인")
    void getProductIdxList() {
        Assertions.assertThat(productBaseDataGroup.getProductIdxList()).hasSize(3);
    }

    @Test
    @DisplayName("리스트가 입력 변수에 따라 정상적으로 limit가 걸리는지 확인")
    void getLimitBySize() {
        Assertions.assertThat(productBaseDataGroup.getLimitBySize(1).getProductIdxList())
            .hasSize(1);
    }

    @Test
    @DisplayName("리스트가 ProductType에 따라 필터 되는지 확인")
    void getProductTypeFilter() {
        Assertions.assertThat(
                productBaseDataGroup.getProductTypeFilter(ProductType.FEED).getProductBaseDataList()
                    .get(0).getProductType())
            .isEqualTo(ProductType.FEED);
    }

    @Test
    @DisplayName("리스트가 알러지 명에 따라 필터 되는지 확인(매칭이 되는게 있을 경우)")
    void getAllergyFilter_Matching() {
        List<ProductBaseData> allergyFilter = productBaseDataGroup.getAllergyFilter(
            List.of("콩", "소고기"));
        Assertions.assertThat(allergyFilter).hasSize(1);
    }

    @Test
    @DisplayName("리스트가 알러지 명에 따라 필터 되는지 확인(매칭이 되는게 없을 경우)")
    void getAllergyFilter_NotMatching() {
        List<ProductBaseData> allergyFilter = productBaseDataGroup.getAllergyFilter(
            List.of("오리"));
        Assertions.assertThat(allergyFilter).hasSize(3);
    }


    @Test
    @DisplayName("리스트가 알러지 명과 상품 타입에 따라 필터 되는지 확인(매칭이 되는게 있을 경우)")
    void getProductTypeAndAllergyFilter_Matching() {
        List<ProductBaseData> allergyFilter = productBaseDataGroup.getProductTypeAndAllergyFilter(
            ProductType.FEED,
            List.of("콩"));
        Assertions.assertThat(allergyFilter).hasSize(1);
    }

    @Test
    @DisplayName("리스트가 알러지 명과 상품 타입에 따라 필터 되는지 확인(매칭이 되는게 없을 경우)")
    void getProductTypeAndAllergyFilter_NotMatching() {
        List<ProductBaseData> allergyFilter = productBaseDataGroup.getProductTypeAndAllergyFilter(
            ProductType.SUPPLIES,
            List.of("콩"));
        Assertions.assertThat(allergyFilter).isEmpty();
    }

}