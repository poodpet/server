package com.pood.server.dto.meta.goods;

import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.dto.meta.goods.PromotionDto.PromotionGoodsProductInfo;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class SortedGoodsGroupTest {

    SortedGoodsGroup sortedGoodsGroup;

    @BeforeEach
    void set() {
        sortedGoodsGroup = new SortedGoodsGroup(List.of(
            SortedGoodsList.builder().idx(2).goodsName("2").build(),
            SortedGoodsList.builder().idx(1).goodsName("1").build()
        ));
    }

    @Test
    @DisplayName("idx 리스트가 정상 반환되는지 확인")
    void getIdxList() {

        Assertions.assertThat(sortedGoodsGroup.getIdxList().size()).isEqualTo(2);
    }

    @Test
    @DisplayName("프로모션이 정상적으로 적용되는지 확인")
    void setPromotionData() {
        PromotionGoodsProductInfo pro = new PromotionGoodsProductInfo(1, "test", 1000, 100, 10,
            "2022", "2022", 1);
        sortedGoodsGroup.setPromotionData(List.of(pro));

        Assertions.assertThat(sortedGoodsGroup.getSortedGoodsList().get(1).getGoodsPrice())
            .isEqualTo(1000);
    }

    @Test
    @DisplayName("프로모션 할인율로 정렬이 되는지 확인")
    void getSortedGoodsListOrderByPrDiscountRateDesc() {
        PromotionGoodsProductInfo pro = new PromotionGoodsProductInfo(1, "test", 1000, 100, 10,
            "2022", "2022", 1);

        PromotionGoodsProductInfo pro2 = new PromotionGoodsProductInfo(2, "test", 1000, 100, 5,
            "2022", "2022", 2);
        sortedGoodsGroup.setPromotionData(List.of(pro, pro2));

        Assertions.assertThat(
                sortedGoodsGroup.getSortedGoodsListOrderByPrDiscountRateDesc().get(1).getGoodsPrice())
            .isEqualTo(1000);
    }

}