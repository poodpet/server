package com.pood.server.dto.meta.goods;

import com.pood.server.dto.meta.goods.PromotionDto.PromotionGoodsListInfo;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class PromotionGoodsListDtoTest {

    @Test
    @DisplayName("PromotionGoodsListInfo 객체 생성 테스트")
    void create() {

        //given when
        PromotionGoodsListInfo promotionGoodsListInfo1 = new PromotionGoodsListInfo();
        PromotionGoodsListInfo promotionGoodsListInfo2 = new PromotionGoodsListInfo();
        promotionGoodsListInfo1.setIdx(1);
        promotionGoodsListInfo1.setName("goods name");
        promotionGoodsListInfo1.setSubName("goods sub name");
        promotionGoodsListInfo1.setStartPeriod("2022-01-01 12:11:10");
        promotionGoodsListInfo1.setEndPeriod("2022-01-01 12:11:10");
        promotionGoodsListInfo1.setMainImages("http://...");
        promotionGoodsListInfo1.setPcIdx(1);
        promotionGoodsListInfo1.setType("E");

        promotionGoodsListInfo2.setIdx(1);
        promotionGoodsListInfo2.setName("goods name");
        promotionGoodsListInfo2.setSubName("goods sub name");
        promotionGoodsListInfo2.setStartPeriod("2022-01-01 12:11:10");
        promotionGoodsListInfo2.setEndPeriod("2022-01-01 12:11:10");
        promotionGoodsListInfo2.setMainImages("http://...");
        promotionGoodsListInfo2.setPcIdx(1);
        promotionGoodsListInfo2.setType("E");

        //then
        Assertions.assertThat(promotionGoodsListInfo1)
            .isEqualToComparingFieldByField(promotionGoodsListInfo2);
    }


}
