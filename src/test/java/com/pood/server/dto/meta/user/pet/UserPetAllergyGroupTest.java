package com.pood.server.dto.meta.user.pet;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.entity.meta.AllergyData;
import com.pood.server.entity.user.UserPetAllergy;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserPetAllergyGroupTest {

    private final UserPetAllergyGroup userPetAllergyGroup = new UserPetAllergyGroup();

    @Test
    @DisplayName("저장객체 알러지 3개인 경우 파싱 테스트")
    void saveObjectAllergySize3() {
        //given
        List<AllergyData> inputEntityList = List.of(
            new AllergyData(1, "name", 1, true, "url", 1, LocalDateTime.now(), LocalDateTime.now()),
            new AllergyData(2, "name2", 2, true, "url", 2, LocalDateTime.now(), LocalDateTime.now()),
            new AllergyData(3, "name3", 3, true, "url", 3, LocalDateTime.now(), LocalDateTime.now())
        );
        //when
        final List<UserPetAllergy> userPetAllergies = userPetAllergyGroup.saveAllObjects(
            inputEntityList, 1);

        //then
        assertThat(userPetAllergies.size()).isEqualTo(3);
        for (UserPetAllergy userPetAllergy : userPetAllergies) {
            assertThat(userPetAllergy.getUserPetIdx()).isEqualTo(1);
        }

    }

    @Test
    @DisplayName("저장객체 알러지 2개인 경우 파싱 테스트")
    void saveObjectAllergySize2() {
        //given
        List<AllergyData> inputEntityList = List.of(
            new AllergyData(1, "name", 1, true, "url", 1, LocalDateTime.now(), LocalDateTime.now()),
            new AllergyData(2, "name2", 2, true, "url", 2, LocalDateTime.now(), LocalDateTime.now())
        );
        //when
        final List<UserPetAllergy> userPetAllergies = userPetAllergyGroup.saveAllObjects(
            inputEntityList, 1);

        //then
        assertThat(userPetAllergies.size()).isEqualTo(2);
        for (UserPetAllergy userPetAllergy : userPetAllergies) {
            assertThat(userPetAllergy.getUserPetIdx()).isEqualTo(1);
        }
    }

    @Test
    @DisplayName("저장객체 알러지 1개인 경우 파싱 테스트")
    void saveObjectAllergySize1() {
        //given
        List<AllergyData> inputEntityList = List.of(
            new AllergyData(1, "name", 1, true, "url", 1, LocalDateTime.now(), LocalDateTime.now())
        );
        //when
        final List<UserPetAllergy> userPetAllergies = userPetAllergyGroup.saveAllObjects(
            inputEntityList, 1);

        //then
        for (UserPetAllergy userPetAllergy : userPetAllergies) {
            assertThat(userPetAllergy.getUserPetIdx()).isEqualTo(1);
        }
    }
}