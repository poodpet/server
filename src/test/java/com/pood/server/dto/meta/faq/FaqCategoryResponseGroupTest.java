package com.pood.server.dto.meta.faq;

import com.pood.server.entity.meta.FaqCategory;
import java.time.LocalDateTime;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class FaqCategoryResponseGroupTest {

    @Test
    @DisplayName("faqCategoryResponseGroup 변환 테스트")
    void faqCategoryResponseGroupConvert() {

        FaqCategoryResponseGroup faqCategoryResponseGroup = new FaqCategoryResponseGroup(
            List.of(new FaqCategory(1l, "test1", 2, LocalDateTime.now(), LocalDateTime.now()),
                new FaqCategory(2l, "test2", 1, LocalDateTime.now(), LocalDateTime.now())
            ));
        Assertions.assertThat(
                faqCategoryResponseGroup.getFaqCategoryResponseList().get(1).getName())
            .isEqualTo("test2");
    }
}