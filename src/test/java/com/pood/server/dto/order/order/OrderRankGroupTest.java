package com.pood.server.dto.order.order;

import static org.assertj.core.api.Assertions.*;

import com.pood.server.dto.meta.goods.GoodsDto.SortedGoodsList;
import com.pood.server.entity.meta.dto.home.OrderRankDto;
import com.pood.server.dto.order.OrderRankGroup;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

class OrderRankGroupTest {

    List<OrderRankDto> orderDtos;
    Page<OrderRankDto> OrderRankDtoPage;
    OrderRankGroup orderRankGroup;

    @BeforeEach
    void SET_UP() {
        orderDtos = List.of(new OrderRankDto(2, 1L), new OrderRankDto(1, 1L));
        OrderRankDtoPage = new PageImpl<>(orderDtos);
        orderRankGroup = new OrderRankGroup(OrderRankDtoPage);
    }

    @Test
    @DisplayName("goods idx List를 받아오는 테스트")
    void GET_GOODS_IDS_LIST() {

        List<Integer> goodsIdsList = orderRankGroup.getGoodsIdsList();
        assertThat(goodsIdsList).isEqualTo(List.of(2, 1));
    }


    @Test
    @DisplayName("Page<OrderRankDto> -> Page<SortedGoodsList> 변환 테스트(SORTED_GOODS)")
    void CONVERT_SORTED_GOODS() {

        List<SortedGoodsList> goodsInfoList = List.of(
            SortedGoodsList.builder().idx(2).goodsName("2").build(),
            SortedGoodsList.builder().idx(1).goodsName("1").build()
        );

        Page<SortedGoodsList> sortedGoodsLists = orderRankGroup.convertSortedGoods(goodsInfoList);

        String goodsName = sortedGoodsLists.get().findFirst().orElse(null)
            .getGoodsName();

        assertThat(goodsName).isEqualTo(goodsInfoList.get(0).getGoodsName());

    }

    @Test
    @DisplayName("Page<OrderRankDto> -> Page<SortedGoodsList> 변환 테스트(FREQUENTLY)")
    void CONVERT_FREQUENTLY_GOODS_() {

        List<SortedGoodsList> goodsInfoList = List.of(
            SortedGoodsList.builder().idx(2).goodsName("2").buyCount(10).score(10).build(),
            SortedGoodsList.builder().idx(1).goodsName("1").buyCount(15).score(20).build()
        );

        Page<SortedGoodsList> sortedGoodsLists = orderRankGroup.convertFrequentlyGoods(
            goodsInfoList);

        int score = (int) sortedGoodsLists.get().findFirst().orElse(null)
            .getScore();

        assertThat(score).isEqualTo(goodsInfoList.get(0).getScore());

    }

    @Test
    @DisplayName("rankGoods Page 의 새로운 Contents 추가시, 순서 유지")
    void contentConnection() {
        //given
        List<OrderRankDto> addList = List.of(
            new OrderRankDto(3, 1L),
            new OrderRankDto(4, 1L)
        );

        OrderRankGroup newGroup = orderRankGroup.addCustomRankContents(addList);

        int[] goodsIdxs = {2, 1, 3, 4,};
        int index = 0;

        for (int goodsIdx : newGroup.getGoodsIdsList()) {
            assertThat(goodsIdx).isEqualTo(goodsIdxs[index++]);
        }
    }
}