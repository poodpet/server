package com.pood.server.dto.user.basket;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.pood.server.exception.LimitQuantityException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class BuyLimitTest {

    @Test
    @DisplayName("수량 제한 없을 때 주문 수 무한대는 0으로 반환")
    void addAmount() {
        final int actual = BuyLimit.INFINITY.remainQuantity(10, -1, BasketQuantity.of(1));
        assertThat(actual).isZero();
    }

    @Test
    @DisplayName("오직 1개만 구입 가능할 때 장바구니에 1개가 존재하는 경우 예외")
    void onlyOneTest() {
        assertThrowBuyLimit(BuyLimit.ONLY_ONE, 0, 1, BasketQuantity.of(1), "이미 이전에 물품을 1개 구매한 이력이 있습니다.");
    }

    @Test
    @DisplayName("이전 주문에서 1개이상 주문이 진행된 경우")
    void onlyOneExceptionTest() {
        assertThrowBuyLimit(BuyLimit.ONLY_ONE, 1, 1, BasketQuantity.of(0), "장바구니에 해당 상품이 이미 최대치까지 들어있습니다.");
    }


    @Test
    @DisplayName("구매이력 + 장바구니에 담긴 갯수가 제한 갯수 이상인 경우 예외")
    void customLimitTest() {
        final BasketQuantity basketQuantity = BasketQuantity.of(4);
        assertThrowBuyLimit(BuyLimit.LIMIT_AMOUNT, 1, 5, basketQuantity, "이미 이전에 물품을 5개 구매한 이력이 있습니다.");
    }

    @Test
    @DisplayName("구매이력 + 장바구니에 담긴 갯수가 제한 갯수 아래인 경우 구매 갯수 반환")
    void buyAvailableTest() {
        final BasketQuantity basketQuantity = BasketQuantity.of(3);
        final int actual = BuyLimit.LIMIT_AMOUNT.remainQuantity(1, 5, basketQuantity);
        assertThat(actual).isEqualTo(1);
    }

    @Test
    @DisplayName("구매이력 + 장바구니에 담긴 갯수 둘다 0개에 최대값이 1개일 때 갯수 반환")
    void onlyOneBuyTest() {
        final BasketQuantity basketQuantity = BasketQuantity.of(0);
        final int actual = BuyLimit.LIMIT_AMOUNT.remainQuantity(0, 1, basketQuantity);
        assertThat(actual).isEqualTo(1);
    }

    private void assertThrowBuyLimit(final BuyLimit buyLimit, final int totalQuantity, final int totalQuantity1, final BasketQuantity of,
        final String message) {
        assertThatThrownBy(() -> buyLimit.remainQuantity(totalQuantity, totalQuantity1, of))
            .isInstanceOf(LimitQuantityException.class)
            .hasMessage(message);
    }

}