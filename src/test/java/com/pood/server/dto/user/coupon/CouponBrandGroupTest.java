package com.pood.server.dto.user.coupon;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.pood.server.entity.meta.CouponBrand;
import java.time.LocalDateTime;
import java.util.List;
import org.assertj.core.api.AbstractThrowableAssert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class CouponBrandGroupTest {

    @Test
    @DisplayName("불변 객체 테스트")
    void unmodifiedList() {
        //given
        CouponBrand couponBrand = new CouponBrand(1, 216, 10, LocalDateTime.now(),
            LocalDateTime.now());

        CouponBrandGroup couponBrandGroup = new CouponBrandGroup(List.of(couponBrand));

        //when
        final AbstractThrowableAssert<?, ? extends Throwable> abstractThrowableAssert = assertThatThrownBy(
            () -> couponBrandGroup.getCouponBrandList().set(1, couponBrand));

        //then
        abstractThrowableAssert
            .isInstanceOf(UnsupportedOperationException.class);
    }

    @Test
    @DisplayName("브랜드 id값 리스트로 만들기")
    void getCouponIdxList() {
        //given
        CouponBrand couponBrand = new CouponBrand(1, 216, 10, LocalDateTime.now(),
            LocalDateTime.now());
        CouponBrand couponBrand2 = new CouponBrand(1, 216, 11, LocalDateTime.now(),
            LocalDateTime.now());

        CouponBrandGroup couponBrandGroup = new CouponBrandGroup(
            List.of(couponBrand, couponBrand2));

        //when
        final List<Integer> resultList = couponBrandGroup.couponBrandIdxList(216);

        //then
        assertThat(resultList.size()).isEqualTo(2);
        assertThat(resultList.get(0)).isEqualTo(10);
        assertThat(resultList.get(1)).isEqualTo(11);
    }
}