package com.pood.server.dto.user.delivery;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.controller.request.RequestDtoValidationTests;
import java.util.ArrayList;
import java.util.List;
import javax.validation.ConstraintViolation;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserDeliveryUpdateRequestTest extends RequestDtoValidationTests {

    @Test
    @DisplayName("주소값이 null 일 때")
    void addressNullTest() {
        //given
        UserDeliveryUpdateRequest request = new UserDeliveryUpdateRequest(1, null, 0, "detailAddress",
            "name", "nickName", "zipcode", "01023444444");

        //when
        final List<ConstraintViolation<UserDeliveryUpdateRequest>> validate = new ArrayList<>(
            validator.validate(request));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("주소값은 필수값입니다.");
    }

    @Test
    @DisplayName("주소값이 빈 문자열일 때")
    void addressEmptyTest() {
        //given
        UserDeliveryUpdateRequest request = new UserDeliveryUpdateRequest(1, "", 1, "detailAddress",
            "name",
            "nickName", "zipcode", "01023444444");

        //when
        final List<ConstraintViolation<UserDeliveryUpdateRequest>> validate = new ArrayList<>(
            validator.validate(request));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("주소값은 필수값입니다.");
    }

    @Test
    @DisplayName("우편번호 null 일 때")
    void zipcodeNullTest() {
        //given
        UserDeliveryUpdateRequest request = new UserDeliveryUpdateRequest(1, "address", 0,
            "detailAddress", "name",
            "nickName", null, "01023444444");

        //when
        final List<ConstraintViolation<UserDeliveryUpdateRequest>> validate = new ArrayList<>(
            validator.validate(request));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("우편번호는 필수값입니다.");
    }

    @Test
    @DisplayName("우편번호 빈 문자열일 때")
    void zipcodeEmptyTest() {
        //given
        UserDeliveryUpdateRequest request = new UserDeliveryUpdateRequest(1, "address", 1,
            "detailAddress", "name",
            "nickName", "", "01023444444");

        //when
        final List<ConstraintViolation<UserDeliveryUpdateRequest>> validate = new ArrayList<>(
            validator.validate(request));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("우편번호는 필수값입니다.");
    }

    @Test
    @DisplayName("휴대폰번호 검증")
    void phoneNumberTest() {
        //given
        UserDeliveryUpdateRequest request = new UserDeliveryUpdateRequest(1, "address", 0,
            "detailAddress", "name",
            "nickName", "zipcode", "11111111111");

        //when
        final List<ConstraintViolation<UserDeliveryUpdateRequest>> validate = new ArrayList<>(
            validator.validate(request));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("휴대폰 번호 입력방식이 잘못되었습니다.");
    }
}