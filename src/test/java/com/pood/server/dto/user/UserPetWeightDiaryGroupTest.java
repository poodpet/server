package com.pood.server.dto.user;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.controller.response.pet.UserPetWeightDiaryResponse;
import com.pood.server.entity.group.UserPetWeightDiaryGroup;
import com.pood.server.entity.user.UserPetWeightDiary;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserPetWeightDiaryGroupTest {

    @Test
    @DisplayName("요청객체 파싱 테스트")
    void toResponseTest() {
        //given
        final UserPetWeightDiaryGroup group = new UserPetWeightDiaryGroup(
            List.of(
                new UserPetWeightDiary(1L, null, 3.5f, LocalDate.now(), LocalDateTime.now(),
                    LocalDateTime.of(2022, 5, 4, 0, 0, 0)),
                new UserPetWeightDiary(2L, null, 4.0f, LocalDate.now(), LocalDateTime.now(),
                    LocalDateTime.now()),
                new UserPetWeightDiary(3L, null, 4.5f, LocalDate.now(), LocalDateTime.now(),
                    LocalDateTime.now()
                )
            )
        );

        //when
        final List<UserPetWeightDiaryResponse> actualList = group.toResponse();

        //then
        assertThat(actualList.get(0).getIdx()).isEqualTo(1L);
        assertThat(actualList.get(0).getRecordBirth()).isEqualTo("2022.05.04");
    }

    @Test
    @DisplayName("날짜 에 대한 리스트 데이터 검색 기능")
    void userPetWeightDiaryByBaseDate(){
        final UserPetWeightDiaryGroup group = new UserPetWeightDiaryGroup(
            List.of(
                new UserPetWeightDiary(1L, null, 3.5f, LocalDate.of(2022,6,16), LocalDateTime.now(),
                    LocalDateTime.of(2022, 5, 4, 0, 0, 0)),
                new UserPetWeightDiary(2L, null, 4.0f, LocalDate.now(), LocalDateTime.now(),
                    LocalDateTime.now()),
                new UserPetWeightDiary(3L, null, 4.5f, LocalDate.now(), LocalDateTime.now(),
                    LocalDateTime.now()
                )
            )
        );
        UserPetWeightDiary userPetWeightDiaryByBaseDate = group.getUserPetWeightDiaryByBaseDate(
            LocalDate.of(2022, 6, 16));
        assertThat(userPetWeightDiaryByBaseDate.getIdx()).isEqualTo(1L);
    }
}