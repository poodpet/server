package com.pood.server.dto.user.coupon;

import com.pood.server.entity.user.UserCoupon;
import java.util.List;
import org.assertj.core.api.AbstractThrowableAssert;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class CouponResponseDtoGroupTest {

    @Test
    @DisplayName("불변 객체 테스트")
    void unmodifiedListTest() {
        //given
        CouponResponseDto couponResponseDto = CouponResponseDto.of(UserCoupon.builder().build(),
            null, List.of(1, 2, 3), List.of(1, 2, 3),1);

        CouponResponseDto couponResponseDto2 = CouponResponseDto.of(UserCoupon.builder().build(),
            null, List.of(1, 2, 3), List.of(1, 2, 3),2);

        CouponResponseDtoGroup couponResponseDtoGroup = new CouponResponseDtoGroup(
            List.of(couponResponseDto, couponResponseDto2));

        //when
        final AbstractThrowableAssert<?, ? extends Throwable> expected = Assertions.assertThatThrownBy(
            () -> couponResponseDtoGroup.getCouponResponseDtoList().set(0, couponResponseDto));

        //then
        expected.isInstanceOf(UnsupportedOperationException.class);
    }

}