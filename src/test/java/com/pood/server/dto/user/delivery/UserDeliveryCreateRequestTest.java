package com.pood.server.dto.user.delivery;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.controller.request.RequestDtoValidationTests;
import java.util.ArrayList;
import java.util.List;
import javax.validation.ConstraintViolation;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserDeliveryCreateRequestTest extends RequestDtoValidationTests {

    @Test
    @DisplayName("주소값이 null 일 때")
    void addressNullTest() {
        //given
        UserDeliveryCreateRequest request = new UserDeliveryCreateRequest(null, 0, "detailAddress",
            "name", "nickName", "zipcode", "01023444444");

        //when
        final List<ConstraintViolation<UserDeliveryCreateRequest>> validate = new ArrayList<>(
            validator.validate(request));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("주소값은 필수값입니다.");
    }

    @Test
    @DisplayName("주소값이 빈 문자열일 때")
    void addressEmptyTest() {
        //given
        UserDeliveryCreateRequest request = new UserDeliveryCreateRequest("", 0, "detailAddress",
            "name", "nickName", "zipcode", "01023444444");

        //when
        final List<ConstraintViolation<UserDeliveryCreateRequest>> validate = new ArrayList<>(
            validator.validate(request));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("주소값은 필수값입니다.");
    }

    @Test
    @DisplayName("우편번호 null 일 때")
    void zipcodeNullTest() {
        //given
        UserDeliveryCreateRequest request = new UserDeliveryCreateRequest("address", 0,
            "detailAddress", "name",
            "nickName", null, "01023444444");

        //when
        final List<ConstraintViolation<UserDeliveryCreateRequest>> validate = new ArrayList<>(
            validator.validate(request));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("우편번호는 필수값입니다.");
    }

    @Test
    @DisplayName("우편번호 빈 문자열일 때")
    void zipcodeEmptyTest() {
        //given
        UserDeliveryCreateRequest request = new UserDeliveryCreateRequest("address", 0,
            "detailAddress", "name",
            "nickName", "", "01023444444");

        //when
        final List<ConstraintViolation<UserDeliveryCreateRequest>> validate = new ArrayList<>(
            validator.validate(request));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("우편번호는 필수값입니다.");
    }

    @Test
    @DisplayName("휴대폰번호 검증")
    void phoneNumberTest() {
        //given
        UserDeliveryCreateRequest request = new UserDeliveryCreateRequest("address", 0,
            "detailAddress", "name",
            "nickName", "zipcode", "11111111111");

        //when
        final List<ConstraintViolation<UserDeliveryCreateRequest>> validate = new ArrayList<>(
            validator.validate(request));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("휴대폰 번호 입력방식이 잘못되었습니다.");
    }

}