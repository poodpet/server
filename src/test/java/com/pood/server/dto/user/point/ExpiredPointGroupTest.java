package com.pood.server.dto.user.point;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.entity.user.UserPoint;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.Test;

class ExpiredPointGroupTest {

    @Test
    void removedPoint() {
        //given
        final List<UserPoint> mockUserPointList = List.of(
            new UserPoint(1, "2199dfde-bed1-4d3b-8428-a160589fb0b8",
                "464978a0-9cff-4e24-a2e7-e12edd89d421",
                "202202148das", 1, 1000, "asasd", 31, 5000,
                1, LocalDateTime.now(), 1, 5000, 3000, LocalDateTime.now(), LocalDateTime.now()),
            new UserPoint(1, "a6e238a5-895d-4828-a9f9-add0ac0600dd",
                "49e9a12e-05a5-45f1-99c8-cb0335b97437",
                "202202148das", 1, 1000, "asasd", 31, 5000,
                1, LocalDateTime.now(), 1, 5000, 5000, LocalDateTime.now(), LocalDateTime.now())
        );
        ExpiredPointGroup expiredPointGroup = new ExpiredPointGroup(mockUserPointList);

        //when
        final int removedPoint = expiredPointGroup.removedPoint();

        //then
        assertThat(removedPoint).isEqualTo(2000);
    }
}