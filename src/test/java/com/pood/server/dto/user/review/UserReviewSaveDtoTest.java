package com.pood.server.dto.user.review;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.controller.request.RequestDtoValidationTests;
import java.util.ArrayList;
import java.util.List;
import javax.validation.ConstraintViolation;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserReviewSaveDtoTest extends RequestDtoValidationTests {

    @Test
    @DisplayName("주문번호 null일 경우 예외처리")
    void orderNumberNullTest() {
        //given
        UserReviewSaveDto saveDto = new UserReviewSaveDto(1, null, 1, 1, "reviewText");

        //when
        final List<ConstraintViolation<UserReviewSaveDto>> validate = new ArrayList<>(
            validator.validate(saveDto));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("주문 번호는 비어있거나 null 일 수 없습니다.");
    }

    @Test
    @DisplayName("주문번호 빈 문자열일 경우 예외처리")
    void orderNumberEmptyStringTest() {
        //given
        UserReviewSaveDto saveDto = new UserReviewSaveDto(1, "", 1, 1, "reviewText");

        //when
        final List<ConstraintViolation<UserReviewSaveDto>> validate = new ArrayList<>(
            validator.validate(saveDto));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("주문 번호는 비어있거나 null 일 수 없습니다.");
    }

    @Test
    @DisplayName("리뷰 코멘트 null일 경우 예외처리")
    void reviewTextNullTest() {
        //given
        UserReviewSaveDto saveDto = new UserReviewSaveDto(1, "orderNumber", 1, 1, null);

        //when
        final List<ConstraintViolation<UserReviewSaveDto>> validate = new ArrayList<>(
            validator.validate(saveDto));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("리뷰 코멘트는 비어있거나 null 일 수 없습니다.");
    }

    @Test
    @DisplayName("리뷰 코멘트 빈 문자열일 경우 예외처리")
    void reviewTextEmptyStringTest() {
        //given
        UserReviewSaveDto saveDto = new UserReviewSaveDto(1, "orderNumber", 1, 1, "");

        //when
        final List<ConstraintViolation<UserReviewSaveDto>> validate = new ArrayList<>(
            validator.validate(saveDto));

        //then
        assertThat(validate.get(0).getMessage()).isEqualTo("리뷰 코멘트는 비어있거나 null 일 수 없습니다.");
    }
}