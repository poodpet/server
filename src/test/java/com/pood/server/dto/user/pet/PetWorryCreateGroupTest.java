package com.pood.server.dto.user.pet;

import static org.assertj.core.api.Assertions.assertThat;

import com.pood.server.controller.request.userpet.PetWorryRequest;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class PetWorryCreateGroupTest {

    @ParameterizedTest
    @MethodSource
    @DisplayName("고민을 바꿔서 반환하는 group 은 priority 순으로 정렬")
    void sortByPriority(List<Long> replaceWorryIdx) {

        //given
        PetWorryCreateGroup origin = new PetWorryCreateGroup(
            List.of(
                new PetWorryRequest(1L, 1),
                new PetWorryRequest(2L, 2),
                new PetWorryRequest(3L, 3)
            )
        );

        List<Long> deleteWorryList = List.of(1L, 2L);

        PetWorryCreateGroup replace = origin.replaceWorry(replaceWorryIdx, deleteWorryList);

        assertThat(replace.getPetWorryCreateRequestList()).isSortedAccordingTo(
            Comparator.comparing(PetWorryRequest::getPriority));

        int i = 1;
        for (PetWorryRequest request : replace.getPetWorryCreateRequestList()) {
            assertThat(request.getPriority()).isEqualTo(i);
            i += 1;
        }
    }

    private static Stream<Arguments> sortByPriority() {
        return Stream.of(
            Arguments.of(List.of(11L)),
            Arguments.of(List.of(11L, 12L))
        );
    }

    @Test
    @DisplayName("유저가 선택한 고민 대체")
    void replaceWorry() {

        //given
        PetWorryCreateGroup origin = new PetWorryCreateGroup(
            List.of(
                new PetWorryRequest(1L, 1),
                new PetWorryRequest(2L, 2),
                new PetWorryRequest(3L, 3)
            )
        );

        //given
        List<Long> replaceWorryIdx = List.of(11L);

        List<Long> deleteWorryList = List.of(1L, 2L);

        PetWorryCreateGroup replace = origin.replaceWorry(replaceWorryIdx, deleteWorryList);

        assertThat(
            replace.getPetWorryCreateRequestList().stream().map(PetWorryRequest::getIdx).collect(
                Collectors.toList()))
            .contains(11L, 3L)
            .doesNotContain(1L, 2L);
    }

    @Test
    @DisplayName("대체하려는 고민의 크가가 3을 넘어가면 앞에 고민만 대체 (size check)")
    void replaceWorrySizeTest() {

        //given
        PetWorryCreateGroup origin = new PetWorryCreateGroup(
            List.of(
                new PetWorryRequest(1L, 1),
                new PetWorryRequest(2L, 2),
                new PetWorryRequest(3L, 3)
            )
        );

        //given
        List<Long> replaceWorryIdx = List.of(11L, 12L, 13L);

        List<Long> deleteWorryList = List.of(1L, 2L);

        PetWorryCreateGroup replace = origin.replaceWorry(replaceWorryIdx, deleteWorryList);

        assertThat(replace.getPetWorryCreateRequestList()).hasSize(3);

    }

    @Test
    @DisplayName("대체하려는 고민의 크가가 3을 넘어가면 앞에 고민만 대체 (value check)")
    void replaceWorryValueTest() {

        //given
        PetWorryCreateGroup origin = new PetWorryCreateGroup(
            List.of(
                new PetWorryRequest(1L, 1),
                new PetWorryRequest(2L, 2),
                new PetWorryRequest(3L, 3)
            )
        );

        //given
        List<Long> replaceWorryIdx = List.of(11L, 12L, 13L);

        List<Long> deleteWorryList = List.of(1L, 2L);

        PetWorryCreateGroup replace = origin.replaceWorry(replaceWorryIdx, deleteWorryList);

        assertThat(
            replace.getPetWorryCreateRequestList().stream().map(PetWorryRequest::getIdx).collect(
                Collectors.toList()))
            .contains(11L, 12L, 3L)
            .doesNotContain(1L, 2L, 13L);
    }
}