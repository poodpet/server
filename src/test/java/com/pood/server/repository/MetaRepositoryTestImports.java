package com.pood.server.repository;

import com.pood.server.config.database.MetaDatabase;
import com.pood.server.config.querydsl.MetaQuerydslConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.Transactional;

@DataJpaTest
@Transactional("metaTransactionManager")
@AutoConfigureTestDatabase(replace = Replace.NONE)
@ImportAutoConfiguration(MetaDatabase.class)
@Import(MetaQuerydslConfiguration.class)
public class MetaRepositoryTestImports {

}
