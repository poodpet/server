package com.pood.server.repository;

import com.pood.server.config.database.OrderDatabase;
import com.pood.server.config.querydsl.OrderQuerydslConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.Transactional;

@DataJpaTest
@Transactional("orderTransactionManager")
@AutoConfigureTestDatabase(replace = Replace.NONE)
@ImportAutoConfiguration(OrderDatabase.class)
@Import(OrderQuerydslConfiguration.class)
public class OrderRepositoryTestImports {

}
