package com.pood.server.repository;

import com.pood.server.config.database.UserDatabase;
import com.pood.server.config.querydsl.UserQuerydslConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.Transactional;

@DataJpaTest
@Transactional("userTransactionManager")
@AutoConfigureTestDatabase(replace = Replace.NONE)
@ImportAutoConfiguration(UserDatabase.class)
@Import(UserQuerydslConfiguration.class)
public class UserRepositoryTestImports {

}
