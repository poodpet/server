package com.pood.server.repository;

import com.pood.server.config.database.LogDatabase;
import com.pood.server.config.querydsl.LogQuerydslConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.Transactional;

@DataJpaTest
@Transactional("logTransactionManager")
@AutoConfigureTestDatabase(replace = Replace.NONE)
@ImportAutoConfiguration(LogDatabase.class)
@Import(LogQuerydslConfiguration.class)
public class LogRepositoryTestImports {

}
